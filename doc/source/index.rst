..
  *******************************************************************************
  Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

#####
Index
#####

The documentation for the *Stochastic Cognitive Model* (SCM) is divided into three parts:

Introduction
============

.. toctree::
  :glob:
  :maxdepth: 1

  prepare_scm_build.rst
  building_scm.rst

This part contains information on how to set up an installation of SCM and run simulations.


.. toctree::
  :caption: User Guide
  :glob:
  :maxdepth: 1

  user_guide/scm_user_guide.rst

  user_guide/UseSCMWithOpenPASS/*
  user_guide/TypesOfInformation/*
  user_guide/SensoryPerception/*
  user_guide/InformationHandling/*
  user_guide/DecisionMaking/*
  user_guide/DrivingBehaviour/*

  user_guide/CoordinateSystemsAndFormulary/*
  user_guide/SecondaryModules/*
  user_guide/ImplementationArchitecture/*

In this part, a systematic overview of SCM is given from a phenomenological point of view.
Its aim is to provide a user of the model with an understanding of the way SCM's logic and mechanisms were designed.
To aid this, a concise, limitedly technical description of the actual software implementation is also provided.


.. toctree-api::
   :caption: Developer Guide
   :glob:
   :maxdepth: 1

   developer_guide/scm_developer_guide.rst
   :api-doc:

The final part of the documentation provides a collection of information useful for developers wishing to change or extend the functionalities of SCM.
