..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _high_cognitive_:

**************
High Cognitive
**************

High Cognitive constitutes an alternative algorithm for the anticipation of lane changing vehicles from the adjacent right lane due to a slower vehicle in front of them.
It was developed by Philipp Ring in his PhD thesis (Modellierung kognitiver Prozesse für ein numerisches Fahrerverhaltensmodell, 2020, Technische Universität Dresden) and provides a complex anticipation mechanism which is based on real world driving data on motorways taken from a field operational test and a human subject study.
The algorithm can be activated by setting the parameter *EnableHighCognitiveMode* of the corresponding driver profile in the ProfilesCatalog to "true".
It is set "false" per default due to a higher resource demand.
It is then used for the evaluation of situations instead of SCM's standard mechanism.
:numref:`image_HighCognitiveSituationAndStateVariables_` illustrates the traffic situation addressed by the algorithm and all of its relevant state variables.

.. _image_HighCognitiveSituationAndStateVariables_:

.. figure:: _static/images/HighCognitiveSituationAndStateVariables.png
   :alt: HighCognitive - Traffic situation and relevant state variables

   HighCognitive - Traffic situation and relevant state variables
   (TTC - time to collision; THW - time headway)

The presence of a vehicle *EgoFront* is not necessary for the algorithm, but it will influence the anticipation of the lane change probability of the vehicle *RightFront*.
Furthermore, *RightFrontFar* has not necessarily to be a moving vehicle; it can also be a stationary object :math:`(v_{RightFrontFar} \ge 0)`.
Similarly to the SCM standard mechanism, HighCognitive provides the detection of the situation pattern and the calculation of its intensity, which will be explained in more detail subsequently.


.. _high_cognitive_situation_pattern_detection_:

Situation pattern detection
===========================

Prerequisites for the situation pattern evaluation are the following:

* a vehicle *RightFront* and a vehicle or object *RightFrontFar* must be present

* the ego-vehicle must approach the vehicle *RightFront* an the approach must be visually perceivable :math:`(0 \le TTC_{RightFront} < \infty)`

The situation pattern detection of High Cognitive extends the usual situation pattern *Anticipated lane changer from right* into a 2-by-2 sub-pattern.
First of all, there is a distinction between a *following* and an *approaching* relation among *RightFront* and *RightFrontFar*.
*Following* means, there is no relative movement perceivable between the two objects :math:`(|TTC_{RightFrontToRightFrontFar}| = \infty)`.
*Approaching* means, *RightFront* is getting closer to *RightFrontFar* and the movement is perceivable :math:`(0 < TTC_{RightFrontToRightFrontFar} < \infty)`.
Therefore, the situation pattern detection of HighCognitive is not triggered, if *RightFrontFar* is faster than *RightFront* :math:`(- \infty < TTC_{RightFrontToRightFrontFar} < 0)`.

The second dimension concerns the saliency of the situation, which is expressed by the time headway (THW) between *RightFront* and *RightFrontFar*.
A *salient approaching* or *following* is detected, if :math:`THW_{RightFrontToRightFrontFar}` is smaller or equal to the minimum following distance.
Otherwise, it is categorized as a *normal* situation.

These definitions were used by Ring to label situations from a human subject study in real world motorway traffic, which served as training data for the HighCognitive situation pattern detection algorithm.
The algorithm consists of three decision tree classifiers: *ApproachFollowClassifier* and subsequently *ApproachSaliencyClassifier* or *FollowSaliencyClassifier*.
They decide about the current situation pattern based on the inputs :math:`TTC_{RightFrontToRightFrontFar}` and :math:`THW_{RightFrontToRightFrontFar}`.
The output is averaged over the last 10 time steps for a more stable model behaviour.

Originally, the 2-by-2 sub-pattern distinction was solely used to influence the gaze behaviour of the agent, but this has become obsolete over the years due to a more complex control of visual attention, consisting of explicit :ref:`bottom-up<gaze_control_bottom_up_>` and :ref:`top-down<gaze_control_top_down_>` mechanisms.
In the current version of SCM, the saliency aspect of the algorithm is used to potentially elevate the risk class of the current situation, i.e. it is always handled as a *High risk* situation if a *Salient Approaching* or a *Salient Following* is detected by High Cognitive (see :ref:`base_intensity_calculation_`).


.. _high_cognitive_situation_intensity_calculation_:

Situation intensity calculation
===============================

The situation intensity (in case a :ref:`High Cognitive situation pattern was detected<high_cognitive_situation_pattern_detection_>`) is expressed by two aspects within the HighCognitive module: *Anticipation* and *Prediction*.

The *Anticipation* model calculates the probability that the vehicle *RightFront* is actually going the change into the ego-vehicle's lane.
It consists of a hybrid model using a Bayesian network and logistic regression.
The model inputs are :math:`TTC_{RightFront}`, :math:`\dot{TTC}_{RightFront}`, :math:`THW_{RightFront}`, :math:`\dot{THW}_{RightFront}`, :math:`TTC_{RightFrontToRightFrontFar}`, and if a vehicle *EgoFront* is present, additionally :math:`TTC_{EgoFront}` and :math:`\dot{TTC}_{EgoFront}`.
The model output is a numeric value between 0 and 1, expressing the anticipated lane change probability of vehicle *RightFront*.
Ring extracted the empirical data for this model from a human subject study in real world motorway traffic.

The *Prediction* model calculates the probability of a collision between the vehicle *RightFront* and the ego-vehicle in case of *RightFront* is changing into the ego-vehicle's lane and the ego-vehicle is continuing its current motion in an unaltered manner.
It consists of a Monte Carlo model using an array of real-world lane change trajectories taken from a field operational test.
The model inputs are :math:`v_{Ego}`, :math:`\Delta s_{RightFront}`, and :math:`\Delta t_{RightFront}`.
The model output is a numeric value between 0 and 1, expressing the predicted collision probability between *RightFront* and the ego-vehicle.

Originally, the situation intensity was calculated by High Cognitive by simply multiplying the two probability values for *Anticipation* and *Prediction*.
This changed when the current evaluation mechanism for situations was introduced which generally expresses situation intensity as the product of the probability for the occurrence of an event and the necessary severity of the ego-vehicle's reaction to this event (e.g., preventing a collision by braking).
In this case, the probability aspect is still the same, i.e. the probability of a lane change by *RightFront*, but the severity aspect is not compatible.
Therefore, only the *Anticipated* probability from the HighCognitive module is still used in the situation intensity calculation and the severity aspect is always calculated by the standard SCM algorithm.
