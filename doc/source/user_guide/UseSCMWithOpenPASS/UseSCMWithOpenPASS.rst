Use SCM with OpenPASS
---------------------

Make yourself familiar with how OpenPASS works in general. It comes with a few config files, see their default config set for a standard simulation.

.. note::

    Get and install OpenPASS following their instructions from
    https://gitlab.eclipse.org/eclipse/openpass/opSimulation

As SCM is built as a Functional Mockup Unit (FMU) it requires some tweaks to the default OpenPASS configs to work properly. An example set is included in this repository and can be found in ``e2eTests/Resources/Common/``.
The following subsections highlight some differences to the default set.

ProfilesCatalog
~~~~~~~~~~~~~~~

In the ProfilesCatalog, add a new ProfileGroup ``FMU1``, if it isn't there already and add a new ``Profile`` called ``RegularScm``.
The profile is the place to adjust all driver parameter SCM supports - here they can be adjusted to your needs (see examples for a whole example file).

Additionally to these parameters specific to the driver model behavior, there are some special parameters needed.
They are prefixed with ``Parameter_AssignSpecial_`` and are followed by ``Simulation`` or ``Vehicle`` respectively.

The ``simulation parameters`` are useful for the overall simulation, the ``vehicle parameters`` are necessary so that SCMs internal dynamics module can work properly.


.. list-table:: Additional ``Simulation``-prefixed parameter expected by SCM

    * - **Parameter**
      - **Description**
    * - RandomSeed
      - As SCM is a stochastic model, it is advisable to provide a random seed for the simulation.
    * - OutputPath
      - If configured, SCM produces some output files regarding its internal state / mental model. This path should be configured so that each agent has an individual place to write data.
    * - ConfigPath
      - Path to configs (e.g. ScmConfig.xml or GazeFollowerTimeSeries.xml)



.. list-table:: Additional ``Vehicle``-prefixed parameter expected by SCM

    * - **Parameter**
      - **Description**
    * - SteeringRatio
      - The steering ratio of the vehicle
    * - MaxSteering
      - The maximum possible steering wheel angle in both directions in radian
    * - NumberOfGears
      - The number of gears the vehicle has
    * - GearRatio<X>
      - Ratio for each gear. The number <X> of GearRatio entries should match the ``NumberOfGears`` from above. If a vehicle has 8 gears, there should be GearRatio1, GearRatio2, ..., GearRatio8.

Older ProfilesCatalog
---------------------
You may find older versions of the ProfilesCatalog from a time before SCM was build as a FMU. For theses configs, there exists a converter script in the ``OpenPASS project`` to rewrite the ProfilesCatalog to the needed format.
The conversion is done via XSL transformation which can be found in ``utils/rules/v2.0.8_1_to_v2.0.9_SCM_FMU``. Although this is a useful tool, it may not cover every possible edge case. Thus it might be necessary to manually check the converted config file afterwards.
One known limitation is that there is only one supported motorbike model ``BMW R 1200 GS`` which can be related to the ``RegularMotorbikeSCM profile`` and bus and truck models have to have these words actually written in their profile names.

ScmConfig
~~~~~~~~~

SCM comes with a config file including different options to log specific simulation data of the agents to files.

.. list-table:: ScmConfig Parameters

    * - **Parameter / LoggingDetails**
      - **Description**
    * - DriverParameter
      - If set to true (default) writes SCM-related driver parameter (e.g. comfortLateralAcceleration, outerKeepingIntensity, ...) to the *DriverSimulationOutput.xml* file.
    * - DriverValues
      - If set to true (default), writes SCM-related cyclic data to a file. This includes information about the ego vehicle as well as information about its surroundings (**Perception.csv**).
    * - XmlOutput
      - If set to true (default false), writes the DriverValues into a XML file instead of csv.


Running a simulation with OpenPASS
----------------------------------

First of all, the simulator ``OpenPASS`` needs to be downloaded or manually build (see official OpenPASS documentation). It is recommended though to rely on an official release version of OpenPASS.

``OpenPASS`` needs a specific set of config files in a directory called ``configs`` right besides the executable. The provided default configurations should be copied to this directory.

The artifact of SCM (``AlgorithmScm.fmu``) must be made available for ``OpenPASS``. It is advisable to create a new directory which will match the resource path referenced in the ProfilesCatalog (in this case ``e2eTests/Resources/Common/``).
However, this is not mandatory as long as the path chosen will match the actual position of ``AlgorithmScm.fmu``.

.. note::

    Of course it is possible to set the ``CMAKE_PREFIX_PATH`` for the SCM build to the ``resource`` directory so that the SCM FMU will be put in the correct place automatically.