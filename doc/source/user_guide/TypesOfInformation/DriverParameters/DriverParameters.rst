..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _driver_parameters_:

*****************
Driver Parameters
*****************

The behaviour of SCM drivers is decided by various parameters.
They are described in the XML form in the file `ProfilesCatalog.xml`, which needs to be placed in the `configs/` directory.
Together, the parameters create a profile for the driver behaviour.
Different profiles can be defined for one simulation configuration.

In the following paragraphs, the driver parameters will be described in detail.
They are grouped into thematic subsections.


Parameters Related to Perception
================================

* **Preview Distance**

  * `PreviewDistanceMin`
  * The preview distance is the distance around the SCM driver's vehicle in which observed objects are considered relevant.
    Its value gets calculated based on the current velocity.
    The parameters visibility distance and `PreviewDistanceMin` form the upper and lower limit.
  * Unit: m

* **Threshold Looming**

  * `ThresholdLooming`
  * The threshold looming is used to determine if an observed vehicle is making a lane change.
    The value represents a ratio between lateral velocity and relative longitudinal distance of the other vehicle.
    If it is exceeded, SCM will percieve the other vehicle as making a lane change.
  * Unit: ∅

* **Threshold for the Angular Speed of a Retinal Projection in the Fovea**

  * `ThresholdRetinalProjectionAngularSpeedFovea`
  * This value represents the threshold above which a change of the size of a retinal projection in the fovea centralis can be perceived by the human eye.
  * Unit: rad/s

* **Ideal Perception**

  * `IdealPerception`
  * A flag that if given bypasses the regular perception model of SCM and lets the agent perceive all of their surroundings completely and permanently without any inaccuracies.
  * Boolean value


Parameters Related to Reaction Time
===================================

* **ReactionBaseTime**

  * `ReactionBaseTime`
  * The "reaction time" is the time the driver needs to complete a decision-making process.
    Its values are drawn from a distribution function configured based on the attributes of the parameter ReactionBaseTime.
    See :ref:`ReactionBaseTime<reaction_base_time_>` for further details.
  * Unit: s


.. _driver_parameters_simplified_cognition_:

Parameters Related to Simplified Cognition
==========================================

* **High Cognitive Mode**

  * `EnableHighCognitiveMode`
  * This flag activates or deactivates the use of the high cognitive algorithm by the agent.
    It is described in the section :ref:`high_cognitive_`.
  * Boolean value

* **Anticipation Quota**

  * `AnticipationQuota`
  * This quota represents the amount of drivers which are able to anticipate the actions of other agents.
  * Unit: ∅ (possible values from 0 to 1)

* **Agent Cooperation Factor**

  * `AgentCooperationFactor`
  * The agent cooperation factor determines how strongly the driver engages in cooperative behavior.
  * Unit: ∅ (possible values from 0 to 1)

* **Agent Evasion Factor for Suspicious Behaviour**

  * `AgentSuspiciousBehaviourEvasionFactor`
  * This parameter determines the agents behaviour when detecting slow or stillstanding vehicles on a side lane.
  * Unit: ∅ (possible values from 0 to 1)

* **Ego Lane Keeping Intensity**

  * `EgoLaneKeepingIntensity`
  * This value represents a bias of the driver for staying on his current lane over executing a lane change.
  * Unit: ∅ (possible values from 0 to 1)


Parameters Related to Velocity Control
======================================

* **Desired Velocity**

  * `VWish`
  * This value is the velocity the driver tries to drive at when there are no outer influences preventing him from doing so.
  * Unit: m/s

* **Desired Time at Target Speed**

  * `DesiredTimeAtTargetSpeed`
  * This parameter represents the mimium amount of time the driver wants to drive at his targeted speed in a specific lane.
  * Unit: s


Parameters Related to Car Following Behaviour
=============================================

* **Proportionality Factor for Following Distance**

  * `proportionalityFactorForFollowingDistance`
  * The proportionality factor for the reduction of following distance at higher speeds (Steven's power law).
  * Unit: ∅

* **Car Queueing Distance**

  * `carQueuingDistance`
  * The distance the driver tries to maintain to his leading vehicle at standstill.
  * Unit: m


Parameters Related to Acceleration Behaviour
============================================

* **Maximum Longitudinal Acceleration**

  * `MaxLongitudinalAcceleration`
  * The maximum longitudinal acceleration of the driver.
  * Unit: m/s²

* **Maximum Longitudinal Deceleration**

  * `MaxLongitudinalDeceleration`
  * The maximum longitudinal deceleration of the driver.
  * Unit: m/s²

* **Maximum Lateral Acceleration**

  * `MaxLateralAcceleration`
  * The maximum lateral acceleration of the driver.
  * Unit: m/s²

* **Comfort Longitudinal Acceleration**

  * `ComfortLongitudinalAcceleration`
  * The comfort longitudinal acceleration of the driver.
  * Unit: m/s²

* **Comfort Longitudinal Deceleration**
  * `ComfortLongitudinalDeceleration`
  * The comfort longitudinal deceleration of the driver.
  * Unit: m/s²

* **Acceleration Adjustment Threshold Velocity**

  * `AccelerationAdjustmentThresholdVelocity`
  * The velocity below which the comfort acceleration is linearly increased from comfort acceleration to maximum comfort acceleration.
  * Unit: m/s

* **Velocity For Maximum Acceleration**

  * `VelocityForMaxAcceleration`
  * The velocity below which the maximum comfort acceleration is used as the comfort acceleration.
  * Unit: m/s

* **Maximum Comfort Acceleration Factor**

  * `MaxComfortAccelerationFactor`
  * Factor for the maximum comfort acceleration. Maximum comfort acceleration is calculated by mutliplying this factor by comfort acceleration.
  * Unit: ∅

* **Maximum Comfort Deceleration Factor**

  * `MaxComfortDecelerationFactor`
  * Factor for the maximum comfort deceleration. Maximum comfort deceleration is calculated by mutliplying this factor by comfort deceleration.
  * Unit: ∅

* **Comfort Lateral Acceleration**

  * `ComfortLateralAcceleration`
  * The comfort lateral acceleration of the driver.
  * Unit: m/s²

* **Comfort Lateral Acceleration Mean**

  .. TODO is this still in use?

  * `ComfortLateralAccelerationMean`
  * The mean value of the comfort lateral acceleration of all drivers.
  * Unit: m/s²


.. _driver_parameters_lateral_positioning_:

Parameters Related to Lateral Positioning
=========================================

* **Lateral Offset Neutral Position**

  * `LateralOffsetNeutralPosition`
  * The lateral offset the driver will keep to the center of the lane.
  * Unit: m

* **Lateral Offset Neutral Position Quota**

  * `LateralOffsetNeutralPositionQuota`
  * Percentage of drivers which drive off-center in the lane.
  * Unit: ∅

* **Lateral Offset Neutral Position Rescue Lane**

  * `LateralOffsetNeutralPositionRescueLane`
  * The lateral offset the driver will keep to build up a rescue lane.
  * Unit: m


Parameters Related to Traffic Rules
===================================

* **Right-Keeping Factor**

  * `RightKeepingFactor`
  * A factor between 0 and 1 to apply the keep-right-rule (the higher the factor, the higher the obligation to the keep-right-rule).
  * Unit: ∅

* **Right Overtaking Prohibition Ignoring Quota**

  * `RightOvertakingProhibitionIgnoringQuota`
  * The amount of drivers that violate the prohibition to overtake on right side (0 to 1).
  * Unit: ∅

* **Lane Change Prohibition Ignoring Quota**

  * `LaneChangeProhibitionIgnoringQuota`
  * The amount of drivers, which violate the prohibition to change lanes due to lane markings (0 to 1).
  * Unit: ∅

* **Amount of Speed Limit Violation**

  * `DeltaVViolation`
  * The amount of velocity which the driver is willing to violate the speed limit.
  * Unit: m/s

* **Amount of Desired Velocity Violation**

  * `DeltaVWish`
  * The amount of velocity which the driver is willing to violate his own desired velocity in uncomfortable situations.
  * Unit: m/s


Parameters Related to Off-Center Driving
========================================

* **Lateral Offset Neutral Position Quota**

  * `LateralOffsetNeutralPositionQuota`
  * The amount of drivers that will drive on an offset position from the lane center at certain speeds (0 to 1).
  * Unit: ∅

* **Lateral Offset Neutral Position**

  * `LateralOffsetNeutralPosition`
  * The offset the drivers will shift from the center. The amount will gradually rise with lower velocities.
  * Unit: m

* **Lateral Offset Neutral Position Rescue Lane**

  * `LateralOffsetNeutralPositionRescueLane`
  * In traffic jam with low velocities the drivers will form a rescue lane and shift even further from the center.
  * Unit: m


.. _driver_parameters_lane_change_:

Parameters Related to Lane Changes
==================================

* **Maximum Angular Velocity of the Steering Wheel**

  * `MaximumAngularVelocitySteeringWheel`
  * The maximum angular velocity the driver can turn the steering wheel in urgent situations.
  * Unit: rad/s

* **Comfort Angular Velocity of the Steering Wheel**

  * `ComfortAngularVelocitySteeringWheel`
  * The comfort angular velocity the driver will turn the steering wheel in common situations.
  * Unit: rad/s

* **Time Headway for Free Lane Changes**

  .. TODO check description of this parameter

  * `TimeHeadwayFreeLaneChange`
  * The time headway to be used for distance calculations as part of free lange changes.
  * Unit: s


Non-adjustable Parameters
=========================

There are also a number of parameters that cannot be adjusted by the user.
However, they are still playing important roles in determining driver behaviour.

* **AdjustmentBaseTime**

  * `AdjustmentBaseTime`
  * The "adjustment time" represents the length of intervals in which the driver holds a constant acceleration before reevaluating and changing it.
    Its values are drawn from a distribution function configured based on the attributes of the parameter AdjustmentBaseTime.
    See :ref:`AdjustmentBaseTime<adjustment_base_time_>` for further details.
  * Unit: s

* **Pedal Change Time**

  * `PedalChangeTime`
  * The duration of time the driver needs to move their foot from the accelerator pedal to the braking pedal (or vice versa) is represented by this parameter.
  * Unit: s
  
* **Speed Limit Anticipation Percentage**

  * `SpeedLimitAnticipationPercentage`
  * This parameter determines how early the driver starts reacting to a new speed limit.
  * Unit: ∅ (possible values from 0 to 1)

* **Lateral Safety Distance for Evading**

  * `LateralSafetyDistanceForEvading`
  * The safety distance is the lateral separation the driver aims for when planning the evasion of an obstacle.
  * Unit: m

* **Car Restarting Distance**

  * `carRestartDistance`
  * The distance to the leading vehicle at which the driver will start moving his vehicle again after standstill in a traffic jam. 
    It is the sum of the `carQueuingDistance` and the `HysteresisForRestart` property.
  * Unit: m

