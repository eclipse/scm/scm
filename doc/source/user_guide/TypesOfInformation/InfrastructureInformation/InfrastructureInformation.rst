..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _infrastructure_information_:

Infrastructure Information
==========================

While :ref:`microscopic_information_` and :ref:`mesoscopic_information_` consider the dynamic aspects of traffic, *infrastructure information* can be defined as the sum of all features of the static traffic environment.
All infrastructure related information about traffic controls and geometric features of the lanes as perceived by the current agent is taken into account.
It can be split into two main categories:

* :ref:`sensor_driver_traffic_rule_information_`,
  which contains infrastructure information of surrounding lanes that are used to establish traffic rules (e.g. traffic signs and lane markings).
* :ref:`sensor_driver_geometry_information_`,
  which contains infrastructure information of surrounding lanes that describe its geometric features (e.g. lane features like curvature and width).

This information is generated in a track-related fashion (laneLeft, laneEgo and laneRight), similar to :ref:`mesoscopic_information_`.
Currently there is no perceptual change or limitation in the use of data; the model has access to the full extent and quality of data from the ground truth.
A more detailed, technical overview of the infrastructure information can be found in :ref:`sensor_driver_`.


Determination of geometric infrastructure information from traffic signs
------------------------------------------------------------------------

A special feature of the model is the ability to derive information about the road infrastructure from traffic signs before the infrastructure element itself becomes visible to the driver.
The mechanisms are rather similar and are described in the following subsections.


Determination of an upcoming end of a lane
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

SCM is able to evaluate traffic signs regarding the announcement of an upcoming end of a lane.
The considered traffic signs here according to the German federal traffic sign catalogue are depicted in :numref:`image_TrafficSignsEndOfLane_`.
The bars above indicate the simplified type used by the model internally.

.. _image_TrafficSignsEndOfLane_:

.. figure:: ../_static/images/TrafficSignsEndOfLane.png
  :alt: Traffic signs for the announcement of an upcoming end of a lane

  Traffic signs for the announcement of an upcoming end of a lane
  ("AnnounceRightLaneEnd" - indicating the end of the rightmost lane; "AnnounceLeftLaneEnd" - indicating the end of the leftmost lane)


The end of a lane may not be visible to the driver yet, but a traffic sign such as one depicted in :numref:`image_TrafficSignsEndOfLane_` is presented can be seen ahead.
In this case, the model evaluates whether this information applies for the current ego lane, the current left lane, or the current right lane of the driver.
If this is true and the information is new, the value for the *distance to the end of the lane* is estimated based on the distance between driver and traffic sign itself and the printed distance information.
The sign does not indicate the distance itself but is accompanied by a supplementary sign as shown in :numref:`image_SupplementarySignDistanceIndication_`:

.. _image_SupplementarySignDistanceIndication_:

.. figure:: ../_static/images/SupplementarySignDistanceIndication.png
  :alt: Supplementary traffic sign for the additional information about distance, for instance to an end of a lane

  Supplementary traffic sign for the additional information about distance, for instance to an end of a lane

If the sign was already seen in an earlier time step, the driver extrapolates the mental value for the remaining distance based on the estimated absolute
velocity of the driver's own vehicle.
If another sign of this type comes into view, the estimated information is updated by resetting the mental value for the distance to the end of the lane with the newer distance information.
Once the end of the lane becomes visible to the driver, all traffic signs associated with this aspect are neglected by the driver and the ground-truth information is used instead.


Determination of an upcoming highway exit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The evaluation of an upcoming highway exit from traffic signs is similar to the evaluation of an upcoming end of a lane (see chapter above).
The most distinguishing part here is that the driver has to make use of two important distances:

* The *distance to the start of the exit* marks the beginning of the highway exit, where the exit lane diverges from the main road and the driver has the first chance of changing into the highway exit.
* The *distance to the end of the exit* marks the end of the highway exit, where the exit lane completely severs from the main road and the driver has the last chance of changing into the highway exit.

Both distance information can be estimated from traffic signs, in case the associated feature is not yet visible to the driver.
On German motorways, the distance to the end of a highway exit is indicated by an announcement sign like the one shown on the left side of :numref:`image_TrafficSignsHighwayExit_`.
The indicated distance value is 1000 metres per default.
The distance to the start of a highway exit is indicated by smaller poles next to the rail guard like the ones shown on the right side of :numref:`image_TrafficSignsHighwayExit_`.

.. _image_TrafficSignsHighwayExit_:

.. figure:: ../_static/images/TrafficSignsHighwayExit.png
  :alt: Traffic signs for the announcement of an upcoming highway exit

  Traffic signs for the announcement of an upcoming highway exit

The process of deriving information from the traffic signs is fairly similar to the one for the end of a lane:

* If the traffic sign is new, the associated distance is estimated based on the indicated value and the distance between the driver and the sign itself.
* If the traffic sign was evaluated already in one of the last time steps, the associated distance is extrapolated by means of the estimated absolute velocity of the driver's own vehicle.
* In case of the highway exit poles: If a new sign comes into view, the mental value for the distance to the start of the exit is reset with the more recent distance information.
* If one of the two features becomes actually visible to the driver, the ground-truth information is used directly and the traffic sign information is neglected.

One major difference is the fact that the driver needs an estimate for both distances for the behaviour calculation at highway exits.
However, the information from the traffic signs is usually not present at the same time.
For instance, the sign for announcing a highway exit is presented about 1 kilometre before the actual exit, but the first highway exit pole (300 metres before the exit) may not be visible to the driver at this moment.
Therefore, the driver has some information about the distance to the end of the exit, but as he does not know how long the exit lane is, he has no information about the distance to the start of exit.
Therefore, an estimation mechanism is implemented, which postulates a length of 250 metres for the exit lane.
This value is used to estimate the other distance information, as long as only one of the two can be derived from traffic signs.

.. TODO more than just the end of lane and highway exit need to be described


Traffic Rules
-------------

The driver remembers the current value of the speed limit they perceived at the last relevant traffic sign.

.. TODO subchapter needs to be written
