..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _manoeuvres_:

**********
Maneuvers
**********

.. TODO unify spelling of maneuvers 

SCM includes a number of more complex mechanisms for longitudinal and lateral control which are described as maneuvers in this chapter.
They enable the model to perform sophisticated driving behaviour that cannot be achieved with simple controllers.


.. _lane_changes_:

Lane Changes
============

The lane change maneuver consists of three phases:

* a steering action to reach the desired heading for the lane change
* a straight segment at the desired heading angle and
* a second steering action to realign with the orientation of the road.

.. figure:: ../_static/images/TrajectoryLaneChangeSimplified.png

  Simplified lane change trajectory

The trajectory of the steering sections is also made up of three phases, as seen in the depiction below:

* The first part is a clothoid segment representing the motion of turning the steering wheel (and subsequently the wheels themselves).
* Once the desired steering wheel angle is reached, it may remain constant for a while.
  A circle segment represents this second phase.
* Finally, the steering wheel motion back into the neutral position is once again the shape of a clothoid.

.. figure:: ../_static/images/TrajectorySteeringManeuver.png

  Steering maneuver

Therefore, a complete lane change trajectory consists of clothoid - circle - clothoid - straight - clothoid - circle - clothoid, as shown below.
Note: In special cases, the length of one or more of the circles or straight segments may be zero.

.. figure:: ../_static/images/TrajectoryLaneChange.png

  Complete lane change trajectory

The width of the maneuver arises from the simple lane width to left or the negative simple lane width in case of a lane change to the right.
The lane change length is determined by the velocity of the ego agent and several other factors, depending on the type of lane change.
A free lane change is characterized by either not being on a collision course with the causing vehicle or by not being caused by another vehicle at all.
The second can e.g. be the case, if a driver has to change lanes in order to leave the highway.
The lane change length is then calculated with the help of driver specific parameters  (see :ref:`driver_parameters_lane_change_`).

.. math::
  s_{maneuver} = THW_{FreeLaneChange} * v_{ego, absolute}

.. TODO check term for "time headway"

For a restricted lane change, the other (lane change causing) object is considered as well.
This is done with the help of the values for relative net distance, time to collision and time headway.
With this information, the lane change can be completed before the collision occurs.

.. math::
  s_{maneuver} = d_{rel, net} + (TTC - THW) * v_{ego, absolute}

In case of a side collision risk, a simplified calculation is used.

.. math::
  s_{maneuver} = TTC_{lateral} * v_{ego, absolute}

The final type of restricted lane changes is confined by a lane end.
Here, the maneuver has to be completed before reaching the end of the current lane, of course.

Note: In case one of the restricted lane changes possesses a greater length than the free lane change, the latter one is used.

.. TODO update ref when driver parameter chaptter is finished

With these parameters, the final maneuver length can be determined.
At this point, it is also decided whether the maneuver will be a comfort or an urgent maneuver.
At first, the possible maneuver length is calculated with the comfort angular velocity of the steering wheel (see :ref:`driver_parameters_lane_change_`).
In case the result exceeds the available distance and the driver cannot execute the maneuver at comfort level, the calculations are redone with the driver's maximum possible steering wheel angular velocity.

These parameters allow an iterative calculation of the distance along the path (s) and lateral displacement from starting point (t) coordinates as well as the curvature at a set amount of points along the path of the trajectory for every necessary segment.
The distance between points remains the same along the trajectory.
The picture below displays the path with a significantly reduced number of points for comprehensive reasons.

.. figure:: ../_static/images/TrajectoryLaneChangePath.png

  Lane change trajectory path

The basic priciple for the iterative calculation:

.. math::
  ds = s_{current} - s_{last}

.. math::
  \kappa_{(s)} = \kappa_{d} / v_{ego, absolute} * s_{current}

.. math::
  d\psi = \kappa_{(s)} * ds

.. math::
  \psi_{s} = \psi_{s, last} + d\psi

.. math::
  dx = cos(\psi_{s}) * ds

.. math::
  dy = sin(\psi_{s}) * ds

.. math::
  x_{current} = x_{last} + dx

.. math::
  y_{current} = y_{last} + dy


The intitial values for :math:`s_{last}`, :math:`x_{last}` and :math:`y_{last}` are zero or the final value of the previous segment.

The steps to create the trajectory for a lane change are performed once at the start of the maneuver.
Every following step records the actual distance traveled in the s and t direction since that initial step.
The s value is then used to name the predicted lateral displacement and curvature.
This is done by finding the closest match of the s coordinate.
The corresponding lateral displacement and curvature are compared to the actual current values.
Finally, these difference are recorded as the lateral deviation and heading error.


.. _manoeuvres_swerving_:

Swerving
========

The lateral control for the execution of an evading maneuver is applicated for the lateral action *swerving*.
The swerving action may further be categorized as *urgent swerving* or *comfort swerving*.
The swerving maneuver differs from the normal lane change executing an aimed evasion in front of an obstacle.
The width of the maneuver :math:`w_{maneuver}` does not extend to the whole lane width but only to a lateral offset as far as necessary to avoid the obstacle.
(Avoiding the obstacle while staying within the current lane is also possible.)
The length of the maneuver :math:`s_{maneuver}` is highly dependent on the still available relative distance to the obstacle.
After avoiding the obstacle successfully, the driver can either drive back to the start lane or turn into one of the potential side lanes.
A swerving maneuver is devided into three phases:

* *Evading*: avoidance-movement in front of the obstacle
* *Defusing*: passing-by at the obstacle
* *Returning*: returning to start lane or final lane change into one of the possible side lanes

The *evading* phase uses the mechanism of a lane change (as described in :ref:`lane_changes_`) where the width of the maneuver :math:`w_{maneuver}` and the maneuver length :math:`s_{maneuver}` are always dependent on a targeted object (i.e. a restricted and not a free lane change).
The desired lateral displacement is derived from the sum of the lateral obstruction between the two objects (=lateral overlap) and a lateral safety distance.
The latter depends on the type of swerving.
For urgent swerving, the lateralSafetyDistanceForEvading, a predefined value from the driver parameters, will be applied.
In case of comfort swerving, the amount is dependent on the velocity of ego (0.2 m at standstill and linearly increased to 1 m at 100 km/h and above).
The maneuver length is set by the relative net distance to the obstacle (as in all restricted lane changes).

After reaching the lateral displacement :math:`w_{maneuver}`, the *defusing* phase begins.
It uses similar bases as lane keeping while the maneuver width of the *evading* phase is maintained as the set lateral coordinate :math:`w_{set}`:

.. math::
  \kappa_{maneuver} = 0

.. math::
  \Phi_{set} = 0

.. math::
  w_{set} = w_{maneuver,evading}


The *defusing* phase ends, once the obstacle has been observed in one of the rear AOIs of the ego agent.
Additionally, the expected duration to pass the obstacle is estimated.
This is done with the help of the longitudinal obstruction (longitudinal distance to be exactly in front of the obstacle), the difference in velocity and the difference in acceleration.

.. math::
  t = -\Delta v + \sqrt(\Delta v^2 + (2 * \Delta a * l_{Lane Change}) / (1/4 * \Delta a^2)) + 1

The additional second added in the end represents a safety buffer for unexpected changes in the acceleration.
Once this time has passed, a top-down request is applied to the EGO_REAR AOI with a very high priority.

The *returning* phase excecutes a normal lane change (see :ref:`lane_changes_`) back to the start lane or into the potentially existing side lane in the direction of the swerving-maneuver.
For this, the maneuver width :math:`w_{maneuver}` is calculated based on the the lateral position of the agent at the end of the *defusing* phase, so that the lane center can be approached.
The criteria for the decision between start lane and side lane is defined as follows:
If one of the both lanes is not safe for the lane change, the other one will be chosen.
If both lanes are safe for a lane change, the lane with the smallest resulting maneuver width :math:`w_{maneuver}` will be selected.
If both lanes are unsafe for a lane change, the lane with the bigger TTC in the respective AOI FRONT will be chosen.


Merging
=======

.. _merge_preparation_:

Calculation of merge preparation maneuvers
------------------------------------------

Merge preparation maneuvers describe the planned alignment towards a (narrow) gap on a neighbouring lane in preparation for a lane change.
The execution of the lane change itself is done by the lane change action (under urgency), but the preparing action before that is executed as the lateral action *preparing to merge*.
The preparing actions are actually described by longitudinal guidance of the vehicle, which means that the lateral action pattern is equal to *lane keeping*.
The lateral action is still marked as merging to better handle the driver's actions in those situations.

Merge preparation maneuvers and the subsequent lane changes under urgency are triggered by navigational restraints on the driver.
Currently, the following events can trigger a merge preparation:

.. TODO check term for CURRENT_LANE_BLOCKED

* approaching the end of the current ego lane (situation CURRENT_LANE_BLOCKED)
* approaching a highway exit the driver is assigned to take (implicitly triggered by :ref:`sensor_driver_`)

The start of the evaluation process for the merge preparation maneuvers will be triggered in the decision making process for the lateral action in the corresponding situations, if a normal lane change is not possible right away (see :ref:`decision_making_`).
It evaluates if the merge is generally possible and then investigates the available gaps and the driving manoeuvre needed to reach them.
Finally, if at least one of the gaps is valid for merging into, the valid gap that can be reached in the least amount of time is selected.

*MergeRegulate* is the AOI that is considered to be the target vehicle for the necessary longitudinal guidance actions and also the leading vehicle of the merge space (see :numref:`image_PossibleMergeSpaces_` for depiction).
It is not necessarily equal to regulating vehicle for the car-following-behaviour (:ref:`action_manager_leading_vehicle_`), but in most cases, it is.
There are the following exceptions:

* The AOI considered as MergeRegulate is empty.
  This can happen if there there is no leading vehicle for the gap.
  In this case, the driver should only overtake the following vehicle behind the merge space.
* The minimum following distance under urgency towards EGO_FRONT is violated while executing the merge preparation maneuver.
  In this case, the driver should switch to EGO_FRONT as the regulating vehicle to prevent a rear-end collision (see also "Blocking vehicle" below).

First, it is determined whether a lane change is not possible or suitable by means of any merge preparation maneuvers.
The general criteria evaluated here, which negate the further evaluation right from the get-go, are the following:

* A merging maneuver is already set.
* The considered target lane does not exist.
* The considered target lane is ending soon.
* The mental information about the vehicles on the considered target lane is too old and therefore not reliable enough for an evaluation.

If none of those criteria are met, the actual evaluation process for a merge preparation maneuver is started for the considered target lane.
The possible merge spaces which are evaluated by the driver are depicted in principal in :numref:`image_PossibleMergeSpaces_`, exemplified by the right lane:

.. _image_PossibleMergeSpaces_:

.. figure:: ../_static/images/PossibleMergeSpaces.png
  :alt: Possible merge spaces evaluated by the driver (exemplified by the right lane)

  Possible merge spaces evaluated by the driver (exemplified by the right lane)

The merge spaces themselves are considered as the actual physical distances between the corresponding leading and following vehicle, minus the minimum distances towards those vehicles accepted by the driver under the current lane change urgency.
If a merge space has no leading vehicle or no following vehicle, the respective border is depicted at the end of the preview distance of the driver.
SIDE vehicles are handled differently in this matter, because if they are missing, FRONT and REAR automatically become leading and following vehicle.
FRONT_FAR cannot exist if FRONT does not exist, because it is always considered as the second closest vehicle towards the ego vehicle in the front.
The evaluation of the merge spaces is accompanied by the calculation of the minimum distances to the leading and following vehicle, if present.

The size of the merge spaces is only one part of the evaluation.
The second half consists of the calculation of the reachabilities for each of those merge spaces.
The term reachability is defined as the time which is necessary to complete a merge preparation along a merge space so that the subsequent lane change into the merge space can start.
The first step for this evaluation is the calculation of the relative distances which the ego vehicle has to overcome in order for the lane change to become possible.
The general approach is that the minimum distances to the leading or following vehicle are not violated anymore.
In most cases, the leading vehicle is the regulation target for this calculation.
But for large merge spaces in front of the ego vehicle (if the merge space is 50 metres long, for instance), it would not be plausible to drive all the way up to the minimum distance of the leading vehicle for reaching the merge space.
In those cases, the necessary relative distance to overcome by the ego vehicle is considered when the minimum distance to the following vehicle is not violated anymore.

.. TODO check naming of driver parameters when driver parameter chapter is done 

With the result from this step, the reachability of a merge space can be calculated.
The general reachabilities consider the necessary time to reach the associated merge space by different methods.
In case of accelerating and decelerating, the change of velocity of the ego vehicle is only possible within certain boundaries.
For accelerating, a maximum velocity of the driver's desired velocity plus an amountOfDesiredVelocityViolation is considered and the driver's maximumLongitudinalAcceleration.
For decelerating, a minimum velocity of 10 km/h is considered as well as the driver's comfortLongitudinalDeceleration (see :ref:`driver_parameters_`).
If the merge space is not reachable in the accelerating or decelerating phase, it can only be reached by a subsequent constant driving with said velocity threshold.

The second step consists of the evaluation, if the actions necessary for the possible gaps can be executed before reaching the end of the available space for the merge preparation.
This depends on the situation that triggered the merge preparation in the first place.

The final step of the evaluation consists of the consideration of a possible blocking vehicle.
Generally, EGO_FRONT will be considered a blocking vehicle, if the merge space lies ahead of the ego vehicle and EGO_REAR will be considered a blocking vehicle, if the merge space lies behind the ego vehicle.
In both cases, a merge space will be regarded as unreachable, if it results in violating the minimum distance towards the blocking vehicle accepted by the driver under the current lane change urgency.

Finally, if the option of "constant driving" survives until the end of the evaluation as a possible way to reach the regarded merge space, it will be marked as preferred.
Otherwise, the manoeuvres will be compared on their reachability.

After all merge spaces are evaluated in terms of size and reachability, the driver has to choose one of the merge spaces.
If no merge space seems to be possible for a merge, because they are all too small or cannot be reached, no merge preparation maneuver will be started.
If exactly one merge space remains, the driver will choose that merge space.
If several merge spaces remain, the driver will choose the optimal merge space by evaluating their sizes, reachabilities and whether or not they can be reached without accelerating or decelerating.
