..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _reliability_:

***********
Reliability
***********

Decisions in SCM are based on several conditions.
Amongst others, the reliability of existing information plays an important role.
Depending on how reliable the given information is assessed, mental actions are triggered, such as information requests.


Reliability of mental information about surrounding objects
-----------------------------------------------------------

*Reliability* is defined as value between 0 and 100 (percent) and describes the quality of mental information about surrounding objects.
Every piece of information on a mentally represented surrounding object has a reliability attached.
This number can be checked against a required level of quality, so the model can decide if the information is still usable or should be updated by means of gaze allocation (see :ref:`information_requests_`).
There are two ways of assessing the reliability of an object’s information:

* quality of visually perceived information
* age of mentally extrapolated information

The quality of *visually perceived* information varies depending on the spot of perception on the retina (see :ref:`gaze_control_`).
In the fovea, i.e. directly along the line of sight, the visual quality is at its highest and therefore represents a reliability of 100.
The further away from the line of sight the presentation occurs, the more the visual quality decreases.

According to the modelling theory of :ref:`gaze_control_`, *cognitive* perception of visual data outside the UFOV is not possible.
That means, the reliability based on visual perception can only be evaluated for presentations of surrounding objects within the borders of the UFOV.
The horizontal extent of the UFOV is defined as 60° and the (optical) reliability on the border is defined as 200/7 (see :ref:`information_requests_`).
The reliability based on visual perception is calculated by linear interpolation between the fovea and the outer border of the UFOV, depending on the optical angle of presentation (see :numref:`image_ReliabilityBasedOnPerception_`).
This value is then set for every object information, except constant parameters (e.g. length and height), because properties which do not change over time are perfectly reliable as long as the object exists or is not forgotten by the mental model.

.. TODO: swap x- and y-axis (reliability is depending on the optical angle)

.. _image_ReliabilityBasedOnPerception_:

.. figure:: ../_static/images/ReliabilityBasedOnPerception.png
   :alt: Reliability of mental information - calculation based on visual perception

   Reliability of mental information - calculation based on visual perception

If the information of a surrounding object is no longer (cognitively) perceivable by means of visual perception, but the object has already been perceived and implemented into the mental model, the information has to be :ref:`extrapolated<extrapolation_>` by mental processes.
The longer this mental extrapolation lasts without updating the information by ground-truth visual perception, the more the reliability of this data decreases.
The starting value for this degradation process is the level of reliability based on the quality of the last visual perception (see above).
Therefore, if the object was lastly perceived in the UFOV, the reliability of the information is already lower at the start of the extrapolation as if it had been perceived in the fovea.
Therefore, the data will become outdated earlier (see :numref:`image_ReliabilityBasedOnExtrapolation_`).
The degradation happens linearly over time and all information is completely outdated after a maximum of 7 s, if the object is not visually perceived again.
The degradation rate over time varies for different types of information, because some object parameters change faster than others.
The following table gives an overview on those types of object parameters and their assigned *change rates* for the degradation process (currently not fully implemented, because all change rates are the same).

.. TODO: check table

.. list-table::
   :header-rows: 1

   * - Parameter change type
     - *Change rate* [1/s]
     - Examples
   * - CONSTANT
     - 0
     - Physical dimensions, agent type
   * - SLOW
     - -100/7 (about 15)
     - Distances
   * - MEDIUM
     - -100/7 (about 15)
     - Velocities
   * - FAST
     - -100/7 (about 15)
     - Accelerations

.. TODO: again, would swap x- and y-axis, because time is the independent variable

.. _image_ReliabilityBasedOnExtrapolation_:

.. figure:: ../_static/images/ReliabilityBasedOnExtrapolation.png
   :alt: Reliability of mental information - calculation based on mental extrapolation

   Reliability of mental information - calculation based on mental extrapolation

The calculation of the reliabilities is done simultaneously by both described mechanisms, as long as the object is visually perceivable.
The resulting higher reliability values are written into the mental model.
Therefore, also a mental extrapolation, which results in more reliable information than a vaguely perceivable observation of the object, is taken into account.
This prevents sudden drops in the reliabilities by gaze movements, which would not represent real human cognition.
Furthermore, every object, which was not visually perceived for 7 s, is “forgotten” by the mechanism and will be deleted from the mental model.
