..
  *******************************************************************************
  Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _prepare_scm_build_:


Prerequisites
=============

Download the project::

    git clone https://gitlab.eclipse.org/eclipse/scm/scm.git

Most needed dependencies are handled with ``Conan``.
To install Conan, follow the official guide https://docs.conan.io/2/installation.html.

.. _building_under_windows:

      cd scm
      git checkout main

Installing the Build Environment
--------------------------------

.. tabs::

   .. tab:: Windows

      #. MSYS2

         On Windows, the build environment of choice are MSYS2 programming tools.
         MSYS2 is used to install some third-party software on which |op| depends.
         Also, the unix-like shell simplifies c++ compilation on Windows.
         For details, see `MSYS2 website <https://www.msys2.org/>`_.

        .. _msys2:

      #. Download MSYS2

         The latest 64-bit package is located in the Installation section of `MSYS2 website <https://www.msys2.org/>`_.


         .. _msys2_installation:

      #. Install MSYS2

         Run the downloaded executable and adjust suggested settings to your needs (defaults are fine).
         In the following, it is assumed that MSYS2 is installed under ``C:\msys64``.

.. _binary_packages:


Installing the Binary Packages
------------------------------

The first set of dependencies we need to install in order to successfully compile SCM are the binary packages. These can be installed via appropiate package manager.
Additionally, download ``boost`` and all packages needed for your operating system to build C++ programs.

.. tabs::

   .. tab:: Windows (MSYS2)

      #. Open ``MSYS2 MinGW 64-bit`` and execute the following package manager ``pacman`` commands to update the package repository and upgrade system packages:

         .. code-block::

            pacman -Syuu

         If the upgrade requires a restart of MSYS2, resume the upgrade by re-opening the shell and call:

         .. code-block::

            pacman -Suu

      #. Required packages:

        .. code-block::

            pacman -S mingw-w64-x86_64-boost
            pacman -S mingw-w64-x86_64-gcc
            pacman -S mingw-w64-x86_64-toolchain
            pacman -S base-devel
            pacman -S mingw-w64-x86_64-ccache
            pacman -S mingw-w64-x86_64-cmake
            pacman -S mingw-w64-x86_64-doxygen
            pacman -S mingw-w64-x86_64-gdb
            pacman -S mingw-w64-x86_64-graphviz
            pacman -S mingw-w64-x86_64-gtest
            pacman -S mingw-w64-x86_64-make

   .. tab:: Linux

      #. Install required binary packages

        .. code-block::

            sudo apt install libboost-all-dev
            sudo apt install build-essential
            sudo apt install ccache
            sudo apt install cmake

.. _building_prerequisites:


Installing the Dependencies

If you don't already have a conan profile, you can let Conan detect and create a suitable default profile for your current environment with::

    conan profile detect

or use one of the provided profiles in `utils/conan`.

The easiest way to get started is to call a script::

    cd utils/ci/scripts && ./15_prepare-thirdParty.sh

This script checks out needed third party dependencies and if they're not already present in local cache or available in the conancenter it builds them one after the other.

#. To build and install the dependencies, run the script ``15_prepare_thirdParty.sh`` located in ``utils/ci/scripts``.

   .. tabs::

    .. tab:: Windows
         
         #. The script ``15_prepare_thirdParty.sh`` also requires the variable PYTHON_WINDOWS_EXE set. This variable refers to the path of the python installation (For ex: It could be either on native Windows or on MSYS).
            If the python is installed on MSYS then set the variable with the following command. 

            .. code-block:: 

               export PYTHON_WINDOWS_EXE=python3

         #. Execute the script ``15_prepare_thirdParty.sh`` on MSYS

            .. code-block:: 

               cd utils/ci/scripts
               ./15_prepare_thirdParty.sh

    .. tab:: Linux

         #. Execute the script

            .. code-block:: 

               cd utils/ci/scripts
               ./15_prepare_thirdParty.sh

#. Upon successful execution of the script, the dependencies will be installed under ``C:\deps`` on Windows and  ``~/deps`` on Linux.
