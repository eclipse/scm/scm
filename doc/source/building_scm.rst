..
  *******************************************************************************
  Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _building_scm_:


Building SCM
============

SCM is a simple CMake Project and requires a minimum version of 3.14.

From within your root directory create a build directory and change into it::

    mkdir build && cd build

Call cmake and provide the dependencies via the ``CMAKE_PREFIX_PATH``

.. tabs::


   .. tab:: Linux

      .. code-block::

         cmake ..
         -G "Unix Makefiles"
         -DUSE_CCACHE=ON
         -DCMAKE_INSTALL_PREFIX="/home/$USER/scm-install"
         -DCMAKE_PREFIX_PATH="deps/conan/direct_deploy/open-simulation-interface; \
                              deps/conan/direct_deploy/protobuf; \
                              deps/conan/direct_deploy/gtest; \
                              deps/conan/direct_deploy/stochastics; \
                              deps/conan/direct_deploy/units; \
                              deps/conan/direct_deploy/libiconv; \
                              deps/conan/direct_deploy/libxml2; \
                              deps/conan/direct_deploy/osiquerylibrary"

   .. tab:: Windows

      Make sure you have a MSYS installation with at least gtest and boost installed.

      .. code-block::

         cmake ..
         -G "MSYS Makefiles"
         -DUSE_CCACHE=ON
         -DCMAKE_INSTALL_PREFIX="C:/scm-install"
         -DCMAKE_PREFIX_PATH="deps/conan/direct_deploy/open-simulation-interface; \
                              deps/conan/direct_deploy/protobuf; \
                              deps/conan/direct_deploy/gtest; \
                              deps/conan/direct_deploy/stochastics; \
                              deps/conan/direct_deploy/units; \
                              deps/conan/direct_deploy/libiconv; \
                              deps/conan/direct_deploy/libxml2; \
                              deps/conan/direct_deploy/osiquerylibrary"


or as a one-liner for easy copy & paste

.. tabs::

   .. tab:: Linux

      .. code-block::

         cmake .. -G "Unix Makefiles" -DUSE_CCACHE=ON -DCMAKE_INSTALL_PREFIX="/home/$USER/scm-install" -DCMAKE_PREFIX_PATH="deps/conan/direct_deploy/open-simulation-interface;deps/conan/direct_deploy/protobuf;deps/conan/direct_deploy/gtest;deps/conan/direct_deploy/stochastics;deps/conan/direct_deploy/units;deps/conan/direct_deploy/libiconv;deps/conan/direct_deploy/libxml2;deps/conan/direct_deploy/osiquerylibrary"

   .. tab:: Windows

      .. code-block::

         cmake .. -G "MSYS Makefiles" -DUSE_CCACHE=ON -DCMAKE_INSTALL_PREFIX="C:/scm-install" -DCMAKE_PREFIX_PATH="deps/conan/direct_deploy/open-simulation-interface;deps/conan/direct_deploy/protobuf;deps/conan/direct_deploy/gtest;deps/conan/direct_deploy/stochastics;deps/conan/direct_deploy/units;deps/conan/direct_deploy/libiconv;deps/conan/direct_deploy/libxml2;deps/conan/direct_deploy/osiquerylibrary"

.. note::

    If you don't have sphinx installed or simply don't want to build the documentation, add the following options to your cmake call::

    -DWITH_DOC=OFF -DWITH_API_DOC=OFF

    Otherwise you may need to install some additional modules::

        pip3 install sphinx sphinx-rtd-theme sphinx-tabs breathe exhale sphinxcontrib-spelling myst-parser

    Contrary, if you just want to build the documentation and not the whole project, call cmake with the following arguments::

        -DWITH_DOC=ON -DONLY_DOC=ON

Then simply call::

    make install -j4

to build SCM and install it to the directory provided by the given ``CMAKE_INSTALL_PREFIX``. Adjust this installation directory to your needs.

The build generates the main output file **AlgorithmScm.fmu**.