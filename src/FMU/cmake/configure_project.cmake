################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

set(BUILD_DOC                ON  CACHE BOOL "Build the project's documentation?")
set(BUILD_EXAMPLE_EXECUTABLE ON  CACHE BOOL "Build the example executable?")
set(BUILD_TESTS              ON  CACHE BOOL "Build the project's unit tests?")

