/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Logging.h"

#include <iostream>

void Logging::Log(LogLevel level, const std::string& message)
{
  // wrapper for spdlog / FMI log
  std::cout << static_cast<int>(level) << ": " << message << "\n";
  //  ScmLogLevelToFmiLogLevelTo();
  switch (level)
  {
    case LogLevel::trace:
      break;
    case LogLevel::debug:
      break;
    case LogLevel::info:
      break;
    case LogLevel::warning:
      break;
    case LogLevel::error:
      break;
    case LogLevel::critical:
      break;
  }
}
void Logging::Trace(const std::string& message)
{
  Log(LogLevel::trace, message);
}
void Logging::Debug(const std::string& message)
{
  Log(LogLevel::debug, message);
}
void Logging::Info(const std::string& message)
{
  Log(LogLevel::info, message);
}
void Logging::Warning(const std::string& message)
{
  Log(LogLevel::warning, message);
}
void Logging::Error(const std::string& message)
{
  Log(LogLevel::error, message);
}
void Logging::Critical(const std::string& message)
{
  Log(LogLevel::critical, message);
}
