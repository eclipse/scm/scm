/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <units.h>

#include "ScmObserver/ScmObserver.h"
#include "include/Logging/DataBufferInterface.h"
#include "include/Logging/PublisherInterface.h"
#include "include/common/ScmRuntimeInformation.h"

namespace scm
{

class SimulationLogging
{
public:
  SimulationLogging(int agentId, const scm::RuntimeInformation& runtimeInformation);
  virtual ~SimulationLogging() = default;

  scm::publisher::PublisherInterface* GetPublisher() const;
  void SimulationPreRun();
  void SimulationPostRun();
  void SimulationUpdate(units::time::millisecond_t time);
  void ClearTimeStep();

private:
  std::unique_ptr<scm::DataBufferInterface> _dataBuffer{nullptr};
  std::unique_ptr<scm::publisher::PublisherInterface> _publisher{nullptr};
  std::unique_ptr<ScmObserver> _scmObserver{nullptr};
};

}  // namespace scm