/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SimulationLogging.h"

#include "src/Logging/SimulationLogging/DataBuffer/BasicDataBuffer.h"
#include "src/Logging/SimulationLogging/DataPublisher/DataPublisher.h"

namespace scm
{

SimulationLogging::SimulationLogging(int agentId, const scm::RuntimeInformation& runtimeInformation)
{
  _dataBuffer = std::make_unique<BasicDataBuffer>(&runtimeInformation);
  _publisher = std::make_unique<scm::publisher::DataPublisher>(_dataBuffer.get(), agentId);
  _scmObserver = std::make_unique<ScmObserver>(_dataBuffer.get(), runtimeInformation);
}

void SimulationLogging::SimulationPreRun()
{
  _scmObserver->SimulationPreHook();
}

void SimulationLogging::SimulationPostRun()
{
  _scmObserver->SimulationPostRunHook();
}

void SimulationLogging::SimulationUpdate(units::time::millisecond_t time)
{
  _scmObserver->SimulationUpdateHook(time.value());
}

void SimulationLogging::ClearTimeStep()
{
  _dataBuffer->ClearTimeStep();
};

scm::publisher::PublisherInterface* SimulationLogging::GetPublisher() const
{
  return _publisher.get();
}

}  // namespace scm