/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <regex>
#include <set>
#include <string>
#include <tuple>
#include <unordered_map>

#include "../../include/Logging/DataBufferInterface.h"
#include "ScmFileHandler.h"
#include "include/common/ScmRuntimeInformation.h"

namespace scm
{

/** \brief This class creates extended Scm simulation output.
//-----------------------------------------------------------------------------
*   \details This class handles an additional simulation output file, namely extendedScmSimulationOutput.
*            Here, additional ScmParameters can be logged to better understand the SCM behaviour.
*
*   \ingroup Observation_Scm
*/
//-----------------------------------------------------------------------------
class ScmObserver
{
public:
  const std::string COMPONENTNAME = "ScmObserver";

  ScmObserver(scm::DataBufferReadInterface *dataBuffer, const scm::RuntimeInformation &runtimeInformation);
  ScmObserver(const ScmObserver &) = delete;
  ScmObserver(ScmObserver &&) = delete;
  ScmObserver &operator=(const ScmObserver &) = delete;
  ScmObserver &operator=(ScmObserver &&) = delete;
  virtual ~ScmObserver() = default;

  void SimulationPreHook();
  void SimulationPostRunHook();
  void SimulationUpdateHook(int time);

private:
  /**
   * @brief Converts any static databuffer value into its string representation
   *
   * @param values value retrieved from the databuffer
   * @return converted string
   */
  const std::string to_string(const Values &values);

  void InsertPerception(const scm::databuffer::ScmCyclicRow& cyclic, const std::string& valueStr, int time);

  scm::DataBufferReadInterface *dataBuffer;

  int runNumber = 0;
  Cyclics cyclics;
  std::map<std::string, Cyclics> perceptionPerAgent;
  std::map<std::string, std::string> driverParameters;
  ScmFileHandler fileHandler;

  const scm::LoggerOptions& loggerOptions;
  std::string outputPath;
  std::string frameWorkVersion;
  bool loggingEnabled();
};
}