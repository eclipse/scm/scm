/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "DataBufferBinding.h"
#include "DataBufferLibrary.h"

namespace scm
{

DataBufferBinding::DataBufferBinding(std::string libraryPath,
                                     const scm::RuntimeInformation& runtimeInformation)
    : _libraryPath{libraryPath},
      _runtimeInformation{runtimeInformation}
{
}

DataBufferBinding::~DataBufferBinding()
{
  Unload();
}

DataBufferInterface* DataBufferBinding::Instantiate()
{
  if (!_library)
  {
    _library = new (std::nothrow) DataBufferLibrary(_libraryPath);

    if (!_library)
    {
      return nullptr;
    }

    if (!_library->Init())
    {
      delete _library;
      _library = nullptr;
      return nullptr;
    }
  }

  return _library->CreateDataBuffer(_runtimeInformation);
}

void DataBufferBinding::Unload()
{
  if (_library)
  {
    _library->ReleaseDataBuffer();
    delete _library;
    _library = nullptr;
  }
}

}  // namespace scm