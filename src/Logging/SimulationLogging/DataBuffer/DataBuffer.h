/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  DataBuffer.h
//! @brief This file provides the interface to a data buffer implementation
//-----------------------------------------------------------------------------

#pragma once

#include "include/Logging/DataBufferInterface.h"
#include "DataBufferBinding.h"

namespace scm
{

class DataBuffer : public DataBufferInterface
{
public:
  DataBuffer(DataBufferBinding *dataBufferBinding)
      : _dataBufferBinding(dataBufferBinding)
  {
  }

  DataBuffer(const DataBuffer &) = delete;
  DataBuffer(DataBuffer &&) = delete;
  DataBuffer &operator=(const DataBuffer &) = delete;
    DataBuffer &operator=(DataBuffer &&) = delete;
    ~DataBuffer() override = default;

    bool Instantiate() override;
    bool isInstantiated() const override;

    void PutCyclic(const scm::type::EntityId entityId, const Key &key, const Value &value) override;
    void PutAcyclic(const scm::type::EntityId entityId, const Key &key, const ScmAcyclic &acyclic) override;
    void PutStatic(const Key &key, const Value &value, bool persist) override;

    void ClearRun() override;
    void ClearTimeStep() override;

    std::unique_ptr<scm::databuffer::ScmCyclicResultInterface> GetCyclic(const std::optional<scm::type::EntityId> entityId, const Key &key) const override;
    std::unique_ptr<scm::databuffer::ScmAcyclicResultInterface> GetAcyclic(const std::optional<scm::type::EntityId> entityId, const Key &key) const override;
    Values GetStatic(const Key &key) const override;
    Keys GetKeys(const Key &key) const override;

  private:
    DataBufferBinding *_dataBufferBinding = nullptr;
    DataBufferInterface *_implementation = nullptr;
};

}  // namespace scm