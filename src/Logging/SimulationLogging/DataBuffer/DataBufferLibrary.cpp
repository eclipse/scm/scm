/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "DataBufferLibrary.h"

#include <iostream>

#include "include/common/ScmRuntimeInformation.h"
#include "src/Logging/Logging.h"

bool DataBufferLibrary::Init()
{
  library = boost::dll::shared_library(
      dataBufferLibraryPath, boost::dll::load_mode::append_decorations | boost::dll::load_mode::search_system_folders);

  if (!library.is_loaded())
  {
    Logging::Error("Failed to load library: " + dataBufferLibraryPath);
    return false;
  }

  getVersionFunc = library.get<DataBufferInterface_GetVersion>(DllGetVersionId);

  if (!getVersionFunc)
  {
    Logging::Error("Unable to resolve the symbol " + DllGetVersionId + " within the DLL located at " + dataBufferLibraryPath);
    return false;
  }

  createInstanceFunc = library.get<DataBufferInterface_CreateInstanceType>(DllCreateInstanceId);
  if (!createInstanceFunc)
  {
    Logging::Error("Unable to resolve the symbol " + DllCreateInstanceId + " within the DLL located at " + dataBufferLibraryPath);
    return false;
  }

  destroyInstanceFunc = library.get<DataBufferInterface_DestroyInstanceType>(DllDestroyInstanceId);
  if (!destroyInstanceFunc)
  {
    Logging::Warning("Unable to resolve the symbol " + DllDestroyInstanceId + " within the DLL located at " + dataBufferLibraryPath);
    return false;
  }

  try
  {
    Logging::Error("Loaded dataBuffer library " + library.location().filename().string() + ", version " + getVersionFunc());
  }
  catch (std::runtime_error const &ex)
  {
    Logging::Error("Unable to retrieve version information from the DLL: " + (std::string)ex.what());
    return false;
  }
  catch (...)
  {
    Logging::Error("Unable to retrieve version information from the DLL");
    return false;
  }

  return true;
}

DataBufferLibrary::~DataBufferLibrary()
{
  if (dataBufferInterface)
  {
    Logging::Warning("unloading library which is still in use");
  }

  if (library.is_loaded())
  {
    Logging::Debug("unloading dataBuffer library ");
    library.unload();
  }
}

bool DataBufferLibrary::ReleaseDataBuffer()
{
    if (!dataBufferInterface)
    {
        return true;
    }

    if (!library)
    {
        return false;
    }

    try
    {
        destroyInstanceFunc(dataBufferInterface);
    }
    catch (std::runtime_error const &ex)
    {
        Logging::Error("dataBuffer could not be released: ");
        return false;
    }
    catch (...)
    {
        Logging::Error("dataBuffer could not be released");
        return false;
    }

    dataBufferInterface = nullptr;

    return true;
}

scm::DataBufferInterface* DataBufferLibrary::CreateDataBuffer(const scm::RuntimeInformation& runtimeInformation)
{
    if (!library.is_loaded())
    {
        return nullptr;
    }

    dataBufferInterface = nullptr;

    try
    {
        dataBufferInterface = createInstanceFunc(&runtimeInformation);
    }
    catch (std::runtime_error const &ex)
    {
        Logging::Error("could not create stochastics instance: " + (std::string)ex.what());
        return nullptr;
    }
  catch (...)
  {
    Logging::Error("could not create stochastics instance");
    return nullptr;
  }

  return dataBufferInterface;
}

