/********************************************************************************
 * Copyright (c) 2016-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2016-2017 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  LateralController.cpp */
//-----------------------------------------------------------------------------

#include "LateralController.h"

namespace scm
{

scm::signal::LateralControllerOutput LateralController::Trigger(const scm::signal::LateralControllerInput& input, units::time::millisecond_t time)
{
    steeringController.SetLateralInput(input);
    return signal::LateralControllerOutput{steeringController.CalculateSteeringAngle(units::time::millisecond_t(time))};
}

}  // namespace scm