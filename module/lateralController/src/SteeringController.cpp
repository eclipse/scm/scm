/********************************************************************************
 * Copyright (c) 2016-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2016-2017 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  SteeringController.cpp */
//-----------------------------------------------------------------------------

#include "SteeringController.h"

#include <algorithm>
#include <cmath>

namespace scm
{

units::angle::radian_t SteeringController::CalculateSteeringAngle(units::time::millisecond_t time)
{
  // Time step length
  auto dt = (time - timeLast);
  tAverage = .05_s;
  auto velocityForCalculations = units::math::fmax(20.0_kph, in_velocity);
  // Scale gains to current velocity. Linear interpolation between 0 and default values at 200km/h.
  double velocityFactor = std::clamp(units::unit_cast<double>(velocityForCalculations / 150.0_kph), .15, 1.);
  gainLateralDeviation *= velocityFactor;
  gainHeadingError *= velocityFactor;
  tAverage = tAverage / velocityFactor;

  // Controller for lateral deviation
  units::angle::radian_t deltaHLateralDeviation = gainLateralDeviation * in_steeringRatio * in_wheelBase / (velocityForCalculations * velocityForCalculations) * lateralDeviation;

  // Controller for heading angle error
  units::angle::radian_t deltaHHeadingError = gainHeadingError * in_steeringRatio * in_wheelBase / velocityForCalculations * headingError;

  // Controller for road curvature
  units::curvature::inverse_meter_t meanCurvatureToNearPoint = 0.0_i_m;
  units::curvature::inverse_meter_t meanCurvatureToFarPoint = 0.0_i_m;
  if (!curvatureOfSegmentsToNearPoint.empty())
  {
    for (unsigned int i = 0; i < curvatureOfSegmentsToNearPoint.size(); ++i)
    {
      meanCurvatureToNearPoint += curvatureOfSegmentsToNearPoint.at(i);
    }

    meanCurvatureToNearPoint = meanCurvatureToNearPoint / curvatureOfSegmentsToNearPoint.size();
  }

  if (!curvatureOfSegmentsToFarPoint.empty())
  {
    for (unsigned int i = 0; i < curvatureOfSegmentsToFarPoint.size(); ++i)
    {
      meanCurvatureToFarPoint += curvatureOfSegmentsToFarPoint.at(i);
    }
    
    meanCurvatureToFarPoint = meanCurvatureToFarPoint / curvatureOfSegmentsToFarPoint.size();
  }

    // Smooth curvatures with a running average filter
    units::curvature::inverse_meter_t meanCurvatureToNearPointSmooth = (dt * meanCurvatureToNearPoint + (tAverage - dt) *
                                                                                                            meanCurvatureToNearPointSmoothLast) /
                                                                       tAverage;
    units::curvature::inverse_meter_t meanCurvatureToFarPointSmooth = (dt * meanCurvatureToFarPoint + (tAverage - dt) *
                                                                                                          meanCurvatureToFarPointSmoothLast) /
                                                                      tAverage;
    units::curvature::inverse_meter_t curvatureRoadSmooth = (dt * kappaRoad + (tAverage - dt) * curvatureRoadSmoothLast) / tAverage;

  // Weighting of different curvature Information RoadSmooth, road, nearPointSmooth, farPointSmooth, nearPointMax
  std::vector<double> weighingCurvaturePortions = {.75, 0.25, .15, -.10};
  if (!curvatureOfSegmentsToNearPoint.empty())
  {
    weighingCurvaturePortions.at(2) = 0.;
  }

    units::curvature::inverse_meter_t calc_kappaRoadAnticipated = (weighingCurvaturePortions.at(0) * curvatureRoadSmooth +
                                                                   weighingCurvaturePortions.at(1) * kappaRoad +
                                                                   weighingCurvaturePortions.at(2) * meanCurvatureToNearPointSmooth +
                                                                   weighingCurvaturePortions.at(3) * meanCurvatureToFarPointSmooth) /
                                                                  (weighingCurvaturePortions.at(0) + weighingCurvaturePortions.at(1) +
                                                                   weighingCurvaturePortions.at(2) + weighingCurvaturePortions.at(3));

    // Controller for road curvaturedelta due to manoeuvre
    units::angle::radian_t deltaHkappa = units::math::atan((kappaManoeuvre + calc_kappaRoadAnticipated) * in_wheelBase) * in_steeringRatio;

    // Total steering wheel angle
    auto deltaH = deltaHLateralDeviation + deltaHHeadingError + deltaHkappa;

    // Limit steering wheel velocity. Human limit set to 320°/s.
    const units::angular_velocity::radians_per_second_t HUMAN_LIMIT{320.0 * M_PI / 180.0};
    const units::angle::radian_t maxDeltaSteeringWheelAngle = (HUMAN_LIMIT / velocityFactor) * dt;
    const auto deltaSteeringWheelAngle = deltaH - in_steeringWheelAngle;
    if (units::math::fabs(deltaSteeringWheelAngle) > maxDeltaSteeringWheelAngle)
    {
        deltaH = units::angle::radian_t(std::copysign(maxDeltaSteeringWheelAngle.value(), deltaSteeringWheelAngle.value())) + in_steeringWheelAngle;
    }

  const auto desiredSteeringWheelAngle = std::clamp(deltaH, -in_steeringMax, in_steeringMax);

  timeLast = time;
  meanCurvatureToNearPointSmoothLast = meanCurvatureToNearPointSmooth;
  meanCurvatureToFarPointSmoothLast = meanCurvatureToFarPointSmooth;
  curvatureRoadSmoothLast = curvatureRoadSmooth;

  return desiredSteeringWheelAngle;
}

}  // namespace scm