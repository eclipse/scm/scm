/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/*
 * NOTE this file is generated.
 * Be careful with manual changes as they may be overridden in future without noticing.
 */
#include "TrafficRules.h"

#include <cmath>

#include "include/common/ScmDefinitions.h"

using namespace units::literals;
TrafficRulesScm GenerateTrafficRules::Generate(const std::string& country)
{
  TrafficRulesScm trafficRules;
  if (country == "DE")
  {
    trafficRules.common.handsFreePhoneMandatory.applicable = true;
    trafficRules.common.handsFreePhoneMandatory.value = true;
    trafficRules.common.rightHandTraffic.applicable = true;
    trafficRules.common.rightHandTraffic.value = true;
    trafficRules.urban.openSpeedLimit.applicable = true;
    trafficRules.urban.openSpeedLimit.value = 50_kph;
    trafficRules.rural.openSpeedLimit.applicable = true;
    trafficRules.rural.openSpeedLimit.value = 100_kph;
    trafficRules.motorway.openSpeedLimit.applicable = true;
    trafficRules.motorway.openSpeedLimit.value = ScmDefinitions::INF_VELOCITY;
    trafficRules.motorway.keepToOuterLanes.applicable = true;
    trafficRules.motorway.keepToOuterLanes.value = true;
    trafficRules.motorway.DontOvertakeOnOuterLanes.applicable = true;
    trafficRules.motorway.DontOvertakeOnOuterLanes.value = true;
    trafficRules.motorway.formRescueLane.applicable = true;
    trafficRules.motorway.formRescueLane.value = true;
    trafficRules.motorway.ZipperMerge.applicable = true;
    trafficRules.motorway.ZipperMerge.value = true;
  }
  if (country == "US")
  {
    trafficRules.common.handsFreePhoneMandatory.applicable = true;
    trafficRules.common.handsFreePhoneMandatory.value = false;
    trafficRules.common.rightHandTraffic.applicable = true;
    trafficRules.common.rightHandTraffic.value = true;
    trafficRules.urban.openSpeedLimit.applicable = true;
    trafficRules.urban.openSpeedLimit.value = 40_kph;
    trafficRules.rural.openSpeedLimit.applicable = true;
    trafficRules.rural.openSpeedLimit.value = 100_kph;
    trafficRules.motorway.openSpeedLimit.applicable = true;
    trafficRules.motorway.openSpeedLimit.value = 120_kph;
    trafficRules.motorway.DontOvertakeOnOuterLanes.applicable = true;
    trafficRules.motorway.DontOvertakeOnOuterLanes.value = false;
    trafficRules.motorway.formRescueLane.applicable = true;
    trafficRules.motorway.formRescueLane.value = true;
    trafficRules.motorway.ZipperMerge.applicable = true;
    trafficRules.motorway.ZipperMerge.value = false;
  }
  if (country == "AT")
  {
    trafficRules.common.handsFreePhoneMandatory.applicable = true;
    trafficRules.common.handsFreePhoneMandatory.value = true;
    trafficRules.common.rightHandTraffic.applicable = true;
    trafficRules.common.rightHandTraffic.value = true;
    trafficRules.urban.openSpeedLimit.applicable = true;
    trafficRules.urban.openSpeedLimit.value = 50_kph;
    trafficRules.rural.openSpeedLimit.applicable = true;
    trafficRules.rural.openSpeedLimit.value = 100_kph;
    trafficRules.motorway.openSpeedLimit.applicable = true;
    trafficRules.motorway.openSpeedLimit.value = 130_kph;
    trafficRules.motorway.keepToOuterLanes.applicable = true;
    trafficRules.motorway.keepToOuterLanes.value = true;
    trafficRules.motorway.DontOvertakeOnOuterLanes.applicable = true;
    trafficRules.motorway.DontOvertakeOnOuterLanes.value = true;
    trafficRules.motorway.formRescueLane.applicable = true;
    trafficRules.motorway.formRescueLane.value = true;
    trafficRules.motorway.ZipperMerge.applicable = true;
    trafficRules.motorway.ZipperMerge.value = true;
  }
  if (country == "CN")
  {
    trafficRules.common.handsFreePhoneMandatory.applicable = true;
    trafficRules.common.handsFreePhoneMandatory.value = false;
    trafficRules.common.rightHandTraffic.applicable = true;
    trafficRules.common.rightHandTraffic.value = true;
    trafficRules.urban.openSpeedLimit.applicable = true;
    trafficRules.urban.openSpeedLimit.value = 50_kph;
    trafficRules.rural.openSpeedLimit.applicable = true;
    trafficRules.rural.openSpeedLimit.value = 100_kph;
    trafficRules.motorway.openSpeedLimit.applicable = true;
    trafficRules.motorway.openSpeedLimit.value = 130_kph;
    trafficRules.motorway.DontOvertakeOnOuterLanes.applicable = true;
    trafficRules.motorway.DontOvertakeOnOuterLanes.value = true;
    trafficRules.motorway.formRescueLane.applicable = true;
    trafficRules.motorway.formRescueLane.value = false;
    trafficRules.motorway.ZipperMerge.applicable = true;
    trafficRules.motorway.ZipperMerge.value = true;
  }

  return trafficRules;
}
