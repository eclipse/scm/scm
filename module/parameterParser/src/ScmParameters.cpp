/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  Parameters.cpp */
//-----------------------------------------------------------------------------

#include "ScmParameters.h"

#include <memory>

#include "DriverCharacteristicsSampler.h"
#include "DriverParameterModifier.h"
#include "include/ScmSampler.h"
#include "include/common/ParameterInterface.h"
#include "include/common/StochasticDefinitions.h"
#include "module/parameterParser/src/DriverBehavior/DriverBehavior.h"
#include "src/Logging/Logging.h"

namespace scm
{
ScmParameters::ScmParameters(const scm::parameter::ParameterMapping* parameterMapping,
                             scm::publisher::PublisherInterface* const scmPublisher,
                             StochasticsInterface* stochastics,
                             common::VehicleClass vehicleClass,
                             units::length::meter_t visibilityDistance)
    : _parameterMapping{parameterMapping},
      _scmPublisher{scmPublisher},
      _stochastics{stochastics},
      _vehicleClass{vehicleClass},
      _visibilityDistance{visibilityDistance}
{
  _initialisation = true;
}

void ScmParameters::Initialize()
{
  _initialisation = false;
  // read parameters
  try
  {
    driverCharacteristicsSampler = std::make_unique<DriverCharacteristicsSampler>(_stochastics);

    PrepareDriverParameter();
    EgoAgentAdapterToWorldTrafficRules();
  }
  catch (const std::out_of_range& e)
  {
    throw std::runtime_error(e.what());
  }
  catch (...)
  {
    const std::string msg = COMPONENTNAME + " could not init parameters";
    Logging::Error(msg);
    throw std::runtime_error(msg);
  }
}
scm::signal::ParameterParserOutput ScmParameters::Trigger(units::velocity::meters_per_second_t egoVelocity)
{
  if (_initialisation)
  {
    Initialize();
  }
  out_driverParameters.previewDistance = DriverParameterModifier::CalculatePreviewDistance(_visibilityDistance, egoVelocity, previewTimeHeadway, previewDistanceMinimum);
  DriverParameterModifier::ComfortAcceleration(egoVelocity, _comfortAccelerationParameter, out_driverParameters);

  if (IsDriverCharacteristicsSamplerRequired())
  {
    ApplyDriverCharacteristicsSampler();
  }
  return (scm::signal::ParameterParserOutput&)out_driverParameters;
}

double ScmParameters::CalculateStochastic(scm::parameter::StochasticDistribution distribution)
{
  return ScmSampler::RollForStochasticAttribute(distribution, _stochastics);
}

void ScmParameters::PrepareDriverParameter()
{
  GenerateDriverBehavior driverBehavior(*driverCharacteristicsSampler.get(), *_parameterMapping, _stochastics);

  const auto behavior = driverBehavior.InitBehavior();
  //  driverBehavior.CheckForMandatoryDistributionTypes(); // currently these behavior parameter are not distributions anymore!

  out_driverParameters.idealPerception = behavior.IdealPerception;
  out_driverParameters.highCognitiveMode = behavior.EnableHighCognitiveMode;
  previewDistanceMinimum = behavior.PreviewDistanceMin;
  out_driverParameters.anticipationQuota = behavior.AnticipationQuota;
  out_driverParameters.proportionalityFactorForFollowingDistance = behavior.ProportionalityFactorForFollowingDistance;
  out_driverParameters.rightOvertakingProhibitionIgnoringQuota = behavior.RightOvertakingProhibitionIgnoringQuota;
  out_driverParameters.laneChangeProhibitionIgnoringQuota = behavior.LaneChangeProhibitionIgnoringQuota;

  _comfortAccelerationParameter.modifications.maxComfortAccelerationFactor = behavior.MaxComfortAccelerationFactor;
  _comfortAccelerationParameter.modifications.maxComfortDecelerationFactor = behavior.MaxComfortDecelerationFactor;

  // perception related parameters
  out_driverParameters.thresholdLoomingFovea = behavior.ThresholdLooming;
  out_driverParameters.thresholdRetinalProjectionAngularSpeedFovea = thresholdRetinalProjectionAngularSpeedFovea;
  previewTimeHeadway = behavior.PreviewTimeHeadway;

  const std::string REACTION_BASE_TIME = "ReactionBaseTime";
  out_driverParameters.reactionBaseTimeMinimum = units::make_unit<units::time::second_t>(std::get<scm::parameter::LogNormalDistribution>(std::get<scm::parameter::StochasticDistribution>(driverBehavior.parameters.at(REACTION_BASE_TIME))).GetMin());
  out_driverParameters.reactionBaseTimeMaximum = units::make_unit<units::time::second_t>(std::get<scm::parameter::LogNormalDistribution>(std::get<scm::parameter::StochasticDistribution>(driverBehavior.parameters.at(REACTION_BASE_TIME))).GetMax());
  out_driverParameters.reactionBaseTimeMean = units::make_unit<units::time::second_t>(std::get<scm::parameter::LogNormalDistribution>(std::get<scm::parameter::StochasticDistribution>(driverBehavior.parameters.at(REACTION_BASE_TIME))).GetMean());
  out_driverParameters.reactionBaseTimeStandardDeviation = units::make_unit<units::time::second_t>(std::get<scm::parameter::LogNormalDistribution>(std::get<scm::parameter::StochasticDistribution>(driverBehavior.parameters.at(REACTION_BASE_TIME))).GetStandardDeviation());

  out_driverParameters.adjustmentBaseTimeMinimum = adjustmentBaseTimeMinimum;
  out_driverParameters.adjustmentBaseTimeMaximum = adjustmentBaseTimeMaximum;
  out_driverParameters.adjustmentBaseTimeMean = adjustmentBaseTimeMean;
  out_driverParameters.adjustmentBaseTimeStandardDeviation = adjustmentBaseTimeStandardDeviation;

  out_driverParameters.pedalChangeTimeMinimum = pedalChangeTimeMinimum;
  out_driverParameters.pedalChangeTimeMean = static_cast<units::time::second_t>(pedalChangeTimeMean);
  out_driverParameters.pedalChangeTimeStandardDeviation = pedalChangeTimeStandardDeviation;

  // simplified cognition related parameters
  out_driverParameters.agentCooperationFactor = behavior.AgentCooperationFactor;
  out_driverParameters.agentSuspiciousBehaviourEvasionFactor = behavior.AgentSuspiciousBehaviourEvasionFactor;
  out_driverParameters.outerKeepingIntensity = behavior.OuterKeepingIntensity;
  out_driverParameters.egoLaneKeepingIntensity = behavior.EgoLaneKeepingIntensity;
  out_driverParameters.egoLaneKeepingQuota = behavior.EgoLaneKeepingQuota;
  out_driverParameters.outerKeepingQuota = behavior.OuterKeepingQuota;

  out_driverParameters.useShoulderInTrafficJamQuota = behavior.UseShoulderInTrafficJamQuota;
  out_driverParameters.useShoulderInTrafficJamToExitQuota = behavior.UseShoulderInTrafficJamToExitQuota;
  out_driverParameters.useShoulderWhenPeerPressuredQuota = behavior.UseShoulderWhenPeerPressuredQuota;

  out_driverParameters.distractionPercentage = behavior.DistractionPercentage;
  out_driverParameters.distractionQuota = behavior.DistractionQuota;

  out_driverParameters.desiredVelocity = behavior.VWish;
  out_driverParameters.amountOfSpeedLimitViolation = behavior.DeltaVViolation;
  out_driverParameters.amountOfDesiredVelocityViolation = behavior.DeltaVWish;
  out_driverParameters.desiredTimeAtTargetSpeed = behavior.DesiredTimeAtTargetSpeed;

  scm::parameter::NormalDistribution lateralSafetyDistanceForEvadingInfo = {lateralSafetyDistanceForEvadingMean.value(),
                                                                            lateralSafetyDistanceForEvadingStandardDeviation.value(),
                                                                            lateralSafetyDistanceForEvadingMin.value(),
                                                                            lateralSafetyDistanceForEvadingMax.value()};
  out_driverParameters.lateralSafetyDistanceForEvading = units::make_unit<units::length::meter_t>(CalculateStochastic(lateralSafetyDistanceForEvadingInfo));

  // car following related parameters
  out_driverParameters.carQueuingDistance = behavior.CarQueuingDistance;
  out_driverParameters.carRestartDistance = out_driverParameters.carQueuingDistance + behavior.HysteresisForRestart;

  // acceleration behaviour related parameters
  out_driverParameters.comfortLongitudinalAcceleration = behavior.ComfortLongitudinalAcceleration;
  out_driverParameters.comfortLongitudinalDeceleration = behavior.ComfortLongitudinalDeceleration;

  _comfortAccelerationParameter.comfortAcceleration = out_driverParameters.comfortLongitudinalAcceleration;
  _comfortAccelerationParameter.comfortDeceleration = out_driverParameters.comfortLongitudinalDeceleration;
  _comfortAccelerationParameter.modifications.accelerationAdjustmentThresholdVelocity = behavior.AccelerationAdjustmentThresholdVelocity;
  _comfortAccelerationParameter.modifications.velocityForMaxAcceleration = behavior.VelocityForMaxAcceleration;

  out_driverParameters.comfortLateralAcceleration = behavior.ComfortLateralAcceleration;
  out_driverParameters.maximumLongitudinalAcceleration = behavior.MaxLongitudinalAcceleration;
  out_driverParameters.maximumLongitudinalDeceleration = behavior.MaxLongitudinalDeceleration;

  out_driverParameters.maximumLateralAcceleration = behavior.MaxLateralAcceleration;

  out_driverParameters.lateralOffsetNeutralPositionQuota = behavior.LateralOffsetNeutralPositionQuota;
  // off-center driving related parameters
  if (_stochastics->GetUniformDistributed(0, 1) > out_driverParameters.lateralOffsetNeutralPositionQuota)
  {
    out_driverParameters.lateralOffsetNeutralPosition = 0_m;
    out_driverParameters.lateralOffsetUsed = false;
  }
  else
  {
    out_driverParameters.lateralOffsetNeutralPosition = behavior.LateralOffsetNeutralPosition;
  }

  out_driverParameters.lateralOffsetNeutralPositionRescueLane = behavior.LateralOffsetNeutralPositionRescueLane;

  // lane change related parameters
  out_driverParameters.maximumAngularVelocitySteeringWheel = behavior.MaximumAngularVelocitySteeringWheel;
  out_driverParameters.comfortAngularVelocitySteeringWheel = behavior.ComfortAngularVelocitySteeringWheel;
  out_driverParameters.timeHeadwayFreeLaneChange = behavior.TimeHeadwayFreeLaneChange;
  CalculateSpeedLimitAnticipationPercentage(driverBehavior);

  LogDriverParameters();
}

void ScmParameters::CalculateSpeedLimitAnticipationPercentage(const GenerateDriverBehavior& driverBehavior)
{
  // get external information
  double speedLimitAnticipationPercentageMin = .1;  // Not 0 and 100% to guarantee that each speed limit sign is followed even when driver is looking away from road
  double speedLimitAnticipationPercentageMax = .9;

  // set speedLimitAnticipationPercentage within boundaries
  const double min = std::get<scm::parameter::LogNormalDistribution>(std::get<scm::parameter::StochasticDistribution>(driverBehavior.parameters.at("DeltaVViolation"))).GetMin();
  const double max = std::get<scm::parameter::LogNormalDistribution>(std::get<scm::parameter::StochasticDistribution>(driverBehavior.parameters.at("DeltaVViolation"))).GetMax();

  out_driverParameters.speedLimitAnticipationPercentage = speedLimitAnticipationPercentageMax -
                                                          (out_driverParameters.amountOfSpeedLimitViolation.value() - min) *
                                                              (speedLimitAnticipationPercentageMax - speedLimitAnticipationPercentageMin) /
                                                              (max - min);
}

void ScmParameters::LogDriverParameters()
{
  const std::string prefix = "Driver.Parameter.";

  _scmPublisher->Publish(prefix + "desiredVelocity", out_driverParameters.desiredVelocity.value());
  _scmPublisher->Publish(prefix + "previewTimeHeadway", previewTimeHeadway.value());
  _scmPublisher->Publish(prefix + "thresholdLooming", out_driverParameters.thresholdLoomingFovea.value());
  _scmPublisher->Publish(prefix + "anticipationQuota", out_driverParameters.anticipationQuota.value());
  _scmPublisher->Publish(prefix + "highCognitiveMode", out_driverParameters.highCognitiveMode);
  _scmPublisher->Publish(prefix + "lateralOffsetUsed", out_driverParameters.lateralOffsetUsed);
  _scmPublisher->Publish(prefix + "carQueuingDistance", out_driverParameters.carQueuingDistance.value());
  _scmPublisher->Publish(prefix + "carRestartDistance", out_driverParameters.carRestartDistance.value());
  _scmPublisher->Publish(prefix + "outerKeepingIntensity", out_driverParameters.outerKeepingIntensity.value());
  _scmPublisher->Publish(prefix + "egoLaneKeepingIntensity", out_driverParameters.egoLaneKeepingIntensity.value());
  _scmPublisher->Publish(prefix + "agentCooperationFactor", out_driverParameters.agentCooperationFactor);
  _scmPublisher->Publish(prefix + "desiredTimeAtTargetSpeed", out_driverParameters.desiredTimeAtTargetSpeed.value());
  _scmPublisher->Publish(prefix + "comfortLateralAcceleration", out_driverParameters.comfortLateralAcceleration.value());
  _scmPublisher->Publish(prefix + "maximumLateralAcceleration", out_driverParameters.maximumLateralAcceleration.value());
  _scmPublisher->Publish(prefix + "amountOfSpeedLimitViolation", out_driverParameters.amountOfSpeedLimitViolation.value());
  _scmPublisher->Publish(prefix + "lateralOffsetNeutralPosition", out_driverParameters.lateralOffsetNeutralPosition.value());
  _scmPublisher->Publish(prefix + "comfortLongitudinalAcceleration", out_driverParameters.comfortLongitudinalAcceleration.value());
  _scmPublisher->Publish(prefix + "comfortLongitudinalDeceleration", out_driverParameters.comfortLongitudinalDeceleration.value());
  _scmPublisher->Publish(prefix + "maxComfortAccelerationFactor", _comfortAccelerationParameter.modifications.maxComfortAccelerationFactor);
  _scmPublisher->Publish(prefix + "maxComfortDecelerationFactor", _comfortAccelerationParameter.modifications.maxComfortDecelerationFactor);
  _scmPublisher->Publish(prefix + "accelerationAdjustmentThresholdVelocity", _comfortAccelerationParameter.modifications.accelerationAdjustmentThresholdVelocity.value());
  _scmPublisher->Publish(prefix + "velocityForMaxAcceleration", _comfortAccelerationParameter.modifications.velocityForMaxAcceleration.value());
  _scmPublisher->Publish(prefix + "lateralSafetyDistanceForEvading", out_driverParameters.lateralSafetyDistanceForEvading.value());
  _scmPublisher->Publish(prefix + "maximumLongitudinalAcceleration", out_driverParameters.maximumLongitudinalAcceleration.value());
  _scmPublisher->Publish(prefix + "maximumLongitudinalDeceleration", out_driverParameters.maximumLongitudinalDeceleration.value());
  _scmPublisher->Publish(prefix + "amountOfDesiredVelocityViolation", out_driverParameters.amountOfDesiredVelocityViolation.value());
  _scmPublisher->Publish(prefix + "speedLimitAnticipationPercentage", out_driverParameters.speedLimitAnticipationPercentage);
  _scmPublisher->Publish(prefix + "lateralOffsetNeutralPositionQuota", out_driverParameters.lateralOffsetNeutralPositionQuota.value());
  _scmPublisher->Publish(prefix + "laneChangeProhibitionIgnoringQuota", out_driverParameters.laneChangeProhibitionIgnoringQuota.value());
  _scmPublisher->Publish(prefix + "agentSuspiciousBehaviourEvasionFactor", out_driverParameters.agentSuspiciousBehaviourEvasionFactor);
  _scmPublisher->Publish(prefix + "lateralOffsetNeutralPositionRescueLane", out_driverParameters.lateralOffsetNeutralPositionRescueLane.value());
  _scmPublisher->Publish(prefix + "rightOvertakingProhibitionIgnoringQuota", out_driverParameters.rightOvertakingProhibitionIgnoringQuota.value());
  _scmPublisher->Publish(prefix + "proportionalityFactorForFollowingDistance", out_driverParameters.proportionalityFactorForFollowingDistance);
  _scmPublisher->Publish(prefix + "thresholdRetinalProjectionAngularSpeedFovea", out_driverParameters.thresholdRetinalProjectionAngularSpeedFovea.value());
  _scmPublisher->Publish(prefix + "maximumAngularVelocitySteeringWheel", out_driverParameters.maximumAngularVelocitySteeringWheel.value());
  _scmPublisher->Publish(prefix + "comfortAngularVelocitySteeringWheel", out_driverParameters.comfortAngularVelocitySteeringWheel.value());
  _scmPublisher->Publish(prefix + "timeHeadwayFreeLaneChange", out_driverParameters.timeHeadwayFreeLaneChange.value());
  _scmPublisher->Publish(prefix + "egoLaneKeepingQuota", out_driverParameters.egoLaneKeepingQuota.value());
  _scmPublisher->Publish(prefix + "outerKeepingQuota", out_driverParameters.outerKeepingQuota.value());
}

bool ScmParameters::IsDriverCharacteristicsSamplerRequired()
{
  return IsUnset(out_driverParameters.desiredVelocity.value()) ||
         IsUnset(out_driverParameters.amountOfSpeedLimitViolation.value()) ||
         IsUnset(out_driverParameters.comfortLongitudinalAcceleration.value()) ||
         IsUnset(out_driverParameters.comfortLongitudinalDeceleration.value()) ||
         IsUnset(out_driverParameters.outerKeepingIntensity.value_or(ScmDefinitions::DEFAULT_VALUE));
}

void ScmParameters::ApplyDriverCharacteristicsSampler()
{
  std::unique_ptr<DriverCharacteristics> sample = driverCharacteristicsSampler->SampleDriverCharacteristics();

  if (IsUnset(out_driverParameters.desiredVelocity.value()))
  {
    out_driverParameters.desiredVelocity = sample->vWish;
  }

  if (IsUnset(out_driverParameters.amountOfSpeedLimitViolation.value()))
  {
    out_driverParameters.amountOfSpeedLimitViolation = sample->deltaVViolation;
  }

  if (IsUnset(out_driverParameters.agentCooperationFactor))
  {
    out_driverParameters.agentCooperationFactor = sample->agentCooperationFactor;
  }

  if (IsUnset(out_driverParameters.agentSuspiciousBehaviourEvasionFactor))
  {
    out_driverParameters.agentSuspiciousBehaviourEvasionFactor = sample->agentSuspiciousBehaviourEvasionFactor;
  }

  if (IsUnset(out_driverParameters.comfortLongitudinalAcceleration.value()))
  {
    out_driverParameters.comfortLongitudinalAcceleration = sample->comfortLongitudinalAcceleration;
  }

  if (IsUnset(out_driverParameters.comfortLongitudinalDeceleration.value()))
  {
    out_driverParameters.comfortLongitudinalDeceleration = sample->comfortLongitudinalDeceleration;
  }

  if (IsUnset(out_driverParameters.outerKeepingIntensity.value_or(ScmDefinitions::DEFAULT_VALUE)))
  {
    out_driverParameters.outerKeepingIntensity = sample->outerKeepingIntensity;
  }
}

bool ScmParameters::IsUnset(double value)
{
  return std::abs(value - ScmDefinitions::DEFAULT_VALUE) < DEFAULT_VALUE_TOLERANCE;
}

void ScmParameters::EgoAgentAdapterToWorldTrafficRules()
{
  GenerateTrafficRules gen;
  TrafficRulesScm trafficRulesScm = gen.Generate("DE");  // Get current country from where?

  out_trafficRulesScm.common = trafficRulesScm.common;
  out_trafficRulesScm.motorway = trafficRulesScm.motorway;

  switch (_vehicleClass)
  {
    case scm::common::VehicleClass::kCompact_car:
    case scm::common::VehicleClass::kMedium_car:
    case scm::common::VehicleClass::kMotorbike:
    case scm::common::VehicleClass::kBicycle:
      out_trafficRulesScm.motorway.openSpeedLimit.value = ScmDefinitions::INF_VELOCITY;
      break;
    case scm::common::VehicleClass::kBus:
      out_trafficRulesScm.motorway.openSpeedLimit.value = 100.0_kph;
    case scm::common::VehicleClass::kHeavy_truck:
      out_trafficRulesScm.motorway.openSpeedLimit.value = 80.0_kph;
      break;
    default:
      throw std::runtime_error("ScmParameters::EgoAgentAdapterToWorldTrafficRules - invalid AgentVehicleType");
  }
}

}  // namespace scm