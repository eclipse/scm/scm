/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  DriverCharacteristicsSampler.h

#pragma once

#include <Stochastics/StochasticsInterface.h>

#include <memory>
#include <string>
#include <vector>

#include "DriverCharacteristics.h"
#include "StochasticData.h"

/** \addtogroup DriverCharacteristicsSampler
 * @{
 * \brief Samples driver characteristic parameters based on a Structural Equation Model (SEM).
 * For sampling it uses Kernel Density Estimates (KDE) of recorded driving simulator data.
 *
 * \details All data, which is needed from the DriverCharacteristicsSampler, is stored in the
 * computer generated StochasticData.h file. The DriverCharacteristicsSampler then uses the SEM
 * and the KDE, which is defined in the StochasticData.h, to sample a value for each random variable.
 * For the random sampling part this class highly depends on functions from the stochastics module.
 * For the data transfer between those two modules a data transfer struct DiscreteDistributionInformation
 * was introduced. For every random variable in the SEM one DiscreteDistributionInformation struct
 * is defined in the StochasticData.h.
 *
 * The DriverCharacteristics parameters being sampled are wrapped into a DriverCharacteristics struct
 * which is finally passed back to the InitDriver.
 *
 * @} */

class DriverCharacteristicsSampler
{
public:
  //! The component name of the DriverCharacteristicsSampler.
  const std::string COMPONENTNAME = "DriverCharacteristicsSampler";

  /*!
   * \brief Constructor
   *
   * @param[in]     stochastics    StochasticsInterface for DriverCharacteristicsSampler from InitDriver.
   */
  explicit DriverCharacteristicsSampler(StochasticsInterface* stochastics);
  ~DriverCharacteristicsSampler();

  /*!
   * \brief Samples driver characteristics from distribution.
   *
   * \details This is the main function of the DriverCharacteristicsSampler and is being called from the InitDriver UpdateOutput() function.
   * Samples driver characteristic parameters based on a Structural Equation Model (SEM). For sampling it uses Kernel Density Estimates (KDE) of recorded driving simulator data.
   *
   * @return       Sampled DriverCharacteristics parameters.
   */
  std::unique_ptr<DriverCharacteristics> SampleDriverCharacteristics();

private:
  //! The IDs of the stochastic variables in order of sampling.
  std::vector<int> sampleIDs;
  //! The names of the stochastic variables in order of sampling.
  std::vector<std::string> sampleNames;
  //! The sampled values of the stochastic variables in order of sampling.
  std::vector<double> sampleValues;

  //! Reference to stochastics framework.
  StochasticsInterface* stochastics;

  /*!
   * \brief Helper method of the SampleDriverCharacteristics method.
   *
   * Searches for name in sampleNames and returns the value at the same position in sampleValues.
   *
   * @param[in]    name    Variable name to be searched for.
   */
  double getSampeledValueByName(std::string name);

  /*!
   * \brief Helper method of the SampleDriverCharacteristics method.
   *
   * Searches for id in sampleIDs and returns the value at the same position in sampleValues.
   *
   * @param[in]    id      Variable ID to be searched for.
   */
  double getSampeledValueByID(int id);
};
