/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  ParametersScmSignal.h
//! @brief This file contains all functions for class
//! ParametersScmSignal
//!
//! This class contains all functionality of the signal.
//-----------------------------------------------------------------------------
#pragma once

#include <sstream>
#include <string>

#include "../TrafficRules/TrafficRules.h"
#include "ParametersScmDefinitions.h"

class ParametersScmSignal
{
public:
  const std::string COMPONENTNAME = "ParametersScmSignal";

  //-----------------------------------------------------------------------------
  //! Constructor
  //-----------------------------------------------------------------------------
  ParametersScmSignal(DriverParameters driverParameters, TrafficRulesScm trafficRulesScm)
      : driverParameters(driverParameters),
        trafficRulesScm(trafficRulesScm)
  {
  }

  virtual ~ParametersScmSignal() = default;

  //-----------------------------------------------------------------------------
  //! Returns the content/payload of the signal as an std::string
  //!
  //! @return                       Content/payload of the signal as an std::string
  //-----------------------------------------------------------------------------
  operator std::string() const
  {
    std::ostringstream stream;
    stream << COMPONENTNAME;
    return stream.str();
  }

  DriverParameters driverParameters;
  TrafficRulesScm trafficRulesScm;
};
