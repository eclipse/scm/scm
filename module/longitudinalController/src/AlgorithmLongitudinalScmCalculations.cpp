/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  AlgorithmLongitudinalScmCalculations.cpp */
//-----------------------------------------------------------------------------

#include "AlgorithmLongitudinalScmCalculations.h"

#include <algorithm>
#include <stdexcept>

#include "include/common/CommonHelper.h"
#include "include/common/ScmDefinitions.h"

/******* <copy from previously inherited public AlgorithmLongitudinalCalculations> **************/

double AlgorithmLongitudinalScmCalculations::GetBrakePedalPosition() const
{
  return brakePedalPosition;
}

double AlgorithmLongitudinalScmCalculations::GetAcceleratorPedalPosition() const
{
  return acceleratorPedalPosition;
}

units::angular_velocity::revolutions_per_minute_t AlgorithmLongitudinalScmCalculations::GetEngineSpeed() const
{
  return _engineSpeed;
}

int AlgorithmLongitudinalScmCalculations::GetGear() const
{
  return gear;
}

units::torque::newton_meter_t AlgorithmLongitudinalScmCalculations::GetEngineTorqueAtGear(int gear, const units::acceleration::meters_per_second_squared_t& acceleration)
{
  if (acceleration == 0_mps_sq || gear == 0)
  {
    return 0.0_Nm;
  }

  if (gear > GetVehicleProperty(scm::common::vehicle::properties::NumberOfGears) || gear < 0)
  {
    throw std::runtime_error("Gear in AlgorithmLongitudinal is invalid");
  }

  units::torque::newton_meter_t wheelSetTorque = _vehicleModelParameters.mass * 0.5 * _vehicleModelParameters.rear_axle.wheel_diameter * acceleration;
  auto engineTorqueAtGear = wheelSetTorque / (GetVehicleProperty(scm::common::vehicle::properties::AxleRatio) * GetVehicleProperty(scm::common::vehicle::properties::GearRatio + std::to_string(gear)));

  return engineTorqueAtGear;
}

double AlgorithmLongitudinalScmCalculations::GetVehicleProperty(const std::string& propertyName)
{
  const auto property = scm::common::map::query(_vehicleModelParameters.properties, propertyName);
  scm::common::THROWIFFALSE(property.has_value(), "Vehicle property \"" + propertyName + "\" was not set in the VehicleCatalog");
  return std::stod(property.value());
}

units::angular_velocity::revolutions_per_minute_t AlgorithmLongitudinalScmCalculations::GetEngineSpeedByVelocity(const units::velocity::meters_per_second_t& xVel, int gear)
{
  return 1_rad * (xVel * GetVehicleProperty(scm::common::vehicle::properties::AxleRatio) * GetVehicleProperty(scm::common::vehicle::properties::GearRatio + std::to_string(gear))) / (_vehicleModelParameters.rear_axle.wheel_diameter * 0.5);  // an dynamic wheel radius rDyn must actually be used here
}

void AlgorithmLongitudinalScmCalculations::CalculateGearAndEngineSpeed()
{
  std::map<units::angular_velocity::revolutions_per_minute_t, int> nEngineSet;
  std::map<units::acceleration::meters_per_second_squared_t, std::tuple<int, units::angular_velocity::revolutions_per_minute_t, units::acceleration::meters_per_second_squared_t>> minDeltaAccWheelBased;

  const auto numberOfGears = GetVehicleProperty(scm::common::vehicle::properties::NumberOfGears);
  for (int gear = 1; gear <= numberOfGears; ++gear)
  {
    const auto engineSpeed = GetEngineSpeedByVelocity(_velocity, gear);
    units::acceleration::meters_per_second_squared_t limitWheelAcc;
    units::acceleration::meters_per_second_squared_t accDelta;

    if (_accelerationWish >= 0.0_mps_sq)
    {
      auto MMax = GetEngineTorqueMax(engineSpeed);
      limitWheelAcc = GetAccFromEngineTorque(MMax, gear);

      if (_accelerationWish == 0.0_mps_sq)
        accDelta = 0_mps_sq;
      else
        accDelta = units::math::fabs(_accelerationWish - limitWheelAcc);
    }
    else
    {
      auto MMin = GetEngineTorqueMin(engineSpeed);
      limitWheelAcc = GetAccFromEngineTorque(MMin, gear);
      accDelta = units::math::fabs(_accelerationWish - limitWheelAcc);
    }

    nEngineSet[engineSpeed] = gear;
    minDeltaAccWheelBased[accDelta] = {gear, engineSpeed, limitWheelAcc};
  }

  bool foundGear = false;

  for (const auto& [engineSpeedResult, gearResult] : nEngineSet)
  {
    if (isWithinEngineLimits(gearResult, engineSpeedResult, _accelerationWish))  //&&isChangeOfGearPossibleNow
    {
      gear = gearResult;
      _engineSpeed = engineSpeedResult;

      foundGear = true;
    }
    else if (foundGear)  // leaving possible range
    {
      return;
    }
  }

  if (foundGear)
  {
    return;
  }

  // take lowest delta for gear and engineSpeed
  auto val = minDeltaAccWheelBased.begin()->second;

  // trim wish acceleration to possible value
  gear = std::get<0>(val);
  _engineSpeed = std::get<1>(val);
  _accelerationWish = units::math::min(_accelerationWish, std::get<2>(val));
}

bool AlgorithmLongitudinalScmCalculations::isWithinEngineLimits(int gear, const units::angular_velocity::revolutions_per_minute_t& engineSpeed, const units::acceleration::meters_per_second_squared_t& acceleration)
{
  if (!isEngineSpeedWithinEngineLimits(engineSpeed))
  {
    return false;
  }

  auto currentWishTorque = GetEngineTorqueAtGear(gear, acceleration);

  return isTorqueWithinEngineLimits(currentWishTorque, engineSpeed);
}

bool AlgorithmLongitudinalScmCalculations::isTorqueWithinEngineLimits(const units::torque::newton_meter_t& torque, const units::angular_velocity::revolutions_per_minute_t& engineSpeed)
{
  auto currentMEngMax = GetEngineTorqueMax(engineSpeed);

  return torque <= currentMEngMax;
}

inline bool AlgorithmLongitudinalScmCalculations::isEngineSpeedWithinEngineLimits(const units::angular_velocity::revolutions_per_minute_t& engineSpeed)
{
  return (engineSpeed >= units::angular_velocity::revolutions_per_minute_t(GetVehicleProperty(scm::common::vehicle::properties::MinimumEngineSpeed)) && engineSpeed <= units::angular_velocity::revolutions_per_minute_t(GetVehicleProperty(scm::common::vehicle::properties::MaximumEngineSpeed)));
}

units::torque::newton_meter_t AlgorithmLongitudinalScmCalculations::GetEngineTorqueMax(const units::angular_velocity::revolutions_per_minute_t& engineSpeed)
{
  const units::torque::newton_meter_t maximumEngineTorque{GetVehicleProperty(scm::common::vehicle::properties::MaximumEngineTorque)};
  const units::angular_velocity::revolutions_per_minute_t maximumEngineSpeed{GetVehicleProperty(scm::common::vehicle::properties::MaximumEngineSpeed)};
  const units::angular_velocity::revolutions_per_minute_t minimumEngineSpeed{GetVehicleProperty(scm::common::vehicle::properties::MinimumEngineSpeed)};

  auto torqueMax = maximumEngineTorque;  // initial value at max
  auto speed = engineSpeed;

  bool isLowerSection = engineSpeed < minimumEngineSpeed + 1000_rpm;
  bool isBeyondLowerSectionBorder = engineSpeed < minimumEngineSpeed;
  bool isUpperSection = engineSpeed > maximumEngineSpeed - 1000_rpm;
  bool isBeyondUpperSectionBorder = engineSpeed > maximumEngineSpeed;

  if (isLowerSection)
  {
    if (isBeyondLowerSectionBorder)  // not within limits
    {
      speed = minimumEngineSpeed;
    }

    const auto tempEngineSpeed = 1000_rpm - (speed - minimumEngineSpeed);
    torqueMax = units::inverse_radian(0.5 / M_PI) * (tempEngineSpeed)*units::unit_t<units::compound_unit<units::torque::newton_meter, units::time::minute>>(-0.1) + maximumEngineTorque;
  }
  else if (isUpperSection)
  {
    if (isBeyondUpperSectionBorder)
    {
      speed = maximumEngineSpeed;
    }

    const auto tempEngineSpeed = speed - maximumEngineSpeed + 1000_rpm;
    torqueMax = units::inverse_radian(0.5 / M_PI) * tempEngineSpeed * units::unit_t<units::compound_unit<units::torque::newton_meter, units::time::minute>>(-0.04) + maximumEngineTorque;
  }

  return torqueMax;
}

units::torque::newton_meter_t AlgorithmLongitudinalScmCalculations::GetEngineTorqueMin(const units::angular_velocity::revolutions_per_minute_t& engineSpeed)
{
  return GetEngineTorqueMax(engineSpeed) * -.1;
}

units::acceleration::meters_per_second_squared_t AlgorithmLongitudinalScmCalculations::GetAccFromEngineTorque(const units::torque::newton_meter_t& engineTorque, int chosenGear)
{
  const auto wheelSetTorque = engineTorque * (GetVehicleProperty(scm::common::vehicle::properties::AxleRatio) * GetVehicleProperty(scm::common::vehicle::properties::GearRatio + std::to_string(chosenGear)));
  const units::force::newton_t wheelSetForce = wheelSetTorque / (0.5 * _vehicleModelParameters.rear_axle.wheel_diameter);
  return wheelSetForce / _vehicleModelParameters.mass;
}

void AlgorithmLongitudinalScmCalculations::CalculatePedalPositions()
{
  const units::acceleration::meters_per_second_squared_t oneG{9.81};

  units::angular_velocity::revolutions_per_minute_t engineSpeedInUnits(_engineSpeed);

  if (_accelerationWish < 0.0_mps_sq)  // speed shall be reduced with drag or brake
  {
    const auto engineTorque = GetEngineTorqueAtGear(gear, _accelerationWish);
    const auto MDragMax = GetEngineTorqueMin(engineSpeedInUnits);

    if (engineTorque < MDragMax)
    {  // brake

      // calculate acceleration of MDragMax and substract
      // this from in_aVehicle since brake and drag work in parallel while clutch is closed
      const auto accMDragMax = GetAccFromEngineTorque(MDragMax, gear);

      acceleratorPedalPosition = 0.0;

      const auto pedalPositionBasedOnAcceleration = -(_accelerationWish - accMDragMax) / oneG;
      brakePedalPosition = std::min(pedalPositionBasedOnAcceleration.value(), 1.0);
      return;
    }
  }

  // cases of acceleration and drag => use engine here

  const auto MDragMax = GetEngineTorqueMin(engineSpeedInUnits);
  const auto MTorqueMax = GetEngineTorqueMax(engineSpeedInUnits);

  const auto wishTorque = GetEngineTorqueAtGear(gear, _accelerationWish);

  acceleratorPedalPosition = std::min(units::unit_cast<double>((wishTorque - MDragMax) / (MTorqueMax - MDragMax)), 1.0);
  brakePedalPosition = 0.0;
}

/******* </copy from previously inherited public AlgorithmLongitudinalCalculations> *************/

double AlgorithmLongitudinalScmCalculations::ClosedLoopController(units::velocity::meters_per_second_t xVel,
                                                                  units::velocity::meters_per_second_t lastXVel,
                                                                  units::acceleration::meters_per_second_squared_t lastIn_aVehicle,
                                                                  units::time::millisecond_t cycleTime)
{
  double proportionalFactor = .5;  // Empirically determied value [s/m]
  double integralFactor = .05;     // "Arbitrarily" defined when expanding from P controller to PI controller
  units::velocity::meters_per_second_t lastVelocityWish = units::math::fmax(lastIn_aVehicle * cycleTime + lastXVel, 0.0_mps);

  // Velocity delta of former setpoint and actual value
  auto deltaVel = lastVelocityWish - xVel;

  if (isSpawn)
  {
    _integralOfDeltaVelocity = 0.0_m;
    return 0.0;
  }
  else
  {
    _integralOfDeltaVelocity += deltaVel * cycleTime;
  }

  return (units::math::abs(deltaVel) >= (3.0_mps_sq * cycleTime)) ? proportionalFactor * units::unit_cast<double>(deltaVel) + integralFactor * units::unit_cast<double>(_integralOfDeltaVelocity) : 0.;
}

void AlgorithmLongitudinalScmCalculations::ApplyClosedLoopController()
{
  double pedalPosition = acceleratorPedalPosition - brakePedalPosition;
  double pedalPosCLC = ClosedLoopController(_velocity, _lastVelocity, _lastAccelerationWish, _cycleTime);
  double newPedalPosition = pedalPosition + pedalPosCLC;
  acceleratorPedalPosition = std::clamp(newPedalPosition, 0.0, 1.0);
  brakePedalPosition = std::clamp(-newPedalPosition, 0.0, 1.0);
}

FootPosition AlgorithmLongitudinalScmCalculations::GetFootPosition() const
{
  return _footPosition;
}

units::length::meter_t AlgorithmLongitudinalScmCalculations::GetIntegralOfDeltaVelocity() const
{
  return _integralOfDeltaVelocity;
}

bool AlgorithmLongitudinalScmCalculations::IsMajorBrake(double currentVal)
{
  constexpr double majorBrakePercentage = 0.6;
  // TODO discuss if pedal acceleration shoul be used to determine major brake
  return (currentVal < 0 && std::fabs(currentVal) >= majorBrakePercentage);
}

void AlgorithmLongitudinalScmCalculations::ApplyFirstOrderTransientBehaviour()
{
  double tOne = 0.12 / units::unit_cast<double>(units::time::second_t(_cycleTime));
  constexpr double tOneMajorBrake = 0.08;
  double pedalPosition = acceleratorPedalPosition - brakePedalPosition;

  if (IsMajorBrake(pedalPosition))  // at a major break the driver has to be faster at changing the pedal position
    tOne = tOneMajorBrake / units::unit_cast<double>(units::time::second_t(_cycleTime));

  double betaVal = tOne / (tOne + 1);
  double alphaVal = 1 / (tOne + 1);

  double newPedalPosition = alphaVal * pedalPosition + betaVal * _lastPedalPositionOLC;
  acceleratorPedalPosition = std::clamp(newPedalPosition, 0.0, 1.0);
  brakePedalPosition = std::clamp(-newPedalPosition, 0.0, 1.0);
}

void AlgorithmLongitudinalScmCalculations::CheckForPedalChange()
{
  if (_footPosition == FootPosition::NotYetActing)  // If foot was not on a pedal yet, then pedalChange is instant
  {
    PedalChangeFinished();
    return;
  }

  if (brakePedalPosition > 0 && _footPosition == FootPosition::AcceleratorPedal)
  {
    _footPosition = FootPosition::ChangingToBrakePedal;
  }
  if (acceleratorPedalPosition > 0 && _footPosition == FootPosition::BrakePedal)
  {
    _footPosition = FootPosition::ChangingToAcceleratorPedal;
  }
}

void AlgorithmLongitudinalScmCalculations::PedalChangeFinished()
{
  if (acceleratorPedalPosition > 0)
  {
    _footPosition = FootPosition::AcceleratorPedal;
  }
  else if (brakePedalPosition > 0)
  {
    _footPosition = FootPosition::BrakePedal;
  }
}

void AlgorithmLongitudinalScmCalculations::PedalChangeOngoing()
{
  acceleratorPedalPosition = 0.0;
  brakePedalPosition = 0.0;
}
