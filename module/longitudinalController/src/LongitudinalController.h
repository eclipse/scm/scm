/********************************************************************************
 * Copyright (c) 2017-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <Stochastics/StochasticsInterface.h>

#include "AlgorithmLongitudinalScmCalculations.h"
#include "include/module/longitudinalControllerInterface.h"

namespace scm
{

class LongitudinalController : public scm::longitudinal_controller::Interface
{
public:
  //! Name of the current component
  const std::string COMPONENTNAME = "AlgorithmLongitudinalSCM";

  explicit LongitudinalController(StochasticsInterface* stochastics)
      : isSpawn{true}, stochastics(stochastics){};

  virtual ~LongitudinalController() = default;
  scm::signal::LongitudinalControllerOutput Trigger(scm::signal::LongitudinalControllerInput input, units::time::millisecond_t time) override;

private:
  //! \brief Set values for stopped car
  virtual void SetValuesAtStandstill();

  //! \brief Calculate the pedal position and the current gear
  //! \param [in] xVel     Longitudinal velocity
  //! \return pedal position of the driver neglecting pedal change time
  virtual void CalculatePedalPositionAndGear(units::time::millisecond_t timeStamp);

  //! \brief  Returns the pedal change time
  //! \return pedal change time
  units::time::millisecond_t GetPedalChangeTime();

  //! initialize module with sended prioritzer data
  bool initializedAccelerationInput{false};

  //! initialize module with sended vehicle parameters sended from ParametersVehicle
  bool initializedVehicleModelParameters{false};

  //! initialize module with sended human parameters sended from ParametersScm
  bool initializedHumanParameters{false};

  //! initialize module with sended parameters sended from SensoDriver
  bool initializedSensorDriverData{false};

  //! Cycle time of this components trigger task [ms].
  units::time::millisecond_t cycleTime{};

  //! Current position of the right foot
  FootPosition footPosition{FootPosition::NotYetActing};

  //! Time stemp at which the pedal change will be finished
  units::time::millisecond_t timeStempPedalChangeFinished{-1};

  //  --- Init Inputs

  //! Minimum pedal change time [ms].
  units::time::millisecond_t pedalChangeTimeMin = 180_ms;

  //! Mean pedal change time [ms].
  units::time::millisecond_t pedalChangeTimeMean = 290_ms;

  //! Standart deviation of pedal change time [ms].
  units::time::millisecond_t pedalChangeTimeStdDev = 73_ms;

  scm::common::vehicle::properties::EntityProperties vehicleModelParameters;

  // --- Inputs
  //! Flag wether agent has just been spawned.
  bool isSpawn{false};
  //! The wish acceleration of the agent in m/s^2.
  units::acceleration::meters_per_second_squared_t accelerationWish{0.0};
  //! current agent acceleration
  units::acceleration::meters_per_second_squared_t currentAccelaration{0.0};
  //! current agent velocity
  units::velocity::meters_per_second_t currentVelocity{0.0};
  //! last calculated pedal position (from 1 to -1)
  double lastPedalPositionOLC = 0.0;
  //! last known velocity of the vehicle
  units::velocity::meters_per_second_t lastVelocity{0.0};
  //! last known setpoint of acceleration value
  units::acceleration::meters_per_second_squared_t lastAccelerationWish{0.0};
  //! integral of deltaVelocity wish-actual
  units::length::meter_t integralOfDeltaVelocity{0.0};

  //  --- Outputs

  //! Position of the accecaleration pedal position in percent.
  double out_accPedalPos = 0.;
  //! Position of the brake pedal position in percent.
  double out_brakePedalPos = 0.;
  //! Number of gears and the currently choosen gear.
  int out_gear{0};

  //! Provides access to the stochastics functionality of the framework.
  StochasticsInterface* stochastics;

  double CalculateAirDrag() const;
  double GetAirDragParameter(std::string paramName) const;
};

}  // namespace scm