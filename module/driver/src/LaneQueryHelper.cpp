/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "LaneQueryHelper.h"

#include <algorithm>

bool LaneQueryHelper::IsLeftLane(AreaOfInterest aoi) noexcept
{
  return aoi == AreaOfInterest::LEFT_FRONT || aoi == AreaOfInterest::LEFT_FRONT_FAR ||
         aoi == AreaOfInterest::LEFT_SIDE || aoi == AreaOfInterest::LEFT_REAR;
}

bool LaneQueryHelper::IsLeftLeftLane(AreaOfInterest aoi) noexcept
{
  return aoi == AreaOfInterest::LEFTLEFT_FRONT || aoi == AreaOfInterest::LEFTLEFT_SIDE ||
         aoi == AreaOfInterest::LEFTLEFT_REAR;
}

bool LaneQueryHelper::IsRightLane(AreaOfInterest aoi) noexcept
{
  return aoi == AreaOfInterest::RIGHT_FRONT || aoi == AreaOfInterest::RIGHT_FRONT_FAR ||
         aoi == AreaOfInterest::RIGHT_SIDE || aoi == AreaOfInterest::RIGHT_REAR;
}

bool LaneQueryHelper::IsRightRightLane(AreaOfInterest aoi) noexcept
{
  return aoi == AreaOfInterest::RIGHTRIGHT_FRONT || aoi == AreaOfInterest::RIGHTRIGHT_SIDE ||
         aoi == AreaOfInterest::RIGHTRIGHT_REAR;
}

bool LaneQueryHelper::IsEgoLane(AreaOfInterest aoi) noexcept
{
  return aoi == AreaOfInterest::EGO_FRONT || aoi == AreaOfInterest::EGO_FRONT_FAR ||
         aoi == AreaOfInterest::EGO_REAR;
}

bool LaneQueryHelper::IsLaneMarkingBold(const scm::LaneMarking::Entity& laneMarking) noexcept
{
  return laneMarking.width > 0.23_m && laneMarking.width <= 0.4_m;
}

bool LaneQueryHelper::IsLaneMarkingStandardWidth(const scm::LaneMarking::Entity& laneMarking) noexcept
{
  return laneMarking.width >= 0.1_m && laneMarking.width <= 0.23_m;
}

bool LaneQueryHelper::IsFrontArea(AreaOfInterest aoi) noexcept
{
  return std::any_of(_frontAois.begin(), _frontAois.end(), [aoi](AreaOfInterest front_aoi)
                     { return aoi == front_aoi; }) ||
         IsFrontFarArea(aoi);
}

bool LaneQueryHelper::IsFrontFarArea(AreaOfInterest aoi) noexcept
{
  return std::any_of(_frontFarAois.begin(), _frontFarAois.end(), [aoi](AreaOfInterest frontFar_aoi)
                     { return aoi == frontFar_aoi; });
}

bool LaneQueryHelper::IsRearArea(AreaOfInterest aoi) noexcept
{
  return std::any_of(_rearAois.begin(), _rearAois.end(), [aoi](AreaOfInterest rear_aoi)
                     { return aoi == rear_aoi; });
}

bool LaneQueryHelper::IsSideArea(AreaOfInterest aoi) noexcept
{
  return std::any_of(_sideAois.begin(), _sideAois.end(), [aoi](AreaOfInterest side_aoi)
                     { return aoi == side_aoi; });
}

RelativeLane LaneQueryHelper::GetRelativeLaneFromAoi(AreaOfInterest aoi)
{
  if (aoi == AreaOfInterest::LEFTLEFT_SIDE || aoi == AreaOfInterest::LEFTLEFT_FRONT ||
      aoi == AreaOfInterest::LEFTLEFT_REAR)
  {
    return RelativeLane::LEFTLEFT;
  }
  else if (aoi == AreaOfInterest::LEFT_SIDE || aoi == AreaOfInterest::LEFT_FRONT ||
           aoi == AreaOfInterest::LEFT_FRONT_FAR || aoi == AreaOfInterest::LEFT_REAR)
  {
    return RelativeLane::LEFT;
  }
  else if (aoi == AreaOfInterest::RIGHT_SIDE || aoi == AreaOfInterest::RIGHT_FRONT ||
           aoi == AreaOfInterest::RIGHT_FRONT_FAR || aoi == AreaOfInterest::RIGHT_REAR)
  {
    return RelativeLane::RIGHT;
  }
  else if (aoi == AreaOfInterest::RIGHTRIGHT_SIDE || aoi == AreaOfInterest::RIGHTRIGHT_FRONT ||
           aoi == AreaOfInterest::RIGHTRIGHT_REAR)
  {
    return RelativeLane::RIGHTRIGHT;
  }
  else
  {
    throw std::runtime_error("LaneQueryHelper::GetRelativeLaneFromAoi: Invalid AOI!");
  }
}

Side LaneQueryHelper::GetSideFromRelativeLane(RelativeLane relativeLane)
{
  switch (relativeLane)
  {
    case RelativeLane::RIGHT:
    case RelativeLane::RIGHTRIGHT:
      return Side::Right;
    case RelativeLane::LEFT:
    case RelativeLane::LEFTLEFT:
      return Side::Left;
    default:
      throw std::runtime_error("LaneQueryHelper::GetSideFromRelativeLane: There is no assignable Side for ego lane!");
  }
}

bool LaneQueryHelper::IsLaneConsideredAsShoulder(const LaneInformationGeometrySCM& laneInformation) noexcept
{
  return laneInformation.laneType == scm::LaneType::Shoulder || laneInformation.laneType == scm::LaneType::Stop;
}
