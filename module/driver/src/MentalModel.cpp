/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "MentalModel.h"

#include <Stochastics/StochasticsInterface.h>

#include <algorithm>
#include <cmath>
#include <memory>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <string>

#include "ActionImplementation.h"
#include "DesiredVelocityCalculations.h"
#include "FeatureExtractorInterface.h"
#include "GazeControl/GazeCone.h"
#include "GazeControl/GazeParameters.h"
#include "InfrastructureCharacteristicsInterface.h"
#include "LaneQueryHelper.h"
#include "LateralAction.h"
#include "LateralActionQuery.h"
#include "MentalCalculationsInterface.h"
#include "Reliability.h"
#include "ScmCommons.h"
#include "ScmDefinitions.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactory.h"
#include "SurroundingVehicles/SurroundingVehiclesModifier.h"
#include "TrafficFlow/TrafficFlow.h"
#include "include/ScmMathUtils.h"
#include "src/Logging/Logging.h"

MentalModel::MentalModel(units::time::millisecond_t cycleTime, const FeatureExtractorInterface& featureExtractor, MentalCalculationsInterface& mentalCalculations, ScmDependenciesInterface& scmDependencies)
    : _scmDependencies{scmDependencies},
      _cycleTime{static_cast<units::time::millisecond_t>(cycleTime)},
      _featureExtractor{featureExtractor},
      _mentalCalculations{mentalCalculations},
      _microscopicData{nullptr},
      _infrastructureCollection{nullptr}
{
  _microscopicData = std::make_unique<MicroscopicCharacteristics>();
  _infrastructureCollection = std::make_unique<InfrastructureCharacteristics>();
  _highCognitive = std::make_unique<HighCognitive>(_microscopicData.get(), _mentalCalculations, *this, GetDriverParameters().highCognitiveMode);
  _vehicleQueryFactory = std::make_unique<SurroundingVehicleQueryFactory>(*_microscopicData->GetOwnVehicleInformation());
  _merging = std::make_unique<Merging>(*this, *_vehicleQueryFactory, featureExtractor);
  _trafficFlow = std::make_shared<TrafficFlow>(_scmDependencies.GetCycleTime(), _scmDependencies.GetDriverParameters().desiredVelocity);
}

void MentalModel::Initialize(DriverParameters driverParameters, const scm::common::vehicle::properties::EntityProperties& vehicleParameters, units::velocity::meters_per_second_t initVEgo, StochasticsInterface* stochastics)
{
  if (!_isInit)
  {
    _stochastics = stochastics;

    _agentsGeneralInfluencingDistanceToEndOfLane = units::math::max(INFLUENCING_DISTANCE_TO_END_OF_LANE_MIN,
                                                                    units::make_unit<units::length::meter_t>(_stochastics->GetNormalDistributed(INFLUENCING_DISTANCE_TO_END_OF_LANE_MEAN.value(),
                                                                                                                                                INFLUENCING_DISTANCE_TO_END_OF_LANE_SD.value())));

    _microscopicData->Initialize(initVEgo);

    // A driver needs to be anticipating to react proactively e.g. on possible lane changers
    SetIsAnticipating(stochastics->GetUniformDistributed(0, 1) <= GetDriverParameters().anticipationQuota);
    SetOuterKeepingQuotaFulfilled(stochastics->GetUniformDistributed(0, 1) <= GetDriverParameters().outerKeepingQuota);
    SetEgoLaneKeepingQuotaFulfilled(stochastics->GetUniformDistributed(0, 1) <= GetDriverParameters().egoLaneKeepingQuota);

    GetInfrastructureCharacteristics()->Initialize(_cycleTime);
    _merging->DrawNewAccelerationLimits(GetStochastics());

    _isNewEgoLane = false;

    InitializeHesitationProbabilities();

    _isInit = true;
  }
}

int MentalModel::GetMergeRegulateId() const
{
  return _mergeRegulateId;
}

void MentalModel::SetMergeRegulateId(int id)
{
  _mergeRegulateId = id;
}

StochasticsInterface* MentalModel::GetStochastics() const
{
  return _stochastics;
}

void MentalModel::Transition()
{
  _mentalCalculations.TransitionOfEgoVehicle(_currentPerceivedEgoLaneChangeState, _isNewEgoLane);
}

bool MentalModel::CheckForEgoLaneTransition()
{
  _isNewEgoLane = false;

  auto ownVehicle = _scmDependencies.GetOwnVehicleInformationScm();
  const bool triggerNewEgoLane{TriggerNewEgoLane(ownVehicle->mainLaneId, ownVehicle->lateralPosition)};

  if (triggerNewEgoLane)
  {
    _isNewEgoLane = true;
    const bool remainInLateralMovement = GetRemainInLaneChangeState() || GetRemainInSwervingState();
    if (remainInLateralMovement)
    {
      _isLaneChangePastTransition = true;
    }

    _trafficFlow->Reset();
    return true;
  }

  return false;
}

bool MentalModel::IsFovea(AreaOfInterest aoi) const
{
  return GetFovea() == aoi;
}

AreaOfInterest MentalModel::GetFovea() const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->fovea;
}

std::vector<AreaOfInterest> MentalModel::GetUfov() const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->ufov;
}

std::vector<AreaOfInterest> MentalModel::GetPeriphery() const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->periphery;
}

bool MentalModel::GetIsNewEgoLane() const
{
  return _isNewEgoLane;
}

bool MentalModel::GetCollisionState() const
{
  return _microscopicData->GetOwnVehicleInformation()->collision;
}

bool MentalModel::TriggerNewEgoLane(int laneId_GroundTruth, units::length::meter_t lateralPositionInLane_GroundTruth)
{
  bool isChangingLanes = false;
  LaneChangeState newLaneChangeState = LaneChangeState::NoLaneChange;
  const LateralActionQuery lastAction{GetLateralAction()};
  const int laneIdLast{GetLaneId()};
  const bool hasLaneIdChanged{laneId_GroundTruth != laneIdLast};

  if (hasLaneIdChanged)
  {
    GetMicroscopicData()->UpdateOwnVehicleData()->mainLaneId = laneId_GroundTruth;
  }

  if (lastAction.IsLaneChangingRight() || lastAction.IsSwervingRight())
  {
    newLaneChangeState = LaneChangeState::LaneChangeRight;
    isChangingLanes = true;
  }
  else if (lastAction.IsLaneChangingLeft() || lastAction.IsSwervingLeft())
  {
    newLaneChangeState = LaneChangeState::LaneChangeLeft;
    isChangingLanes = true;
  }

  _currentPerceivedEgoLaneChangeState = newLaneChangeState;

  const auto lateralPositionInLaneLast{GetLateralPosition()};
  const bool hasLateralPositionSignChanged{lateralPositionInLaneLast.value() * lateralPositionInLane_GroundTruth < 0._m};

  return isChangingLanes && hasLaneIdChanged && hasLateralPositionSignChanged;
}

bool MentalModel::IsObjectInRearArea(AreaOfInterest aoiReference, AreaOfInterest aoiObserved) const
{
  return MentalModel::IsObjectInSideOrRearArea(aoiReference, aoiObserved, false);
}

bool MentalModel::IsObjectInSideArea(AreaOfInterest aoiReference, AreaOfInterest aoiObserved) const
{
  return MentalModel::IsObjectInSideOrRearArea(aoiReference, aoiObserved, true);
}

bool MentalModel::IsObjectInSideOrRearArea(AreaOfInterest aoiReference, AreaOfInterest aoiObserved, bool isSide) const
{
  if (aoiReference == AreaOfInterest::NumberOfAreaOfInterests)  // Reference is ego
  {
    return isSide ? LaneQueryHelper::IsSideArea(aoiObserved) : LaneQueryHelper::IsRearArea(aoiObserved);
  }
  const int sideIndexReferenceObject = DetermineIndexOfSideObject(aoiReference, SideAoiCriteria::FRONT);
  const int sideIndexObservedObject = DetermineIndexOfSideObject(aoiObserved, SideAoiCriteria::FRONT);
  const auto* surroundingReferenceObject{GetVehicle(aoiReference, sideIndexReferenceObject)};
  // TODO Consider multiple side objects, once workaround for ALL_SEQUENTIAL mechanic is implemented
  const auto* surroundingObservedObject{GetVehicle(aoiObserved, sideIndexObservedObject)};

  if (surroundingReferenceObject == nullptr)
  {
    throw std::runtime_error("MentalModel - IsSideOrRearArea: Reference AOI does not contain an object!");
  }
  if (surroundingObservedObject == nullptr)
  {
    return false;
  }

  const ObstructionLongitudinal obstructionReference{surroundingReferenceObject->GetLongitudinalObstruction()};
  const ObstructionLongitudinal obstructionObserved{surroundingObservedObject->GetLongitudinalObstruction()};
  const auto distanceToFrontOfReference{obstructionReference.mainLaneLocator};
  const auto distanceToRearOfReference{distanceToFrontOfReference - surroundingReferenceObject->GetLength().GetValue()};
  const auto distanceToFrontOfObserved{obstructionObserved.mainLaneLocator};

  const bool isObservedCompletelyBehindReference{distanceToRearOfReference > distanceToFrontOfObserved};

  if (isSide)
  {
    const auto distanceToRearOfObserved{distanceToFrontOfObserved - surroundingObservedObject->GetLength().GetValue()};
    const bool isObservedCompletelyBeforeReference{distanceToFrontOfReference < distanceToRearOfObserved};

    // IsSideArea, if observed object is not totally behind or before reference object
    return !(isObservedCompletelyBehindReference || isObservedCompletelyBeforeReference);
  }

  // IsRearArea, if observed object is not totally behind reference object
  return isObservedCompletelyBehindReference;
}

bool MentalModel::IsObjectInSurroundingArea(AreaOfInterest aoiReference, AreaOfInterest aoiObserved, bool includeSideSideLanes) const
{
  // Aois are inside vehicle
  if (IsInsideVehicleArea(aoiReference) || IsInsideVehicleArea(aoiObserved))
  {
    return false;
  }

  // AoiReference and aoiObserved are the same
  if (aoiReference == aoiObserved)
  {
    // May need to be considered when there is also a differentiation for longitudinal segments (front, side, …) since side aoi can contain multiple objects
    throw std::runtime_error("MentalModel - IsSurroundingArea: Reference and observed aoi can't be identical!");
  }

  // Observed is ego
  if (aoiObserved == AreaOfInterest::NumberOfAreaOfInterests)
  {
    // Observing ego vehicle not implemented yet. Add here if ever necessary.
    throw std::runtime_error("MentalModel – IsSurroundingArea: Observed aoi can't be ego vehicle!");
  }

  // Reference is ego
  if (aoiReference == AreaOfInterest::NumberOfAreaOfInterests)
  {
    const bool isSideSideLane = LaneQueryHelper::IsLeftLeftLane(aoiObserved) || LaneQueryHelper::IsRightRightLane(aoiObserved);
    return includeSideSideLanes ? true : !isSideSideLane;
  }

  // AoiReference and AoiObserved are in surrounding area
  const bool isRightSide{LaneQueryHelper::IsRightLane(aoiObserved) || LaneQueryHelper::IsRightRightLane(aoiObserved)};
  const bool isLeftSide{LaneQueryHelper::IsLeftLane(aoiObserved) || LaneQueryHelper::IsLeftLeftLane(aoiObserved)};
  if (LaneQueryHelper::IsLeftLeftLane(aoiReference))
  {
    return includeSideSideLanes ? !isRightSide : isLeftSide;
  }

  if (LaneQueryHelper::IsLeftLane(aoiReference))
  {
    return includeSideSideLanes ? !LaneQueryHelper::IsRightRightLane(aoiObserved) : !isRightSide;
  }

  if (LaneQueryHelper::IsEgoLane(aoiReference))
  {
    return includeSideSideLanes ? true : (!LaneQueryHelper::IsRightRightLane(aoiObserved) && !LaneQueryHelper::IsLeftLeftLane(aoiObserved));
  }

  if (LaneQueryHelper::IsRightLane(aoiReference))
  {
    return includeSideSideLanes ? !LaneQueryHelper::IsLeftLeftLane(aoiObserved) : !isLeftSide;
  }

  if (LaneQueryHelper::IsRightRightLane(aoiReference))
  {
    return includeSideSideLanes ? !isLeftSide : isRightSide;
  }

  // Should not be possible!
  throw std::runtime_error("MentalModel – IsSurroundingArea : Unknown aoi or lane !");
}

bool MentalModel::IsInsideVehicleArea(AreaOfInterest aoi) const
{
  static constexpr std::array<AreaOfInterest, 3> INSIDE_VEHICLE_AOIS{AreaOfInterest::HUD, AreaOfInterest::INFOTAINMENT, AreaOfInterest::INSTRUMENT_CLUSTER};
  return std::find(INSIDE_VEHICLE_AOIS.cbegin(), INSIDE_VEHICLE_AOIS.cend(), aoi) != INSIDE_VEHICLE_AOIS.end();
}

void MentalModel::ResetInformationRequests()
{
  const auto ufov{GetUfov()};
  const auto periphery{GetPeriphery()};

  // the renewal of a higher level includes the renewal of lower level information requests
  ResetInformationRequestLevel(GetFovea(), FieldOfViewAssignment::FOVEA);

  for (const auto& aoi : ALL_AREAS_OF_INTEREST)
  {
    if (std::find(std::begin(ufov), std::end(ufov), aoi) != std::end(ufov))
    {
      ResetInformationRequestLevel(aoi, FieldOfViewAssignment::UFOV);
    }
  }

  for (const auto& aoi : ALL_AREAS_OF_INTEREST)
  {
    if (std::find(std::begin(periphery), std::end(periphery), aoi) != std::end(periphery))
    {
      ResetInformationRequestLevel(aoi, FieldOfViewAssignment::PERIPHERY);
    }
  }
}

void MentalModel::ResetInformationRequestLevel(AreaOfInterest aoi, FieldOfViewAssignment sufficientViewAssignment)
{
  if (aoi == AreaOfInterest::DISTRACTION)
  {
    return;
  }
  unsigned int currentViewAssignment = static_cast<unsigned int>(sufficientViewAssignment);

  std::vector<InformationRequest> requestsActual{};

  const int sideAoiIndex{DetermineIndexOfSideObject(aoi, SideAoiCriteria::FRONT)};
  if (auto* information{_microscopicData->UpdateObjectInformation(aoi, sideAoiIndex)})
  {
    std::vector<InformationRequest>* requests = &(information->opticalRequest);
    int timeSpanToRemember = 1600;  // 1200;

    if (!requests->empty())
    {
      for (std::vector<InformationRequest>::iterator it = requests->begin(); it != requests->end(); ++it)
      {
        if (!((static_cast<unsigned int>((*it).Request) >= currentViewAssignment) ||          // request is fullfilled
              (GetTime().value() - static_cast<int>((*it).Timestamp) > timeSpanToRemember)))  // request cannot be remembered and is out of date
          requestsActual.push_back((*it));
      }
    }
    _microscopicData->UpdateObjectInformation(aoi, sideAoiIndex)->opticalRequest = requestsActual;
  }
}

void MentalModel::SetTime(units::time::millisecond_t time)
{
  this->time = time;
}

units::time::millisecond_t MentalModel::GetTime() const
{
  return time;
}

double MentalModel::GetRequesterPriority(InformationRequestTrigger trigger) const
{
  if (trigger == InformationRequestTrigger::LATERAL_ACTION ||
      trigger == InformationRequestTrigger::LONGITUDINAL_ACTION)
  {
    return 0.4;
  }

  return 0.;
}

std::vector<double> MentalModel::AddInformationRequest(AreaOfInterest aoi, FieldOfViewAssignment sufficiencyLevel, double priority, InformationRequestTrigger requester) const
{
  // TODO will temporarily only activate lateral or longitudinal action trigger
  // -> this shall secure Lane Changes and Merges to be safe
  // Please activate all InformationRequestTrigger when implementation is complete

  if ((requester == InformationRequestTrigger::LATERAL_ACTION ||
       requester == InformationRequestTrigger::LONGITUDINAL_ACTION) &&
      aoi != AreaOfInterest::NumberOfAreaOfInterests)
  {
    CreateInformationRequest(aoi, sufficiencyLevel, priority, requester);
  }
  else
  {
    // at least report the request
    std::stringstream message;
    message << "Set optical request (" << ScmCommons::AreaOfInterestToString(aoi) << ") :"
            << " Time " << GetTime()
            << " Requester " << static_cast<int>(requester)
            << " Priority " << priority
            << " Sufficiency " << ScmCommons::FieldOfViewAssignmentToString(sufficiencyLevel);
    Logging::Info(message.str());
  }

  return {static_cast<double>(requester), static_cast<double>(sufficiencyLevel), priority};
}

void MentalModel::CreateInformationRequest(AreaOfInterest aoi, FieldOfViewAssignment sufficientViewAssignment, double priority, InformationRequestTrigger trigger) const
{
  InformationRequest infoRequest;

  infoRequest.Timestamp = GetTime();
  infoRequest.RequesterPriority = GetRequesterPriority(trigger);
  infoRequest.Priority = priority;
  infoRequest.Request = sufficientViewAssignment;

  const int sideAoiIndex{DetermineIndexOfSideObject(aoi, SideAoiCriteria::FRONT)};
  if (auto* information{_microscopicData->UpdateObjectInformation(aoi, sideAoiIndex)})
  {
    information->opticalRequest.push_back(infoRequest);
    // sort by 4 conditions
    std::vector<InformationRequest>* requests = &(information->opticalRequest);

    std::sort(requests->begin(), requests->end(), [this](InformationRequest const& a, InformationRequest const& b)
              { return this->CompareInformationRequest(a, b); });

    const int MAX_ALLOWED_REQUESTS{5};  // of remembered information requests per aoi
    if (requests->size() >= MAX_ALLOWED_REQUESTS)
    {
      requests->pop_back();
    }
  }
}

std::map<AreaOfInterest, double> MentalModel::ScaleTopDownRequestMap(std::map<AreaOfInterest, double> map) const
{
  if (!map.empty())
  {
    double maxIntensity = 0.;
    for (auto i1 = map.cbegin(); i1 != map.cend(); ++i1)
    {
      if (i1->second > maxIntensity)
      {
        maxIntensity = i1->second;
      }
    }

    if (maxIntensity > 0.)
    {
      for (auto i2 = map.cbegin(); i2 != map.cend(); ++i2)
      {
        map.at(i2->first) = i2->second / maxIntensity;
      }
    }
    else
    {
      const std::map<AreaOfInterest, double> emptyMap;
      return emptyMap;
    }
  }

  return map;
}

bool MentalModel::CompareInformationRequest(const InformationRequest& a, const InformationRequest& b) const
{
  // first level sort condition
  if (a.Timestamp == b.Timestamp)
  {
    // second level sort condition
    if (a.RequesterPriority == b.RequesterPriority)
    {
      // third level sort condition
      if (static_cast<int>(a.Request) == static_cast<int>(b.Request))
      {
        // fourth level sort condition
        return a.Priority > b.Priority;
      }
      else
      {
        return static_cast<int>(a.Request) < static_cast<int>(b.Request);
      }
    }
    else
    {
      return a.RequesterPriority > b.RequesterPriority;
    }
  }
  else
  {
    return a.Timestamp > b.Timestamp;
  }
}

std::map<AreaOfInterest, double> MentalModel::GenerateTopDownAoiScoring(bool isHafSignalProcessed) const
{
  std::map<AreaOfInterest, double> topDownRequestMap = {};

  for (const auto& aoi : ALL_AREAS_OF_INTEREST)
  {
    if (aoi == AreaOfInterest::DISTRACTION) continue;

    const int sideAoiIndex{DetermineIndexOfSideObject(aoi, SideAoiCriteria::FRONT)};
    double currentScore = 0.;
    if (auto* information{_microscopicData->UpdateObjectInformation(aoi, sideAoiIndex)})
    {
      std::vector<InformationRequest>* const requests = &(information->opticalRequest);
      for (auto request : *requests)
      {
        const double prioRequester = request.RequesterPriority;
        const double prio = request.Priority;
        const double prioViewAssignment = (static_cast<int>(request.Request) == 0) ? 4. : 2.;

        currentScore += prioRequester * prio * prioViewAssignment;
      }
    }
    topDownRequestMap[aoi] = currentScore;
  }

  if (isHafSignalProcessed && GetMicroscopicData()->GetOwnVehicleInformation()->fovea == AreaOfInterest::INSTRUMENT_CLUSTER)
  {
    topDownRequestMap[AreaOfInterest::EGO_FRONT] = 1.0;
  }

  return ScaleTopDownRequestMap(topDownRequestMap);
}

void MentalModel::CalculateMicroscopicData(bool updateLaneMeanVelocities)
{
  // Intention: Not only perceiving data but also triggering a cognitive process and supplying resulting combined data

  // update target velocity of ego vehicle
  SetAbsoluteVelocityTargeted();

  // update mean velocity of observed lanes
  if (updateLaneMeanVelocities)
  {
    _trafficFlow->UpdateMeanVelocities(GetSurroundingVehicles());
  }
}

units::time::millisecond_t MentalModel::GetCycleTime() const
{
  return _cycleTime;
}

LongitudinalActionState MentalModel::GetLongitudinalActionState() const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->longitudinalActionState;
}

void MentalModel::SetLongitudinalActionState(LongitudinalActionState state)
{
  GetMicroscopicData()->UpdateOwnVehicleData()->longitudinalActionState = state;
}

// TODO: We can probably get rid of sideAoiIndex here, since the caller usually has to determine the index beforehand.
// TODO: Therefore if an index was returned before, existence of the target is implicitly true.
bool MentalModel::GetIsVehicleVisible(AreaOfInterest aoi, int sideAoiIndex) const
{
  if (aoi == AreaOfInterest::NumberOfAreaOfInterests)
  {
    return true;
  }

  if ((_aoiMapping.count(aoi) == 0) || ((_aoiMapping.at(aoi).size() + 1) <= sideAoiIndex))
  {
    return false;
  }

  return true;
}

bool MentalModel::GetIsStaticObject(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return false;
  }

  return vehicle->IsStatic();
}

units::time::second_t MentalModel::GetTtc(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return ScmDefinitions::TTC_LIMIT;
  }

  return vehicle->GetTtc().GetValue();
}

units::time::second_t MentalModel::GetGap(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return 99._s;
  }

  return vehicle->GetThw().GetValue();
}

units::length::meter_t MentalModel::GetRelativeNetDistance(AreaOfInterest aoi) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, 0)};
  if (vehicle == nullptr)
  {
    return units::length::meter_t(ScmDefinitions::DEFAULT_VALUE);
  }

  return vehicle->GetRelativeNetDistance().GetValue();
}

units::acceleration::meters_per_second_squared_t MentalModel::GetAccelerationDelta(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, 0)};
  if (vehicle == nullptr)
  {
    return units::make_unit<units::acceleration::meters_per_second_squared_t>(ScmDefinitions::DEFAULT_VALUE);
  }

  // TODO In exisiting implementation, Deltas are always zero in side area, check if this is really necessary
  if (vehicle->ToTheSideOfEgo())
  {
    return 0.0_mps_sq;
  }

  const auto query{_vehicleQueryFactory->GetQuery(*vehicle)};

  // NOTE Delta is now always calculated as EGO - Observed, regardless if approaching or not
  // --> Sign change required to remain compatible with existing implementations
  return vehicle->BehindEgo() ? query->GetLongitudinalAccelerationDelta() : -query->GetLongitudinalAccelerationDelta();
}

units::velocity::meters_per_second_t MentalModel::GetLongitudinalVelocityDelta(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, 0)};
  if (vehicle == nullptr)
  {
    return units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE);
  }

  // TODO In exisiting implementation, Deltas are always zero in side area, check if this is really necessary
  if (vehicle->ToTheSideOfEgo())
  {
    return 0.0_mps;
  }

  const auto query{_vehicleQueryFactory->GetQuery(*vehicle)};

  // NOTE Delta is now always calculated as EGO - Observed, regardless if approaching or not
  // --> Sign change required to remain compatible with existing implementations
  return vehicle->BehindEgo() ? query->GetLongitudinalVelocityDelta() : -query->GetLongitudinalVelocityDelta();
}

units::velocity::meters_per_second_t MentalModel::GetLateralVelocityDelta(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, 0)};
  if (vehicle == nullptr)
  {
    return units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE);
  }

  const auto query{_vehicleQueryFactory->GetQuery(*vehicle)};

  // NOTE Delta is now always calculated as EGO - Observed
  // --> Sign change required to remain compatible with existing implementations
  return -query->GetLateralVelocityDelta();
}

units::velocity::meters_per_second_t MentalModel::GetAbsoluteVelocity(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE);
  }

  return vehicle->GetAbsoluteVelocity().GetValue();
}

units::acceleration::meters_per_second_squared_t MentalModel::GetAcceleration(AreaOfInterest aoi, int sideAoiIndex) const
{
  if (aoi == AreaOfInterest::NumberOfAreaOfInterests)
  {
    return GetAcceleration();
  }
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return units::make_unit<units::acceleration::meters_per_second_squared_t>(ScmDefinitions::DEFAULT_VALUE);
  }

  return vehicle->GetLongitudinalAcceleration().GetValue();
}

units::velocity::meters_per_second_t MentalModel::GetLateralVelocity(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE);
  }

  return vehicle->GetLateralVelocity().GetValue();
}

units::length::meter_t MentalModel::GetDistanceToBoundaryLeft(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return units::length::meter_t(ScmDefinitions::DEFAULT_VALUE);
  }

  return vehicle->GetDistanceToLaneBoundary(Side::Left).GetValue();
}

units::length::meter_t MentalModel::GetDistanceToBoundaryRight(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return units::length::meter_t(ScmDefinitions::DEFAULT_VALUE);
  }

  return vehicle->GetDistanceToLaneBoundary(Side::Right).GetValue();
}

units::length::meter_t MentalModel::GetDistanceToBoundaryLeft() const
{
  return _microscopicData->GetOwnVehicleInformation()->distanceToLaneBoundaryLeft;
}

units::length::meter_t MentalModel::GetDistanceToBoundaryRight() const
{
  return _microscopicData->GetOwnVehicleInformation()->distanceToLaneBoundaryRight;
}

units::length::meter_t MentalModel::GetVehicleLength() const
{
  return GetVehicleModelParameters().bounding_box.dimension.length;
}

units::length::meter_t MentalModel::GetVehicleWidth() const
{
  return GetVehicleModelParameters().bounding_box.dimension.width;
}

scm::common::VehicleClass MentalModel::GetVehicleClassification() const
{
  return GetVehicleModelParameters().classification;
}

units::length::meter_t MentalModel::GetVehicleLength(AreaOfInterest aoi, int sideAoiIndex) const
{
  if (aoi == AreaOfInterest::NumberOfAreaOfInterests)
  {
    return GetVehicleLength();
  }
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return units::length::meter_t(ScmDefinitions::DEFAULT_VALUE);
  }

  return vehicle->GetLength().GetValue();
}

units::length::meter_t MentalModel::GetVehicleWidth(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE);
  }

  return vehicle->GetWidth().GetValue();
}

units::length::meter_t MentalModel::GetVehicleHeight(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE);
  }

  return vehicle->GetHeight().GetValue();
}

scm::common::VehicleClass MentalModel::GetVehicleClassification(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return scm::common::VehicleClass::kInvalid;
  }

  return vehicle->GetVehicleClassification();
}

void MentalModel::SetAgentId(int id)
{
  _microscopicData->UpdateOwnVehicleData()->id = id;
}

int MentalModel::GetAgentId() const
{
  return _microscopicData->GetOwnVehicleInformation()->id;
}

int MentalModel::GetAgentId(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return -1;
  }

  return vehicle->GetId();
}

units::time::second_t MentalModel::GetReactionBaseTime() const
{
  return _reactionBaseTime;
}

void MentalModel::SetReactionBaseTime(units::time::second_t time)
{
  _reactionBaseTime = time;
}

void MentalModel::ResetReactionBaseTime()
{
  _reactionBaseTime = std::clamp(units::make_unit<units::time::second_t>(_stochastics->GetLogNormalDistributed(GetDriverParameters().reactionBaseTimeMean.value(), GetDriverParameters().reactionBaseTimeStandardDeviation.value())),
                                 GetDriverParameters().reactionBaseTimeMinimum,
                                 GetDriverParameters().reactionBaseTimeMaximum) +
                      _cycleTime;
  // An additional amount of 1 cycle time step is added, because the reaction base time is advanced at the beginning
  // of every time step;
  // otherwise, the model would skip 1 amount of cycle time right at the beginning of the next time step.
}

void MentalModel::AdvanceReactionBaseTime()
{
  _reactionBaseTime -= _cycleTime;
}

units::length::meter_t MentalModel::GetCarQueuingDistance() const
{
  return GetDriverParameters().carQueuingDistance;
}

units::length::meter_t MentalModel::GetCarRestartDistance() const
{
  return GetDriverParameters().carRestartDistance;
}

units::length::meter_t MentalModel::GetLateralOffsetNeutralPosition() const
{
  return GetDriverParameters().lateralOffsetNeutralPosition;
}

units::length::meter_t MentalModel::GetLateralOffsetNeutralPositionRescueLane() const
{
  return GetDriverParameters().lateralOffsetNeutralPositionRescueLane;
}

units::velocity::meters_per_second_t MentalModel::GetLateralVelocityTowardsEgoLane(AreaOfInterest aoi, int sideAoiIndex) const
{
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};

  if (vehicle == nullptr)
  {
    return 0.0_mps;
  }

  if (vehicle->GetRelativeLateralPosition().GetValue() > 0.0_m)
  {
    return -vehicle->GetLateralVelocity().GetValue();
  }

  if (vehicle->GetRelativeLateralPosition().GetValue() < 0.0_m)
  {
    return vehicle->GetLateralVelocity().GetValue();
  }

  return 0.0_mps;
}

units::length::meter_t MentalModel::GetDistanceTowardsEgoLane(AreaOfInterest aoi, int sideAoiIndex) const
{
  switch (aoi)
  {
    case AreaOfInterest::RIGHT_FRONT:
    case AreaOfInterest::RIGHT_FRONT_FAR:
    case AreaOfInterest::RIGHT_REAR:
    case AreaOfInterest::RIGHT_SIDE:
      return GetDistanceToBoundaryLeft(aoi, sideAoiIndex);

    case AreaOfInterest::LEFT_FRONT:
    case AreaOfInterest::LEFT_FRONT_FAR:
    case AreaOfInterest::LEFT_REAR:
    case AreaOfInterest::LEFT_SIDE:
      return GetDistanceToBoundaryRight(aoi, sideAoiIndex);

    case AreaOfInterest::LEFTLEFT_FRONT:
    case AreaOfInterest::LEFTLEFT_REAR:
    case AreaOfInterest::LEFTLEFT_SIDE:
      return GetDistanceToBoundaryRight(aoi) + GetInfrastructureCharacteristics()->GetLaneInformationGeometry(RelativeLane::LEFT).width;

    case AreaOfInterest::RIGHTRIGHT_FRONT:
    case AreaOfInterest::RIGHTRIGHT_REAR:
    case AreaOfInterest::RIGHTRIGHT_SIDE:
      return GetDistanceToBoundaryLeft(aoi) + GetInfrastructureCharacteristics()->GetLaneInformationGeometry(RelativeLane::RIGHT).width;

    case AreaOfInterest::EGO_FRONT:
    case AreaOfInterest::EGO_FRONT_FAR:
    case AreaOfInterest::EGO_REAR:
      return 0._m;

    case AreaOfInterest::NumberOfAreaOfInterests:
      return units::make_unit<units::length::meter_t>(ScmDefinitions::DEFAULT_VALUE);

    default:
      throw std::runtime_error("GetExtrapolatedDistanceTowardsEgoLane - Unexpected AreaOfInterest");
  }
}

scm::LightState::Indicator MentalModel::GetIndicatorState(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return scm::LightState::Indicator::Off;
  }

  return vehicle->GetIndicatorState();
}

bool MentalModel::ImplementStochasticActivation(units::time::millisecond_t startTime, units::time::millisecond_t endTime, units::time::millisecond_t durationSinceInitiation, bool hasStateChanged, double probActivation, double maxProb, double minProb, bool activationState) const
{
  const auto diffTimeIndicator = endTime - startTime;

  if (!hasStateChanged || probActivation > maxProb)  // indicator is already activated for the desired direction, or driver won't activate indicator at all
  {
    return activationState;
  }

  activationState = false;

  // indicator activation is only considered after timeToStartIndicator has passed ("reaction time") and only until timeToEndIndicator has passed (else driver is one of the "non-indicators")
  if (durationSinceInitiation >= startTime && durationSinceInitiation <= endTime)
  {
    // Probability threshold for indicator activation in %
    const double probThresholdIndicatorActivation = minProb + ((durationSinceInitiation - startTime) / _cycleTime) *
                                                                  ((maxProb - minProb) / (diffTimeIndicator / _cycleTime));

    if (probActivation <= probThresholdIndicatorActivation)
    {
      activationState = true;
    }
  }

  return activationState;
}

units::time::millisecond_t MentalModel::GetTimeSinceLastUpdate(AreaOfInterest aoi) const
{
  auto lastUpdateFovea{-9999_ms};
  auto lastUpdateUfov{-9999_ms};
  auto lastUpdatePeriphery{-9999_ms};
  auto lastUpdate{-9999_ms};

  if (LaneQueryHelper::IsSideArea(aoi))
  {
    const int sizeSide{static_cast<int>(GetMicroscopicData()->GetSideObjectVector(aoi)->size())};
    for (int sideAoiIndex = 0; sideAoiIndex < sizeSide; ++sideAoiIndex)
    {
      lastUpdateFovea = _microscopicData->GetObjectInformation(aoi, sideAoiIndex)->reliabilityMap.at(FieldOfViewAssignment::FOVEA);
      lastUpdateUfov = _microscopicData->GetObjectInformation(aoi, sideAoiIndex)->reliabilityMap.at(FieldOfViewAssignment::UFOV);
      lastUpdatePeriphery = _microscopicData->GetObjectInformation(aoi, sideAoiIndex)->reliabilityMap.at(FieldOfViewAssignment::PERIPHERY);

      lastUpdate = std::max(lastUpdate, std::max(lastUpdateFovea, std::max(lastUpdateUfov, lastUpdatePeriphery)));
    }
  }
  else if (auto* information{_microscopicData->GetObjectInformation(aoi)})
  {
    lastUpdateFovea = information->reliabilityMap.at(FieldOfViewAssignment::FOVEA);
    lastUpdateUfov = information->reliabilityMap.at(FieldOfViewAssignment::UFOV);
    lastUpdatePeriphery = information->reliabilityMap.at(FieldOfViewAssignment::PERIPHERY);

    lastUpdate = std::max(lastUpdateFovea, std::max(lastUpdateUfov, lastUpdatePeriphery));
  }

  return GetTime() - lastUpdate;
}

bool MentalModel::GetLaneExistence(RelativeLane relativeLane, bool isEmergency) const
{
  const auto geometryInformation = GetInfrastructureCharacteristics()->GetLaneInformationGeometry(relativeLane);

  if (isEmergency)
  {
    return geometryInformation.exists;
  }

  return (geometryInformation.exists &&
          (geometryInformation.laneType == scm::LaneType::Driving ||
           geometryInformation.laneType == scm::LaneType::Entry ||
           geometryInformation.laneType == scm::LaneType::Exit ||
           geometryInformation.laneType == scm::LaneType::OffRamp ||
           geometryInformation.laneType == scm::LaneType::OnRamp));
}

bool MentalModel::GetInteriorHudExistence() const
{
  return false;
}

bool MentalModel::GetInteriorInfotainmentExistence() const
{
  return false;
}

int MentalModel::GetLaneId() const
{
  return _microscopicData->GetOwnVehicleInformation()->mainLaneId;
}

std::map<FieldOfViewAssignment, units::time::millisecond_t> MentalModel::GetReliabilityMap(AreaOfInterest aoi, int sideAoiIndex) const
{
  return _microscopicData->GetObjectInformation(aoi, sideAoiIndex)->reliabilityMap;
}

double MentalModel::GetTauDot(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return ScmDefinitions::DEFAULT_VALUE;
  }

  return vehicle->GetTauDot();
}

Situation MentalModel::GetCurrentSituation() const
{
  return _microscopicData->GetOwnVehicleInformation()->situation;
}

double MentalModel::GetAgentCooperationFactor() const
{
  return GetDriverParameters().agentCooperationFactor;
}

double MentalModel::GetAgentCooperationFactorForSuspiciousBehaviourEvasion() const
{
  return GetDriverParameters().agentSuspiciousBehaviourEvasionFactor;
}

void MentalModel::CheckSituationCooperativeBehavior()
{
  double situationCooperationFactor = _stochastics->GetUniformDistributed(0, 1);

  if (situationCooperationFactor < GetAgentCooperationFactor())
  {
    _cooperativeBehavior = true;
  }
  else
  {
    _cooperativeBehavior = false;
  }
}

void MentalModel::SetCurrentSituation(Situation currentSituation)
{
  if (GetCurrentSituation() != currentSituation)
  {
    _microscopicData->UpdateOwnVehicleData()->hasSituationChanged = true;
  }
  else
  {
    _microscopicData->UpdateOwnVehicleData()->hasSituationChanged = false;
  }

  _microscopicData->UpdateOwnVehicleData()->situation = currentSituation;
}

bool MentalModel::GetHasSituationChanged() const
{
  return _microscopicData->GetOwnVehicleInformation()->hasSituationChanged;
}

bool MentalModel::GetHasMesoscopicSituationChanged() const
{
  return _microscopicData->GetOwnVehicleInformation()->hasMesoscopicSituationChanged;
}

bool MentalModel::HasLeadingVehicleChanged() const
{
  return _leadingVehicleChanged;
}

bool MentalModel::GetIsLateralActionForced() const
{
  const auto forcedLaneChange{_microscopicData->GetOwnVehicleInformation()->currentLaneChangeForced};
  return forcedLaneChange == LaneChangeAction::ForceNegative || forcedLaneChange == LaneChangeAction::ForcePositive;
}

void MentalModel::SetLateralActionAsForced(LaneChangeAction isForced)
{
  _microscopicData->UpdateOwnVehicleData()->currentLaneChangeForced = isForced;
}

bool MentalModel::GetIsLateralMovementStillSafe() const
{
  return _isLateralMovementStillSafe;
}

bool MentalModel::GetIsLaneChangePastTransition() const
{
  return _isLaneChangePastTransition;
}

bool MentalModel::GetIsEndOfLateralMovement() const
{
  return _endOfLateralMovement;
}

void MentalModel::SetIsEndOfLateralMovement(bool end)
{
  _endOfLateralMovement = end;
}

void MentalModel::ReevaluateMergeGap()
{
  auto currentGap = GetMergeGap();
  static constexpr auto MERGEGAP_REEVALUTE_THRESHOLD = 2000.0_ms;  // keep mergegaps min. for 2 seconds
  if (!currentGap.IsValid() || GetTime() - currentGap.timestep <= MERGEGAP_REEVALUTE_THRESHOLD)
  {
    return;
  }

  auto bestGap = _merging->RetrieveMergeGap(currentGap.relativeLane, GetSurroundingVehicles());
  if (!bestGap.IsValid())
  {
    SetMergeGap(Merge::Gap(nullptr, nullptr));
    SetMergeRegulate(AreaOfInterest::NumberOfAreaOfInterests);
    SetMergeRegulateId(-1);
    return;
  }

  SetMergeGap(bestGap);
}

void MentalModel::ReevaluateCurrentLaneChange()
{
  const bool remainInLateralMovement = GetRemainInLaneChangeState() || GetRemainInSwervingState();

  if (!remainInLateralMovement)
  {
    _isLaneChangePastTransition = false;
    _isLateralMovementStillSafe = true;  // reset to initial value
  }
  else  // while lane changing or swerving
  {
    RelativeLane lane{};
    auto laneChangeDirection{GetCurrentDirection()};
    const auto lateralActionQuery{LateralActionQuery(GetLateralAction())};
    if (laneChangeDirection != SurroundingLane::EGO &&
        lateralActionQuery.IsLeavingEgoLane() &&
        !_isLaneChangePastTransition)
    {
      lane = SurroundingLane2RelativeLane.at(laneChangeDirection);
    }
    else
    {
      lane = RelativeLane::EGO;
    }
    if (_mentalCalculations.GetDistanceToPointOfNoReturnForBrakingToEndOfLane(GetAbsoluteVelocityEgo(false), lane) <= 0_m)
    {
      if (!_featureExtractor.HasDrivableSuccessor(true, lane) && !_featureExtractor.IsLaneChangeSafe(_isLaneChangePastTransition))
      {
        _isLateralMovementStillSafe = true;
      }
      else
      {
        _isLateralMovementStillSafe = false;
      }
    }
    else
    {
      _isLateralMovementStillSafe = _featureExtractor.IsLaneChangeSafe(_isLaneChangePastTransition);
    }
    // Check the current lane change for new risks
  }
}

bool MentalModel::GetRemainInSwervingState() const
{
  return _remainInSwervingState;
}

void MentalModel::SetRemainInSwervingState(bool remain)
{
  _remainInSwervingState = remain;
}

void MentalModel::ResetTransitionState()
{
  _isLaneChangePastTransition = false;
}

DriverParameters MentalModel::GetDriverParameters() const
{
  return _scmDependencies.GetDriverParameters();
}

scm::common::vehicle::properties::EntityProperties MentalModel::GetVehicleModelParameters() const
{
  return _scmDependencies.GetVehicleModelParameters();
}

bool MentalModel::GetIsMergeRegulate(AreaOfInterest aoi, int sideAoiIndex) const
{
  const auto vehicle = GetVehicle(aoi, sideAoiIndex);
  if (vehicle == nullptr)
  {
    return false;
  }
  auto vehicleId = vehicle->GetId();
  auto mergeRegulateId = GetMergeRegulateId();
  return vehicleId == mergeRegulateId;
}

void MentalModel::SetIsAnticipating(bool anticipating)
{
  _microscopicData->UpdateOwnVehicleData()->isAnticipating = anticipating;
}

bool MentalModel::GetIsAnticipating() const
{
  return _microscopicData->GetOwnVehicleInformation()->isAnticipating;
}

InfrastructureCharacteristicsInterface* MentalModel::GetInfrastructureCharacteristics() const
{
  return _infrastructureCollection.get();
}

LateralAction MentalModel::GetLateralAction() const
{
  return _microscopicData->GetOwnVehicleInformation()->lateralAction;
}

void MentalModel::SetLateralAction(LateralAction currentLateralAction)
{
  _microscopicData->UpdateOwnVehicleData()->lateralAction = currentLateralAction;
}

void MentalModel::SetDirection(int direction)
{
  if (direction == 0 || direction == -1 || direction == 1)
  {
    _microscopicData->UpdateOwnVehicleData()->direction = direction;
  }
  else
  {
    throw std::runtime_error("MentalModel - SetCurrentDirection: Unexpected direction!");
  }
}

SurroundingLane MentalModel::GetCurrentDirection() const
{
  const auto direction{_microscopicData->GetOwnVehicleInformation()->direction};
  if (direction == 0) return SurroundingLane::EGO;
  if (direction == 1) return SurroundingLane::LEFT;
  return SurroundingLane::RIGHT;
}

void MentalModel::TriggerHighCognitive(bool externalControlActive)
{
  _highCognitive->AdvanceHighCognitive(GetTime().value(), externalControlActive);
}

HighCognitiveSituation MentalModel::GetHighCognitiveSituation() const
{
  return _highCognitive->GetCurrentSituation(GetTime().value());
}

units::time::millisecond_t MentalModel::GetDurationCurrentSituation() const
{
  return _durationCurrentSituation;
}

void MentalModel::SetDurationCurrentSituation(units::time::millisecond_t durationCurrentSituation)
{
  _durationCurrentSituation = durationCurrentSituation;
}

void MentalModel::InitializeHesitationProbabilities()
{
  _probIndicatorActivation = _stochastics->GetUniformDistributed(0, 100);
  _probFlasherActivation = _stochastics->GetUniformDistributed(0, 100);
}

double MentalModel::GetIndicatorActiviationProbability() const
{
  return _probIndicatorActivation;
}

double MentalModel::GetFlasherActiviationProbability() const
{
  return _probFlasherActivation;
}

bool MentalModel::IsLeadingVehicleAnticipatedLaneChanger() const
{
  Situation currentSituation = GetCurrentSituation();
  const auto leadingVehicle{GetLeadingVehicle()};

  return (leadingVehicle != nullptr &&
          ((leadingVehicle->GetAssignedAoi() == AreaOfInterest::LEFT_FRONT &&
            (currentSituation == Situation::LANE_CHANGER_FROM_LEFT && IsSituationAnticipated(Situation::LANE_CHANGER_FROM_LEFT))) ||
           (leadingVehicle->GetAssignedAoi() == AreaOfInterest::RIGHT_FRONT &&
            (currentSituation == Situation::LANE_CHANGER_FROM_RIGHT && IsSituationAnticipated(Situation::LANE_CHANGER_FROM_RIGHT)))));
}

bool MentalModel::IsAbleToCooperate() const
{
  if (_cooperativeBehavior && IsLeadingVehicleAnticipatedLaneChanger())
  {
    return true;
  }

  return false;
}

bool MentalModel::IsAbleToCooperateByAccelerating() const
{
  const auto leadingVehicle{GetLeadingVehicle()};
  if (leadingVehicle == nullptr)
  {
    return false;
  }

  const auto possibleDeltaVelocity{units::math::abs(leadingVehicle->GetLongitudinalVelocity().GetValue()) - GetAbsoluteVelocityTargeted()};  // Approaching
  const auto aoi{leadingVehicle->GetAssignedAoi()};
  const auto sideAoiIndex{DetermineIndexOfSideObject(aoi,
                                                     SideAoiCriteria::MIN,
                                                     [](const SurroundingVehicleInterface* vehicle)
                                                     { return std::abs(vehicle->GetRelativeLateralPosition().GetValue().value()); })};
  // Is able to pass anticipated lane changer with future vTarget and future delta velocity
  if (IsAbleToCooperate() && _featureExtractor.AnticipatedAbleToPass(leadingVehicle, possibleDeltaVelocity))
  {
    if (GetIsVehicleVisible(AreaOfInterest::EGO_FRONT))
    {
      if (GetRelativeNetDistance(AreaOfInterest::EGO_FRONT) <
          (_mentalCalculations.GetMinDistance(AreaOfInterest::EGO_FRONT) +
           GetVehicleLength() + leadingVehicle->GetLength().GetValue() + leadingVehicle->GetRelativeLongitudinalPosition().GetValue()))
      {
        return false;  // no acceleration because EGO_FRONT is close
      }

      return true;  // accelerate to cooperate with anticipated lane changer, EGO_FRONT is far enough away
    }

    return true;  // accelerate to cooperate with anticipated lane changer, no EGO_FRONT
  }

  return false;  // ego is not cooperative, aoiRegulate is not an anticipated lane changer or target velocity is not sufficient to overtake reasonably
}

void MentalModel::SetMergeRegulate(AreaOfInterest mergeRegulate)
{
  SurroundingVehiclesModifier surroundingVehiclesModifier(_surroundingVehicles);
  for (auto* vehicle : surroundingVehiclesModifier.Update())
  {
    if (vehicle->GetAssignedAoi() == mergeRegulate)
    {
      SetMergeRegulateId(vehicle->GetId());
      return;
    }
  }
  SetMergeRegulateId(-1);
}

AreaOfInterest MentalModel::GetMergeRegulate() const
{
  const auto vehicle = GetVehicle(GetMergeRegulateId());
  if (vehicle != nullptr)
  {
    return vehicle->GetAssignedAoi();
  }
  return AreaOfInterest::NumberOfAreaOfInterests;
}

void MentalModel::ResetMergeRegulate()
{
  SetMergeRegulateId(-1);
  SetMergeGap(Merge::Gap(nullptr, nullptr));
}

bool MentalModel::IsMergePreparationActive() const
{
  return GetMergeGap().IsValid();
}

bool MentalModel::IsSideLaneSafeWithMergePreparation(AreaOfInterest aoi)
{
  if (!_mergeGap.IsValid() || (_mergeGap.IsValid() && GetTime() - _mergeGap.timestep >= 2000_ms))
  {
    auto relativeLane = LaneQueryHelper::GetRelativeLaneFromAoi(aoi);
    if (!GetLaneExistence(relativeLane, true) || !_featureExtractor.IsLaneDriveablePerceived(relativeLane) || !IsDataForLaneChangeReliable(ScmCommons::RelativeLaneToSide(relativeLane)))
    {
      return false;
    }

    auto bestGap = _merging->RetrieveMergeGap(relativeLane, GetSurroundingVehicles());
    if (!bestGap.IsValid())
    {
      return false;
    }

    SetMergeGap(bestGap);
    return true;
  }

  return _mergeGap.IsValid();
}

units::length::meter_t MentalModel::GetPreviewDistance() const
{
  return GetDriverParameters().previewDistance;
}

units::velocity::meters_per_second_t MentalModel::GetMeanVelocityLaneLeft() const
{
  return _trafficFlow->GetMeanVelocity(SurroundingLane::LEFT);
}

units::velocity::meters_per_second_t MentalModel::GetMeanVelocityLaneEgo() const
{
  return _trafficFlow->GetMeanVelocity(SurroundingLane::EGO);
}

units::velocity::meters_per_second_t MentalModel::GetMeanVelocityLaneRight() const
{
  return _trafficFlow->GetMeanVelocity(SurroundingLane::RIGHT);
}

units::velocity::meters_per_second_t MentalModel::GetMeanVelocity(SurroundingLane lane) const
{
  if (lane == SurroundingLane::LEFT)
  {
    return GetMeanVelocityLaneLeft();
  }
  if (lane == SurroundingLane::EGO)
  {
    return GetMeanVelocityLaneEgo();
  }
  if (lane == SurroundingLane::RIGHT)
  {
    return GetMeanVelocityLaneRight();
  }
  throw std::runtime_error("GetMeanVelocity called with invalid SurroundingLane lane");
}

bool MentalModel::IsLateWithFrontUpdate() const
{
  return GetTimeSinceLastUpdate(AreaOfInterest::EGO_FRONT) > CRITICAL_UPDATE_TIME_EGO_FRONT;
}

bool MentalModel::IsUrgentlyLateWithUpdate(GazeState gs) const
{
  AreaOfInterest aoi = ScmCommons::MapGazeStateToAreaOfInterest(gs);

  return GetTimeSinceLastUpdate(aoi) > _urgentUpdateThreshold;
}

bool MentalModel::IsUrgentlyLateWithUpdate(AreaOfInterest aoi) const
{
  return GetTimeSinceLastUpdate(aoi) > _urgentUpdateThreshold;
}

units::time::millisecond_t MentalModel::GetUrgentUpdateThreshold() const
{
  return _urgentUpdateThreshold;
}

void MentalModel::SetUrgentUpdateThreshold(units::time::millisecond_t threshold)
{
  _urgentUpdateThreshold = threshold;
}

bool MentalModel::IsInconsistent(AreaOfInterest aoi) const
{
  return std::find(begin(_inconsistentAois), end(_inconsistentAois), aoi) != end(_inconsistentAois);
}

void MentalModel::MarkInconsistent(AreaOfInterest aoi)
{
  if (!IsInconsistent(aoi))
  {
    _inconsistentAois.push_back(aoi);
  }
}

void MentalModel::ClearInconsistency(AreaOfInterest aoi)
{
  _inconsistentAois.erase(std::remove(begin(_inconsistentAois), end(_inconsistentAois), aoi), end(_inconsistentAois));
}

const std::vector<AreaOfInterest>& MentalModel::GetInconsistentAois() const
{
  return _inconsistentAois;
}

bool MentalModel::IsEarlyWithUpdate(GazeState gs) const
{
  AreaOfInterest aoi = ScmCommons::MapGazeStateToAreaOfInterest(gs);
  return GetTimeSinceLastUpdate(aoi) < RECENT_UPDATE_THRESHOLD;
}

bool MentalModel::IsEarlyWithUpdate(AreaOfInterest aoi) const
{
  return GetTimeSinceLastUpdate(aoi) < RECENT_UPDATE_THRESHOLD;
}

double MentalModel::GetPrioritySinceLastUpdate(GazeState gs) const
{
  AreaOfInterest aoi = ScmCommons::MapGazeStateToAreaOfInterest(gs);
  return GetTimeSinceLastUpdate(aoi) / CRITICAL_UPDATE_TIME_EGO_FRONT;
}

void MentalModel::UpdateReliabilityMap()
{
  AreaOfInterest aoi = GetFovea();
  if (LaneQueryHelper::IsSideArea(aoi))
  {
    for (unsigned int i = 0; i < GetMicroscopicData()->GetSideObjectVector(aoi)->size(); ++i)
    {
      _microscopicData->UpdateObjectInformation(aoi, i)->reliabilityMap.at(FieldOfViewAssignment::FOVEA) = GetTime();
    }
  }
  else if (auto* information{_microscopicData->UpdateObjectInformation(aoi)})
  {
    information->reliabilityMap.at(FieldOfViewAssignment::FOVEA) = GetTime();
  }

  for (AreaOfInterest aoi : GetUfov())
  {
    if (LaneQueryHelper::IsSideArea(aoi))
    {
      for (unsigned int i = 0; i < GetMicroscopicData()->GetSideObjectVector(aoi)->size(); ++i)
      {
        auto* information{_microscopicData->UpdateObjectInformation(aoi, i)};
        information->reliabilityMap.at(FieldOfViewAssignment::UFOV) = GetTime();
      }
    }
    else if (auto* information{_microscopicData->UpdateObjectInformation(aoi)})
    {
      information->reliabilityMap.at(FieldOfViewAssignment::UFOV) = GetTime();
    }
  }

  for (AreaOfInterest aoi : GetPeriphery())
  {
    if (LaneQueryHelper::IsSideArea(aoi))
    {
      for (unsigned int i = 0; i < GetMicroscopicData()->GetSideObjectVector(aoi)->size(); ++i)
      {
        auto* information{_microscopicData->UpdateObjectInformation(aoi, i)};
        information->reliabilityMap.at(FieldOfViewAssignment::PERIPHERY) = GetTime();
      }
    }
    else if (auto* information{_microscopicData->UpdateObjectInformation(aoi)})
    {
      information->reliabilityMap.at(FieldOfViewAssignment::PERIPHERY) = GetTime();
    }
  }

  if (GetDriverParameters().idealPerception)
  {
    std::vector<AreaOfInterest> aois = {AreaOfInterest::LEFT_FRONT,
                                        AreaOfInterest::LEFT_FRONT_FAR,
                                        AreaOfInterest::RIGHT_FRONT,
                                        AreaOfInterest::RIGHT_FRONT_FAR,
                                        AreaOfInterest::LEFT_REAR,
                                        AreaOfInterest::RIGHT_REAR,
                                        AreaOfInterest::EGO_FRONT,
                                        AreaOfInterest::EGO_FRONT_FAR,
                                        AreaOfInterest::EGO_REAR,
                                        AreaOfInterest::LEFT_SIDE,
                                        AreaOfInterest::RIGHT_SIDE,
                                        AreaOfInterest::INSTRUMENT_CLUSTER,
                                        AreaOfInterest::INFOTAINMENT,
                                        AreaOfInterest::HUD,
                                        AreaOfInterest::LEFTLEFT_FRONT,
                                        AreaOfInterest::LEFTLEFT_SIDE,
                                        AreaOfInterest::LEFTLEFT_REAR,
                                        AreaOfInterest::RIGHTRIGHT_FRONT,
                                        AreaOfInterest::RIGHTRIGHT_SIDE,
                                        AreaOfInterest::RIGHTRIGHT_REAR};
    for (AreaOfInterest aoi : aois)
    {
      if (LaneQueryHelper::IsSideArea(aoi))
      {
        for (unsigned int i = 0; i < GetMicroscopicData()->GetSideObjectVector(aoi)->size(); ++i)
        {
          auto* information{_microscopicData->UpdateObjectInformation(aoi, i)};
          information->reliabilityMap.at(FieldOfViewAssignment::FOVEA) = GetTime();
        }
      }
      else if (auto* information{_microscopicData->UpdateObjectInformation(aoi)})
      {
        information->reliabilityMap.at(FieldOfViewAssignment::FOVEA) = GetTime();
      }
    }
  }
}

bool MentalModel::IsDataForLaneChangeReliable(Side side) const
{
  LateralActionQuery actionQuery{GetLateralAction()};
  const bool stillInIntent = actionQuery.IsLaneKeepingRightActions() || actionQuery.IsLaneKeepingLeftActions();
  const bool changeToLeftLane{side == Side::Left};

  const auto frontAoi{changeToLeftLane ? AreaOfInterest::LEFT_FRONT : AreaOfInterest::RIGHT_FRONT};
  const auto sideAoi{changeToLeftLane ? AreaOfInterest::LEFT_SIDE : AreaOfInterest::RIGHT_SIDE};
  const auto rearAoi{changeToLeftLane ? AreaOfInterest::LEFT_REAR : AreaOfInterest::RIGHT_REAR};
  const auto sideSector{changeToLeftLane ? VisualPerceptionSector::LEFT : VisualPerceptionSector::RIGHT};

  const auto* frontVehicle{GetVehicle(frontAoi)};
  const auto* sideVehicle{GetVehicle(sideAoi)};
  const auto* rearVehicle{GetVehicle(rearAoi)};
  // TODO Thomas: Visual reliability currently disabled since lane change is often never started
  // More analysis required
  bool frontReliable{frontVehicle != nullptr ? frontVehicle->IsReliable(DataQuality::MEDIUM) : true};
  bool sideReliable{sideVehicle != nullptr ? sideVehicle->IsReliable(DataQuality::LOW) : true};
  bool rearReliable{rearVehicle != nullptr ? rearVehicle->IsReliable(DataQuality::HIGH) : true};

  if (IsInconsistent(frontAoi))
  {
    frontReliable = false;
  }

  if (IsInconsistent(sideAoi))
  {
    sideReliable = false;
  }

  if (IsInconsistent(rearAoi))
  {
    rearReliable = false;
  }

  if (!stillInIntent)
  {
    rearReliable = true;
  }

  if (!frontReliable)
  {
    AddInformationRequest(frontAoi, FieldOfViewAssignment::UFOV, 1.0, InformationRequestTrigger::LATERAL_ACTION);
  }

  if (!sideReliable)
  {
    AddInformationRequest(sideAoi, FieldOfViewAssignment::PERIPHERY, 1.0, InformationRequestTrigger::LATERAL_ACTION);
  }

  if (!rearReliable)
  {
    AddInformationRequest(rearAoi, FieldOfViewAssignment::FOVEA, 1.0, InformationRequestTrigger::LATERAL_ACTION);
  }

  return frontReliable && sideReliable && rearReliable;
}

bool MentalModel::IsSideSideLaneObjectChangingIntoSide(RelativeLane relativeLane, bool merge) const
{
  if (relativeLane == RelativeLane::LEFT && !_infrastructureCollection->CheckLaneExistence(AreaOfInterest::LEFTLEFT_SIDE))
  {
    return false;
  }
  else if (relativeLane == RelativeLane::RIGHT && !_infrastructureCollection->CheckLaneExistence(AreaOfInterest::RIGHTRIGHT_SIDE))
  {
    return false;
  }

  AreaOfInterest aoiSideSideFront = AreaOfInterest::NumberOfAreaOfInterests;
  AreaOfInterest aoiSideSideSide = AreaOfInterest::NumberOfAreaOfInterests;
  AreaOfInterest aoiSideSideRear = AreaOfInterest::NumberOfAreaOfInterests;
  scm::LightState::Indicator relevantIndicatorState = scm::LightState::Indicator::Off;

  if (relativeLane == RelativeLane::LEFT)
  {
    aoiSideSideFront = AreaOfInterest::LEFTLEFT_FRONT;
    aoiSideSideSide = AreaOfInterest::LEFTLEFT_SIDE;
    aoiSideSideRear = AreaOfInterest::LEFTLEFT_REAR;
    relevantIndicatorState = scm::LightState::Indicator::Right;
  }
  else
  {
    aoiSideSideFront = AreaOfInterest::RIGHTRIGHT_FRONT;
    aoiSideSideSide = AreaOfInterest::RIGHTRIGHT_SIDE;
    aoiSideSideRear = AreaOfInterest::RIGHTRIGHT_REAR;
    relevantIndicatorState = scm::LightState::Indicator::Left;
  }

  const int sideAoiIndex{DetermineIndexOfSideObject(aoiSideSideSide, SideAoiCriteria::FRONT)};

  // Is Vehicle already moving towards side lane and critically close
  if ((_featureExtractor.IsExceedingLateralMotionThresholdTowardsEgoLane(GetVehicle(aoiSideSideFront)) &&
       _featureExtractor.IsMinimumFollowingDistanceViolated(GetVehicle(aoiSideSideFront))) ||
      (_featureExtractor.IsExceedingLateralMotionThresholdTowardsEgoLane(GetVehicle(aoiSideSideSide, sideAoiIndex)) &&
       _featureExtractor.IsMinimumFollowingDistanceViolated(GetVehicle(aoiSideSideSide))) ||
      (_featureExtractor.IsExceedingLateralMotionThresholdTowardsEgoLane(GetVehicle(aoiSideSideRear)) &&
       _featureExtractor.IsMinimumFollowingDistanceViolated(GetVehicle(aoiSideSideRear))))
  {
    return true;
  }

  if (!merge)
  {
    // Does Vehicle have its inidicator set and is his reason to change into the side lane higher than mine
    double urgencyFactorForLaneChange = _mentalCalculations.GetUrgencyFactorForLaneChange();
    if ((_mentalCalculations.GetIntensityForNeedToChangeLane(aoiSideSideFront) > urgencyFactorForLaneChange &&
         GetIndicatorState(aoiSideSideFront) == relevantIndicatorState &&
         _featureExtractor.IsMinimumFollowingDistanceViolated(GetVehicle(aoiSideSideFront))) ||
        (_mentalCalculations.GetIntensityForNeedToChangeLane(aoiSideSideSide) > urgencyFactorForLaneChange &&
         GetIndicatorState(aoiSideSideSide, sideAoiIndex) == relevantIndicatorState &&
         _featureExtractor.IsMinimumFollowingDistanceViolated(GetVehicle(aoiSideSideSide))) ||
        (_mentalCalculations.GetIntensityForNeedToChangeLane(aoiSideSideRear) > urgencyFactorForLaneChange &&
         GetIndicatorState(aoiSideSideRear) == relevantIndicatorState &&
         _featureExtractor.IsMinimumFollowingDistanceViolated(GetVehicle(aoiSideSideRear))))
    {
      return true;
    }
  }

  return false;
}

units::length::meter_t MentalModel::GetLateralPosition() const
{
  return _microscopicData->GetOwnVehicleInformation()->lateralPosition;
}

units::length::meter_t MentalModel::GetLateralPositionFrontAxle() const
{
  return _microscopicData->GetOwnVehicleInformation()->lateralPositionFrontAxle;
}

units::velocity::meters_per_second_t MentalModel::GetAbsoluteVelocityEgo(bool isEstimatedValue) const
{
  if (isEstimatedValue)
  {
    return _microscopicData->GetOwnVehicleInformation()->absoluteVelocity;
  }
  else
  {
    return _microscopicData->GetOwnVehicleInformation()->absoluteVelocityReal;
  }
}

units::velocity::meters_per_second_t MentalModel::GetLateralVelocity() const
{
  return _microscopicData->GetOwnVehicleInformation()->lateralVelocity;
}

units::velocity::meters_per_second_t MentalModel::GetLateralVelocityFrontAxle() const
{
  return _microscopicData->GetOwnVehicleInformation()->lateralVelocityFrontAxle;
}

void MentalModel::SetAbsoluteVelocityTargeted()
{
  DesiredVelocityCalculations::TargetVelocityParameters params{
      DetermineVelocityReason(),
      GetDriverParameters().desiredVelocity,
      _mentalCalculations.CalculateDeltaVelocityWish(),
      GetVehicleModelParameters().performance.max_speed,
      GetLegalVelocity(SurroundingLane::EGO),
      GetDriverParameters().amountOfSpeedLimitViolation};

  _microscopicData->UpdateOwnVehicleData()->absoluteVelocityTargeted = DesiredVelocityCalculations::CalculateTargetVelocity(params);
}

units::velocity::meters_per_second_t MentalModel::GetAbsoluteVelocityTargeted() const
{
  return _microscopicData->GetOwnVehicleInformation()->absoluteVelocityTargeted;
}

units::velocity::meters_per_second_t MentalModel::GetDesiredVelocity() const
{
  LaneChangeDimension laneChangeDimension(*this);
  const bool approachingExit{GetDistanceToEndOfNextExit() > 0._m && _mentalCalculations.DetermineUrgencyFactorForNextExit(false, laneChangeDimension) > 0.};

  if (!approachingExit)
  {
    return GetDriverParameters().desiredVelocity;
  }
  else
  {
    return units::math::fmin(GetDriverParameters().desiredVelocity, units::velocity::meters_per_second_t(130_kph));
  }
}

units::acceleration::meters_per_second_squared_t MentalModel::GetAcceleration() const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->acceleration;
}

units::velocity::meters_per_second_t MentalModel::GetLegalVelocity(SurroundingLane lane) const
{
  const auto relativeLane{SurroundingLane2RelativeLane.at(lane)};
  const auto trafficRuleInformation{GetInfrastructureCharacteristics()->GetLaneInformationTrafficRules(relativeLane)};

  if (!trafficRuleInformation.signDetected)
  {
    return trafficRuleInformation.speedLimit;
  }

  const auto influencingDistance{_mentalCalculations.CalculateVLegalInfluencigDistance(
      GetAbsoluteVelocityEgo(true),
      trafficRuleInformation.speedLimit)};

  const auto relativeDistanceOfSign{trafficRuleInformation.relativeDistanceToTrafficsign};

  if (relativeDistanceOfSign - influencingDistance < 0._m)
  {
    return trafficRuleInformation.speedLimit;
  }

  return trafficRuleInformation.previousRelevantSpeedLimit;
}

units::velocity::meters_per_second_t MentalModel::GetVelocityLegalLeft() const
{
  return GetLegalVelocity(SurroundingLane::LEFT);
}

units::velocity::meters_per_second_t MentalModel::GetVelocityLegalEgo() const
{
  return GetLegalVelocity(SurroundingLane::EGO);
}

units::velocity::meters_per_second_t MentalModel::GetVelocityLegalRight() const
{
  return GetLegalVelocity(SurroundingLane::RIGHT);
}

units::velocity::meters_per_second_t MentalModel::GetLongitudinalVelocityEgo() const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->longitudinalVelocity;
}

units::velocity::meters_per_second_t MentalModel::GetLongitudinalVelocity(AreaOfInterest aoi, int sideAoiIndex) const
{
  if (aoi == AreaOfInterest::NumberOfAreaOfInterests)
  {
    return GetLongitudinalVelocityEgo();
  }

  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE);
  }

  return vehicle->GetLongitudinalVelocity();
}

units::angle::radian_t MentalModel::GetHeading() const
{
  return _microscopicData->GetOwnVehicleInformation()->heading;
}

units::length::meter_t MentalModel::GetEgoLaneWidth() const
{
  return GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().width;
}

units::curvature::inverse_meter_t MentalModel::GetCurvatureOnCurrentPosition() const
{
  return GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().curvature;
}

units::curvature::inverse_meter_t MentalModel::GetCurvatureInPreviewDistance() const
{
  return GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().curvatureInPreviewDistance;
}

units::length::meter_t MentalModel::GetDistanceToEndOfLane(RelativeLane relativeLane, bool isEmergency) const
{
  switch (relativeLane)
  {
    case RelativeLane::EGO:
    {
      return GetDistanceToEndOfLane(0, isEmergency);
    }
    case RelativeLane::LEFT:
    {
      return GetDistanceToEndOfLane(1, isEmergency);
    }
    case RelativeLane::RIGHT:
    {
      return GetDistanceToEndOfLane(-1, isEmergency);
    }
    default:
      return ScmDefinitions::INF_DISTANCE;
  }
}

units::length::meter_t MentalModel::GetDistanceToEndOfLane(int relativeLaneId, bool isEmergency) const
{
  auto distToEndOfLane{ScmDefinitions::INF_DISTANCE};

  switch (relativeLaneId)
  {
    case 0:  // Ego
      distToEndOfLane = isEmergency ? GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().distanceToEndOfLaneDuringEmergency : GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().distanceToEndOfLane;
      break;

    case -1:  // Right
      distToEndOfLane = isEmergency ? GetInfrastructureCharacteristics()->GetGeometryInformation()->Right().distanceToEndOfLaneDuringEmergency : GetInfrastructureCharacteristics()->GetGeometryInformation()->Right().distanceToEndOfLane;
      break;

    case 1:  // Left
      distToEndOfLane = isEmergency ? GetInfrastructureCharacteristics()->GetGeometryInformation()->Left().distanceToEndOfLaneDuringEmergency : GetInfrastructureCharacteristics()->GetGeometryInformation()->Left().distanceToEndOfLane;
      break;

    default:
      break;
  }

  return distToEndOfLane;
}

units::length::meter_t MentalModel::GetDistanceToEndOfNextExit() const
{
  return GetInfrastructureCharacteristics()->GetGeometryInformation()->distanceToEndOfNextExit;
}

units::length::meter_t MentalModel::GetDistanceToStartOfNextExit() const
{
  return GetInfrastructureCharacteristics()->GetGeometryInformation()->distanceToStartOfNextExit;
}

LaneInformationTrafficRulesScmExtended MentalModel::ResetSpeedLimitSign() const
{
  LaneInformationTrafficRulesScmExtended lane;
  lane.speedLimit = ScmDefinitions::INF_VELOCITY;
  lane.relativeDistanceToTrafficsign = 999.0_m;
  lane.timeOfLastDetection = 0.0_ms;
  lane.speedWhenDetected = GetAbsoluteVelocityEgo(true);
  lane.previousRelevantSpeedLimit = ScmDefinitions::INF_VELOCITY;
  lane.signDetected = false;
  return lane;
}

void MentalModel::CalculateVLegal(bool spawn)
{
  TrafficRuleInformationScmExtended* trafficRules = GetInfrastructureCharacteristics()->UpdateTrafficRuleInformation();
  const bool hasLeftLane = GetLaneExistence(RelativeLane::LEFT);
  const bool hasRightLane = GetLaneExistence(RelativeLane::RIGHT);

  if (!hasLeftLane)
  {
    trafficRules->Left() = ResetSpeedLimitSign();
  }

  if (!hasRightLane)
  {
    trafficRules->Right() = ResetSpeedLimitSign();
  }

  const auto resultCalculateVLegal = _mentalCalculations.CalculateVLegal(spawn);

  SetRelevantSpeedLimitForLane(resultCalculateVLegal.at(0), trafficRules->Left());
  SetRelevantSpeedLimitForLane(resultCalculateVLegal.at(1), trafficRules->Close());
  SetRelevantSpeedLimitForLane(resultCalculateVLegal.at(2), trafficRules->Right());
}

void MentalModel::SetRelevantSpeedLimitForLane(LaneInformationTrafficRulesScmExtended in, LaneInformationTrafficRulesScmExtended& out)
{
  auto relevantVelocityForDistanceExtrapolation{GetAbsoluteVelocityEgo(true)};
  auto timeSinceLastTrafficSignDetection{0._s};

  relevantVelocityForDistanceExtrapolation = units::math::fmax(relevantVelocityForDistanceExtrapolation,
                                                               in.speedWhenDetected);
  timeSinceLastTrafficSignDetection = (time - in.timeOfLastDetection);

  if ((in.relativeDistanceToTrafficsign -
       relevantVelocityForDistanceExtrapolation * timeSinceLastTrafficSignDetection) < 0._m)
  {
    out.previousRelevantSpeedLimit = in.speedLimit;
  }
  out.speedLimit = in.speedLimit;
  out.relativeDistanceToTrafficsign = in.relativeDistanceToTrafficsign;
  out.timeOfLastDetection = in.timeOfLastDetection;
  out.speedWhenDetected = in.speedWhenDetected;
  out.signDetected = in.signDetected;
}

MicroscopicCharacteristicsInterface* MentalModel::GetMicroscopicData() const
{
  return _microscopicData.get();
}

units::velocity::meters_per_second_t MentalModel::GetVelocityViolationDelta() const
{
  return GetDriverParameters().amountOfSpeedLimitViolation;
}
// TODO: rename to UpdateEndOfLaneChange or similar
// TODO: fix case of argument names
// TODO: rename TrajectoryPlanningCompleted, it only receives "LateralDisplacementReached", which indicates that the
//       lateral distance of a lane change has been completely covered
void MentalModel::CheckForEndOfLaneChange(units::length::meter_t LaneChangeWidth, units::velocity::meters_per_second_t LateralVelocity, bool TrajectoryPlanningCompleted)
{
  SetRemainInRealignState(true);

  const LateralActionQuery actionState{GetLateralAction()};

  if (!GetIsLaneChangePastTransition())
  {
    _isInTargetLane = false;
  }

  if (actionState.IsLaneChangingLeft())
  {
    _laneChangeWidthTraveled = DetermineDistanceTraveledLeft(LaneChangeWidth);
  }

  if (actionState.IsLaneChangingRight())
  {
    _laneChangeWidthTraveled = DetermineDistanceTraveledRight(LaneChangeWidth);
  }

  if (HasEgoLateralDistanceReached(LaneChangeWidth, _laneChangeWidthTraveled) && TrajectoryPlanningCompleted)
  {
    ResetLaneChangeFlags();

    // reset the merge state
    ResetMergeRegulate();

    // action needs to evaluated again, though reactionBaseTime might not be done
    _reactionBaseTime = 0._s;

    _isInTargetLane = false;
  }
}

bool MentalModel::HasEgoLateralDistanceReached(units::length::meter_t desiredLateralDisplacement, units::length::meter_t lateralPositionSet) const
{
  return (desiredLateralDisplacement > 0._m && lateralPositionSet >= .95 * desiredLateralDisplacement) ||
         (desiredLateralDisplacement < 0._m && lateralPositionSet <= .95 * desiredLateralDisplacement);
}

units::length::meter_t MentalModel::DetermineDistanceTraveledLeft(units::length::meter_t laneChangeWidth)
{
  if (GetIsNewEgoLane())
  {
    _isInTargetLane = true;
  }

  if (_isInTargetLane)
  {
    auto distanceTraveled = laneChangeWidth - units::math::abs(GetLateralPositionFrontAxle());
    return distanceTraveled;
  }
  else
  {
    return GetLateralPositionFrontAxle();
  }
}

units::length::meter_t MentalModel::DetermineDistanceTraveledRight(units::length::meter_t laneChangeWidth)
{
  if (GetIsNewEgoLane())
  {
    _isInTargetLane = true;
  }

  if (_isInTargetLane)
  {
    auto distanceTraveled = units::math::abs(laneChangeWidth) - units::math::abs(GetLateralPositionFrontAxle());
    return -distanceTraveled;
  }
  else
  {
    return GetLateralPositionFrontAxle();
  }
}

void MentalModel::ResetLaneChangeFlags()
{
  SetRemainInLaneChangeState(false);
  SetRemainInRealignState(false);
  SetZipMerge(false);
  SetDirection(0);
  SetLateralActionAsForced(LaneChangeAction::None);
  SetIsEndOfLateralMovement(true);
  _laneChangeWidthTraveled = 0._m;
}

units::velocity::meters_per_second_t MentalModel::DetermineVelocityReason() const
{
  // Vector of situatively appropriate velocities for each influence
  auto VReason = std::vector<units::velocity::meters_per_second_t>({DetermineVelocityReasonSight(),
                                                                    DetermineVelocityReasonLaneWidth(),
                                                                    DetermineVelocityReasonCurvature(),
                                                                    DetermineVelocityReasonRightOvertakingProhibition(),
                                                                    DetermineVelocityReasonPassingSlowPlatoon(),
                                                                    DetermineVelocityReasonForDetectedTrafficJam()});

  return *std::min_element(VReason.begin(), VReason.end());
}

units::velocity::meters_per_second_t MentalModel::DetermineVelocityReasonForDetectedTrafficJam() const
{
  if (IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC) && _featureExtractor.IsInfluencingDistanceViolated(GetVehicle(AreaOfInterest::EGO_FRONT)))
  {
    return units::math::max(_trafficFlow->GetMeanVelocity(SurroundingLane::EGO), ScmDefinitions::MIN_TRAFFIC_JAM_APROACHING_VELOCITY);
  }

  return ScmDefinitions::INF_VELOCITY;
}

units::velocity::meters_per_second_t MentalModel::DetermineVelocityReasonSight() const
{
  auto SightDistance = GetInfrastructureCharacteristics()->GetGeometryInformation()->visibilityDistance;
  auto MaxLongitudinalDeceleration = GetDriverParameters().maximumLongitudinalDeceleration;
  auto CarQueuingDist = GetDriverParameters().carQueuingDistance;
  auto Rt = GetDriverParameters().reactionBaseTimeMinimum;

  double frictionEstimationFactor = 0.;
  double coefficientOfFrictionEstimate = 0.;
  double coefficientOfFriction = .85;  // Assumption (DUMMY) --> tbd!

  frictionEstimationFactor = _stochastics->GetNormalDistributed(1, .1);
  coefficientOfFrictionEstimate = frictionEstimationFactor * coefficientOfFriction;
  MaxLongitudinalDeceleration = units::math::fmin(coefficientOfFrictionEstimate * ScmDefinitions::ACCELERATION_OF_GRAVITY, MaxLongitudinalDeceleration);

  return -MaxLongitudinalDeceleration * Rt + units::math::sqrt(units::math::pow<2>(MaxLongitudinalDeceleration) * units::math::pow<2>(Rt) + 2. * MaxLongitudinalDeceleration * (SightDistance - CarQueuingDist));
}

units::velocity::meters_per_second_t MentalModel::DetermineVelocityReasonLaneWidth() const
{
  return units::math::fmax((20._mps + 50._mps / 1.75_m * (GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().width - GetVehicleWidth())), 20._mps);
}

units::velocity::meters_per_second_t MentalModel::DetermineVelocityReasonCurvature() const
{
  auto vReasonCurvatureDistance = ScmDefinitions::INF_VELOCITY;
  auto vReasonCurvatureNow = ScmDefinitions::INF_VELOCITY;
  auto vReasonCurvatureConnector = ScmDefinitions::INF_VELOCITY;
  auto comfLateralAcceleration = GetDriverParameters().comfortLateralAcceleration;

  if (_featureExtractor.IsOnEntryLane())
  {
    comfLateralAcceleration *= 2.;
  }

  try
  {
    if (GetCurvatureInPreviewDistance() != 0._i_m && GetCurvatureInPreviewDistance() != -999._i_m)
    {
      vReasonCurvatureDistance = units::math::sqrt(comfLateralAcceleration / units::math::fabs(GetCurvatureInPreviewDistance()));
    }
  }
  catch (...)
  {
    // Keep vReasonCurvatureDistance at INFINITY
  }

  if (GetCurvatureOnCurrentPosition() != 0._i_m)
  {
    vReasonCurvatureNow = units::math::sqrt(comfLateralAcceleration / units::math::fabs(GetCurvatureOnCurrentPosition()));
  }

  return units::math::fmin(units::math::fmin(vReasonCurvatureDistance, vReasonCurvatureNow), vReasonCurvatureConnector);
}

units::velocity::meters_per_second_t MentalModel::DetermineVelocityReasonRightOvertakingProhibition() const
{
  const auto rightHandTraffic = _scmDependencies.GetTrafficRulesScm()->common.rightHandTraffic;
  const AreaOfInterest aoiSideFront = rightHandTraffic ? AreaOfInterest::LEFT_FRONT : AreaOfInterest::RIGHT_FRONT;

  if (!_featureExtractor.DoesOuterLaneOvertakingProhibitionApply(aoiSideFront, false))
  {
    return ScmDefinitions::INF_VELOCITY;
  }

  return GetAbsoluteVelocity(aoiSideFront);  // every other situation
}

units::velocity::meters_per_second_t MentalModel::DetermineVelocityReasonPassingSlowPlatoon() const
{
  return (_mentalCalculations.VReasonPassingSlowPlatoon());
}

std::vector<std::vector<scm::CommonTrafficSign::Entity>> MentalModel::GetTrafficSigns() const
{
  // Vector for lane specific traffic signs
  std::vector<std::vector<scm::CommonTrafficSign::Entity>> trafficSigns;

  // Perceive traffic signs
  trafficSigns.push_back(GetInfrastructureCharacteristics()->GetTrafficRuleInformation()->Left().trafficSigns);
  trafficSigns.push_back(GetInfrastructureCharacteristics()->GetTrafficRuleInformation()->Close().trafficSigns);
  trafficSigns.push_back(GetInfrastructureCharacteristics()->GetTrafficRuleInformation()->Right().trafficSigns);

  return trafficSigns;
}

units::velocity::meters_per_second_t MentalModel::GetTrafficJamVelocityThreshold() const
{
  return ScmDefinitions::TRAFFIC_JAM_VELOCITY_THRESHOLD;
}

units::velocity::meters_per_second_t MentalModel::GetFormRescueLaneVelocityThreshold() const
{
  return ScmDefinitions::FORM_RESCUELANE_VELOCITY_THRESHOLD;
}

units::time::second_t MentalModel::GetAdjustmentBaseTime() const
{
  return _adjustmentBaseTime;
}

void MentalModel::CalculateAdjustmentBaseTime(ActionImplementationInterface* actionImplementation)
{
  // Recalculate adjustment base time when wish acceleration has changed since the last cycle
  if (actionImplementation->GetLongitudinalAccelerationWish() != _accelerationWishLongitudinalLast)
  {
    _adjustmentBaseTime =
        units::math::fmin(units::math::fmax(GetDriverParameters().adjustmentBaseTimeMinimum,
                                            units::make_unit<units::time::second_t>(_stochastics->GetLogNormalDistributed(GetDriverParameters().adjustmentBaseTimeMean.value(),
                                                                                                                          GetDriverParameters().adjustmentBaseTimeStandardDeviation.value()))),
                          GetDriverParameters().adjustmentBaseTimeMaximum) +
        _cycleTime;
    // An additional amount of 1 cycle time step is added, because the adjustment base time is advanced at the beginning of every
    // time step. Otherwise, the model would skip 1 amount of cycle time right at the beginning of the next time step.
    _accelerationWishLongitudinalLast = actionImplementation->GetLongitudinalAccelerationWish();
  }
}

void MentalModel::AdvanceAdjustmentBaseTime()
{
  _adjustmentBaseTime -= _cycleTime;
}

void MentalModel::SetAdjustmentBaseTime(units::time::second_t adjustmentBaseTime)
{
  _adjustmentBaseTime = adjustmentBaseTime;
}

bool MentalModel::IsAdjustmentBaseTimeExpired() const
{
  return _adjustmentBaseTime <= 0._s;
}

double MentalModel::GetHighCognitiveLaneChangeProbability() const
{
  return _highCognitive->AnticipateLaneChangeProbability(GetTime().value());
}

int MentalModel::GetClosestRelativeLaneIdForJunctionIngoing() const
{
  const std::vector<int> laneIdForJunctionIngoing{GetInfrastructureCharacteristics()->GetGeometryInformation()->relativeLaneIdForJunctionIngoing};
  if (std::find(laneIdForJunctionIngoing.begin(), laneIdForJunctionIngoing.end(), 0) != laneIdForJunctionIngoing.end())
  {
    return 0;
  }

  int lanesToCross{999};
  int relativeLane{-999};

  for (const auto& lane : laneIdForJunctionIngoing)
  {
    if (std::abs(lane) < lanesToCross)
    {
      lanesToCross = std::abs(lane);
      relativeLane = lane;
    }
  }

  return relativeLane;
}

bool MentalModel::GetLaneChangePrevention() const
{
  return (GetMicroscopicData()->GetOwnVehicleInformation()->preventLaneChangeDueToSpawn ||
          GetMicroscopicData()->GetOwnVehicleInformation()->preventLaneChangeDueExternalLateralControl);
}
void MentalModel::SetLaneChangePreventionAtSpawn(bool laneChangePrevention)
{
  GetMicroscopicData()->UpdateOwnVehicleData()->preventLaneChangeDueToSpawn = laneChangePrevention;
}

bool MentalModel::GetLaneChangePreventionAtSpawn() const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->preventLaneChangeDueToSpawn;
}
bool MentalModel::GetLaneChangePreventionExternalControl() const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->preventLaneChangeDueExternalLateralControl;
}

void MentalModel::SetLaneChangePreventionExternalControl(bool laneChangePrevention)
{
  GetMicroscopicData()->UpdateOwnVehicleData()->preventLaneChangeDueExternalLateralControl = laneChangePrevention;
}

bool MentalModel::GetRemainInLaneChangeState() const
{
  return _remainInLaneChangeState;
}

void MentalModel::SetRemainInLaneChangeState(bool remain)
{
  _remainInLaneChangeState = remain;
}

void MentalModel::ResetLaneChangeWidthTraveled()
{
  _laneChangeWidthTraveled = 0._m;
}

void MentalModel::SetLaneChangeWidthTraveled(units::length::meter_t width)
{
  _laneChangeWidthTraveled = width;
}

units::length::meter_t MentalModel::GetLaneChangeWidthTraveled() const
{
  return _laneChangeWidthTraveled;
}

bool MentalModel::GetCooperativeBehavior() const
{
  return _cooperativeBehavior;
}

ObstructionScm MentalModel::GetObstruction(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return ObstructionScm::NoOpponent();
  }

  return vehicle->GetLateralObstruction();
}

AreaOfInterest MentalModel::GetCausingVehicleOfSituationFrontCluster() const
{
  auto const situation = GetCurrentSituation();
  auto causingVehicle{GetMicroscopicData()->GetOwnVehicleInformation()->causingVehicleOfSituationFrontCluster};
  if (causingVehicle.find(situation) != causingVehicle.end())
  {
    return causingVehicle.at(situation);
  }
  return AreaOfInterest::NumberOfAreaOfInterests;
}

void MentalModel::SetCausingVehicleOfSituationFrontCluster(Situation situation, AreaOfInterest aoi)
{
  auto causingVehicle{GetMicroscopicData()->UpdateOwnVehicleData()->causingVehicleOfSituationFrontCluster};

  if (causingVehicle.find(situation) != causingVehicle.end())
  {
    GetMicroscopicData()->UpdateOwnVehicleData()->causingVehicleOfSituationFrontCluster.at(situation) = aoi;
  }
  else
  {
    throw std::runtime_error("MentalModel - SetCausingVehicleOfSituationFrontCluster(..): Not a front cluster situation.");
  }
}

void MentalModel::ResetCausingVehicleOfFrontCluster()
{
  for (auto cause : GetMicroscopicData()->UpdateOwnVehicleData()->causingVehicleOfSituationFrontCluster)
  {
    cause.second = AreaOfInterest::NumberOfAreaOfInterests;
  }
}

AreaOfInterest MentalModel::GetCausingVehicleOfSituationSideCluster() const
{
  auto const situation = GetCurrentSituation();
  auto causingVehicle{GetMicroscopicData()->GetOwnVehicleInformation()->causingVehicleOfSituationSideCluster};
  if (causingVehicle.find(situation) != causingVehicle.end())
  {
    return causingVehicle.at(situation);
  }
  return AreaOfInterest::NumberOfAreaOfInterests;
}

void MentalModel::SetCausingVehicleOfSituationSideCluster(Situation situation, AreaOfInterest aoi)
{
  auto causingVehicle{GetMicroscopicData()->UpdateOwnVehicleData()->causingVehicleOfSituationSideCluster};

  if (causingVehicle.find(situation) != causingVehicle.end())
  {
    GetMicroscopicData()->UpdateOwnVehicleData()->causingVehicleOfSituationSideCluster.at(situation) = aoi;
  }
  else
  {
    throw std::runtime_error("MentalModel - SetCausingVehicleOfSituationSideCluster(..): Not a side cluster situation.");
  }
}

void MentalModel::ResetCausingVehicleOfSideCluster()
{
  for (auto cause : GetMicroscopicData()->UpdateOwnVehicleData()->causingVehicleOfSituationSideCluster)
  {
    cause.second = AreaOfInterest::NumberOfAreaOfInterests;
  }
}

bool MentalModel::IsFrontSituation() const
{
  const auto s = GetCurrentSituation();
  return s == Situation::FOLLOWING_DRIVING || s == Situation::OBSTACLE_ON_CURRENT_LANE;
}

bool MentalModel::IsSituationAnticipated(Situation situation) const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->situationAnticipated.at(situation);
}

void MentalModel::SetSituationAnticipated(Situation situation, bool probability)
{
  GetMicroscopicData()->UpdateOwnVehicleData()->situationAnticipated.at(situation) = probability;
}

Risk MentalModel::GetSituationRisk(Situation situation) const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->situationRisk.at(situation);
}

void MentalModel::SetSituationRisk(Situation situation, Risk risk)
{
  GetMicroscopicData()->UpdateOwnVehicleData()->situationRisk.at(situation) = risk;
}

int MentalModel::DetermineIndexOfSideObject(AreaOfInterest aoi, SideAoiCriteria criterion, std::function<double(const SurroundingVehicleInterface*)> sortingParameter) const
{
  if (!LaneQueryHelper::IsSideArea(aoi) ||
      _aoiMapping.count(aoi) == 0 ||
      criterion == SideAoiCriteria::FRONT)
  {
    return 0;
  }

  const auto vehicles{_aoiMapping.at(aoi)};

  if (criterion == SideAoiCriteria::REAR)
  {
    return vehicles.size() - 1;
  }

  if (sortingParameter == nullptr)
  {
    throw std::runtime_error("MentalModel::DetermineIndexOfSideObject no sortingParameter given!\n");
  }

  if (criterion == SideAoiCriteria::MIN)
  {
    const auto it = std::min_element(begin(vehicles), end(vehicles), [sortingParameter](const auto* lhs, const auto* rhs)
                                     { return sortingParameter(lhs) < sortingParameter(rhs); });

    if (it != end(vehicles))
    {
      return std::distance(begin(vehicles), it);
    }
  }

  if (criterion == SideAoiCriteria::MAX)
  {
    const auto it = std::max_element(begin(vehicles), end(vehicles), [sortingParameter](const auto* lhs, const auto* rhs)
                                     { return sortingParameter(lhs) < sortingParameter(rhs); });

    if (it != end(vehicles))
    {
      return std::distance(begin(vehicles), it);
    }
  }

  return 0;
}

units::length::meter_t MentalModel::GetProjectedVehicleWidth() const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->projectedDimensions.widthProjected;
}

units::length::meter_t MentalModel::GetProjectedVehicleWidthLeft() const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->projectedDimensions.widthProjectedLeft;
}

units::length::meter_t MentalModel::GetProjectedVehicleWidthRight() const
{
  return GetMicroscopicData()->GetOwnVehicleInformation()->projectedDimensions.widthProjectedRight;
}

ProjectedSpacialDimensions MentalModel::GetProjectedDimensions(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return {};
  }

  return vehicle->GetProjectedDimensions();
}

units::acceleration::meters_per_second_squared_t MentalModel::GetComfortLongitudinalAcceleration() const
{
  return GetDriverParameters().comfortLongitudinalAcceleration;
}

units::acceleration::meters_per_second_squared_t MentalModel::GetComfortLongitudinalDeceleration() const
{
  return GetDriverParameters().comfortLongitudinalDeceleration;
}

units::length::meter_t MentalModel::GetInfluencingDistanceToEndOfLane() const
{
  double velocityFactor = units::math::max((units::velocity::meters_per_second_t(10._kph)), GetAbsoluteVelocityEgo(true)) / ScmDefinitions::NORMATIVE_VELOCITY;
  return velocityFactor * _agentsGeneralInfluencingDistanceToEndOfLane;                                                                                                      // adapt the general influencing distance of the agent due to his current velocity
}

units::acceleration::meters_per_second_squared_t MentalModel::GetDecelerationLimitForMerging() const
{
  return _merging->GetDecelerationLimitForMerging();
}

units::acceleration::meters_per_second_squared_t MentalModel::GetAccelerationLimitForMerging() const
{
  return _merging->GetAccelerationLimitForMerging();
}

bool MentalModel::IsObstacleCloserThanEndOfLane(double laneChangeSafetyFactor, units::velocity::meters_per_second_t velocityEgoAbsolute, units::length::meter_t distanceForFreeLaneChange, units::length::meter_t& distanceToEndOfMergeConsideringObstacle) const
{
  if (GetCurrentSituation() == Situation::OBSTACLE_ON_CURRENT_LANE)
  {
    AreaOfInterest causingVehicle = GetCausingVehicleOfSituationFrontCluster();
    if (causingVehicle != AreaOfInterest::NumberOfAreaOfInterests)  // There is actually an obstacle
    {
      auto relativeNetDistanceToObstacle = GetRelativeNetDistance(causingVehicle);
      auto ttcToObstacle = GetTtc(causingVehicle, 0);
      auto thwToObstacle = GetGap(causingVehicle, 0);
      auto distanceToEndOfLane = GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().distanceToEndOfLane;

      if (ttcToObstacle > 0._s && ttcToObstacle < ScmDefinitions::TTC_LIMIT)  // An approach on the obstacle is perceived
      {
        units::length::meter_t distanceAvailable = relativeNetDistanceToObstacle + (ttcToObstacle - thwToObstacle) * velocityEgoAbsolute;

        if (distanceAvailable < distanceToEndOfLane)  // Only regard obstacle, if end of lane is not more immanent (will be handled by next else-if-condition)
        {
          distanceToEndOfMergeConsideringObstacle = distanceAvailable - laneChangeSafetyFactor * distanceForFreeLaneChange;
          return true;
        }
      }
    }
  }
  return false;
}

void MentalModel::CheckIsLaneChangeProhibitionIgnored() const
{
  const auto& trafficRuleInformation{GetInfrastructureCharacteristics()->GetLaneInformationTrafficRules(RelativeLane::EGO)};
  if (!trafficRuleInformation.laneMarkingsLeft.empty() &&
      !trafficRuleInformation.laneMarkingsRight.empty())
  {
    const auto laneMarkingLeft{trafficRuleInformation.laneMarkingsLeft.at(0).type};
    const auto laneMarkingRight{trafficRuleInformation.laneMarkingsRight.at(0).type};

    if ((laneMarkingLeft == scm::LaneMarking::Type::Solid || laneMarkingLeft == scm::LaneMarking::Type::Solid_Solid || laneMarkingLeft == scm::LaneMarking::Type::Broken_Solid) ||
        (laneMarkingRight == scm::LaneMarking::Type::Solid || laneMarkingRight == scm::LaneMarking::Type::Solid_Solid || laneMarkingRight == scm::LaneMarking::Type::Solid_Broken))
    {
      const auto laneMarkingLeftLast{GetInfrastructureCharacteristics()->GetLaneMarkingTypeLeftLast()};
      const auto laneMarkingRightLast{GetInfrastructureCharacteristics()->GetLaneMarkingTypeRightLast()};

      if (laneMarkingLeft != laneMarkingLeftLast || laneMarkingRight != laneMarkingRightLast)
      {
        DrawLaneChangeProhibitionIgnored();
      }
    }
    else
    {
      GetMicroscopicData()->UpdateOwnVehicleData()->isLaneChangeProhibitionIgnored = false;  // No lane change prevention
    }
  }
}

void MentalModel::DrawLaneChangeProhibitionIgnored() const
{
  const bool laneChangeProhibitionIgnored{GetStochastics()->GetUniformDistributed(0, 1) < GetDriverParameters().laneChangeProhibitionIgnoringQuota.value_or(ScmDefinitions::DEFAULT_VALUE)};
  GetMicroscopicData()->UpdateOwnVehicleData()->isLaneChangeProhibitionIgnored = laneChangeProhibitionIgnored;
}

void MentalModel::DrawShoulderLaneUsage() const
{
  const bool whenTrafficJam = _stochastics->GetUniformDistributed(0, 1) <= GetDriverParameters().useShoulderInTrafficJamQuota;
  const bool whenTrafficJamPeerPressured = !whenTrafficJam && _stochastics->GetUniformDistributed(0, 1) <= GetDriverParameters().useShoulderWhenPeerPressuredQuota;

  const bool whenTrafficJamAndExit = _stochastics->GetUniformDistributed(0, 1) <= GetDriverParameters().useShoulderInTrafficJamToExitQuota;
  const bool whenTrafficJamAndExitPeerPressured = !whenTrafficJamAndExit && _stochastics->GetUniformDistributed(0, 1) <= GetDriverParameters().useShoulderWhenPeerPressuredQuota;

  GetMicroscopicData()->UpdateOwnVehicleData()->useShoulderLane = {whenTrafficJam, whenTrafficJamPeerPressured, whenTrafficJamAndExit, whenTrafficJamAndExitPeerPressured};
}

bool MentalModel::IsShoulderLaneUsageLegit(RelativeLane relativeLane) const
{
  const auto lane = GetInfrastructureCharacteristics()->GetLaneInformationGeometry(relativeLane);
  if (!IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC) || !LaneQueryHelper::IsLaneConsideredAsShoulder(lane))
    return false;

  auto shoulderUsages = GetMicroscopicData()->UpdateOwnVehicleData()->useShoulderLane;
  const auto surroundingVehicles = GetSurroundingVehicles(relativeLane);
  const bool anyVehicleVisibileOnShoulderLane = !surroundingVehicles.empty();

  if (shoulderUsages.stayOnceStarted || shoulderUsages.inTrafficJam)
  {
    return true;
  }
  else if (shoulderUsages.inTrafficJamWhenPeerPressured && anyVehicleVisibileOnShoulderLane)
  {
    shoulderUsages.stayOnceStarted = true;
    return true;
  }
  else if (!IsMesoscopicSituationActive(MesoscopicSituation::MANDATORY_EXIT))
  {
    return false;
  }
  else if (shoulderUsages.inTrafficJamCloseToExit)
  {
    return true;
  }
  else if (shoulderUsages.inTrafficJamCloseToExitWhenPeerPressured && anyVehicleVisibileOnShoulderLane)
  {
    shoulderUsages.stayOnceStarted = true;
    return true;
  }
  else
  {
    return false;
  }
}

units::length::meter_t MentalModel::AdjustMinDistanceDueToCooperation(units::length::meter_t minDistance) const
{
  const bool isCooperative{GetCooperativeBehavior()};

  if (isCooperative && _mentalCalculations.EgoBelowJamSpeed())
  {
    const double reductionFactor{.8};  // Factor of MinDistance that is considered enough
    minDistance *= reductionFactor;
  }

  return minDistance;
}

bool MentalModel::IsSideSituation() const
{
  const auto situation = GetCurrentSituation();
  return situation == Situation::LANE_CHANGER_FROM_RIGHT ||
         situation == Situation::LANE_CHANGER_FROM_LEFT ||
         situation == Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE ||
         situation == Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE ||
         situation == Situation::SIDE_COLLISION_RISK_FROM_RIGHT ||
         situation == Situation::SIDE_COLLISION_RISK_FROM_LEFT;
}

ObstructionLongitudinal MentalModel::GetLongitudinalObstruction(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return ObstructionLongitudinal::NoOpponent();
  }

  return vehicle->GetLongitudinalObstruction();
}

AreaOfInterest MentalModel::GetObjectToEvade() const
{
  AreaOfInterest objectToEvade{GetCausingVehicleOfSituationFrontCluster()};
  if (objectToEvade == AreaOfInterest::NumberOfAreaOfInterests)
  {
    return AreaOfInterest::EGO_FRONT;
  }
  return objectToEvade;
}

TrafficRuleInformationScmExtended* MentalModel::GetTrafficRuleInformation() const
{
  return GetInfrastructureCharacteristics()->GetTrafficRuleInformation();
}

units::length::meter_t MentalModel::GetVisibilityDistance() const
{
  return GetInfrastructureCharacteristics()->GetGeometryInformation()->visibilityDistance;
}

void MentalModel::SetHorizontalGazeAngle(units::angle::radian_t angle)
{
  _gazeAngle = angle;
}

units::angle::radian_t MentalModel::GetHorizontalGazeAngle() const
{
  // If ego looks at mirror fake look back
  return LaneQueryHelper::IsRearArea(GetFovea()) ? units::make_unit<units::angle::radian_t>(-M_PI) : _gazeAngle;
}

bool MentalModel::IsPointInVisibilityDistance(const Scm::Point& point) const
{
  return units::math::abs(units::math::hypot(point.s, point.t)) < GetInfrastructureCharacteristics()->GetGeometryInformation()->visibilityDistance;
}

Scm::Point CorrectPointDueToDriverOffset(const Scm::Point& worldPoint)
{
  constexpr auto driverLongitudinalPositionRelativeToVehicle{1._m};
  constexpr auto driverLateralPositionRelativeToVehicle{-0.75_m};

  return {worldPoint.s + driverLongitudinalPositionRelativeToVehicle,
          worldPoint.t + driverLateralPositionRelativeToVehicle};
}

bool MentalModel::IsPointVisible(const Scm::Point& point) const
{
  const auto gazeAngle{LaneQueryHelper::IsRearArea(GetFovea()) ? units::make_unit<units::angle::radian_t>(-M_PI) : _gazeAngle};
  const GazeCone gazeCone(gazeAngle, GazeParameters::HORIZONTAL_SIZE_PERIPHERY);
  const auto correctedPoint{CorrectPointDueToDriverOffset(point)};
  return gazeCone.IsPointInside(correctedPoint) &&
         IsPointInVisibilityDistance(correctedPoint);
}

bool MentalModel::IsVisible(const Scm::BoundingBox& boundingBox, units::angle::radian_t gazeConeOpeningAngle) const
{
  const auto gazeAngle{GetHorizontalGazeAngle()};
  const GazeCone gazeCone(gazeAngle, gazeConeOpeningAngle);

  const auto boundingBoxArray{boundingBox.ToArray()};
  return std::any_of(begin(boundingBoxArray), end(boundingBoxArray), [gazeCone, this](const auto& point)
                     {
    const auto correctedPoint{CorrectPointDueToDriverOffset(point)};
    return gazeCone.IsPointInside(correctedPoint) &&
           IsPointInVisibilityDistance(correctedPoint); });
}

void MentalModel::SetPossibleRegulateIds(const std::vector<int>& possibleRegulateIds)
{
  _possibleRegulateIds = possibleRegulateIds;
}

std::vector<int> MentalModel::GetPossibleRegulateIds() const
{
  return _possibleRegulateIds;
}

int MentalModel::GetLeadingVehicleId() const
{
  return _leadingVehicleId;
}

void MentalModel::SetLeadingVehicleId(int id)
{
  _leadingVehicleChanged = id != _leadingVehicleId;
  _leadingVehicleId = id;
}

const SurroundingVehicleInterface* MentalModel::GetVehicle(int id) const
{
  for (const auto* vehicle : _surroundingVehicles.Get())
  {
    if (vehicle->GetId() == id)
    {
      return vehicle;
    }
  }

  return nullptr;
}

const SurroundingVehicleInterface* MentalModel::GetVehicle(AreaOfInterest aoi, int sideAoiIndex) const
{
  // Ensure compability with existing side AOI Index scheme
  if (sideAoiIndex == -1)
  {
    sideAoiIndex = 0;
  }
  if (_aoiMapping.count(aoi) == 0 ||
      _aoiMapping.at(aoi).size() < sideAoiIndex + 1)
  {
    return nullptr;
  }

  return _aoiMapping.at(aoi).at(sideAoiIndex);
}

const SurroundingVehicleInterface* MentalModel::GetLeadingVehicle() const
{
  const auto id{GetLeadingVehicleId()};
  return GetVehicle(id);
}

void MentalModel::ActivateMesoscopicSituation(MesoscopicSituation situation)
{
  _microscopicData->UpdateOwnVehicleData()->hasMesoscopicSituationChanged = !_mesoscopicSituations.at(situation);
  _mesoscopicSituations.at(situation) = true;
}

void MentalModel::DeactivateMesoscopicSituation(MesoscopicSituation situation)
{
  _microscopicData->UpdateOwnVehicleData()->hasMesoscopicSituationChanged = _mesoscopicSituations.at(situation);
  _mesoscopicSituations.at(situation) = false;
}

bool MentalModel::IsMesoscopicSituationActive(MesoscopicSituation situation) const
{
  return _mesoscopicSituations.at(situation);
}

std::vector<MesoscopicSituation> MentalModel::GetActiveMesoscopicSituations() const
{
  std::vector<MesoscopicSituation> activeSituations;
  for (const auto& [situation, active] : _mesoscopicSituations)
  {
    if (active)
    {
      activeSituations.push_back(situation);
    }
  }
  return activeSituations;
}

void MentalModel::SetZipMerge(bool zipMerge)
{
  _isZipMerge = zipMerge;
}

bool MentalModel::GetZipMerge() const
{
  return _isZipMerge;
}

bool MentalModel::GetRemainInRealignState() const
{
  return _remainInRealignState;
}

void MentalModel::SetRemainInRealignState(bool remain)
{
  _remainInRealignState = remain;
}

void MentalModel::SetLastLateralAction(const LateralAction& action)
{
  _lastLateralAction.state = action.state;
  _lastLateralAction.direction = action.direction;
}

LateralAction MentalModel::GetLastLateralActionState() const
{
  return _lastLateralAction;
}

VirtualAgentsSCM MentalModel::GetVirtualAgents() const
{
  return _scmDependencies.GetSurroundingObjectsScm()->virtualTraffic;
}

std::vector<const SurroundingVehicleInterface*> MentalModel::GetSurroundingVehicles() const
{
  return _surroundingVehicles.Get();
}
std::vector<const SurroundingVehicleInterface*> MentalModel::GetSurroundingVehicles(RelativeLane relativeLane) const
{
  auto filter = [relativeLane](const SurroundingVehicleInterface* vehicle)
  { return vehicle->GetLaneOfPerception() == relativeLane; };
  std::vector<const SurroundingVehicleInterface*> surroundingVehiclesOnLane{};
  auto surroundingVehicles = _surroundingVehicles.Get();
  std::copy_if(begin(surroundingVehicles), end(surroundingVehicles), std::back_inserter(surroundingVehiclesOnLane), filter);
  return surroundingVehiclesOnLane;
}

SurroundingVehicles* MentalModel::UpdateSurroundingVehicles()
{
  return &_surroundingVehicles;
}

void MentalModel::SetAoiMapping(const AoiVehicleMapping& mapping)
{
  _aoiMapping = mapping;
}

double MentalModel::GetThresholdLooming(AreaOfInterest aoi, int sideAoiIndex) const
{
  // TODO Refactor usages to use vehicle directly
  const auto* vehicle{GetVehicle(aoi, sideAoiIndex)};
  if (vehicle == nullptr)
  {
    return ScmDefinitions::DEFAULT_VALUE;
  }

  return vehicle->GetTtcThresholdLooming();
}

double MentalModel::GetVisibilityFactorForHighwayExit() const
{
  return _microscopicData->GetOwnVehicleInformation()->distributionForHighwayExit;
}

void MentalModel::SetOuterKeepingQuotaFulfilled(bool outerKeeping)
{
  _microscopicData->UpdateOwnVehicleData()->isOuterKeeping = outerKeeping;
}
bool MentalModel::GetOuterKeepingQuotaFulfilled() const
{
  return _microscopicData->GetOwnVehicleInformation()->isOuterKeeping;
}

void MentalModel::SetEgoLaneKeepingQuotaFulfilled(bool egoLaneKeeping)
{
  _microscopicData->UpdateOwnVehicleData()->isEgoLaneKeeping = egoLaneKeeping;
}

bool MentalModel::GetEgoLaneKeepingQuotaFulfilled() const
{
  return _microscopicData->GetOwnVehicleInformation()->isEgoLaneKeeping;
}

void MentalModel::UpdateVisualReliabilityMap()
{
  for (const auto& [sector, reliability] : _visualReliabilityMap)
  {
    _visualReliabilityMap[sector] = ReliabilityCalculations::DecayReliability(reliability, ParameterChangeRate::SLOW, _cycleTime);
  }

  const auto fovea{GetFovea()};
  if (ScmCommons::IsSurroundingAreaOfInterest(fovea))
  {
    const auto sectorUpdateMap{VisualReliabilityCalculations::CalculateSectorUpdates(GetHorizontalGazeAngle())};
    for (const auto& [sector, reliabilityValue] : sectorUpdateMap)
    {
      _visualReliabilityMap.at(sector) = std::max(_visualReliabilityMap.at(sector), reliabilityValue);
    }
  }
}

bool MentalModel::IsVisualSectorReliable(VisualPerceptionSector sector, double requiredQuality) const
{
  return _visualReliabilityMap.at(sector) >= requiredQuality;
}

double MentalModel::GetVisualReliability(VisualPerceptionSector sector) const
{
  return _visualReliabilityMap.at(sector);
}

Merge::EgoInfo MentalModel::GetEgoInfo() const
{
  Merge::EgoInfo egoData;

  egoData.length = GetVehicleModelParameters().bounding_box.dimension.length;
  egoData.velocity = GetLongitudinalVelocityEgo();
  egoData.urgencyFactorForLaneChange = _mentalCalculations.GetUrgencyFactorForLaneChange();

  return egoData;
}

void MentalModel::SetMergeGap(Merge::Gap mergeGap)
{
  _mergeGap = mergeGap;

  if (mergeGap.IsValid())
  {
    SetMergeRegulate(mergeGap.GetMergeRegulate()->GetAssignedAoi());
    SetMergeRegulateId(mergeGap.GetMergeRegulate()->GetId());
  }
  else
  {
    SetMergeRegulate(AreaOfInterest::NumberOfAreaOfInterests);
    SetMergeRegulateId(-1);
  }
}

Merge::Gap MentalModel::GetMergeGap() const
{
  return _mergeGap;
}

bool MentalModel::IsRightHandTraffic() const
{
  return _scmDependencies.GetTrafficRulesScm()->common.rightHandTraffic;
}

double MentalModel::GetReductionFactorUnderUrgency() const
{
  return 1. - _mentalCalculations.GetUrgencyFactorForLaneChange();
}

AdasHmiSignal MentalModel::GetCombinedAcousticOpticSignal() const
{
  return _acousticAndOpticAdasSignal;
}
void MentalModel::SetCombinedAcousticOpticSignal(AdasHmiSignal acousticAndOpticAdasSignal)
{
  _acousticAndOpticAdasSignal = acousticAndOpticAdasSignal;
}

void MentalModel::SetPlannedLaneChangeDimensions(const TrajectoryPlanning::TrajectoryDimensions& dimensions)
{
  _plannedTrajectoryDimensions = dimensions;
}

const std::optional<TrajectoryPlanning::TrajectoryDimensions>& MentalModel::GetPlannedLaneChangeDimensions() const
{
  return _plannedTrajectoryDimensions;
}

void MentalModel::ResetPlannedLaneChangeDimensions()
{
  _plannedTrajectoryDimensions = std::nullopt;
}

double MentalModel::GetUrgencyReductionFactor() const
{
  return 1. - _mentalCalculations.GetUrgencyFactorForLaneChange();
}

std::vector<const SurroundingVehicleInterface*> MentalModel::GetVehicleVector(AreaOfInterest aoi) const
{
  std::vector<const SurroundingVehicleInterface*> result;
  if (_aoiMapping.count(aoi))
  {
    std::copy(begin(_aoiMapping.at(aoi)), end(_aoiMapping.at(aoi)), std::back_inserter(result));
  }

  return result;
}

void MentalModel::DrawDistractionUsage() const
{
  const bool isAgentDistracted = _stochastics->GetUniformDistributed(0, 1) <= GetDriverParameters().distractionQuota;
  GetMicroscopicData()->UpdateOwnVehicleData()->isDistracted = isAgentDistracted;
}

std::map<std::string, std::string> MentalModel::GetDebugValues() const
{
  return _DEBUG_VALUES;
}

void MentalModel::AddDebugValue(const std::string& description, const std::string& value)
{
  _DEBUG_VALUES[description] = value;
}