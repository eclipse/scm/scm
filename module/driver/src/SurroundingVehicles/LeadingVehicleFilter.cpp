/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "LeadingVehicleFilter.h"

#include <cmath>

#include "LaneQueryHelper.h"
#include "ScmCommons.h"
#include "include/common/ScmDefinitions.h"

LeadingVehicleFilter::LeadingVehicleFilter(const LeadingVehicleQueryInterface& leadingVehicleQuery, const SurroundingVehicleQueryFactoryInterface& vehicleQueryFactory)
    : _leadingVehicleQuery{leadingVehicleQuery},
      _vehicleQueryFactory{vehicleQueryFactory}
{
}

std::vector<const SurroundingVehicleInterface*> LeadingVehicleFilter::FilterRearVehicles(const std::vector<const SurroundingVehicleInterface*>& vehicles) const
{
  std::vector<const SurroundingVehicleInterface*> keepVehicleVector;
  std::remove_copy_if(begin(vehicles), end(vehicles), std::back_inserter(keepVehicleVector), [](const auto* vehicle)
                      { return vehicle->BehindEgo(); });

  return keepVehicleVector;
}

std::vector<const SurroundingVehicleInterface*> LeadingVehicleFilter::FilterSideLaneVehicles(const std::vector<const SurroundingVehicleInterface*>& vehicles) const
{
  const auto sideSituationVehicleId{_leadingVehicleQuery.GetCausingVehicleIdSideSituation()};
  const auto egoLaneChangeDirection{_leadingVehicleQuery.GetEgoLaneChangeDirection()};

  std::vector<const SurroundingVehicleInterface*> keepVehicleVector;
  std::copy_if(begin(vehicles), end(vehicles), std::back_inserter(keepVehicleVector), [this, sideSituationVehicleId, egoLaneChangeDirection](const auto* vehicle)
               {
    const auto relativeLane{vehicle->GetLaneOfPerception()};
    if (!(relativeLane == RelativeLane::LEFT || relativeLane == RelativeLane::RIGHT))
    {
      return true;
    }

    // Ensure right overtaking prohibition by selecting appropriate leading vehicle
    if (_leadingVehicleQuery.DoesOvertakingProhibitionApply(*vehicle))
    {
      return true;
    }

    const auto vehicleQuery{_vehicleQueryFactory.GetQuery(*vehicle)};
    // Keep vehicles that approach from the side
    if ((vehicle->IsExceedingLateralMotionThreshold() &&
        vehicleQuery->CannotClearLongitudinalObstructionBeforeCollision() &&
        vehicleQuery->IsChangingLanesStillOnStartLane()))
    {
      return true;
    }

    // Keep vehicles on target lane of ego when changing lanes
    if (egoLaneChangeDirection.has_value())
    {
      return SurroundingLane2RelativeLane.at(egoLaneChangeDirection.value()) == relativeLane;
    }

    // Keep causing vehicle of side situation or overlapping vehicle
    if (vehicle->LateralObstructionOverlapping() || 
        (sideSituationVehicleId.has_value() && vehicle->GetId() == sideSituationVehicleId.value() && vehicle->IsExceedingLateralMotionThreshold()))
    {
      const auto minDistance{_leadingVehicleQuery.GetMinDistance(*vehicle)};
      return _vehicleQueryFactory.GetQuery(*vehicle)->WillEnterMinDistanceBeforeObstructionIsCleared(minDistance);
    }

    return false; });

  return keepVehicleVector;
}

std::vector<const SurroundingVehicleInterface*> LeadingVehicleFilter::FilterSideSideVehicles(const std::vector<const SurroundingVehicleInterface*>& vehicles) const
{
  std::vector<const SurroundingVehicleInterface*> keepVehicleVector;
  std::copy_if(begin(vehicles), end(vehicles), std::back_inserter(keepVehicleVector), [this](const auto* vehicle)
               {
      if(vehicle->GetLaneOfPerception() != RelativeLane::LEFTLEFT && vehicle->GetLaneOfPerception() != RelativeLane::RIGHTRIGHT)
      {
        return true;
      }

      const auto egoLaneChangeDirection {_leadingVehicleQuery.GetEgoLaneChangeDirection()};
      if(!egoLaneChangeDirection.has_value())
      {
        return false;
      }

      if((egoLaneChangeDirection.value() == SurroundingLane::LEFT && vehicle->GetLaneOfPerception() == RelativeLane::LEFTLEFT) ||
         (egoLaneChangeDirection.value() == SurroundingLane::RIGHT && vehicle->GetLaneOfPerception() == RelativeLane::RIGHTRIGHT))
      {
        const auto query {_vehicleQueryFactory.GetQuery(*vehicle)};
        return query->IsSideSideVehicleChangingIntoSide();
      }
      
      return false; });

  return keepVehicleVector;
}

std::vector<const SurroundingVehicleInterface*> LeadingVehicleFilter::FilterSwervingTarget(const std::vector<const SurroundingVehicleInterface*>& vehicles) const
{
  const auto swervingTargetId{_leadingVehicleQuery.GetSwervingTargetId()};
  const auto swervingPlanned{_leadingVehicleQuery.IsSwervingPlanned()};

  if (!swervingTargetId.has_value() && !swervingPlanned)
  {
    return vehicles;
  }

  std::vector<const SurroundingVehicleInterface*> keepVehicleVector;
  std::copy_if(begin(vehicles), end(vehicles), std::back_inserter(keepVehicleVector), [swervingTargetId, swervingPlanned, this](const auto* vehicle)
               {
    if (swervingTargetId.has_value() && vehicle->GetId() == swervingTargetId.value())
    {
      return false;
    }

    if (swervingPlanned)
    {
      const auto causingVehicleIdFront{_leadingVehicleQuery.GetCausingVehicleIdFrontSituation()};
      if (causingVehicleIdFront.has_value() && vehicle->GetId() == causingVehicleIdFront.value())
      {
        return false;
      }
    }

    return true; });

  return keepVehicleVector;
}

std::vector<const SurroundingVehicleInterface*> LeadingVehicleFilter::FilterVehiclesWithUnassignedAoi(const std::vector<const SurroundingVehicleInterface*>& vehicles) const
{
  std::vector<const SurroundingVehicleInterface*> keepVehicleVector;
  std::remove_copy_if(begin(vehicles), end(vehicles), std::back_inserter(keepVehicleVector), [](const auto* vehicle)
                      { return vehicle->GetAssignedAoi() == AreaOfInterest::NumberOfAreaOfInterests; });

  return keepVehicleVector;
}