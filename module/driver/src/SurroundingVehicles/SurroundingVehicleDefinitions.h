/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include <map>

#include "ScmDefinitions.h"
#include "SurroundingVehicleInterface.h"
#include "include/common/ScmDefinitions.h"

using AoiVehicleMapping = std::map<AreaOfInterest, std::vector<SurroundingVehicleInterface*>>;
using LaneVehicleMapping = std::map<RelativeLane, std::vector<SurroundingVehicleInterface*>>;
using SurroundingVehicleVector = std::vector<SurroundingVehicleInterface*>;