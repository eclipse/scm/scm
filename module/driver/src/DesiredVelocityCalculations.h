/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once
#include <units.h>

#include <algorithm>

namespace DesiredVelocityCalculations
{
//! @brief Struct definition for the description of a TargetVelocityParameters
struct TargetVelocityParameters
{
  //! @brief vReason
  units::velocity::meters_per_second_t vReason;
  //! @brief desiredVelocity
  units::velocity::meters_per_second_t desiredVelocity;
  //! @brief desiredVelocityViolationDelta
  units::velocity::meters_per_second_t desiredVelocityViolationDelta;
  //! @brief maximumVehicleVelocity
  units::velocity::meters_per_second_t maximumVehicleVelocity;
  //! @brief legalVelocity
  units::velocity::meters_per_second_t legalVelocity;
  //! @brief legalVelocityViolationDelta
  units::velocity::meters_per_second_t legalVelocityViolationDelta;
};

//! @brief Calculate the target velocity
//! @param param const TargetVelocityParameters &
//! @return target velocity
static inline units::velocity::meters_per_second_t CalculateTargetVelocity(const TargetVelocityParameters& param)
{
  return std::min({param.vReason,
                   param.desiredVelocity + param.desiredVelocityViolationDelta,
                   param.legalVelocity + param.legalVelocityViolationDelta,
                   param.maximumVehicleVelocity});
}
}  // namespace DesiredVelocityCalculations