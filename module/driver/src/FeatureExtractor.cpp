/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "FeatureExtractor.h"

#include <algorithm>

#include "LaneQueryHelper.h"
#include "MentalCalculationsInterface.h"
#include "MentalModelInterface.h"
#include "ScmDefinitions.h"
#include "SurroundingVehicles/LeadingVehicleQueryInterface.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQuery.h"
using namespace units::literals;
FeatureExtractor::FeatureExtractor()
{
}

void FeatureExtractor::SetMentalModel(const MentalModelInterface& mentalModel)
{
  _mentalModel = &mentalModel;
}

void FeatureExtractor::SetMentalCalculations(const MentalCalculationsInterface& mentalCalculations)
{
  _mentalCalculations = &mentalCalculations;
}

void FeatureExtractor::SetSurroundingVehicleQueryFactory(const SurroundingVehicleQueryFactoryInterface* queryFactory)
{
  _surroundingVehicleQueryFactory = queryFactory;
}

void FeatureExtractor::SetOuterLaneOvertakingProhibitionQuota(IgnoringOuterLaneOvertakingProhibitionInterface* outerLaneOvertakingProhibitionQuota)
{
  _outerLaneOvertakingProhibitionQuota = outerLaneOvertakingProhibitionQuota;
}

bool FeatureExtractor::IsVehicleInSideLane(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }

  auto relativelane = vehicle->GetLaneOfPerception();
  return relativelane == RelativeLane::RIGHT || relativelane == RelativeLane::LEFT;
}

bool FeatureExtractor::IsMinimumFollowingDistanceViolated(const SurroundingVehicleInterface* vehicle, double reductionFactorUrgency) const
{
  if (vehicle == nullptr)
  {
    return false;
  }

  reductionFactorUrgency = std::clamp(reductionFactorUrgency, 0., 1.);
  auto minDistance = units::math::fabs(_mentalCalculations->GetMinDistance(vehicle));
  auto currentDistance = units::math::fabs(vehicle->GetRelativeNetDistance().GetValue());
  auto minDistanceUnderUrgency = minDistance * reductionFactorUrgency;

  LateralActionQuery lateralActionQuery {_mentalModel->GetLateralAction()};
  if (lateralActionQuery.IsPreparingToMerge() && _mentalModel->GetMergeRegulateId() == vehicle->GetId())
  {
    const auto gapDistance{_mentalModel->GetMergeGap().distance};
    return currentDistance <= units::math::abs(gapDistance);
  }

  return (currentDistance <= minDistanceUnderUrgency);  // no urgency
}

bool FeatureExtractor::IsInfluencingDistanceViolated(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }

  auto influencingDistance = _mentalCalculations->GetInfluencingDistance(vehicle);
  auto currentDistance = vehicle->GetRelativeNetDistance().GetValue();
  return (currentDistance <= influencingDistance);
}

bool FeatureExtractor::IsNearEnoughForFollowing(const SurroundingVehicleInterface& vehicle, double reductionFactor) const
{
  reductionFactor = std::clamp(reductionFactor, 0., 1.);
  auto eqDistance = _mentalCalculations->GetEqDistance(&vehicle);
  auto extEqDistance = eqDistance + 1.5_s * _mentalModel->GetLongitudinalVelocityEgo();
  auto extEqDistanceUnderUrgency = extEqDistance * reductionFactor;

  auto currentDistance = vehicle.GetRelativeNetDistance().GetValue();

  return (currentDistance <= extEqDistanceUnderUrgency);
}

bool FeatureExtractor::IsExceptionallySlow(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }

  const units::velocity::meters_per_second_t velocitySurroundingObject = vehicle->GetAbsoluteVelocity().GetValue();
  const auto velocityEgo = _mentalModel->GetAbsoluteVelocityEgo(true);

  // in a traffic jam (QUEUED_TRAFFIC) no vehicle should be considered as exceptionally slow
  if (_mentalModel->IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC))
  {
    return false;
  }

  units::velocity::meters_per_second_t meanLaneVelocity = 0_mps;
  units::velocity::meters_per_second_t velocityLegal = 0_mps;
  auto laneOfPerception = vehicle->GetLaneOfPerception();

  if (laneOfPerception == RelativeLane::RIGHT)
  {
    meanLaneVelocity = _mentalModel->GetMeanVelocity(RelativeLane2SurroundingLane.at(laneOfPerception));
    velocityLegal = _mentalModel->GetVelocityLegalRight();
  }
  else if (laneOfPerception == RelativeLane::LEFT)
  {
    meanLaneVelocity = _mentalModel->GetMeanVelocity(RelativeLane2SurroundingLane.at(laneOfPerception));
    velocityLegal = _mentalModel->GetVelocityLegalLeft();
  }
  else
  {
    meanLaneVelocity = 99._mps;
    velocityLegal = 99._mps;
  }

  if (velocitySurroundingObject > 0.5 * (velocityEgo - units::velocity::meters_per_second_t(10_kph)))  // -10km/h to allow some difference while driving slowly without resulting in Exceptionally slow driving
  {
    return false;  // Not exceptionally slower than EgoVehicle
  }

  if (velocitySurroundingObject > 0.6 * velocityLegal)
  {
    return false;  // Not exceptionally slow since general lane speed is slow //not exceptionally slow since the lane is supposed to be slower than ego lane
  }
  if (meanLaneVelocity > 0.0_mps && velocitySurroundingObject > 0.6 * meanLaneVelocity)
  {
    return false;  // Not exceptionally slow since general lane speed is slow
  }

  auto aoi = vehicle->GetAssignedAoi();
  switch (aoi)
  {
    case AreaOfInterest::LEFT_FRONT:
      if (_mentalModel->GetIsVehicleVisible(AreaOfInterest::LEFT_FRONT_FAR))
      {
        if (velocitySurroundingObject > 0.5 * _mentalModel->GetAbsoluteVelocity(AreaOfInterest::LEFT_FRONT_FAR))
        {
          return false;  // Not exceptionally slow since front vehicle is quite slow aswell
        }
      }

      break;
    case AreaOfInterest::LEFT_FRONT_FAR:
      return false;  // If "slow" vehicle is far object there might be a genarally slow traffic or it will become LEFT_FRONT object soon anyways
    case AreaOfInterest::LEFT_SIDE:
      return false;  // Too late to react anyways
    case AreaOfInterest::RIGHT_FRONT:
      if (_mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHT_FRONT_FAR))
      {
        if (velocitySurroundingObject > 0.5 * _mentalModel->GetAbsoluteVelocity(AreaOfInterest::RIGHT_FRONT_FAR))
        {
          return false;  // Not exceptionally slow since front vehicle is quite slow aswell
        }
      }

      break;
    case AreaOfInterest::RIGHT_FRONT_FAR:
      return false;  // If "slow" vehicle is far object there is either a genarally slow traffic or it will become LEFT_FRONT object soon anyways
    case AreaOfInterest::RIGHT_SIDE:
      return false;  // Too late to react anyways
    default:
      return false;  // Only front side aois are analyzed since front situations are taken care seperately
  }

  return true;  // In every other case when aoi = LEFT/RIGHT_FRONT && vehicle is much slower than average && ego &&Side_FAR
}

bool FeatureExtractor::HasSuspiciousBehaviour(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }

  return IsObstacle(vehicle) || IsExceptionallySlow(vehicle) || vehicle->IsCollided();
}

bool FeatureExtractor::IsUnfavorable(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle != nullptr && !vehicle->BehindEgo())
  {
    if (vehicle->IsReliable(DataQuality::MEDIUM, ParameterChangeRate::MEDIUM))
    {
      auto lane = RelativeLane2SurroundingLane.at(vehicle->GetLaneOfPerception());
      return (GetTimeAtTargetSpeedInLane(lane) < _mentalModel->GetDriverParameters().desiredTimeAtTargetSpeed);
    }
    else
    {
      _mentalModel->AddInformationRequest(vehicle->GetAssignedAoi(), FieldOfViewAssignment::UFOV, 0.5, InformationRequestTrigger::LATERAL_ACTION);
    }
  }

  return true;  // standard case: unfavorable
}

bool FeatureExtractor::IsObstacle(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }

  if (_mentalModel->IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC))
  {
    return vehicle->IsStatic();
  }

  auto velCompare = _mentalModel->GetTrafficJamVelocityThreshold() / 2.;

  return (vehicle->IsStatic() ||
          (vehicle->GetAbsoluteVelocity().GetValue() < velCompare &&  // Other car is slow
           vehicle->GetTtc().GetValue() < ScmDefinitions::TTC_LIMIT));
}

bool FeatureExtractor::HasJamVelocity(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }
  return vehicle->GetAbsoluteVelocity().GetValue() < _mentalModel->GetTrafficJamVelocityThreshold();
}

bool FeatureExtractor::HasJamVelocityEgo() const
{
  return _mentalModel->GetAbsoluteVelocityEgo(false) < _mentalModel->GetTrafficJamVelocityThreshold();
}

bool FeatureExtractor::IsDeceleratingDueToSuspiciousSideVehicles() const
{
  const double cooperationFactor = _mentalModel->GetAgentCooperationFactorForSuspiciousBehaviourEvasion();
  if (cooperationFactor > .5 && cooperationFactor < .75)
  {
    return true;
  }

  return false;
}

bool FeatureExtractor::IsAvoidingLaneNextToSuspiciousSideVehicles() const
{
  if (_mentalModel->GetAgentCooperationFactorForSuspiciousBehaviourEvasion() >= .75)
  {
    return true;
  }

  return false;
}

bool FeatureExtractor::HasIntentionalLaneCrossingConflict(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }

  switch (vehicle->GetAssignedAoi())
  {
    case AreaOfInterest::LEFT_FRONT:
    case AreaOfInterest::LEFT_REAR:
      if (vehicle->GetIndicatorState() == scm::LightState::Indicator::Right)
      {
        return true;
      }

      return false;
    case AreaOfInterest::LEFT_SIDE:

      for (const auto vehicle : *_mentalModel->GetMicroscopicData()->GetSideObjectVector(vehicle->GetAssignedAoi()))
      {
        if (vehicle.indicatorState == scm::LightState::Indicator::Right)
        {
          return true;
        }
      }
      return false;
    case AreaOfInterest::RIGHT_FRONT:
      if (vehicle->GetIndicatorState() == scm::LightState::Indicator::Left)
      {
        return true;
      }

      return false;
    case AreaOfInterest::RIGHT_SIDE:
      for (const auto vehicle : *_mentalModel->GetMicroscopicData()->GetSideObjectVector(vehicle->GetAssignedAoi()))
      {
        if (vehicle.indicatorState == scm::LightState::Indicator::Left)
        {
          return true;
        }
      }

      return false;
    default:
      return false;
  }
}

bool FeatureExtractor::AbleToPass(const SurroundingVehicleInterface* vehicle, units::velocity::meters_per_second_t deltaVelocity) const
{
  if (vehicle == nullptr)
  {
    return false;
  }
  auto passingTime = GetPassingTime(vehicle, deltaVelocity);
  return (PerceivedTimeToLaneCrossing(vehicle) >= passingTime);
}

bool FeatureExtractor::AnticipatedAbleToPass(const SurroundingVehicleInterface* vehicle, units::velocity::meters_per_second_t deltaVelocity) const
{
  if (vehicle == nullptr)
  {
    return false;
  }
  auto passingTime = GetPassingTime(vehicle, deltaVelocity);
  return (AnticipatedTimeToLaneCrossing(vehicle) >= passingTime);
}

bool FeatureExtractor::IsOnEntryOrExitLane() const
{
  const scm::LaneType currentLaneType = _mentalModel->GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().laneType;
  if (currentLaneType == scm::LaneType::OffRamp || currentLaneType == scm::LaneType::OnRamp ||
      currentLaneType == scm::LaneType::Exit || currentLaneType == scm::LaneType::Entry)
  {
    return true;
  }

  return false;
}

bool FeatureExtractor::IsOnEntryLane() const
{
  const scm::LaneType currentLaneType = _mentalModel->GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().laneType;
  if (currentLaneType == scm::LaneType::OnRamp || currentLaneType == scm::LaneType::Entry)
  {
    return true;
  }

  return false;
}

bool FeatureExtractor::IsAlreadyPreparingToMerge() const
{
  LateralActionQuery action(_mentalModel->GetLateralAction());
  return action.IsPreparingToMerge();
}

bool FeatureExtractor::CanLeaveLaneBeforeItIsEnding(const units::length::meter_t lateralDistanceToLeaveLane, const units::velocity::meters_per_second_t lateralVelocity) const
{
  const auto vEgo = units::math::max(_mentalModel->GetLongitudinalVelocityEgo(), 1.0_mps);
  const auto distanceToEndOfLane = _mentalModel->GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().distanceToEndOfLane;
  const auto timeToEndOfLaneIsReached = distanceToEndOfLane / vEgo;
  const auto timeToLeaveEgoLane = lateralDistanceToLeaveLane / lateralVelocity;
  return units::math::fabs(timeToLeaveEgoLane) < units::math::fabs(timeToEndOfLaneIsReached);
}

// To do maybe optional
units::length::meter_t FeatureExtractor::GetRelevantVehicleWidth(units::velocity::meters_per_second_t lateralVelocity) const
{
  if (lateralVelocity > 0.0_mps)
  {
    return _mentalModel->GetProjectedVehicleWidthRight();
  }
  else if (lateralVelocity < 0.0_mps)
  {
    return _mentalModel->GetProjectedVehicleWidthLeft();
  }
  else
  {
    return ScmDefinitions::INF_DISTANCE;
  }
}

units::length::meter_t FeatureExtractor::CalculateLateralDistanceToLeaveLane(units::velocity::meters_per_second_t lateralVelocity, units::length::meter_t relevantVehicleWidth) const
{
  const auto lateralPositionEgo = lateralVelocity > 0_mps ? -_mentalModel->GetLateralPosition() : _mentalModel->GetLateralPosition();
  const auto relevantLaneWidth = _mentalModel->GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().width / 2.0;
  return relevantVehicleWidth + relevantLaneWidth + lateralPositionEgo;
}

bool FeatureExtractor::IsEvadingEndOfLane() const
{
  // Merging maneuvre is only activated when agent determined that he can reach the gap before end of lane thus it is
  // save to continue (braking could interrupt merging by acceleration maneuver)
  if (IsAlreadyPreparingToMerge())
  {
    return true;
  }

  const auto lateralVelocityEgo = _mentalModel->GetLateralVelocity();
  if (lateralVelocityEgo == 0_mps)
  {
    return false;  // No lateral movement implies no evading
  }

  const auto relevantVehicleWidth = GetRelevantVehicleWidth(lateralVelocityEgo);
  const auto lateralDistanceToLeaveEgoLane = CalculateLateralDistanceToLeaveLane(lateralVelocityEgo, relevantVehicleWidth);
  return CanLeaveLaneBeforeItIsEnding(lateralDistanceToLeaveEgoLane, lateralVelocityEgo);
}

bool FeatureExtractor::IsLaneChangePermittedDueToLaneMarkings(Side side) const
{
  if (_mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()->isLaneChangeProhibitionIgnored ||
      (!IsLaneTypeDriveable(_mentalModel->GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().laneType) && !_mentalModel->IsShoulderLaneUsageLegit(RelativeLane::EGO)))  // if agents are on prohibited area they will ignore markings to get back on legal road
  {
    return true;
  }

  // Check for lane makrings
  std::vector<scm::LaneMarking::Entity> laneMarkings{};
  std::vector<scm::LaneMarking::Type> preventiveLaneMarkings{scm::LaneMarking::Type::Solid, scm::LaneMarking::Type::Solid_Solid};

  if (side == Side::Left)
  {
    laneMarkings = _mentalModel->GetInfrastructureCharacteristics()->GetTrafficRuleInformation()->Close().laneMarkingsLeft;
    preventiveLaneMarkings.push_back(scm::LaneMarking::Type::Broken_Solid);
  }
  else
  {
    laneMarkings = _mentalModel->GetInfrastructureCharacteristics()->GetTrafficRuleInformation()->Close().laneMarkingsRight;
    preventiveLaneMarkings.push_back(scm::LaneMarking::Type::Solid_Broken);
  }

  RelativeLane relativeLane = side == Side::Left ? RelativeLane::LEFT : RelativeLane::RIGHT;
  auto laneInformation = _mentalModel->GetInfrastructureCharacteristics()->GetLaneInformationGeometry((relativeLane));
  if (LaneQueryHelper::IsLaneConsideredAsShoulder(laneInformation) && _mentalModel->IsShoulderLaneUsageLegit(relativeLane))
  {
    preventiveLaneMarkings.clear();
  }

  for (const auto& laneMarking : laneMarkings)
  {
    if (laneMarking.relativeStartDistance <= 0.0_m)  // Current valid lane marking
    {
      // Is the current valid lane marking a preventive one
      if (std::find(preventiveLaneMarkings.begin(), preventiveLaneMarkings.end(), laneMarking.type) != preventiveLaneMarkings.end() ||
          DoesLaneMarkingBrokenBoldPreventLaneChange(laneMarking, side))
      {
        return false;
      }
    }
  }

  return true;
}
bool FeatureExtractor::IsLaneChangePermittedDueToLaneMarkingsForAoi(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return true;
  }
  auto relativeLane = LaneQueryHelper::GetRelativeLaneFromAoi(vehicle->GetAssignedAoi());
  auto laneInformation = _mentalModel->GetInfrastructureCharacteristics()->GetLaneInformationGeometry((relativeLane));

  if (!IsLaneTypeDriveable(laneInformation.laneType) && !_mentalModel->IsShoulderLaneUsageLegit(relativeLane))
  {
    return true;
  }
  std::vector<scm::LaneMarking::Type> preventiveLaneMarkings{scm::LaneMarking::Type::Solid, scm::LaneMarking::Type::Solid_Solid};

  if (relativeLane == RelativeLane::LEFT)
  {
    preventiveLaneMarkings.push_back(scm::LaneMarking::Type::Solid_Broken);
  }
  else if (relativeLane == RelativeLane::RIGHT)
  {
    preventiveLaneMarkings.push_back(scm::LaneMarking::Type::Broken_Solid);
  }
  else
  {
    throw std::runtime_error("FeatureExtractor - Invalid AOI used for lane change permission check!");
  }

  if (LaneQueryHelper::IsLaneConsideredAsShoulder(laneInformation) && _mentalModel->IsShoulderLaneUsageLegit(relativeLane))
  {
    preventiveLaneMarkings.clear();
  }

  Side side = LaneQueryHelper::GetSideFromRelativeLane(relativeLane);
  // Is the lane marking at the observed aoi a preventive one
  const scm::LaneMarking::Entity laneMarkingsAoi = _mentalCalculations->GetLaneMarkingAtAoi(vehicle->GetAssignedAoi());
  if (std::find(preventiveLaneMarkings.begin(), preventiveLaneMarkings.end(), laneMarkingsAoi.type) != preventiveLaneMarkings.end() ||
      DoesLaneMarkingBrokenBoldPreventLaneChange(laneMarkingsAoi, side))
  {
    return false;
  }

  return true;
}

bool FeatureExtractor::CheckLaneChange(Side side, bool isEmergecy) const
{
  if (isEmergecy)
  {
    return IsSideLaneSafe(side);
  }

  return IsLaneChangePermittedDueToLaneMarkings(side) && IsSideLaneSafe(side);
}

bool FeatureExtractor::DoesLaneMarkingBrokenBoldPreventLaneChange(const scm::LaneMarking::Entity& laneMarking, Side side) const
{
  if (laneMarking.type == scm::LaneMarking::Type::Broken && LaneQueryHelper::IsLaneMarkingBold(laneMarking))
  {
    if (_mentalCalculations->InterpreteBrokenBoldLaneMarking(0.0_m, side).type == scm::LaneMarking::Type::Solid)
    {
      return true;
    }
  }

  return false;
}

bool FeatureExtractor::IsRightLaneEmptyLaneConvenienceAndHigherThanEgo() const
{
  const bool laneConvenienceRightIsHigherThanEgo{
      _mentalCalculations->GetLaneChangeBehavior().CalculateBaseLaneConvenience(SurroundingLane::RIGHT) >
      _mentalCalculations->GetLaneChangeBehavior().CalculateBaseLaneConvenience(SurroundingLane::EGO)};

  const bool rightLaneIsEmpty{!_mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE) &&
                              !_mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHT_FRONT) &&
                              !_mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHT_FRONT_FAR)};

  return laneConvenienceRightIsHigherThanEgo && rightLaneIsEmpty;  // The situation would allow an overtaking on the right, but this must be prevented due to traffic laws!
}

bool FeatureExtractor::DoesVehicleSurroundingFit(AreaOfInterest aoiToEvaluate) const
{
  if (aoiToEvaluate == AreaOfInterest::LEFT_FRONT || aoiToEvaluate == AreaOfInterest::RIGHT_FRONT)
  {
    return _mentalModel->IsMesoscopicSituationActive(MesoscopicSituation::QUEUED_TRAFFIC);
  }
  else  // Specific AOI related criteria for EGO_FRONT
  {
    return IsRightLaneEmptyLaneConvenienceAndHigherThanEgo();
  }
}

bool FeatureExtractor::IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition(AreaOfInterest aoiToEvaluate) const
{
  const Situation currentSituation{_mentalModel->GetCurrentSituation()};

  if (aoiToEvaluate == AreaOfInterest::LEFT_FRONT || aoiToEvaluate == AreaOfInterest::RIGHT_FRONT)
  {
    // Evaluate current situation of the driver
    return (currentSituation != Situation::FREE_DRIVING && currentSituation != Situation::FOLLOWING_DRIVING);
  }
  else  // Specific AOI related criteria for EGO_FRONT
  {
    // Evaluate current situation of the driver
    return ((currentSituation != Situation::FREE_DRIVING &&
             currentSituation != Situation::FOLLOWING_DRIVING) ||
            _mentalModel->IsMesoscopicSituationActive(MesoscopicSituation::CURRENT_LANE_BLOCKED));
  }
}

bool FeatureExtractor::IsSideLaneSeparateRoadway(bool rightHandTraffic) const
{
  Side side = rightHandTraffic ? Side::Left : Side::Right;
  auto laneMarking{_mentalCalculations->GetLaneMarkingAtDistance(0._m, side)};
  return laneMarking.type == scm::LaneMarking::Type::Broken && LaneQueryHelper::IsLaneMarkingBold(laneMarking);
}

bool FeatureExtractor::IsAoiRelevantForOuterLaneOvertaking(AreaOfInterest aoi, bool rightHandTraffic) const
{
  if (rightHandTraffic)
  {
    return !(aoi == AreaOfInterest::LEFT_FRONT || aoi == AreaOfInterest::EGO_FRONT);
  }
  else
  {
    return !(aoi == AreaOfInterest::RIGHT_FRONT || aoi == AreaOfInterest::EGO_FRONT);
  }
}

bool FeatureExtractor::DoesOuterLaneOvertakingProhibitionApply(AreaOfInterest aoiToEvaluate, bool rightHandTraffic) const
{
  if (IsAoiRelevantForOuterLaneOvertaking(aoiToEvaluate, rightHandTraffic) ||
      IsDriverSituationIrrelevantForOuterLaneOvertakingProhibition(aoiToEvaluate) ||
      DoesVehicleSurroundingFit(aoiToEvaluate) ||
      !_mentalModel->GetIsVehicleVisible(aoiToEvaluate) ||
      !IsInfluencingDistanceViolated(_mentalModel->GetVehicle(aoiToEvaluate)) ||
      _mentalModel->IsMergePreparationActive() ||
      _outerLaneOvertakingProhibitionQuota->IsFulfilled(aoiToEvaluate) ||
      IsSideLaneSeparateRoadway(rightHandTraffic))
    return false;
  else
    return true;
}

bool FeatureExtractor::WillEgoLeaveLaneBeforeCollision(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }

  const auto query{_surroundingVehicleQueryFactory->GetQuery(*vehicle)};
  if (query->IsChangingIntoEgoLane())
  {
    const auto timeToCollision{vehicle->GetTtc().GetValue()};
    const auto timeToLeaveObstruction{query->GetLateralObstructionDynamics().timeToLeave};

    return timeToLeaveObstruction <= timeToCollision;
  }

  return false;  // Currently only used for ego lane. Expand in case of side lane consideration.
}

units::time::second_t FeatureExtractor::GetPassingTime(const SurroundingVehicleInterface* vehicle, units::velocity::meters_per_second_t deltaVelocity) const
{
  auto ttc = vehicle->GetTtc().GetValue();
  auto absDeltaSpeed = units::math::fabs(deltaVelocity);

  auto carLengthObservedVehicle = vehicle->GetLength().GetValue();
  auto carLengthEgo = _mentalModel->GetVehicleModelParameters().bounding_box.dimension.length;

  units::time::second_t timeOfCarLengthOther = ScmDefinitions::INF_TIME;
  units::time::second_t timeOfCarLength = ScmDefinitions::INF_TIME;

  if (absDeltaSpeed != 0._mps)
  {
    timeOfCarLengthOther = carLengthObservedVehicle / absDeltaSpeed;
    timeOfCarLength = carLengthEgo / absDeltaSpeed;
  }

  auto timeSpacer = .5_s;

  return ttc + timeOfCarLength + timeOfCarLengthOther + timeSpacer;
}

bool DetectLaneCrossingConflictSideArea(const SurroundingVehicleQueryInterface& vehicleQuery, SurroundingLane egoLaneChangeDirection, RelativeLane laneOfVehicle)
{
  if (vehicleQuery.IsChangingIntoEgoLane())
  {
    return true;
  }

  if ((egoLaneChangeDirection == SurroundingLane::LEFT && laneOfVehicle == RelativeLane::LEFTLEFT) ||
      (egoLaneChangeDirection == SurroundingLane::RIGHT && laneOfVehicle == RelativeLane::RIGHTRIGHT))
  {
    return vehicleQuery.IsSideSideVehicleChangingIntoSide();
  }

  if ((egoLaneChangeDirection == SurroundingLane::LEFT && laneOfVehicle == RelativeLane::LEFT) ||
      (egoLaneChangeDirection == SurroundingLane::RIGHT && laneOfVehicle == RelativeLane::RIGHT))
  {
    return vehicleQuery.CannotClearLongitudinalObstructionBeforeCollision();
  }

  return false;
}

bool FeatureExtractor::HasActualLaneCrossingConflict(const SurroundingVehicleInterface& vehicle) const
{
  const auto query{_surroundingVehicleQueryFactory->GetQuery(vehicle)};
  if (vehicle.ToTheSideOfEgo())
  {
    const auto egoLaneChangeDirection{_mentalModel->GetCurrentDirection()};
    const auto laneOfPerception{vehicle.GetLaneOfPerception()};
    return DetectLaneCrossingConflictSideArea(*query, egoLaneChangeDirection, laneOfPerception);
  }
  // vehicle not in side area
  if (IsObstacle(&vehicle))
  {
    return static_cast<ObstructionScm>(vehicle.GetLateralObstruction()).isOverlapping;
  }

  if (!query->IsChangingIntoEgoLane() ||     // Observed vehicle is not changing into ego lane or is already in front
      !IsNearEnoughForFollowing(vehicle) ||  // Observed vehicle needs to be in the range of equilibrium distance
      vehicle.GetTtc().GetValue() < 0._s)    // Observed vehicle is faster anyways - cannot pass by without changing speed
  {
    return false;
  }

  return query->CannotClearLongitudinalObstructionBeforeCollision();
}

bool FeatureExtractor::HasAnticipatedLaneCrossingConflict(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }
  const auto query{_surroundingVehicleQueryFactory->GetQuery(*vehicle)};

  if (IsObstacle(vehicle) ||                // Observed vehicle is very slow and will be ignored therefore
      !query->IsChangingIntoEgoLane() ||    // Observed vehicle is not changing into ego lane or is already in front
      !IsNearEnoughForFollowing(*vehicle))  // Observed vehicle needs to be in the range of equilibrium distance
  {
    return false;
  }

  const auto ttc = vehicle->GetTtc().GetValue();
  if (ttc < 0._s)
  {
    return false;  // Observed vehicle is faster anyways - cannot pass by without changing speed
  }

  const bool egoPassingBy = AnticipatedAbleToPass(vehicle,
                                                  query->GetLongitudinalVelocityDelta());  // Can the observed vehicle be passed by?

  return !(egoPassingBy);
}

bool FeatureExtractor::IsExceedingLateralMotionThresholdTowardsEgoLane(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }
  if (_mentalModel->GetLateralVelocityTowardsEgoLane(vehicle->GetAssignedAoi()) < 0._mps)
  {
    return vehicle->IsExceedingLateralMotionThreshold();
  }

  return false;
}

bool FeatureExtractor::IsSideLaneSafe(Side side) const
{
  auto virtualAgents = _mentalModel->GetVirtualAgents();

  bool isVirtual = true;
  if (!virtualAgents.empty())
  {
    std::vector<units::length::meter_t> distanceToVirtualAgents;
    for (auto& virtualVehicle : virtualAgents)
    {
      distanceToVirtualAgents.push_back(virtualVehicle.relativeLongitudinalDistance);
    }
    std::vector<units::length::meter_t>::iterator result = std::min_element(distanceToVirtualAgents.begin(), distanceToVirtualAgents.end());
    int minDistance = std::distance(distanceToVirtualAgents.begin(), result);

    if (virtualAgents[minDistance].lanetype != scm::LaneType::OnRamp && virtualAgents[minDistance].lanetype != scm::LaneType::Entry)
    {
      isVirtual = false;
    }
  }

  if (!_mentalModel->GetLaneExistence(ScmCommons::SideToRelativeLane(side), true))
  {
    return false;
  }

  // When the driver cannot trust the data fully, he will not decide to change lanes due to the potential risk
  if (!_mentalModel->IsDataForLaneChangeReliable(side))
  {
    return false;
  }

  if (_mentalModel->IsSideSideLaneObjectChangingIntoSide(ScmCommons::SideToRelativeLane(side), false))  // TODO use function from SideSideVehicleQuery after PR is in develop
  {
    return false;
  }

  return (IsLaneSafe(side) && isVirtual);
}

bool FeatureExtractor::IsLaneSafe(Side side) const
{
  auto frontVehicle = side == Side::Right
                          ? _mentalModel->GetVehicle(AreaOfInterest::RIGHT_FRONT)
                          : _mentalModel->GetVehicle(AreaOfInterest::LEFT_FRONT);

  auto rearVehicle = side == Side::Right
                         ? _mentalModel->GetVehicle(AreaOfInterest::RIGHT_REAR)
                         : _mentalModel->GetVehicle(AreaOfInterest::LEFT_REAR);

  std::vector<const SurroundingVehicleInterface*> sideVehicles = side == Side::Right
                                                                     ? _mentalModel->GetVehicleVector(AreaOfInterest::RIGHT_SIDE)
                                                                     : _mentalModel->GetVehicleVector(AreaOfInterest::LEFT_SIDE);

  // Side area

  if (!sideVehicles.empty())
  {
    return false;
  }

  bool highwayExit{true};

  if (_mentalModel->IsMesoscopicSituationActive(MesoscopicSituation::MANDATORY_EXIT) &&
      (_mentalModel->GetDistanceToStartOfNextExit() > 0_m || _mentalModel->GetDistanceToEndOfNextExit() > 0_m))
  {
    highwayExit = false;
  }

  if (!IsLaneDriveablePerceived(ScmCommons::SideToRelativeLane(side)) && highwayExit)  // Check logic above if it is required (maybe for merging) else use this logic
  {
    auto lane = _mentalModel->GetInfrastructureCharacteristics()->GetLaneInformationGeometry(ScmCommons::SideToRelativeLane(side));
    if (!LaneQueryHelper::IsLaneConsideredAsShoulder(lane))
    {
      return false;
    }
  }

  // Front area
  double urgencyFactor = 1. - _mentalCalculations->GetUrgencyFactorForLaneChange();
  if (frontVehicle != nullptr)
  {
    if ((IsMinimumFollowingDistanceViolatedWhileCooperating(frontVehicle) && IsCollisionCourseDetected(frontVehicle)) ||
        (urgencyFactor != 1. && IsMinimumFollowingDistanceViolated(frontVehicle, urgencyFactor)))  // Front car is too close and a lane change might lead to a collision
    {
      return false;
    }
  }

  if (rearVehicle != nullptr && _mentalModel->GetLongitudinalActionState() != LongitudinalActionState::ACTIVE_ZIP_MERGING)
  {
    // Rear area
    if ((IsMinimumFollowingDistanceViolatedWhileCooperating(rearVehicle) && IsCollisionCourseDetected(rearVehicle)) ||
        (urgencyFactor != 1. && IsMinimumFollowingDistanceViolated(rearVehicle, urgencyFactor)))  // Rear car is too close, no matter if its approaching or not
    {
      return false;
    }
  }

  return true;
}

bool FeatureExtractor::IsLaneChangeSafe(bool isTransitionTrigger) const
{
  const SurroundingLane direction{_mentalModel->GetCurrentDirection()};
  if (direction == SurroundingLane::EGO)
  {
    return false;
  }

  const RelativeLane targetLane{SurroundingLane2RelativeLane.at(direction)};
  if (!isTransitionTrigger && !_mentalModel->GetLaneExistence(targetLane, true))
  {
    return false;
  }

  // Determine which side lane to check
  if (!isTransitionTrigger)
  {
    const auto sideAoi{direction == SurroundingLane::LEFT ? AreaOfInterest::LEFT_SIDE : AreaOfInterest::RIGHT_SIDE};
    // While in the lane where the lane change started use the existing mechanic
    auto relativelane = AreaOfInterest2RelativeLane.at(sideAoi);

    if (relativelane == RelativeLane::EGO)
    {
      throw std::runtime_error("FeatureExtractor::IsLaneChangeSafe: egoLane not supportet");
    }

    Side side = RelativeLane2Side.at(relativelane);

    return IsSideLaneSafe(side) && IsCurrentLaneSafe();
  }

  static const std::map<SurroundingLane, std::array<AreaOfInterest, 3>> AOIS_TO_CHECK{
      {SurroundingLane::LEFT, {AreaOfInterest::LEFT_FRONT, AreaOfInterest::LEFT_SIDE, AreaOfInterest::LEFT_REAR}},
      {SurroundingLane::RIGHT, {AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_SIDE, AreaOfInterest::RIGHT_REAR}},
  };

  // After transitioning only check the new ego lane for collision threats
  const auto aoisToCheck{AOIS_TO_CHECK.at(direction)};
  // Check the new side lane for vehicles performing a lane change into the ego lane
  bool sideThreat{std::any_of(begin(aoisToCheck), end(aoisToCheck), [this](const AreaOfInterest aoi)
                              {
                          const auto *vehicle {_mentalModel->GetVehicle(aoi)};
                          if(vehicle != nullptr)
                          {
                            return HasActualLaneCrossingConflict(*vehicle);
                          }
                          return false; })};

  return IsCurrentLaneSafe() && !sideThreat;
}

bool FeatureExtractor::IsCurrentLaneSafe() const
{
  auto frontVehicle = _mentalModel->GetVehicle(AreaOfInterest::EGO_FRONT);
  auto rearVehicle = _mentalModel->GetVehicle(AreaOfInterest::EGO_REAR);

  return !((IsMinimumFollowingDistanceViolated(frontVehicle) &&
            IsCollisionCourseDetected(frontVehicle) &&
            !WillEgoLeaveLaneBeforeCollision(frontVehicle)) ||
           (IsMinimumFollowingDistanceViolated(rearVehicle) &&
            IsCollisionCourseDetected(rearVehicle) &&
            !WillEgoLeaveLaneBeforeCollision(rearVehicle)));
}

units::time::second_t FeatureExtractor::TimeToLaneCrossing(const SurroundingVehicleInterface* vehicle, units::velocity::meters_per_second_t vwToEgoLane) const
{
  if (vehicle == nullptr || vehicle->GetLaneOfPerception() == RelativeLane::EGO)
  {
    return 0._s;
  }

  if (vwToEgoLane <= 0._mps)
  {
    return ScmDefinitions::INF_TIME;
  }

  if (!IsExceedingLateralMotionThresholdTowardsEgoLane(vehicle))
  {
    return ScmDefinitions::INF_TIME;
  }

  auto deltaWToEgoLane = _mentalModel->GetDistanceTowardsEgoLane(vehicle->GetAssignedAoi());

  if (deltaWToEgoLane <= 0._m)
  {
    return 0._s;
  }

  return deltaWToEgoLane / vwToEgoLane;
}

units::time::second_t FeatureExtractor::PerceivedTimeToLaneCrossing(const SurroundingVehicleInterface* vehicle) const
{
  auto vwToEgoLane = _mentalModel->GetLateralVelocityTowardsEgoLane(vehicle->GetAssignedAoi());

  return TimeToLaneCrossing(vehicle, vwToEgoLane);
}

units::time::second_t FeatureExtractor::AnticipatedTimeToLaneCrossing(const SurroundingVehicleInterface* vehicle) const
{
  // anticipated value
  constexpr auto vwToEgoLane = 1._mps;
  return TimeToLaneCrossing(vehicle, vwToEgoLane);
}

units::time::second_t FeatureExtractor::GetTimeAtTargetSpeedInLane(SurroundingLane lane) const
{
  if ((lane == SurroundingLane::LEFT && !_mentalModel->GetLaneExistence(RelativeLane::LEFT)) || (lane == SurroundingLane::RIGHT && !_mentalModel->GetLaneExistence(RelativeLane::RIGHT)))
  {
    return 0._s;
  }

  const SurroundingVehicleInterface* vehicle = lane == SurroundingLane::LEFT ? _mentalModel->GetVehicle(AreaOfInterest::LEFT_FRONT) : _mentalModel->GetVehicle(AreaOfInterest::RIGHT_FRONT);
  if (vehicle == nullptr)
  {
    return 99._s;
  }

  auto timeAtTargetSpeed = -(vehicle->GetRelativeNetDistance().GetValue() /
                             (vehicle->GetAbsoluteVelocity().GetValue() - _mentalModel->GetAbsoluteVelocityTargeted()));

  if (timeAtTargetSpeed < 0._s)
  {
    return 99._s;
  }
  return timeAtTargetSpeed;
}

bool FeatureExtractor::CanVehicleCauseCollisionCourseForNonSideAreas(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }

  const auto ttc = vehicle->GetTtc().GetValue();
  if (units::math::abs(ttc) < ScmDefinitions::TTC_LIMIT)
  {
    const double tauDot = vehicle->GetTauDot();
    return ttc >= 0.0_s && tauDot <= ScmDefinitions::TAU_DOT_LIMIT;
  }

  return false;
}

bool FeatureExtractor::IsCollisionCoursePossibleForNonSideAreas(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle != nullptr && IsCollisionRelevantArea(vehicle->GetAssignedAoi()))
  {
    return CanVehicleCauseCollisionCourseForNonSideAreas(vehicle);
  }

  return false;
}

units::time::second_t FeatureExtractor::DetermineTimeToChangeSides(const SurroundingVehicleInterface* vehicle) const
{
  const auto carWidthObservedVehicle = _mentalModel->GetProjectedDimensions(vehicle->GetAssignedAoi()).widthProjected;
  const auto vwToEgoLane = _mentalModel->GetLateralVelocityTowardsEgoLane(vehicle->GetAssignedAoi());

  return (vwToEgoLane != 0._mps) ? carWidthObservedVehicle / units::math::fabs(vwToEgoLane) : ScmDefinitions::INF_TIME;
}

bool FeatureExtractor::CantPreventCollisionCourse(const SurroundingVehicleInterface* vehicle) const
{
  const auto timeToChangeSides = DetermineTimeToChangeSides(vehicle);
  const auto tlc = PerceivedTimeToLaneCrossing(vehicle);
  auto constexpr timeSpacer = .5_s;
  const auto effectiveTtc = vehicle->GetTtc().GetValue() + timeSpacer;

  if (effectiveTtc >= (tlc + timeToChangeSides))
    return false;

  return true;
}

bool FeatureExtractor::IsCollisionCourseDetectedNonSideArea(const SurroundingVehicleInterface* vehicle) const
{
  bool collisionPossible = IsCollisionCoursePossibleForNonSideAreas(vehicle);

  if (collisionPossible && (vehicle->GetLaneOfPerception() == RelativeLane::EGO && vehicle->IsExceedingLateralMotionThreshold()))
  {
    collisionPossible = CantPreventCollisionCourse(vehicle);
  }

  return collisionPossible;
}

bool FeatureExtractor::IsCollisionCoursePossibleForSideAreas(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }

  const auto vDeltaObservedMinusEgo{vehicle->GetLateralVelocity().GetValue() - _mentalModel->GetLateralVelocity()};
  const auto relativeNetDistance = vehicle->GetRelativeNetDistance().GetValue();
  const auto ttc = CalculateTimeToCollision(vDeltaObservedMinusEgo, relativeNetDistance);

  return IsTTCInBounds(ttc);
}

units::time::second_t FeatureExtractor::CalculateTimeToCollision(const units::velocity::meters_per_second_t vDeltaObservedMinusEgo, const units::length::meter_t relativeNetDistance) const
{
  return ScmCommons::CalculateTimeToCollision(vDeltaObservedMinusEgo, relativeNetDistance);
}

bool FeatureExtractor::IsTTCInBounds(const units::time::second_t ttc) const
{
  constexpr units::time::second_t LOWER_TTC_BOUND{0.};
  constexpr units::time::second_t UPPER_TTC_BOUND{ScmDefinitions::TTC_LIMIT};

  return LOWER_TTC_BOUND < ttc && ttc < UPPER_TTC_BOUND;
}

bool FeatureExtractor::IsCollisionCourseDetectedSideArea(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }

  if (IsCollisionCoursePossibleForSideAreas(vehicle) &&
      vehicle->GetLaneOfPerception() == RelativeLane::EGO &&
      vehicle->IsExceedingLateralMotionThreshold() &&
      CantPreventCollisionCourse(vehicle))
  {
    return true;
  }
  return false;
}

bool FeatureExtractor::IsCollisionRelevantArea(AreaOfInterest aoi) const
{
  return aoi == AreaOfInterest::EGO_FRONT || aoi == AreaOfInterest::EGO_FRONT_FAR ||
         aoi == AreaOfInterest::LEFT_FRONT || aoi == AreaOfInterest::LEFT_FRONT_FAR ||
         aoi == AreaOfInterest::RIGHT_FRONT || aoi == AreaOfInterest::RIGHT_FRONT_FAR ||
         aoi == AreaOfInterest::EGO_REAR || aoi == AreaOfInterest::LEFT_REAR ||
         aoi == AreaOfInterest::RIGHT_REAR;
}

bool FeatureExtractor::IsCollisionCourseDetected(const SurroundingVehicleInterface* vehicle) const
{
  // not only returns true to ACTUAL collision courses (ego front and back), but also to POSSIBLE collision courses
  // e.g. if a lane change left starts, will ego collide with left aois

  if (vehicle == nullptr)
  {
    return false;
  }

  if (vehicle->ToTheSideOfEgo())
  {
    return IsCollisionCourseDetectedSideArea(vehicle);
  }
  return IsCollisionCourseDetectedNonSideArea(vehicle);
}

bool FeatureExtractor::IsLaneDriveablePerceived(RelativeLane relativeLane) const
{
  return _mentalModel->GetInfrastructureCharacteristics()->GetLaneInformationGeometry(relativeLane).distanceToEndOfLane >
         _mentalModel->GetInfluencingDistanceToEndOfLane();
}

bool FeatureExtractor::IsLaneDriveablePerceived() const
{
  auto distToEndOfLane = _mentalModel->GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().distanceToEndOfLane;
  return distToEndOfLane > _mentalModel->GetInfluencingDistanceToEndOfLane();
}

bool FeatureExtractor::IsLaneTypeDriveable(scm::LaneType type) const
{
  switch (type)
  {
    case scm::LaneType::Driving:
    case scm::LaneType::Exit:
    case scm::LaneType::Entry:
    case scm::LaneType::OnRamp:
    case scm::LaneType::OffRamp:
    case scm::LaneType::ConnectingRamp:
      return true;
    case scm::LaneType::Shoulder:
    case scm::LaneType::Stop:
    case scm::LaneType::Border:
    case scm::LaneType::Restricted:
    case scm::LaneType::None:
    case scm::LaneType::Parking:
    case scm::LaneType::Median:
    case scm::LaneType::Biking:
    case scm::LaneType::Sidewalk:
    case scm::LaneType::Curb:
      return false;
    default:
      return false;
  }
}

bool FeatureExtractor::IsMinimumFollowingDistanceViolatedWhileCooperating(const SurroundingVehicleInterface* vehicle) const
{
  if (vehicle == nullptr)
  {
    return false;
  }

  const auto ttcThresold{3._s};
  const bool isOutsideApproachingThreshold{vehicle->GetTtc().GetValue() > ttcThresold};

  const bool isRelevant{(_mentalCalculations->EgoBelowJamSpeed() && isOutsideApproachingThreshold) || _mentalModel->GetMergeRegulateId() == vehicle->GetId()};

  if (isRelevant)
  {
    // Note: reductionFactor and urgency have the same effect, so the urgency function is used here
    const double reductionFactor{.8};  // Factor of MinFollowingDistance that is considered enough (i.e. .8 = 80% of MinFollowingDistance is enough)
    return IsMinimumFollowingDistanceViolated(vehicle, reductionFactor);
  }
  return IsMinimumFollowingDistanceViolated(vehicle);
}

bool FeatureExtractor::HasSideLaneNoCollisionCourses(Side side) const
{
  if (side == Side::Left)
  {
    auto sideVehicles = _mentalModel->GetVehicleVector(AreaOfInterest::LEFT_SIDE);
    bool hasCollisionCourse = std::any_of(sideVehicles.begin(), sideVehicles.end(), [this](const auto* vehicle)
                                          { return IsCollisionCourseDetected(vehicle); });

    return !hasCollisionCourse &&
           !IsCollisionCourseDetected(_mentalModel->GetVehicle(AreaOfInterest::LEFT_FRONT)) &&
           !IsCollisionCourseDetected(_mentalModel->GetVehicle(AreaOfInterest::LEFT_REAR));
  }
  else
  {
    auto sideVehicles = _mentalModel->GetVehicleVector(AreaOfInterest::RIGHT_SIDE);
    bool hasCollisionCourse = std::any_of(sideVehicles.begin(), sideVehicles.end(), [this](const auto* vehicle)
                                          { return IsCollisionCourseDetected(vehicle); });

    return !hasCollisionCourse &&
           !IsCollisionCourseDetected(_mentalModel->GetVehicle(AreaOfInterest::RIGHT_FRONT)) &&
           !IsCollisionCourseDetected(_mentalModel->GetVehicle(AreaOfInterest::RIGHT_REAR));
  }
}

bool FeatureExtractor::HasDrivableSuccessor(bool isEmergency, RelativeLane relativeLane) const
{
  const auto relativeLaneId = RelativeLane2RelativeLaneId.at(relativeLane);

  if (_mentalModel->GetDistanceToEndOfLane(relativeLaneId) != ScmDefinitions::INF_DISTANCE)
  {
    auto laneInformation = _mentalModel->GetInfrastructureCharacteristics()->GetLaneInformationGeometry(relativeLane);

    if (laneInformation.laneTypeSuccessor.has_value())
    {
      if (isEmergency && (laneInformation.laneTypeSuccessor.value() == scm::LaneType::Shoulder || laneInformation.laneTypeSuccessor.value() == scm::LaneType::Stop))
      {
        return true;
      }
      else
      {
        return IsLaneTypeDriveable(laneInformation.laneType);
      }
    }
  }
  return false;
}
