/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  AlgorithmSceneryCarInterface.h

#pragma once

#include "ActionImplementation.h"
#include "ActionImplementationInterface.h"
#include "ActionManagerInterface.h"

class AlgorithmSceneryCarInterface
{
public:
  virtual ~AlgorithmSceneryCarInterface() = default;

  //! \brief
  //! \details
  //! \param [in] mentalModel             Holds all functionality regarding perceived environment
  //! \param [in] actionManager           Holds all functionality regarding action patterns
  //! \param [in] actionImplementation    Deduces action implementations from the stochastic cognitive model
  //! \param [in] laneChangeAction        The current lane change action of the agent
  virtual void ForcedLaneChange(ActionManagerInterface* actionManager,
                                ActionImplementationInterface* actionImplementation,
                                LaneChangeAction laneChangeAction) = 0;

  //! \brief Return status of the variable _resetReactionBaseTime after the new action to force a lane change has been issued
  //! \return yes/no
  virtual bool ResetReactionBaseTime() = 0;
};