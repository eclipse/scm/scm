/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  InformationAcquisition.h

#pragma once

#include "Extrapolation.h"
#include "FeatureExtractorInterface.h"
#include "InformationAcquisitionInterface.h"
#include "MentalCalculationsInterface.h"
#include "MicroscopicCharacteristics.h"
#include "ScmDefinitions.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/OsiQlData.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

//! @brief functionality for interfacing between the physical data and the data stored internally by an agent.
//! @details This component is responsible for acquiring data, applying possible perceptive distortions and biases, and for storing
//! that data in OpticalData, so that it can then be integrated into the MentalModel. When fixation in a certain AoI is
//! completed, this triggers a signal to information acquisition for that AoI.
class InformationAcquisition : public InformationAcquisitionInterface
{
public:
  //! @brief InformationAcquisition Constructor.
  //! @param mentalModel         Interface to the mental processes of the stochastic cognitive model
  //! @param featureExtractor    Interface to FeatureExtractor to avoid accessing it through MentalModel
  //! @param mentalCalculations  Interface to MentalCalculations to avoid accessing it through MentalModel
  //! @param ownVehicleInformationSCM  Interface to MentalCalculations to avoid accessing it through MentalModel
  //! @param surroundingObjectsSCM  Interface to MentalCalculations to avoid accessing it through MentalModel
  //! @param idealPerception  Interface to MentalCalculations to avoid accessing it through MentalModel
  InformationAcquisition(MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations, OwnVehicleInformationSCM& ownVehicleInformationSCM, SurroundingObjectsSCM& surroundingObjectsSCM, bool idealPerception, OwnVehicleRoutePose& ownVehicleRoutePose);

  void SetOwnVehicleHelperVariables() const override;
  void UpdateSurroundingVehicleData(bool forceUpdate, bool isNewInformationAcquisitionRequested) const override;
  void UpdateOwnVehicleData(OwnVehicleInformationSCM* ownVehicle_GroundTruth) override;
  void ExtrapolateOwnVehicleData(OwnVehicleInformationSCM* ownVehicle_GroundTruth) override;
  void UpdateEgoVehicleData(bool forceUpdate) override;
  void ResetAoiDataForNoObject(ObjectInformationScmExtended* surroundingObject) const override;
  void SetAoiDataWithGroundTruth(ObjectInformationScmExtended* surroundingObject, const ObjectInformationSCM* surroundingObject_GroundTruth, AreaOfInterest aoi) const override;
  std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>> PerceiveSurroundingVehicles(bool idealPerception) const override;
  void CorrectAoiDataDueToLongitudinalTransitionFromSideArea(ObjectInformationScmExtended* object, AreaOfInterest newAoi) const override;
  void CorrectAoiDataDueToLongitudinalTransitionToSideArea(ObjectInformationScmExtended* object) const override;
  std::vector<int> GetVisibleAgentIds() const override;
  bool IsOutOfSight(units::length::meter_t relativeLongitudinalDistance) const override;
  units::time::millisecond_t GetCycleTime() const override;
  units::time::millisecond_t GetTime() const override;

protected:
  //! @brief Marks given inconsistent AOIs as inconsistent in MentalModel, clears inconsistency from AOIs in UFOV if they are not inconsistent.
  //! @param triggerInformationAcquisition
  //! @param inconsistentAois list of inconsistent aois
  void UpdateInconsistencies(bool triggerInformationAcquisition, const std::vector<AreaOfInterest>& inconsistentAois) const;

  //! @brief Extrapolates vehicle information in the lateral direction.
  //! @param surroundingObject Object to extrapolate
  //! @param aoi               AreaOfInterest in which the object was perceived
  void ExtrapolateSurroundingVehicleDataLateral(ObjectInformationScmExtended* surroundingObject, AreaOfInterest aoi) const;

  //! @brief Extrapolate longitudinal acceleration
  //! @param surroundingObject    Currently observed surrounding object
  void ExtrapolateLongitudinalAcceleration(ObjectInformationScmExtended* surroundingObject) const;

private:
  //! @brief Acquire FOVEA quality data for every area of interest around the agent
  void AcquireIdealOpticalData() const;

  //! @brief Assign focus area (field of view assignment) specific optical data for a surrounding object.
  //! @param surroundingObject                All surrounding object data as known to the agent in the mental model
  //! @param surroundingObject_GroundTruth    All ground truth surrounding vehicle data
  //! @param ownVehicle                       All ego vehicle data as known to the agent in the mental model
  void AssignFocusAreaSpecificOpticalDataForSurroundingObject(ObjectInformationScmExtended* surroundingObject,
                                                              const ObjectInformationSCM* surroundingObject_GroundTruth,
                                                              const OwnVehicleInformationScmExtended* ownVehicle) const;

  //! @brief Assign area of interest specific optical data for a surrounding object.
  //! @param surroundingObject                All surrounding object data as known to the agent in the mental model
  //! @param surroundingObject_GroundTruth    All ground truth surrounding vehicle data
  //! @param ownVehicle                       All ego vehicle data as known to the agent in the mental model
  //! @param aoi                              Observed area of interest
  void AssignAoiSpecificOpticalDataForSurroundingObject(ObjectInformationScmExtended* surroundingObject,
                                                        const ObjectInformationSCM* surroundingObject_GroundTruth,
                                                        const OwnVehicleInformationScmExtended* ownVehicle,
                                                        AreaOfInterest aoi) const;

  //! @brief Gets all the objects related to the given aoi
  //! @param objects
  //! @param aoi
  //! @throws std::runtime_error if there is no data associated in objects with the aoi
  //! @return Objects in the given aoi
  std::vector<ObjectInformationSCM>* GetTrafficObjectsFromAoiAllVectors(OngoingTrafficObjectsSCM* objects, AreaOfInterest aoi) const;

  //! @brief Combines AOIs of Fovea and Ufov and avoids duplicates.
  std::vector<AreaOfInterest> CombineFoveaAndUfov() const;

  //! @brief Corrects Merge and Aoi Regulate Side index after size of Side vector has changed during SurroundingVehicles update.
  void CorrectSideAoiIndices() const;

  //! @brief Returns all existion objects from the ground truth data.
  std::vector<std::pair<const ObjectInformationSCM*, AreaOfInterest>> GetExistingGroundTruthObjects() const;

  //! @brief Interface to the MentalModel
  MentalModelInterface& _mentalModel;
  //! @brief Interface to the FeatureExtractor
  const FeatureExtractorInterface& _featureExtractor;
  //! @brief Interface to the MentalCalculations
  const MentalCalculationsInterface& _mentalCalculations;

  //! @brief Module responsible to extrapolate data for which there is no reliable update
  const std::unique_ptr<Extrapolation> _extrapolation;

  //! @brief Reference to the ownVehicleInformation
  OwnVehicleInformationSCM& _ownVehicleInformationSCM;

  //! @brief Ground truth traffic objects
  SurroundingObjectsSCM& _surroundingObjectsSCM;

  //! @brief Flag indicating if the agent has a perfect perception of its environment
  bool _idealPerception;

  //! @brief Cycletime of the simulation in ms.
  units::time::millisecond_t _cycleTime{};

  OwnVehicleRoutePose& _ownVehicleRoutePose;

  //! @brief Distance, below which an agent can recognise the acceleration of an object [m].
  constexpr static units::length::meter_t ACCELERATION_SENSING_DISTANCE = 100._m;

  //! @brief Estimation of ego velocity will be misjudged by this factor
  double _estimateFactorForVEgo{.9};
};