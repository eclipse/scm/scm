/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file AnticipatedLaneChangerModel.h
#pragma once

#include <cmath>

#include "../MicroscopicCharacteristics.h"
#include "LaneChangerCachedData.h"
#include "MentalModelInterface.h"

class MicroscopicCharacteristics;
class MentalCalculationsInterface;

//! *****************************************************************************************************************
//! @brief This component is an interface to probability distributions for the bayesian probability estimator
//! inside the HighCognitve-Class. It determines the probability of a lane change from the lane to the right of ego.
//! @details This class provides an interface to get the probability to observe a value during anticipating
//! a lanechange p( evidence_i | LC ) and to observe the value in general p( evidence_i ).
//!
//! The bayesian estimator follows this formula to calculate the probability of a lane change:
//!
//!                     p( evidence_1 | LC ) p( evidence_2 | LC ) ... p( evidence_n | LC ) p( LC )^n
//! p( LC | evidence ) = ---------------------------------------------------------------------------
//!                               p( evidence_1 ) p( evidence_2 ) ... p( evidence_n )
//!
//! Where n is the number of evidence features like thwRightFront and p(evidence) the general
//! probability to observe a feature of specific value given by an observation.
//! *****************************************************************************************************************
class AnticipatedLaneChangerModel
{
public:
  AnticipatedLaneChangerModel(MicroscopicCharacteristicsInterface* microscopicData,
                              MentalCalculationsInterface& mentalCalculations,
                              const MentalModelInterface& mentalModel);
  AnticipatedLaneChangerModel(const AnticipatedLaneChangerModel&) = delete;
  AnticipatedLaneChangerModel(AnticipatedLaneChangerModel&&) = delete;
  AnticipatedLaneChangerModel& operator=(const AnticipatedLaneChangerModel&) = delete;
  AnticipatedLaneChangerModel& operator=(AnticipatedLaneChangerModel&&) = delete;
  virtual ~AnticipatedLaneChangerModel() = default;

  //! @brief Estimates the probability of a lane change by using the bayes-method
  //! @return double
  double GetLaneChangeProbability(void);

  //! @brief gets p( thwRightFront | LC ) * ... * p( evidence_n | LC) with an EgoFront-vehicle present
  //! @return double
  double GetLaneChangeEvidenceLikelihoodWithEgoFrontVehicle(void);

  //! @brief gets general p( thwRightFront ) * ... * p( evidence_n ) with an EgoFront-vehicle present
  //! @return double
  double GetGeneralEvidenceProbabilityWithEgoFrontVehicle(void);

  //! @brief gets p( thwRightFront | LC ) * ... * p( evidence_n | LC) with an EgoFront-vehicle present
  //! @return double
  double GetLaneChangeEvidenceLikelihoodWithoutEgoFrontVehicle(void);

  //! @brief gets general p( thwRightFront ) * ... * p( evidence_n ) without an EgoFront-vehicle present
  //! @return double
  double GetGeneralEvidenceProbabilityWithoutEgoFrontVehicle(void);

  //! @brief weightens the bayesian probability estimation of a lane change with a logistics function
  //! @return double
  double GetLogisticRegressionWeight(double bayesianPosterior);

  //! @brief Returns TtcInverse RightFront to RightFrontFar
  //! @return double
  double GetTtcInverseRightFrontToRightFrontFar();

  MicroscopicCharacteristicsInterface* _microscopicData = nullptr;
  MentalCalculationsInterface& _mentalCalculations;

  //! @brief Returns thwRightFront
  //! @return double
  virtual double GetThwRightFront(void);

  //! @brief Returns TtcInverse RightFront
  //! @return double
  virtual double GetTtcInverseRightFront(void);

  //! @brief Returns ThwDot RightFront
  //! @return double
  virtual double GetThwDotRightFront(void);

  //! @brief Returns TtcInverseDot RightFront
  //! @return double
  virtual double GetTtcInverseDotRightFront(void);

  //! @brief Returns TtcInverse EgoFront
  //! @return double
  virtual double GetTtcInverseEgoFront(void);

  //! @brief Returns TtcInverseDot EgoFront
  //! @return double
  virtual double GetTtcInverseDotEgoFront(void);

  //! @brief returns the inversed TTC between two aoi's.
  //! @param aoi_first AreaOfInterest
  //! @param aoi_second AreaOfInterest
  //! @return TTCInverse
  double GetTtcInverseBetweenTwoAreasOfInterest(AreaOfInterest aoi_first, AreaOfInterest aoi_second) const;

private:
  //! @brief checks, if RightFront and RightFrontFar metrics are all valid
  //! @return bool
  bool CheckMetricsAvailable(void);

  //! @brief returns key from value for lookup table
  //! @return int
  int GetMapKeyFromValue(double value);

  //! @brief updates metricsArray with needed metrics for lane change anticipation
  //! @returns double*
  void UpdateMetricsArray();

  //! @brief array to save anticipation metrics
  double metricsArray[7] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

  //! @brief prior probability of a Lane Change with EgoFront vehicle present
  static constexpr double PRIOR_LANE_CHANGE_WITH_EGO_FRONT_VEHICLE = 0.49641;

  //! @brief prior probability of a Lane Change without EgoFront vehicle present
  static constexpr double PRIOR_LANE_CHANGE_WITHOUT_EGO_FRONT_VEHICLE = 0.5011;

  //! @brief upper limit of the lookup-table
  static constexpr int MAX_LOOKUP_INDEX = 4001;

  //! @brief lower limit of the lookup-table
  static constexpr int MIN_LOOKUP_INDEX = 1;

  //! @brief Resolution of lookup-table
  static constexpr double TABLE_RESOLUTION = 0.001;

  //! @brief Parameter for the Logistics Function from Regression
  static constexpr double BETA_0 = 2.8696;

  //! @brief Parameter for the Logistics Function from Regression
  static constexpr double BETA_1 = -4.5002;

  //! @brief Parameter for Time Headway
  static constexpr double THW_OUT_OF_REACH = 999.0;

  //! @brief Interface to the MentalModel
  const MentalModelInterface& _mentalModel;
};