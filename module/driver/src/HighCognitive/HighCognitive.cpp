/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "HighCognitive.h"

#include "MentalCalculationsInterface.h"
#include "MentalModelInterface.h"

HighCognitive::HighCognitive(MicroscopicCharacteristicsInterface* _microscopicData, MentalCalculationsInterface& _mentalCalculations, const MentalModelInterface& mentalModel, bool isActive)
    : microscopicData{_microscopicData}, mentalCalculations{_mentalCalculations}, _mentalModel(mentalModel), _isHighCognitiveActive{isActive}
{
  anticipatedLaneChangerModel = std::make_unique<AnticipatedLaneChangerModel>(microscopicData, mentalCalculations, _mentalModel);
}

int HighCognitive::DetermineCurrentSituationPattern()
{
  const float thwRightFrontToRightFrontFar{
      static_cast<float>(mentalCalculations.GetTimeHeadwayBetweenTwoAreasOfInterest(AreaOfInterest::RIGHT_FRONT,
                                                                                    AreaOfInterest::RIGHT_FRONT_FAR))};

  const float ttcRightFrontToRightToFrontFarInverted{static_cast<float>(anticipatedLaneChangerModel->GetTtcInverseRightFrontToRightFrontFar())};

  // Return undefinded, if Ttc or Thw was not valid
  if (thwRightFrontToRightFrontFar <= 0. || std::isnan(ttcRightFrontToRightToFrontFarInverted) || std::isinf(ttcRightFrontToRightToFrontFarInverted))
  {
    return -1;
  }

  float features[2] = {ttcRightFrontToRightToFrontFarInverted, thwRightFrontToRightFrontFar};
  int trafficSituationPattern = PredictTrafficSituation(features);

  if (trafficSituationPattern == 0)
  {
    trafficSituationPattern = PredictApproachSaliency(features);
  }
  else if (trafficSituationPattern == 1)
  {
    trafficSituationPattern = PredictFollowSaliency(features);
    if (trafficSituationPattern == 0)
    {
      trafficSituationPattern = 2;
    }
    else
    {
      trafficSituationPattern = 3;
    }
  }
  else
  {
    return -1;  // No Traffic Situation
  }
  return trafficSituationPattern;
}

void HighCognitive::InsertPatternToDeque(int situationPatternLabel)
{
  if (observedSituationPattern.size() < movingAverageMaxWindowSize)
  {
    observedSituationPattern.push_front(situationPatternLabel);
  }
  else
  {
    observedSituationPattern.pop_back();
    observedSituationPattern.push_front(situationPatternLabel);
  }
}

bool HighCognitive::RightFrontAndRightFrontFarAvailable()
{
  auto* front{microscopicData->GetObjectInformation(AreaOfInterest::RIGHT_FRONT)};
  auto* frontFar{microscopicData->GetObjectInformation(AreaOfInterest::RIGHT_FRONT_FAR)};
  return front && frontFar && front->relativeLongitudinalDistance < frontFar->relativeLongitudinalDistance;
}

void HighCognitive::AdvanceHighCognitive(int time, bool externalControlActive)
{
  if (externalControlActive)  // External control just activated
  {
    _isHighCognitiveActive = false;
    return;
  }
  else if (!_isHighCognitiveActive)  // External control just deactivated
  {
    observedSituationPattern.clear();  // Clear outdated entires before executing following code
    _isHighCognitiveActive = true;
  }  // Remaining state "_isHighCognitiveActive && !externalControlActive" is just the normal case, so just execute following code

  if (time > advanceTime)
  {
    if (RightFrontAndRightFrontFarAvailable())
    {
      InsertPatternToDeque(DetermineCurrentSituationPattern());
      int situationCounter[5] = {};

      // Count observed traffic situations
      for (unsigned int i = 0; i < observedSituationPattern.size(); ++i)
      {
        if (observedSituationPattern[i] == -1)
        {
          situationCounter[4]++;
        }
        else
        {
          situationCounter[static_cast<unsigned int>(observedSituationPattern[i])]++;
        }
      }

      int index = 0;
      for (int i = 1; i < 5; i++)
      {
        index = situationCounter[i] > situationCounter[index] ? i : index;
      }

      switch (index)
      {
        case 0:
          currentHighCognitiveSituation = HighCognitiveSituation::APPROACH;
          break;
        case 1:
          currentHighCognitiveSituation = HighCognitiveSituation::SALIENT_APPROACH;
          break;
        case 2:
          currentHighCognitiveSituation = HighCognitiveSituation::FOLLOW;
          break;
        case 3:
          currentHighCognitiveSituation = HighCognitiveSituation::SALIENT_FOLLOW;
          break;
        default:
          currentHighCognitiveSituation = HighCognitiveSituation::UNDEFINED;
          break;
      }
    }
    else
    {
      currentHighCognitiveSituation = HighCognitiveSituation::UNDEFINED;
    }

    advanceTime = time;
    anticipatedLaneChangeProbability = std::nan("");
  }
}

double HighCognitive::AnticipateLaneChangeProbability(int time)
{
  if (_isHighCognitiveActive && time == advanceTime)
  {
    if (std::isnan(anticipatedLaneChangeProbability))
    {
      anticipatedLaneChangeProbability = anticipatedLaneChangerModel->GetLaneChangeProbability();
    }

    return anticipatedLaneChangeProbability;
  }
  return 0.0;
}

HighCognitiveSituation HighCognitive::GetCurrentSituation(int time) const
{
  return _isHighCognitiveActive && time == advanceTime ? currentHighCognitiveSituation : HighCognitiveSituation::UNDEFINED;
}
