/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <variant>

#include "ScmDefinitions.h"

class MicroscopicCharacteristicsInterface
{
public:
  virtual ~MicroscopicCharacteristicsInterface() = default;

  //! \brief Get a pointer that allows the manipulation of the stored microsocpic information concerning the own vehicle
  //! \return ownVehicleInformation
  virtual OwnVehicleInformationScmExtended* UpdateOwnVehicleData() = 0;

  //! \brief Get a pointer that allows the manipulation of the stored microsocpic information concerning a surrounding vehicle
  //! \param aoi      Area of interest
  //! \param [in] sideIndex    Index of the object in the side aoi vector
  //! \return surroundingObjectInformation of the specified area of interest
  virtual ObjectInformationScmExtended* UpdateObjectInformation(AreaOfInterest aoi, int sideIndex = -1) = 0;

  virtual std::vector<ObjectInformationScmExtended>* UpdateSideObjectsInformation(AreaOfInterest aoi) = 0;
  virtual const ObjectInformationScmExtended* GetObjectInformation(AreaOfInterest aoi, int sideIndex = -1) const = 0;
  virtual const std::vector<ObjectInformationScmExtended>* GetSideObjectsInformation(AreaOfInterest aoi) const = 0;

  virtual int GetSideAoiAoiRegulateIndex() const = 0;
  virtual const OwnVehicleInformationScmExtended* GetOwnVehicleInformation() const = 0;
  virtual void UpdateSideAoiMergeRegulateIndex(int index) = 0;
  virtual int GetSideAoiMergeRegulateIndex() const = 0;
  virtual void UpdateSideAoiAoiRegulateIndex(int index) = 0;
  virtual const SurroundingObjectsScmExtended* GetSurroundingVehicleInformation() const = 0;
  virtual std::vector<ObjectInformationScmExtended>* GetSideObjectVector(AreaOfInterest aoi) const = 0;
  virtual std::vector<ObjectInformationScmExtended>* GetObjectVector(AreaOfInterest aoi) const = 0;
  virtual void Initialize(units::velocity::meters_per_second_t initVEgo) = 0;
};
