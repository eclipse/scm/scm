/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file TrajectoryPlanningTypes.h
//! Contains datatypes required for trajectory planning.
#pragma once

#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "TrajectoryCalculations/TrajectoryCalculationTypes.h"
#include "include/common/ScmDefinitions.h"

namespace TrajectoryPlanning
{

//! @brief Parameters required to calculate the width of a lane change.
struct LaneChangeWidthParameters
{
  //! @brief width of ego lane
  units::length::meter_t egoLaneWidth{0};
  //! @brief width of neighboring lane (target lane of the intended lane change)
  units::length::meter_t neighbourLaneWidth{0};
  //! @brief the t coordinate of ego lane when the lane change is planned
  units::length::meter_t tStart{0};
  //! @brief t coordinate of the target lane (if set to zero, the lane change will end in the middle/center of the target lane)
  units::length::meter_t tEnd{0};
  //! @brief Relative lane (i.e. target lane) of the lane change
  RelativeLane relativeLane{RelativeLane::EGO};
};

//! @brief Vehicles that might be limiting the maximum available lane change length.
struct RelevantVehiclesForLaneChangeLength
{
  //! @brief the vehicle right in front of ego agent in the same lane (might be null if not present)
  const SurroundingVehicleInterface* egoFrontVehicle{nullptr};
  //! @brief the vehicle which causes a "front situation" (e.g. for swerving maneuvers; might be null if not present)
  const SurroundingVehicleInterface* causingVehicleFrontSituation{nullptr};
  //! @brief the vehicle which causes a "side situation" (e.g. another vehicle perfoming a lane change in front of us; might be null if not present)
  const SurroundingVehicleInterface* causingVehicleSideSituation{nullptr};
};

//! @brief Information about the current highway exit, relevant to determine available lane change length.
struct HighwayExitInformation
{
  //! @brief distance to the end of the exit lane
  units::length::meter_t distanceToEndOfExit{};
  //! @brief how many lanes ego has to cross to reach the exit lane
  int numberOfLanesToCross{};
  //! @brief the minimum time ego should drive on the exit lane (this should prevent risky lane changes right at the end of the exit lane)
  units::time::second_t minimumTimeOnExitLane{2._s};

  bool operator==(const HighwayExitInformation& other) const;
  bool operator!=(const HighwayExitInformation& other) const;
};

//! @brief Checks for equality of two HighwayExitInformation
//! @param other the other
//! @return true if they are the same, false otherwise
inline bool HighwayExitInformation::operator==(const HighwayExitInformation& other) const
{
  return units::math::abs(distanceToEndOfExit - other.distanceToEndOfExit) < units::length::meter_t(ScmDefinitions::EPSILON) &&
         numberOfLanesToCross == other.numberOfLanesToCross &&
         units::math::abs(minimumTimeOnExitLane - other.minimumTimeOnExitLane) < units::time::second_t(ScmDefinitions::EPSILON);
}

//! @brief Checks for inequality of two HighwayExitInformation
//! @param other the other
//! @return true if they differ, false otherwise
inline bool HighwayExitInformation::operator!=(const HighwayExitInformation& other) const
{
  return !(*this == other);
}

//! @brief Parameters required to determine the available lane change length.
struct LaneChangeLengthParameters
{
  //! @brief the time of a free lane change (i.e. one without any other influences)
  units::time::second_t timeHeadwayFreeLaneChange{};
  //! @brief longitudinal ego velocity
  units::velocity::meters_per_second_t egoVelocityLongitudinal{};
  //! @brief distance till the ego lane ends
  units::length::meter_t distanceToEndOfEgoLane{};
  //! @brief predominant vehicle for a lane change
  const SurroundingVehicleInterface* laneChangeObstacle{nullptr};
  //! @brief additional information for a lane change when the next highway exit should be taken
  std::optional<HighwayExitInformation> highwayExitInformation{std::nullopt};
};

//! @brief Parameters required to determine if a lane change with these parameters is safe.
struct SafetyParameters
{
  //! @brief longitudinal ego velocity
  units::velocity::meters_per_second_t egoVelocityLongitudinal{};
  //! @brief calculated lane change length
  units::length::meter_t laneChangeLength{};
  //! @brief the minimum distance (to another vehicle)
  units::length::meter_t minDistance{};
  //! @brief the closest (or relevant) vehicle on the target lane
  const SurroundingVehicleInterface* closestVehicleOnTargetLane{nullptr};
};

//! @brief Shorter notation of TrajectoryCalculations::TrajectoryDimensions
using TrajectoryDimensions = TrajectoryCalculations::TrajectoryDimensions;
//! @brief Shorter notation of TrajectoryCalculations::Trajectory
using Trajectory = TrajectoryCalculations::Trajectory;

}  // namespace TrajectoryPlanning