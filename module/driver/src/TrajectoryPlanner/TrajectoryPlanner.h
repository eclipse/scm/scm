/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file TrajectoryPlanner.h

#pragma once

#include <memory>

#include "LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculationsInterface.h"
#include "MentalModelInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "TrajectoryPlanner/TrajectoryPlannerInterface.h"
#include "TrajectoryPlanner/TrajectoryPlanningQuery.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"

//! @brief Implements TrajectoryPlannerInterface
class TrajectoryPlanner : public TrajectoryPlannerInterface
{
public:
  //! @brief Constructor that sets internal members to given parameters.
  //! @param planningQuery
  //! @param laneChangeTrajectoryCalculations
  //! @param surroundingVehicleQueryFactory
  TrajectoryPlanner(std::shared_ptr<TrajectoryPlanningQueryInterface> planningQuery,
                    std::shared_ptr<LaneChangeTrajectoryCalculationsInterface> laneChangeTrajectoryCalculations,
                    const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory);

  TrajectoryPlanning::Trajectory PlanTrajectory(const TrajectoryPlanning::TrajectoryDimensions& dimensions) const override;
  TrajectoryPlanning::TrajectoryDimensions CalculateLaneChangeDimensions(RelativeLane targetLane) const override;

  //! @brief Calculates lane change width using LaneChangeTrajectoryCalculations and TrajectoryPlanningQuery.
  //! @param targetLane Lane to which the lane change should be planned
  //! @return The calculated lane change width for a lane change to the given lane in meter.
  units::length::meter_t DetermineLaneChangeWidth(RelativeLane targetLane) const;

  //! @brief Calculates lane change length using LaneChangeTrajectoryCalculations and TrajectoryPlanningQuery.
  //! @param targetLane Lane to which the lane change should be planned
  //! @return The calculated lane change length for a lane change to the given lane in meter.
  units::length::meter_t DetermineLaneChangeLength(RelativeLane targetLane) const;

private:
  //! @brief The TrajectoryPlanningQuery instance used to access data from other components.
  std::shared_ptr<TrajectoryPlanningQueryInterface> _planningQuery;

  //! @brief The TrajectoryCalculations instance used to calculate lane change dimensions.
  std::shared_ptr<LaneChangeTrajectoryCalculationsInterface> _laneChangeTrajectoryCalc;

  //! @brief Reference to the SurroundingVehicleQueryFactory.
  const SurroundingVehicleQueryFactoryInterface& _surroundingVehicleQueryFactory;
};