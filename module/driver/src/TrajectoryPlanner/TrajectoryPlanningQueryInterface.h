/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file TrajectoryPlanningQueryInterface.h
#pragma once

#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"

//! @brief Interface for query class that provides access to required data to plan trajectories from various SCM components.
class TrajectoryPlanningQueryInterface
{
public:
  virtual ~TrajectoryPlanningQueryInterface() = default;

  //! @brief Creates a set with driver parameters for lane change (comfort & maximum angular velocity steering wheel and acceleration).
  //! @return A struct with the corresponding parameters.
  virtual TrajectoryCalculations::DriverParameters CreateDriverParameters() const = 0;

  //! @brief Creates a set with vehicle parameters and information for lane change (steeringRatio, wheelbase, velocity and lateral position in lane)
  //! @return A struct with the corresponding parameters.
  virtual TrajectoryCalculations::VehicleParameters CreateVehicleParameters() const = 0;

  //! @brief Creates a struct with lane widths of ego and target lane, as well as current and target offset to determine lane change width.
  //! @param targetLane
  //! @return A struct with the corresponding parameters
  virtual TrajectoryPlanning::LaneChangeWidthParameters CreateLaneChangeWidthInput(RelativeLane targetLane) const = 0;

  //! @brief Returns struct with relevant parameters to determine the desired length for a lane change.
  //! @param targetLane Lane to which the lane change should be planned
  //! @param laneChangeObstacle Vehicle that is the limiting factor for the available lane change length
  //! @return Parameters requried to calculate the desired lane change length.
  virtual TrajectoryPlanning::LaneChangeLengthParameters CreateLaneChangeLengthInput(RelativeLane targetLane, const SurroundingVehicleInterface* laneChangeObstacle) const = 0;

  //! @brief Creates a set of vehicles (causing vehicles of front and side situation as well as ego front vehicle)
  //! @return The corresponding set of vehicles
  virtual TrajectoryPlanning::RelevantVehiclesForLaneChangeLength GetRelevantVehiclesForLaneChangeLength() const = 0;

  //! @brief Getter heading
  //! @return the current heading of the ego vehicle relative to its lane.
  virtual units::angle::radian_t GetHeading() const = 0;

  //! @brief Getter situation.
  //! @return the currently active situation
  virtual Situation GetSituation() const = 0;
};