/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SideLaneSafety.h"

bool SideLaneSafety::CanFinishLaneChangeSafely(units::length::meter_t laneChangeLength, RelativeLane targetLane) const
{
  if (!(targetLane == RelativeLane::RIGHT || targetLane == RelativeLane::LEFT))
  {
    throw std::out_of_range("CanFinishLaneChangeSafely called with invalid target lane");
  }

  const SideLaneSafetyTypes::LaneSafetyParameters laneChangeSafetyParameters{_sideLaneSafetyQuery->GetLaneChangeSafetyParameters(laneChangeLength, targetLane)};

  const bool frontSafe = _sideLaneSafetyCalculations->CanFinishLaneChangeWithoutEnteringMinDistance(laneChangeSafetyParameters.estimatedLaneChangeDuration, laneChangeSafetyParameters.front, _surroundingVehicleQueryFactory);
  const bool rearSafe = _sideLaneSafetyCalculations->CanFinishLaneChangeWithoutEnteringMinDistance(laneChangeSafetyParameters.estimatedLaneChangeDuration, laneChangeSafetyParameters.rear, _surroundingVehicleQueryFactory);

  return frontSafe && rearSafe;
}