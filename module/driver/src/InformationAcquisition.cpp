/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "InformationAcquisition.h"

#include <algorithm>
#include <iterator>
#include <optional>
#include <vector>

#include "AoiAssigner.h"
#include "GazeControl/GazeParameters.h"
#include "LaneQueryHelper.h"
#include "MentalModelInterface.h"
#include "MicroscopicCharacteristics.h"
#include "ScmDefinitions.h"
#include "SurroundingVehicles/SurroundingVehicle.h"
#include "SurroundingVehicles/SurroundingVehicleDefinitions.h"
#include "SurroundingVehicles/SurroundingVehicles.h"
#include "SurroundingVehicles/SurroundingVehiclesModifier.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"

using namespace units::literals;

InformationAcquisition::InformationAcquisition(MentalModelInterface& mentalModel, const FeatureExtractorInterface& featureExtractor, const MentalCalculationsInterface& mentalCalculations, OwnVehicleInformationSCM& ownVehicleInformationSCM, SurroundingObjectsSCM& surroundingObjectsSCM, bool idealPerception, OwnVehicleRoutePose& ownVehicleRoutePose)
    : _mentalModel{mentalModel},
      _featureExtractor{featureExtractor},
      _mentalCalculations{mentalCalculations},
      _ownVehicleInformationSCM{ownVehicleInformationSCM},
      _surroundingObjectsSCM{surroundingObjectsSCM},
      _idealPerception{idealPerception},
      _extrapolation{std::make_unique<Extrapolation>(_mentalModel, ownVehicleRoutePose)},
      _ownVehicleRoutePose(ownVehicleRoutePose)
{
  _cycleTime = _mentalModel.GetCycleTime();
}

void InformationAcquisition::SetOwnVehicleHelperVariables() const
{
  auto ownVehicle = _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData();

  ownVehicle->id = _ownVehicleInformationSCM.id;
  ownVehicle->forcedLaneChangeStartTrigger = _ownVehicleInformationSCM.forcedLaneChangeStartTrigger;
  ownVehicle->ownConnectorCurvature = _ownVehicleInformationSCM.ownConnectorCurvature;
  ownVehicle->steeringWheelAngle = _ownVehicleInformationSCM.steeringWheelAngle;
  ownVehicle->takeNextExit = _ownVehicleInformationSCM.takeNextExit;
}

void ResetLeadingVehicleIdIfNotPresentAnymore(const AoiVehicleMapping& aoiMapping, MentalModelInterface& mentalModel)
{
  const auto leadingVehicleId{mentalModel.GetLeadingVehicleId()};
  const auto LeadingVehicleIsInVehiclesVector = [leadingVehicleId](const auto& vehicleVector)
  {
    return std::any_of(begin(vehicleVector), end(vehicleVector), [leadingVehicleId](const auto* vehicle)
                       { return vehicle->GetId() == leadingVehicleId; });
  };

  if (leadingVehicleId != -1 &&
      std::none_of(begin(aoiMapping), end(aoiMapping), [LeadingVehicleIsInVehiclesVector](const auto& mapping)
                   {
          const auto &[aoi, vehicleVector] = mapping;
          return LeadingVehicleIsInVehiclesVector(vehicleVector); }))
  {
    mentalModel.SetLeadingVehicleId(-1);  // reset LeadingVehicle if it has been removed
  }
}

void InformationAcquisition::UpdateSurroundingVehicleData(bool forceUpdate, bool isNewInformationAcquisitionRequested) const
{
  if (forceUpdate || _idealPerception)
  {
    AcquireIdealOpticalData();
  }
  else
  {
    const auto* informationAcquisition{static_cast<const InformationAcquisitionInterface*>(this)};

    SurroundingVehicles* surroundingVehicles{_mentalModel.UpdateSurroundingVehicles()};
    SurroundingVehiclesModifier surroundingVehiclesModifier(*surroundingVehicles);

    const bool triggerInformationAcquisition{isNewInformationAcquisitionRequested || _mentalModel.GetIsNewEgoLane()};
    surroundingVehiclesModifier.UpdateVehicles(informationAcquisition, *_extrapolation.get(), triggerInformationAcquisition);

    std::vector<AreaOfInterest> inconsistentAois;
    if (triggerInformationAcquisition)
    {
      const auto visibleAgents{GetVisibleAgentIds()};
      const auto visualInconsistencies{surroundingVehiclesModifier.RemoveVisibleVehiclesNotPresentInGroundTruth(visibleAgents, &_mentalModel)};
      std::copy(begin(visualInconsistencies), end(visualInconsistencies), std::back_inserter(inconsistentAois));
    }

    const auto& infrastructureData{*_mentalModel.GetInfrastructureCharacteristics()};
    AoiAssigner aoiAssigner(infrastructureData, _ownVehicleInformationSCM);
    LaneVehicleMapping laneMapping{aoiAssigner.AssignToLanes(surroundingVehiclesModifier.Update())};
    AoiVehicleMapping aoiMapping{aoiAssigner.AssignToAois(laneMapping)};

    const auto inconsistenciesFromExtrapolation{surroundingVehiclesModifier.DetectInconsistenciesFromExtrapolation(aoiMapping, infrastructureData)};
    std::copy(begin(inconsistenciesFromExtrapolation), end(inconsistenciesFromExtrapolation), std::back_inserter(inconsistentAois));

    surroundingVehiclesModifier.AssignRelativeLane(laneMapping);
    surroundingVehiclesModifier.AssignAreasOfInterest(aoiMapping);
    UpdateInconsistencies(triggerInformationAcquisition, inconsistentAois);

    SurroundingVehiclesModifier::HandleSideAreaTransitions(&aoiMapping, informationAcquisition);
    ResetLeadingVehicleIdIfNotPresentAnymore(aoiMapping, _mentalModel);
    auto* microscopicCharaceristics{_mentalModel.GetMicroscopicData()};
    // SurroundingVehiclesModifier::WriteToMicroscopicData(aoiMapping, microscopicCharaceristics, informationAcquisition);

    CorrectSideAoiIndices();

    _mentalModel.SetAoiMapping(aoiMapping);
    if (triggerInformationAcquisition)
    {
      _mentalModel.CalculateVLegal(false);
    }
  }
}

void UpdateReliabilityMaps(MentalModelInterface* _mentalModel)
{
  for (const auto& aoi : SURROUNDING_AREAS_OF_INTEREST)
  {
    // Agent close to the spawn zone do not know their rear aoi
    if (!(_mentalModel->GetLaneChangePrevention() && LaneQueryHelper::IsRearArea(aoi)))
    {
      if (!LaneQueryHelper::IsSideArea(aoi))
      {
        if (auto* information{_mentalModel->GetMicroscopicData()->UpdateObjectInformation(aoi)})
        {
          information->reliabilityMap[FieldOfViewAssignment::FOVEA] = _mentalModel->GetTime();
        }
      }
      else
      {
        for (unsigned int i = 0; i < _mentalModel->GetMicroscopicData()->GetSideObjectVector(aoi)->size(); ++i)
        {
          _mentalModel->GetMicroscopicData()->UpdateObjectInformation(aoi, i)->reliabilityMap[FieldOfViewAssignment::FOVEA] = _mentalModel->GetTime();
        }
      }
    }
  }
}

void UpdateReliabilityMapsVector(MentalModelInterface* _mentalModel)
{
  for (const auto& aoi : SURROUNDING_AREAS_OF_INTEREST_VECTOR)
  {
    // Agent close to the spawn zone do not know their rear aoi
    if (!(_mentalModel->GetLaneChangePrevention() && LaneQueryHelper::IsRearArea(aoi)))
    {
      for (unsigned int i = 0; i < _mentalModel->GetMicroscopicData()->GetObjectVector(aoi)->size(); ++i)
      {
        if (auto* information{_mentalModel->GetMicroscopicData()->UpdateObjectInformation(aoi, i)})
        {
          information->reliabilityMap[FieldOfViewAssignment::FOVEA] = _mentalModel->GetTime();
        }
      }
    }
  }
}

void InformationAcquisition::AcquireIdealOpticalData() const
{
  UpdateReliabilityMapsVector(&_mentalModel);
  UpdateReliabilityMaps(&_mentalModel);
  const auto* informationAcquisition{static_cast<const InformationAcquisitionInterface*>(this)};

  SurroundingVehicles* surroundingVehicles{_mentalModel.UpdateSurroundingVehicles()};
  SurroundingVehiclesModifier surroundingVehiclesModifier(*surroundingVehicles);

  surroundingVehiclesModifier.UpdateVehicles(informationAcquisition, *_extrapolation.get(), true, true);

  const auto& infrastructureData{*_mentalModel.GetInfrastructureCharacteristics()};
  AoiAssigner aoiAssigner(infrastructureData, _ownVehicleInformationSCM);
  LaneVehicleMapping laneMapping{aoiAssigner.AssignToLanes(surroundingVehiclesModifier.Update())};
  AoiVehicleMapping aoiMapping{aoiAssigner.AssignToAois(surroundingVehiclesModifier.Update())};

  surroundingVehiclesModifier.AssignRelativeLane(laneMapping);
  surroundingVehiclesModifier.AssignAreasOfInterest(aoiMapping);
  ResetLeadingVehicleIdIfNotPresentAnymore(aoiMapping, _mentalModel);
  auto* microscopicCharaceristics{_mentalModel.GetMicroscopicData()};
  SurroundingVehiclesModifier::WriteToMicroscopicData(aoiMapping, microscopicCharaceristics, informationAcquisition);
  CorrectSideAoiIndices();
  _mentalModel.SetAoiMapping(aoiMapping);
  _mentalModel.CalculateVLegal(false);
}

void InformationAcquisition::ResetAoiDataForNoObject(ObjectInformationScmExtended* surroundingObject) const
{
  const OwnVehicleInformationScmExtended *ownVehicle{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()};
  surroundingObject->accelerationChange = -999._mps_sq;
  surroundingObject->relativeLongitudinalDistance = _mentalModel.GetDriverParameters().previewDistance;
  surroundingObject->gap = _mentalModel.GetDriverParameters().previewDistance / units::math::max(1._mps, ownVehicle->longitudinalVelocity);
  surroundingObject->lateralVelocity = 0._mps;
  surroundingObject->lateralPositionInLane = 0._m;
  surroundingObject->distanceToLaneBoundaryLeft = 0.5_m;
  surroundingObject->distanceToLaneBoundaryRight = 0.5_m;
  surroundingObject->tauDot = 0.;
  surroundingObject->ttc = ScmDefinitions::TTC_LIMIT;
  surroundingObject->acceleration = 0._mps_sq;
  surroundingObject->absoluteVelocity = 0._mps;
  surroundingObject->exist = false;
  surroundingObject->isStatic = false;
  surroundingObject->width = 0._m;
  surroundingObject->height = 0._m;
  surroundingObject->length = 0._m;
  surroundingObject->indicatorState = scm::LightState::Indicator::Off;
  surroundingObject->id = -1;
  surroundingObject->collision = false;
  surroundingObject->heading = 0.;
  surroundingObject->relativeLateralDistance = _mentalModel.GetDriverParameters().previewDistance;
  surroundingObject->longitudinalVelocity = 0.0_mps;
  surroundingObject->brakeLightsActive = false;
  surroundingObject->obstruction = ObstructionScm::NoOpponent();
  surroundingObject->longitudinalObstruction = ObstructionLongitudinal::NoOpponent();
  surroundingObject->projectedDimensions = ProjectedSpacialDimensions();
  surroundingObject->opticalAnglePositionHorizontal = -999._rad;
  surroundingObject->opticalAngleSizeHorizontal = -999._rad;
  surroundingObject->thresholdLooming = -999.;
}

void InformationAcquisition::SetAoiDataWithGroundTruth(ObjectInformationScmExtended* surroundingObject,
                                                       const ObjectInformationSCM* surroundingObject_GroundTruth,
                                                       AreaOfInterest aoi) const
{
  const OwnVehicleInformationScmExtended* ownVehicle{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()};
  AssignFocusAreaSpecificOpticalDataForSurroundingObject(surroundingObject, surroundingObject_GroundTruth, ownVehicle);

  surroundingObject->relativeLateralDistance = surroundingObject_GroundTruth->relativeLateralDistance;
  surroundingObject->exist = true;
  surroundingObject->width = surroundingObject_GroundTruth->width;
  surroundingObject->height = surroundingObject_GroundTruth->height;
  surroundingObject->length = surroundingObject_GroundTruth->length;
  surroundingObject->brakeLightsActive = surroundingObject_GroundTruth->brakeLightsActive;
  surroundingObject->indicatorState = surroundingObject_GroundTruth->indicatorState;
  surroundingObject->id = surroundingObject_GroundTruth->id;
  surroundingObject->collision = surroundingObject_GroundTruth->collision;
  surroundingObject->isStatic = surroundingObject_GroundTruth->isStatic;
  surroundingObject->heading = surroundingObject_GroundTruth->heading;
  surroundingObject->relativeLongitudinalDistance = surroundingObject_GroundTruth->relativeLongitudinalDistance;  // always positive
  surroundingObject->lateralPositionInLane = surroundingObject_GroundTruth->lateralPositionInLane;
  surroundingObject->obstruction = surroundingObject_GroundTruth->obstruction;
  surroundingObject->longitudinalObstruction = surroundingObject_GroundTruth->longitudinalObstruction;

  surroundingObject->projectedDimensions = surroundingObject_GroundTruth->projectedDimensions;
  surroundingObject->opticalAnglePositionHorizontal = surroundingObject_GroundTruth->opticalAnglePositionHorizontal;
  surroundingObject->orientationRelativeToDriver = surroundingObject_GroundTruth->orientationRelativeToDriver;

  AssignAoiSpecificOpticalDataForSurroundingObject(surroundingObject, surroundingObject_GroundTruth, ownVehicle, aoi);

  surroundingObject->thresholdLooming = surroundingObject_GroundTruth->thresholdLooming;

  surroundingObject->longitudinalDistanceFrontBumperToRearAxle = surroundingObject_GroundTruth->longitudinalDistanceFrontBumperToRearAxle;
  surroundingObject->relativeX = surroundingObject_GroundTruth->relativeX;
  surroundingObject->relativeY = surroundingObject_GroundTruth->relativeY;
}

namespace
{
bool AccelerationValid(units::acceleration::meters_per_second_squared_t acceleration)
{
  return acceleration > units::make_unit<units::acceleration::meters_per_second_squared_t>(ScmDefinitions::DEFAULT_VALUE);
}
}  // namespace
void InformationAcquisition::AssignFocusAreaSpecificOpticalDataForSurroundingObject(ObjectInformationScmExtended* surroundingObject,
                                                                                    const ObjectInformationSCM* surroundingObject_GroundTruth,
                                                                                    const OwnVehicleInformationScmExtended* ownVehicle) const
{
  surroundingObject->accelerationLastStep = surroundingObject->acceleration;

  surroundingObject->accelerationChange = AccelerationValid(surroundingObject->acceleration) ? surroundingObject_GroundTruth->acceleration - surroundingObject->acceleration : 0.0_mps_sq;
  surroundingObject->acceleration = surroundingObject_GroundTruth->acceleration;
  surroundingObject->absoluteVelocity = surroundingObject_GroundTruth->absoluteVelocity;
  surroundingObject->lateralVelocity = surroundingObject_GroundTruth->lateralVelocity;
  surroundingObject->longitudinalVelocity = surroundingObject_GroundTruth->longitudinalVelocity;
  surroundingObject->distanceToLaneBoundaryLeft = surroundingObject_GroundTruth->distanceToLaneBoundaryLeft;
  surroundingObject->distanceToLaneBoundaryRight = surroundingObject_GroundTruth->distanceToLaneBoundaryRight;
}

void InformationAcquisition::AssignAoiSpecificOpticalDataForSurroundingObject(ObjectInformationScmExtended* surroundingObject,
                                                                              const ObjectInformationSCM* surroundingObject_GroundTruth,
                                                                              const OwnVehicleInformationScmExtended* ownVehicle,
                                                                              AreaOfInterest aoi) const
{
  if (const auto* information{_mentalModel.GetMicroscopicData()->UpdateObjectInformation(aoi, 0)})
  {
    surroundingObject->reliabilityMap = information->reliabilityMap;
  }

  auto longitudinalVelocityDelta = surroundingObject->longitudinalVelocity - ownVehicle->longitudinalVelocity;

  if (LaneQueryHelper::IsRearArea(aoi))
  {
    longitudinalVelocityDelta *= -1.;
  }

  surroundingObject->gap = ScmCommons::CalculateNetGap(ownVehicle->longitudinalVelocity,
                                                       surroundingObject_GroundTruth->longitudinalVelocity,
                                                       surroundingObject_GroundTruth->relativeLongitudinalDistance,
                                                       aoi);  // always >= 0
  surroundingObject->gapDot = ScmCommons::CalculateNetGapDot(ownVehicle->longitudinalVelocity,
                                                             surroundingObject_GroundTruth->longitudinalVelocity,
                                                             ownVehicle->acceleration,
                                                             surroundingObject_GroundTruth->acceleration,
                                                             surroundingObject_GroundTruth->relativeLongitudinalDistance,
                                                             longitudinalVelocityDelta,
                                                             aoi);

  const auto timeSinceLastPerceptionFOVEA{_mentalModel.GetTime() - surroundingObject->reliabilityMap[FieldOfViewAssignment::FOVEA]};
  if (timeSinceLastPerceptionFOVEA == 0_ms || timeSinceLastPerceptionFOVEA > 500_ms)
  {
    surroundingObject->ttcthreshold = ScmCommons::CalculateTtcThreshold(surroundingObject_GroundTruth->opticalAngleSizeHorizontal,
                                                                        surroundingObject_GroundTruth->opticalAnglePositionHorizontal,
                                                                        _mentalModel.GetDriverParameters(),
                                                                        false);
    // Note: Cortical madnification is ignored since there currently is no deteriation of quality in the useful field of fiew
  }

  const units::time::second_t ttc = ScmCommons::CalculateTTC(longitudinalVelocityDelta,
                                                             surroundingObject_GroundTruth->relativeLongitudinalDistance,
                                                             aoi);

  if (units::math::fabs(ttc) >= surroundingObject->ttcthreshold)
  {
    surroundingObject->ttc = ScmDefinitions::TTC_LIMIT;
    surroundingObject->tauDot = 0.;
  }
  else
  {
    surroundingObject->ttc = ttc;
    const auto accelerationDelta{LaneQueryHelper::IsRearArea(aoi) ? ownVehicle->acceleration - surroundingObject->acceleration : surroundingObject->acceleration - ownVehicle->acceleration};
    surroundingObject->tauDot = ScmCommons::CalculateTauDot(longitudinalVelocityDelta,
                                                            surroundingObject_GroundTruth->relativeLongitudinalDistance,
                                                            accelerationDelta,
                                                            aoi);
  }
  if (LaneQueryHelper::IsSideArea(aoi))
  {
    surroundingObject->gap = 0._s;
    surroundingObject->gapDot = 0.;
    surroundingObject->ttc = ScmDefinitions::TTC_LIMIT;
    surroundingObject->tauDot = 0.;
  }
}

std::vector<ObjectInformationSCM>* InformationAcquisition::GetTrafficObjectsFromAoiAllVectors(OngoingTrafficObjectsSCM* objects, AreaOfInterest aoi) const
{
  switch (aoi)
  {
    case AreaOfInterest::EGO_FRONT:
      // case AreaOfInterest::EGO_FRONT_FAR:
      return &objects->Ahead().Close();
    case AreaOfInterest::LEFT_FRONT:
      // case AreaOfInterest::LEFT_FRONT_FAR:
      return &objects->Ahead().Left();
    case AreaOfInterest::LEFTLEFT_FRONT:
      return &objects->Ahead().FarLeft();
    case AreaOfInterest::RIGHT_FRONT:
      // case AreaOfInterest::RIGHT_FRONT_FAR:
      return &objects->Ahead().Right();
    case AreaOfInterest::RIGHTRIGHT_FRONT:
      return &objects->Ahead().FarRight();
    case AreaOfInterest::EGO_REAR:
      return &objects->Behind().Close();
    case AreaOfInterest::LEFT_REAR:
      return &objects->Behind().Left();
    case AreaOfInterest::LEFTLEFT_REAR:
      return &objects->Behind().FarLeft();
    case AreaOfInterest::RIGHT_REAR:
      return &objects->Behind().Right();
    case AreaOfInterest::RIGHTRIGHT_REAR:
      return &objects->Behind().FarRight();
    case AreaOfInterest::LEFT_SIDE:
      return &objects->Close().Left();
    case AreaOfInterest::RIGHT_SIDE:
      return &objects->Close().Right();
    case AreaOfInterest::LEFTLEFT_SIDE:
      return &objects->Close().FarLeft();
    case AreaOfInterest::RIGHTRIGHT_SIDE:
      return &objects->Close().FarRight();
    default:
      throw std::runtime_error("InformationAcquisition - GetObjectFromAoi: Invalid AOI!");
  }
}

void InformationAcquisition::UpdateOwnVehicleData(OwnVehicleInformationSCM* ownVehicle_GroundTruth)
{
  OwnVehicleInformationScmExtended* ownVehicle = _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData();

  ownVehicle->acceleration = ownVehicle_GroundTruth->acceleration;
  ownVehicle->absoluteVelocity = ownVehicle_GroundTruth->absoluteVelocity;
  ownVehicle->absoluteVelocityReal = ownVehicle_GroundTruth->absoluteVelocity;
  ownVehicle->distanceToLaneBoundaryLeft = ownVehicle_GroundTruth->distanceToLaneBoundaryLeft;
  ownVehicle->distanceToLaneBoundaryRight = ownVehicle_GroundTruth->distanceToLaneBoundaryRight;
  ownVehicle->lateralVelocity = ownVehicle_GroundTruth->lateralVelocity;
  ownVehicle->lateralVelocityFrontAxle = ownVehicle_GroundTruth->lateralVelocityFrontAxle;
  ownVehicle->lateralPosition = ownVehicle_GroundTruth->lateralPosition;
  ownVehicle->lateralPositionFrontAxle = ownVehicle_GroundTruth->lateralPositionFrontAxle;
  ownVehicle->mainLaneId = ownVehicle_GroundTruth->mainLaneId;
  ownVehicle->collision = ownVehicle_GroundTruth->collision;
  ownVehicle->heading = ownVehicle_GroundTruth->heading;
  ownVehicle->longitudinalVelocity = ownVehicle_GroundTruth->longitudinalVelocity;
  ownVehicle->lateralOffsetNeutralPositionScaled = _mentalCalculations.CalculateLateralOffsetNeutralPositionScaled();
  ownVehicle->projectedDimensions = ownVehicle_GroundTruth->projectedDimensions;
  ownVehicle->posX = ownVehicle_GroundTruth->posX;
  ownVehicle->posY = ownVehicle_GroundTruth->posY;
}

void InformationAcquisition::ExtrapolateOwnVehicleData(OwnVehicleInformationSCM* ownVehicle_GroundTruth)
{
  OwnVehicleInformationScmExtended *ownVehicle = _mentalModel.GetMicroscopicData()->UpdateOwnVehicleData();
  const units::time::second_t dt = _cycleTime;

  ownVehicle->acceleration = ownVehicle_GroundTruth->acceleration;
  ownVehicle->absoluteVelocityReal = ownVehicle_GroundTruth->absoluteVelocity;
  ownVehicle->distanceToLaneBoundaryLeft = ownVehicle_GroundTruth->distanceToLaneBoundaryLeft;
  ownVehicle->distanceToLaneBoundaryRight = ownVehicle_GroundTruth->distanceToLaneBoundaryRight;
  ownVehicle->lateralVelocity = ownVehicle_GroundTruth->lateralVelocity;
  ownVehicle->lateralVelocityFrontAxle = ownVehicle_GroundTruth->lateralVelocityFrontAxle;
  ownVehicle->lateralPosition = ownVehicle_GroundTruth->lateralPosition;
  ownVehicle->lateralPositionFrontAxle = ownVehicle_GroundTruth->lateralPositionFrontAxle;
  ownVehicle->mainLaneId = ownVehicle_GroundTruth->mainLaneId;
  ownVehicle->heading = ownVehicle_GroundTruth->heading;
  ownVehicle->longitudinalVelocity = ownVehicle_GroundTruth->longitudinalVelocity;
  ownVehicle->lateralOffsetNeutralPositionScaled = _mentalCalculations.CalculateLateralOffsetNeutralPositionScaled();
  ownVehicle->projectedDimensions = ownVehicle_GroundTruth->projectedDimensions;
  ownVehicle->posX = ownVehicle_GroundTruth->posX;
  ownVehicle->posY = ownVehicle_GroundTruth->posY;

  // Extrapolated values
  ownVehicle->absoluteVelocity = _mentalModel.GetAbsoluteVelocityEgo(true) + dt * ownVehicle_GroundTruth->acceleration * _estimateFactorForVEgo;
}

void InformationAcquisition::UpdateEgoVehicleData(bool forceUpdate)
{
  const AreaOfInterest fovea{_mentalModel.GetFovea()};

  if (fovea == AreaOfInterest::NumberOfAreaOfInterests || fovea == AreaOfInterest::INSTRUMENT_CLUSTER || fovea == AreaOfInterest::HUD || forceUpdate || _idealPerception)
    UpdateOwnVehicleData(&_ownVehicleInformationSCM);
  else
    ExtrapolateOwnVehicleData(&_ownVehicleInformationSCM);

  SetOwnVehicleHelperVariables();
}

void InformationAcquisition::CorrectAoiDataDueToLongitudinalTransitionFromSideArea(ObjectInformationScmExtended* object, AreaOfInterest newAoi) const
{
  const bool transitionToFront{LaneQueryHelper::IsFrontArea(newAoi)};
  units::velocity::meters_per_second_t longitudinalVelocityDelta{0.0_mps};
  if (transitionToFront)
  {
    object->relativeLongitudinalDistance = -1 * object->longitudinalObstruction.rear;
    longitudinalVelocityDelta = object->longitudinalVelocity - _mentalModel.GetLongitudinalVelocityEgo();
  }
  else
  {
    object->relativeLongitudinalDistance = -1 * object->longitudinalObstruction.front;
    longitudinalVelocityDelta = _mentalModel.GetLongitudinalVelocityEgo() - object->longitudinalVelocity;
  }

  object->gap = ScmCommons::CalculateNetGap(_mentalModel.GetLongitudinalVelocityEgo(),
                                            object->longitudinalVelocity,
                                            object->relativeLongitudinalDistance,
                                            newAoi);

  object->gapDot = ScmCommons::CalculateNetGapDot(_mentalModel.GetLongitudinalVelocityEgo(),
                                                  object->longitudinalVelocity,
                                                  _mentalModel.GetAcceleration(),
                                                  object->acceleration,
                                                  object->relativeLongitudinalDistance,
                                                  longitudinalVelocityDelta,
                                                  newAoi);

  object->ttc = ScmCommons::CalculateTTC(longitudinalVelocityDelta, object->relativeLongitudinalDistance, newAoi);

  if (units::math::fabs(object->ttc) >= ScmDefinitions::TTC_LIMIT)
  {
    object->tauDot = 0.;
  }
  else
  {
    const bool canPerceiveAcceleration{object->relativeLongitudinalDistance < ACCELERATION_SENSING_DISTANCE};
    const auto egoAcceleration{_mentalModel.GetAcceleration()};
    const auto accelerationDelta{LaneQueryHelper::IsRearArea(newAoi) ? egoAcceleration - object->acceleration : object->acceleration - egoAcceleration};
    object->tauDot = ScmCommons::CalculateTauDot(longitudinalVelocityDelta,
                                                 object->relativeLongitudinalDistance,
                                                 canPerceiveAcceleration ? accelerationDelta : 0_mps_sq,
                                                 newAoi);
  }
}

void InformationAcquisition::CorrectAoiDataDueToLongitudinalTransitionToSideArea(ObjectInformationScmExtended* object) const
{
  object->gap = 0._s;
  object->gapDot = 0.;
  object->ttc = ScmDefinitions::TTC_LIMIT;
  object->tauDot = 0.;
}

std::vector<AreaOfInterest> InformationAcquisition::CombineFoveaAndUfov() const
{
  std::vector<AreaOfInterest> aoisToCheck;
  const std::vector<AreaOfInterest>& ufovAois{_mentalModel.GetUfov()};
  std::copy_if(begin(ufovAois), end(ufovAois), std::back_inserter(aoisToCheck), [](const AreaOfInterest aoi)
               { return ScmCommons::IsSurroundingAreaOfInterest(aoi); });

  const auto& fovea{_mentalModel.GetFovea()};
  if (ScmCommons::IsSurroundingAreaOfInterest(fovea) &&
      std::none_of(begin(aoisToCheck), end(aoisToCheck), [fovea](AreaOfInterest aoi)
                   { return fovea == aoi; }))
  {
    aoisToCheck.push_back(fovea);
  }

  return aoisToCheck;
}

std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>> InformationAcquisition::PerceiveSurroundingVehicles(bool idealPerception) const
{
  std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>> result;

  for (const auto& aoi : SURROUNDING_AREAS_OF_INTEREST_VECTOR)
  {
    std::vector<ObjectInformationSCM>* surroundingObjects{GetTrafficObjectsFromAoiAllVectors(&_surroundingObjectsSCM.ongoingTraffic, aoi)};

    if (!surroundingObjects->empty())
    {
      for (auto& surroundingObject : *surroundingObjects)
      {
        if (surroundingObject.id == -1)
          continue;

        auto obstructionLateral = surroundingObjects->data()->obstruction.mainLaneLocator;
        auto obstructionLongitudinal = surroundingObjects->data()->longitudinalObstruction.mainLaneLocator;

        Scm::Point mainLaneLocater{obstructionLongitudinal, obstructionLateral};

        bool isVisible = _mentalModel.IsPointVisible(mainLaneLocater) || idealPerception;

        if (isVisible)
        {
          result.emplace_back(aoi, &surroundingObject);
        }
      }
    }
  }
  return result;
}

std::vector<std::pair<const ObjectInformationSCM*, AreaOfInterest>> InformationAcquisition::GetExistingGroundTruthObjects() const
{
  std::vector<std::pair<const ObjectInformationSCM*, AreaOfInterest>> result;

  for (const auto& aoi : SURROUNDING_AREAS_OF_INTEREST_VECTOR)
  {
    const std::vector<ObjectInformationSCM>* surroundingObjects{GetTrafficObjectsFromAoiAllVectors(&_surroundingObjectsSCM.ongoingTraffic, aoi)};

    for (const auto& surroundingObject_GroundTruth : *surroundingObjects)
    {
      result.emplace_back(&surroundingObject_GroundTruth, aoi);
    }
  }
  return result;
}

std::vector<int> InformationAcquisition::GetVisibleAgentIds() const
{
  const auto existingVehicles{GetExistingGroundTruthObjects()};
  std::vector<int> visibleAgentIds{};

  for (const auto& [vehicle, aoi] : existingVehicles)
  {
    ObjectInformationScmExtended extendedObjectInfo;
    SetAoiDataWithGroundTruth(&extendedObjectInfo, vehicle, aoi);
    const auto surroundingVehicle{SurroundingVehicle(extendedObjectInfo, aoi)};

    if (_mentalModel.IsVisible(surroundingVehicle.GetBoundingBox(), GazeParameters::HORIZONTAL_SIZE_PERIPHERY))
    {
      visibleAgentIds.push_back(surroundingVehicle.GetId());
    }
  }
  return visibleAgentIds;
}

bool InformationAcquisition::IsOutOfSight(units::length::meter_t relativeLongitudinalDistance) const
{
  return relativeLongitudinalDistance > _mentalModel.GetInfrastructureCharacteristics()->GetGeometryInformation()->visibilityDistance;
}

void InformationAcquisition::CorrectSideAoiIndices() const
{
  const auto mergeRegulate{_mentalModel.GetMergeRegulate()};
  const auto mergeSideIndex = _mentalModel.GetMicroscopicData()->GetSideAoiMergeRegulateIndex();
  if (LaneQueryHelper::IsSideArea(mergeRegulate) && _mentalModel.GetMicroscopicData()->GetSideObjectVector(mergeRegulate)->size() >= mergeSideIndex)
  {
    _mentalModel.GetMicroscopicData()->UpdateSideAoiMergeRegulateIndex(0);
  }
}

void InformationAcquisition::UpdateInconsistencies(bool triggerInformationAcquisition, const std::vector<AreaOfInterest>& inconsistentAois) const
{
  for (const auto aoi : inconsistentAois)
  {
    _mentalModel.MarkInconsistent(aoi);
  }

  if (triggerInformationAcquisition)
  {
    for (const auto aoi : CombineFoveaAndUfov())
    {
      if (std::find(begin(inconsistentAois), end(inconsistentAois), aoi) == inconsistentAois.end())
      {
        _mentalModel.ClearInconsistency(aoi);
      }
    }
  }
}

units::time::millisecond_t InformationAcquisition::GetCycleTime() const
{
  return _cycleTime;
}

units::time::millisecond_t InformationAcquisition::GetTime() const
{
  return _mentalModel.GetTime();
}
