/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file TrafficFlow.h
#pragma once

#include <vector>

#include "LaneMeanVelocityInterface.h"
#include "TrafficFlowInterface.h"

//! **************************************
//! @brief implements TrafficFlowInterface
//! **************************************
class TrafficFlow : public TrafficFlowInterface
{
public:
  TrafficFlow(units::time::millisecond_t cycleTime, units::velocity::meters_per_second_t desiredVelocity);

  void UpdateMeanVelocities(const std::vector<const SurroundingVehicleInterface*>& traffic) override;
  units::velocity::meters_per_second_t GetMeanVelocity(SurroundingLane surroundingLane) override;
  void Reset() override;

private:
  units::velocity::meters_per_second_t _desiredVelocity{};
  std::unique_ptr<LaneMeanVelocityInterface> _egoLane{nullptr};
  std::unique_ptr<LaneMeanVelocityInterface> _leftLane{nullptr};
  std::unique_ptr<LaneMeanVelocityInterface> _rightLane{nullptr};
  units::velocity::meters_per_second_t CalculateCurrentMeanVelocity(const std::vector<const SurroundingVehicleInterface*>& traffic, SurroundingLane surroundingLane);
};
