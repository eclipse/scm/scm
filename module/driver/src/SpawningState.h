/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once
#include "ScmComponentsInterface.h"
#include "ScmLifetimeState.h"

/**
 * \brief SpawningState is responsible for intializing a valid SCM state in the first time step of a newly spawned agent.
 *
 */
class SpawningState : public ScmLifetimeState
{
public:
  SpawningState(const std::string& componentName,
                units::time::millisecond_t cycleTime,
                PeriodicLoggerInterface* pLog,
                ScmComponentsInterface& scmComponents,
                ExternalControlState externalControlState);
  void DetermineAgentStates() override;

protected:
  void SetInitialParameters(units::time::millisecond_t time) override;
  ExternalControlState _externalControlState;
  void UpdateMicroscopicData() override;

private:
  ScmComponentsInterface* _scmComponents;
  inline static const std::vector<AreaOfInterest> _RELEVANT_AOIS_AT_SPAWN{
      AreaOfInterest::LEFT_FRONT, AreaOfInterest::RIGHT_FRONT, AreaOfInterest::LEFT_FRONT_FAR, AreaOfInterest::RIGHT_FRONT_FAR, AreaOfInterest::LEFT_SIDE, AreaOfInterest::RIGHT_SIDE, AreaOfInterest::EGO_FRONT};

  void UpdateAgentInformation(units::time::millisecond_t time) override;
};
