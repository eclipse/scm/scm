/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  LaneChangeBehaviorInterface.h

#pragma once

#include "ScmCommons.h"
#include "module/parameterParser/src/TrafficRules/TrafficRules.h"

/*! \addtogroup LaneChangeBehaviorInterface
 * @{
 * \brief contains the necessary public functionality for the lane change behavior of the stochastic cognitive model (SCM).
 * \details This component implements the lane change behavior, containing functions to calculate lane change intensities, calculation of lane change wishes, etc.
 */

class LaneChangeBehaviorInterface
{
public:
  virtual ~LaneChangeBehaviorInterface() = default;

  //! \brief Calculate lane intensities for left, ego and right lane between [0.0, 1.0].
  //! This intensity is a measure for the satisfaction level to drive on the corresponding lane
  //! from where a potential lane change wish can be derived.
  //! \param rightHandTraffic flag indicating traffic direction
  //! \param keepToOuterLane flag indicating traffic rule to drive on the most outer lane if possible
  virtual void CalculateLaneIntensities(const TrafficRulesScm& trafficRulesScm) = 0;

  //! @brief Returns the lane change wish based on the highest calculated lane change intensities.
  //!
  //! @return SurroundingLane Lane to change to, EGO_LANE to keep the current lane.
  virtual SurroundingLane GetLaneChangeWish() const = 0;

  //! \brief Calculates the lane convenience(= Lane change intensity) for the given lane.
  //! \param targetLane Lane for which the convencience should be calculated
  //! \return The lane convenience for the given lane
  virtual double CalculateBaseLaneConvenience(SurroundingLane targetLane) const = 0;

  //! \brief Gets the lane convenience(= Lane change intensity) for the given lane.
  //! \param targetLane Lane for which the convencience should be calculated
  //! \return The lane convenience for the given lane
  virtual double GetLaneConvenience(SurroundingLane targetLane) const = 0;
};
/** @} */  // End of group LaneChangeBehaviorInterface
