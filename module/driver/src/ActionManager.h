/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  ActionManager.h

#pragma once

#include <Stochastics/StochasticsInterface.h>

#include <algorithm>
#include <memory>

#include "../../parameterParser/src/TrafficRules/TrafficRules.h"
#include "ActionManagerInterface.h"
#include "GazeControl/GazeControl.h"
#include "LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculationsInterface.h"
#include "LaneKeepingCalculationsInterface.h"
#include "MentalCalculationsInterface.h"
#include "Merging/ZipMergingInterface.h"
#include "SideLaneSafety/SideLaneSafetyInterface.h"
#include "SideLaneSafety/SideLaneSafetyQueryInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "Swerving/SwervingInterface.h"
#include "TrajectoryPlanner/TrajectoryPlannerInterface.h"

class FeatureExtractorInterface;
class MentalModelInterface;

//! @brief Consolidation of relevant data for lateral action patterns
struct LateralActionPatternSet
{
  //! @brief if there is a corresponding side lane
  bool hasSideLane{};
  //! @brief the front side area of interest
  AreaOfInterest frontSideAoi{AreaOfInterest::NumberOfAreaOfInterests};
  //! @brief the side area of interest
  AreaOfInterest sideAoi{AreaOfInterest::NumberOfAreaOfInterests};
  //! @brief the lateral action for lane change
  LateralAction laneChange{};
  //! @brief the lateral action for intended lane change
  LateralAction intentLaneChange{};
  //! @brief the side
  Side side{};
};

//! @brief Holds all functionality regarding action patterns.
//! @details This component holds all functionality regarding action patterns. It is responsible for generating
//! probability distributions over action patterns given a situation and mental model state, and for sampling
//! from these distributions. Moreover it provides collections of logically coherent sets of actions, such as
//! all actions that represent a lane change to the right, etc.
//! Implements the ActionManagerInterface
class ActionManager : public ActionManagerInterface
{
public:
  //! @brief Unordered Map that holds LateralActions with its associated intensity and a custom hash.
  typedef std::unordered_map<LateralAction, double, LateralActionHash> ActionPatternIntensities;

  //! @brief Constructor.
  //! @param stochastics         Provides access to the stochastics functionality of the framework.
  //! @param mentalModel         Interface to the mental processes of the stochastic cognitive model.
  //! @param featureExtractor    Interface to FeatureExtractor to avoid accessing it through MentalModel
  //! @param mentalCalculations  Interface to MentalCalculations to avoid accessing it through MentalModel
  //! @param swerving            Interface to Swerving
  //! @param surroundingVehicleQueryFactory Factory to create query objects for specific surroundingVehicles
  //! @param trafficRulesScm     Reference to the currently active traffic rules
  //! @param trajectoryPlanner   Interface to the central TrajectoryPlanner instance
  //! @param zipMerging          Interface to the central zipMergign instance
  ActionManager(StochasticsInterface* stochastics,
                MentalModelInterface& mentalModel,
                const FeatureExtractorInterface& featureExtractor,
                const MentalCalculationsInterface& mentalCalculations,
                SwervingInterface& swerving,
                const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory,
                const TrafficRulesScm& trafficRulesScm,
                const TrajectoryPlannerInterface& trajectoryPlanner,
                ZipMergingInterface& zipMerging);

  ActionManager(const ActionManager&) = delete;
  ActionManager(ActionManager&&) = delete;
  ActionManager& operator=(const ActionManager&) = delete;
  ActionManager& operator=(ActionManager&&) = delete;
  virtual ~ActionManager() = default;

  void GenerateIntensityAndSampleLateralAction() override;

  LateralAction GetLateralActionLastTick() override;

  void DetermineLongitudinalActionState() override;

  void DiscardLastActionPatternIntensities() override;

  //! @brief Generate intensity vector
  //! @param aoi                      AreaOfInterest
  //! @param evadeTargetOnItsLeft     Calculate intensities while trying to evade to the left of the observed object. FALSE will calculate intensities for the right side.
  //! @return Intensitiy Vector
  virtual std::map<CriticalActionState, double> GenerateCriticalActionIntensityVector(AreaOfInterest aoi,
                                                                                      bool evadeTargetOnItsLeft);

  //! @brief Interpolate critical action intensity between upper and lower TTC limit
  //! @param ttcCurrent      Current TTC value in s
  //! @param ttcUpperLimit   Upper TTC limit for action (above this, intensity is equal 1)
  //! @param ttcLowerLimit   Lower TTC limit for action (below this, intensity is equal 0)
  //! @return actionIntensity
  virtual double GetDividedCriticalActionIntensity(units::time::second_t ttcCurrent, units::time::second_t ttcUpperLimit, units::time::second_t ttcLowerLimit);

  LongitudinalActionState GetActionSubStateLastTick() const override;
  void SetActionSubStateLastTick(LongitudinalActionState actionSubStateLastTick) override;
  void SetLateralActionLastTick(LateralAction lastLateralAction) override;
  //! The lateral action of the last cycle.
  LateralAction _lateralActionLastTick;

  //! @brief The longitudinal action state of the last cycle.
  LongitudinalActionState _actionSubStateLastTick{LongitudinalActionState::SPEED_ADJUSTMENT};

  virtual void RunLeadingVehicleSelector() override;

private:
  struct CollisionBrakingEvadingIntensities
  {
    bool collisionUnavoidable;
    bool evading;
    double intensityEvading;
    double intensityBrakingOrEvading;
    double intensityEmergencyBraking;
  };

  //! @brief Returns evading, braking intensities for collision case
  CollisionBrakingEvadingIntensities GetBrakingEvadingIntensititesForCollisionCase(const std::map<CriticalActionState, double>& mapFront, CriticalActionState criticalActionState);

  //! @brief Setting lane change intensities for collision case
  void SetLaneChangeForCollisionCase(LateralAction lateralAction, double intensityEmergencyBraking, double intensityBrakingEvading);

  //! @brief Setting intent to change lane intensities for collision case
  void SetIntentToChangeLanesForCollisionCase(bool evadingFront, bool swervingPermitted, AreaOfInterest aoiCollision, LateralAction intentLaneChange, double intensityEmergencyBraking, double intensityBrakingOrEvading, double intensityEvading, bool useMax);

  //! @brief Setting action pattern intensities for collision case
  void SetActionPatternIntensitiesForCollisionCase(AreaOfInterest aoiCollision, bool swervingPermitted, std::array<bool, 5> lanes, CollisionBrakingEvadingIntensities frontLeft, CollisionBrakingEvadingIntensities intensitiesRight);

  //! @brief Query urgency from MentalModel
  double GetUrgency();

  //! @brief CheckLaneChangePreventionWithCollisionRisk
  //! @param evadingFront         Possibility to evade a collision
  //! @param IntensityBraking     Intensity for a braking maneuver
  void CheckLaneChangePreventionWithCollisionRisk(bool evadingFront, double IntensityBraking);

  virtual bool IsActionNecessary() const;
  void CalculateTTCSteerLimits(bool evadeTargetOnItsLeft);

  virtual bool IsCollisionUnavoidable() const;

  virtual bool IsEvading() const;
  virtual bool IsNormalDriving() const;
  virtual bool IsTTCWithinBrakeLimits() const;
  virtual bool IsTTCWithinSteerLimits() const;

protected:
  //! @brief Determines action limits for time to collision calculations
  //! @param aoi
  //! @param evadeTargetOnItsLeft
  virtual void CalculateTTCActionLimits(AreaOfInterest aoi, bool evadeTargetOnItsLeft);

  //! @brief Set action pattern intensities, if changing right wish is possible or is left intended
  //! @return True if the agent tends to perform a lane change to right, false otherwise
  virtual bool CheckIntensitiesForChangingRight();

  //! @brief Set action pattern intensities, if changing left wish is possible or is left intended
  //! @return True if the agent tends to perform a lane change to left, false otherwise
  virtual bool CheckIntensitiesForChangingLeft();

  //! @brief Decide for the less risk, when there are multiple bad decision options for the lateral movement
  virtual void ChooseLateralActionForMinimalCollisionRiskWhileLateralMovement();

  //! @brief Determine the intensities for lateral behaviour in situations when there is no collision threat with other objects.
  virtual void SetLateralIntensitiesForNoCollisionThreatSituation();

  //! @brief Generate intensities for possible actions for detected lane changer from sides.
  //! @param side
  virtual void GenerateLateralActionPatternIntensitiesForDetectedLaneChanger(Side side);

  //! @brief Queries the actual ActionPatternIntensities.
  //! @return _actionPatternIntensities       Copy of map holding action states and corresponding intensities.
  ActionPatternIntensities GetActionPatternIntensities() const;

  //! @brief Set a flag when the lane change was modified or aborted.
  //! @param unsafeLateralMovement
  //! @param lateralAction
  void DetermineIsLaneChangeModifiedOrAborted(bool unsafeLateralMovement, LateralAction lateralAction);

  //! @brief Determine several intensities for lateral guidance in case of impending collision and while the driver is within a lateral movement.
  //! @param aoiCollision    The area of interest that is checked for possible impending collisions.
  //! @param urgent          Boolean, to tell about the urgency of the future action.
  virtual void ChooseLateralActionIntensitiesForCollisionCaseWhileLateralMovement(AreaOfInterest aoiCollision, bool urgent);

  //! @brief Calculates and collects data for critical action intensities
  //! @param aoi
  virtual void CalculateCriticalActionIntensitiesParameters(AreaOfInterest aoi);

  //! @brief Returns a combination of useable lanes for collision case
  //! @param aoiCollision
  //! @return Returns a combination of useable lanes for collision case
  std::array<bool, 5> GetAvailableLanesForCollisionCase(AreaOfInterest aoiCollision);

  //! @brief Generate intensities for possible actions for mesoscopic situations.
  virtual void GenerateLaneChangeIntensitiesForOnlyMesoscopicSituations();

  //! @brief Generate lane change intensities for zip merging
  virtual void GenerateLaneChangeIntensitiesForZipMerging();

  //! @brief Calculate lateral action intensity VALUES for ending lane (not LateralAction)
  //! @param laneChangeIntensity                 Starting value of the lane change intensity
  //! @param laneKeepingIntensity                Starting value of the lane keeping intensity
  //! @param distanceToEndOfLaneEgo              Distance to the end of the current ego lane in m
  //! @param distanceToEndOfLaneToStartZipMerge  Threshold of distance to the end of the current ego lane to start a zip merge
  //! @return <LaneChangeIntensity, LaneKeepingIntensity>
  virtual std::pair<double, double> CalculateLateralActionIntensityValuesForEndingLane(double laneChangeIntensity,
                                                                                       double laneKeepingIntensity,
                                                                                       units::length::meter_t distanceToEndOfLaneEgo,
                                                                                       units::length::meter_t distanceToEndOfLaneToStartZipMerge);

  //! @brief Generate intensities for possible actions for suspicious vehicle in right lane.
  virtual void GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInRightLane();

  //! @brief Generate intensities for possible actions for suspicious vehicle in left lane.
  virtual void GenerateLateralActionPatternIntensitiesForSuspiciousVehicleInLeftLane();

  //! @brief Generate intensities for possible actions for anticipated lane changer from right.
  virtual void GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromRight();

  //! @brief Generate intensities for possible actions for anticipated lane changer from left.
  virtual void GenerateLateralActionPatternIntensitiesForAnticipatedLaneChangerFromLeft();

  //! @brief Generate intensities for possible actions for free driving.
  virtual void GenerateLateralActionPatternIntensitiesForFreeDriving();

  //! @brief Generate intensities for possible actions for following driving.
  virtual void GenerateLateralActionPatternIntensitiesForFollowingDriving();

  //! @brief Generate intensities for possible actions for situation too close.
  virtual void GenerateLateralActionPatternIntensitiesForTooClose();

  //! @brief Generate intensities for possible actions for situation caused by current obstacle.
  virtual void GenerateLateralActionPatternIntensitiesForObstacleOnCurrentLane();

  //! @brief Determine several intensities for lateral guidance in case of impending collision.
  //! @param aoiCollision        The area of interest that is checked for possible impending collisions.
  //! @param swervingPermitted   Does the situation permit swerving. Temporary parameter until swerving is implemented for all respective situations.
  virtual void ChooseLateralActionIntensitiesForCollisionCase(AreaOfInterest aoiCollision, bool swervingPermitted = false);

  //! @brief Checks whether the agent should be in lane keeping state
  //! @param laneKeeping
  //! @return True if ego should stay in lane keeping, false otherwise
  virtual bool IsLaneKeepingMostAppropriate(const LaneKeepingCalculationsInterface& laneKeeping) const;

  //! @brief Generate intensities for possible actions for anticipated and high risk lane changer.
  virtual void GenerateLateralActionPatternIntensitiesForAnticipatedAndHighRiskLaneChanger();

  //! @brief Checks and evalutes if ego should brake, evade, carry on etc
  //! @param criticalActionIntensities
  //! @param evadeTargetOnItsLeft
  virtual void EvaluateActionIntensities(std::map<CriticalActionState, double>& criticalActionIntensities, bool evadeTargetOnItsLeft);

  //! @brief Checks whether ego agent should brake due to ttc action limits
  //! @return True if ego should brake, false otherwise
  virtual bool IsBraking() const;

  //! @brief Generate intensities for possible actions depending on the actual situation.
  void GenerateLateralActionPatternIntensities();

  //! @brief Generate intensities for possible actions for side collision risk
  //! @param sideAoi
  virtual void GenerateLateralActionPatternIntensitiesForSideCollisionRisk(AreaOfInterest sideAoi);

  //! @brief Change intensities while agent is within the spawn zone.
  void SetIntensitiesWhileLaneChangeBlocked();

  //! @brief Set main lateral direction {-1, 0, 1} of lateral action in mental model memory
  //! @param action determines the direction for the action
  virtual void SetLateralMove(LateralAction action);

  //! @brief Intensities for possible actions depending on the actual situation.
  ActionPatternIntensities _actionPatternIntensities;

  //! @brief Last known intensities for possible actions depending on the actual situation.
  ActionPatternIntensities _lastActionPatternIntensities;

  //! @brief Actions beeing interpreted as a lane change to the right.
  static LateralAction LaneChangingRightActions[];

  //! @brief Actions beeing interpreted as a lane change to the left.
  static LateralAction LaneChangingLeftActions[];

  //! @brief Actions beeing interpreted as a lane keeping and waiting for lane change left.
  static LateralAction LaneKeepingLeftActions[];

  //! @brief Actions beeing interpreted as a lane keeping and waiting for lane change right.
  static LateralAction LaneKeepingRightActions[];

  //! @brief Flag indicating that the driver is currently performing a lane changing action to the left.
  bool isChangingLaneLeft = false;
  //! @brief Flag indicating that the driver is currently performing a lane changing action to the right.
  bool isChangingLaneRight = false;

  //! @brief Flag indicating that the point where lane change intensity reaches its maximum was already passed.
  bool pastPointOfLaneChangeIntensityMax = false;

  //! @brief Flag indicating that the driver is currently performing a lane keeping action.
  bool laneKeeping = true;

  //! @brief time to collision
  units::time::second_t _ttc;

  //! @brief upper and lower limits for ttc-based braking
  TTCActionLimits _ttcActionLimits;

  //! @brief Set of parameters relevant for critical action intensities
  CriticalActionIntensitiesParameters _criticalActionIntensitiesParameters;

  //! @brief Side lane safety module
  std::shared_ptr<SideLaneSafetyInterface> _sideLaneSafety;

private:
  //! @brief stochastics module
  StochasticsInterface* _stochastics = nullptr;

  //! @brief Cycle time [ms].
  units::time::millisecond_t _cycleTime{};

  //! @brief Set the name of the component for debugging.
  const std::string COMPONENTNAME = "ActionManager";

  //! @brief Flag indicating that the point of no return for a successful braking to an upcoming end of lane was already passed.
  bool pastPointOfNoReturnForBrakingToEndOfLane = false;

  //! @brief Flag indicating the state of lane change prevention the last time the lateral action was determined.
  bool lastPreventLaneChangeAtLateralActionDetermination{false};
  //! @brief Flag indicating that there is no microscopic situation to be considered.
  bool _onlyMesoscopicSituation = false;

  //! @brief Flag indicating that the driver needs to perform an urgent lateral manoeuvre.
  bool urgentLateralMovement = false;

  //! @brief Misjudgement factor for acceleration estimation
  const double MIS_JUDGEMENT_FACTOR{.2};

  //! @brief Checks whether a detected lane changer can be avoided
  //! @param laps
  //! @return True if it is possible or makes sense for the agent to avoid the lane changer, false otherwise
  bool CantAvoidDetectedLaneChanger(LateralActionPatternSet laps) const;

  //! @brief Constructs a LateralActionPatternSet for the given side and lane changer
  //! @param side
  //! @return LateralActionPatternSet
  LateralActionPatternSet GenerateLateralActionPatternSetForDetectedLaneChanger(Side side) const;

  //! @brief Sets action pattern intensities to possibly avoid a detected lane changer
  //! @param laps
  void AvoidDetectedLaneChanger(LateralActionPatternSet laps);

  //! @brief Checks whether a given aoi is actually a side aoi
  //! @param sideAoi
  void ValidateSideAoi(AreaOfInterest sideAoi) const;

  //! @brief Checks whether a given aoi is actually a front side aoi
  //! @param frontSideAoi
  void ValidateFrontSideAoi(AreaOfInterest frontSideAoi) const;

  //! @brief Checks whether a vehicle is in the targets side area
  //! @param lateralAction
  //! @param isInTargetLane
  //! @return True if ego is lane changing to a side and there is a vehicle, false otherwise
  virtual bool IsObjectInTargetSide(LateralAction lateralAction, bool isInTargetLane) const;

  //! @brief Checks whether ego changed into an opposing state compared to the previous time step
  //! @return True if the direction of the lateral action changed from the last time step, false otherwise
  bool IsOpposingState() const;

  //! @brief Checks whether the last lateral action was lane changing and as changed to lane keeping
  //! @return True if switch happened, false otherwise
  bool SwitchedFromLaneChangingToLaneKeeping() const;

  //! @brief Checks whether a lane change is viable, i.e. if the relative lane exists and is safe to change to
  //! @param lane
  //! @param aoi
  //! @return True if a safe lane change is possible, false otherwise
  bool IsLaneChangeViable(RelativeLane lane, AreaOfInterest aoi) const;

  //! @brief Checks whether there is an active lane change wish for the given surrounding lane
  //! @param surroundingLane
  //! @return True if there is currently a lane change wish to the surrounding lane, false otherwise
  bool IsLaneChangeWishActive(SurroundingLane surroundingLane) const;

  //! @brief MentalModel access
  MentalModelInterface& _mentalModel;

  //! @brief FeatureExtractor access
  const FeatureExtractorInterface& _featureExtractor;

  //! @brief MentalCalculations access
  const MentalCalculationsInterface& _mentalCalculations;

  //! @brief Swerving module
  SwervingInterface& _swerving;

  //! @brief SurroundingVehicleQueryFactory access
  const SurroundingVehicleQueryFactoryInterface& _surroundingVehicleQueryFactory;

  //! @brief Applicable traffic rules
  const TrafficRulesScm& _trafficRulesScm;

  //! @brief TrajectoryPlanner module
  const TrajectoryPlannerInterface& _trajectoryPlanner;

  //! @brief ZipMerging module
  ZipMergingInterface& _zipMerging;
};
