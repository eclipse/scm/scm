/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  GazeFieldQueryInterface.h
#pragma once
#include <map>

#include "include/common/AreaOfInterest.h"
#include "include/common/SensorDriverScmDefinitions.h"

//! @brief Struct definition for the description of BordersAndOrientation
struct BordersAndOrientation
{
  //! @brief orientation
  units::angle::radian_t orientation{};
  //! @brief leftBorder
  units::angle::radian_t leftBorder{};
  //! @brief rightBorder
  units::angle::radian_t rightBorder{};
};
//! @brief This component acts as a helper for module gazeControl
class GazeFieldQueryInterface
{
public:
  virtual ~GazeFieldQueryInterface() = default;

  //! @brief Checks all orientations and assigns an AOI of possible
  //! @param aoiToCheck
  //! @param vectorAoi
  //! @param isUfov
  //! @param orientations
  //! @param gazeAngleHorizontal
  //! @return vectorAoi enhanced with one additional AOI if assignment was succesful
  virtual std::vector<AreaOfInterest> AssignFieldOfViewToAoi(AreaOfInterest aoiToCheck, const std::vector<AreaOfInterest>& vectorAoi, bool isUfov, const std::vector<units::angle::radian_t>& orientations, units::angle::radian_t gazeAngleHorizontal) const = 0;

  //! @brief Gets Aoi within horizontal limits of 'Useful Filed Of View'
  //! @param bordersAndOrientation
  //! @param aoiToCheck
  //! @param gazeAngleHorizontal
  //! @return aoiToCheck if applicable, otherwise NumberOfAreaOfInterests
  virtual AreaOfInterest AssignUsefulFieldOfViewToAoi(const BordersAndOrientation& bordersAndOrientation, AreaOfInterest aoiToCheck, units::angle::radian_t gazeAngleHorizontal) const = 0;

  //! @brief Updates vector currentAoi based on gazeAngle and orienteations to driver
  //! @param objectScmInformation
  //! @param aoi
  //! @param currentAoi
  //! @param isUfov
  //! @param gazeAngleHorizontal
  //! @return AssignFieldOfViewToAoi with vector of orientationsRelativeToDriver
  virtual std::vector<AreaOfInterest> UpdateAreaOfInterest(const ObjectInformationSCM& objectScmInformation, AreaOfInterest aoi, const std::vector<AreaOfInterest>& currentAoi, bool isUfov, units::angle::radian_t gazeAngleHorizontal) const = 0;
};