/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "GazeControl.h"

#include <algorithm>
#include <map>
#include <memory>
#include <vector>

#include "GazeControl/BottomUpRequest.h"
#include "GazeControl/BottomUpRequestInterface.h"
#include "GazeControl/CurrentGazeState.h"
#include "GazeControl/OpticalInformation.h"
#include "GazeControl/OpticalInformationInterface.h"
#include "GazeControl/TopDownRequest.h"
#include "GazeControl/TopDownRequestInterface.h"
#include "GazeParameters.h"
#include "LaneQueryHelper.h"
#include "MentalModelInterface.h"
#include "ScmDefinitions.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"

GazeControl::GazeControl(MentalModelInterface& mentalModel,
                         StochasticsInterface* stochastics,
                         const FeatureExtractorInterface& featureExtractor,
                         SurroundingObjectsSCM& surroundingObjects,
                         const OwnVehicleInformationSCM& ownVehicleInformationSCM,
                         bool idealPerception,
                         GazeControlComponentsInterface& gazeControlComponents)
    : _mentalModel{mentalModel},
      _surroundingObjects{surroundingObjects},
      _ownVehicleInformationSCM{ownVehicleInformationSCM},
      _featureExtractor{featureExtractor},
      _idealPerception{idealPerception},
      _bottomUpRequest{*gazeControlComponents.GetBottomUpRequest()},
      _topDownRequest{*gazeControlComponents.GetTopDownRequest()},
      _opticalInformation{*gazeControlComponents.GetOpticalInformation()},
      _updateCurrentGazeState{*gazeControlComponents.GetCurrentGazeState()}
{
  _isInInitState = true;  // indicate intialisation state
  _cycleTime = static_cast<units::time::millisecond_t>(_mentalModel.GetCycleTime());
  _stochastics = stochastics;

  _gazeStateIntensities = _topDownRequest.GetIntensitiesForLaneKeepingWithoutLeadingVehicle();
  _meanAndSigmaDurations = GazeParameters::MeanAndSigmaDurationsForLaneKeepingWithoutLeadingVehicle;
  _durationCurrentGazeState = -_cycleTime;
}

void GazeControl::AdvanceGazeState(GazeOverwrite gazeOverwrite)
{
  if (_isInInitState)
  {
    InitializeGazeState();
  }
  else if (_currentGazeState != GazeState::DISTRACTION)
  {
    _gazeStateIntensities = _bottomUpRequest.GetBottomUpIntensities();
  }

  _durationCurrentGazeState += _cycleTime;

  if (gazeOverwrite.gazeFollowerActive)
  {
    OverwriteGazeBehaviour(gazeOverwrite);
  }

  UpdateNewGazeState();  // update gaze state and field of view, when changed by decision in cycle before

  auto supplementedOpticalInformation = _opticalInformation.SupplementOpticalInformation(_surroundingObjects, _gazeAngleHorizontal, _isInInitState, _idealPerception);

  _gazeAngleHorizontal = static_cast<units::angle::radian_t>(supplementedOpticalInformation.gazeAngleHorizontal);
  _surroundingObjects = supplementedOpticalInformation.surroundingObjects;

  if (_isInInitState)
  {
    _isInInitState = false;
  }

  // correct gaze state duration when changing back to random gaze algorithm
  if (gazeOverwrite.gazeFollowerActive && gazeOverwrite.gazeTargetIsLastPointInTimeSeries)
  {
    _durationCurrentGazeState += _cycleTime;
  }

  if (!(gazeOverwrite.gazeFollowerActive && !gazeOverwrite.gazeTargetIsLastPointInTimeSeries))
  {
    ChooseNextGazeState();  // choose next gaze state, but not yet set it
  }
}

void GazeControl::OverwriteGazeBehaviour(GazeOverwrite gazeOverwrite)
{
  // Start saccade at the beginning of a new gaze target
  if (gazeOverwrite.hasGazeTargetChanged)
  {
    _durationCurrentGazeState = 0_ms;
    _plannedGazeState = GazeState::SACCADE;

    if (_currentGazeState != GazeState::SACCADE)
    {
      _previousFixationTarget = _currentGazeState;
    }

    _nextFixationTarget = ScmCommons::MapAreaOfInterestToGazeState(gazeOverwrite.gazeTarget);
    _requiredDurationCurrentGazeState = DrawSaccadeDuration();
  }

  // Start of the actual gaze fixation after the saccade
  if (_durationCurrentGazeState >= _requiredDurationCurrentGazeState)
  {
    _plannedGazeState = ScmCommons::MapAreaOfInterestToGazeState(gazeOverwrite.gazeTarget);
    _requiredDurationCurrentGazeState = gazeOverwrite.durationGazeTarget - _durationCurrentGazeState;
    _durationCurrentGazeState = 0_ms;
  }
}

void GazeControl::InitializeGazeState()
{
  GazeState fixationGazeState = DrawFixation();

  std::map<GazeState, double> stateIntensity;
  stateIntensity[GazeState::SACCADE] = 1.;
  stateIntensity[fixationGazeState] = 1.;

  GazeState initialGazeState = ScmSampler::Sample(stateIntensity, GazeState::EGO_FRONT, _stochastics->GetUniformDistributed(0., 1.));

  if (initialGazeState != GazeState::SACCADE)
  {
    _plannedGazeState = fixationGazeState;
    _requiredDurationCurrentGazeState = DrawFixationDuration();
  }
  else
  {
    _nextFixationTarget = DrawFixation();
    _requiredDurationCurrentGazeState = DrawSaccadeDuration();
  }
}

std::vector<int> GazeControl::GenerateInformationCycles(const units::time::millisecond_t startTime, const units::time::millisecond_t endTime)
{
  if (startTime >= endTime) return {};

  size_t elements = static_cast<size_t>((endTime - startTime + 0.5_ms) / _cycleTime);
  if (elements == 0)
  {
    elements = 1;
  }  // endTime only slightly larger -> include one element

  const int init_value = static_cast<int>(startTime / _cycleTime) + 1;

  std::vector<int> infoCycles(elements);
  std::iota(begin(infoCycles), end(infoCycles), init_value);
  return infoCycles;
}

void GazeControl::UpdateNewGazeState()
{
  GazeStateAndAngle gazeStateAndAngle{
      .currentGazeState = _currentGazeState,
      .gazeAngleHorizontal = _gazeAngleHorizontal};

  auto gaze = _updateCurrentGazeState.UpdateCurrentGazeState(_plannedGazeState, gazeStateAndAngle, _surroundingObjects);

  _gazeAngleHorizontal = gaze.gazeAngleHorizontal;
  _currentGazeState = gaze.currentGazeState;

  // no visual perception due to the current gaze state saccade or distraction
  if (_currentGazeState == GazeState::SACCADE || _currentGazeState == GazeState::DISTRACTION)
  {
    _isNewInformationAcquisitionRequested = false;
    return;
  }

  if (_cycleTime > 0.5 * GazeParameters::DURATION_FADE_IN && _cycleTime > 0.5 * GazeParameters::DURATION_FADE_OUT)
  {
    _isNewInformationAcquisitionRequested = true;
    return;
  }

  // saccadic suppression
  if (_requiredDurationCurrentGazeState <= GazeParameters::DURATION_FADING)
  {
    _isNewInformationAcquisitionRequested = false;
    return;
  }

  // neglecting saccadic suppression for very short fixation times
  if (_requiredDurationCurrentGazeState - GazeParameters::DURATION_FADE_OUT <= _cycleTime)
  {
    _isNewInformationAcquisitionRequested = true;
    return;
  }

  // _requiredDurationCurrentGazeState > one cycletime
  if (_durationCurrentGazeState == 0_ms)  // init vector "informationCycles" in first duration (contains every cycle-number with visual perception for current _requiredDurationCurrentGazeState)
  {
    const auto informationStart = GazeParameters::DURATION_FADE_IN;
    const auto informationEnd = _requiredDurationCurrentGazeState - GazeParameters::DURATION_FADE_OUT;
    _informationCycles = GenerateInformationCycles(informationStart, informationEnd);
  }

  auto isCurrentGazeStateIn = [this](const auto& cycles)
  {
    return std::find(begin(cycles), end(cycles), static_cast<int>(_durationCurrentGazeState / _cycleTime)) != end(cycles);
  };
  _isNewInformationAcquisitionRequested = isCurrentGazeStateIn(_informationCycles);
}

void GazeControl::ChooseNextGazeState()
{
  if (_durationCurrentGazeState >= _requiredDurationCurrentGazeState)
  {
    _durationCurrentGazeState = -_cycleTime;

    if (_currentGazeState == GazeState::SACCADE)  // if the finished gaze state was a saccade
    {
      _plannedGazeState = _nextFixationTarget;  // set gaze target from the last time step, where the saccade was heading to

      _requiredDurationCurrentGazeState = _plannedGazeState == GazeState::DISTRACTION ? DrawDurationDistraction() : DrawFixationDuration();
    }
    else  // if the finished gaze state was a gaze fixation
    {
      _gazeStateIntensities.at(_currentGazeState) = 0.;  // Prevent repeating gaze states
      _previousFixationTarget = _currentGazeState;
      _plannedGazeState = GazeState::SACCADE;
      _nextFixationTarget = DrawFixation();                       // draw the next gaze target for the upcoming saccade
      _requiredDurationCurrentGazeState = DrawSaccadeDuration();  // draw the duration of the upcoming saccade
    }
  }
}

units::time::millisecond_t GazeControl::DrawFixationDuration()
{
  return RoundGazeStateDurationToCycleTime(
      units::math::max(GazeParameters::MINIMUM_DURATION_GAZE_STATE,
                       units::make_unit<units::time::millisecond_t>(std::round(
                           _stochastics->GetLogNormalDistributed(_meanAndSigmaDurations[_plannedGazeState].mean.value(),
                                                                 _meanAndSigmaDurations[_plannedGazeState].sigma.value())))),
      GazeParameters::MINIMUM_DURATION_GAZE_STATE);
}

units::time::millisecond_t GazeControl::DrawDurationDistraction()
{
  const int mean = 1000;
  const int sd = 200;
  auto durationDistraction = _stochastics->GetLogNormalDistributed(mean, sd);
  auto roundedDuration = RoundGazeStateDurationToCycleTime(units::make_unit<units::time::millisecond_t>(durationDistraction), GazeParameters::MINIMUM_DURATION_GAZE_STATE);

  const units::time::millisecond_t min = 400_ms;
  const units::time::millisecond_t max = 3000_ms;
  return std::clamp(roundedDuration, min, max);
}

units::time::millisecond_t GazeControl::DrawSaccadeDuration()
{
  auto duration = 0_ms;
  switch (_nextFixationTarget)
  {
    case GazeState::RIGHT_REAR:
    case GazeState::LEFT_REAR:
    case GazeState::RIGHT_SIDE:
    case GazeState::LEFT_SIDE:
    case GazeState::EGO_REAR:
    case GazeState::INSTRUMENT_CLUSTER:
    case GazeState::INFOTAINMENT:

      // Next gaze target is part of the back field

      if (_nextFixationTarget != _previousFixationTarget)  // Gaze target is changing, so the saccade will be a "long" saccade
      {
        duration = units::make_unit<units::time::millisecond_t>(std::max(std::round(
                                                                             _stochastics->GetNormalDistributed(GazeParameters::MEAN_LONG_SACCADE_DURATION.value(), GazeParameters::SIGMA_LONG_SACCADE_DURATION.value())),
                                                                         0.));
        break;
      }

      // Gaze target is not changing, so the saccade will be a "short" saccade
      duration = units::make_unit<units::time::millisecond_t>(std::round(_stochastics->GetLogNormalDistributed(_meanAndSigmaDurations[GazeState::SACCADE].mean.value(),
                                                                                                               _meanAndSigmaDurations[GazeState::SACCADE].sigma.value())));
      break;

    default:

      // Next gaze target is part of the front field, so the saccade will be a "short" saccade
      duration = units::make_unit<units::time::millisecond_t>(std::round(_stochastics->GetLogNormalDistributed(_meanAndSigmaDurations[GazeState::SACCADE].mean.value(),
                                                                                                               _meanAndSigmaDurations[GazeState::SACCADE].sigma.value())));
      break;
  }

  return RoundGazeStateDurationToCycleTime(duration + GazeParameters::STATIONARY_PART_SACCADE_DURATION);
}

units::time::millisecond_t GazeControl::RoundGazeStateDurationToCycleTime(units::time::millisecond_t duration, units::time::millisecond_t minimumDuration) const
{
  duration = units::math::max(duration, minimumDuration);
  const auto lowerValue = std::floor(duration.value() / _cycleTime.value()) * _cycleTime;
  const auto upperValue = lowerValue + _cycleTime;

  if (duration < lowerValue || duration > upperValue)
  {
    throw std::runtime_error("GazeControl::RoundGazeStateDurationToCycleTime: Duration outside determined boundaries!");
  }

  if (lowerValue < minimumDuration) return upperValue;
  if (duration - lowerValue < upperValue - duration)
    return lowerValue;
  else
    return upperValue;
}

GazeState GazeControl::DrawFixation()
{
  return ScmSampler::Sample(_gazeStateIntensities, GazeState::EGO_FRONT, _stochastics->GetUniformDistributed(0., 1.));
}

GazeState GazeControl::GetCurrentGazeState() const
{
  return _currentGazeState;
}

bool GazeControl::IsNewInformationAcquisitionRequested() const
{
  return _isNewInformationAcquisitionRequested;
}

void GazeControl::UpdateGazeRequests(LateralAction actionState, const std::map<AreaOfInterest, double>& topDownRequestMap, const std::vector<AdasHmiSignal>& opticAdasSignals, const AdasHmiSignal& combinedSignal, AuditoryPerceptionInterface& auditoryPerception, double distractionPercentage)
{
  auto gazeIntensitiesAndDurations = _topDownRequest.Update(actionState, topDownRequestMap, _isInInitState);

  if (_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->isDistracted)
  {
    gazeIntensitiesAndDurations.gazeStateIntensities[GazeState::DISTRACTION] = distractionPercentage;
  }
  else
  {
    gazeIntensitiesAndDurations.gazeStateIntensities[GazeState::DISTRACTION] = 0;
  }

  if (!_isInInitState)
  {
    _gazeStateIntensities = gazeIntensitiesAndDurations.gazeStateIntensities;
    _meanAndSigmaDurations = gazeIntensitiesAndDurations.distributions;
  }

  _bottomUpRequest.Update(_knownObjectIds, gazeIntensitiesAndDurations.gazeStateIntensities, _surroundingObjects, _ownVehicleInformationSCM, opticAdasSignals, auditoryPerception, combinedSignal, *this);
}

void GazeControl::SetKnownObjectIds(const std::vector<int>& knownIds)
{
  _knownObjectIds.clear();
  _knownObjectIds = knownIds;
}

void GazeControl::SetHafSignalProcessed(bool processedSignal)
{
  _hafSignalProcessed = processedSignal;
}

bool GazeControl::GetHafSignalProcessed() const
{
  return _hafSignalProcessed;
}