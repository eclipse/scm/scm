/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  GazeControlComponentsInterface.h
#pragma once
#include "GazeControl/BottomUpRequestInterface.h"
#include "GazeControl/CurrentGazeStateInterface.h"
#include "GazeControl/GazeFieldQueryInterface.h"
#include "GazeControl/GazeFoveaInterface.h"
#include "GazeControl/GazePeripheryInterface.h"
#include "GazeControl/GazeUsefulFieldOfViewInterface.h"
#include "GazeControl/ImpulseRequestInterface.h"
#include "GazeControl/OpticalInformationInterface.h"
#include "GazeControl/StimulusRequestInterface.h"
#include "GazeControl/TopDownRequestInterface.h"

//! @brief Implements the GazeControlComponents
class GazeControlComponentsInterface
{
public:
  virtual ~GazeControlComponentsInterface() = default;
  //! @brief Get bottomUpRequestInterface
  //! @return BottomUpRequest
  virtual BottomUpRequestInterface* GetBottomUpRequest() = 0;
  //! @brief Get impulseRequestInterface
  //! @return ImpulseRequest
  virtual ImpulseRequestInterface* GetImpulseRequest() = 0;
  //! @brief Get StimulusRequestInterface
  //! @return StimulusRequest
  virtual StimulusRequestInterface* GetStimulusRequest() = 0;
  //! @brief Get TopDownRequestInterface
  //! @return TopDownRequest
  virtual TopDownRequestInterface* GetTopDownRequest() = 0;
  //! @brief Get GazeFieldQueryInterface
  //! @return GazeFieldQuery
  virtual GazeFieldQueryInterface* GetGazeFieldQuery() = 0;
  //! @brief Get GazePeripheryInterface
  //! @return GazePeriphery
  virtual GazePeripheryInterface* GetGazePeriphery() = 0;
  //! @brief Get OpticalInformationInterface
  //! @return OpticalInformation
  virtual OpticalInformationInterface* GetOpticalInformation() = 0;
  //! @brief Get GazeUsefulFieldOfViewInterface
  //! @return GazeUsefulFieldOfView
  virtual GazeUsefulFieldOfViewInterface* GetUsefulFieldOfView() = 0;
  //! @brief Get GazeFoveaInterface
  //! @return GazeFovea
  virtual GazeFoveaInterface* GetGazeFovea() = 0;
  //! @brief Get CurrentGazeStateInterface
  //! @return CurrentGazeState
  virtual CurrentGazeStateInterface* GetCurrentGazeState() = 0;
};