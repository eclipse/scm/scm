/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  ImpulseRequest.h
#pragma once
#include <map>

#include "ImpulseRequestInterface.h"
#include "MentalModelInterface.h"
#include "include/common/ScmEnums.h"

//! @brief This component update the impulse request
class ImpulseRequest : public ImpulseRequestInterface
{
public:
  //! @brief Constructor initializes and creates ImpulseRequest.
  //! @param mentalModel
  ImpulseRequest(const MentalModelInterface& mentalModel)
      : _mentalModel(mentalModel)
  {
  }

  std::map<GazeState, double> UpdateImpulseDataFront(std::map<GazeState, double>& gazeStateIntensities, const std::vector<ObjectInformationSCM>* objects, AreaOfInterest aoi) override;
  std::map<GazeState, double> UpdateImpulseDataEgoFront(std::map<GazeState, double>& gazeStateIntensities, const std::vector<ObjectInformationSCM>* objects) override;

private:
  //! @brief Check is non exiting lane observed
  //! @param aoi         Area of Interest
  //! @return yes/no
  bool IsNonExistingLaneObserved(AreaOfInterest aoi);

  //! @brief Check if area of interest is contained in a list.
  //! @param aoi         Area of Interest
  //! @param aoiList     List of one or more area of interest(s)
  //! @return yes/no
  static bool IsAreaOfInterestPartOfList(AreaOfInterest aoi, std::vector<AreaOfInterest> aoiList);

  //! @brief check is break light switch on
  //! @param lastState
  //! @param currentState
  //! @return yes/no
  bool BrakeLightsSwitchedOn(const bool lastState, const bool currentState) const;

  //! @brief check is indicator switch on
  //! @param lastState
  //! @param currentState
  //! @return yes/no
  bool IndicatorSwitchedOn(const scm::LightState::Indicator lastState, const scm::LightState::Indicator currentState) const;

  //! @brief Indicator stae of the vehicle to the left in the previous cycle.
  scm::LightState::Indicator _prevIndicatorLeft = scm::LightState::Indicator::Off;
  //! @brief Indicator stae of the vehicle to the right in the previous cycle.
  scm::LightState::Indicator _prevIndicatorRight = scm::LightState::Indicator::Off;
  //! @brief Impuls from LEFT_FRONT or RIGHT_FRONT.
  bool _impulsFrontSide = false;
  //! @brief Gaze intensity when it IsUrgentlyLateWithUpdate
  static constexpr double HIGH_GAZE_INTENSITY = 2.;
  //! @brief Brake light state of the vehicle to the left in the previous cycle.
  bool _prevBrakeLightLeft = false;
  //! @brief Brake light state of the vehicle to the right in the previous cycle.
  bool _prevBrakeLightRight = false;
  //! @brief  mentalModel
  const MentalModelInterface& _mentalModel;
};