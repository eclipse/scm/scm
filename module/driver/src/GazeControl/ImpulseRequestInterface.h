/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  ImpulseRequestInterface.h
#pragma once
#include "include/common/ScmEnums.h"
#include "include/common/SensorDriverScmDefinitions.h"

//! @brief Implements the ImpulseRequest
class ImpulseRequestInterface
{
public:
  virtual ~ImpulseRequestInterface() = default;

  //! @brief update impulse request in front by given aoi
  //! @param gazeStateIntensities
  //! @param objects
  //! @param aoi
  //! @return updated map with Gazestates and intensities
  virtual std::map<GazeState, double> UpdateImpulseDataFront(std::map<GazeState, double>& gazeStateIntensities, const std::vector<ObjectInformationSCM>* objects, AreaOfInterest aoi) = 0;

  //! @brief update impulse request in front of ego
  //! @param gazeStateIntensities
  //! @param objects
  //! @return updated map with Gazestates and intensities
  virtual std::map<GazeState, double> UpdateImpulseDataEgoFront(std::map<GazeState, double>& gazeStateIntensities, const std::vector<ObjectInformationSCM>* objects) = 0;
};