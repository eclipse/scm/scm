/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  GazeFieldQuery.h
#pragma once

#include "GazeControl/GazeFieldQueryInterface.h"
#include "MentalModelInterface.h"

//! @brief This component acts as a helper for module gazeControl
class GazeFieldQuery : public GazeFieldQueryInterface
{
public:
  //! @brief specialized constructor
  //! @param mentalModel
  GazeFieldQuery(const MentalModelInterface& mentalModel)
      : _mentalModel(mentalModel)
  {
  }

  std::vector<AreaOfInterest> AssignFieldOfViewToAoi(AreaOfInterest aoiToCheck, const std::vector<AreaOfInterest>& vectorAoi, bool isUfov, const std::vector<units::angle::radian_t>& orientations, units::angle::radian_t gazeAngleHorizontal) const override;
  AreaOfInterest AssignUsefulFieldOfViewToAoi(const BordersAndOrientation& bordersAndOrientation, AreaOfInterest aoiToCheck, units::angle::radian_t gazeAngleHorizontal) const override;
  std::vector<AreaOfInterest> UpdateAreaOfInterest(const ObjectInformationSCM& objectScmInformation, AreaOfInterest aoi, const std::vector<AreaOfInterest>& currentAoi, bool isUfov, units::angle::radian_t gazeAngleHorizontal) const override;

protected:
  //! @brief Determines left and right border of horizontal gaze angle
  //! @param isUfov
  //! @param gazeAngleHorizontal
  //! @return left and right border
  std::tuple<units::angle::radian_t, units::angle::radian_t> DetermineGazeAngleSideBorders(bool isUfov, units::angle::radian_t gazeAngleHorizontal) const;

  //! @brief Corrects side border range based on change of quadrants
  //! @param border
  //! @return corrected border
  units::angle::radian_t CorrectGazeAngleSideBordersDueChangeOfQuadrants(units::angle::radian_t border) const;

  //! @brief Gets aoi within horizontal limits
  //! @param bordersAndOrientation
  //! @param ufovLimit
  //! @param gazeAngleHorizontal
  //! @return true if aoi in Horizontal Limits
  bool IsAoiWithinHorizontalLimits(const BordersAndOrientation& bordersAndOrientation, units::angle::radian_t ufovLimit, units::angle::radian_t gazeAngleHorizontal) const;

  //! @brief Gets Aoi within horizontal limits of periphery
  //! @param bordersAndOrientation
  //! @param aoiToCheck
  //! @param gazeAngleHorizontal
  //! @return aoiToCheck if applicable, otherwise NumberOfAreaOfInterests
  AreaOfInterest AssignPeripheryFieldOfViewToAoi(const BordersAndOrientation& bordersAndOrientation, AreaOfInterest aoiToCheck, units::angle::radian_t gazeAngleHorizontal) const;

private:
  //! @brief Check if area of interest is contained in a list.
  //! @param aoi         Area of Interest
  //! @param aoiList     List of one or more area of interest(s)
  //! @return true when Aoi is part of List
  bool IsAreaOfInterestPartOfList(AreaOfInterest aoi, const std::vector<AreaOfInterest>& aoiList) const;

  //! @brief Interface to the MentalModel
  const MentalModelInterface& _mentalModel;
};