/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  OpticalInformation.h
#pragma once

#include <memory>

#include "GazeControl/GazeFieldQueryInterface.h"
#include "GazeControl/OpticalInformationInterface.h"
#include "MentalModelInterface.h"

//! @brief contains the gaze orientations
struct GazeOrientations
{
  //! @brief evaluateMirror
  bool evaluateMirror{false};
  //! @brief opticalAnglePositionHorizontal
  units::angle::radian_t opticalAnglePositionHorizontal{-ScmDefinitions::DEFAULT_VALUE};
  //! @brief opticalAngleSizeHorizontal
  units::angle::radian_t opticalAngleSizeHorizontal{-ScmDefinitions::DEFAULT_VALUE};
  //! @brief minimumOrientationRelativToViewAxis
  units::angle::radian_t minimumOrientationRelativToViewAxis{-ScmDefinitions::DEFAULT_VALUE};
  //! @brief maximumOrientationRelativToViewAxis
  units::angle::radian_t maximumOrientationRelativToViewAxis{ScmDefinitions::DEFAULT_VALUE};
};

//! @brief contains the optical angle
struct OpticalAngle
{
  //! @brief opticalAnglePositionHorizontal
  units::angle::radian_t opticalAnglePositionHorizontal{ScmDefinitions::DEFAULT_VALUE};
  //! @brief opticalAngleSizeHorizontal
  units::angle::radian_t opticalAngleSizeHorizontal{ScmDefinitions::DEFAULT_VALUE};
};

//! @brief this class determine the optical gaze information
class OpticalInformation : public OpticalInformationInterface
{
public:
  //! @brief Constructor initializes and creates OpticalInformation.
  //! @param mentalModel
  OpticalInformation(const MentalModelInterface& mentalModel)
      : _mentalModel(mentalModel)
  {
  }

  OpticalValues SupplementOpticalInformation(SurroundingObjectsSCM surroundingObjects, units::angle::radian_t gazeAngleHorizontal, bool isInit, bool idealPerception) const override;

protected:
  //! @brief Calculate opticalAnglePositionHorizontal and opticalAngleSizeHorizontal for an object, derived from its bounding box orientations relative to driver.
  //! @param isInit                          gazeControl is Init
  //! @param orientationsRelativeToDriver    Vector of bounding box orientation angles in rad relative to driver position.
  //! @param aoi                             AreaOfInterest for witch the calculation is to be done (to determine between mirror and direct views)
  //! @param gazeAngleHorizontal
  //! @param idealPerception
  //! @return OpticalAngle struct in rad
  OpticalAngle CalculateOpticalInformationForSurroundingObject(bool isInit, const std::vector<units::angle::radian_t>& orientationsRelativeToDriver, const AreaOfInterest aoi, units::angle::radian_t gazeAngleHorizontal, bool idealPerception) const;

  //! @brief adjust current object optical angle and threshold looming
  //! @param IsFoveaOrSpawn
  //! @param currentObjectInformationSCM
  //! @param opticalAngle
  //! @return ObjectInformationSCM
  ObjectInformationSCM AdjustCurrentObjectOpticalAngleAndThresholdLooming(bool IsFoveaOrSpawn, const ObjectInformationSCM& currentObjectInformationSCM, const OpticalAngle& opticalAngle) const;

  //! @brief anaylze orientations vector to determine min and max orientations and optical angle position and size
  //! @param isInit
  //! @param orientationsRelativeToDriver
  //! @param gazeAngleHorizontal
  //! @param gazeOrientations
  //! @param idealPerception
  //! @return GazeOrientations
  GazeOrientations AnalyzeOrientationsRelativeToDriver(bool isInit, const std::vector<units::angle::radian_t>& orientationsRelativeToDriver, units::angle::radian_t gazeAngleHorizontal, const GazeOrientations& gazeOrientations, bool idealPerception) const;

  //! @brief adjust optical angle Size based on min and max orientations
  //! @param gazeOrientations
  //! @return opticalAngleSizeHorizontal
  units::angle::radian_t AdjustOpticalAngleSizeHorizontal(const GazeOrientations& gazeOrientations) const;

  //! @brief adjust optical angle position based on min and max orientations
  //! @param gazeOrientations
  //! @return opticalAnglePositionHorizontal
  units::angle::radian_t AdjustOpticalAnglePositionHorizontal(const GazeOrientations& gazeOrientations) const;

  //! @brief correct optical angle position for mirror fixation
  //! @param aoi
  //! @param opticalAnglePosition
  //! @param gazeAngleHorizontal
  //! @return opticalAnglePositionHorizontal for mirror
  units::angle::radian_t CorrectOpticalAnglePositionForMirror(AreaOfInterest aoi, units::angle::radian_t opticalAnglePosition, units::angle::radian_t gazeAngleHorizontal) const;

  //! @brief apply hemicylce changes to orientationRelativeToViewAxis
  //! @param orientationRelativToViewAxis
  //! @return orientationRelativToViewAxis
  units::angle::radian_t ApplyHemicycleChanges(units::angle::radian_t orientationRelativToViewAxis) const;

private:
  //! @brief Create a map of pointers to the ground truth surrounding objects according to there AreasOfInterest
  //! @param surroundingObjects_GroundTruth  Ground truth data to be supplemented
  //! @return map with aois respective Vehicles
  std::map<AreaOfInterest, std::vector<ObjectInformationSCM>*> MapSurroundingAoiObjectsGroundTruth(SurroundingObjectsSCM& surroundingObjects_GroundTruth) const;

  //! @brief Determine, if a given AreaOfInterest is observed via mirrors or by direct gaze fixation
  //! @param aoi AreaOfInterest to be evaluated
  //! @return yes/no
  bool IsMirrorAreaOfInterest(const AreaOfInterest aoi) const;

  //! @brief mentalModel
  const MentalModelInterface& _mentalModel;
};