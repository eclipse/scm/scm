/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "GazeControl/GazeUsefulFieldOfView.h"

#include <vector>

#include "GazeControl/GazeParameters.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/SensorDriverScmDefinitions.h"

std::vector<AreaOfInterest> GazeUsefulFieldOfView::GetUsefulFieldOfViewAois(const SurroundingObjectsSCM& surroundingObjects, units::angle::radian_t gazeAngleHorizontal) const
{
  std::vector<AreaOfInterest> ufov{};
  AreaOfInterest fovea{_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->fovea};

  auto rearAndInteriouerUfovs = GazeParameters::GetRearAndInteriourAoiUfovMap();

  if (rearAndInteriouerUfovs.count(fovea) > 0)
  {
    ufov = rearAndInteriouerUfovs[fovea];
  }
  else
  {
    ufov = GenerateUfovForFrontAndSides(ufov, surroundingObjects.ongoingTraffic, gazeAngleHorizontal);
  }

  return ufov;
}

std::vector<AreaOfInterest> GazeUsefulFieldOfView::GenerateUfovForFrontAndSides(std::vector<AreaOfInterest> ufov, const OngoingTrafficObjectsSCM& ongoingTraffic, units::angle::radian_t gazeAngleHorizontal) const
{
  for (const auto& aoi : FRONT_SIDE_AREAS_OF_INTEREST)
  {
    std::vector<units::angle::radian_t> orientationRelativeToDriver{};
    switch (aoi)
    {
      case AreaOfInterest::HUD:
        if (_mentalModel.GetInteriorHudExistence())
        {
          orientationRelativeToDriver.push_back(_mentalModel.GetMicroscopicData()->GetOwnVehicleInformation()->HeadUpDisplayOrientationRelativeToDriver);
          ufov = _gazeFieldQuery.AssignFieldOfViewToAoi(AreaOfInterest::HUD, ufov, true, orientationRelativeToDriver, gazeAngleHorizontal);
        }
        break;

      default:
        if (auto object = ongoingTraffic.FromAreaOfInterest(aoi); !object->empty())
        {
          ufov = _gazeFieldQuery.UpdateAreaOfInterest(object->front(), aoi, ufov, true, gazeAngleHorizontal);
        }
        break;
    }
  }
  return ufov;
}