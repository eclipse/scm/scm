/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  GazeFovea.h
#pragma once

#include "GazeControl/GazeFieldQuery.h"
#include "GazeControl/GazeFoveaInterface.h"
#include "MentalModelInterface.h"
#include "include/common/AreaOfInterest.h"

//! @brief Get gazeAngleHorizontal for currentGazeState
class GazeFovea : public GazeFoveaInterface
{
public:
  //! @brief specialized constructor
  //! @param mentalModel
  GazeFovea(const MentalModelInterface& mentalModel)
      : _mentalModel(mentalModel)
  {
  }
  units::angle::radian_t GetFoveaGazeAngleHorizontal(GazeState currentGazeState, const SurroundingObjectsSCM& surroundingObjects) const override;

private:
  //! @brief Interface to the MentalModel
  const MentalModelInterface& _mentalModel;
};