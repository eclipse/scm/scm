/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  GazePeripheryInterface.h
#pragma once

#include "include/common/ScmEnums.h"
#include "include/common/SensorDriverScmDefinitions.h"

//! @brief Get the periphery aois from the current gaze
class GazePeripheryInterface
{
public:
  virtual ~GazePeripheryInterface() = default;

  //! @brief Assign AreaOfInterests to peripheral field of view, according to current gaze fixation.
  //! @param surroundingObjects
  //! @param gazeAngleHorizontal
  //! @return vector of periphery aois
  virtual std::vector<AreaOfInterest> GetPeripheryAois(const SurroundingObjectsSCM& surroundingObjects, units::angle::radian_t gazeAngleHorizontal) const = 0;
};