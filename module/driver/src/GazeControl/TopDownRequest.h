/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file  TopDownRequest.h
#pragma once

#include <map>

#include "FeatureExtractorInterface.h"
#include "LateralAction.h"
#include "MentalModelInterface.h"
#include "TopDownRequestInterface.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmEnums.h"

//! @brief this class determine the top down request
class TopDownRequest : public TopDownRequestInterface
{
public:
  //! @brief Constructor initializes and creates TopDownRequest.
  //! @param featureExtractor
  //! @param stochastics
  //! @param mentalModel
  TopDownRequest(const FeatureExtractorInterface& featureExtractor, StochasticsInterface* stochastics, const MentalModelInterface& mentalModel)
      : _featureExtractor(featureExtractor), _stochastics(stochastics), _mentalModel(mentalModel)
  {
  }

  GazeStateIntensitiesAndDurations Update(LateralAction actionState, std::map<AreaOfInterest, double> topDownRequestMap, bool isInInitState) const override;
  std::map<GazeState, double> GetIntensitiesForLaneKeepingWithoutLeadingVehicle() const override;

protected:
  //! @brief Change intensities to avoid impossibilities
  //! @param intensities
  //! @return map with gazeState and intensities
  std::map<GazeState, double> ChangeIntensitiesToAvoidImpossibilities(std::map<GazeState, double> intensities) const;

  //! @brief Change intensities for information requests
  //! @param topDownRequestMap
  //! @param gazeStateIntensities
  //! @return map with gazeState and intensities
  std::map<GazeState, double> ChangeIntensitiesForInformationRequests(std::map<AreaOfInterest, double>& topDownRequestMap, const std::map<GazeState, double>& gazeStateIntensities) const;

  //! @brief Change intensities for vehicles overtaking
  //! @param gazeStateIntensities
  //! @return map with gazeState and intensities
  std::map<GazeState, double> ChangeIntensitiesForVehiclesOvertaking(const std::map<GazeState, double>& gazeStateIntensities) const;

private:
  //! @brief Get intensities and durations for lane Keeping
  //! @return gazestate intensities and durations
  GazeStateIntensitiesAndDurations GetIntensitiesAndDurationsForLaneKeeping() const;

  //! @brief Get intensities for slowly driving
  //! @return map with gazeState and intensities
  std::map<GazeState, double> GetIntensitiesForSlowlyDriving() const;

  //! @brief Get intensities for lane keeping with leading vehicle
  //! @return map with gazeState and intensities
  std::map<GazeState, double> GetIntensitiesForLaneKeepingWithLeadingVehicle() const;

  //! @brief Get intensities for lane changing actions to right lane
  //! @return map with gazeState and intensities
  std::map<GazeState, double> GetIntensitiesForLaneChangingActionsToRightLane() const;

  //! @brief Get intensities for lane changing actions to left lane
  //! @return map with gazeState and intensities
  std::map<GazeState, double> GetIntensitiesForLaneChangingActionsToLeftLane() const;

  //! @brief Get intensities for ambientNoise
  //! @return map with gazeState and intensities
  std::map<GazeState, double> GetIntensitiesForAmbientNoise() const;

  //! @brief featureExtractor
  const FeatureExtractorInterface& _featureExtractor;
  //! @brief stochastics
  StochasticsInterface* _stochastics;
  //! @brief mentalModel
  const MentalModelInterface& _mentalModel;
};