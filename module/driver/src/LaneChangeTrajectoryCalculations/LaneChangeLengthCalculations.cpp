/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "LaneChangeLengthCalculations.h"

#include <cmath>

namespace
{
bool TtcInvalid(units::time::second_t ttc)
{
  return ttc >= ScmDefinitions::TTC_LIMIT ||
         ttc < 0._s;
}
}  // namespace

units::length::meter_t LaneChangeLengthCalculations::DetermineFreeLaneChangeLength(units::time::second_t timeHeadwayFreeLaneChange, units::velocity::meters_per_second_t egoVelocityLongitudinal) const
{
  constexpr auto MIN_FREE_LANE_CHANGE_LENGTH{40.0_m};
  return units::math::max(MIN_FREE_LANE_CHANGE_LENGTH, units::time::second_t(timeHeadwayFreeLaneChange) * egoVelocityLongitudinal);
}

units::length::meter_t LaneChangeLengthCalculations::DetermineLaneChangeLengthFromObstacle(const SurroundingVehicleInterface* laneChangeObstacle, units::velocity::meters_per_second_t egoVelocityLongitudinal, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory) const
{
  if (laneChangeObstacle == nullptr)
  {
    return ScmDefinitions::INF_DISTANCE;
  }

  const auto query{surroundingVehicleQueryFactory.GetQuery(*laneChangeObstacle)};
  const auto ttc{laneChangeObstacle->ToTheSideOfEgo() ? query->GetLateralObstructionDynamics().timeToEnter : laneChangeObstacle->GetTtc().GetValue()};
  if (TtcInvalid(ttc))
  {
    return ScmDefinitions::INF_DISTANCE;
  }

  if (laneChangeObstacle->ToTheSideOfEgo())
  {
    return ttc * egoVelocityLongitudinal;
  }

  return laneChangeObstacle->GetRelativeNetDistance().GetValue() + (ttc - laneChangeObstacle->GetThw().GetValue()) * egoVelocityLongitudinal;
}

units::length::meter_t LaneChangeLengthCalculations::DetermineLaneChangeLengthFromHighwayExit(const TrajectoryPlanning::HighwayExitInformation& exitInformation, units::velocity::meters_per_second_t egoVelocityLongitudinal) const
{
  static constexpr auto MINIMUM_SAFETY_BUFFER_DISTANCE{20.0_m};
  const auto safetyBufferDistanceToEndOfExit{units::math::max(MINIMUM_SAFETY_BUFFER_DISTANCE, exitInformation.minimumTimeOnExitLane * egoVelocityLongitudinal)};

  if (exitInformation.distanceToEndOfExit <= safetyBufferDistanceToEndOfExit)
  {
    return ScmDefinitions::INF_DISTANCE;
  }

  const auto availableDistance{exitInformation.distanceToEndOfExit - safetyBufferDistanceToEndOfExit};
  return availableDistance / exitInformation.numberOfLanesToCross;
}