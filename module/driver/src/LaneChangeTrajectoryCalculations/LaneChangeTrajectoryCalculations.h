/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LaneChangeTrajectoryCalculations.h

#pragma once

#include "LaneChangeTrajectoryCalculations/LaneChangeLengthCalculationsInterface.h"
#include "LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculationsInterface.h"
#include "MentalModelInterface.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"

//! @brief Implements the LaneChangeTrajectoryCalculationsInterface.
class LaneChangeTrajectoryCalculations : public LaneChangeTrajectoryCalculationsInterface
{
public:
  LaneChangeTrajectoryCalculations(std::shared_ptr<LaneChangeLengthCalculationsInterface> laneChangeLengthCalculations);
  const SurroundingVehicleInterface* DetermineReferenceObjectForLaneChange(Situation currentSituation, const TrajectoryPlanning::RelevantVehiclesForLaneChangeLength& relevantVehicles) const override;
  units::length::meter_t DetermineLaneChangeLength(const TrajectoryPlanning::LaneChangeLengthParameters& parameters, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory) const override;
  units::length::meter_t DetermineLaneChangeWidth(const TrajectoryPlanning::LaneChangeWidthParameters& parameters) const override;
  bool CanFinishLaneChangeWithoutEnteringMinDistance(const TrajectoryPlanning::SafetyParameters& parameters, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory) const override;

protected:
  //! @brief Returns true if the vehicle directly in front of ego is more relevant for the lane change than the given reference vehicle.
  bool EgoFrontVehicleMoreUrgentThanReference(const SurroundingVehicleInterface* egoFrontVehicle, const SurroundingVehicleInterface* referenceVehicle) const;

private:
  //! @brief Holds LaneChangeLengthCalculations
  std::shared_ptr<LaneChangeLengthCalculationsInterface> _laneChangeLengthCalculations;
};
