/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LaneChangeLengthCalculations.h

#pragma once

#include "LaneChangeLengthCalculationsInterface.h"
#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryFactoryInterface.h"
#include "SurroundingVehicles/SurroundingVehicleQueryInterface.h"
#include "TrajectoryPlanner/TrajectoryPlanningTypes.h"

//! @brief Implements the LaneChangeLengthCalculationsInterface
class LaneChangeLengthCalculations : public LaneChangeLengthCalculationsInterface
{
public:
  units::length::meter_t DetermineLaneChangeLengthFromObstacle(const SurroundingVehicleInterface* laneChangeObstacle, units::velocity::meters_per_second_t egoVelocityLongitudinal, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory) const override;
  units::length::meter_t DetermineFreeLaneChangeLength(units::time::second_t timeHeadwayFreeLaneChange, units::velocity::meters_per_second_t egoVelocityLongitudinal) const override;
  units::length::meter_t DetermineLaneChangeLengthFromHighwayExit(const TrajectoryPlanning::HighwayExitInformation& exitInformation, units::velocity::meters_per_second_t egoVelocityLongitudinal) const override;
};
