/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file IgnoringOuterLaneOvertakingProhibition.h

#pragma once
#include "OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibitionInterface.h"

class MentalModelInterface;

//! ************************************************************************************************************************************
//! @brief Especially on the german highway, it is prohibited to overtake on the outer (right) lane.
//! This class will roll stochastically violations against this rule based on the corresponding driver parameter in the ProfilesCatalog.
//! ************************************************************************************************************************************
class IgnoringOuterLaneOvertakingProhibition : public IgnoringOuterLaneOvertakingProhibitionInterface
{
public:
  IgnoringOuterLaneOvertakingProhibition(MentalModelInterface& mentalModel);

  void Update(int egoFrontId, int sideFrontId) override;
  void Update() override;
  bool IsFulfilled(AreaOfInterest aoi) const override;

private:
  //! @brief Rolls stochastically if the prohibition should be ignored for the given aoi
  //! @param aoi aoi for which is rolled if the prohibition will be ignored
  void RollIsRightOvertakingProhibitionIgnoringQuotaFulfilled(AreaOfInterest aoi);

  //! @brief Checks whether there is a new agent (id) in the aoi
  //! @param aoi aoi to check
  //! @param id id of the agent
  //! @return true if the agent id changed in the aoi
  bool IsNewAgentInAoi(AreaOfInterest aoi, int id) const;

  //! @brief internal struct for mapping aoi's to relevant agents
  struct CausingAgent
  {
    int _id{-1};
    bool _quotaFulfilled{false};
  };

  MentalModelInterface& _mentalModel;

  CausingAgent _agentEgoFront;
  CausingAgent _agentLeftFront;
  CausingAgent _agentRightFront;
  std::map<AreaOfInterest, CausingAgent> _aoiToCausingAgents;
};
