/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <TrajectoryCalculations/SteeringManeuver.h>

#include <vector>

#include "TrajectoryCalculations/TrajectoryCalculationTypes.h"
#include "module/parameterParser/src/Signals/ParametersScmDefinitions.h"

// Types for internal use only
namespace
{
struct DriverParametersInternal
{
  units::angular_velocity::radians_per_second_t maxSteeringWheelSpeed;
  units::acceleration::meters_per_second_squared_t maxLateralAcceleration;
};
}  // namespace

namespace TrajectoryCalculations
{
//! \brief Calculates a s-curve trajectory in s/t coordinates with the given dimensions.
//! \throws std::invalid_argument on parameterization errors
Trajectory CalculateTrajectory(const TrajectoryDimensions& dimensions, const DriverParameters& driverParameters, const VehicleParameters& vehicleParameters, units::length::meter_t planningInterval = 0.1_m);

//! \brief Validates trajectory parameters.
//! \throws std::invalid_argument on parameterization errors
void ValidateTrajectoryParameters(const TrajectoryDimensions& dimensions);

//! \brief Calculates the required headings to reach the desired width and length of the trajectory.
TrajectoryHeadings CalculateHeadings(const TrajectoryDimensions& dimensions);

//! \brief Calculates the necessary first and second steering trajectories to create the previously determined heading offsets.
SteeringTrajectories CalculateSteeringTrajectories(const TrajectoryParameters& trajectoryParameters, const DriverParametersInternal& driverParameters, const VehicleParameters& vehicleParameters);

//! \brief Combines the trajectory pieces consisting of the steering trajectories and an optional straight section that might be empty.
Trajectory CombineTrajectories(const SteeringTrajectories& steeringTrajectories, const Trajectory& straightSectionTrajectory);
}  // namespace TrajectoryCalculations