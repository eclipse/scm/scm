/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include <iostream>
#include <memory>

#include "AuditoryPerceptionInterface.h"
#include "Merging/ZipMergingInterface.h"
#include "ScmComponentsInterface.h"
#include "SideLaneSafety/SideLaneSafetyInterface.h"
#include "TrajectoryPlanner/TrajectoryPlannerInterface.h"

//! @brief ScmComponents is a collection of all different components (e.g. MentalModel, InformationAcquisition, ActionManager, ...) and dependencies SCM consists of.
class ScmComponents : public ScmComponentsInterface
{
public:
  //! @brief Constructor initializes and creates ScmComponents.
  //! @param scmDependencies
  explicit ScmComponents(ScmDependenciesInterface& scmDependencies);
  virtual ~ScmComponents(){};

  MentalModelInterface* GetMentalModel() override;
  GazeControlInterface* GetGazeControl() override;
  GazeFollowerInterface* GetGazeFollower() override;
  SituationManagerInterface* GetSituationManager() override;
  ActionManagerInterface* GetActionManager() override;
  ActionImplementationInterface* GetActionImplementation() override;
  AlgorithmSceneryCarInterface* GetAlgorithmSceneryCar() override;
  AuditoryPerceptionInterface* GetAuditoryPerception() override;
  InformationAcquisitionInterface* GetInformationAcquisition() override;
  FeatureExtractorInterface* GetFeatureExtractor() override;
  MentalCalculationsInterface* GetMentalCalculations() override;
  ScmDependenciesInterface* GetScmDependencies() override;
  IgnoringOuterLaneOvertakingProhibitionInterface* GetIgnoringOuterLaneOvertakingProhibition() override;
  SwervingInterface* GetSwerving() override;
  LongitudinalCalculationsInterface* GetLongitudinalCalculations() override;
  GazeControlComponentsInterface* GetGazeControlComponents() override;
  ZipMergingInterface* GetZipMerging() override;

private:
  //! @brief Receives informations from ScmDependencies
  ScmDependenciesInterface* _scmDependencies;

  //! @brief Receives informations from mentalModel
  std::unique_ptr<MentalModelInterface> _mentalModel{nullptr};

  //! @brief Receives informations from auditoryPerception
  std::unique_ptr<AuditoryPerceptionInterface> _auditoryPerception{nullptr};

  //! @brief Receives informations from gazeControl
  std::unique_ptr<GazeControlInterface> _gazeControl{nullptr};

  //! @brief Receives informations from gazeComponents
  std::unique_ptr<GazeControlComponentsInterface> _gazeComponents{nullptr};

  //! @brief Receives informations from gazeFollower
  std::unique_ptr<GazeFollowerInterface> _gazeFollower{nullptr};

  //! @brief Receives informations from informationAcquisition
  std::unique_ptr<InformationAcquisitionInterface> _informationAcquisition{nullptr};

  //! @brief Receives informations from situationManager
  std::unique_ptr<SituationManagerInterface> _situationManager{nullptr};

  //! @brief Receives informations from actionManager
  std::unique_ptr<ActionManagerInterface> _actionManager{nullptr};

  //! @brief Receives informations from actionImplementation
  std::unique_ptr<ActionImplementationInterface> _actionImplementation{nullptr};

  //! @brief Receives informations from algorithmSceneryCar
  std::unique_ptr<AlgorithmSceneryCarInterface> _algorithmSceneryCar{nullptr};

  //! @brief Receives informations from featureExtractor
  std::unique_ptr<FeatureExtractorInterface> _featureExtractor{nullptr};

  //! @brief Receives informations from mentalCalculations
  std::unique_ptr<MentalCalculationsInterface> _mentalCalculations{nullptr};

  //! @brief Receives informations from ignoringOuterLaneOvertakingProhibition
  std::unique_ptr<IgnoringOuterLaneOvertakingProhibitionInterface> _ignoringOuterLaneOvertakingProhibition{nullptr};

  //! @brief Receives informations from swerving
  std::unique_ptr<SwervingInterface> _swerving{nullptr};

  //! @brief Receives informations from longitudinalCalculations
  std::unique_ptr<LongitudinalCalculationsInterface> _longitudinalCalculations{nullptr};

  //! @brief Receives informations from vehicleQueryFactory
  std::unique_ptr<SurroundingVehicleQueryFactoryInterface> _vehicleQueryFactory{nullptr};

  //! @brief Receives informations from trajectoryPlanner
  std::unique_ptr<TrajectoryPlannerInterface> _trajectoryPlanner{nullptr};

  //! @brief Receives informations from zipMerging
  std::unique_ptr<ZipMergingInterface> _zipMerging{nullptr};
};
