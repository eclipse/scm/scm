/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "AlgorithmSceneryCar.h"

#include "MentalModelInterface.h"

AlgorithmSceneryCar::AlgorithmSceneryCar(MentalModelInterface& mentalModel)
    : _mentalModel{mentalModel}
{
}

void AlgorithmSceneryCar::ForcedLaneChange(ActionManagerInterface* actionManagerInterface,
                                           ActionImplementationInterface* actionImplementationInterface,
                                           LaneChangeAction laneChangeAction)
{
  // Check for a new lane change action
  if (laneChangeAction != _laneChangeActionLastTick)
  {
    switch (laneChangeAction)
    {
      // Set initial parameters for the lane change to be executed
      case LaneChangeAction::ForcePositive:  // forced lane change left
        _mentalModel.SetLateralActionAsForced(laneChangeAction);
        _mentalModel.SetLateralAction(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT));
        CorrectLaneChange(actionManagerInterface, actionImplementationInterface);
        _mentalModel.SetDirection(1);
        _resetReactionBaseTime = true;
        break;
      case LaneChangeAction::ForceNegative:  // forced lane change right
        _mentalModel.SetLateralActionAsForced(laneChangeAction);
        _mentalModel.SetLateralAction(LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT));
        CorrectLaneChange(actionManagerInterface, actionImplementationInterface);
        _mentalModel.SetDirection(-1);
        _resetReactionBaseTime = true;
        break;
      default:
        _resetReactionBaseTime = false;
        break;
    }
  }
  _laneChangeActionLastTick = laneChangeAction;
}

void AlgorithmSceneryCar::CorrectLaneChange(ActionManagerInterface* actionManagerInterface,
                                            ActionImplementationInterface* actionImplementation)
{
  // Calculate the necessary output variables for the lane change and overwrite the existing ones
  actionManagerInterface->RunLeadingVehicleSelector();
  actionManagerInterface->DetermineLongitudinalActionState();
  actionImplementation->UpdateInformationFromActionManager(actionManagerInterface);
  actionImplementation->ImplementAction();
  actionManagerInterface->DiscardLastActionPatternIntensities();
}

bool AlgorithmSceneryCar::ResetReactionBaseTime()
{
  bool reset = _resetReactionBaseTime;
  _resetReactionBaseTime = false;
  return reset;
}
