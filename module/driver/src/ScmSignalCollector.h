/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include <Stochastics/StochasticsInterface.h>

#include <memory>

#include "ScmDependencies.h"
#include "include/common/VehicleProperties.h"
#include "include/signal/sensorOutput.h"

/**
 * @brief The ScmSignalCollector class is responsible for collecting ParametersScmSignal, ParametersVehicleSignal and SensorOutput from other modules and storing them in scmDependencies,
 */
class ScmSignalCollector
{
public:
  ScmSignalCollector(StochasticsInterface* stochastics, units::time::millisecond_t cycleTime);

  void Update(const scm::signal::DriverInput& driverInput);
  ScmDependenciesInterface* GetDependencies();
  bool IsAlive();

private:
  StochasticsInterface* _stochastics;
  units::velocity::meters_per_second_t _spawnVelocity{};
  units::time::millisecond_t _cycleTime{};
  std::unique_ptr<ScmDependenciesInterface> _scmDependencies;
  std::optional<DriverParameters> _driverParameters;
  std::optional<TrafficRulesScm> _trafficRulesScm;
  std::optional<scm::common::vehicle::properties::EntityProperties> _vehicleModelParameters;
  std::optional<scm::signal::SensorOutput> _sensorOutputSignal;

  void TryCreate();
};
