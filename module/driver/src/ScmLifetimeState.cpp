/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "ScmLifetimeState.h"

#include <algorithm>
#include <cassert>
#include <string>

#include "ScmCommons.h"
#include "ScmDefinitions.h"
#include "Swerving/Swerving.h"
#include "include/common/LateralInformation.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"
#include "include/common/SensorDriverScmDefinitions.h"

using namespace units::literals;

ScmLifetimeState::ScmLifetimeState(const std::string& componentName,
                                   units::time::millisecond_t cycleTime,
                                   PeriodicLoggerInterface* pLog,
                                   ScmComponentsInterface& scmComponents,
                                   ExternalControlState externalControlState)
    : _cycleTime(cycleTime), _scmComponents(&scmComponents), _externalControlState(externalControlState), _componentName(componentName), _pLog(pLog)
{
}

scm::signal::DriverOutput ScmLifetimeState::GetOutput() const
{
  return {
      scm::LateralInformation{out_laneWidth, out_lateralDeviation, out_gainLateralDeviation, out_headingError, out_gainHeadingError, out_kappaManoeuvre, out_kappaRoad},
      out_triggerNewRouteRequest,
      SecondaryDriverTasks{out_indicatorState, out_hornSwitch, out_headLight, out_highBeamLight, out_flasher},
      out_longitudinal_acc};
}

void ScmLifetimeState::Trigger(units::time::millisecond_t time)
{
  if (_scmComponents->GetMentalModel()->GetAgentId() == 5)
  {
    volatile int debug_break{4};
  }
  SetInitialParameters(time);
  UpdateAgentInformation(time);
  DetermineAgentStates();
  ImplementAgentStates();
  SetFinalParameters();
  TriggerSimulationOutputLog();
}

void ScmLifetimeState::SetInitialParameters(units::time::millisecond_t time)
{
  _scmComponents->GetMentalModel()->SetTime(static_cast<units::time::millisecond_t>(time));
  _scmComponents->GetMentalModel()->AdvanceReactionBaseTime();
  _scmComponents->GetMentalModel()->AdvanceAdjustmentBaseTime();
  _scmComponents->GetMentalModel()->GetMicroscopicData()->UpdateOwnVehicleData()->triggerNewRouteRequest = false;

  LaneChangePrevention();
}

void ScmLifetimeState::UpdateAgentInformation(units::time::millisecond_t time)
{
  // Transition - Check for a lane change of the ego agent
  if (_isNewEgoLane)
  {
    _scmComponents->GetMentalModel()->Transition();
  }

  // Execute GazeFollower (externally definied gaze target time series, if applicable)
  GazeOverwrite gazeOverwrite = _scmComponents->GetGazeFollower()->EvaluateGazeOverwrite(static_cast<units::time::millisecond_t>(time));

  if (_scmComponents->GetGazeFollower()->GetThrowWarning())
  {
    const std::string msg = "Warning: Gaze target durations smaller than 250 ms may contradict the physiological mechanisms of visual perception (please only state time series points for the start of changes of gaze targets in the GazeFollower time series and keep the durations in mind, which are necessary to allocate the gaze direction).";
    //    _callbacks->Log(CbkLogLevel::Warning, __FILE__, __LINE__, msg);
  }
  _scmComponents->GetGazeControl()->AdvanceGazeState(gazeOverwrite);

  if (_scmComponents->GetGazeControl()->GetCurrentGazeState() != GazeState::SACCADE && _scmComponents->GetGazeControl()->IsNewInformationAcquisitionRequested())
  {
    _scmComponents->GetMentalModel()->UpdateReliabilityMap();
  }

  UpdateInfrastructureData();
  UpdateMesoscopicData();
  UpdateStimulusData();
  UpdateMicroscopicData();

  _scmComponents->GetMentalModel()->ResetInformationRequests();
}

void ScmLifetimeState::HandleExternalControl()
{
  if (_externalControlState.lateralActive)  // hard-code ActionStates when under externalControl
  {
    _scmComponents->GetMentalModel()->SetLateralAction(LateralAction(LateralAction::State::LANE_KEEPING));
    _scmComponents->GetActionManager()->GetLateralActionLastTick() = LateralAction(LateralAction::State::LANE_KEEPING);
  }

  if (_externalControlState.longitudinalActive)
  {
    _scmComponents->GetMentalModel()->SetLongitudinalActionState(LongitudinalActionState::SPEED_ADJUSTMENT);
    _scmComponents->GetActionManager()->SetActionSubStateLastTick(LongitudinalActionState::SPEED_ADJUSTMENT);
  }
}

void ScmLifetimeState::ResetBaseTimes()
{
  _scmComponents->GetMentalModel()->ResetReactionBaseTime();
  _scmComponents->GetMentalModel()->SetAdjustmentBaseTime(0._s);
}

void ScmLifetimeState::ResetMerges()
{
  _scmComponents->GetMentalModel()->ResetMergeRegulate();
  // When merge maneuver is reset, get a meaningful longitudinal action state and leading vehicle again
  _scmComponents->GetActionManager()->DetermineLongitudinalActionState();
  _scmComponents->GetActionManager()->RunLeadingVehicleSelector();
}

void ScmLifetimeState::CheckForEndOfLateralMovement()
{
  const LateralActionQuery action{_scmComponents->GetMentalModel()->GetLateralAction()};
  if (action.IsLaneChanging())
  {
    _scmComponents->GetMentalModel()->CheckForEndOfLaneChange(_scmComponents->GetActionImplementation()->GetDesiredLateralDisplacement(),
                                                              _scmComponents->GetMentalModel()->GetLateralVelocity(),
                                                              _scmComponents->GetActionImplementation()->GetLateralDisplacementReached());
  }
  else if (action.IsSwerving())
  {
    auto mentalModel = _scmComponents->GetMentalModel();
    auto swerving = _scmComponents->GetSwerving();

    swerving->UpdateAndCheckForEndOfSwerving(
        _scmComponents->GetActionImplementation()->GetDesiredLateralDisplacement(),
        mentalModel->GetLateralVelocity(),
        _cycleTime);

    if (swerving->IsPastSwervingEndTime())
    {
      const auto aoiToSwerve = swerving->GetAoiToSwerve().first;
      if (aoiToSwerve != AreaOfInterest::NumberOfAreaOfInterests)
      {
        try
        {
          const std::map<AreaOfInterest, AreaOfInterest> aoiMapping{
              {AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_REAR},
              {AreaOfInterest::LEFT_FRONT, AreaOfInterest::LEFT_REAR},
              {AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_REAR},
          };
          const auto aoi = aoiMapping.at(aoiToSwerve);
          mentalModel->AddInformationRequest(aoi,
                                             FieldOfViewAssignment::FOVEA,
                                             1.,
                                             InformationRequestTrigger::LATERAL_ACTION);
        }
        catch (const std::out_of_range&)
        {
          throw std::runtime_error("ScmLifetimeState::CheckForEndOfLateralMovement: Invalid AOI to swerve");
        }
      }
      swerving->SetSwervingEndTime(units::make_unit<units::time::millisecond_t>(std::numeric_limits<double>::max()));
    }
  }
  else
  {
    _scmComponents->GetMentalModel()->SetRemainInRealignState(false);
    _scmComponents->GetMentalModel()->SetRemainInLaneChangeState(false);
    _scmComponents->GetMentalModel()->SetRemainInSwervingState(false);
    _scmComponents->GetMentalModel()->ResetLaneChangeWidthTraveled();
    _scmComponents->GetMentalModel()->SetIsEndOfLateralMovement(false);  // reset to false before even starting a lateral movement
    _scmComponents->GetMentalModel()->ResetTransitionState();
  }
}

void ScmLifetimeState::ResetLateralMovement()
{
  _scmComponents->GetMentalModel()->SetRemainInRealignState(false);
  _scmComponents->GetMentalModel()->SetRemainInLaneChangeState(false);
  _scmComponents->GetMentalModel()->SetRemainInSwervingState(false);
  _scmComponents->GetMentalModel()->ResetLaneChangeWidthTraveled();
  _scmComponents->GetMentalModel()->SetIsEndOfLateralMovement(true);
  _scmComponents->GetMentalModel()->ResetTransitionState();
  _scmComponents->GetSwerving()->SetSwervingState(SwervingState::NONE);
  _scmComponents->GetSwerving()->ResetAgentIdOfSwervingTarget();
  ResetMerges();
  _scmComponents->GetMentalModel()->SetLateralAction(LateralAction());
}

void ScmLifetimeState::ImplementAgentStates()
{
  if (_externalControlState.lateralActive && _externalControlState.longitudinalActive)
  {
    _scmComponents->GetActionImplementation()->ImplementAction();
  }
  if (!_externalControlState.lateralActive && !_externalControlState.longitudinalActive)
  {
    // update the action manager with the newly determined longitudinal/lateral action state and aoi regulate
    _scmComponents->GetActionImplementation()->UpdateInformationFromActionManager(_scmComponents->GetActionManager());

    // Implement actions of the current situation
    _scmComponents->GetActionImplementation()->ImplementAction();
    _scmComponents->GetMentalModel()->CalculateAdjustmentBaseTime(_scmComponents->GetActionImplementation());  // this seems odd

    // Consider forced lane change
    // Check for a changed lane change action and execute manouver when it is a forced lane change left/right
    _scmComponents->GetAlgorithmSceneryCar()->ForcedLaneChange(_scmComponents->GetActionManager(),
                                                               _scmComponents->GetActionImplementation(),
                                                               _scmComponents->GetMentalModel()->GetMicroscopicData()->GetOwnVehicleInformation()->forcedLaneChangeStartTrigger);

    // Reset the reaction base time to zero when a forced lane change has been initiated
    if (_scmComponents->GetAlgorithmSceneryCar()->ResetReactionBaseTime())
    {
      _scmComponents->GetMentalModel()->ResetReactionBaseTime();
    }
  }
}

void ScmLifetimeState::SetFinalParameters()
{
  // Gather top-down requests from mental model
  std::map<AreaOfInterest, double> topDownRequestMap = _scmComponents->GetMentalModel()->GenerateTopDownAoiScoring(_scmComponents->GetGazeControl()->GetHafSignalProcessed());

  // Select gaze state intensities associated with the current action for the next time step
  _scmComponents->GetGazeControl()->UpdateGazeRequests(_scmComponents->GetMentalModel()->GetLateralAction(),
                                                       topDownRequestMap,
                                                       _opticAdasSignals,
                                                       _scmComponents->GetMentalModel()->GetCombinedAcousticOpticSignal(),
                                                       *_scmComponents->GetAuditoryPerception(),
                                                       _scmComponents->GetMentalModel()->GetDriverParameters().distractionPercentage.value());

  // Save values for next time step
  _scmComponents->GetSituationManager()->SetSituationLastTick(_scmComponents->GetMentalModel()->GetCurrentSituation());
  _scmComponents->GetActionManager()->SetLateralActionLastTick(_scmComponents->GetMentalModel()->GetLateralAction());
  _scmComponents->GetActionManager()->SetActionSubStateLastTick(_scmComponents->GetMentalModel()->GetLongitudinalActionState());

  // Write outputs
  out_indicatorState = _scmComponents->GetActionImplementation()->GetNextIndicatorState();
  out_flasher = _scmComponents->GetActionImplementation()->GetFlasherActiveNext();
  out_longitudinal_acc = _scmComponents->GetActionImplementation()->GetLongitudinalAccelerationWish();
  out_laneWidth = _scmComponents->GetMentalModel()->GetInfrastructureCharacteristics()->GetGeometryInformation()->Close().width;
  out_gainLateralDeviation = _scmComponents->GetActionImplementation()->GetLateralDeviationGain();
  out_lateralDeviation = _scmComponents->GetActionImplementation()->GetLateralDeviationNext();
  out_gainHeadingError = _scmComponents->GetActionImplementation()->GetHeadingErrorGain();
  out_headingError = _scmComponents->GetActionImplementation()->GetHeadingErrorNext();
  out_kappaManoeuvre = _scmComponents->GetActionImplementation()->GetKappaManoeuvre();
  out_kappaRoad = _scmComponents->GetActionImplementation()->GetKappaRoad();
  out_triggerNewRouteRequest = _scmComponents->GetMentalModel()->GetMicroscopicData()->GetOwnVehicleInformation()->triggerNewRouteRequest;

  if (_scmComponents->GetMentalModel()->GetCollisionState())
  {
    out_indicatorState = scm::LightState::Indicator::Warn;
  }
}

void ScmLifetimeState::TriggerSimulationOutputLog()
{
  if (_pLog)
  {
    SendDriverSimulationOutput(_pLog->GetLogger());
    SendScmPerceptionSimulationOutputInformation(_pLog->GetLogger());
  }
}

void ScmLifetimeState::LaneChangePrevention()
{
  const auto spawningTime = _scmComponents->GetScmDependencies()->GetSpawnTime();
  assert(spawningTime >= 0_ms);
  static constexpr units::time::millisecond_t defaultUrgentUpdateThreshold{1000._ms};
  const auto currentTime = _scmComponents->GetMentalModel()->GetTime();

  if (currentTime - spawningTime <= LANE_CHANGE_PREVENTION_THRESHOLD)
  {
    _scmComponents->GetMentalModel()->SetLaneChangePreventionAtSpawn(true);
    const auto ownVehicle = _scmComponents->GetScmDependencies()->GetOwnVehicleInformationScm();
    auto preventionZoneSize = ownVehicle->longitudinalVelocity * LANE_CHANGE_PREVENTION_THRESHOLD;  // Size of the velocity dependend lane change prevention zone [m]
    auto spawnPosition = ownVehicle->longitudinalPosition;
    auto currentUpdateThreshold = spawnPosition / preventionZoneSize * defaultUrgentUpdateThreshold;
    _scmComponents->GetMentalModel()->SetUrgentUpdateThreshold(units::math::min(currentUpdateThreshold, defaultUrgentUpdateThreshold));
  }
  else if (_scmComponents->GetMentalModel()->GetLaneChangePreventionAtSpawn())
  {
    _scmComponents->GetMentalModel()->SetLaneChangePreventionAtSpawn(false);
    _scmComponents->GetMentalModel()->SetUrgentUpdateThreshold(defaultUrgentUpdateThreshold);
  }

  if (currentTime - _externalControlState.lateralDeactivationTime <= LANE_CHANGE_PREVENTION_THRESHOLD &&
      _externalControlState.lateralDeactivationTime > 0_ms)  // If external control was deactivated shortly before or was never activated
  {
    _scmComponents->GetMentalModel()->SetLaneChangePreventionExternalControl(true);
  }
  else if (_scmComponents->GetMentalModel()->GetLaneChangePreventionExternalControl())
  {
    _scmComponents->GetMentalModel()->SetLaneChangePreventionExternalControl(false);
    _scmComponents->GetMentalModel()->SetUrgentUpdateThreshold(defaultUrgentUpdateThreshold);
  }
}

void ScmLifetimeState::UpdateInfrastructureData()
{
  _scmComponents->GetMentalModel()->GetInfrastructureCharacteristics()->SetTrafficRuleInformation(_scmComponents->GetScmDependencies()->GetTrafficRuleInformationScm());
  _scmComponents->GetMentalModel()->GetInfrastructureCharacteristics()->SetGeometryInformation(_scmComponents->GetScmDependencies()->GetGeometryInformationScm(),
                                                                                               _scmComponents->GetMentalModel()->GetAbsoluteVelocityEgo(true),
                                                                                               _scmComponents->GetScmDependencies()->GetOwnVehicleInformationScm()->takeNextExit,
                                                                                               _scmComponents->GetMentalModel()->GetLaneId());
}

void ScmLifetimeState::UpdateMesoscopicData()
{
  _scmComponents->GetMentalModel()->CalculateMicroscopicData(true);
}

void ScmLifetimeState::UpdateStimulusData()
{
  _scmComponents->GetAuditoryPerception()->UpdateStimulusData(_acousticAdasSignals);
}

void ScmLifetimeState::UpdateMicroscopicData()
{
  const auto rightHandTraffic = _scmComponents->GetScmDependencies()->GetTrafficRulesScm()->common.rightHandTraffic;
  const auto surroundingVehicles = _scmComponents->GetMentalModel()->GetMicroscopicData()->GetSurroundingVehicleInformation();
  const auto& objects{rightHandTraffic ? surroundingVehicles->Ahead().Left() : surroundingVehicles->Ahead().Right()};
  if (!objects.empty())
  {
    const auto objectSideId = objects.front().id;
    _scmComponents->GetIgnoringOuterLaneOvertakingProhibition()->Update(surroundingVehicles->Ahead().Close().front().id, objectSideId);
  }
}

std::string GetAoiAsStringNumber(AreaOfInterest aoi)
{
  return std::to_string(static_cast<int>(aoi));
}

void ScmLifetimeState::LogSurroundingVehicles(scm::publisher::PublisherInterface* simLog, std::vector<ObjectInformationScmExtended> vehicles, const std::string& direction)
{
  for (const auto& sideVehicle : vehicles)
  {
    LogSurroundingVehicles(simLog, sideVehicle, direction);
  }
}

void ScmLifetimeState::LogVehicles(scm::publisher::PublisherInterface* simLog, const std::vector<const SurroundingVehicleInterface*>& vehicles)
{
  for (const auto& vehicle : vehicles)
  {
    const std::string& aoi{GetAoiAsStringNumber(vehicle->GetAssignedAoi())};
    LogSurroundingVehicles(simLog, *vehicle->GetObjectInformation(), aoi);
    const std::string prefix = "Driver.Perception." + aoi + "#" + std::to_string(vehicle->GetId());
    simLog->Publish(prefix + ":reliability", vehicle->GetReliability());
  }
}

void ScmLifetimeState::LogSurroundingVehicles(scm::publisher::PublisherInterface* simLog, ObjectInformationScmExtended vehicle, const std::string& direction)
{
  const std::string prefix = "Driver.Perception." + direction + "#" + std::to_string(vehicle.id);
  simLog->Publish(prefix + ":acceleration", vehicle.acceleration.value());
  simLog->Publish(prefix + ":velocity", vehicle.longitudinalVelocity.value());
  simLog->Publish(prefix + ":gap", vehicle.gap.value());
  simLog->Publish(prefix + ":ttc", vehicle.ttc.value());
  simLog->Publish(prefix + ":heading", vehicle.heading);
  simLog->Publish(prefix + ":tauDot", vehicle.tauDot);
  simLog->Publish(prefix + ":ttcthreshold", vehicle.ttcthreshold.value());
  simLog->Publish(prefix + ":netDistance", vehicle.relativeLongitudinalDistance.value());
  simLog->Publish(prefix + ":lateralVelocity", vehicle.lateralVelocity.value());
  simLog->Publish(prefix + ":orientationRelativeToDriver", vehicle.orientationRelativeToDriver.frontCenter.value());
  simLog->Publish(prefix + ":relativeDistance", vehicle.longitudinalObstruction.mainLaneLocator.value());

  auto mentalModel = _scmComponents->GetMentalModel();
  auto egoX = mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()->posX.value();
  auto egoY = mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()->posY.value();

  std::vector<double> lonLatVector;
  if (vehicle.relativeX.value() == ScmDefinitions::DEFAULT_VALUE && vehicle.relativeY.value() == ScmDefinitions::DEFAULT_VALUE)
  {
    lonLatVector.assign({ScmDefinitions::DEFAULT_VALUE, ScmDefinitions::DEFAULT_VALUE});
  }
  else
  {
    lonLatVector.assign({egoX + vehicle.relativeX.value(), egoY + vehicle.relativeY.value()});
  }

  simLog->Publish(prefix + ":PosLonLat", lonLatVector);
}

void ScmLifetimeState::SendScmPerceptionSimulationOutputInformation(scm::publisher::PublisherInterface* simLog)
{
  LogVehicles(simLog, _scmComponents->GetMentalModel()->GetSurroundingVehicles());
}

void ScmLifetimeState::SendDriverSimulationOutput(scm::publisher::PublisherInterface* simLog)
{
  const std::string prefix = "Driver.";
  const auto mentalModel = _scmComponents->GetMentalModel();
  const auto mentalCalculations = _scmComponents->GetMentalCalculations();

  LateralAction action = mentalModel->GetLateralAction();
  std::string actionEnumsAsString = std::to_string(static_cast<int>(action.state)) + std::to_string(static_cast<int>(action.direction));
  simLog->Publish(prefix + "LateralActionState", std::stoi(actionEnumsAsString));
  simLog->Publish(prefix + "PreviewDistance", _scmComponents->GetScmDependencies()->GetDriverParameters().previewDistance.value());
  simLog->Publish(prefix + "LongitudinalActionState", static_cast<int>(mentalModel->GetLongitudinalActionState()));

  const Situation situation = mentalModel->GetCurrentSituation();
  std::string situationAsString = std::to_string(static_cast<int>(situation));
  if (situation == Situation::LANE_CHANGER_FROM_LEFT || situation == Situation::LANE_CHANGER_FROM_RIGHT)
  {
    const bool isAnticipated = mentalModel->IsSituationAnticipated(situation);
    const Risk risk = mentalModel->GetSituationRisk(situation);
    situationAsString.append(std::to_string(static_cast<int>(isAnticipated)) + std::to_string(static_cast<int>(risk)));
  }
  simLog->Publish(prefix + "CurrentSituation", situationAsString);
  const GazeState gazeState = _scmComponents->GetGazeControl()->GetCurrentGazeState();
  const int aoi = gazeState == GazeState::SACCADE ? -1 : static_cast<int>(mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()->fovea);
  simLog->Publish(prefix + "AOI", aoi);  // Note: Remove once visualisation can handle GazeState parameter
  simLog->Publish(prefix + "GazeState", static_cast<int>(gazeState));
  simLog->Publish(prefix + "MesoscopicSituation", ScmCommons::ToIntegerString(mentalModel->GetActiveMesoscopicSituations()));
  simLog->Publish(prefix + "SCMLeadingVehicle", mentalModel->GetLeadingVehicleId());
  const auto& frontObject{mentalModel->GetMicroscopicData()->GetSurroundingVehicleInformation()->Ahead().Close()};

  simLog->Publish(prefix + "AgentInFront", frontObject.empty() ? -1 : frontObject.front().id);

  for (const auto& [description, value] : mentalModel->GetDebugValues())
  {
    simLog->Publish(prefix + description, value);
  }

  simLog->Publish(prefix + "LateralOffset", std::to_string(mentalModel->GetMicroscopicData()->GetOwnVehicleInformation()->lateralOffsetNeutralPositionScaled.value()));
  simLog->Publish(prefix + "LateralPosition", mentalModel->GetLateralPosition().value());
  simLog->Publish(prefix + "LateralPositionFrontAxle", mentalModel->GetLateralPositionFrontAxle().value());
  simLog->Publish(prefix + "isDecisionPossible", mentalModel->GetReactionBaseTime() <= 0_s);
  simLog->Publish(prefix + "ReactionBaseTime", mentalModel->GetReactionBaseTime().value());
  simLog->Publish(prefix + "LaneType", static_cast<int>(mentalModel->GetInfrastructureCharacteristics()->GetLaneInformationGeometry(RelativeLane::EGO).laneType));
  simLog->Publish(prefix + "LaneId", _scmComponents->GetMentalModel()->GetLaneId());
  simLog->Publish(prefix + "EgoLaneWidth", _scmComponents->GetMentalModel()->GetInfrastructureCharacteristics()->GetLaneInformationGeometry(RelativeLane::EGO).width.value());

  simLog->Publish(prefix + "DistanceToLeftEOL", mentalModel->GetDistanceToEndOfLane(1).value());
  simLog->Publish(prefix + "DistanceToEgoEOL", mentalModel->GetDistanceToEndOfLane(0).value());
  simLog->Publish(prefix + "DistanceToRightEOL", mentalModel->GetDistanceToEndOfLane(-1).value());

  const auto& actionImpl = _scmComponents->GetActionImplementation();
  simLog->Publish(prefix + "LateralDisplacementTraveled", actionImpl->GetLateralDisplacementTraveled().value());
  simLog->Publish(prefix + "DesiredLateralDisplacement", actionImpl->GetDesiredLateralDisplacement().value());
  simLog->Publish(prefix + "LateralDisplacementReached", actionImpl->GetLateralDisplacementReached());
  const auto laneChangeDimensions{mentalModel->GetPlannedLaneChangeDimensions()};
  simLog->Publish(prefix + "LaneChangeLength", laneChangeDimensions.has_value() ? laneChangeDimensions.value().length.value() : -1);

  const auto mergeGap = mentalModel->GetMergeGap();
  simLog->Publish(prefix + "GapMergeRegulate", ScmCommons::AreaOfInterestToString(mentalModel->GetMergeRegulate()));
  simLog->Publish(prefix + "GapMergeRegulateId", mentalModel->GetMergeRegulateId());
  simLog->Publish(prefix + "GapApproachingV", mergeGap.approachingVelocity.value());
  simLog->Publish(prefix + "GapFullMergeSpace", mergeGap.fullMergeSpace.value());
  simLog->Publish(prefix + "GapDistanceToEndOfLane", mentalModel->GetDistanceToEndOfLane(0).value());
  simLog->Publish(prefix + "GapVelocity", mergeGap.velocity.value());
  simLog->Publish(prefix + "GapDistance", mergeGap.distance.value());
  simLog->Publish(prefix + "GapSafetyClearanceToLeader", mergeGap.safetyClearanceToLeader.value());
  simLog->Publish(prefix + "GapSafetyClearanceToFollower", mergeGap.safetyClearanceToFollower.value());
  simLog->Publish(prefix + "GapPosition", mergeGap.position == Merge::GapPosition::inFront ? "GapInFront" : "GapBehind");
  simLog->Publish(prefix + "GapLeaderId", mergeGap.leaderId);
  simLog->Publish(prefix + "GapFollowerId", mergeGap.followerId);
  simLog->Publish(prefix + "isZipMerge", mentalModel->GetZipMerge());
  simLog->Publish(prefix + "reevaluateLongitudinalRegulation", mentalModel->GetAdjustmentBaseTime() <= 0_s);

  simLog->Publish(prefix + "LaneConvenienceLeft", mentalCalculations->GetLaneChangeBehavior().GetLaneConvenience(SurroundingLane::LEFT));
  simLog->Publish(prefix + "LaneConvenienceRight", mentalCalculations->GetLaneChangeBehavior().GetLaneConvenience(SurroundingLane::RIGHT));
  simLog->Publish(prefix + "LaneConvenienceEgo", mentalCalculations->GetLaneChangeBehavior().GetLaneConvenience(SurroundingLane::EGO));
  simLog->Publish(prefix + "LateralVelocity", mentalModel->GetLateralVelocity().value());
  simLog->Publish(prefix + "LateralVelocityFrontAxle", mentalModel->GetLateralVelocityFrontAxle().value());
  simLog->Publish(prefix + "UFOV", ScmCommons::AoisToString(mentalModel->GetUfov()));
  simLog->Publish(prefix + "InconsistentAois", ScmCommons::AoisToString(mentalModel->GetInconsistentAois()));
  simLog->Publish(prefix + "GazeAngle", mentalModel->GetHorizontalGazeAngle().value());
  simLog->Publish(prefix + "VelocityMeanLeft", mentalModel->GetMeanVelocity(SurroundingLane::LEFT).value());
  simLog->Publish(prefix + "VelocityMeanRight", mentalModel->GetMeanVelocity(SurroundingLane::RIGHT).value());
  simLog->Publish(prefix + "VelocityMeanEgo", mentalModel->GetMeanVelocity(SurroundingLane::EGO).value());

  simLog->Publish(prefix + "Reliability FRONT", mentalModel->GetVisualReliability(VisualPerceptionSector::FRONT));
  simLog->Publish(prefix + "Reliability REAR", mentalModel->GetVisualReliability(VisualPerceptionSector::REAR));
  simLog->Publish(prefix + "Reliability LEFT", mentalModel->GetVisualReliability(VisualPerceptionSector::LEFT));
  simLog->Publish(prefix + "Reliability RIGHT", mentalModel->GetVisualReliability(VisualPerceptionSector::RIGHT));

  simLog->Publish(prefix + "AccelerationWish", _scmComponents->GetActionImplementation()->GetLongitudinalAccelerationWish().value());

  std::string regulateIds;
  for (const auto& agentId : mentalModel->GetPossibleRegulateIds())
  {
    regulateIds += std::to_string(agentId) + ";";
  }
  simLog->Publish(prefix + "PossibleRegulateVehicles", regulateIds);

  if (mentalModel->IsFrontSituation())
  {
    AreaOfInterest causingAoi = mentalModel->GetCausingVehicleOfSituationFrontCluster();
    simLog->Publish(prefix + "TTC", mentalModel->GetTtc(causingAoi).value());
  }
  else
  {
    simLog->Publish(prefix + "TTC", mentalModel->GetTtc(AreaOfInterest::EGO_FRONT).value());
  }

  std::string velocityLegalEgo{};
  if (!units::math::isinf(mentalModel->GetVelocityLegalEgo()))
  {
    velocityLegalEgo = std::to_string(mentalModel->GetVelocityLegalEgo().value());
  }

  simLog->Publish(prefix + "velocityLegalEgo", velocityLegalEgo);
  simLog->Publish(prefix + "velocityTargetEgo", mentalModel->GetAbsoluteVelocityTargeted().value());

  simLog->Publish(prefix + "vehicleVisibleEgoFront", mentalModel->GetIsVehicleVisible(AreaOfInterest::EGO_FRONT));
  simLog->Publish(prefix + "vehicleVisibleEgoFrontFar", mentalModel->GetIsVehicleVisible(AreaOfInterest::EGO_FRONT_FAR));
  simLog->Publish(prefix + "vehicleVisibleEgoRear", mentalModel->GetIsVehicleVisible(AreaOfInterest::EGO_REAR));

  simLog->Publish(prefix + "vehicleVisibleLeftRear", mentalModel->GetIsVehicleVisible(AreaOfInterest::LEFT_REAR));
  simLog->Publish(prefix + "vehicleVisibleLeftSide", mentalModel->GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE));
  simLog->Publish(prefix + "vehicleVisibleLeftFront", mentalModel->GetIsVehicleVisible(AreaOfInterest::LEFT_FRONT));
  simLog->Publish(prefix + "vehicleVisibleLeftFrontFar", mentalModel->GetIsVehicleVisible(AreaOfInterest::LEFT_FRONT_FAR));
  simLog->Publish(prefix + "vehicleVisibleLeftLeftRear", mentalModel->GetIsVehicleVisible(AreaOfInterest::LEFTLEFT_REAR));
  simLog->Publish(prefix + "vehicleVisibleLeftLeftSide", mentalModel->GetIsVehicleVisible(AreaOfInterest::LEFTLEFT_SIDE));
  simLog->Publish(prefix + "vehicleVisibleLeftLeftFront", mentalModel->GetIsVehicleVisible(AreaOfInterest::LEFTLEFT_FRONT));

  simLog->Publish(prefix + "vehicleVisibleRightRear", mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHT_REAR));
  simLog->Publish(prefix + "vehicleVisibleRightSide", mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE));
  simLog->Publish(prefix + "vehicleVisibleRightFront", mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHT_FRONT));
  simLog->Publish(prefix + "vehicleVisibleRightFrontFar", mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHT_FRONT_FAR));
  simLog->Publish(prefix + "vehicleVisibleRightRightRear", mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHTRIGHT_REAR));
  simLog->Publish(prefix + "vehicleVisibleRightRightSide", mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHTRIGHT_SIDE));
  simLog->Publish(prefix + "vehicleVisibleRightRightFront", mentalModel->GetIsVehicleVisible(AreaOfInterest::RIGHTRIGHT_FRONT));
}
