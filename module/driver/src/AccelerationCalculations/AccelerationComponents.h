/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file AccelerationComponents.h
#pragma once
#include "AccelerationAdjustmentParameters.h"
#include "AccelerationCalculationsQueryInterface.h"
#include "AccelerationComponentsInterface.h"

//! @brief Implements the AccelerationComponentsInterface
class AccelerationComponents : public AccelerationComponentsInterface
{
public:
  AccelerationComponents(const AccelerationCalculationsQueryInterface& query, const AccelerationAdjustmentParameters& parameters, const SurroundingVehicleInterface& vehicle);

  units::acceleration::meters_per_second_squared_t CalculateAccelerationFromDistance() const override;
  units::acceleration::meters_per_second_squared_t CalculateAccelerationFromVelocity() const override;
  units::acceleration::meters_per_second_squared_t CalculateAccelerationFromAcceleration() const override;

private:
  const AccelerationCalculationsQueryInterface& _query;
  const SurroundingVehicleInterface& _vehicle;
  const AccelerationAdjustmentParameters& _parameters;
};