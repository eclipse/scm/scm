/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "AccelerationAdjustmentParameters.h"

#include "AccelerationCalculationsQueryInterface.h"
#include "include/common/ScmDefinitions.h"

AccelerationAdjustmentParameters AccelerationParameters::CalculateAccelerationAdjustmentParameters(const AccelerationCalculationsQueryInterface& query, const SurroundingVehicleInterface& vehicle, units::acceleration::meters_per_second_squared_t maxDeceleration, int mergeRegulateId, units::length::meter_t deltaDistanceToAdjustment)
{
  const auto desiredDistance{CalculateDesiredDistance(query, vehicle, mergeRegulateId) + deltaDistanceToAdjustment};
  const auto regulationWindow{CalculateRegulationWindowSize(query, vehicle, desiredDistance, mergeRegulateId)};
  const double approachingFactor{CalculateApproachingFactor(vehicle, desiredDistance, regulationWindow)};

  return {desiredDistance, regulationWindow, approachingFactor, maxDeceleration};
}

units::length::meter_t AccelerationParameters::CalculateDesiredDistance(const AccelerationCalculationsQueryInterface& query, const SurroundingVehicleInterface& vehicle, int mergeRegulateId)
{
  if (query.GetMentalModel().GetLongitudinalActionState() == LongitudinalActionState::ACTIVE_ZIP_MERGING || query.GetMentalModel().GetLongitudinalActionState() == LongitudinalActionState::PASSIVE_ZIP_MERGING)
  {
    return query.GetMinDistance(vehicle);
  }
  if (query.GetMergeGap().IsValid())
  {
    return query.GetMinDistanceDuringMerge(vehicle);
  }

  if (query.IsEgoBelowJamSpeed() &&
      query.IsDetectedLaneChangerLeadingVehicle(vehicle) &&
      query.IsEgoCooperative())
  {
    return query.GetEqDistance(vehicle) * 1.2;
  }

  return query.GetEqDistance(vehicle);
}

units::length::meter_t AccelerationParameters::CalculateRegulationWindowSize(const AccelerationCalculationsQueryInterface& query, const SurroundingVehicleInterface& vehicle, units::length::meter_t desiredDistance, int mergeRegulateId)
{
  if (query.IsEgoNearStandstill())
  {
    return query.GetCarQueuingDistance();
  }

  if (vehicle.RearDistance().GetValue() > desiredDistance)
  {
    return units::math::max(1.0_m, query.GetEgoVelocity() * (1_s + query.GetReactionBaseTimeMean() + query.GetPedalChangeTimeMean()));
  }

  if (vehicle.GetId() == mergeRegulateId)
  {
    return units::math::max(1.0_m, desiredDistance - query.GetCarQueuingDistance() * query.GetUrgency());
  }

  return units::math::max(1.0_m, desiredDistance - query.GetMinDistance(vehicle));
}

double AccelerationParameters::CalculateApproachingFactor(const SurroundingVehicleInterface& vehicle, units::length::meter_t desiredDistance, units::length::meter_t regulationWindowSize)
{
  const auto vehicleDistance{vehicle.RearDistance().GetValue()};
  if (vehicleDistance <= desiredDistance)
  {
    return 1.0;
  }

  const auto distanceDelta{vehicleDistance - desiredDistance};
  const auto distanceRegulationRatio{distanceDelta / regulationWindowSize};

  return std::clamp(1.0 - distanceRegulationRatio.value(), 0.0, 1.0);
}