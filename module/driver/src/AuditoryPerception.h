/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  AuditoryPerception.h

#pragma once

#include <Stochastics/StochasticsInterface.h>

#include "AuditoryPerceptionInterface.h"
#include "include/ScmSampler.h"

class MentalModel;

//! @brief modells the driver's perception of acustic signals.
//! The auditory perception models the driver's hearing abilities. It detects ADAS warnings and information from
//! inside the driver's own vehicle and also sounds from other surrounding vehicles (e.g. horns and sirens).
//! After processing these sound information, the auditory perception affects the gaze behaviour of the driver
//! in the sub-module GazeControl (bottom-up gaze behaviour) and the information from the detected sound signals
class AuditoryPerception : public AuditoryPerceptionInterface
{
public:
  //! @brief Constructor.
  //! @param cycleTime               Cycle time of this components trigger task [ms].
  //! @param stochastics             Provides access to the stochastics functionality of the framework.
  AuditoryPerception(units::time::millisecond_t cycleTime, StochasticsInterface* stochastics);
  AuditoryPerception(const AuditoryPerception&) = delete;
  AuditoryPerception(AuditoryPerception&&) = delete;
  AuditoryPerception& operator=(const AuditoryPerception&) = delete;
  AuditoryPerception& operator=(AuditoryPerception&&) = delete;
  virtual ~AuditoryPerception() = default;

  bool IsAcousticStimulusDetected(std::vector<AdasHmiSignal> acousticAdasSignals) const override;
  void UpdateStimulusData(std::vector<AdasHmiSignal> acousticAdasSignals) override;
  bool IsAcousticStimulusProcessed() const override;
  AcousticStimulusDirection GetAcousticStimulusDirection() const override;

protected:
  //! @brief Dircetion of the (choosen; if multiple) acoustic stimulus (Definition according to clock face:
  //! FRONT = 0, RIGHT = 1, REAR = 2, LEFT = 3, DEFAULT = -1
  AcousticStimulusDirection _acousticStimulusDirection = DEFAULT;

  //! @brief Duration of an acoustic signal from the current agent's own vehicle since activation in [ms].
  units::time::millisecond_t _durationOwnVehicleSignalActivity{};

private:
  //! @brief Translation of the spot of presentation of a warning signal towards an orientation related to the driver expressed in clock face format
  //! @param spotOfPresentation          Spot of presentation as communicated from the ADAS component
  //! @return acoustic stimulus direction
  AcousticStimulusDirection TranslateSpotOfPresentationToDirection(const std::string& spotOfPresentation);

  //! @brief Cycletime of the simulation in [ms].
  units::time::millisecond_t _cycleTime{};

  //! @brief Interface object for stochastic calculations.
  StochasticsInterface* _stochastics = nullptr;

  //! @brief Human processing time for auditory signals in [ms].
  units::time::millisecond_t _auditoryProcessingTime{meanAuditoryProcessingTime};

  //! @brief Mean value of the human processing time for auditory signals in [ms].
  static constexpr units::time::millisecond_t meanAuditoryProcessingTime{242_ms};

  //! @brief Standard deviation of the human processing time for auditory signals in [ms].
  static constexpr units::time::millisecond_t sigmaAuditoryProcessingTime{88_ms};

  //! @brief Flag to indicate that an acoustic siganl was perceived and processed by the driver
  bool _acousticStimulusProcessed = false;
};