/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file LongitudinalCalculations.h

#pragma once

#include <memory>

#include "../../parameterParser/src/Signals/ParametersScmDefinitions.h"
#include "LaneQueryHelper.h"
#include "LongitudinalCalculationsInterface.h"
#include "LongitudinalPartCalculationsInterface.h"
#include "MentalModelInterface.h"
#include "ScmDefinitions.h"

//! @brief Implements the LongitudinalCalculationsInterface
class LongitudinalCalculations : public LongitudinalCalculationsInterface
{
public:
  explicit LongitudinalCalculations(const MentalModelInterface& mentalModel);

  units::length::meter_t CalculateSecureNetDistance(const SurroundingVehicleInterface& observedVehicle,
                                                    const SurroundingVehicleInterface& referenceVehicle,
                                                    units::acceleration::meters_per_second_squared_t anticipAccFront,
                                                    units::velocity::meters_per_second_t anticipVelEnd,
                                                    units::time::second_t reactionTime,
                                                    units::acceleration::meters_per_second_squared_t anticipAccRear,
                                                    units::length::meter_t deltaDistEnd,
                                                    units::velocity::meters_per_second_t vGap) const override;

  units::length::meter_t CalculateSecureNetDistance(const SurroundingVehicleInterface& observedVehicle,
                                                    const OwnVehicleInformationScmExtended& egoVehicle,
                                                    units::acceleration::meters_per_second_squared_t anticipAccFront,
                                                    units::velocity::meters_per_second_t anticipVelEnd,
                                                    units::time::second_t reactionTime,
                                                    units::acceleration::meters_per_second_squared_t anticipAccRear,
                                                    units::length::meter_t deltaDistEnd,
                                                    MinThwPerspective perspective,
                                                    units::velocity::meters_per_second_t vGap = units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE)) const override;

  units::velocity::meters_per_second_t CalculateVelocityDelta(const SurroundingVehicleInterface& observedVehicle,
                                                              const OwnVehicleInformationScmExtended& egoVehicle) const override;

private:
  const MentalModelInterface& _mentalModel;
  std::unique_ptr<LongitudinalPartCalculationsInterface> _longitudinalPartInterface;
};
