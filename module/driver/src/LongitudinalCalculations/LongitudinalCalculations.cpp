/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "LongitudinalCalculations.h"

#include "LongitudinalCalculations/LongitudinalPartCalculations.h"
#include "MentalModelInterface.h"

//! \brief Sanity check that two surrounding vehicles are not the same by checking their ID
//! \throws std::runtime_error if both surrounding vehicles should have the same id
static void ValidateInput(const SurroundingVehicleInterface& observedVehicle,
                          const SurroundingVehicleInterface& referenceVehicle)
{
  if (observedVehicle.GetId() == referenceVehicle.GetId())
    throw std::runtime_error("LongitudinalCalculations::ValidateInput: Observed and reference vehicle can't the same!");
}

LongitudinalCalculations::LongitudinalCalculations(const MentalModelInterface& mentalModel)
    : _mentalModel(mentalModel)
{
  _longitudinalPartInterface = std::make_unique<LongitudinalPartCalculations>(_mentalModel);
}

units::length::meter_t LongitudinalCalculations::CalculateSecureNetDistance(const SurroundingVehicleInterface& observedVehicle,
                                                                            const SurroundingVehicleInterface& referenceVehicle,
                                                                            units::acceleration::meters_per_second_squared_t anticipAccFront,
                                                                            units::velocity::meters_per_second_t anticipVelEnd,
                                                                            units::time::second_t reactionTime,
                                                                            units::acceleration::meters_per_second_squared_t anticipAccRear,
                                                                            units::length::meter_t deltaDistEnd,
                                                                            units::velocity::meters_per_second_t vGap) const
{
  ValidateInput(observedVehicle, referenceVehicle);

  auto sdd = _longitudinalPartInterface->DetermineSecureDistanceData(observedVehicle, referenceVehicle, vGap);

  // leading vehicle - distance to break
  auto distFront = _longitudinalPartInterface->DistanceToBrakeLeadingVehicle(anticipVelEnd, anticipAccFront, sdd.velocityFront);

  // following vehicle - distance to break
  auto distRear = _longitudinalPartInterface->DistanceToBrakeFollowingVehicle(anticipVelEnd, anticipAccRear, sdd.accelerationRear, sdd.velocityRear, reactionTime);
  const auto gapDistance{distRear - distFront};

  auto insecurityDistance = _longitudinalPartInterface->CalculateInsecurityDistance(observedVehicle, referenceVehicle);

  // lookup current relevant properties depending on TTC to neglect stevens's power law in case of high criticality
  double scalingFactorFollowingDistance = _longitudinalPartInterface->DetermineScalingFactorForFollowingDistance(observedVehicle, referenceVehicle);

  // calculate base secureDeltaDist
  const auto secureDeltaDist{std::max(deltaDistEnd, gapDistance + deltaDistEnd + insecurityDistance)};
  const auto scaledSecureDeltaDist = _longitudinalPartInterface->ExecuteStevensPowLaw(secureDeltaDist, scalingFactorFollowingDistance, _mentalModel.GetDriverParameters().proportionalityFactorForFollowingDistance);

  return scaledSecureDeltaDist;
}

units::length::meter_t LongitudinalCalculations::CalculateSecureNetDistance(const SurroundingVehicleInterface& observedVehicle,
                                                                            const OwnVehicleInformationScmExtended& egoVehicle,
                                                                            units::acceleration::meters_per_second_squared_t anticipAccFront,
                                                                            units::velocity::meters_per_second_t anticipVelEnd,
                                                                            units::time::second_t reactionTime,
                                                                            units::acceleration::meters_per_second_squared_t anticipAccRear,
                                                                            units::length::meter_t deltaDistEnd,
                                                                            MinThwPerspective perspective,
                                                                            units::velocity::meters_per_second_t vGap) const
{
  auto sdd = _longitudinalPartInterface->DetermineSecureDistanceData(observedVehicle, egoVehicle, perspective, vGap);
  // leading vehicle - distance to break
  auto distFront = _longitudinalPartInterface->DistanceToBrakeLeadingVehicle(anticipVelEnd, anticipAccFront, sdd.velocityFront);

  // following vehicle - distance to break
  auto distRear = _longitudinalPartInterface->DistanceToBrakeFollowingVehicle(anticipVelEnd, anticipAccRear, sdd.accelerationRear, sdd.velocityRear, reactionTime);

  auto insecurityDistance = _longitudinalPartInterface->CalculateInsecurityDistance(observedVehicle);

  // lookup current relevant properties depending on TTC to neglect stevens's power law in case of high criticality
  double scalingFactorFollowingDistance = _longitudinalPartInterface->DetermineTtcBasedScalingFactorForFollowingDistance(observedVehicle.GetTtc().GetValue());

  const auto gapDistance{distRear - distFront};
  const auto secureDeltaDist{std::max(deltaDistEnd, gapDistance + deltaDistEnd + insecurityDistance)};
  const auto scaledSecureDeltaDist = _longitudinalPartInterface->ExecuteStevensPowLaw(secureDeltaDist, scalingFactorFollowingDistance, _mentalModel.GetDriverParameters().proportionalityFactorForFollowingDistance);

  return scaledSecureDeltaDist;
}

units::velocity::meters_per_second_t LongitudinalCalculations::CalculateVelocityDelta(const SurroundingVehicleInterface& observedVehicle,
                                                                                      const OwnVehicleInformationScmExtended& egoVehicle) const
{
  return _longitudinalPartInterface->CalculateVelocityDelta(observedVehicle, egoVehicle);
}