/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "PeriodicLoggerInterface.h"
#include "include/Logging/PublisherInterface.h"

class PeriodicLogger : public PeriodicLoggerInterface
{
public:
  explicit PeriodicLogger(scm::publisher::PublisherInterface* publisher);
  PeriodicLogger& operator=(const PeriodicLogger&) = delete;
  PeriodicLogger& operator=(PeriodicLogger&&) = delete;
  virtual ~PeriodicLogger() = default;

  scm::publisher::PublisherInterface* GetLogger() override;

private:
  scm::publisher::PublisherInterface* _publisher = nullptr;
};
