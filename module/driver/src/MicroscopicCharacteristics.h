/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <map>
#include <memory>
#include <variant>
#include <vector>

#include "MicroscopicCharacteristicsInterface.h"
#include "ScmDefinitions.h"
#include "include/common/ScmEnums.h"

/*! \addtogroupp MicroscopicCharacteristics
 * @{
 * \brief holds all internal data of the mental model, including information on vehicle's surrounding.
 * \details This class holds all internal data of the mental model, including information on vehicle's surrounding
 * it contains the informations of driver and vehicle which are based on components ParametersScm and ParametersVehicle
 * and estimates the current velocity of the veicle (longitudinal/lateral)according to the surrounding conditions
 * within the current time step of the simulation
 *
 */

//! \ingroup MicroscopicCharacteristics
class MicroscopicCharacteristics : public MicroscopicCharacteristicsInterface
{
public:
  MicroscopicCharacteristics();
  MicroscopicCharacteristics(const MicroscopicCharacteristics&) = delete;
  MicroscopicCharacteristics(MicroscopicCharacteristics&&) = delete;
  MicroscopicCharacteristics& operator=(const MicroscopicCharacteristics&) = delete;
  MicroscopicCharacteristics& operator=(MicroscopicCharacteristics&&) = delete;
  ~MicroscopicCharacteristics() override = default;

  void Initialize(units::velocity::meters_per_second_t initVEgo) override;

  OwnVehicleInformationScmExtended* UpdateOwnVehicleData() override;

  //! \brief Get a pointer that allows the manipulation of the stored microsocpic information concerning a surrounding vehicle
  //! \param aoi      Area of interest
  //! \param [in] sideIndex    Index of the object in the side aoi vector
  //! \return surroundingObjectInformation of the specified area of interest
  ObjectInformationScmExtended* UpdateObjectInformation(AreaOfInterest aoi, int sideIndex = -1) override;

  //! \brief Get a pointer that allows the manipulation of the stored microsocpic information concerning surrounding vehicles in side area of interest
  //! \param aoi      Area of interest
  //! \return Vector of surroundingObjectInformations of the specified area of interest
  std::vector<ObjectInformationScmExtended>* UpdateSideObjectsInformation(AreaOfInterest aoi) override;

  //! \brief Getter for the own vehicle information
  //! \return ownVehicleInformation
  const OwnVehicleInformationScmExtended* GetOwnVehicleInformation() const override;

  //! \brief Getter for the own vehicle information
  //! \return surroundingObjectInformation
  const SurroundingObjectsScmExtended* GetSurroundingVehicleInformation() const override;

  //! \brief Getter for object information in an area of interest
  //! \param [in] aoi  Area of interest
  //! \param [in] sideIndex    Index of the object in the side aoi vector
  //! \return ObjectInformationScmExtended
  const ObjectInformationScmExtended* GetObjectInformation(AreaOfInterest aoi, int sideIndex = -1) const override;

  std::vector<ObjectInformationScmExtended>* GetSideObjectVector(AreaOfInterest aoi) const override;
  std::vector<ObjectInformationScmExtended>* GetObjectVector(AreaOfInterest aoi) const override;

  //! \brief Getter for object information in a side area of inerest
  //! \param [in] side-aoi  Area of interest
  //! \return ObjectInformationScmExtended
  const std::vector<ObjectInformationScmExtended>* GetSideObjectsInformation(AreaOfInterest aoi) const override;

  //! \brief Replace the current mergeRegulate index pointing to the side object which is mergeRegulate (either FRONT or REAR)
  //! \param parameters
  void UpdateSideAoiMergeRegulateIndex(int index) override;

  //!  \brief Getter for the mergeRegulate index
  //! \param parameters
  int GetSideAoiMergeRegulateIndex() const override;

  //! \brief Replace the current aoiRegulate index pointing to the side object which is mergeRegulate (either FRONT or REAR)
  //! \param parameters
  void UpdateSideAoiAoiRegulateIndex(int index) override;

  //!  \brief Getter for the mergeRegulate index
  //! \param parameters
  int GetSideAoiAoiRegulateIndex() const override;
#ifdef TESTING_ENABLED
  friend class TestMicroscopicCharacteristics;
  friend class TestMicroscopicCharacteristics2;
#endif

protected:
  //! Information about the egos surrounding vehicles as perceived or extrapolated by the agent.
  std::unique_ptr<SurroundingObjectsScmExtended> _surroundingObjectInformation{std::make_unique<SurroundingObjectsScmExtended>()};

private:
  const std::string COMPONENTNAME{"MicroscopicCharacteristics"};

  //! \brief Map the surrounding object information to an object in an aoi.
  //! \param aoi  Area of interest
  //! \param [in] sideIndex    Index of the object in the side aoi vector
  //! \return ObjectInformationScmExtended
  ObjectInformationScmExtended* GetObjectFromAoi(AreaOfInterest aoi, int sideIndex = -1) const;

  //! \brief Map the surrounding object information to an object in a side aoi.
  //! \param aoi  Area of interest
  //! \return Vector ObjectInformationScmExtended
  std::vector<ObjectInformationScmExtended>* GetObjectsFromSideAoi(AreaOfInterest aoi) const;

  //! Flag indicating whether the class has been initialized
  bool isInit{false};

  //! Information about the ego vehicle as perceived or extrapolated by the agent.
  std::unique_ptr<OwnVehicleInformationScmExtended> ownVehicleInformation{std::make_unique<OwnVehicleInformationScmExtended>()};

  int mergeRegulateIndex{0};
  int aoiRegulateIndex{0};
};
/** @} */  // End of class