################################################################################
# Copyright (c) 2017-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

set(SCM_DRIVER_DIR ${CMAKE_CURRENT_LIST_DIR}/src)
set(SCM_PARAMETER_DIR ${CMAKE_CURRENT_LIST_DIR}/../parameterParser/src)

# Automated include of all SurroundingVehicles source files
file(GLOB SRC_SUR_VEH  CONFIGURE_DEPENDS FOLLOW_SYMLINKS false LIST_DIRECTORIES false
    "${SCM_DRIVER_DIR}/SurroundingVehicles/*.cpp")

file(GLOB HEADER_SUR_VEH  CONFIGURE_DEPENDS FOLLOW_SYMLINKS false LIST_DIRECTORIES false
    "${SCM_DRIVER_DIR}/SurroundingVehicles/*.h")

# Automated include of all AccelerationCalculations source files
file(GLOB SRC_ACCELERATION_CALC  CONFIGURE_DEPENDS FOLLOW_SYMLINKS false LIST_DIRECTORIES false
    "${SCM_DRIVER_DIR}/AccelerationCalculations/*.cpp")

file(GLOB HEADER_ACCELERATION_CALC  CONFIGURE_DEPENDS FOLLOW_SYMLINKS false LIST_DIRECTORIES false
    "${SCM_DRIVER_DIR}/AccelerationCalculations/*.h")

add_scm_target(
    NAME ScmDriver TYPE library LINKAGE static COMPONENT ${COMPONENT}

    HEADERS
    ${HEADER_SUR_VEH}
    ${HEADER_ACCELERATION_CALC}
    ${CMAKE_CURRENT_LIST_DIR}/include/Driver.h
    ${CMAKE_CURRENT_LIST_DIR}/include/DriverFactory.h
    ${ROOT_DIR}/include/common/ScmEnums.h
    ${ROOT_DIR}/dllExport/scmExport.h
    ${SCM_DRIVER_DIR}/ScmDriver.h
    ${SCM_DRIVER_DIR}/ActionImplementation.h
    ${SCM_DRIVER_DIR}/ActionManager.h
    ${SCM_DRIVER_DIR}/AlgorithmSceneryCar.h
    ${SCM_DRIVER_DIR}/AoiAssigner.h
    ${SCM_DRIVER_DIR}/AuditoryPerception.h
    ${SCM_DRIVER_DIR}/Extrapolation.h
    ${SCM_DRIVER_DIR}/ExtrapolationInterface.h
    ${SCM_DRIVER_DIR}/FeatureExtractor.h
    ${SCM_DRIVER_DIR}/GazeControl/GazeControl.h
    ${SCM_DRIVER_DIR}/GazeControl/GazeParameters.h
    ${SCM_DRIVER_DIR}/GazeControl/GazeFollower.h
    ${SCM_DRIVER_DIR}/GazeControl/GazeCone.h
    ${SCM_DRIVER_DIR}/GazeControl/TopDownRequest.h
    ${SCM_DRIVER_DIR}/GazeControl/BottomUpRequest.h
    ${SCM_DRIVER_DIR}/GazeControl/ImpulseRequest.h
    ${SCM_DRIVER_DIR}/GazeControl/StimulusRequest.h
    ${SCM_DRIVER_DIR}/GazeControl/OpticalInformation.h
    ${SCM_DRIVER_DIR}/GazeControl/CurrentGazeState.h
    ${SCM_DRIVER_DIR}/GazeControl/GazePeriphery.h
    ${SCM_DRIVER_DIR}/GazeControl/GazeFieldQuery.h
    ${SCM_DRIVER_DIR}/GazeControl/GazeUsefulFieldOfView.h
    ${SCM_DRIVER_DIR}/GazeControl/GazeFovea.h
    ${SCM_DRIVER_DIR}/GazeControl/GazeControlComponents.h
    ${SCM_DRIVER_DIR}/InformationAcquisition.h
    ${SCM_DRIVER_DIR}/InfrastructureCharacteristics.h
    ${SCM_DRIVER_DIR}/InfrastructureCharacteristicsInterface.h
    ${SCM_DRIVER_DIR}/LaneChangeBehavior.h
    ${SCM_DRIVER_DIR}/LaneChangeBehaviorInterface.h
    ${SCM_DRIVER_DIR}/LaneChangeBehaviorQuery.h
    ${SCM_DRIVER_DIR}/LaneChangeBehaviorQueryInterface.h
    ${SCM_DRIVER_DIR}/LaneQueryHelper.h
    ${SCM_DRIVER_DIR}/LateralAction.h
    ${SCM_DRIVER_DIR}/LateralActionQuery.h
    ${SCM_DRIVER_DIR}/MentalCalculations.h
    ${SCM_DRIVER_DIR}/MentalCalculationsInterface.h
    ${SCM_DRIVER_DIR}/MentalModel.h
    ${SCM_DRIVER_DIR}/MentalModelDefinitions.h
    ${SCM_DRIVER_DIR}/MentalModelInterface.h
    ${SCM_DRIVER_DIR}/Merging/Merging.h
    ${SCM_DRIVER_DIR}/Merging/ZipMerging.h
    ${SCM_DRIVER_DIR}/Merging/ZipMergeParameters.h
    ${SCM_DRIVER_DIR}/Merging/ZipMergingInterface.h
    ${SCM_DRIVER_DIR}/MicroscopicCharacteristics.h
    ${SCM_DRIVER_DIR}/MicroscopicCharacteristicsInterface.h
    ${SCM_DRIVER_DIR}/PeriodicLogger.h
    ${SCM_DRIVER_DIR}/Reliability.h
    ${SCM_DRIVER_DIR}/ScmCommons.h
    ${SCM_DRIVER_DIR}/ScmComponents.h
    ${SCM_DRIVER_DIR}/ScmDefinitions.h
    ${SCM_DRIVER_DIR}/ScmDependencies.h
    ${SCM_DRIVER_DIR}/ScmLifetimeState.h
    ${SCM_DRIVER_DIR}/LaneChangeDimension.h
    ${SCM_DRIVER_DIR}/LaneKeepingCalculations.h
    ${SCM_DRIVER_DIR}/LaneKeepingCalculationsInterface.h
    ${SCM_DRIVER_DIR}/LivingState.h
    ${SCM_DRIVER_DIR}/LongitudinalCalculations/LongitudinalCalculations.h
    ${SCM_DRIVER_DIR}/LongitudinalCalculations/LongitudinalCalculationsInterface.h
    ${SCM_DRIVER_DIR}/LongitudinalCalculations/LongitudinalPartCalculations.h
    ${SCM_DRIVER_DIR}/LongitudinalCalculations/LongitudinalPartCalculationsInterface.h
    ${SCM_DRIVER_DIR}/SpawningState.h
    ${SCM_DRIVER_DIR}/TrafficFlow/TrafficFlow.h
    ${SCM_DRIVER_DIR}/TrafficFlow/TrafficFlowInterface.h
    ${SCM_DRIVER_DIR}/TrafficFlow/LaneMeanVelocity.h
    ${SCM_DRIVER_DIR}/TrafficFlow/LaneMeanVelocityInterface.h
    ${SCM_DRIVER_DIR}/ScmSignalCollector.h
    ${SCM_DRIVER_DIR}/SituationManager.h
    ${SCM_DRIVER_DIR}/HighCognitive/HighCognitive.h
    ${SCM_DRIVER_DIR}/HighCognitive/AnticipatedLaneChangerModel.h
    ${SCM_DRIVER_DIR}/HighCognitive/ApproachFollowClassifier.h
    ${SCM_DRIVER_DIR}/HighCognitive/ApproachSaliencyClassifier.h
    ${SCM_DRIVER_DIR}/HighCognitive/FollowSaliencyClassifier.h
    ${SCM_DRIVER_DIR}/HighCognitive/LaneChangeTrajectories.h
    ${SCM_DRIVER_DIR}/HighCognitive/LaneChangerCachedData.h
    ${SCM_DRIVER_DIR}/OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibition.h
    ${SCM_DRIVER_DIR}/OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibitionInterface.h
    ${SCM_DRIVER_DIR}/Swerving/SwervingInterface.h
    ${SCM_DRIVER_DIR}/Swerving/Swerving.h
    ${SCM_PARAMETER_DIR}/TrafficRules/TrafficRules.h
    ${SCM_DRIVER_DIR}/TrajectoryCalculations/SteeringManeuver.h
    ${SCM_DRIVER_DIR}/TrajectoryCalculations/SteeringManeuverTypes.h
    ${SCM_DRIVER_DIR}/TrajectoryCalculations/TrajectoryCalculations.h
    ${SCM_DRIVER_DIR}/TrajectoryCalculations/TrajectoryCalculationTypes.h
    ${SCM_DRIVER_DIR}/LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculations.h
    ${SCM_DRIVER_DIR}/LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculationsInterface.h
    ${SCM_DRIVER_DIR}/LaneChangeTrajectoryCalculations/LaneChangeLengthCalculations.h
    ${SCM_DRIVER_DIR}/LaneChangeTrajectoryCalculations/LaneChangeLengthCalculationsInterface.h
    ${SCM_DRIVER_DIR}/DesiredVelocityCalculations.h
    ${SCM_DRIVER_DIR}/SideLaneSafety/SideLaneSafety.h
    ${SCM_DRIVER_DIR}/SideLaneSafety/SideLaneSafetyQuery.h
    ${SCM_DRIVER_DIR}/SideLaneSafety/SideLaneSafetyQueryInterface.h
    ${SCM_DRIVER_DIR}/SideLaneSafety/SideLaneSafetyTypes.h
    ${SCM_DRIVER_DIR}/SideLaneSafety/SideLaneSafetyInterface.h
    ${SCM_DRIVER_DIR}/SideLaneSafety/SideLaneSafetyCalculations.h
    ${SCM_DRIVER_DIR}/SideLaneSafety/SideLaneSafetyCalculationsInterface.h
    ${SCM_DRIVER_DIR}/TrajectoryPlanner/TrajectoryPlanner.h
    ${SCM_DRIVER_DIR}/TrajectoryPlanner/TrajectoryPlannerInterface.h
    ${SCM_DRIVER_DIR}/TrajectoryPlanner/TrajectoryPlanningQuery.h
    ${SCM_DRIVER_DIR}/TrajectoryPlanner/TrajectoryPlanningQueryInterface.h
    ${SCM_DRIVER_DIR}/TrajectoryPlanner/TrajectoryPlanningTypes.h
    ${ROOT_DIR}/include/common/CommonHelper.h
    ${ROOT_DIR}/include/common/VehicleProperties.h
    ${ROOT_DIR}/include/common/XmlParser.h
    ${ROOT_DIR}/include/ScmMathUtils.h
    ${ROOT_DIR}/include/ScmSampler.h
    ${ROOT_DIR}/src/Logging/Logging.h

    SOURCES
    ${SRC_SUR_VEH}
    ${SRC_ACCELERATION_CALC}
    ${CMAKE_CURRENT_LIST_DIR}/src/Driver.cpp
    ${SCM_DRIVER_DIR}/ScmDriver.cpp
    ${SCM_DRIVER_DIR}/ActionImplementation.cpp
    ${SCM_DRIVER_DIR}/ActionManager.cpp
    ${SCM_DRIVER_DIR}/AlgorithmSceneryCar.cpp
    ${SCM_DRIVER_DIR}/AoiAssigner.cpp
    ${SCM_DRIVER_DIR}/AuditoryPerception.cpp
    ${SCM_DRIVER_DIR}/Extrapolation.cpp
    ${SCM_DRIVER_DIR}/FeatureExtractor.cpp
    ${SCM_DRIVER_DIR}/GazeControl/GazeControl.cpp
    ${SCM_DRIVER_DIR}/GazeControl/GazeFollower.cpp
    ${SCM_DRIVER_DIR}/GazeControl/GazeCone.cpp
    ${SCM_DRIVER_DIR}/GazeControl/TopDownRequest.cpp
    ${SCM_DRIVER_DIR}/GazeControl/BottomUpRequest.cpp
    ${SCM_DRIVER_DIR}/GazeControl/ImpulseRequest.cpp
    ${SCM_DRIVER_DIR}/GazeControl/StimulusRequest.cpp
    ${SCM_DRIVER_DIR}/GazeControl/OpticalInformation.cpp
    ${SCM_DRIVER_DIR}/GazeControl/CurrentGazeState.cpp
    ${SCM_DRIVER_DIR}/GazeControl/GazePeriphery.cpp
    ${SCM_DRIVER_DIR}/GazeControl/GazeFieldQuery.cpp
    ${SCM_DRIVER_DIR}/GazeControl/GazeUsefulFieldOfView.cpp
    ${SCM_DRIVER_DIR}/GazeControl/GazeFovea.cpp
    ${SCM_DRIVER_DIR}/GazeControl/GazeControlComponents.cpp
    ${SCM_DRIVER_DIR}/InformationAcquisition.cpp
    ${SCM_DRIVER_DIR}/InfrastructureCharacteristics.cpp
    ${SCM_DRIVER_DIR}/LaneChangeBehavior.cpp
    ${SCM_DRIVER_DIR}/LaneChangeBehaviorQuery.cpp
    ${SCM_DRIVER_DIR}/LaneQueryHelper.cpp
    ${SCM_DRIVER_DIR}/MicroscopicCharacteristics.cpp
    ${SCM_DRIVER_DIR}/MentalCalculations.cpp
    ${SCM_DRIVER_DIR}/MentalModel.cpp
    ${SCM_DRIVER_DIR}/Merging/Merging.cpp
    ${SCM_DRIVER_DIR}/Merging/ZipMerging.cpp
    ${SCM_DRIVER_DIR}/PeriodicLogger.cpp
    ${SCM_DRIVER_DIR}/Reliability.cpp
    ${SCM_DRIVER_DIR}/ScmCommons.cpp
    ${SCM_DRIVER_DIR}/ScmComponents.cpp
    ${SCM_DRIVER_DIR}/ScmDependencies.cpp
    ${SCM_DRIVER_DIR}/ScmLifetimeState.cpp
    ${SCM_DRIVER_DIR}/LaneChangeDimension.cpp
    ${SCM_DRIVER_DIR}/LaneKeepingCalculations.cpp
    ${SCM_DRIVER_DIR}/LivingState.cpp
    ${SCM_DRIVER_DIR}/LongitudinalCalculations/LongitudinalCalculations.cpp
    ${SCM_DRIVER_DIR}/LongitudinalCalculations/LongitudinalPartCalculations.cpp
    ${SCM_DRIVER_DIR}/SpawningState.cpp
    ${SCM_DRIVER_DIR}/TrafficFlow/LaneMeanVelocity.cpp
    ${SCM_DRIVER_DIR}/TrafficFlow/TrafficFlow.cpp
    ${SCM_DRIVER_DIR}/ScmSignalCollector.cpp
    ${SCM_DRIVER_DIR}/SituationManager.cpp
    ${SCM_DRIVER_DIR}/HighCognitive/HighCognitive.cpp
    ${SCM_DRIVER_DIR}/HighCognitive/AnticipatedLaneChangerModel.cpp
    ${SCM_DRIVER_DIR}/HighCognitive/ApproachFollowClassifier.cpp
    ${SCM_DRIVER_DIR}/HighCognitive/ApproachSaliencyClassifier.cpp
    ${SCM_DRIVER_DIR}/HighCognitive/FollowSaliencyClassifier.cpp
    ${SCM_DRIVER_DIR}/HighCognitive/LaneChangeTrajectories.cpp
    ${SCM_DRIVER_DIR}/HighCognitive/LaneChangerCachedData.cpp
    ${SCM_DRIVER_DIR}/OuterLaneOvertaking/IgnoringOuterLaneOvertakingProhibition.cpp
    ${SCM_DRIVER_DIR}/Swerving/Swerving.cpp
    ${SCM_PARAMETER_DIR}/TrafficRules/TrafficRules.cpp
    ${SCM_DRIVER_DIR}/TrajectoryCalculations/SteeringManeuver.cpp
    ${SCM_DRIVER_DIR}/TrajectoryCalculations/TrajectoryCalculations.cpp
    ${SCM_DRIVER_DIR}/LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculations.cpp
    ${SCM_DRIVER_DIR}/LaneChangeTrajectoryCalculations/LaneChangeLengthCalculations.cpp
    ${SCM_DRIVER_DIR}/SideLaneSafety/SideLaneSafetyCalculations.cpp
    ${SCM_DRIVER_DIR}/SideLaneSafety/SideLaneSafety.cpp
    ${SCM_DRIVER_DIR}/SideLaneSafety/SideLaneSafetyQuery.cpp
    ${SCM_DRIVER_DIR}/TrajectoryPlanner/TrajectoryPlanner.cpp
    ${SCM_DRIVER_DIR}/TrajectoryPlanner/TrajectoryPlanningQuery.cpp
    ${ROOT_DIR}/include/common/XmlParser.cpp
    ${ROOT_DIR}/src/Logging/Logging.cpp

    INCDIRS
    ${ROOT_DIR}
    ${CMAKE_CURRENT_LIST_DIR}/include
    ${SCM_DRIVER_DIR}
    ${SCM_DRIVER_DIR}/HighCognitive
    ${SCM_DRIVER_DIR}/Swerving
    ${SCM_DRIVER_DIR}/Signals

    LIBRARIES
    Stochastics::Stochastics
    Iconv::Iconv
    LibXml2::LibXml2
    OsiQueryLibrary

    LINKOSI static
)
