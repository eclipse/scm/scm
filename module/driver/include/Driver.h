/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file Driver.h
#pragma once

#include <Stochastics/StochasticsInterface.h>
#include <units.h>

#include "include/Logging/PublisherInterface.h"

namespace scm::driver
{
class Interface;

//! @brief Creates an instance of a driver module
//! @note  The creator is responsible for destroying the instance
Interface* Create(StochasticsInterface* stochastics, units::time::millisecond_t cycleTime, scm::publisher::PublisherInterface* const scmPublisher);
}  // namespace scm::driver