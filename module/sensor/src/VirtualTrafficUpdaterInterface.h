/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <memory>

#include "SensorDriverScmCalculationsInterface.h"
#include "SensorTypes.h"
#include "SurroundingTrafficUpdaterInterface.h"
#include "include/common/SensorDriverScmDefinitions.h"

namespace scm
{
class VirtualTrafficUpdaterInterface
{
public:
  virtual ~VirtualTrafficUpdaterInterface() = default;
  virtual VirtualAgentsSCM GetVirtualAgentInformation(double previewDistance) = 0;
};
}  // namespace scm