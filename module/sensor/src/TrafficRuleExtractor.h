/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <OsiQueryLibrary/osiql.h>

#include <memory>
#include <optional>
#include <string>

#include "SensorTypes.h"
#include "TrafficRuleExtractorInterface.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"
namespace scm
{
class TrafficRuleExtractor : public TrafficRuleExtractorInterface
{
public:
  TrafficRuleExtractor(const scm::VehicleState& state, units::length::meter_t visibilityDistance)
      : _state{state},
        _visibilityDistance{visibilityDistance}
  {
  }
  const TrafficRuleInformationSCM& Update(units::length::meter_t previewDistance) override;

private:
  int _time = 0;
  const scm::VehicleState& _state;
  units::length::meter_t _visibilityDistance{};
  TrafficRuleInformationSCM _trafficRulesInformation;

  //! \brief Position to move a sign from back to front (at spawn)
  static constexpr units::length::meter_t DISTANCE_TO_VIRTUAL_SIGN{1._m};
  //! \brief Spawn flag. Set to false, when initialization is done.
  bool _spawn{true};

  std::vector<CommonTrafficSign::Entity> GetTrafficSignsOsi(const std::vector<std::pair<const osiql::TrafficSign*, osiql::Stream::Distance>>& trafficSigns) const;

  //! \brief Get traffic signs in range for spawn.
  //! Traffic signs behind ego get moved 1m in front of ego and their value or the value of the respective supplementary signs is adjusted.
  //! \param visibilityDistance How far to look for relevant signs
  //! \param relativeLaneId Lane to check for relevant signs
  //! \return Vector of relevant traffic signs.
  //  std::vector<CommonTrafficSign::Entity> GetTrafficSignsRelevantForSpawn(double visibilityDistance, int relativeLaneId) const;

  //! \brief Gets the relevant maximum speed limit signs for spawn.
  //! \param [in] trafficSigns Traffic signs in front of the agent.
  //! \param [in] trafficSignsBack Traffic signs behind the agent.
  //! \return Vector of MaxSpeedLimit signs with the closest sign behind the agent in first place and all other SpeedLimit signs in front of agent.
  std::vector<CommonTrafficSign::Entity> GetMaximumSpeedLimitForSpawn(const std::vector<CommonTrafficSign::Entity>& trafficSigns, const std::vector<CommonTrafficSign::Entity>& trafficSignsBack) const;

  //! \brief Get lane ending signs for spawn.
  //! If a lane ending sign is found in front of the agent, only the lane ending signs in front are returned. Otherwise the closest lane ending sign from the back is inserted 1m ahead and the supplementary sign value is adjusted.
  //! \throws Exception if laneEndingSign is not CommonTrafficSign::AnnounceLeftLaneEnd or CommonTrafficSign::AnnounceRightLaneEnd.
  //! \param [in] trafficSigns Traffic signs in front of the agent.
  //! \param [in] trafficSignsBack Traffic signs behind the agent.
  //! \param [in] laneEndingSing Sign that should be searched in front and behind agent. Should only be AnnounceLeftLaneEnd or AnnounceRightLaneEnd
  //! \return Vector of lane ending signs.
  std::vector<CommonTrafficSign::Entity> GetLaneEndingForSpawn(const std::vector<CommonTrafficSign::Entity>& trafficSigns, const std::vector<CommonTrafficSign::Entity>& trafficSignsBack, CommonTrafficSign::Type laneEndingSign) const;

  //! @brief Tries to move the given EndOfLane sign in front of the agent and adjust the supplementary sign value.
  //! @param rearEndOfLaneSign EndOfLaneSign that should be moved
  //! @return End of lane sign with adjusted relative distance and supplementary sign value or std::nullopt if the end of lane has already started.
  std::optional<CommonTrafficSign::Entity> MoveEndOfLaneSignToVirtualPosition(const CommonTrafficSign::Entity& rearEndOfLaneSign) const;

  //! @brief Get the HighwayExitPole signs for spawn.
  //! If exit poles are found in front of the agent only those are returned, otherwise the closest sign behind the ego is moved 1m in front of the ego and is also returned.
  //! @param trafficSigns Traffic signs in front of the agent.
  //! @param trafficSignsBack Traffic signs behind the agent.
  //! @return Vector of HighwayExitPole traffic signs
  std::vector<CommonTrafficSign::Entity> GetHighwayExitPolesForSpawn(const std::vector<CommonTrafficSign::Entity>& trafficSigns, const std::vector<CommonTrafficSign::Entity>& trafficSignsBack) const;
  //! @brief Finds the closest instance of the given traffic sign type behind the agent.
  //! @param trafficSignsBehindEgo Vector of traffic signs that are behind the agent.
  //! @param relevantType Traffic sign type that should be found.
  //! @return The closest found traffic sign or std::nullopt if no sign of the specified type was found behind the agent.
  std::optional<CommonTrafficSign::Entity> FindClosestSignBehindEgo(const std::vector<CommonTrafficSign::Entity>& trafficSignsBehindEgo, CommonTrafficSign::Type relevantType) const;

  //! \brief Get traffic rules sensor data from the requested lane.
  //! \param [in] relativeLaneId      Lane id relative to ego lane (left lane --> 1, ego lane --> 0, right lane --> -1)
  //! \return Traffic rules lane information of the requested lane
  LaneInformationTrafficRulesSCM GetTrafficRulesLaneInformationOsi(units::length::meter_t previewDistance, osiql::Side side, size_t laneOffset) const;

  std::vector<LaneMarking::Entity> GetLaneMarkings(units::length::meter_t range, osiql::Side, size_t laneOffset) const;

  std::vector<CommonTrafficSign::Entity> ExtractInformation(const std::vector<std::pair<const osiql::TrafficSign*, double>>&) const;
  std::vector<CommonTrafficSign::Entity> ExtractInformation(const std::vector<std::pair<const osiql::TrafficSign*, osiql::Stream::Distance>>&) const;
};
std::ostream& operator<<(std::ostream&, LaneMarking::Color);
std::ostream& operator<<(std::ostream&, LaneMarking::Type);
std::ostream& operator<<(std::ostream&, const LaneMarking::Entity&);
}  // namespace scm
