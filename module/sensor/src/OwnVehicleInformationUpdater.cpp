/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "OwnVehicleInformationUpdater.h"

#include <OsiQueryLibrary/osiql.h>

#include "include/common/ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "src/Logging/Logging.h"

namespace scm
{
/**
 * @brief Calculate the width left of the reference point of a leaning object
 *
 * @param width     width of the object
 * @param height    height of the object
 * @param roll      rolling angle
 * @return the width left of the reference point of a leaning object
 */
static units::length::meter_t GetWidthLeft(units::length::meter_t width,
                                           units::length::meter_t height,
                                           units::angle::radian_t roll)
{
  return 0.5 * width * units::math::cos(roll) + (roll < 0_rad ? height * units::math::sin(-roll) : 0_m);
}

/**
 * @brief Calculate the width right of the reference point of a leaning object
 *
 * @param width     width of the object
 * @param height    height of the object
 * @param roll      rolling angle
 * @return the width right of the reference point of a leaning object
 */
static units::length::meter_t GetWidthRight(units::length::meter_t width,
                                            units::length::meter_t height,
                                            units::angle::radian_t roll)
{
  return 0.5 * width * units::math::cos(roll) + (roll > 0_rad ? height * units::math::sin(roll) : 0_m);
}

ProjectedSpacialDimensions GetProjectedSpacialDimensions(units::angle::radian_t rollAngle, units::length::meter_t width, units::length::meter_t height)
{
  if (rollAngle.value() > std::fabs(M_PI / 2.))
  {
    std::string msg = "SensorDriverScmImplementation - GetProjectedSpacialDimensions: Roll angle above 90° not possible";
    Logging::Error(msg);
    throw std::runtime_error(msg);
  }

  const auto heightProjected{height * units::math::cos(rollAngle)};

  const auto widthLeft{GetWidthLeft(width, height, rollAngle)};
  const auto widthRight{GetWidthRight(width, height, rollAngle)};
  const auto widthProjected{widthLeft + widthRight};

  return ProjectedSpacialDimensions(heightProjected, widthProjected, widthLeft, widthRight);
}

const OwnVehicleInformationSCM& OwnVehicleInformationUpdater::Update(const common::vehicle::properties::EntityProperties& vehicleParameters, LaneChangeAction forcedLaneChangeAction)
{
  _ownVehicleInformation.id = static_cast<int>(_state.vehicle->GetId());

  {  // Front axle
     const auto point{_state.route.Localize(_state.vehicle->GetAxleXY<osiql::Axle::Front>())};
     _ownVehicleInformation.lateralPositionFrontAxle = units::make_unit<units::length::meter_t>(point.GetDistanceFromCenterline());
  }

  {  // Front bumper
    const auto longitude{units::make_unit<units::length::meter_t>(_state.GetPose().GetLane().GetCenterlineCoordinates(_state.GetPose()).distanceFromCenterline)};
    // s-coordinate
    _ownVehicleInformation.longitudinalPosition = units::make_unit<units::length::meter_t>(_state.GetPose().latitude);
    // t-coordinate
    _ownVehicleInformation.lateralPosition = units::make_unit<units::length::meter_t>(_state.GetPose().GetDistanceFromCenterline());

    // x-coordinate
    _ownVehicleInformation.posX = units::make_unit<units::length::meter_t>(_state.GetPose().GetXY().x);
    // y-coordinate
    _ownVehicleInformation.posY = units::make_unit<units::length::meter_t>(_state.GetPose().GetXY().y);

    // relative heading
    _ownVehicleInformation.heading = units::make_unit<units::angle::radian_t>(_state.GetPose().angle);

    _ownVehicleInformation.mainLaneId = _state.GetPose().GetLane().GetOpenDriveId();
  }
  {  // Velocity
    const double laneAngle{_state.GetPose().GetLane().GetAngle(_state.GetPose().latitude)};

    // Speed
    _ownVehicleInformation.absoluteVelocity = units::make_unit<units::velocity::meters_per_second_t>(_state.vehicle->GetVelocity().Length());

    // Longitudinal velocity(forwards)
    _ownVehicleInformation.longitudinalVelocity = units::make_unit<units::velocity::meters_per_second_t>(static_cast<osiql::Vector2d>(_state.vehicle->GetVelocity()).ProjectionLength(laneAngle));

    // Lateral velocity (sideways)
    _ownVehicleInformation.lateralVelocity = units::make_unit<units::velocity::meters_per_second_t>(static_cast<osiql::Vector2d>(_state.vehicle->GetVelocity()).ProjectionLength(laneAngle + M_PI_2));

    // Lateral tangential velocity
     const osiql::Vector2d frontAxleOffset{_state.vehicle->GetAxleOffset<osiql::Axle::Front>()};
     _ownVehicleInformation.lateralVelocityFrontAxle = units::make_unit<units::velocity::meters_per_second_t>(static_cast<osiql::Vector2d>(_state.vehicle->GetVelocity(frontAxleOffset)).ProjectionLength(laneAngle + M_PI_2));
  }

  {  // Longitudinal acceleration
     const auto longitudinalAcceleration{units::make_unit<units::acceleration::meters_per_second_squared_t>(static_cast<osiql::Vector2d>(_state.vehicle->GetAcceleration()).ProjectionLength(_state.vehicle->GetYaw()))};
     _ownVehicleInformation.acceleration = longitudinalAcceleration;
  }

  {
    _ownVehicleInformation.projectedDimensions = GetProjectedSpacialDimensions(units::make_unit<units::angle::radian_t>(_state.vehicle->GetRoll()), units::make_unit<units::length::meter_t>(_state.vehicle->GetWidth()), units::make_unit<units::length::meter_t>(_state.vehicle->GetHeight()));
  }

  {  // Distance to boundary
    const auto [leftDistance, rightDistance]{_state.vehicle->GetApproximateBoundsDistancesToBoundaries(_state.GetPose().GetLane())};
    _ownVehicleInformation.distanceToLaneBoundaryLeft = units::make_unit<units::length::meter_t>(leftDistance);
    _ownVehicleInformation.distanceToLaneBoundaryRight = units::make_unit<units::length::meter_t>(rightDistance);
  }

  // Steering wheel angle
  {
    _ownVehicleInformation.steeringWheelAngle = units::make_unit<units::angle::radian_t>(_state.vehicle->GetSteeringWheelAngle());
  }

  {  // Collision
    const auto overlappingMovingObjects{_state.query.FindOverlapping<osiql::MovingObject>(_state.vehicle)};
    const auto overlappingStaticObjects{_state.query.FindOverlapping<osiql::StaticObject>(_state.vehicle)};
    _ownVehicleInformation.collision = overlappingMovingObjects.size() > 1 || !overlappingStaticObjects.empty();
  }

  _ownVehicleInformation.forcedLaneChangeStartTrigger = forcedLaneChangeAction;
  return _ownVehicleInformation;
}
}  // namespace scm
