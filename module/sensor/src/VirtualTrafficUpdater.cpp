/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//
// #include "VirtualTrafficUpdater.h"
//
//
//
// #include "DependencyInterface.h"
// #include "OsiEnumConverter.h"
// #include "SensorDriverScmDefinitions.h"
// #include "SensorTypes.h"
// #include "SurroundingTrafficUpdater.h"
// #include "include/common/ScmDefinitions.h"
// #include "include/common/SensorDriverScmDefinitions.h"
// #include <OsiQueryLibrary/osiql.h>
//
// namespace scm
//{
//
// VirtualAgentsSCM VirtualTrafficUpdater::GetVirtualAgentInformation(double previewDistance)
//{
//  if (_routeElementForGraph.roadId.empty())
//  {
//    _routeElementForGraph = FindRouteElementForRoadGraph(previewDistance);
//  }
//
//  const double visibilityDistance{_dependency->GetVisibilityDistance()};
//  auto roadId = _dependency->GetRoadId();
//  if (IsEgoOnSharedRoadPositionOverVisibilityDistance(visibilityDistance, roadId, _routeElementForGraph.roadId))
//  {
//    _routeElementForGraph.roadId = "";
//  }
//
//  VirtualAgentsSCM virtualTraffic;
//  if (!_routeElementForGraph.roadId.empty())
//  {
//    std::vector<std::vector<RouteElement>> routeElements;
//    routeElements = CreateBackwardRoadGraph(_routeElementForGraph);
//    _sharedRoadId = routeElements.front().back().roadId;
//
//    std::vector<const WorldObjectInterface*> virtualObjects;
//    auto egoRoadId = _dependency->GetRoadId();
//
//    if (isEgoOnLeftLane(egoRoadId))
//    {
//      for (const auto routeElement : routeElements)
//      {
//        if (egoRoadId != routeElement[0].roadId)
//        {
//          for (auto& road : routeElement)
//          {
//            double roadLength = _dependency->GetRoadLength(road.roadId).value();
//            double startPoint = 0;
//            double endPoint = 0;
//
//            if (routeElement.front().roadId == road.roadId)
//            {
//              const std::vector<RouteElement> routeElements{routeElement.front()};
//              auto roadStream = _dependency->GetRoadStream(routeElements);
//              int targetLaneId = GetTargetLaneId(roadStream);
//              startPoint = roadLength - visibilityDistance;
//              endPoint = roadLength;
//              auto objects = GetVirtualAgents(targetLaneId, road, roadStream, startPoint, endPoint);
//              virtualObjects.insert(virtualObjects.end(), objects.begin(), objects.end());
//            }
//
//            if ((routeElement.front().roadId != road.roadId) && (routeElement.back().roadId != road.roadId))
//            {
//              const std::vector<RouteElement> routeElements{road};
//              auto roadStream = _dependency->GetRoadStream(routeElements);
//              int targetLaneId = GetTargetLaneId(roadStream);
//              startPoint = 0;
//              endPoint = roadLength;
//              auto objects = GetVirtualAgents(targetLaneId, road, roadStream, startPoint, endPoint);
//              virtualObjects.insert(virtualObjects.end(), objects.begin(), objects.end());
//            }
//
//            if (routeElement.back().roadId == road.roadId)
//            {
//              const std::vector<RouteElement> routeElements{routeElement.back()};
//              auto roadStream = _dependency->GetRoadStream(routeElements);
//              int targetLaneId = GetTargetLaneId(roadStream);
//              startPoint = 0;
//              endPoint = visibilityDistance;
//              auto objects = GetVirtualAgents(targetLaneId, road, roadStream, startPoint, endPoint);
//              virtualObjects.insert(virtualObjects.end(), objects.begin(), objects.end());
//            }
//          }
//        }
//      }
//    }
//
//    if (!virtualObjects.empty())
//    {
//      std::sort(virtualObjects.begin(), virtualObjects.end());
//      virtualObjects.erase(std::unique(virtualObjects.begin(), virtualObjects.end()), virtualObjects.end());
//    }
//
//    auto stream = osiql::Stream(_state.GetPose(), 1200.0, 1200.0, osiql::Strategy::Panoramic);
//    auto resultfind = stream.FindPeripheral<osiql::MovingObject>();
//    std::vector<std::pair<const osiql::MovingObject*, osiql::Stream::Distance>> virtualAgentsOsi = stream.FindPeripheral<osiql::MovingObject>();
//
//    ObjectInformationSCM oldobj{};
//    ObjectInformationSCM osiobj{};
//    for (auto& virtualObject : virtualObjects)
//    {
//      auto surroundingVehicle = dynamic_cast<const AgentInterface*>(virtualObject);
//      if (!surroundingVehicle) continue;
//
//      if (surroundingVehicle->GetId() != _dependency->GetAgent()->GetId())
//      {
//        oldobj = DetermineVirtualAgentParameters(surroundingVehicle);
//        virtualTraffic.push_back(DetermineVirtualAgentParameters(surroundingVehicle));
//      }
//    }
//
//    for (const auto& [object, distance] : virtualAgentsOsi)
//    {
//      osiobj = DetermineVirtualAgentParametersOsi(object, distance.value);
//    }
//
//    isVirtualAgentsOsiAndOldEqual(oldobj, osiobj);
//  }
//  return virtualTraffic;
//}
//
// ObjectInformationSCM VirtualTrafficUpdater::DetermineVirtualAgentParametersOsi(const osiql::MovingObject* movingObject, double distance) const
//{
//  auto position{movingObject->positions.front()};
//  auto frontPosition{_state.GetRoute().GetRoadPoint(movingObject->GetXY(osiql::Anchor::FRONT))};
//
//  ObjectInformationSCM obj;
//
//  obj.id = movingObject->GetId();
//  obj.exist = true;
//  obj.isStatic = false;
//
//  obj.length = movingObject->GetLength();
//  obj.width = movingObject->GetWidth();
//  obj.heading = movingObject->GetYaw();
//  obj.height = movingObject->GetHeight();
//  const auto velocity{movingObject->GetVelocity()};
//  obj.absoluteVelocity = velocity.Length();
//
//  const auto acceleration{movingObject->GetAcceleration()};
//  const osiql::Vector2d projectionVector(std::cos(position.angle), std::sin(position.angle));
//  const auto projectedAcceleration{acceleration.Dot(projectionVector) * projectionVector};
//  obj.acceleration = projectedAcceleration.x;
//  obj.lanetype = TranslateLaneType(_state.GetPose().GetLane().GetType());
//
//  obj.relativeLongitudinalDistance = distance;
//  const osiql::Vehicle* vehicleCast{static_cast<const osiql::Vehicle*>(movingObject)};
//  if (vehicleCast)
//  {
//    vehicleCast->GetVehicleType();
//    obj.vehicleClassification = TranslateVehicleType(vehicleCast->GetVehicleType());
//    obj.brakeLightsActive = TranslateBrakeLightState(vehicleCast->GetBrakeLightState());
//    obj.indicatorState = TranslateIndicatorState(vehicleCast->GetIndicatorState());
//  }
//  return obj;
//}
//
// void VirtualTrafficUpdater::isVirtualAgentsOsiAndOldEqual(ObjectInformationSCM objold, ObjectInformationSCM objOsi)
//{
//  assert(objold.id == objOsi.id);
//  assert(objold.exist == objOsi.exist);
//  assert(std::abs(objold.length - objOsi.length) < 0.01);
//  assert(std::abs(objold.width - objOsi.width) < 0.01);
//  assert(std::abs(objold.heading - objOsi.heading) < 0.01);
//  assert(std::abs(objold.height - objOsi.height) < 0.01);
//  assert(std::abs(objold.absoluteVelocity - objOsi.absoluteVelocity) < 0.01);
//  assert(std::abs(objold.acceleration - objOsi.acceleration) < 0.01);
//  assert(objold.isStatic == objOsi.isStatic);
//  assert(objold.lanetype == objOsi.lanetype);
//  assert(std::abs(objold.relativeLongitudinalDistance - objOsi.relativeLongitudinalDistance) < 0.1);
//  assert(objold.vehicleClassification == objOsi.vehicleClassification);
//  assert(objold.brakeLightsActive == objOsi.brakeLightsActive);
//  assert(objold.indicatorState == objOsi.indicatorState);
//}
//
// std::vector<const WorldObjectInterface*> VirtualTrafficUpdater::GetVirtualAgents(int targetLaneId, RouteElement road, std::unique_ptr<RoadStreamInterface>& roadStream, double startPoint, double endPoint)
//{
//  GlobalRoadPosition position;
//  position.roadId = road.roadId;
//  position.laneId = targetLaneId;
//  position.roadPosition.s = units::length::meter_t(0.);
//  auto streamPosition = roadStream->GetStreamPosition(position);
//  auto laneStream = roadStream->GetLaneStream(streamPosition, targetLaneId);
//
//  GlobalRoadPosition start;
//  start.roadId = road.roadId;
//  start.laneId = targetLaneId;
//  start.roadPosition.s = units::length::meter_t(startPoint);
//
//  GlobalRoadPosition end;
//  end.roadId = road.roadId;
//  end.laneId = targetLaneId;
//  end.roadPosition.s = units::length::meter_t(endPoint);
//
//  auto startpoint = laneStream->GetStreamPosition(start);
//  auto endpoint = laneStream->GetStreamPosition(end);
//  auto objects = laneStream->GetObjectsInRange(startpoint, endpoint);
//
//  std::vector<const WorldObjectInterface*> objectsWithValidMlp{};
//  std::copy_if(objects.cbegin(), objects.cend(), std::back_inserter(objectsWithValidMlp), [](const WorldObjectInterface* object)
//               { return !object->GetRoadPosition(ObjectPointPredefined::FrontCenter).empty(); });
//
//  return objectsWithValidMlp;
//}
//
// RouteElement VirtualTrafficUpdater::FindRouteElementForRoadGraph(double previewDistance)
//{
//  const double visibilityDistance{std::min(previewDistance, _dependency->GetVisibilityDistance().value())};
//  auto junctionConnectors = _dependency->GetRelativeRoads(units::length::meter_t(visibilityDistance));
//
//  bool isJunction{false};
//  RouteElement routeElement;
//  for (int i = 0; i < junctionConnectors.size(); i++)
//  {
//    if (isJunction == true)
//    {
//      routeElement.roadId = junctionConnectors[i].roadId;
//      routeElement.inOdDirection = junctionConnectors[i].inOdDirection;
//      return routeElement;
//    }
//
//    if (junctionConnectors[i].junction == true)
//    {
//      isJunction = true;
//    }
//  }
//
//  return routeElement;
//}
//
// std::vector<std::vector<RouteElement>> VirtualTrafficUpdater::CreateBackwardRoadGraph(RouteElement routeElementForGraph)
//{
//  static constexpr size_t MAX_ROADGRAPH_DEPTH = 4;
//  auto [roadGraph, root] = _dependency->GetRoadGraph(routeElementForGraph, MAX_ROADGRAPH_DEPTH, false);
//  std::vector<std::vector<RouteElement>> routeElements;
//  for (auto [successorBegin, succesorEnd] = adjacent_vertices(root, roadGraph); successorBegin != succesorEnd; successorBegin++)
//  {
//    std::vector<RouteElement> routeSections;
//    auto routeElement = get(RouteElement{}, roadGraph, *successorBegin);
//
//    for (auto [successor, succesorEnd] = adjacent_vertices(*successorBegin, roadGraph); successor != succesorEnd; successor++)
//    {
//      auto routeElement = get(RouteElement{}, roadGraph, *successor);
//      routeSections.push_back(routeElement);
//    }
//    routeSections.push_back(routeElement);
//    routeSections.push_back(routeElementForGraph);
//    routeElements.push_back(routeSections);
//  }
//  return routeElements;
//}
//
// bool VirtualTrafficUpdater::IsEgoOnSharedRoadPositionOverVisibilityDistance(double visibilityDistance, std::string egoRouteId, std::string sharedRoadId)
//{
//  if (egoRouteId == sharedRoadId)
//  {
//    auto pos = _dependency->GetRoadPositions(ObjectPointPredefined::FrontCenter, _dependency->GetAgent());
//    auto posEgo = pos.at(egoRouteId).roadPosition.s.value();
//
//    if (posEgo >= visibilityDistance)
//    {
//      return true;
//    }
//  }
//  return false;
//}
//
// LaneType VirtualTrafficUpdater::GetLaneType(const WorldObjectInterface* object) const
//{
//  auto virtualAgentPosition = object->GetRoadPosition(ObjectPointPredefined::FrontCenter);
//  auto virtualRoadId = virtualAgentPosition.begin()->first;
//  auto virtualLaneId = virtualAgentPosition[virtualRoadId].laneId;
//
//  RouteElement routeElement;
//  routeElement.roadId = virtualRoadId;
//  routeElement.inOdDirection = true;
//  const std::vector<RouteElement> route{routeElement};
//  auto roadStream = _dependency->GetRoadStream(route);
//  StreamPosition streamPosition{virtualAgentPosition[virtualRoadId].roadPosition.s, virtualAgentPosition[virtualRoadId].roadPosition.t, virtualAgentPosition[virtualRoadId].roadPosition.hdg};
//  auto laneStream = roadStream->GetLaneStream(streamPosition, virtualLaneId);
//  auto laneType = laneStream->GetLaneTypes();
//  return laneType[0].second;
//}
//
// double VirtualTrafficUpdater::GetDistanceToEndOfLaneEgo() const
//{
//  auto roadIdEgo = _dependency->GetRoadId();
//  auto lengthEgo = _dependency->GetAgent()->GetLength().value();
//  auto pos = _dependency->GetRoadPositions(ObjectPointPredefined::FrontCenter, _dependency->GetAgent());
//  if (roadIdEgo == _sharedRoadId)
//  {
//    auto posEgo = pos.at(roadIdEgo).roadPosition.s.value();
//    return posEgo;
//  }
//
//  auto roadLengthEgo = _dependency->GetRoadLength(roadIdEgo).value();
//  auto posEgo = pos.at(roadIdEgo).roadPosition.s.value();
//  return roadLengthEgo - posEgo;
//}
//
// double VirtualTrafficUpdater::GetDistanceToEndOfLaneVirtualAgent(const WorldObjectInterface* object) const
//{
//  auto virtualpos = object->GetRoadPosition(ObjectPointPredefined::FrontCenter);
//  auto it = virtualpos.begin();
//  auto lengthAgent = object->GetLength();
//
//  if (it->first == _sharedRoadId)
//  {
//    return virtualpos.at(it->first).roadPosition.s.value();
//  }
//  auto roadLength = _dependency->GetRoadLength(it->first).value();
//  auto posVirt = virtualpos.at(it->first).roadPosition.s.value();
//  return roadLength - posVirt;
//}
//
// double VirtualTrafficUpdater::GetDistanceBetweenVirtualAgentAndEgo(const WorldObjectInterface* object) const
//{
//  double distanceToEndOfLaneEgo = GetDistanceToEndOfLaneEgo();
//  double distanceToEndOfLaneAgent = GetDistanceToEndOfLaneVirtualAgent(object);
//  auto lengthEgo = _dependency->GetAgent()->GetLength().value();
//  auto lengthVirtualAgent = object->GetLength().value();
//
//  if (distanceToEndOfLaneEgo > distanceToEndOfLaneAgent)
//  {
//    return distanceToEndOfLaneEgo - distanceToEndOfLaneAgent - lengthVirtualAgent;
//  }
//  else if (distanceToEndOfLaneEgo < distanceToEndOfLaneAgent)
//  {
//    return distanceToEndOfLaneAgent - distanceToEndOfLaneEgo - lengthEgo;
//  }
//  return 0;
//}
//
// ObjectInformationSCM VirtualTrafficUpdater::DetermineVirtualAgentParameters(const AgentInterface* agent) const
//{
//  ObjectInformationSCM obj;
//  const auto object = dynamic_cast<const WorldObjectInterface*>(agent);
//  auto egoID = _dependency->GetAgent()->GetId();
//  if (egoID == object->GetId())
//  {
//    return {};
//  }
//
//  obj.id = object->GetId();
//  obj.exist = true;
//  obj.length = object->GetLength().value();
//  obj.width = object->GetWidth().value();
//  obj.heading = object->GetYaw().value();
//  obj.height = object->GetHeight().value();
//  obj.absoluteVelocity = _dependency->GetVelocity(VelocityScope::Absolute, object).value();
//  obj.acceleration = object->GetAcceleration().Projection(object->GetYaw()).value();
//  obj.isStatic = false;
//  obj.lanetype = GetLaneType(object);
//  obj.relativeLongitudinalDistance = GetDistanceBetweenVirtualAgentAndEgo(object);
//  obj.vehicleClassification = scm::common::VehicleClass::kInvalid;
//  obj.brakeLightsActive = agent->GetBrakeLight();
//  obj.indicatorState = agent->GetIndicatorState();
//
//  return obj;
//}
//
// int VirtualTrafficUpdater::GetTargetLaneId(std::unique_ptr<RoadStreamInterface>& roadStream)
//{
//  auto allLanes = roadStream->GetAllLaneStreams();
//  for (int i = 0; i < allLanes.size(); i++)
//  {
//    StreamPosition position{};
//    auto pos = allLanes[i]->GetRoadPosition(position);
//    auto type = allLanes[i]->GetLaneTypes();
//
//    if (type.at(0).second == LaneType::Driving || type.at(0).second == LaneType::Entry || type.at(0).second == LaneType::OnRamp)
//    {
//      return pos.laneId;
//    }
//  }
//  return 0;
//}
//
// bool VirtualTrafficUpdater::isEgoOnLeftLane(std::string egoRoadId)
//{
//  auto egoPosition = _dependency->GetRoadPositions(ObjectPointPredefined::FrontCenter, _dependency->GetAgent());
//  auto egoLaneId = egoPosition.at(egoRoadId).laneId;
//
//  RouteElement routeElement;
//  routeElement.roadId = egoRoadId;
//  routeElement.inOdDirection = true;
//  const std::vector<RouteElement> route{routeElement};
//  auto stream = _dependency->GetRoadStream(route);
//  StreamPosition streamPosition{};
//  streamPosition.s = egoPosition[egoRoadId].roadPosition.s;
//  streamPosition.t = egoPosition[egoRoadId].roadPosition.t;
//  streamPosition.hdg = egoPosition[egoRoadId].roadPosition.hdg;
//  auto allLanesAtRoad = stream->GetAllLaneStreams();
//  std::vector<std::pair<int, LaneType>> allLanesAndTypes{};
//
//  for (int i = 0; i < allLanesAtRoad.size(); i++)
//  {
//    StreamPosition position{};
//    auto LanePostion = allLanesAtRoad[i]->GetRoadPosition(position);
//    auto LaneType = allLanesAtRoad[i]->GetLaneTypes();
//    allLanesAndTypes.push_back(std::make_pair(LanePostion.laneId, LaneType.at(0).second));
//  }
//  auto agentId = _dependency->GetAgent()->GetId();
//  LaneType type{};
//  bool isEgoOnLeftLane = false;
//  if (allLanesAndTypes.size() == 1)
//  {
//    isEgoOnLeftLane = true;
//  }
//  else
//  {
//    for (const auto& [laneId, laneType] : allLanesAndTypes)
//    {
//      if (laneId == egoLaneId)
//      {
//        if (type != LaneType::Driving)
//        {
//          isEgoOnLeftLane = true;
//          break;
//        }
//      }
//      type = laneType;
//    }
//  }
//
//  return isEgoOnLeftLane;
//}
//}  // namespace scm
