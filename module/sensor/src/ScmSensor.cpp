/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "../include/ScmSensor.h"

#include "Sensor.h"

namespace scm::sensor
{
Interface* Create(int cycleTime, const std::string& configurationDirectory, StochasticsInterface* staticStochastics, osiql::Query* osiQl, units::length::meter_t visibilityDistance, int agentId)
{
  return new scm::Sensor(configurationDirectory, osiQl, staticStochastics, visibilityDistance, agentId);
}
}  // namespace scm::sensor
