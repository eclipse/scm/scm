################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout, CMakeDeps
import re, os, subprocess


required_conan_version = ">=2.0"

class ScmRecipe(ConanFile):
    name = "scm"
    version = "1.0.0"

    # Optional metadata
    license = "Eclipse Public License 2.0"
    author = "<Put your name here> <And your email here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of hello package here>"
    topics = ("<Put some tag here>", "<here>", "<and here>")

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*", "include/*", "module/*", "cmake/*", "doc/*", "tests/*", "dllExport/*"

    def requirements(self):
        self.requires("stochastics/0.8.0@scm/testing")
        self.requires("osiquerylibrary/1.1.2@scm/testing")
        self.requires("libxml2/2.11.5")
        self.requires("libiconv/1.17")
        self.requires("units/2.3.3@scm/testing")
        self.requires("gtest/1.14.0")
        self.requires("open-simulation-interface/3.6.0@scm/testing")
        self.requires("protobuf/3.20.0")

    def configure(self):
        # We can control the options of our dependencies based on current options
        self.options["protobuf"].shared = False


    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def layout(self):
        cmake_layout(self)

    def generate(self):
        _cmake_prefix_paths = []
        for _, dependency in self.dependencies.items():
            _cmake_prefix_paths.append(dependency.package_folder)
        _cmake_prefix_paths = ';'.join(str(_cmake_prefix_path) for _cmake_prefix_path in _cmake_prefix_paths)

        deps = CMakeDeps(self)
        deps.generate()
        tc = CMakeToolchain(self)
        tc.cache_variables["CMAKE_PREFIX_PATH"] = _cmake_prefix_paths
        tc.generate()

    def build(self):
        cmake = CMake(self)

        # work around for the moment
        # better solution is to add self.cpp_info.set_property("cmake_find_mode", "none") in protobuf recipe and open-simulation-interface recipe
        os.remove(os.path.join(os.getcwd(), "generators/FindProtobuf.cmake"))
        os.remove(os.path.join(os.getcwd(), "generators/protobuf-config.cmake"))
        os.remove(os.path.join(os.getcwd(), "generators/open-simulation-interface-config.cmake"))

        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["AlgorithmScm"]