/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file dynamicsModelInterface.h
#pragma once

#include "include/signal/dynamicsModelInput.h"
#include "include/signal/dynamicsModelOutput.h"

namespace scm::dynamics_model
{
//! @brief Interface for dynamicsModel
class Interface
{
public:
  //! @brief Trigger the DynamicsModelOutput
  //! @param input DynamicsModelInput
  //! @return dynamics model signal
  virtual scm::signal::DynamicsModelOutput Trigger(const scm::signal::DynamicsModelInput& input) = 0;
  virtual ~Interface() = default;
};
}  // namespace scm::dynamics_model