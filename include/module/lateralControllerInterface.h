/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file lateralControllerInterface.h
#pragma once

#include "../include/signal/lateralControllerInput.h"
#include "../include/signal/lateralControllerOutput.h"

namespace scm::lateral_controller
{
//! @brief Interface for lateralcontroller
class Interface
{
public:
  //! @brief Trigger the LateralControllerOutput
  //! @param input LateralControllerInput
  //! @param time
  //! @return lateral controller output signal
  virtual scm::signal::LateralControllerOutput Trigger(const scm::signal::LateralControllerInput& input, units::time::millisecond_t time) = 0;
  virtual ~Interface() = default;
};
}  // namespace scm::lateral_controller