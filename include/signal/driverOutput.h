/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file driverOutput.h
#pragma once

#include <sstream>
#include <string>

#include "include/common/LateralInformation.h"
#include "include/common/SecondaryDriverTasks.h"

namespace scm::signal
{
//! @brief Struct definition for the description of a DriverOutput
struct DriverOutput
{
  //! @brief information about the lateralInformation
  LateralInformation lateralInformation;
  //! @brief information about the triggerNewRouteRequest
  bool triggerNewRouteRequest;
  //! @brief information about the secondaryDriverTasks
  SecondaryDriverTasks secondaryDriverTasks;
  //! @brief information about the acceleration
  units::acceleration::meters_per_second_squared_t acceleration;
};

}  // namespace scm::signal