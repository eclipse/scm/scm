/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file lateralControllerInput.h
#pragma once

#include <units.h>
#include <sstream>
#include <string>
#include <vector>

#include "../common/ScmDefinitions.h"

namespace scm::signal
{
//! @brief Struct definition for the description of a LateralControllerInput
struct LateralControllerInput
{
  //! @brief information about the velocity
  units::velocity::meters_per_second_t velocity;
  //! @brief information about the laneWidth
  units::length::meter_t laneWidth;
  //! @brief information about the steeringWheelAngle
  units::angle::radian_t steeringWheelAngle;
  //! @brief information about the lateralDeviation
  units::length::meter_t lateralDeviation;
  //! @brief information about the gainLateralDeviation
  units::angular_acceleration::radians_per_second_squared_t gainLateralDeviation;
  //! @brief information about the headingError
  units::angle::radian_t headingError;
  //! @brief information about the gainHeadingError
  units::frequency::hertz_t gainHeadingError;
  //! @brief information about the kappaManoeuvre
  units::curvature::inverse_meter_t kappaManoeuvre;
  //! @brief information about the kappaRoad
  units::curvature::inverse_meter_t kappaRoad;
  //! @brief information about the curvatureOfSegmentsToNearPoint
  std::vector<units::curvature::inverse_meter_t> curvatureOfSegmentsToNearPoint;
  //! @brief information about the curvatureOfSegmentsToFarPoint
  std::vector<units::curvature::inverse_meter_t> curvatureOfSegmentsToFarPoint;
  //! @brief information about the steeringRatio
  double steeringRatio;
  //! @brief information about the frontAxleMaxSteering
  double frontAxleMaxSteering;
  //! @brief information about the wheelBase
  units::length::meter_t wheelBase;
};

}  // namespace scm::signal