/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file longitudinalControllerInput.h
#pragma once

#include <sstream>
#include <string>

#include "../../module/parameterParser/src/Signals/ParametersScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "include/common/VehicleProperties.h"

namespace scm::signal
{
//! @brief Struct definition for the description of a LongitudinalControllerInput
struct LongitudinalControllerInput
{
  //! @brief information about the own vehicle
  OwnVehicleInformationSCM ownVehicleInformationSCM;
  //! @brief information about the acceleration
  units::acceleration::meters_per_second_squared_t acceleration;
  //! @brief information about driver parameters
  DriverParameters driverParameters;
  //! @brief information about vehicle parameters
  scm::common::vehicle::properties::EntityProperties vehicleParameters;
  //! @brief The cycleTime
  units::time::millisecond_t cycleTime;
};

}  // namespace scm::signal