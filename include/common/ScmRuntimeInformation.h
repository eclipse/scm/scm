/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//! @file ScmRuntimeInformation.h
#pragma once
#include <string>

namespace scm
{
//! @brief Struct definition for the description of a LoggerOptions
struct LoggerOptions
{
  //! @brief logParameters
  bool logParameters{false};
  //! @brief logValues
  bool logValues{false};
  //! @brief log output as xml instead of csv
  bool xmlOutput{false};
};

//! @brief Struct definition for the description of a RuntimeInformation
struct RuntimeInformation
{
  //! @brief loggerOptions
  LoggerOptions loggerOptions;

  //! @brief Struct definition for the description of a Directories
  struct Directories
  {
    //! @brief configuration
    std::string configuration{};
    //! @brief output
    std::string output{};
    //! @brief library
    std::string library{};
  };

  //! @brief directories
  Directories directories;
  //! @brief frameworkVersion
  std::string frameworkVersion{};
};
}  // namespace scm