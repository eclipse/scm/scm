/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <memory>

#include "Parameters.h"
#include "ScmRuntimeInformation.h"
#include "include/common/ParameterInterface.h"

namespace scm::parameter::internal
{

/// transforms raw parameters into the corresponding paramter interface add method
static void TransformParameterValue(ParameterInterface* param, const std::string& key, const ParameterValue& value)
{
  if (std::holds_alternative<bool>(value))
  {
    param->AddParameterBool(key, std::get<bool>(value));
  }
  else if (std::holds_alternative<int>(value))
  {
    param->AddParameterInt(key, std::get<int>(value));
  }
  else if (std::holds_alternative<double>(value))
  {
    param->AddParameterDouble(key, std::get<double>(value));
  }
  else if (std::holds_alternative<std::string>(value))
  {
    param->AddParameterString(key, std::get<std::string>(value));
  }
  else if (std::holds_alternative<std::vector<bool>>(value))
  {
    param->AddParameterBoolVector(key, std::get<std::vector<bool>>(value));
  }
  else if (std::holds_alternative<std::vector<int>>(value))
  {
    param->AddParameterIntVector(key, std::get<std::vector<int>>(value));
  }
  else if (std::holds_alternative<std::vector<double>>(value))
  {
    param->AddParameterDoubleVector(key, std::get<std::vector<double>>(value));
  }
  else if (std::holds_alternative<std::vector<std::string>>(value))
  {
    param->AddParameterStringVector(key, std::get<std::vector<std::string>>(value));
  }
  else if (std::holds_alternative<StochasticDistribution>(value))
  {
    param->AddParameterStochastic(key, std::get<StochasticDistribution>(value));
  }
  else
    throw std::runtime_error("unable to transform parameter type");
}

/// switches between flat parameters and parameter lists
struct VisitParameterElement
{
  const ParameterKey key;     //!< Name of parameter as string
  ParameterInterface* param;  //!< Parameter interface

  //! VisitParameterElement constructor
  //!
  //! @param[in] key      Name of parameter as string
  //! @param[in] param    Parameter interface
  VisitParameterElement(const ParameterKey key, ParameterInterface* param)
      : key{key}, param{param}
  {
  }

  //! Execute if visited variant holds a ParameterValue
  //!
  //! @param[in] value    Value of parameter
  void operator()(const ParameterValue& value)
  {
    TransformParameterValue(param, key, value);
  }

  //! Execute if visited variant holds a ParameterList
  //!
  //! @param[in] list    List of parameter sets level 3
  void operator()(const ParameterListLevel2& list)
  {
    for (const auto& parameterSet : list)
    {
      auto& sub_param = param->InitializeListItem(key);
      for (const auto& [sub_key, sub_value] : parameterSet)
      {
        TransformParameterValue(&sub_param, sub_key, sub_value);
      }
    }
  }

  //! Execute if visited variant holds a ParameterList
  //!
  //! @param[in] list    List of parameter sets level 2
  void operator()(const ParameterListLevel1& list)
  {
    for (const auto& parameterSet : list)
    {
      auto& sub_param = param->InitializeListItem(key);
      for (const auto& [sub_key, sub_value] : parameterSet)
      {
        VisitParameterElement visitor(sub_key, &sub_param);
        std::visit(visitor, sub_value);
      }
    }
  }
};

}  // namespace scm::parameter::internal

namespace scm::parameter
{

//! @brief Takes a parameter set and transforms it into an unqiue parameter interface
//! @param runtimeInformation    Common runtimeInformation
//! @param container             The parameter set
//! @returns Unique pointer to a new parameter interface
template <typename T>
std::unique_ptr<T> make(const RuntimeInformation& runtimeInformation, const ParameterSetLevel1& container)
{
  auto parameter = std::make_unique<T>(runtimeInformation);

  for (const auto& [key, value] : container)
  {
    internal::VisitParameterElement visitor(key, parameter.get());
    std::visit(visitor, value);
  }

  return std::move(parameter);
}

}  // namespace scm::parameter
