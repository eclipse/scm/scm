/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <units.h>

#include "include/common/CommonHelper.h"
namespace scm
{
namespace LaneMarking
{
enum class Type
{
  None,
  Solid,
  Broken,
  Solid_Solid,
  Solid_Broken,
  Broken_Solid,
  Broken_Broken,
  Grass,
  Botts_Dots,
  Curb
};

enum class Color
{
  White,
  Yellow,
  Red,
  Blue,
  Green,
  Other
};

/// @brief Structure defining an entity
struct Entity
{
  Type type{Type::None};              ///< type of the entity
  Color color{Color::White};          ///< color of the entity
  units::length::meter_t relativeStartDistance{0.0};  ///< relative start distance
  units::length::meter_t width{0.0};                  ///< width of the entity

  bool operator==(const Entity& entity) const
  {
    return type == entity.type && color == entity.color && scm::common::DoubleEquality(relativeStartDistance.value(), entity.relativeStartDistance.value()) && scm::common::DoubleEquality(width.value(), entity.width.value());
  }
};
}  // namespace LaneMarking
}  // namespace scm