/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file  ScmEnums.h

#pragma once

/*! \addtogroup ScmEnums
 * @{
 * \brief This component contains discrete states of action and gaze.

 * \section ScmEnums_Inputs Inputs
 * none
 *
 * \section ScmEnums_Outputs Outputs
 * none
 *
 * \section ScmEnums_InternalParameters InternalParameters
 * none
 *
 * \section ScmEnums_ExternalParameters ExternalParameters
 * none
 */

//! @brief A collection of longitudinal action states that a driver can be in
enum class LongitudinalActionState
{
  // The numbers assignet to each state correspond are used in the visualisation.
  // Thus they may not be changed, unless the same change is applied there!
  // FAILURE to do so will lead to a WRONG display of these values!

  SPEED_ADJUSTMENT = 0,
  BRAKING_TO_END_OF_LANE = 1,
  APPROACHING = 2,
  FOLLOWING_AT_DESIRED_DISTANCE = 3,
  FALL_BACK_TO_DESIRED_DISTANCE = 5,
  PREPARING_TO_MERGE = 6,
  STOPPED = 8,
  PASSIVE_ZIP_MERGING = 9,
  ACTIVE_ZIP_MERGING = 10
};

//! @brief A collection of gaze states that the driver in question can be in
enum class GazeState
{
  SACCADE = 0,
  EGO_FRONT,           // 1
  EGO_FRONT_FAR,       // 2
  RIGHT_FRONT,         // 3
  RIGHT_FRONT_FAR,     // 4
  LEFT_FRONT,          // 5
  LEFT_FRONT_FAR,      // 6
  EGO_REAR,            // 7
  RIGHT_REAR,          // 8
  LEFT_REAR,           // 9
  RIGHT_SIDE,          // 10
  LEFT_SIDE,           // 11
  UNDEFINED,           // 12
  INSTRUMENT_CLUSTER,  // 13
  INFOTAINMENT,        // 14
  HUD,                 // 15
  RIGHTRIGHT_FRONT,    // 16
  LEFTLEFT_FRONT,      // 17
  RIGHTRIGHT_REAR,     // 18
  LEFTLEFT_REAR,       // 19
  RIGHTRIGHT_SIDE,     // 20
  LEFTLEFT_SIDE,       // 21
  DISTRACTION,         // 22
  //
  NumberOfGazeStates
};

//! @brief A collection of the situations that a driver might find himself in
enum class Situation
{
  // The numbers assignet to each state correspond are used in the visualisation.
  // Thus they may not be changed, unless the same change is applied there!
  // FAILURE to do so will lead to a WRONG display of these values!

  UNDEFINED = 0,

  FREE_DRIVING = 1,
  FOLLOWING_DRIVING = 2,
  SUSPICIOUS_OBJECT_IN_LEFT_LANE = 3,
  SUSPICIOUS_OBJECT_IN_RIGHT_LANE = 4,

  LANE_CHANGER_FROM_LEFT = 5,
  LANE_CHANGER_FROM_RIGHT = 6,

  SIDE_COLLISION_RISK_FROM_RIGHT = 7,
  SIDE_COLLISION_RISK_FROM_LEFT = 8,

  OBSTACLE_ON_CURRENT_LANE = 9,
  COLLISION = 10,

  // Collection of sorted situation numbers
  NumberOfSituations = 11  // Always needs to be the highest consecutive number!
};

enum class MesoscopicSituation
{
  FREE_DRIVING,
  CURRENT_LANE_BLOCKED,
  MANDATORY_EXIT,
  QUEUED_TRAFFIC,
  ACTIVE_ZIP_MERGING,
  PASSIVE_ZIP_MERGING
};

enum class InformationRequestTrigger
{
  LATERAL_ACTION,
  LONGITUDINAL_ACTION,
  UNDEFINED
};

//! @brief A high cognitive collection of sub situations in case of anticipating lane changer from right
enum class HighCognitiveSituation
{
  UNDEFINED = 0,

  APPROACH,
  SALIENT_APPROACH,
  FOLLOW,
  SALIENT_FOLLOW
};

enum class MinThwPerspective
{
  NORM = 0,
  EGO_ANTICIPATED_FRONT = 1,
  EGO_ANTICIPATED_REAR = 2
};

//! @brief A collection of sight areas with different sight qualities
enum class FieldOfViewAssignment
{
  FOVEA = 0,      // foveal sight area
  UFOV = 1,       // useful field of view around the foveal center
  PERIPHERY = 2,  // peripherical view and other areas, which are not seen yet
                  // information is only overwritten by strong information like
                  // bottom up stimulus
  NONE = 3
};

//! @brief A collection of risk grades which are divided into Low, Average and High
enum class Risk
{
  LOW = 1,
  MEDIUM = 4,
  HIGH = 8
};

//! @brief A collection for different states of Lanes
enum class LaneExistence
{
  UNKNOWN = 0,
  TRUE,
  FALSE
};

//! @brief A collection of critical actions from which a driver has to choose one
enum class CriticalActionState
{
  COLLISION_UNAVOIDABLE,
  BRAKING,
  EVADING_LEFT,
  EVADING_RIGHT,
  BRAKING_OR_EVADING

};

enum class SwervingState
{
  NONE,
  EVADING,
  DEFUSING,
  RETURNING
};

enum class SideAoiCriteria
{
  NONE,
  FRONT,
  REAR,
  MAX,
  MIN
};

enum class LaneChangeState
{
  NoLaneChange = 0,
  LaneChangeLeft,
  LaneChangeRight
};

/** @} */  // End of class