/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "SensorDriverScmDefinitions.h"

units::length::meter_t ObstructionLongitudinal::GetNetDistance() const
{
  if (front >= 0.0_m)
  {
    return front;
  }
  if (rear <= 0.0_m)
  {
    return rear;
  }
  return 0.0_m;
}

bool operator==(const LaneInformationTrafficRulesSCM& lhs, const LaneInformationTrafficRulesSCM& rhs)
{
  return std::tie(lhs.trafficSigns, lhs.trafficLights, lhs.laneMarkingsRight, lhs.laneMarkingsLeft) ==
         std::tie(rhs.trafficSigns, rhs.trafficLights, rhs.laneMarkingsRight, rhs.laneMarkingsLeft);
}
bool operator!=(const LaneInformationTrafficRulesSCM& lhs, const LaneInformationTrafficRulesSCM& rhs)
{
  return !(lhs == rhs);
}
