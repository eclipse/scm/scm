/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <units.h>

#include <vector>

#include "ScmDefinitions.h"
using namespace units::literals;
namespace scm
{
struct LateralInformation
{
  //! @brief Lateral net distance that needs to be covered
  units::length::meter_t laneWidth{0.0_m};
  //! @brief Current lateral deviation regarding trajectory
  units::length::meter_t deviation{0.0_m};
  //! @brief Gain of lateral deviation controller
  units::angular_acceleration::radians_per_second_squared_t gainDeviation{0.0_rad_per_s_sq};
  //! @brief Current heading error regarding trajectory
  units::angle::radian_t headingError{0.0_rad};
  //! @brief Gain of the heading error controller
  units::frequency::hertz_t gainHeadingError{0.0_Hz};
  //! @brief Current curvature adjustment due to manoeuvre e.g. lane changing
  units::curvature::inverse_meter_t kappaManoeuvre{0.0_i_m};
  //! @brief Current curvature of road
  units::curvature::inverse_meter_t kappaRoad{0.0_i_m};
};
}  // namespace scm