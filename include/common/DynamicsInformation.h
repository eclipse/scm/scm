/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <units.h>

#include "include/common/ScmDefinitions.h"
using namespace units::literals;
namespace scm
{
//! @brief This struct contains all values regarding the dynamic calculation of the agent
struct DynamicsInformation
{
  //! @brief Acceleration of th agent
  units::acceleration::meters_per_second_squared_t acceleration{0.0_mps_sq};
  //! @brief Velocity in x direction
  units::velocity::meters_per_second_t velocityX{0.0_mps};
  //! @brief Velocity in y direction
  units::velocity::meters_per_second_t velocityY{0.0_mps};
  //! @brief x-position of agent
  units::length::meter_t positionX{0.0_m};
  //! @brief y-position of agent
  units::length::meter_t positionY{0.0_m};
  //! @brief Yaw angle of agent
  units::angle::radian_t yaw{0.0_rad};
  //! @brief Yaw rate of the agent
  units::angular_velocity::radians_per_second_t yawRate{0.0_rad_per_s};
  //! @brief Yaw acceleration of the agent
  units::angular_acceleration::radians_per_second_squared_t yawAcceleration{0.0_rad_per_s_sq};
  //! @brief Roll angle of agent
  units::angle::radian_t roll{0.0_rad};
  //! @brief New angle of the steering wheel angle
  units::angle::radian_t steeringWheelAngle{0.0_rad};
  //! @brief Centripetal acceleration of the agent
  units::acceleration::meters_per_second_squared_t centripetalAcceleration{0.0_mps_sq};
  //! @brief Distance traveled by the agent during this timestep
  units::length::meter_t travelDistance{0.0_m};
};
}  // namespace scm