/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <functional>
#include <map>
#include <optional>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

namespace scm::common
{
//-----------------------------------------------------------------------------
//! @brief Tokenizes string by delimiter.
//!
//! @param [in] str             String to be tokenized
//! @param [in] delimiter       Delimiter by which string gets tokenized
//!
//! @return                     Vector of trimmed tokens
//-----------------------------------------------------------------------------
[[maybe_unused]] static std::vector<std::string> TokenizeString(const std::string& str, const char delimiter = ',')
{
  constexpr char WHITESPACE[] = " \t\n\v\f\r";
  std::vector<std::string> tokens;

  if (str.empty())
  {
    return tokens;
  }

  std::string_view remaining_input_view{str};
  auto tokenlength = static_cast<std::string_view::size_type>(-1);

  do
  {
    remaining_input_view.remove_prefix(tokenlength + 1);  // remove previous token from view
    tokenlength = remaining_input_view.find(delimiter);

    // if no delimiter is left, use whole string
    tokenlength = tokenlength != std::string::npos ? tokenlength : remaining_input_view.length();

    // untrimmed view on token
    std::string_view token{remaining_input_view.data(), tokenlength};
    auto trim_start = token.find_first_not_of(WHITESPACE);

    if (trim_start == std::string::npos)
    {
      // only whitespace charactes in current token
      tokens.push_back("");
      continue;
    }

    auto trim_end = token.find_last_not_of(WHITESPACE);
    tokens.emplace_back(token.substr(trim_start, trim_end - trim_start + 1));
  } while (tokenlength < remaining_input_view.length());

  return tokens;
}

static constexpr double EPSILON = 0.001;  //! Treat values smaller than epsilon as zero for geometric calculations

[[maybe_unused]] static inline constexpr bool DoubleEquality(double value1, double value2, double epsilon = EPSILON)
{
  return std::abs(value1 - value2) <= epsilon;
}

template <typename ContainerT, typename PredicateT>
void erase_if(ContainerT& items, const PredicateT& predicate)
{
  for (auto it = items.begin(); it != items.end();)
  {
    if (predicate(*it))
      it = items.erase(it);
    else
      ++it;
  }
}

static inline void THROWIFFALSE(bool success, const std::string& message)
{
  if (!success)
  {
    throw std::runtime_error(message);
  }
}

namespace map
{
/// @brief queries a map for a given key and returns the value if available
template <typename KeyType, typename ValueType>
std::optional<std::reference_wrapper<const ValueType>> query(const std::map<KeyType, ValueType>& the_map, KeyType the_value)
{
  const auto& iter = the_map.find(the_value);
  return iter == the_map.end() ? std::nullopt : std::optional<std::reference_wrapper<const ValueType>>{std::cref<ValueType>(iter->second)};
}

template <typename ValueType>
std::optional<ValueType> query(const std::map<std::string, ValueType>& the_map, std::string the_value)
{
  const auto& iter = the_map.find(the_value);
  return iter == the_map.end() ? std::nullopt : std::make_optional(iter->second);
}

template <typename KeyType, typename ValueType>
std::optional<ValueType> query(const std::unordered_map<KeyType, ValueType>& the_map, KeyType the_value)
{
  const auto& iter = the_map.find(the_value);
  return iter == the_map.end() ? std::nullopt : std::make_optional(iter->second);
}

template <typename ValueType>
std::optional<ValueType> query(const std::unordered_map<std::string, ValueType>& the_map, std::string the_value)
{
  const auto& iter = the_map.find(the_value);
  return iter == the_map.end() ? std::nullopt : std::make_optional(iter->second);
}
}  // namespace map

}  // namespace scm::common