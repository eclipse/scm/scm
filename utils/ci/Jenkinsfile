/********************************************************************************
 * Copyright (c)  2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


pipeline {
  agent none
  parameters {
    booleanParam(name: 'BUILD_DOCKER_IMAGE', defaultValue: false, description: 'Force docker image (re-)build')
  }
  options {
    checkoutToSubdirectory('repo')
    timeout(time: 10, unit: 'HOURS')
    timestamps()
    disableConcurrentBuilds(abortPrevious: true)
    buildDiscarder(logRotator(numToKeepStr: '14', artifactNumToKeepStr: '2'))
  }
  environment {
    IMAGE_NAME = "eclipsescm/scm-ci"
    REPO_URL = "https://gitlab.eclipse.org/eclipse/scm/scm.git"
    DOCKERFILE_PATH = "utils/Dockerfile"
    OP_RELEASE_VERSION = "v1.1.0"

  }

  stages {
    stage('Build docker image or set image tag') {
      agent {
            kubernetes {
              inheritFrom 'scm-agent-pod-' + env.BUILD_NUMBER
              yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: ose-build
    image: eclipseopenpass/ubuntu:base
    tty: true
    resources:
      limits:
        memory: "16Gi"
        cpu: "4"
      requests:
        memory: "16Gi"
        cpu: "4"
  - name: jnlp
    volumeMounts:
    - name: volume-known-hosts
      mountPath: /home/jenkins/.ssh
  volumes:
  - name: volume-known-hosts
    configMap:
      name: known-hosts
"""
            }
          }
      stages {
        stage('Build docker image') {
          when {
            expression {
              return params.BUILD_DOCKER_IMAGE
            }
          }
          steps {
            script {
              env.IMAGE_TAG = "v0.1"
            }
            build job: 'Docker-build', parameters: [string(name: 'IMAGE_NAME', value: "${env.IMAGE_NAME}"), 
                                                    string(name: 'IMAGE_TAG', value:"${env.IMAGE_TAG}"),
                                                    string(name: 'REPO_URL', value: "${env.REPO_URL}"), 
                                                    string(name: 'BRANCH_NAME', value: "${env.GIT_BRANCH}"), 
                                                    string(name: 'DOCKERFILE_PATH', value: "${env.DOCKERFILE_PATH}")],
                                       propagate: true
          }
        }
        stage('Set image tag') {
          when {
            expression {
              return !params.BUILD_DOCKER_IMAGE
            }
          }
          steps {
            script {
              env.IMAGE_TAG = "v0.1"
            }
          }
        }
      }
    } 
    stage('SCM Linux build')
    {
      parallel
      {
        stage('Stage: E2E')
        {
          agent {
              kubernetes {
                inheritFrom 'scm-agent-pod-' + env.BUILD_NUMBER
                yaml """
          apiVersion: v1
          kind: Pod
          spec:
            containers:
            - name: e2etest-build
              image: "${env.IMAGE_NAME}:${env.IMAGE_TAG}"
              tty: true
              resources:
                limits:
                  memory: "8Gi"
                  cpu: "2"
                requests:
                  memory: "8Gi"
                  cpu: "2"
            - name: jnlp
              volumeMounts:
              - name: volume-known-hosts
                mountPath: /home/jenkins/.ssh
            volumes:
            - name: volume-known-hosts
              configMap:
                name: known-hosts
          """
                      }
                    }
          stages
          {
            stage('End2End-Tests (Prepare and run)')
             {
              steps
              {
                container('e2etest-build')
                {
                   sh 'bash repo/utils/ci/scripts/10_prepare.sh'
                   sh 'bash repo/utils/ci/scripts/12_download-openpass-artifacts.sh'
                   sh 'bash repo/utils/ci/scripts/11_prepare-openpass.sh'
                   sh 'bash repo/utils/ci/scripts/15_prepare-thirdParty.sh'
                   sh 'bash repo/utils/ci/scripts/21_configure-integration.sh'
                   sh 'bash repo/utils/ci/scripts/30_build.sh'
                   sh 'bash repo/utils/ci/scripts/51_e2e-test.sh'
                }
              }
               post
               {
                 always
                 {
                    junit allowEmptyResults: true, testResults: 'dist/pyOpenPASS/result_*.xml'
                    sh 'bash repo/utils/ci/scripts/60_pack_artifacts.sh'
                    archiveArtifacts allowEmptyArchive: true, artifacts: 'artifacts/**', followSymlinks: false
                 }
               }
            }
          }
        } 
        stage('Stage: Unittests')
        {
          agent {
                  kubernetes {
                    inheritFrom 'scm-agent-pod-' + env.BUILD_NUMBER
                    yaml """
              apiVersion: v1
              kind: Pod
              spec:
                containers:
                - name: unittest-build
                  image: "${env.IMAGE_NAME}:${env.IMAGE_TAG}"
                  tty: true
                  resources:
                    limits:
                      memory: "8Gi"
                      cpu: "2"
                    requests:
                      memory: "8Gi"
                      cpu: "2"
                - name: jnlp
                  volumeMounts:
                  - name: volume-known-hosts
                    mountPath: /home/jenkins/.ssh
                volumes:
                - name: volume-known-hosts
                  configMap:
                    name: known-hosts
              """
                          }
                        }
          stages
          {
            stage('Unittests') 
            {
              steps
              {
                container('unittest-build')
                {
                  sh 'bash repo/utils/ci/scripts/10_prepare.sh'
                  sh 'bash repo/utils/ci/scripts/15_prepare-thirdParty.sh'
                  sh 'bash repo/utils/ci/scripts/20_configure.sh'
                  sh 'bash repo/utils/ci/scripts/30_build.sh'
                  sh 'bash repo/utils/ci/scripts/50_test.sh'
                  withCredentials([string(credentialsId: 'gitlab-api-token', variable: 'GITLAB_BOT_TOKEN')])
                  {
                    sh 'bash repo/utils/ci/scripts/61_pack_scm_artifacts.sh'
                  }
                }
              }
              post
              {
                always
                {
                  junit allowEmptyResults: true, testResults: 'build/**/*Tests.xml'
                  archiveArtifacts allowEmptyArchive: true, artifacts: 'dist/**', followSymlinks: false

                  emailext attachLog: (currentBuild.currentResult == 'FAILURE') ? true : false,
                    subject: "Jenkins ${env.JOB_BASE_NAME} ${STAGE_NAME} [${currentBuild.currentResult}] - ${env.BRANCH_NAME} #${env.BUILD_NUMBER}",
                    body: "More info at: ${env.BUILD_URL}",
                    recipientProviders: [[$class: 'DevelopersRecipientProvider']]
                }
              }
            }
            stage ('Deploy') 
            {
              when {
                anyOf {
                  tag pattern: "v.*", comparator: "REGEXP"
                  branch "main"
                }
              }
              steps {
                container('jnlp') {
                  sshagent ( ['projects-storage.eclipse.org-bot-ssh']) {
                    sh 'bash repo/utils/ci/scripts/70_publish_artifacts.sh'
                  }
                }
              }
            }
          }
        }
      }
    }
  } 
}
