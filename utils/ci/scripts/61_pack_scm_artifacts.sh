#!/bin/bash

################################################################################
# Copyright (c) 2023 - 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script packs the artifacts
################################################################################

cd "${WORKSPACE}" || exit 1

DOC_VERSION=$(cat repo/doc/source/version.txt)
ARTIFACT_PATH=dist/SCM_FMU
DOC_PATH=dist/doc
SOURCES=dist/scm/AlgorithmScm.fmu
DOC_SOURCES=dist/scm/doc

if [[ "${OSTYPE}" = "msys" ]]; then
zip -q -r -9 ${ARTIFACT_PATH}.zip ${SOURCES}

else
tar -czf ${ARTIFACT_PATH}.tar.gz ${SOURCES}
tar -czf ${DOC_PATH}_v${DOC_VERSION}.tar.gz ${DOC_SOURCES}

fi

exit 0
