################################################################################
# Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building units with Conan
################################################################################

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain
from conan.tools.files import rm, export_conandata_patches, apply_conandata_patches
from conan.tools.scm import Git

required_conan_version = ">=1.47.0"

class UnitsConan(ConanFile):
    name = "units"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],
               "fPIC": [True, False],
               "units_Tests": [True, False]}
    default_options = {"shared": False,
                       "fPIC": True,
                       "units_Tests": False}
    exports_sources = "*"
    no_copy_source = False
    short_paths = True

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def export_sources(self):
        export_conandata_patches(self)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.cache_variables["BUILD_TESTS"] = self.options.units_Tests
        tc.generate()

    def _get_url_sha(self):
        if "CommitID" in self.version:
            url = self.conan_data["sources"][self.version.split("_")[0]]["url"]
            sha256 = self.version.split("_")[1]
        else:
            url = self.conan_data["sources"][self.version]["url"]
            sha256 = self.conan_data["sources"][self.version]["sha256"]
        return url, sha256

    def source(self):
        url, sha256 = self._get_url_sha()
        git = Git(self)
        git.clone(url=url, target=self.name)
        git.folder=self.name
        git.checkout(commit=sha256)

    def build(self):
        apply_conandata_patches(self)
        cmake = CMake(self)
        cmake.configure(build_script_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
        rm(self, "conanbuildinfo.txt", self.package_folder)
        rm(self, "conandata.yml", self.package_folder)
