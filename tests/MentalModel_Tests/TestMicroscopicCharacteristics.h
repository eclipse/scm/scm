/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/MicroscopicCharacteristics.h"
#include "module/driver/src/ScmDefinitions.h"

class TestMicroscopicCharacteristics : public MicroscopicCharacteristics
{
public:
  TestMicroscopicCharacteristics();
  void SetExtrapolatedRelativeNetDistance(AreaOfInterest aoi, units::length::meter_t distance);
  void SetVisible(AreaOfInterest aoi, bool visible);
  void SetExtrapolatedGap(AreaOfInterest aoi, units::time::second_t gap);
  void SetAbsoluteVelocity(AreaOfInterest aoi, units::velocity::meters_per_second_t velocity);
  void SetLongitudinalVelocity(AreaOfInterest aoi, units::velocity::meters_per_second_t velocity);
  void SetExtrapolatedA(AreaOfInterest aoi, units::acceleration::meters_per_second_squared_t acceleration);
  void SetVTargetEgo(units::velocity::meters_per_second_t velocity);
  void SetCurrentSituation(Situation situation);
  void SetAccelerationEgo(units::acceleration::meters_per_second_squared_t acc);
  void SetLongitudinalVelocityEgo(units::velocity::meters_per_second_t vel);
  void SetOwnVehicleData(OwnVehicleInformationScmExtended* data);
  void SetEgoVelocity(units::velocity::meters_per_second_t velocity, bool isEstimatedValue);
  void SetSurroundingObjectsData(SurroundingObjectsScmExtended* data);

  SurroundingObjectsScmExtended* GetSurroundingObjectTest();
  OwnVehicleInformationSCM* GetOwnVehicleInfoTest();
  OwnVehicleInformationScmExtended* GetOwnVehicleInfoSCMTest();
};

class TestMicroscopicCharacteristics2 : public MicroscopicCharacteristics
{
public:
  TestMicroscopicCharacteristics2();
  void SetSurroundingObjectsData(SurroundingObjectsScmExtended* data);
  MOCK_CONST_METHOD1(GetSideObjectVector, std::vector<ObjectInformationScmExtended>*(AreaOfInterest aoi));
};
