/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../module/longitudinalController/include/ScmLongitudinalControllerFactory.h"
#include "Fakes/FakeStochastics.h"

using ::testing::NiceMock;
using ::testing::NotNull;

/****************
 * CHECK Create *
 ****************/

TEST(ScmLongitudinalControllerFactory_Create, Check_Create)
{
  NiceMock<FakeStochastics> fakeStochastics;
  auto result = scm::longitudinal_controller::factory::Create(&fakeStochastics);

  ASSERT_THAT(result, NotNull());
}