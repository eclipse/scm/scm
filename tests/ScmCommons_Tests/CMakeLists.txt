################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME ScmCommons_Tests)
set(DRIVER_DIR ${ROOT_DIR}/module/driver)
set(PARAMETER_DIR ${ROOT_DIR}/module/parameterParser)
set(SENSOR_DIR ${ROOT_DIR}/module/sensor)

add_scm_target(
  NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
  DEFAULT_MAIN
  LINKOSI

  SOURCES
    UnittestsScmCommons.cpp
    ${DRIVER_DIR}/src/ScmCommons.cpp

  HEADERS
    ${DRIVER_DIR}/src/ScmCommons.h

 

  INCDIRS
  ${DRIVER_DIR}
  ${DRIVER_DIR}/src
  ${DRIVER_DIR}/src/HighCognitive
  ${PARAMETER_DIR}/src
  ${PARAMETER_DIR}/src/Signals
  ${SENSOR_DIR}/src/Signals
  ${ROOT_DIR}/include/signal

  LIBRARIES
  Stochastics::Stochastics
)

target_compile_definitions(${COMPONENT_TEST_NAME} PRIVATE
  PARAMETERS_SCM_TEST
  PARAMETERS_VEHICLE_TEST
  SCM_TESTING
  TESTING_ENABLED
)