/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "Merging/MergingInterface.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

/************************************************************************
 * Mock class for testing functions and classes that depend on Merging. *
 ************************************************************************/

class FakeMerging : public MergingInterface
{
public:
  MOCK_METHOD1(DrawNewAccelerationLimits, void(StochasticsInterface* stochastics));
  MOCK_CONST_METHOD0(GetDecelerationLimitForMerging, units::acceleration::meters_per_second_squared_t());
  MOCK_CONST_METHOD0(GetAccelerationLimitForMerging, units::acceleration::meters_per_second_squared_t());
  MOCK_METHOD2(RetrieveMergeGap, Merge::Gap(RelativeLane relativeLane, const std::vector<const SurroundingVehicleInterface *> &surroundingVehicles));
};