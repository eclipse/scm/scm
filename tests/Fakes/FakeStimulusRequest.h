/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "GazeControl/StimulusRequest.h"

/*******************************************************************************
 * Mock class for testing functions and classes that depend on StimulusRequest *
 *******************************************************************************/

class FakeStimulusRequest : public StimulusRequestInterface
{
public:
  MOCK_METHOD4(UpdateStimulusDataFront, std::map<GazeState, double>(const std::map<GazeState, double>& gazeStateIntensities, const std::vector<int>& knownObjectIds, SurroundingObjectsSCM& surroundingObjects, const OwnVehicleInformationSCM& ownVehicleInformationSCM));
  MOCK_METHOD4(UpdateStimulusDataSide, std::map<GazeState, double>(const std::map<GazeState, double>& gazeStateIntensities, SurroundingObjectsSCM& surroundingObjects, const std::vector<int>& knownObjectIds, const OwnVehicleInformationSCM& ownVehicleInformationSCM));
  MOCK_METHOD2(UpdateStimulusDataAcoustic, std::map<GazeState, double>(const std::map<GazeState, double>& gazeStateIntensities, const AuditoryPerceptionInterface& auditoryPerception));
  MOCK_METHOD2(UpdateStimulusDataOptical, std::map<GazeState, double>(const std::map<GazeState, double>& gazeStateIntensities, const std::vector<AdasHmiSignal> opticAdasSignals));
  MOCK_METHOD3(UpdateStimulusDataCombinedSignal, std::map<GazeState, double>(std::map<GazeState, double>& gazeStateIntensities, const AdasHmiSignal& combinedSignal, GazeControlInterface& gazeControl));
};