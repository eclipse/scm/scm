/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "HighCognitive/HighCognitiveInterface.h"

/*****************************************************************************
 * Mock class for testing functions and classes that depend on HighCognitive *
 *****************************************************************************/

class FakeHighCognitive : public HighCognitiveInterface
{
public:
  MOCK_METHOD0(RightFrontAndRightFrontFarAvailable, bool());
  MOCK_METHOD2(AdvanceHighCognitive, void(int time, bool externalControlActive));
  MOCK_METHOD1(AnticipateLaneChangeProbability, double(int time));
  MOCK_CONST_METHOD1(GetCurrentSituation, HighCognitiveSituation(int time));
};