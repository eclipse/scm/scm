/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "module/driver/src/MentalCalculationsInterface.h"
#include "module/driver/src/MentalModelInterface.h"
#include "module/driver/src/ScmDefinitions.h"
#include "module/driver/src/SurroundingVehicles/SurroundingVehicleInterface.h"

/***********************************************************************************
 * Mock class for testing functions and classes that depend on mental calculations. *
 ************************************************************************************/

class FakeMentalCalculations : public MentalCalculationsInterface
{
public:
  MOCK_METHOD3(Initialize, void(MentalModelInterface* mentalModel, SurroundingVehicleQueryFactoryInterface* queryFactory, const LongitudinalCalculationsInterface* longitudinalCalculation));
  MOCK_METHOD2(TransitionOfEgoVehicle, void(LaneChangeState, bool));
  MOCK_CONST_METHOD2(GetTimeHeadwayBetweenTwoAreasOfInterest, units::time::second_t(AreaOfInterest, AreaOfInterest));
  MOCK_CONST_METHOD1(GetTauDotEq, double(AreaOfInterest));
  MOCK_CONST_METHOD0(CalculateDeltaVelocityWish, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD2(CalculateVLegalInfluencigDistance, units::length::meter_t(units::velocity::meters_per_second_t, units::velocity::meters_per_second_t));
  MOCK_CONST_METHOD1(CalculateVLegal, std::vector<LaneInformationTrafficRulesScmExtended>(bool));
  MOCK_CONST_METHOD0(VReasonPassingSlowPlatoon, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD1(GetIntensityForNeedToChangeLane, double(AreaOfInterest));
  MOCK_CONST_METHOD1(GetIntensityForSlowerLeadingVehicle, double(AreaOfInterest));
  MOCK_CONST_METHOD4(GetMinDistance, units::length::meter_t(AreaOfInterest, MinThwPerspective perspective, AreaOfInterest, units::velocity::meters_per_second_t));
  MOCK_CONST_METHOD3(GetMinDistance, units::length::meter_t(const SurroundingVehicleInterface* vehicle, MinThwPerspective perspective, units::velocity::meters_per_second_t));
  MOCK_CONST_METHOD2(GetEqDistance, units::length::meter_t(AreaOfInterest, AreaOfInterest));
  MOCK_CONST_METHOD1(GetEqDistance, units::length::meter_t(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD2(GetInfDistance, units::length::meter_t(AreaOfInterest, AreaOfInterest));
  MOCK_CONST_METHOD1(GetInfluencingDistance, units::length::meter_t(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD2(GetDistanceToPointOfNoReturnForBrakingToEndOfLane, units::length::meter_t(units::velocity::meters_per_second_t, units::length::meter_t));
  MOCK_CONST_METHOD2(GetDistanceToPointOfNoReturnForBrakingToEndOfLane, units::length::meter_t(units::velocity::meters_per_second_t, RelativeLane));
  MOCK_CONST_METHOD0(GetDistanceToPointOfNoReturnForBrakingToEndOfExit, double());
  MOCK_CONST_METHOD0(GetUrgencyFactorForLaneChange, double());
  MOCK_CONST_METHOD0(GetUrgencyFactorForAcceleration, double());
  MOCK_CONST_METHOD3(GetPassingTime, double(AreaOfInterest, double, int));
  MOCK_CONST_METHOD2(PerceivedTimeToLaneCrossing, double(AreaOfInterest, int));
  MOCK_CONST_METHOD2(AnticipatedTimeToLaneCrossing, double(AreaOfInterest, int));
  MOCK_CONST_METHOD1(GetLaneMarkingAtAoi, scm::LaneMarking::Entity(AreaOfInterest));
  MOCK_CONST_METHOD2(GetLaneMarkingAtDistance, scm::LaneMarking::Entity(units::length::meter_t, Side));
  MOCK_CONST_METHOD2(GetSwervingTimes, std::unordered_map<LateralAction, double, LateralActionHash>(AreaOfInterest, bool));
  MOCK_CONST_METHOD2(InterpreteBrokenBoldLaneMarking, scm::LaneMarking::Entity(units::length::meter_t, Side));
  MOCK_CONST_METHOD2(DetermineUrgencyFactorForNextExit, double(bool, const LaneChangeDimensionInterface&));
  MOCK_CONST_METHOD4(CalculateReachedDistanceForUniformlyAcceleratedMotion, units::length::meter_t(units::time::second_t, units::acceleration::meters_per_second_squared_t, units::velocity::meters_per_second_t, units::length::meter_t));
  MOCK_CONST_METHOD3(CalculateReachedVelocityForUniformlyAcceleratedMotion, units::velocity::meters_per_second_t(units::time::second_t, units::acceleration::meters_per_second_squared_t, units::velocity::meters_per_second_t));
  MOCK_CONST_METHOD3(CalculateNecessaryTimeForCertainDistanceChangeInUniformlyAcceleratedMotion, units::time::second_t(units::acceleration::meters_per_second_squared_t, units::velocity::meters_per_second_t, units::length::meter_t));
  MOCK_CONST_METHOD2(CalculateNecessaryTimeForCertainVelocityChangeInUniformlyAcceleratedMotion, units::time::second_t(units::acceleration::meters_per_second_squared_t, units::velocity::meters_per_second_t));
  MOCK_CONST_METHOD4(DetermineLateralDistanceToEvade, double(AreaOfInterest, Side, bool, int));
  MOCK_CONST_METHOD2(DetermineLateralDistanceToEvadeComfort, double(AreaOfInterest, Side));
  MOCK_CONST_METHOD0(CalculateLateralOffsetNeutralPositionScaled, units::length::meter_t());
  MOCK_CONST_METHOD2(CalculateAccelerationToDefuse, units::acceleration::meters_per_second_squared_t(AreaOfInterest, units::acceleration::meters_per_second_squared_t));
  MOCK_CONST_METHOD1(CalculateAccelerationToDefuseLateral, units::acceleration::meters_per_second_squared_t(AreaOfInterest));
  MOCK_CONST_METHOD0(EgoBelowJamSpeed, bool());
  MOCK_CONST_METHOD0(GetLaneChangeBehavior, LaneChangeBehaviorInterface&());
  MOCK_CONST_METHOD2(IsApproaching, bool(AreaOfInterest, AreaOfInterest));
  MOCK_CONST_METHOD0(EstimatedTimeToLeaveLane, units::time::second_t());
  MOCK_CONST_METHOD2(DetermineTimeToSwervePast, int(AreaOfInterest, int));
  MOCK_CONST_METHOD3(CalculateFollowingAcceleration, units::acceleration::meters_per_second_squared_t(const SurroundingVehicleInterface&, units::acceleration::meters_per_second_squared_t, units::length::meter_t));
};
