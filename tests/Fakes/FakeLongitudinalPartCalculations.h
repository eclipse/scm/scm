/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "module/driver/src/LongitudinalCalculations/LongitudinalPartCalculationsInterface.h"

/*****************************************************************************************
 * Mock class for testing functions and classes that depend on LongitudinalCalculations. *
 *****************************************************************************************/

class FakeLongitudinalPartCalculations : public LongitudinalPartCalculationsInterface
{
  MOCK_METHOD4(DetermineSecureDistanceData, SecureDistanceData(const SurroundingVehicleInterface& observedVehicle, const OwnVehicleInformationScmExtended& egoVehicle, MinThwPerspective perspective, units::velocity::meters_per_second_t vGap));
  MOCK_METHOD3(DetermineSecureDistanceData, SecureDistanceData(const SurroundingVehicleInterface& observedVehicle, const SurroundingVehicleInterface& referenceVehicle, units::velocity::meters_per_second_t vGap));
  MOCK_METHOD3(DistanceToBrakeLeadingVehicle, units::length::meter_t(units::velocity::meters_per_second_t anticipatedVelocityEnd, units::acceleration::meters_per_second_squared_t anticipatedAcceleration, units::velocity::meters_per_second_t velocity));
  MOCK_METHOD5(DistanceToBrakeFollowingVehicle, units::length::meter_t(units::velocity::meters_per_second_t anticipatedVelocityEnd, units::acceleration::meters_per_second_squared_t anticipatedAcceleration, units::acceleration::meters_per_second_squared_t currentAccRear, units::velocity::meters_per_second_t velocity, units::time::second_t reactionTime));
  MOCK_METHOD2(CalculateInsecurityDistance, units::length::meter_t(const SurroundingVehicleInterface& observedVehicle, const SurroundingVehicleInterface& referenceVehicle));
  MOCK_METHOD1(CalculateInsecurityDistance, units::length::meter_t(const SurroundingVehicleInterface& vehicle));
  MOCK_METHOD2(DetermineScalingFactorForFollowingDistance, double(const SurroundingVehicleInterface& observedVehicle, const SurroundingVehicleInterface& referenceVehicle));
  MOCK_METHOD1(DetermineScalingFactorForFollowingDistance, double(const SurroundingVehicleInterface& vehicle));
  MOCK_METHOD1(DetermineTtcBasedScalingFactorForFollowingDistance, double(units::time::second_t ttc));
  MOCK_METHOD3(ExecuteStevensPowLaw, units::length::meter_t(units::length::meter_t deltaDistance, double scalingFactorFollowingDistance, double proportionalityFactorForFollowingDistance));
  MOCK_METHOD2(CalculateVelocityDelta, units::velocity::meters_per_second_t(const SurroundingVehicleInterface& observedVehicle, const OwnVehicleInformationScmExtended& egoVehicle));
};