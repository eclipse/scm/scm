/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "module/driver/src/LongitudinalCalculations/LongitudinalCalculationsInterface.h"

/*****************************************************************************************
 * Mock class for testing functions and classes that depend on LongitudinalCalculations. *
 *****************************************************************************************/

class FakeLongitudinalCalculations : public LongitudinalCalculationsInterface
{
public:
  MOCK_CONST_METHOD8(CalculateSecureNetDistance, units::length::meter_t(const SurroundingVehicleInterface& observedVehicle, const SurroundingVehicleInterface& referenceVehicle, units::acceleration::meters_per_second_squared_t anticipAccFront, units::velocity::meters_per_second_t anticipVelEnd, units::time::second_t reactionTime, units::acceleration::meters_per_second_squared_t anticipAccRear, units::length::meter_t deltaDistEnd, units::velocity::meters_per_second_t vGap));
  MOCK_CONST_METHOD9(CalculateSecureNetDistance, units::length::meter_t(const SurroundingVehicleInterface& observedVehicle, const OwnVehicleInformationScmExtended& egoVehicle, units::acceleration::meters_per_second_squared_t anticipAccFront, units::velocity::meters_per_second_t anticipVelEnd, units::time::second_t reactionTime, units::acceleration::meters_per_second_squared_t anticipAccRear, units::length::meter_t deltaDistEnd, MinThwPerspective perspective, units::velocity::meters_per_second_t vGap));
  MOCK_CONST_METHOD2(CalculateVelocityDelta, units::velocity::meters_per_second_t(const SurroundingVehicleInterface& observedVehicle, const OwnVehicleInformationScmExtended& egoVehicle));
};