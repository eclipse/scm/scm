/********************************************************************************
 * Copyright (c) 2019-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/InfrastructureCharacteristicsInterface.h"
#include "module/driver/src/ScmDefinitions.h"

using ::testing::_;

class FakeInfrastructureCharacteristics : public InfrastructureCharacteristicsInterface
{
public:
  MOCK_METHOD1(SetLaneMarkingTypeLeftLast, void(scm::LaneMarking::Type laneTypeLeftLast));
  MOCK_METHOD3(Initialize, void(units::time::millisecond_t cycleTime, TrafficRuleInformationScmExtended* trafficRuleInformation, GeometryInformationSCM* geometryInformation));
  MOCK_CONST_METHOD0(GetLaneMarkingTypeLeftLast, scm::LaneMarking::Type());
  MOCK_METHOD1(SetLaneMarkingTypeRightLast, void(scm::LaneMarking::Type laneTypeRightLast));
  MOCK_CONST_METHOD0(GetLaneMarkingTypeRightLast, scm::LaneMarking::Type());
  MOCK_METHOD1(SetTrafficRuleInformation, void(TrafficRuleInformationSCM* trafficRuleInformation));
  MOCK_METHOD4(SetGeometryInformation, void(GeometryInformationSCM* geometryInformation, units::velocity::meters_per_second_t velocityEgoEstimated, bool takeNextExit, int laneIdEgo));
  MOCK_CONST_METHOD1(GetLaneInformationTrafficRules, const LaneInformationTrafficRulesScmExtended&(RelativeLane relativeLane));
  MOCK_CONST_METHOD0(GetTrafficRuleInformation, TrafficRuleInformationScmExtended*());
  MOCK_CONST_METHOD0(GetGeometryInformation, GeometryInformationSCM*());
  MOCK_CONST_METHOD1(GetLaneInformationGeometry, const LaneInformationGeometrySCM&(RelativeLane relativeLane));
  MOCK_METHOD1(UpdateLaneInformationTrafficRules, LaneInformationTrafficRulesScmExtended*(RelativeLane relativeLane));
  MOCK_METHOD0(UpdateTrafficRuleInformation, TrafficRuleInformationScmExtended*());
  MOCK_METHOD1(UpdateLaneInformationTrafficRules, LaneInformationTrafficRulesScmExtended*(AreaOfInterest aoi));
  MOCK_METHOD0(UpdateGeometryInformation, GeometryInformationSCM*());
  MOCK_METHOD1(UpdateLaneInformationGeometry, LaneInformationGeometrySCM*(RelativeLane relativeLane));
  MOCK_CONST_METHOD2(GetLaneExistence, bool(RelativeLane relativeLane, bool isEmergency));
  MOCK_CONST_METHOD2(CheckLaneExistence, bool(AreaOfInterest aoi, bool isEmergency));
};