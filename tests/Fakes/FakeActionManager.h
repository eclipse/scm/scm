/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "module/driver/src/ActionManagerInterface.h"
#include "module/driver/src/ScmDefinitions.h"

/******************************************************************************
 * Mock class for testing functions and classes that depend on ActionManager. *
 ******************************************************************************/

class FakeActionManager : public ActionManagerInterface
{
public:
  MOCK_METHOD0(RunLeadingVehicleSelector, void());
  MOCK_METHOD0(DetermineLongitudinalActionState, void());
  MOCK_METHOD0(DiscardLastActionPatternIntensities, void());
  MOCK_METHOD0(GetLateralActionLastTick, LateralAction());
  MOCK_CONST_METHOD2(DetermineLaneChangeLength, double(RelativeLane targetLane, Situation currentSituation));
  MOCK_CONST_METHOD0(GetActionSubStateLastTick, LongitudinalActionState());
  MOCK_METHOD1(SetActionSubStateLastTick, void(LongitudinalActionState actionSubStateLastTick));
  MOCK_METHOD1(SetLateralActionLastTick, void(LateralAction lastLateralAction));
  MOCK_METHOD0(GenerateIntensityAndSampleLateralAction, void());
};