/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "SurroundingVehicles/SurroundingVehicleInterface.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "module/driver/src/FeatureExtractorInterface.h"
#include "module/driver/src/ScmDefinitions.h"

/****************************************************************************
 * Mock class for testing functions and classes that depend on FeatureExtractor. *
 ****************************************************************************/

class FakeFeatureExtractor : public FeatureExtractorInterface
{
public:
  MOCK_CONST_METHOD1(IsVehicleInSideLane, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD2(IsMinimumFollowingDistanceViolated, bool(const SurroundingVehicleInterface* vehicle, double reductionFactorUrgency));
  MOCK_CONST_METHOD1(IsInfluencingDistanceViolated, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD1(IsChangingIntoEgoLane, bool(AreaOfInterest aoi));
  MOCK_CONST_METHOD1(IsUnfavorable, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD1(IsObstacle, bool(const SurroundingVehicleInterface*));
  MOCK_CONST_METHOD1(HasJamVelocity, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD0(HasJamVelocityEgo, bool());
  MOCK_CONST_METHOD0(IsDeceleratingDueToSuspiciousSideVehicles, bool());
  MOCK_CONST_METHOD0(IsAvoidingLaneNextToSuspiciousSideVehicles, bool());
  MOCK_CONST_METHOD2(IsNearEnoughForFollowing, bool(const SurroundingVehicleInterface& vehicle, double reductionFactor));
  MOCK_CONST_METHOD1(IsExceptionallySlow, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD1(HasSuspiciousBehaviour, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD1(HasIntentionalLaneCrossingConflict, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD1(HasActualLaneCrossingConflict, bool(const SurroundingVehicleInterface&));
  MOCK_CONST_METHOD1(HasAnticipatedLaneCrossingConflict, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD1(IsCollisionCourseDetected, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD1(IsSideLaneSafe, bool(Side side));
  MOCK_CONST_METHOD1(IsLaneChangeSafe, bool(bool isTransitionTrigger));
  MOCK_CONST_METHOD2(AbleToPass, bool(const SurroundingVehicleInterface* vehicle, units::velocity::meters_per_second_t deltaVelocity));
  MOCK_CONST_METHOD2(AnticipatedAbleToPass, bool(const SurroundingVehicleInterface* vehicle, units::velocity::meters_per_second_t deltaVelocity));
  MOCK_CONST_METHOD3(IsSwervingSafe, bool(double lateralEvadeDistance, Side side, bool checkSafety));
  MOCK_CONST_METHOD1(IsPlannedSwervingPossible, bool(AreaOfInterest aoi));
  MOCK_CONST_METHOD0(DeterminePossibilityOfEvadingBySwerving, bool());
  MOCK_CONST_METHOD0(HasSwervingTargetBeenPassed, bool());
  MOCK_CONST_METHOD0(IsOnEntryOrExitLane, bool());
  MOCK_CONST_METHOD0(IsOnEntryLane, bool());
  MOCK_CONST_METHOD0(IsEvadingEndOfLane, bool());
  MOCK_CONST_METHOD1(IsLaneDriveablePerceived, bool(RelativeLane relativeLane));
  MOCK_CONST_METHOD0(IsLaneDriveablePerceived, bool());
  MOCK_CONST_METHOD1(IsLaneTypeDriveable, bool(scm::LaneType type));
  MOCK_CONST_METHOD1(IsExceedingLateralMotionThresholdTowardsEgoLane, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD1(IsLaneChangePermittedDueToLaneMarkings, bool(Side side));
  MOCK_CONST_METHOD1(IsLaneChangePermittedDueToLaneMarkingsForAoi, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD2(CheckLaneChange, bool(Side side, bool isEmergecy));
  MOCK_CONST_METHOD2(DoesLaneMarkingBrokenBoldPreventLaneChange, bool(const scm::LaneMarking::Entity& laneMarking, Side side));
  MOCK_CONST_METHOD2(DoesOuterLaneOvertakingProhibitionApply, bool(AreaOfInterest aoiToEvaluate, bool rightHandTraffic));
  MOCK_CONST_METHOD3(IsLaneSafe, bool(AreaOfInterest aoi_front, AreaOfInterest aoi_side, AreaOfInterest aoi_rear));
  MOCK_CONST_METHOD2(IsCurrentLaneSafe, bool(AreaOfInterest aoi_front, AreaOfInterest aoi_rear));
  MOCK_CONST_METHOD3(TimeToLaneCrossingdouble, double(AreaOfInterest aoi, double vwToEgoLane, int sideAoiIndex));
  MOCK_CONST_METHOD2(PerceivedTimeToLaneCrossing, double(AreaOfInterest aoi, int sideAoiIndex));
  MOCK_CONST_METHOD2(AnticipatedTimeToLaneCrossing, double(AreaOfInterest aoi, int sideAoiIndex));
  MOCK_CONST_METHOD1(GetTimeAtTargetSpeedInLane, units::time::second_t(SurroundingLane lane));
  MOCK_CONST_METHOD3(TimeToLaneCrossing, double(AreaOfInterest aoi, double vwToEgoLane, int sideAoiIndex));
  MOCK_CONST_METHOD1(WillEgoLeaveLaneBeforeCollision, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD1(HasSideLaneNoCollisionCourses, bool(Side side));
  MOCK_CONST_METHOD0(IsRightLaneEmptyLaneConvenienceAndHigherThanEgo, bool());
  MOCK_CONST_METHOD1(DoesVehicleSurroundingFit, bool(AreaOfInterest aoiToEvaluate));
  MOCK_CONST_METHOD1(CanVehicleCauseCollisionCourseForNonSideAreas, bool(const SurroundingVehicleInterface* vehicle));
  MOCK_CONST_METHOD1(IsShoulderLaneUsageLegit, bool(RelativeLane));
  MOCK_CONST_METHOD2(HasDrivableSuccessor, bool(bool, RelativeLane));
};
