/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "module/driver/src/InformationAcquisitionInterface.h"

class FakeInformationAcquisition : public InformationAcquisitionInterface
{
public:
  MOCK_CONST_METHOD0(SetOwnVehicleHelperVariables, void());
  MOCK_CONST_METHOD2(UpdateSurroundingVehicleData, void(bool forceUpdate, bool isNewInformationAcquisitionRequested));
  MOCK_METHOD1(UpdateOwnVehicleData, void(OwnVehicleInformationSCM* ownVehicle_GroundTruth));
  MOCK_METHOD1(ExtrapolateOwnVehicleData, void(OwnVehicleInformationSCM* ownVehicle_GroundTruth));
  MOCK_METHOD1(UpdateEgoVehicleData, void(bool forceUpdate));
  MOCK_CONST_METHOD1(ResetAoiDataForNoObject, void(ObjectInformationScmExtended*));
  MOCK_CONST_METHOD3(SetAoiDataWithGroundTruth, void(ObjectInformationScmExtended* surroundingObject, const ObjectInformationSCM* surroundingObject_GroundTruth, AreaOfInterest aoi));
  MOCK_CONST_METHOD1(PerceiveSurroundingVehicles, std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>>(bool idealPerception));
  MOCK_CONST_METHOD2(CorrectAoiDataDueToLongitudinalTransitionFromSideArea, void(ObjectInformationScmExtended*, AreaOfInterest));
  MOCK_CONST_METHOD1(CorrectAoiDataDueToLongitudinalTransitionToSideArea, void(ObjectInformationScmExtended*));
  MOCK_CONST_METHOD0(GetVisibleAgentIds, std::vector<int>());
  MOCK_CONST_METHOD1(IsOutOfSight, bool(units::length::meter_t relativeLongitudinalDistance));
  MOCK_CONST_METHOD0(GetCycleTime, units::time::millisecond_t());
  MOCK_CONST_METHOD0(GetTime, units::time::millisecond_t());
};