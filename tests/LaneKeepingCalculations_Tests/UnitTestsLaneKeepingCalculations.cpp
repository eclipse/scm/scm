/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeMentalModel.h"
#include "module/driver/src/LaneKeepingCalculations.h"
#include "module/driver/src/LateralAction.h"

using ::testing::_;
using ::testing::An;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

/*******************************************
 * CHECK ChangingRightAppropriate  is true *
 ******************************************/

TEST(LaneKeepingCalculations, ChangingRightAppropriateIsTrue)
{
  NiceMock<FakeMentalModel> fakeMentalModel;
  LateralAction lateral{LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT};

  const LateralActionQuery lateralAction{lateral};
  LaneKeepingCalculations lane(fakeMentalModel, lateralAction);

  ON_CALL(fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(false));
  ON_CALL(fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(false));
  ON_CALL(fakeMentalModel, GetIsEndOfLateralMovement()).WillByDefault(Return(false));

  // Run Test
  ASSERT_EQ(lane.ChangingRightAppropriate(), true);
}

/***********************************************************
 * CHECK ChangingRightAppropriate is false with Parameters *
 ***********************************************************/

struct DataFor_ChangingRightAppropriate
{
  LateralAction lateral;
  bool input_hasRightLane;
  bool input_isInTargetLane;
  bool input_isEndOfLateralMovement;
  bool result_isChangingRightAppropriate;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_ChangingRightAppropriate& obj)
  {
    return os
           << "  LateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.lateral)
           << "  input_hasRightLane (bool): " << ScmCommons::BooleanToString(obj.input_hasRightLane)
           << "  input_isInTargetLane (bool): " << ScmCommons::BooleanToString(obj.input_isInTargetLane)
           << "  input_isEndOfLateralMovement (bool): " << ScmCommons::BooleanToString(obj.input_isEndOfLateralMovement)
           << "| result_isChangingRightAppropriate (bool): " << obj.result_isChangingRightAppropriate;
  }
};

class LaneKeepingCalculationsRight : public ::testing::Test,
                                     public ::testing::WithParamInterface<DataFor_ChangingRightAppropriate>
{
};

TEST_P(LaneKeepingCalculationsRight, ChangingRightAppropriateIsFalse)
{
  NiceMock<FakeMentalModel> fakeMentalModel;
  DataFor_ChangingRightAppropriate data = GetParam();

  LateralAction lateral{data.lateral.state, data.lateral.direction};

  const LateralActionQuery lateralAction{lateral};
  LaneKeepingCalculations lane(fakeMentalModel, lateralAction);

  ON_CALL(fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(data.input_hasRightLane));
  ON_CALL(fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(data.input_isInTargetLane));
  ON_CALL(fakeMentalModel, GetIsEndOfLateralMovement()).WillByDefault(Return(data.input_isEndOfLateralMovement));

  // Run Test
  ASSERT_EQ(lane.ChangingRightAppropriate(), data.result_isChangingRightAppropriate);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneKeepingCalculationsRight,
    testing::Values(
        DataFor_ChangingRightAppropriate{
            .lateral = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::RIGHT),
            .input_hasRightLane = true,
            .input_isInTargetLane = false,
            .input_isEndOfLateralMovement = false,
            .result_isChangingRightAppropriate = false},
        DataFor_ChangingRightAppropriate{
            .lateral = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .input_hasRightLane = false,
            .input_isInTargetLane = true,
            .input_isEndOfLateralMovement = false,
            .result_isChangingRightAppropriate = false},
        DataFor_ChangingRightAppropriate{
            .lateral = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT),
            .input_hasRightLane = false,
            .input_isInTargetLane = false,
            .input_isEndOfLateralMovement = true,
            .result_isChangingRightAppropriate = false}));

/*******************************************
 * CHECK ChangingLeftAppropriate is true   *
 ******************************************/

TEST(LaneKeepingCalculations, ChangingLeftAppropriateIsTrue)
{
  NiceMock<FakeMentalModel> fakeMentalModel;

  LateralAction lateral{LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT};
  const LateralActionQuery lateralAction{lateral};
  LaneKeepingCalculations lane(fakeMentalModel, lateralAction);

  ON_CALL(fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(false));
  ON_CALL(fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(false));
  ON_CALL(fakeMentalModel, GetIsEndOfLateralMovement()).WillByDefault(Return(false));

  // Run Test
  ASSERT_EQ(lane.ChangingLeftAppropriate(), true);
}

/***********************************************************
 * CHECK ChangingLeftAppropriate is false with Parameters  *
 ***********************************************************/

struct DataFor_ChangingLeftAppropriate
{
  LateralAction lateral;
  bool input_hasLeftLane;
  bool input_isInTargetLane;
  bool input_isEndOfLateralMovement;
  bool result_isChangingLeftAppropriate;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_ChangingLeftAppropriate& obj)
  {
    return os
           << "  LateralAction (LateralAction): " << ScmCommons::LateralActionToString(obj.lateral)
           << "  input_hasLeftLane (bool): " << ScmCommons::BooleanToString(obj.input_hasLeftLane)
           << "  input_isInTargetLane (bool): " << ScmCommons::BooleanToString(obj.input_isInTargetLane)
           << "  input_isEndOfLateralMovement (bool): " << ScmCommons::BooleanToString(obj.input_isEndOfLateralMovement)
           << "| result_isChangingLeftAppropriate (bool): " << obj.result_isChangingLeftAppropriate;
  }
};

class LaneKeepingCalculationsLeft : public ::testing::Test,
                                    public ::testing::WithParamInterface<DataFor_ChangingLeftAppropriate>
{
};

TEST_P(LaneKeepingCalculationsLeft, ChangingLeftAppropriateIsFalse)
{
  NiceMock<FakeMentalModel> fakeMentalModel;
  DataFor_ChangingLeftAppropriate data = GetParam();

  LateralAction lateral{data.lateral.state, data.lateral.direction};
  const LateralActionQuery lateralAction{lateral};
  LaneKeepingCalculations lane(fakeMentalModel, lateralAction);

  ON_CALL(fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(data.input_hasLeftLane));
  ON_CALL(fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(data.input_isInTargetLane));
  ON_CALL(fakeMentalModel, GetIsEndOfLateralMovement()).WillByDefault(Return(data.input_isEndOfLateralMovement));

  // Run Test
  ASSERT_EQ(lane.ChangingLeftAppropriate(), data.result_isChangingLeftAppropriate);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneKeepingCalculationsLeft,
    testing::Values(
        DataFor_ChangingLeftAppropriate{
            .lateral = LateralAction(LateralAction::State::LANE_KEEPING, LateralAction::Direction::LEFT),
            .input_hasLeftLane = true,
            .input_isInTargetLane = false,
            .input_isEndOfLateralMovement = false,
            .result_isChangingLeftAppropriate = false},
        DataFor_ChangingLeftAppropriate{
            .lateral = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .input_hasLeftLane = false,
            .input_isInTargetLane = true,
            .input_isEndOfLateralMovement = false,
            .result_isChangingLeftAppropriate = false},
        DataFor_ChangingLeftAppropriate{
            .lateral = LateralAction(LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT),
            .input_hasLeftLane = false,
            .input_isInTargetLane = false,
            .input_isEndOfLateralMovement = true,
            .result_isChangingLeftAppropriate = false}));

/******************************************
 * CHECK IsAtEndOfLateralMovement is true *
 ******************************************/

TEST(LaneKeepingCalculationsTrue, IsAtEndOfLateralMovement)
{
  NiceMock<FakeMentalModel> fakeMentalModel;

  LateralAction lateral{LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT};
  const LateralActionQuery lateralAction{lateral};
  LaneKeepingCalculations lane(fakeMentalModel, lateralAction);

  ON_CALL(fakeMentalModel, GetIsEndOfLateralMovement()).WillByDefault(Return(true));

  // Run Test
  ASSERT_EQ(lane.IsAtEndOfLateralMovement(), true);
}

/*******************************************
 * CHECK IsAtEndOfLateralMovement is false *
 *******************************************/

TEST(LaneKeepingCalculationsFalse, IsAtEndOfLateralMovement)
{
  NiceMock<FakeMentalModel> fakeMentalModel;

  LateralAction lateral{LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT};
  const LateralActionQuery lateralAction{lateral};
  LaneKeepingCalculations lane(fakeMentalModel, lateralAction);

  ON_CALL(fakeMentalModel, GetIsEndOfLateralMovement()).WillByDefault(Return(false));

  // Run Test
  ASSERT_EQ(lane.IsAtEndOfLateralMovement(), false);
}

/*******************************
 * CHECK IsObjectInTargetSide  *
 *******************************/

TEST(LaneKeepingCalculations, IsObjectInTargetSideLANE_CHANGEINGLEFT_TRUE)
{
  NiceMock<FakeMentalModel> fakeMentalModel;
  LateralAction lateral{LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT};
  const LateralActionQuery lateralAction{lateral};
  LaneKeepingCalculations lane(fakeMentalModel, lateralAction);

  ON_CALL(fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE, 0)).WillByDefault(Return(true));

  ASSERT_EQ(lane.IsObjectInTargetSide(), true);
}

TEST(LaneKeepingCalculations, IsObjectInTargetSideLANE_CHANGEINGLEFT_FALSE)
{
  NiceMock<FakeMentalModel> fakeMentalModel;
  LateralAction lateral{LateralAction::State::LANE_KEEPING, LateralAction::Direction::LEFT};
  const LateralActionQuery lateralAction{lateral};
  LaneKeepingCalculations lane(fakeMentalModel, lateralAction);

  ON_CALL(fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_SIDE, 0)).WillByDefault(Return(true));

  ASSERT_EQ(lane.IsObjectInTargetSide(), false);
}

TEST(LaneKeepingCalculations, IsObjectInTargetSideCHANGEINGRIGHT_TRUE)
{
  NiceMock<FakeMentalModel> fakeMentalModel;
  LateralAction lateral{LateralAction::State::LANE_CHANGING, LateralAction::Direction::RIGHT};
  const LateralActionQuery lateralAction{lateral};
  LaneKeepingCalculations lane(fakeMentalModel, lateralAction);

  ON_CALL(fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE, 0)).WillByDefault(Return(true));

  ASSERT_EQ(lane.IsObjectInTargetSide(), true);
}

TEST(LaneKeepingCalculations, IsObjectInTargetSideCHANGEINGRIGHT_FALSE)
{
  NiceMock<FakeMentalModel> fakeMentalModel;
  LateralAction lateral{LateralAction::State::LANE_KEEPING, LateralAction::Direction::RIGHT};
  const LateralActionQuery lateralAction{lateral};
  LaneKeepingCalculations lane(fakeMentalModel, lateralAction);

  ON_CALL(fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE, 0)).WillByDefault(Return(true));

  ASSERT_EQ(lane.IsObjectInTargetSide(), false);
}

/*************************************
 * CHECK EgoFasterThanDeltaSideTrue  *
 *************************************/

TEST(LaneKeepingCalculations, EgoFasterThanDeltaSideTrue)
{
  NiceMock<FakeMentalModel> fakeMentalModel;
  LateralAction lateral{LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT};
  const LateralActionQuery lateralAction{lateral};
  LaneKeepingCalculations lane(fakeMentalModel, lateralAction);

  ON_CALL(fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE, 0)).WillByDefault(Return(true));

  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_FRONT, _)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetTtc(AreaOfInterest::RIGHT_FRONT, _)).WillByDefault(Return(5._s));

  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_REAR, _)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetTtc(AreaOfInterest::RIGHT_REAR, _)).WillByDefault(Return(5._s));

  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetTtc(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(3._s));

  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::EGO_REAR, _)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetTtc(AreaOfInterest::EGO_REAR, _)).WillByDefault(Return(3._s));

  ASSERT_EQ(lane.EgoFasterThanDeltaSide(), true);
}

/**************************************
 * CHECK EgoFasterThanDeltaSideFalse  *
 *************************************/

TEST(LaneKeepingCalculations, EgoFasterThanDeltaSideFalse)
{
  NiceMock<FakeMentalModel> fakeMentalModel;
  LateralAction lateral{LateralAction::State::LANE_CHANGING, LateralAction::Direction::LEFT};
  const LateralActionQuery lateralAction{lateral};
  LaneKeepingCalculations lane(fakeMentalModel, lateralAction);

  ON_CALL(fakeMentalModel, GetIsLaneChangePastTransition()).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::LEFT_SIDE, 0)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_FRONT, _)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetTtc(AreaOfInterest::RIGHT_FRONT, _)).WillByDefault(Return(3._s));
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::RIGHT_REAR, _)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetTtc(AreaOfInterest::RIGHT_REAR, _)).WillByDefault(Return(3._s));
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetTtc(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(5._s));
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::EGO_REAR, _)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, GetTtc(AreaOfInterest::EGO_REAR, _)).WillByDefault(Return(5._s));

  ASSERT_EQ(lane.EgoFasterThanDeltaSide(), false);
}