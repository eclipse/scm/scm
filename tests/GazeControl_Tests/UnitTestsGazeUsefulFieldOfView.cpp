/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "../Fakes/FakeGazeFieldQuery.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "GazeControl/GazeUsefulFieldOfView.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/SensorDriverScmDefinitions.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

struct UsefulFieldOfViewTester
{
  class UsefulFieldOfViewUnderTest : GazeUsefulFieldOfView
  {
  public:
    template <typename... Args>
    UsefulFieldOfViewUnderTest(Args&&... args)
        : GazeUsefulFieldOfView{std::forward<Args>(args)...} {};

    using GazeUsefulFieldOfView::GenerateUfovForFrontAndSides;
    using GazeUsefulFieldOfView::GetUsefulFieldOfViewAois;
  };

  UsefulFieldOfViewTester()
      : fakeMentalModel{},
        fakeMicroscopicCharacteristics{},
        fakeGazeFieldQuery{},
        usefulFieldOfView(fakeMentalModel, fakeGazeFieldQuery)
  {
  }

  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeGazeFieldQuery> fakeGazeFieldQuery;
  UsefulFieldOfViewUnderTest usefulFieldOfView;
};

/********************************************************
 * CHECK GazeUsefulFieldOfView GetUsefulFieldOfViewAois *
 ********************************************************/

TEST(GazeUsefulFieldOfView_GetUsefulFieldOfViewAois, GetUsefulFieldOfViewAois)
{
  UsefulFieldOfViewTester TEST_HELPER;

  OwnVehicleInformationScmExtended mockInfo;
  mockInfo.fovea = AreaOfInterest::LEFT_REAR;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&mockInfo));

  ObjectInformationSCM egoFront;
  egoFront.id = 1;

  SurroundingObjectsSCM surroundingObjects;
  surroundingObjects.ongoingTraffic.Ahead().Close().push_back(egoFront);

  auto result = TEST_HELPER.usefulFieldOfView.GetUsefulFieldOfViewAois(surroundingObjects, 5.0_rad);

  ASSERT_EQ(result.size(), 2);
  ASSERT_EQ(result.at(0), AreaOfInterest::LEFTLEFT_REAR);
  ASSERT_EQ(result.at(1), AreaOfInterest::EGO_REAR);
}

/************************************************************
 * CHECK GazeUsefulFieldOfView GenerateUfovForFrontAndSides *
 ************************************************************/

TEST(GazeUsefulFieldOfView_GenerateUfovForFrontAndSides, GenerateUfovForFrontAndSides)
{
  UsefulFieldOfViewTester TEST_HELPER;

  ObjectInformationSCM objectEgoFront;
  objectEgoFront.id = 1;

  ObjectInformationSCM objectLeftFront;
  objectLeftFront.id = 2;

  std::map<AreaOfInterest, ObjectInformationSCM> aoiObjects;
  aoiObjects[AreaOfInterest::EGO_FRONT] = objectEgoFront;
  aoiObjects[AreaOfInterest::LEFT_FRONT] = objectLeftFront;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetInteriorHudExistence()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeGazeFieldQuery, FillSurroundingAoiObjects(_)).WillByDefault(Return(aoiObjects));
  std::vector<AreaOfInterest> ufov{AreaOfInterest::EGO_FRONT, AreaOfInterest::LEFT_FRONT};
  ON_CALL(TEST_HELPER.fakeGazeFieldQuery, UpdateAreaOfInterest(_, _, _, _, _)).WillByDefault(Return(ufov));

  OngoingTrafficObjectsSCM ongoingTraffic;
  ongoingTraffic.Ahead().Close().push_back(objectEgoFront);
  ongoingTraffic.Ahead().Left().push_back(objectLeftFront);
  auto result = TEST_HELPER.usefulFieldOfView.GenerateUfovForFrontAndSides(ufov, ongoingTraffic, 5.0_rad);

  ASSERT_EQ(result.size(), ufov.size());
}