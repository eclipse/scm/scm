/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <optional>
#include <ostream>

#include "../Fakes/FakeLeadingVehicleQuery.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "SurroundingVehicles/LeadingVehicleFilter.h"
#include "include/common/ScmDefinitions.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

/***************************
 * CHECK FilterRearVehicles *
 ****************************/

TEST(LeadingVehicleFilter_FilterRearVehicles, RemoveVehiclesBehindEgo)
{
  NiceMock<FakeSurroundingVehicle> keepVehicle;
  NiceMock<FakeSurroundingVehicle> removeVehicle;

  ON_CALL(keepVehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(removeVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(keepVehicle, BehindEgo()).WillByDefault(Return(false));
  ON_CALL(removeVehicle, BehindEgo()).WillByDefault(Return(true));
  const std::vector<const SurroundingVehicleInterface*> vehicles{&keepVehicle, &removeVehicle};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterRearVehicles(vehicles)};

  ASSERT_EQ(result.size(), 1);
  ASSERT_EQ(result.at(0)->GetId(), 0);
}

/*******************************
 * CHECK FilterSideSideVehicles *
 ********************************/

TEST(LeadingVehicleFilter_FilterSideSideVehicles, KeepVehiclesOnSideSideLaneOnlyIfEgoIsChangingLaneAndVehicleIsMovingTowardsEgoLane_Left)
{
  NiceMock<FakeSurroundingVehicle> keepVehicle;
  NiceMock<FakeSurroundingVehicle> removeVehicle;

  ON_CALL(keepVehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(removeVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(keepVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFTLEFT));
  ON_CALL(removeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFTLEFT));
  const std::vector<const SurroundingVehicleInterface*> vehicles{&keepVehicle, &removeVehicle};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, GetEgoLaneChangeDirection()).WillByDefault(Return(SurroundingLane::LEFT));

  const auto keepVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  const auto removeVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  ON_CALL(*keepVehicleQuery, IsSideSideVehicleChangingIntoSide()).WillByDefault(Return(true));
  ON_CALL(*removeVehicleQuery, IsSideSideVehicleChangingIntoSide()).WillByDefault(Return(false));
  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  ON_CALL(surroundingVehicleQueryFactory, GetQuery(::testing::Ref(keepVehicle))).WillByDefault(Return(keepVehicleQuery));
  ON_CALL(surroundingVehicleQueryFactory, GetQuery(::testing::Ref(removeVehicle))).WillByDefault(Return(removeVehicleQuery));

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSideSideVehicles(vehicles)};

  ASSERT_EQ(result.size(), 1);
  ASSERT_EQ(result.at(0)->GetId(), 0);
}

TEST(LeadingVehicleFilter_FilterSideSideVehicles, KeepVehiclesOnSideSideLaneOnlyIfEgoIsChangingLaneAndVehicleIsMovingTowardsEgoLane_Right)
{
  NiceMock<FakeSurroundingVehicle> keepVehicle;
  NiceMock<FakeSurroundingVehicle> removeVehicle;

  ON_CALL(keepVehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(removeVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(keepVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::RIGHTRIGHT));
  ON_CALL(removeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::RIGHTRIGHT));
  const std::vector<const SurroundingVehicleInterface*> vehicles{&keepVehicle, &removeVehicle};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, GetEgoLaneChangeDirection()).WillByDefault(Return(SurroundingLane::RIGHT));

  const auto keepVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  const auto removeVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  ON_CALL(*keepVehicleQuery, IsSideSideVehicleChangingIntoSide()).WillByDefault(Return(true));
  ON_CALL(*removeVehicleQuery, IsSideSideVehicleChangingIntoSide()).WillByDefault(Return(false));
  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  ON_CALL(surroundingVehicleQueryFactory, GetQuery(::testing::Ref(keepVehicle))).WillByDefault(Return(keepVehicleQuery));
  ON_CALL(surroundingVehicleQueryFactory, GetQuery(::testing::Ref(removeVehicle))).WillByDefault(Return(removeVehicleQuery));

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSideSideVehicles(vehicles)};

  ASSERT_EQ(result.size(), 1);
  ASSERT_EQ(result.at(0)->GetId(), 0);
}

TEST(LeadingVehicleFilter_FilterSideSideVehicles, RemoveVehiclesOnSideSideLaneIfEgoIsNotChangingLane)
{
  NiceMock<FakeSurroundingVehicle> keepVehicle;
  NiceMock<FakeSurroundingVehicle> removeVehicle;

  ON_CALL(keepVehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(removeVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(keepVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFTLEFT));
  ON_CALL(removeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFTLEFT));
  const std::vector<const SurroundingVehicleInterface*> vehicles{&keepVehicle, &removeVehicle};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, GetEgoLaneChangeDirection()).WillByDefault(Return(std::nullopt));
  const auto keepVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();

  const auto removeVehicleQuery = std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>();
  ON_CALL(*keepVehicleQuery, IsSideSideVehicleChangingIntoSide()).WillByDefault(Return(true));
  ON_CALL(*removeVehicleQuery, IsSideSideVehicleChangingIntoSide()).WillByDefault(Return(false));
  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  ON_CALL(surroundingVehicleQueryFactory, GetQuery(::testing::Ref(keepVehicle))).WillByDefault(Return(keepVehicleQuery));
  ON_CALL(surroundingVehicleQueryFactory, GetQuery(::testing::Ref(removeVehicle))).WillByDefault(Return(removeVehicleQuery));

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSideSideVehicles(vehicles)};

  ASSERT_EQ(result.size(), 0);
}

/******************************
 * CHECK FilterSwervingTarget **
 *******************************/

TEST(LeadingVehicleFilter_FilterSwervingTarget, RemovesCurrentSwervingTargetIfSwervingHasStartedAndEgoWillLeaveObstructionBeforeEnteringMinDistance)
{
  constexpr auto MIN_DISTANCE{10._m};
  NiceMock<FakeSurroundingVehicle> swervingTarget;
  NiceMock<FakeSurroundingVehicle> keepVehicle;
  ON_CALL(swervingTarget, GetId()).WillByDefault(Return(0));
  ON_CALL(keepVehicle, GetId()).WillByDefault(Return(1));

  const std::vector<const SurroundingVehicleInterface*> vehicles{&swervingTarget, &keepVehicle};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, GetSwervingTargetId()).WillByDefault(Return(0));
  ON_CALL(leadingVehicleQuery, IsSwervingPlanned()).WillByDefault(Return(false));
  ON_CALL(leadingVehicleQuery, GetMinDistance(::testing::Ref(swervingTarget))).WillByDefault(Return(MIN_DISTANCE));

  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  const auto surroundingVehicleQuery{std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>()};
  ON_CALL(surroundingVehicleQueryFactory, GetQuery(::testing::Ref(swervingTarget))).WillByDefault(Return(surroundingVehicleQuery));
  ON_CALL(*surroundingVehicleQuery, WillEnterMinDistanceBeforeObstructionIsCleared(MIN_DISTANCE)).WillByDefault(Return(false));

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSwervingTarget(vehicles)};

  ASSERT_EQ(result.size(), 1);
  ASSERT_EQ(result.at(0)->GetId(), 1);
}

TEST(LeadingVehicleFilter_FilterSwervingTarget, DoesNotRemoveCurrentSwervingTargetIfSwervingHasStartedAndEgoCannotLeaveObstructionBeforeEnteringMinDistance)
{
  constexpr auto MIN_DISTANCE{10._m};
  NiceMock<FakeSurroundingVehicle> swervingTarget;
  NiceMock<FakeSurroundingVehicle> keepVehicle;
  ON_CALL(swervingTarget, GetId()).WillByDefault(Return(0));
  ON_CALL(keepVehicle, GetId()).WillByDefault(Return(1));

  const std::vector<const SurroundingVehicleInterface*> vehicles{&swervingTarget, &keepVehicle};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, GetSwervingTargetId()).WillByDefault(Return(0));
  ON_CALL(leadingVehicleQuery, IsSwervingPlanned()).WillByDefault(Return(false));
  ON_CALL(leadingVehicleQuery, GetMinDistance(::testing::Ref(swervingTarget))).WillByDefault(Return(MIN_DISTANCE));

  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  const auto surroundingVehicleQuery{std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>()};
  ON_CALL(surroundingVehicleQueryFactory, GetQuery(::testing::Ref(swervingTarget))).WillByDefault(Return(surroundingVehicleQuery));
  ON_CALL(*surroundingVehicleQuery, WillEnterMinDistanceBeforeObstructionIsCleared(MIN_DISTANCE)).WillByDefault(Return(true));

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSwervingTarget(vehicles)};

  ASSERT_EQ(result.size(), 1);
}

TEST(LeadingVehicleFilter_FilterSwervingTarget, RemovesCausingVehicleOfFrontSituationIfSwervingIsPlanned)
{
  NiceMock<FakeSurroundingVehicle> causingVehicle;
  NiceMock<FakeSurroundingVehicle> keepVehicle;

  ON_CALL(causingVehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(keepVehicle, GetId()).WillByDefault(Return(1));

  const std::vector<const SurroundingVehicleInterface*> vehicles{&causingVehicle, &keepVehicle};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, GetSwervingTargetId()).WillByDefault(Return(std::nullopt));
  ON_CALL(leadingVehicleQuery, IsSwervingPlanned()).WillByDefault(Return(true));
  ON_CALL(leadingVehicleQuery, GetCausingVehicleIdFrontSituation()).WillByDefault(Return(0));

  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSwervingTarget(vehicles)};

  ASSERT_EQ(result.size(), 1);
  ASSERT_EQ(result.at(0)->GetId(), 1);
}

TEST(LeadingVehicleFilter_FilterSwervingTarget, DoesNotRemoveVehiclesIfSwervingIsNeitherInProgressNorPlanned)
{
  NiceMock<FakeSurroundingVehicle> causingVehicle;
  NiceMock<FakeSurroundingVehicle> swervingTarget;

  ON_CALL(causingVehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(swervingTarget, GetId()).WillByDefault(Return(1));

  const std::vector<const SurroundingVehicleInterface*> vehicles{&causingVehicle, &swervingTarget};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, GetSwervingTargetId()).WillByDefault(Return(std::nullopt));
  ON_CALL(leadingVehicleQuery, IsSwervingPlanned()).WillByDefault(Return(false));

  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSwervingTarget(vehicles)};

  ASSERT_EQ(result.size(), 2);
}

/*******************************
 * CHECK FilterSideLaneVehicles *
 ********************************/

TEST(LeadingVehicleFilter_FilterSideLaneVehicles, DoesNotRemoveVehiclesIfNotOnSideLane)
{
  NiceMock<FakeSurroundingVehicle> vehicle;

  ON_CALL(vehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(vehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::EGO));

  const std::vector<const SurroundingVehicleInterface*> vehicles{&vehicle};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSideLaneVehicles(vehicles)};

  ASSERT_EQ(result.size(), 1);
}

TEST(LeadingVehicleFilter_FilterSideLaneVehicles, DoesNotRemoveVehicleIfOuterLaneOvertakingProhibitionApplies)
{
  NiceMock<FakeSurroundingVehicle> keepVehicle;
  NiceMock<FakeSurroundingVehicle> removeVehicle;

  ON_CALL(keepVehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(removeVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(keepVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));
  ON_CALL(removeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));

  const std::vector<const SurroundingVehicleInterface*> vehicles{&keepVehicle, &removeVehicle};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, DoesOvertakingProhibitionApply(::testing::Ref(keepVehicle))).WillByDefault(Return(true));
  ON_CALL(leadingVehicleQuery, DoesOvertakingProhibitionApply(::testing::Ref(removeVehicle))).WillByDefault(Return(false));
  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSideLaneVehicles(vehicles)};

  ASSERT_EQ(result.size(), 1);
  ASSERT_EQ(result.at(0)->GetId(), 0);
}

TEST(LeadingVehicleFilter_FilterSideLaneVehicles, DoesNotRemoveVehicleOnTargetLaneIfChangingLanes)
{
  NiceMock<FakeSurroundingVehicle> keepVehicle;
  NiceMock<FakeSurroundingVehicle> removeVehicle;

  ON_CALL(keepVehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(removeVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(keepVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::RIGHT));
  ON_CALL(removeVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));

  const std::vector<const SurroundingVehicleInterface*> vehicles{&keepVehicle, &removeVehicle};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, GetEgoLaneChangeDirection).WillByDefault(Return(SurroundingLane::RIGHT));

  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSideLaneVehicles(vehicles)};

  ASSERT_EQ(result.size(), 1);
  ASSERT_EQ(result.at(0)->GetId(), 0);
}

struct DataFor_FilterSideLaneVehicles_VehicleApproachingFromSide
{
  bool vehicleIsExceedingLateralMotionThreshold;
  bool egoCannotClearLongitudinalObstructionBeforeSideCollision;
  bool vehicleIsStartingToChangeLanes;
  bool expected_keepVehicle;
};

class LeadingVehicleFilter_FilterSideVehiclesApproachingFromSide : public ::testing::Test,
                                                                   public ::testing::WithParamInterface<DataFor_FilterSideLaneVehicles_VehicleApproachingFromSide>
{
};

TEST_P(LeadingVehicleFilter_FilterSideVehiclesApproachingFromSide, KeepRelevantApproachingVehicles)
{
  const auto data = GetParam();
  NiceMock<FakeSurroundingVehicle> vehicle;

  ON_CALL(vehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(vehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));
  ON_CALL(vehicle, IsExceedingLateralMotionThreshold).WillByDefault(Return(data.vehicleIsExceedingLateralMotionThreshold));

  const std::vector<const SurroundingVehicleInterface*> vehicles{&vehicle};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  const auto surroundingVehicleQuery{std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>()};
  ON_CALL(surroundingVehicleQueryFactory, GetQuery(::testing::Ref(vehicle))).WillByDefault(Return(surroundingVehicleQuery));
  ON_CALL(*surroundingVehicleQuery, CannotClearLongitudinalObstructionBeforeCollision).WillByDefault(Return(data.egoCannotClearLongitudinalObstructionBeforeSideCollision));
  ON_CALL(*surroundingVehicleQuery, IsChangingLanesStillOnStartLane).WillByDefault(Return(data.vehicleIsStartingToChangeLanes));

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSideLaneVehicles(vehicles)};

  ASSERT_EQ(result.size(), data.expected_keepVehicle ? 1 : 0);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LeadingVehicleFilter_FilterSideVehiclesApproachingFromSide,
    testing::Values(
        DataFor_FilterSideLaneVehicles_VehicleApproachingFromSide{
            .vehicleIsExceedingLateralMotionThreshold = true,
            .egoCannotClearLongitudinalObstructionBeforeSideCollision = true,
            .vehicleIsStartingToChangeLanes = true,
            .expected_keepVehicle = true},
        DataFor_FilterSideLaneVehicles_VehicleApproachingFromSide{
            .vehicleIsExceedingLateralMotionThreshold = false,
            .egoCannotClearLongitudinalObstructionBeforeSideCollision = true,
            .vehicleIsStartingToChangeLanes = true,
            .expected_keepVehicle = false},
        DataFor_FilterSideLaneVehicles_VehicleApproachingFromSide{
            .vehicleIsExceedingLateralMotionThreshold = true,
            .egoCannotClearLongitudinalObstructionBeforeSideCollision = false,
            .vehicleIsStartingToChangeLanes = true,
            .expected_keepVehicle = false},
        DataFor_FilterSideLaneVehicles_VehicleApproachingFromSide{
            .vehicleIsExceedingLateralMotionThreshold = true,
            .egoCannotClearLongitudinalObstructionBeforeSideCollision = true,
            .vehicleIsStartingToChangeLanes = false,
            .expected_keepVehicle = false}));

TEST(LeadingVehicleFilter_FilterSideLaneVehicles, DoesNotRemoveVehicleIfLateralObstructionIsOverlappingAndMinDistanceWillBeEnteredBeforeObstructionIsCleared)
{
  NiceMock<FakeSurroundingVehicle> keepVehicle;
  NiceMock<FakeSurroundingVehicle> removeVehicle1;
  NiceMock<FakeSurroundingVehicle> removeVehicle2;

  ON_CALL(keepVehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(removeVehicle1, GetId()).WillByDefault(Return(1));
  ON_CALL(removeVehicle2, GetId()).WillByDefault(Return(2));

  ON_CALL(keepVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::RIGHT));
  ON_CALL(removeVehicle1, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));
  ON_CALL(removeVehicle2, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));

  ON_CALL(keepVehicle, LateralObstructionOverlapping()).WillByDefault(Return(true));
  ON_CALL(removeVehicle1, LateralObstructionOverlapping()).WillByDefault(Return(true));
  ON_CALL(removeVehicle2, LateralObstructionOverlapping()).WillByDefault(Return(false));

  const std::vector<const SurroundingVehicleInterface*> vehicles{&keepVehicle, &removeVehicle1, &removeVehicle2};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, GetMinDistance(_)).WillByDefault(Return(10.0_m));

  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  const auto keepSurroundingVehicleQuery{std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>()};
  const auto removeSurroundingVehicleQuery{std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>()};
  ON_CALL(surroundingVehicleQueryFactory, GetQuery(::testing::Ref(keepVehicle))).WillByDefault(Return(keepSurroundingVehicleQuery));
  ON_CALL(surroundingVehicleQueryFactory, GetQuery(::testing::Ref(removeVehicle1))).WillByDefault(Return(removeSurroundingVehicleQuery));
  ON_CALL(*keepSurroundingVehicleQuery, WillEnterMinDistanceBeforeObstructionIsCleared).WillByDefault(Return(true));
  ON_CALL(*removeSurroundingVehicleQuery, CannotClearLongitudinalObstructionBeforeCollision).WillByDefault(Return(false));

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSideLaneVehicles(vehicles)};

  ASSERT_EQ(result.size(), 1);
  ASSERT_EQ(result.at(0)->GetId(), 0);
}

struct DataFor_FilterSideLaneVehicles_FilterCausingVehicle
{
  std::optional<int> causingVehicleId;
  bool vehicleIsExceedingLateralMotionThreshold;
  bool egoWillEnterMinDistanceBeforeObstructionIsCleared;
  bool expected_keepVehicle;
};

class LeadingVehicleFilter_FilterSideVehiclesCausingVehicle : public ::testing::Test,
                                                              public ::testing::WithParamInterface<DataFor_FilterSideLaneVehicles_FilterCausingVehicle>
{
};

TEST_P(LeadingVehicleFilter_FilterSideVehiclesCausingVehicle, KeepCausingVehicleOfSideSituationIfItIsMovingLateralAndEgoWillEnterMinDistanceBeforeClearing)
{
  const auto data = GetParam();
  NiceMock<FakeSurroundingVehicle> vehicle;

  ON_CALL(vehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(vehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::LEFT));
  ON_CALL(vehicle, IsExceedingLateralMotionThreshold).WillByDefault(Return(data.vehicleIsExceedingLateralMotionThreshold));

  const std::vector<const SurroundingVehicleInterface*> vehicles{&vehicle};

  NiceMock<FakeLeadingVehicleQuery> leadingVehicleQuery;
  ON_CALL(leadingVehicleQuery, GetMinDistance(_)).WillByDefault(Return(10.0_m));
  ON_CALL(leadingVehicleQuery, GetCausingVehicleIdSideSituation).WillByDefault(Return(data.causingVehicleId));
  NiceMock<FakeSurroundingVehicleQueryFactory> surroundingVehicleQueryFactory;
  const auto surroundingVehicleQuery {std::make_shared<NiceMock<FakeSurroundingVehicleQuery>>()};
  ON_CALL(surroundingVehicleQueryFactory, GetQuery(::testing::Ref(vehicle))).WillByDefault(Return(surroundingVehicleQuery));
  ON_CALL(*surroundingVehicleQuery, WillEnterMinDistanceBeforeObstructionIsCleared(_)).WillByDefault(Return(data.egoWillEnterMinDistanceBeforeObstructionIsCleared));

  LeadingVehicleFilter testFilter(leadingVehicleQuery, surroundingVehicleQueryFactory);
  const auto result{testFilter.FilterSideLaneVehicles(vehicles)};

  ASSERT_EQ(result.size(), data.expected_keepVehicle ? 1 : 0);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LeadingVehicleFilter_FilterSideVehiclesCausingVehicle,
    testing::Values(
        DataFor_FilterSideLaneVehicles_FilterCausingVehicle{
            .causingVehicleId = 0,
            .vehicleIsExceedingLateralMotionThreshold = true,
            .egoWillEnterMinDistanceBeforeObstructionIsCleared = true,
            .expected_keepVehicle = true},
        DataFor_FilterSideLaneVehicles_FilterCausingVehicle{
            .causingVehicleId = 0,
            .vehicleIsExceedingLateralMotionThreshold = false,
            .egoWillEnterMinDistanceBeforeObstructionIsCleared = true,
            .expected_keepVehicle = false},
        DataFor_FilterSideLaneVehicles_FilterCausingVehicle{
            .causingVehicleId = 0,
            .vehicleIsExceedingLateralMotionThreshold = true,
            .egoWillEnterMinDistanceBeforeObstructionIsCleared = false,
            .expected_keepVehicle = false}));