/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/*
 * Unit Tests for SurroundingVehicles class
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <algorithm>
#include <cmath>
#include <ostream>
#include <tuple>

#include "../Fakes/FakeExtrapolation.h"
#include "../Fakes/FakeInformationAcquisition.h"
#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "TestSurroundingVehicles.h"
#include "gmock/gmock.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "module/driver/src/LaneQueryHelper.h"
#include "module/driver/src/Reliability.h"
#include "module/driver/src/ScmDefinitions.h"
#include "module/driver/src/SurroundingVehicles/SurroundingVehicles.h"
#include "module/driver/src/SurroundingVehicles/SurroundingVehiclesModifier.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

/****************************************
 * CHECK SurroundingVehicles Constructor *
 *****************************************/

struct DataFor_SurroundingVehiclesConstructor
{
  AreaOfInterest aoi;
  int vehicleId;

  friend std::ostream& operator<<(std::ostream& os, const DataFor_SurroundingVehiclesConstructor& rhs)
  {
    os << "aoi: " << ScmCommons::AreaOfInterestToString(rhs.aoi);
    os << " expectedVehicleIndex: " << rhs.vehicleId << std::endl;
    return os;
  }
};

class SurroundingVehicles_TestConstructor : public ::testing::Test,
                                            public ::testing::WithParamInterface<DataFor_SurroundingVehiclesConstructor>
{
protected:
  void SET_UP_SURROUNDING_OBJECTS_MOCK(NiceMock<FakeMicroscopicCharacteristics>* mockMicroscopicData)
  {
    _objectFront.id = 1;
    _objectFront.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(&_objectFront));

    _objectFrontFar.id = 2;
    _objectFrontFar.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::EGO_FRONT_FAR, _)).WillByDefault(Return(&_objectFrontFar));

    _objectFrontLeft.id = 3;
    _objectFrontLeft.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(&_objectFrontLeft));

    _objectFrontFarLeft.id = 4;
    _objectFrontFarLeft.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::LEFT_FRONT_FAR, _)).WillByDefault(Return(&_objectFrontFarLeft));

    _objectSideLeft = FILL_SIDE_VEHICLE_VECTOR({5, 6, 7});
    ON_CALL(*mockMicroscopicData, GetSideObjectsInformation(AreaOfInterest::LEFT_SIDE)).WillByDefault(Return(&_objectSideLeft));

    _objectRearLeft.id = 8;
    _objectRearLeft.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::LEFT_REAR, _)).WillByDefault(Return(&_objectRearLeft));

    _objectRear.id = 9;
    _objectRear.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::EGO_REAR, _)).WillByDefault(Return(&_objectRear));

    _objectFrontRight.id = 10;
    _objectFrontRight.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::RIGHT_FRONT, _)).WillByDefault(Return(&_objectFrontRight));

    _objectFrontFarRight.id = 11;
    _objectFrontFarRight.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::RIGHT_FRONT_FAR, _)).WillByDefault(Return(&_objectFrontFarRight));

    _objectSideRight = FILL_SIDE_VEHICLE_VECTOR({12, 13});
    ON_CALL(*mockMicroscopicData, GetSideObjectsInformation(AreaOfInterest::RIGHT_SIDE)).WillByDefault(Return(&_objectSideRight));

    _objectRearRight.id = 14;
    _objectRearRight.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::RIGHT_REAR, _)).WillByDefault(Return(&_objectRearRight));

    _objectFrontLeftLeft.id = 15;
    _objectFrontLeftLeft.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::LEFTLEFT_FRONT, _)).WillByDefault(Return(&_objectFrontLeftLeft));

    _objectSideLeftLeft = FILL_SIDE_VEHICLE_VECTOR({16, 17});
    ON_CALL(*mockMicroscopicData, GetSideObjectsInformation(AreaOfInterest::LEFTLEFT_SIDE)).WillByDefault(Return(&_objectSideLeftLeft));

    _objectRearLeftLeft.id = 18;
    _objectRearLeftLeft.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::LEFTLEFT_REAR, _)).WillByDefault(Return(&_objectRearLeftLeft));

    _objectFrontRightRight.id = 19;
    _objectFrontRightRight.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::RIGHTRIGHT_FRONT, _)).WillByDefault(Return(&_objectFrontRightRight));

    _objectSideRightRight = FILL_SIDE_VEHICLE_VECTOR({20, 21});
    ON_CALL(*mockMicroscopicData, GetSideObjectsInformation(AreaOfInterest::RIGHTRIGHT_SIDE)).WillByDefault(Return(&_objectSideRightRight));

    _objectRearRightRight.id = 22;
    _objectRearRightRight.exist = true;
    ON_CALL(*mockMicroscopicData, GetObjectInformation(AreaOfInterest::RIGHTRIGHT_REAR, _)).WillByDefault(Return(&_objectRearRightRight));
  }

  static std::vector<ObjectInformationScmExtended> FILL_SIDE_VEHICLE_VECTOR(std::vector<int> ids)
  {
    std::vector<ObjectInformationScmExtended> resultVector;
    for (auto id : ids)
    {
      ObjectInformationScmExtended newObject;
      newObject.id = id;
      newObject.exist = true;
      resultVector.push_back(newObject);
    }
    return resultVector;
  }

  static bool VEHICLE_NOT_IN_VECTOR(std::vector<const SurroundingVehicleInterface*> result, int id)
  {
    return std::none_of(begin(result), end(result), [id](const auto& vehicle)
                        { return vehicle->GetId() == id; });
  }

  ObjectInformationScmExtended _objectFront;
  ObjectInformationScmExtended _objectFrontFar;
  ObjectInformationScmExtended _objectFrontLeft;
  ObjectInformationScmExtended _objectFrontFarLeft;
  std::vector<ObjectInformationScmExtended> _objectSideLeft;
  ObjectInformationScmExtended _objectRearLeft;
  ObjectInformationScmExtended _objectRear;
  ObjectInformationScmExtended _objectFrontRight;
  ObjectInformationScmExtended _objectFrontFarRight;
  std::vector<ObjectInformationScmExtended> _objectSideRight;
  ObjectInformationScmExtended _objectRearRight;
  ObjectInformationScmExtended _objectFrontLeftLeft;
  std::vector<ObjectInformationScmExtended> _objectSideLeftLeft;
  ObjectInformationScmExtended _objectRearLeftLeft;
  ObjectInformationScmExtended _objectFrontRightRight;
  std::vector<ObjectInformationScmExtended> _objectSideRightRight;
  ObjectInformationScmExtended _objectRearRightRight;
};

TEST_P(SurroundingVehicles_TestConstructor, GivenDefaultVehiclesInMicroscopicCharacteristics_CreateCorrectVehiclesInVector)
{
  NiceMock<FakeMicroscopicCharacteristics> microscopicData;

  SET_UP_SURROUNDING_OBJECTS_MOCK(&microscopicData);

  const auto data = GetParam();

  SurroundingVehicles testSurroundingVehicles(&microscopicData);

  const auto aoi = data.aoi;
  const auto id = data.vehicleId;

  const auto& vehiclesVector = testSurroundingVehicles.Get();

  const auto matching_agents = std::count_if(begin(vehiclesVector), end(vehiclesVector), [aoi, id](const auto& vehicle)
                                             { return vehicle->GetId() == id && vehicle->GetAssignedAoi() == aoi && vehicle->WasPerceived() == false; });

  ASSERT_EQ(matching_agents, 1);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SurroundingVehicles_TestConstructor,
    testing::Values(
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::EGO_FRONT,
            .vehicleId = 1},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::EGO_FRONT_FAR,
            .vehicleId = 2},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::LEFT_FRONT,
            .vehicleId = 3},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::LEFT_FRONT_FAR,
            .vehicleId = 4},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .vehicleId = 5},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .vehicleId = 6},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .vehicleId = 7},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::LEFT_REAR,
            .vehicleId = 8},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::EGO_REAR,
            .vehicleId = 9},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::RIGHT_FRONT,
            .vehicleId = 10},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::RIGHT_FRONT_FAR,
            .vehicleId = 11},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::RIGHT_SIDE,
            .vehicleId = 12},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::RIGHT_SIDE,
            .vehicleId = 13},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::RIGHT_REAR,
            .vehicleId = 14},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::LEFTLEFT_FRONT,
            .vehicleId = 15},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::LEFTLEFT_SIDE,
            .vehicleId = 16},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::LEFTLEFT_SIDE,
            .vehicleId = 17},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::LEFTLEFT_REAR,
            .vehicleId = 18},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::RIGHTRIGHT_FRONT,
            .vehicleId = 19},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::RIGHTRIGHT_SIDE,
            .vehicleId = 20},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::RIGHTRIGHT_SIDE,
            .vehicleId = 21},
        DataFor_SurroundingVehiclesConstructor{
            .aoi = AreaOfInterest::RIGHTRIGHT_REAR,
            .vehicleId = 22}));

TEST_F(SurroundingVehicles_TestConstructor, TestSomeVehiclesDoNotExist_NotAddedToVector)
{
  NiceMock<FakeMicroscopicCharacteristics> microscopicData;

  SET_UP_SURROUNDING_OBJECTS_MOCK(&microscopicData);

  _objectFrontLeft.exist = false;
  _objectRearLeft.exist = false;
  _objectFront.exist = false;
  _objectSideRight.at(0).exist = false;

  SurroundingVehicles testSurroundingVehicles(&microscopicData);

  const auto& resultingData = testSurroundingVehicles.Get();
  ASSERT_TRUE(VEHICLE_NOT_IN_VECTOR(resultingData, _objectFrontLeft.id));
  ASSERT_TRUE(VEHICLE_NOT_IN_VECTOR(resultingData, _objectRearLeft.id));
  ASSERT_TRUE(VEHICLE_NOT_IN_VECTOR(resultingData, _objectFront.id));
  ASSERT_TRUE(VEHICLE_NOT_IN_VECTOR(resultingData, _objectSideRight.at(0).id));
}

/**********************************************
    Test SurroundingVehicles UpdateVehicles
***********************************************/

TEST(SurroundingVehicles_TestUpdateVehicles, AddsPerceivedVehiclesToSurroundingVehiclesVector)
{
  NiceMock<FakeExtrapolation> extrapolation;
  NiceMock<FakeInformationAcquisition> informationAcquisition;

  ObjectInformationScmExtended perceivedVehicle1;
  perceivedVehicle1.exist = true;
  perceivedVehicle1.id = 1;
  perceivedVehicle1.opticalAnglePositionHorizontal = 0_rad;

  ObjectInformationScmExtended perceivedVehicle2;
  perceivedVehicle2.exist = true;
  perceivedVehicle2.id = 2;
  perceivedVehicle2.opticalAnglePositionHorizontal = 0_rad;

  std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>> perceptionMap{
      {AreaOfInterest::EGO_FRONT, &perceivedVehicle1},
      {AreaOfInterest::LEFT_SIDE, &perceivedVehicle2}};
  ON_CALL(informationAcquisition, PerceiveSurroundingVehicles(false)).WillByDefault(Return(perceptionMap));
  EXPECT_CALL(informationAcquisition, SetAoiDataWithGroundTruth(_, &perceivedVehicle1, AreaOfInterest::EGO_FRONT))
      .WillOnce(::testing::SetArgPointee<0>(perceivedVehicle1));
  EXPECT_CALL(informationAcquisition, SetAoiDataWithGroundTruth(_, &perceivedVehicle2, AreaOfInterest::LEFT_SIDE))
      .WillOnce(::testing::SetArgPointee<0>(perceivedVehicle2));

  SurroundingVehicles testSurroundingVehicles;
  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  surroundingVehiclesModifier.UpdateVehicles(&informationAcquisition, extrapolation, true, false);
  const auto& vehiclesVector = testSurroundingVehicles.Get();

  ASSERT_EQ(vehiclesVector.size(), 2);
  ASSERT_TRUE(std::all_of(begin(vehiclesVector), end(vehiclesVector), [](const auto& vehicle)
                          { return vehicle->WasPerceived() == true; }));
}

TEST(SurroundingVehicles_TestUpdateVehicles, UpdatesPerceivedVehiclesIfAlreadyExistingInSurroundingVehiclesVector)
{
  NiceMock<FakeMicroscopicCharacteristics> microscopicData;
  NiceMock<FakeExtrapolation> extrapolation;
  NiceMock<FakeInformationAcquisition> informationAcquisition;

  constexpr int agentId{1};
  ObjectInformationScmExtended perceivedVehicle;
  perceivedVehicle.id = agentId;
  perceivedVehicle.width = 2.0_m;
  perceivedVehicle.obstruction.mainLaneLocator = 2.0_m;
  perceivedVehicle.longitudinalObstruction.mainLaneLocator = 3.0_m;
  perceivedVehicle.relativeLongitudinalDistance = 0.0_m;
  perceivedVehicle.opticalAnglePositionHorizontal = 61.0_rad / 180 * M_PI;
  const AreaOfInterest perceptionAoi{AreaOfInterest::LEFT_FRONT_FAR};
  std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>> perceptionMap{
      {perceptionAoi, &perceivedVehicle}};
  ON_CALL(informationAcquisition, PerceiveSurroundingVehicles(false)).WillByDefault(Return(perceptionMap));

  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle;
  ON_CALL(fakeSurroundingVehicle, GetId()).WillByDefault(Return(agentId));
  ON_CALL(fakeSurroundingVehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(0.5_m));
  ON_CALL(fakeSurroundingVehicle, GetRelativeLateralPosition()).WillByDefault(Return(0.5_m));
  EXPECT_CALL(fakeSurroundingVehicle, UpdateWithGroundTruth(&perceivedVehicle, perceptionAoi, &informationAcquisition));

  TestSurroundingVehicles testSurroundingVehicles;
  testSurroundingVehicles.SetSurroundingVehicles({&fakeSurroundingVehicle});
  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  surroundingVehiclesModifier.UpdateVehicles(&informationAcquisition, extrapolation, true, false);

  const auto& vehiclesVector = testSurroundingVehicles.Get();
  ASSERT_EQ(vehiclesVector.size(), 1);
}

TEST(SurroundingVehicles_TestUpdateVehicles, DoesNotPerceiveSurroundingsIfPerceiveVehiclesFlagIsFalse)
{
  NiceMock<FakeMicroscopicCharacteristics> microscopicData;
  NiceMock<FakeExtrapolation> extrapolation;
  NiceMock<FakeInformationAcquisition> informationAcquisition;

  constexpr int agentId{1};

  EXPECT_CALL(informationAcquisition, PerceiveSurroundingVehicles(_)).Times(0);

  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle;
  ON_CALL(fakeSurroundingVehicle, GetId()).WillByDefault(Return(agentId));
  ON_CALL(fakeSurroundingVehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(0.5_m));
  ON_CALL(fakeSurroundingVehicle, GetRelativeLateralPosition()).WillByDefault(Return(0.5_m));
  EXPECT_CALL(fakeSurroundingVehicle, UpdateWithGroundTruth(_, _, _)).Times(0);

  TestSurroundingVehicles testSurroundingVehicles;
  testSurroundingVehicles.SetSurroundingVehicles({&fakeSurroundingVehicle});
  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  surroundingVehiclesModifier.UpdateVehicles(&informationAcquisition, extrapolation, false, false);

  const auto& vehiclesVector = testSurroundingVehicles.Get();
  ASSERT_EQ(vehiclesVector.size(), 1);
}

TEST(SurroundingVehicles_TestUpdateVehicles, ExtrapolatesOnlyNonPerceivedVehicles)
{
  NiceMock<FakeMicroscopicCharacteristics> microscopicData;
  NiceMock<FakeExtrapolation> extrapolation;
  NiceMock<FakeInformationAcquisition> informationAcquisition;

  constexpr int perceivedAgentId{1};
  constexpr int nonPerceivedAgentId{2};

  ObjectInformationScmExtended perceivedVehicle;
  perceivedVehicle.id = perceivedAgentId;
  perceivedVehicle.width = 2.0_m;
  perceivedVehicle.obstruction.mainLaneLocator = 2.0_m;
  perceivedVehicle.longitudinalObstruction.mainLaneLocator = 3.0_m;
  perceivedVehicle.relativeLongitudinalDistance = 0.0_m;
  perceivedVehicle.opticalAnglePositionHorizontal = 61.0_rad / 180 * M_PI;

  const AreaOfInterest perceptionAoi{AreaOfInterest::LEFT_FRONT_FAR};
  std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>> perceptionMap{
      {perceptionAoi, &perceivedVehicle}};
  ON_CALL(informationAcquisition, PerceiveSurroundingVehicles(_)).WillByDefault(Return(perceptionMap));
  ON_CALL(informationAcquisition, IsOutOfSight(_)).WillByDefault(Return(false));

  NiceMock<FakeSurroundingVehicle> perceivedSurroundingVehicle;
  ON_CALL(perceivedSurroundingVehicle, GetId()).WillByDefault(Return(perceivedAgentId));
  ON_CALL(perceivedSurroundingVehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(0.5_m));
  ON_CALL(perceivedSurroundingVehicle, GetRelativeLateralPosition()).WillByDefault(Return(0.5_m));
  EXPECT_CALL(perceivedSurroundingVehicle, UpdateWithGroundTruth(&perceivedVehicle, perceptionAoi, &informationAcquisition));

  NiceMock<FakeSurroundingVehicle> nonPerceivedSurroundingVehicle;
  ON_CALL(nonPerceivedSurroundingVehicle, GetId()).WillByDefault(Return(nonPerceivedAgentId));
  ON_CALL(nonPerceivedSurroundingVehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(0.5_m));
  ON_CALL(nonPerceivedSurroundingVehicle, GetRelativeLateralPosition()).WillByDefault(Return(0.5_m));
  EXPECT_CALL(nonPerceivedSurroundingVehicle, Extrapolate(Ref(extrapolation)));

  TestSurroundingVehicles testSurroundingVehicles;
  testSurroundingVehicles.SetSurroundingVehicles({&perceivedSurroundingVehicle, &nonPerceivedSurroundingVehicle});
  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  surroundingVehiclesModifier.UpdateVehicles(&informationAcquisition, extrapolation, true, false);

  const auto& vehiclesVector = testSurroundingVehicles.Get();
  ASSERT_EQ(vehiclesVector.size(), 2);
}

TEST(SurroundingVehicles_TestUpdateVehicles, RemovesVehicleIfExtrapolatedOutOfSight)
{
  NiceMock<FakeMicroscopicCharacteristics> microscopicData;
  NiceMock<FakeExtrapolation> extrapolation;
  NiceMock<FakeInformationAcquisition> informationAcquisition;

  ObjectInformationScmExtended inSightVehicle;
  inSightVehicle.id = 1;
  inSightVehicle.relativeLongitudinalDistance = 123._m;
  ObjectInformationScmExtended outOfSightVehicle;
  outOfSightVehicle.id = 2;
  outOfSightVehicle.relativeLongitudinalDistance = 456._m;

  std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>> perceptionMap{};
  ON_CALL(informationAcquisition, PerceiveSurroundingVehicles(_)).WillByDefault(Return(perceptionMap));

  // Mock objects need to be created on heap, because they are potentially deleted during the test
  auto visibleVehicle = new NiceMock<FakeSurroundingVehicle>;
  ON_CALL(*visibleVehicle, GetId()).WillByDefault(Return(1));
  ON_CALL(*visibleVehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(123._m));

  auto invisibleVehicle = new NiceMock<FakeSurroundingVehicle>;
  ON_CALL(*invisibleVehicle, GetId()).WillByDefault(Return(2));
  ON_CALL(*invisibleVehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(456._m));

  ON_CALL(informationAcquisition, IsOutOfSight(_)).WillByDefault(Return(false));
  ON_CALL(informationAcquisition, IsOutOfSight(456._m)).WillByDefault(Return(true));

  TestSurroundingVehicles testSurroundingVehicles;
  testSurroundingVehicles.SetSurroundingVehicles({visibleVehicle, invisibleVehicle});
  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  surroundingVehiclesModifier.UpdateVehicles(&informationAcquisition, extrapolation, false);

  const auto& vehiclesVector = testSurroundingVehicles.Get();
  ASSERT_EQ(vehiclesVector.size(), 1);
  ASSERT_TRUE(vehiclesVector.at(0)->GetId() == 1);

  delete (visibleVehicle);
}

TEST(SurroundingVehicles_TestUpdateVehicles, DecaysReliabilityForNonPerceivedVehicles)
{
  const auto DEFAULT_VEHICLE_DISTANCE{10.0_m};
  const auto CYCLE_TIME{100_ms};
  const auto NOW{1000_ms};
  const auto START_RELIABILITY{100.0_ms};

  FakeSurroundingVehicle surroundingVehicle;
  // Set default values to avoid removing vehicle out of sight or because it is unseen for too long
  ON_CALL(surroundingVehicle, GetRelativeLongitudinalPosition).WillByDefault(Return(DEFAULT_VEHICLE_DISTANCE));
  ON_CALL(surroundingVehicle, GetTimeOfLastPerception).WillByDefault(Return(NOW - 100_ms));

  NiceMock<FakeExtrapolation> extrapolation;
  NiceMock<FakeInformationAcquisition> informationAcquisition;
  ON_CALL(informationAcquisition, GetCycleTime).WillByDefault(Return(CYCLE_TIME));
  ON_CALL(informationAcquisition, GetTime).WillByDefault(Return(NOW));
  TestSurroundingVehicles testSurroundingVehicles;
  testSurroundingVehicles.SetSurroundingVehicles({&surroundingVehicle});
  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);

  EXPECT_CALL(surroundingVehicle, DecayReliability(START_RELIABILITY));

  surroundingVehiclesModifier.UpdateVehicles(&informationAcquisition, extrapolation, false);
}

TEST(SurroundingVehicles_TestUpdateVehicles, SetsReliabilityForPerceivedVehicles)
{
  NiceMock<FakeExtrapolation> extrapolation;
  NiceMock<FakeInformationAcquisition> informationAcquisition;

  const auto ANGLE_TO_OPTICAL_AXIS{10._rad / 180. * M_PI};
  ObjectInformationScmExtended perceivedVehicle;
  perceivedVehicle.exist = true;
  perceivedVehicle.id = 1;
  perceivedVehicle.opticalAnglePositionHorizontal = ANGLE_TO_OPTICAL_AXIS;

  std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>> perceptionMap{
      {AreaOfInterest::EGO_FRONT, &perceivedVehicle}};
  ON_CALL(informationAcquisition, PerceiveSurroundingVehicles(_)).WillByDefault(Return(perceptionMap));
  ON_CALL(informationAcquisition, SetAoiDataWithGroundTruth(_, _, _))
      .WillByDefault(::testing::SetArgPointee<0>(perceivedVehicle));

  SurroundingVehicles testSurroundingVehicles;
  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  surroundingVehiclesModifier.UpdateVehicles(&informationAcquisition, extrapolation, true, false);
  const auto vehicle = testSurroundingVehicles.Get().at(0);
  const double expectedReliabilty{ReliabilityCalculations::CalculateReliability(ANGLE_TO_OPTICAL_AXIS)};

  ASSERT_EQ(vehicle->GetReliability(), expectedReliabilty);
}

TEST(SurroundingVehicles_TestUpdateVehicles, SetsTimeOfLastPerceptionForPerceivedVehicles)
{
  NiceMock<FakeExtrapolation> extrapolation;
  NiceMock<FakeInformationAcquisition> informationAcquisition;

  const auto ANGLE_TO_OPTICAL_AXIS{10._rad / 180. * M_PI};
  ObjectInformationScmExtended perceivedVehicle;
  perceivedVehicle.exist = true;
  perceivedVehicle.id = 1;
  perceivedVehicle.opticalAnglePositionHorizontal = ANGLE_TO_OPTICAL_AXIS;

  std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>> perceptionMap{
      {AreaOfInterest::EGO_FRONT, &perceivedVehicle}};
  ON_CALL(informationAcquisition, PerceiveSurroundingVehicles(_)).WillByDefault(Return(perceptionMap));
  ON_CALL(informationAcquisition, GetTime).WillByDefault(Return(123_ms));
  ON_CALL(informationAcquisition, SetAoiDataWithGroundTruth(_, _, _))
      .WillByDefault(::testing::SetArgPointee<0>(perceivedVehicle));

  SurroundingVehicles testSurroundingVehicles;
  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  surroundingVehiclesModifier.UpdateVehicles(&informationAcquisition, extrapolation, true, false);
  const auto vehicle = testSurroundingVehicles.Get().at(0);

  ASSERT_EQ(vehicle->GetTimeOfLastPerception(), 123_ms);
}

TEST(SurroundingVehicles_TestUpdateVehicles, RemovesVehicleIfTimeOfLastPerceptionIsLongerAgoThanSevenSeconds)
{
  NiceMock<FakeExtrapolation> extrapolation;
  NiceMock<FakeInformationAcquisition> informationAcquisition;
  ON_CALL(informationAcquisition, GetTime).WillByDefault(Return(7001_ms));
  // vehicle is deleted during test, create it on heap
  auto vehicle = new NiceMock<FakeSurroundingVehicle>;
  ON_CALL(*vehicle, WasPerceived).WillByDefault(Return(false));
  ON_CALL(*vehicle, GetTimeOfLastPerception).WillByDefault(Return(0_ms));
  ON_CALL(*vehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(0.5_m));
  ON_CALL(*vehicle, GetRelativeLateralPosition()).WillByDefault(Return(0.5_m));

  TestSurroundingVehicles testSurroundingVehicles;
  testSurroundingVehicles.SetSurroundingVehicles({vehicle});
  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  surroundingVehiclesModifier.UpdateVehicles(&informationAcquisition, extrapolation, false);

  const auto& vehiclesVector = testSurroundingVehicles.Get();
  ASSERT_EQ(0, vehiclesVector.size());
}

TEST(SurroundingVehicles_TestUpdateVehicles, AddsVehiclePerceivedInPeripheryIfUnknown)
{
  NiceMock<FakeExtrapolation> extrapolation;
  NiceMock<FakeInformationAcquisition> informationAcquisition;

  const auto ANGLE_TO_OPTICAL_AXIS{units::make_unit<units::angle::radian_t>(M_PI)};
  ObjectInformationScmExtended perceivedVehicle;
  perceivedVehicle.exist = true;
  perceivedVehicle.id = 1;
  perceivedVehicle.opticalAnglePositionHorizontal = ANGLE_TO_OPTICAL_AXIS;

  std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>> perceptionMap{
      {AreaOfInterest::EGO_FRONT, &perceivedVehicle}};
  ON_CALL(informationAcquisition, PerceiveSurroundingVehicles).WillByDefault(Return(perceptionMap));
  EXPECT_CALL(informationAcquisition, SetAoiDataWithGroundTruth(_, &perceivedVehicle, _))
      .WillOnce(::testing::SetArgPointee<0>(perceivedVehicle));

  SurroundingVehicles testSurroundingVehicles;
  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  surroundingVehiclesModifier.UpdateVehicles(&informationAcquisition, extrapolation, true, false);
  const auto vehicle = testSurroundingVehicles.Get().at(0);

  ASSERT_EQ(vehicle->GetId(), 1);
}

TEST(SurroundingVehicles_TestUpdateVehicles, UpdatesVehiclePerceivedInPeripheryIfPeripheryReliabilityIsBetterThanCurrent)
{
  NiceMock<FakeExtrapolation> extrapolation;
  NiceMock<FakeInformationAcquisition> informationAcquisition;

  const auto ANGLE_TO_OPTICAL_AXIS{60.0_rad / 180.0 * M_PI};  // On the border between UFOV and Periphery
  const auto EXPECTED_RELIABILITY = ReliabilityCalculations::CalculateReliability(ANGLE_TO_OPTICAL_AXIS);
  const auto CURRENT_RELIABILITY = EXPECTED_RELIABILITY - 0.1;

  const int VEHICLE_ID{1};
  ObjectInformationScmExtended objectData;
  objectData.id = VEHICLE_ID;
  SurroundingVehicle knownVehicle(objectData);
  knownVehicle.SetReliability(CURRENT_RELIABILITY);

  ObjectInformationScmExtended perceivedVehicle;
  perceivedVehicle.exist = true;
  perceivedVehicle.id = VEHICLE_ID;
  perceivedVehicle.opticalAnglePositionHorizontal = ANGLE_TO_OPTICAL_AXIS;

  std::vector<std::tuple<AreaOfInterest, const ObjectInformationSCM*>> perceptionMap{
      {AreaOfInterest::EGO_FRONT, &perceivedVehicle}};
  ON_CALL(informationAcquisition, PerceiveSurroundingVehicles).WillByDefault(Return(perceptionMap));
  EXPECT_CALL(informationAcquisition, SetAoiDataWithGroundTruth(_, &perceivedVehicle, _))
      .WillOnce(::testing::SetArgPointee<0>(perceivedVehicle));

  TestSurroundingVehicles testSurroundingVehicles;
  testSurroundingVehicles.SetSurroundingVehicles({&knownVehicle});
  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);

  surroundingVehiclesModifier.UpdateVehicles(&informationAcquisition, extrapolation, true);
  const auto vehicle = testSurroundingVehicles.Get().at(0);
  ASSERT_DOUBLE_EQ(vehicle->GetReliability(), EXPECTED_RELIABILITY);
}

/********************************
 * CHECK WriteToMicroscopicData *
 ********************************/

class SurroundingVehicles_WriteToMicroscopicDataVector : public ::testing::Test,
                                                         public ::testing::WithParamInterface<AreaOfInterest>
{
};

TEST_P(SurroundingVehicles_WriteToMicroscopicDataVector, GivenVehicleInAoiSetVehicleInformationInMicroscopicData)
{
  MicroscopicCharacteristics microscopicData;
  AreaOfInterest assignedAoi = GetParam();

  ObjectInformationScmExtended emptyObjectInformation;
  ObjectInformationScmExtended filledObjectInformation;
  filledObjectInformation.exist = true;
  filledObjectInformation.id = 123;

  NiceMock<FakeSurroundingVehicle> surroundingVehicle;
  ON_CALL(surroundingVehicle, GetObjectInformation).WillByDefault(Return(&filledObjectInformation));

  AoiVehicleMapping mapping{
      {assignedAoi, {&surroundingVehicle}}};

  NiceMock<FakeInformationAcquisition> informationAcquisition;
  SurroundingVehiclesModifier::WriteToMicroscopicDataVector(mapping, &microscopicData);

  for (const auto& aoi : SURROUNDING_AREAS_OF_INTEREST_VECTOR)
  {
    if (!LaneQueryHelper::IsSideArea(aoi))
    {
      if (assignedAoi == aoi)
      {
        auto result = *microscopicData.GetObjectInformation(aoi);
        ASSERT_THAT(result, ::testing::Field(&ObjectInformationScmExtended::id, filledObjectInformation.id));
        ASSERT_THAT(result, ::testing::Field(&ObjectInformationScmExtended::exist, true));
      }
      else
      {
        ASSERT_THAT(*microscopicData.GetObjectInformation(aoi), ::testing::Field(&ObjectInformationScmExtended::exist, false));
      }
    }
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SurroundingVehicles_WriteToMicroscopicDataVector,
    testing::Values(
        AreaOfInterest::EGO_FRONT,
        AreaOfInterest::EGO_REAR,
        AreaOfInterest::LEFT_FRONT,
        AreaOfInterest::LEFT_REAR,
        AreaOfInterest::RIGHT_FRONT,
        AreaOfInterest::RIGHT_REAR,
        AreaOfInterest::LEFTLEFT_FRONT,
        AreaOfInterest::LEFTLEFT_REAR,
        AreaOfInterest::RIGHTRIGHT_FRONT,
        AreaOfInterest::RIGHTRIGHT_REAR));

class SurroundingVehicles_WriteToMicroscopicDataVectorSideAreas : public ::testing::Test,
                                                                  public ::testing::WithParamInterface<AreaOfInterest>
{
};

TEST_P(SurroundingVehicles_WriteToMicroscopicDataVectorSideAreas, GivenVehiclesInSideAoiVectorSetVehicleInformationInMicroscopicData)
{
  MicroscopicCharacteristics microscopicData;
  AreaOfInterest assignedAoi = GetParam();

  ObjectInformationScmExtended emptyObjectInformation;
  std::vector<SurroundingVehicleInterface*> surroundingVehicleVector;
  std::vector<ObjectInformationScmExtended> sideObjectsVector;

  ObjectInformationScmExtended filledObjectInformation1;
  filledObjectInformation1.exist = true;
  filledObjectInformation1.id = 1;
  NiceMock<FakeSurroundingVehicle> surroundingVehicle1;
  ON_CALL(surroundingVehicle1, GetObjectInformation).WillByDefault(Return(&filledObjectInformation1));
  surroundingVehicleVector.push_back(&surroundingVehicle1);
  sideObjectsVector.push_back(filledObjectInformation1);

  ObjectInformationScmExtended filledObjectInformation2;
  filledObjectInformation2.exist = true;
  filledObjectInformation2.id = 2;
  NiceMock<FakeSurroundingVehicle> surroundingVehicle2;
  ON_CALL(surroundingVehicle2, GetObjectInformation).WillByDefault(Return(&filledObjectInformation2));
  surroundingVehicleVector.push_back(&surroundingVehicle2);
  sideObjectsVector.push_back(filledObjectInformation2);

  AoiVehicleMapping mapping{
      {assignedAoi, surroundingVehicleVector}};

  SurroundingVehiclesModifier::WriteToMicroscopicDataVector(mapping, &microscopicData);

  for (const auto& aoi : SURROUNDING_AREAS_OF_INTEREST_VECTOR)
  {
    if (LaneQueryHelper::IsSideArea(aoi))
    {
      if (assignedAoi == aoi)
      {
        ASSERT_THAT(microscopicData.UpdateSideObjectsInformation(aoi)->at(0),
                    ::testing::Field(&ObjectInformationScmExtended::id, sideObjectsVector.at(0).id));
        ASSERT_THAT(microscopicData.UpdateSideObjectsInformation(aoi)->at(0),
                    ::testing::Field(&ObjectInformationScmExtended::exist, true));

        ASSERT_THAT(microscopicData.UpdateSideObjectsInformation(aoi)->at(1),
                    ::testing::Field(&ObjectInformationScmExtended::id, sideObjectsVector.at(1).id));
        ASSERT_THAT(microscopicData.UpdateSideObjectsInformation(aoi)->at(1),
                    ::testing::Field(&ObjectInformationScmExtended::exist, true));
      }
      else
      {
        ASSERT_THAT(microscopicData.UpdateSideObjectsInformation(aoi)->at(0),
                    ::testing::Field(&ObjectInformationScmExtended::exist, false));
      }
    }
    else
    {
      ASSERT_THAT(*microscopicData.GetObjectInformation(aoi),
                  ::testing::Field(&ObjectInformationScmExtended::exist, false));
    }
  }
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SurroundingVehicles_WriteToMicroscopicDataVectorSideAreas,
    testing::Values(
        AreaOfInterest::LEFT_SIDE,
        AreaOfInterest::RIGHT_SIDE,
        AreaOfInterest::LEFTLEFT_SIDE,
        AreaOfInterest::RIGHTRIGHT_SIDE));

/*********************************************
 * CHECK HandleSideAreaTransitionsToSideArea *
 *********************************************/

class SurroundingVehicles_HandleSideAreaTransitionsToSideArea : public ::testing::Test,
                                                                public ::testing::WithParamInterface<std::tuple<AreaOfInterest, AreaOfInterest>>
{
};

TEST_P(SurroundingVehicles_HandleSideAreaTransitionsToSideArea, IfVehicleTransitionedToSideArea_CallDataCorrectionFunction)
{
  const auto& [previousAoi, currentAoi] = GetParam();

  NiceMock<FakeInformationAcquisition> fakeInformationAcquisition;
  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> vehicle;

  ON_CALL(vehicle, GetAssignedAoi).WillByDefault(Return(previousAoi));
  aoiMapping[currentAoi] = {&vehicle};

  EXPECT_CALL(vehicle, HandleTransitionToSideArea(&fakeInformationAcquisition)).Times(1);
  EXPECT_CALL(vehicle, HandleTransitionFromSideArea(_, _)).Times(0);

  SurroundingVehiclesModifier::HandleSideAreaTransitions(&aoiMapping, &fakeInformationAcquisition);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SurroundingVehicles_HandleSideAreaTransitionsToSideArea,
    testing::Values(
        std::make_tuple(AreaOfInterest::LEFT_REAR, AreaOfInterest::LEFT_SIDE),
        std::make_tuple(AreaOfInterest::LEFT_FRONT, AreaOfInterest::LEFT_SIDE),
        std::make_tuple(AreaOfInterest::RIGHT_REAR, AreaOfInterest::RIGHT_SIDE),
        std::make_tuple(AreaOfInterest::RIGHT_FRONT, AreaOfInterest::RIGHT_SIDE),
        std::make_tuple(AreaOfInterest::LEFTLEFT_REAR, AreaOfInterest::LEFTLEFT_SIDE),
        std::make_tuple(AreaOfInterest::RIGHTRIGHT_REAR, AreaOfInterest::RIGHTRIGHT_SIDE)));

/***********************************************
 * CHECK HandleSideAreaTransitionsFromSideArea *
 ***********************************************/

class SurroundingVehicles_HandleSideAreaTransitionsFromSideArea : public ::testing::Test,
                                                                  public ::testing::WithParamInterface<std::tuple<AreaOfInterest, AreaOfInterest>>
{
};

TEST_P(SurroundingVehicles_HandleSideAreaTransitionsFromSideArea, IfVehicleTransitionedFromSideArea_CallDataCorrectionFunction)
{
  const auto& [previousAoi, currentAoi] = GetParam();

  NiceMock<FakeInformationAcquisition> fakeInformationAcquisition;
  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> vehicle;

  ON_CALL(vehicle, GetAssignedAoi).WillByDefault(Return(previousAoi));
  aoiMapping[currentAoi] = {&vehicle};

  EXPECT_CALL(vehicle, HandleTransitionToSideArea(_)).Times(0);
  EXPECT_CALL(vehicle, HandleTransitionFromSideArea(currentAoi, &fakeInformationAcquisition)).Times(1);

  SurroundingVehiclesModifier::HandleSideAreaTransitions(&aoiMapping, &fakeInformationAcquisition);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SurroundingVehicles_HandleSideAreaTransitionsFromSideArea,
    testing::Values(
        std::make_tuple(AreaOfInterest::LEFT_SIDE, AreaOfInterest::LEFT_REAR),
        std::make_tuple(AreaOfInterest::LEFT_SIDE, AreaOfInterest::LEFT_FRONT),
        std::make_tuple(AreaOfInterest::RIGHT_SIDE, AreaOfInterest::RIGHT_REAR),
        std::make_tuple(AreaOfInterest::RIGHT_SIDE, AreaOfInterest::RIGHT_FRONT),
        std::make_tuple(AreaOfInterest::LEFTLEFT_SIDE, AreaOfInterest::LEFTLEFT_REAR),
        std::make_tuple(AreaOfInterest::RIGHTRIGHT_SIDE, AreaOfInterest::RIGHTRIGHT_FRONT)));

/************************************
 * CHECK HandlesSideAreaTransitions *
 ************************************/

TEST(SurroundingVehicles_HandlesSideAreaTransitions, NoTransitionsHappenedCheckNoTransitionFunctionCalled)
{
  NiceMock<FakeInformationAcquisition> fakeInformationAcquisition;
  AoiVehicleMapping aoiMapping;
  NiceMock<FakeSurroundingVehicle> sideVehicle;
  NiceMock<FakeSurroundingVehicle> nonSideVehicle;
  constexpr auto sideAoi{AreaOfInterest::LEFT_SIDE};
  constexpr auto nonSideAoi{AreaOfInterest::LEFT_FRONT};

  ON_CALL(sideVehicle, GetAssignedAoi).WillByDefault(Return(sideAoi));
  aoiMapping[sideAoi] = {&sideVehicle};
  EXPECT_CALL(sideVehicle, HandleTransitionToSideArea(_)).Times(0);
  EXPECT_CALL(sideVehicle, HandleTransitionFromSideArea(_, _)).Times(0);

  ON_CALL(nonSideVehicle, GetAssignedAoi).WillByDefault(Return(nonSideAoi));
  aoiMapping[nonSideAoi] = {&nonSideVehicle};
  EXPECT_CALL(nonSideVehicle, HandleTransitionToSideArea(_)).Times(0);
  EXPECT_CALL(nonSideVehicle, HandleTransitionFromSideArea(_, _)).Times(0);

  SurroundingVehiclesModifier::HandleSideAreaTransitions(&aoiMapping, &fakeInformationAcquisition);
}

/*******************************************************
 * CHECK RemoveVisibleVehiclesNotPresentInGroundTruth  *
 *******************************************************/

Scm::BoundingBox CREATE_FAKE_BOUNDING_BOX(units::length::meter_t sPos, units::length::meter_t tPos)
{
  constexpr auto width{1._m};
  constexpr auto length{1._m};

  Scm::BoundingBox bb{
      .leftFront = {sPos, tPos + width / 2},
      .rightFront = {sPos, tPos - width / 2},
      .leftRear = {sPos - length, tPos + width / 2},
      .rightRear = {sPos - length, tPos - width / 2}};

  return bb;
}
TEST(SurroundingVehicles_HandleVisualGroundTruthInconsistencies, RemoveAgentIfVisibleButNotInGroundTruthAgentIdVector)
{
  // Mock objects need to be created on heap, because they are potentially deleted during the test
  auto visibleVehicle = new NiceMock<FakeSurroundingVehicle>;
  auto invisibleVehicle = new NiceMock<FakeSurroundingVehicle>;

  const std::vector<int> visibleAgentIds{123};
  ON_CALL(*visibleVehicle, GetId()).WillByDefault(Return(123));
  ON_CALL(*invisibleVehicle, GetId()).WillByDefault(Return(456));
  ON_CALL(*visibleVehicle, GetBoundingBox()).WillByDefault(Return(Scm::BoundingBox()));
  ON_CALL(*invisibleVehicle, GetBoundingBox()).WillByDefault(Return(Scm::BoundingBox()));
  ON_CALL(*visibleVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::EGO_FRONT));
  ON_CALL(*invisibleVehicle, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::EGO_REAR));

  NiceMock<FakeMentalModel> fakeMentalModel;
  ON_CALL(fakeMentalModel, IsVisible(_, _)).WillByDefault(Return(true));

  TestSurroundingVehicles testSurroundingVehicles;
  testSurroundingVehicles.SetSurroundingVehicles({visibleVehicle, invisibleVehicle});

  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  const auto removedAois{surroundingVehiclesModifier.RemoveVisibleVehiclesNotPresentInGroundTruth(visibleAgentIds, &fakeMentalModel)};

  const auto& resultingVehicles = testSurroundingVehicles.Get();

  ASSERT_EQ(resultingVehicles.size(), 1);
  ASSERT_EQ(resultingVehicles.front()->GetId(), 123);
  ASSERT_EQ(removedAois.size(), 1);
  ASSERT_EQ(removedAois.front(), AreaOfInterest::EGO_REAR);

  delete (visibleVehicle);
  // InvisibleVehicle is deleted during test
}

TEST(SurroundingVehicles_HandleVisualGroundTruthInconsistencies, DoNotRemoveAgentIfNotVisible)
{
  // Mock objects need to be created on heap, because they are potentially deleted during the test
  auto visibleVehicle = new NiceMock<FakeSurroundingVehicle>;
  auto invisibleVehicle = new NiceMock<FakeSurroundingVehicle>;

  const std::vector<int> visibleAgentIds{123};

  ON_CALL(*visibleVehicle, GetId()).WillByDefault(Return(123));
  const auto visibleBoundingBox{CREATE_FAKE_BOUNDING_BOX(2._m, 0._m)};
  ON_CALL(*visibleVehicle, GetBoundingBox()).WillByDefault(Return(visibleBoundingBox));

  ON_CALL(*invisibleVehicle, GetId()).WillByDefault(Return(456));
  const auto invisibleBoundingBox{CREATE_FAKE_BOUNDING_BOX(3._m, 0._m)};
  ON_CALL(*invisibleVehicle, GetBoundingBox()).WillByDefault(Return(invisibleBoundingBox));

  NiceMock<FakeMentalModel> fakeMentalModel;
  ON_CALL(fakeMentalModel, IsVisible(visibleBoundingBox, _)).WillByDefault(Return(true));
  ON_CALL(fakeMentalModel, IsVisible(invisibleBoundingBox, _)).WillByDefault(Return(false));

  TestSurroundingVehicles testSurroundingVehicles;
  testSurroundingVehicles.SetSurroundingVehicles({visibleVehicle, invisibleVehicle});

  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  const auto removedAois{surroundingVehiclesModifier.RemoveVisibleVehiclesNotPresentInGroundTruth(visibleAgentIds, &fakeMentalModel)};
  const auto& resultingVehicles = testSurroundingVehicles.Get();

  ASSERT_EQ(resultingVehicles.size(), 2);
  ASSERT_EQ(removedAois.size(), 0);

  delete (visibleVehicle);
  delete (invisibleVehicle);
}

/*******************************************************************
 * CHECK SurroundingVehicles DetectInconsistenciesFromExtrapolation *
 ********************************************************************/

class SurroundingVehicles_TestDetectInconsistenciesFromExtrapolation : public ::testing::Test
{
public:
};

TEST_F(SurroundingVehicles_TestDetectInconsistenciesFromExtrapolation, AoiIsReturnedIfContainedVehicleIsOverlapping)
{
  TestSurroundingVehicles testSurroundingVehicles;

  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle1;
  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle2;
  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle3;

  ON_CALL(fakeSurroundingVehicle1, Overlaps(_)).WillByDefault(Return(false));

  ON_CALL(fakeSurroundingVehicle2, GetId()).WillByDefault(Return(2));
  ON_CALL(fakeSurroundingVehicle2, Overlaps(::testing::Property(&SurroundingVehicleInterface::GetId, 3))).WillByDefault(Return(true));

  ON_CALL(fakeSurroundingVehicle3, GetId()).WillByDefault(Return(3));
  ON_CALL(fakeSurroundingVehicle3, Overlaps(::testing::Property(&SurroundingVehicleInterface::GetId, 2))).WillByDefault(Return(true));

  testSurroundingVehicles.SetSurroundingVehicles({&fakeSurroundingVehicle1, &fakeSurroundingVehicle2, &fakeSurroundingVehicle3});
  AoiVehicleMapping aoiMapping{{AreaOfInterest::LEFT_FRONT, {&fakeSurroundingVehicle1}},
                               {AreaOfInterest::EGO_FRONT, {&fakeSurroundingVehicle2}},
                               {AreaOfInterest::EGO_FRONT_FAR, {&fakeSurroundingVehicle3}}};

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  ON_CALL(fakeInfrastructureCharacteristics, CheckLaneExistence(_, _)).WillByDefault(Return(true));

  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  auto result = surroundingVehiclesModifier.DetectInconsistenciesFromExtrapolation(aoiMapping, fakeInfrastructureCharacteristics);

  EXPECT_THAT(result, ::testing::ElementsAre(AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_FRONT_FAR));
}

TEST_F(SurroundingVehicles_TestDetectInconsistenciesFromExtrapolation, AoiIsNotReturnedIfContainedVehicleIsOverlappingButInCollidedState)
{
  TestSurroundingVehicles testSurroundingVehicles;

  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle1;
  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle2;
  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle3;

  ON_CALL(fakeSurroundingVehicle1, Overlaps(_)).WillByDefault(Return(false));

  ON_CALL(fakeSurroundingVehicle2, GetId()).WillByDefault(Return(2));
  ON_CALL(fakeSurroundingVehicle2, Overlaps(::testing::Property(&SurroundingVehicleInterface::GetId, 3))).WillByDefault(Return(true));
  ON_CALL(fakeSurroundingVehicle2, IsCollided()).WillByDefault(Return(false));

  ON_CALL(fakeSurroundingVehicle3, GetId()).WillByDefault(Return(3));
  ON_CALL(fakeSurroundingVehicle3, Overlaps(::testing::Property(&SurroundingVehicleInterface::GetId, 2))).WillByDefault(Return(true));
  ON_CALL(fakeSurroundingVehicle3, IsCollided()).WillByDefault(Return(true));

  testSurroundingVehicles.SetSurroundingVehicles({&fakeSurroundingVehicle1, &fakeSurroundingVehicle2, &fakeSurroundingVehicle3});
  AoiVehicleMapping aoiMapping{{AreaOfInterest::LEFT_FRONT, {&fakeSurroundingVehicle1}},
                               {AreaOfInterest::EGO_FRONT, {&fakeSurroundingVehicle2}},
                               {AreaOfInterest::EGO_FRONT_FAR, {&fakeSurroundingVehicle3}}};

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  ON_CALL(fakeInfrastructureCharacteristics, CheckLaneExistence(_, _)).WillByDefault(Return(true));

  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  auto result = surroundingVehiclesModifier.DetectInconsistenciesFromExtrapolation(aoiMapping, fakeInfrastructureCharacteristics);

  EXPECT_THAT(result, ::testing::ElementsAre(AreaOfInterest::EGO_FRONT));
}

TEST_F(SurroundingVehicles_TestDetectInconsistenciesFromExtrapolation, AoiIsReturnedIfContainedVehicleIsOnNonExistingLaneAndLastKnownAoiIsValid)
{
  TestSurroundingVehicles testSurroundingVehicles;

  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle1;
  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle2;
  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle3;

  ON_CALL(fakeSurroundingVehicle1, Overlaps(_)).WillByDefault(Return(false));
  ON_CALL(fakeSurroundingVehicle2, Overlaps(_)).WillByDefault(Return(false));
  ON_CALL(fakeSurroundingVehicle3, Overlaps(_)).WillByDefault(Return(false));

  ON_CALL(fakeSurroundingVehicle1, IsCollided()).WillByDefault(Return(false));
  ON_CALL(fakeSurroundingVehicle2, IsCollided()).WillByDefault(Return(false));
  ON_CALL(fakeSurroundingVehicle3, IsCollided()).WillByDefault(Return(false));

  testSurroundingVehicles.SetSurroundingVehicles({&fakeSurroundingVehicle1, &fakeSurroundingVehicle2, &fakeSurroundingVehicle3});
  AoiVehicleMapping aoiMapping{{AreaOfInterest::LEFT_FRONT, {&fakeSurroundingVehicle1}},
                               {AreaOfInterest::RIGHTRIGHT_FRONT, {&fakeSurroundingVehicle2}},
                               {AreaOfInterest::EGO_FRONT, {&fakeSurroundingVehicle3}}};

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;
  ON_CALL(fakeInfrastructureCharacteristics, CheckLaneExistence(_, _)).WillByDefault(Return(true));
  ON_CALL(fakeInfrastructureCharacteristics, CheckLaneExistence(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(false));
  ON_CALL(fakeInfrastructureCharacteristics, CheckLaneExistence(AreaOfInterest::RIGHTRIGHT_FRONT, _)).WillByDefault(Return(false));

  ON_CALL(fakeSurroundingVehicle1, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::LEFT_FRONT));
  ON_CALL(fakeSurroundingVehicle2, GetAssignedAoi()).WillByDefault(Return(AreaOfInterest::RIGHT_FRONT));

  SurroundingVehiclesModifier surroundingVehiclesModifier(testSurroundingVehicles);
  auto result = surroundingVehiclesModifier.DetectInconsistenciesFromExtrapolation(aoiMapping, fakeInfrastructureCharacteristics);

  EXPECT_THAT(result, ::testing::ElementsAre(AreaOfInterest::RIGHT_FRONT));
}

/**************************************************
 * CHECK SurroundingVehicles UpdateVisibleVehicles *
 ***************************************************/

struct DataFor_TestUpdateVisibleVehicles
{
  units::length::meter_t relativeLongitudinalDistance;
  units::angle::radian_t opticalAnglePositionHorizontal;
  units::length::meter_t groundTruthDistance;
  units::length::meter_t mentalDistance;
  bool expectedUpdate;
};

class SurroundingVehicles_TestUpdateVisibleVehicles_LongitudinalDifferences : public ::testing::Test,
                                                                              public ::testing::WithParamInterface<DataFor_TestUpdateVisibleVehicles>
{
};

TEST_P(SurroundingVehicles_TestUpdateVisibleVehicles_LongitudinalDifferences, LongidutinalDistanceDiffersTooMuch_LeadsToUpdate)
{
  auto data = GetParam();
  ObjectInformationScmExtended gtVehicle;
  NiceMock<FakeSurroundingVehicle> perceivedSurroundingVehicle;

  gtVehicle.relativeLongitudinalDistance = data.relativeLongitudinalDistance;
  gtVehicle.width = 2.0_m;
  gtVehicle.opticalAnglePositionHorizontal = data.opticalAnglePositionHorizontal / 180.0 * M_PI;

  // set equal lateral distance, this test checks for longitudinal differences
  gtVehicle.obstruction.mainLaneLocator = 1.0_m;
  ON_CALL(perceivedSurroundingVehicle, GetRelativeLateralPosition()).WillByDefault(Return(gtVehicle.obstruction.mainLaneLocator));

  gtVehicle.longitudinalObstruction.mainLaneLocator = data.groundTruthDistance;
  ON_CALL(perceivedSurroundingVehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(data.mentalDistance));

  SurroundingVehicles surroundingVehicles;
  SurroundingVehiclesModifier surroundingVehiclesModifier(surroundingVehicles);

  auto result = surroundingVehiclesModifier.ShouldUpdateDueDetectedPeripheralInaccuracy(&gtVehicle, &perceivedSurroundingVehicle);
  ASSERT_EQ(result, data.expectedUpdate);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SurroundingVehicles_TestUpdateVisibleVehicles_LongitudinalDifferences,
    testing::Values(
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 0.0_m,
            .opticalAnglePositionHorizontal = 61.0_rad,
            .groundTruthDistance = 222.0_m,
            .mentalDistance = 1.0_m,
            .expectedUpdate = true},
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 0.0_m,
            .opticalAnglePositionHorizontal = 61.0_rad,
            .groundTruthDistance = 1.1_m,
            .mentalDistance = 1.0_m,
            .expectedUpdate = false},
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 0.0_m,
            .opticalAnglePositionHorizontal = 61.0_rad,
            .groundTruthDistance = 2.0_m,
            .mentalDistance = 1.0_m,
            .expectedUpdate = true},
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 50.0_m,
            .opticalAnglePositionHorizontal = 61.0_rad,
            .groundTruthDistance = 2.0_m,
            .mentalDistance = 1.0_m,
            .expectedUpdate = false},
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 10.0_m,
            .opticalAnglePositionHorizontal = 61.0_rad,
            .groundTruthDistance = 1.0_m,
            .mentalDistance = 5.0_m,
            .expectedUpdate = true},
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 10.0_m,
            .opticalAnglePositionHorizontal = 120.0_rad,
            .groundTruthDistance = 1.0_m,
            .mentalDistance = 5.0_m,
            .expectedUpdate = false},
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 0.0_m,
            .opticalAnglePositionHorizontal = 61.0_rad,
            .groundTruthDistance = 1.0_m,
            .mentalDistance = 1.0_m,
            .expectedUpdate = false}));

class SurroundingVehicles_TestUpdateVisibleVehicles_LateralDifferences : public ::testing::Test,
                                                                         public ::testing::WithParamInterface<DataFor_TestUpdateVisibleVehicles>
{
};
TEST_P(SurroundingVehicles_TestUpdateVisibleVehicles_LateralDifferences, LateralDistanceDiffersTooMuch_LeadsToUpdate)
{
  auto data = GetParam();
  ObjectInformationScmExtended gtVehicle;
  NiceMock<FakeSurroundingVehicle> perceivedSurroundingVehicle;

  gtVehicle.relativeLongitudinalDistance = data.relativeLongitudinalDistance;
  gtVehicle.width = 2.0_m;
  gtVehicle.opticalAnglePositionHorizontal = data.opticalAnglePositionHorizontal / 180.0 * M_PI;

  gtVehicle.obstruction.mainLaneLocator = data.groundTruthDistance;
  ON_CALL(perceivedSurroundingVehicle, GetRelativeLateralPosition()).WillByDefault(Return(data.mentalDistance));

  // set equal longitudinal distance, this test checks for lateral differences
  gtVehicle.longitudinalObstruction.mainLaneLocator = 1.0_m;
  ON_CALL(perceivedSurroundingVehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(gtVehicle.longitudinalObstruction.mainLaneLocator));

  SurroundingVehicles surroundingVehicles;
  SurroundingVehiclesModifier surroundingVehiclesModifier(surroundingVehicles);

  auto result = surroundingVehiclesModifier.ShouldUpdateDueDetectedPeripheralInaccuracy(&gtVehicle, &perceivedSurroundingVehicle);
  ASSERT_EQ(result, data.expectedUpdate);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SurroundingVehicles_TestUpdateVisibleVehicles_LateralDifferences,
    testing::Values(
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 0.0_m,
            .opticalAnglePositionHorizontal = 61.0_rad,
            .groundTruthDistance = 222.0_m,
            .mentalDistance = 1.0_m,
            .expectedUpdate = true},
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 0.0_m,
            .opticalAnglePositionHorizontal = 61.0_rad,
            .groundTruthDistance = 1.1_m,
            .mentalDistance = 1.0_m,
            .expectedUpdate = false},
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 10.0_m,
            .opticalAnglePositionHorizontal = 61.0_rad,
            .groundTruthDistance = 2.0_m,
            .mentalDistance = 1.0_m,
            .expectedUpdate = true},
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 30.0_m,
            .opticalAnglePositionHorizontal = 61.0_rad,
            .groundTruthDistance = 2.0_m,
            .mentalDistance = 1.0_m,
            .expectedUpdate = false},
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 0.0_m,
            .opticalAnglePositionHorizontal = 61.0_rad,
            .groundTruthDistance = 2.0_m,
            .mentalDistance = 1.0_m,
            .expectedUpdate = true},
        DataFor_TestUpdateVisibleVehicles{
            .relativeLongitudinalDistance = 0.0_m,
            .opticalAnglePositionHorizontal = 180.0_rad,
            .groundTruthDistance = 2.0_m,
            .mentalDistance = 1.0_m,
            .expectedUpdate = false}));

TEST(SurroundingVehicles_TestUpdateVisibleVehicles, UpdateNeededBasedOnOpticalAxis)
{
  ObjectInformationScmExtended gtVehicle;
  NiceMock<FakeSurroundingVehicle> perceivedSurroundingVehicle;

  gtVehicle.relativeLongitudinalDistance = 0.0_m;  // -> current distance
  gtVehicle.width = 2.0_m;
  gtVehicle.opticalAnglePositionHorizontal = 61.0_rad / 180 * M_PI;  // good peripheral vision, almost ufov

  gtVehicle.obstruction.mainLaneLocator = 1.0_m;  // lateral
  ON_CALL(perceivedSurroundingVehicle, GetRelativeLateralPosition()).WillByDefault(Return(1.0_m));

  gtVehicle.longitudinalObstruction.mainLaneLocator = 2.0_m;
  ON_CALL(perceivedSurroundingVehicle, GetRelativeLongitudinalPosition()).WillByDefault(Return(1.0_m));

  SurroundingVehicles surroundingVehicles;
  SurroundingVehiclesModifier surroundingVehiclesModifier(surroundingVehicles);

  auto result = surroundingVehiclesModifier.ShouldUpdateDueDetectedPeripheralInaccuracy(&gtVehicle, &perceivedSurroundingVehicle);
  ASSERT_TRUE(result);

  gtVehicle.opticalAnglePositionHorizontal = 190.0_rad / 180 * M_PI;  // near end of peripheral perception; barely visible in periphery

  result = surroundingVehiclesModifier.ShouldUpdateDueDetectedPeripheralInaccuracy(&gtVehicle, &perceivedSurroundingVehicle);
  ASSERT_FALSE(result);
}