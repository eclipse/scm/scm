/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeLaneChangeBehavior.h"
#include "../Fakes/FakeLaneChangeBehaviorQuery.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeStochastics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "module/driver/src/InfrastructureCharacteristicsInterface.h"
#include "module/driver/src/LaneChangeBehavior.h"
#include "module/driver/src/MicroscopicCharacteristics.h"
#include "module/driver/src/ScmCommons.h"
#include "module/driver/src/ScmDefinitions.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

struct LaneChangeBehaviorTester
{
  class LaneChangeBehaviorUnderTest : LaneChangeBehavior
  {
  public:
    template <typename... Args>
    LaneChangeBehaviorUnderTest(Args&&... args)
        : LaneChangeBehavior{std::forward<Args>(args)...} {};

    using LaneChangeBehavior::CalculateBaseLaneConvenience;
    using LaneChangeBehavior::CalculateIntensity;
    using LaneChangeBehavior::CalculateLaneIntensities;
    using LaneChangeBehavior::CalculateLongitudinalVelocityDelta;
    using LaneChangeBehavior::CalculateVelocityDeltaTimeAtTargetSpeed;
    using LaneChangeBehavior::CalculateVelocityDeltaToSlowBlockerDelta;
    using LaneChangeBehavior::CalculateVelocityToLaneMeanDelta;
    using LaneChangeBehavior::CheckIsReplacementsEmpty;
    using LaneChangeBehavior::CheckLaneIsDenied;
    using LaneChangeBehavior::DetermineBaseLaneConvenienceFromParameters;
    using LaneChangeBehavior::FindMaximumReplacementVelocity;
    using LaneChangeBehavior::GetLaneChangeWish;
    using LaneChangeBehavior::IsTimeAtTargetSpeed;
  };

  LaneChangeBehaviorTester()
      : fakeStochastics{},
        fakeMentalModel{},
        fakeFeatureExtractor{},
        fakeMentalCalculations{},
        fakeInfrastructureCharacteristics{},
        laneChangeBehavior(fakeLaneChangeBehaviorQuery,
                           fakeMentalModel)
  {
  }

  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  std::shared_ptr<FakeLaneChangeBehaviorQuery> fakeLaneChangeBehaviorQuery = std::make_shared<FakeLaneChangeBehaviorQuery>();
  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;

  LaneChangeBehaviorUnderTest laneChangeBehavior;
};

/*****************************************
 * CHECK CalculateTimeAtTargetSpeedDelta *
 *****************************************/

TEST(LaneChangeBehavior, CalculateTimeAtTargetSpeedDelta_Without_Agent_In_Front)
{
  LaneChangeBehaviorTester TEST_HELPER;
  /* Set up mocks */
  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, GetTimeAtTargetSpeedInLane(_)).WillByDefault(Return(99.0_s));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::EGO, _)).WillByDefault(Return(200._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(300_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(20._mps));

  auto result = TEST_HELPER.laneChangeBehavior.CalculateVelocityDeltaTimeAtTargetSpeed(SurroundingLane::EGO);

  ASSERT_EQ(result, 10_mps);
}

TEST(LaneChangeBehavior, CalculateTimeAtTargetSpeedDelta_With_Agent_In_Front)
{
  LaneChangeBehaviorTester TEST_HELPER;
  /* Set up mocks */

  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, GetTimeAtTargetSpeedInLane(_)).WillByDefault(Return(19.0_s));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::RIGHT, _)).WillByDefault(Return(400._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(300_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(testing::Matcher<AreaOfInterest>(testing::_))).WillByDefault(Return(150._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocity(_, _)).WillByDefault(Return(25._mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(15._mps));
  auto result = TEST_HELPER.laneChangeBehavior.CalculateVelocityDeltaTimeAtTargetSpeed(SurroundingLane::RIGHT);

  ASSERT_EQ(result, 17.5_mps);
}

TEST(LaneChangeBehavior, CalculateTimeAtTargetSpeedDelta_With_Obstacle_On_Lane_And_Lane_Ends)
{
  LaneChangeBehaviorTester TEST_HELPER;
  /* Set up mocks */
  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, GetTimeAtTargetSpeedInLane(_)).WillByDefault(Return(19.0_s));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::RIGHT, _)).WillByDefault(Return(200._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(300_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(20._mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::EGO, _)).WillByDefault(Return(200._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(300_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(testing::Matcher<AreaOfInterest>(testing::_))).WillByDefault(Return(150._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocity(_, _)).WillByDefault(Return(25._mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(15._mps));
  auto result = TEST_HELPER.laneChangeBehavior.CalculateVelocityDeltaTimeAtTargetSpeed(SurroundingLane::RIGHT);

  ASSERT_EQ(result, 17.5_mps);
}

TEST(LaneChangeBehavior, CalculateTimeAtTargetSpeedDelta_With_Obstacle_On_Lane_And_Lane_Ends2)
{
  LaneChangeBehaviorTester TEST_HELPER;
  /* Set up mocks */
  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, GetTimeAtTargetSpeedInLane(_)).WillByDefault(Return(19.0_s));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::RIGHT, _)).WillByDefault(Return(200._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(300_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(20._mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::EGO_FRONT)).WillByDefault(Return(50._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocity(_, _)).WillByDefault(Return(20._mps));
  auto result = TEST_HELPER.laneChangeBehavior.CalculateVelocityDeltaTimeAtTargetSpeed(SurroundingLane::RIGHT);

  ASSERT_EQ(result, 10_mps);
}

/********************************************
 * CHECK CalculateLongitudinalVelocityDelta *
 ********************************************/

TEST(LaneChangeBehavior, CalculateLongitudinalVelocityDelta_Distance_To_End_Of_Lane_Greater_InfluencingDistanceToEndOfLane)
{
  LaneChangeBehaviorTester TEST_HELPER;

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleLength()).WillByDefault(Return(5._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::EGO, _)).WillByDefault(Return(6._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(5._m));

  auto result = TEST_HELPER.laneChangeBehavior.CalculateLongitudinalVelocityDelta(SurroundingLane::EGO);
  ASSERT_EQ(result, 0_mps);
}

TEST(LaneChangeBehavior, CalculateLongitudinalVelocityDelta_Distance_To_End_Of_Lane_Lesser_InfluencingDistanceToEndOfLane)
{
  LaneChangeBehaviorTester TEST_HELPER;

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleLength()).WillByDefault(Return(5._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::EGO, _)).WillByDefault(Return(5._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(5._m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleLength(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(5._m));
  InfrastructureCharacteristics infrastructureCharacteristics;
  infrastructureCharacteristics.UpdateGeometryInformation()->Close().distanceToEndOfLane = 5.0_m;
  infrastructureCharacteristics.UpdateGeometryInformation()->Close().width = 2.5_m;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&infrastructureCharacteristics));

  DriverParameters driverParameters;
  driverParameters.comfortLateralAcceleration = 50_mps_sq;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(false)).WillByDefault(Return(4_mps));

  auto result = TEST_HELPER.laneChangeBehavior.CalculateLongitudinalVelocityDelta(SurroundingLane::EGO);
  ASSERT_NEAR(result.value(), 3.7, 0.1);
}

/**************************************************
 * CHECK CalculateVelocityDeltaToSlowBlockerDelta *
 **************************************************/

TEST(LaneChangeBehavior, CalculateVelocityDeltaToSlowBlockerDelta)
{
  LaneChangeBehaviorTester TEST_HELPER;

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(5._mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(10._mps));
  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, CanVehicleCauseCollisionCourseForNonSideAreas(_)).WillByDefault(Return(true));
  auto result = TEST_HELPER.laneChangeBehavior.CalculateVelocityDeltaToSlowBlockerDelta(SurroundingLane::EGO);

  ASSERT_EQ(result, 5_mps);
}

TEST(LaneChangeBehavior, CalculateVelocityDeltaToSlowBlockerDelta_Without_Slow_Blocker)
{
  LaneChangeBehaviorTester TEST_HELPER;

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(5._mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(5._mps));
  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, CanVehicleCauseCollisionCourseForNonSideAreas(_)).WillByDefault(Return(false));

  auto result = TEST_HELPER.laneChangeBehavior.CalculateVelocityDeltaToSlowBlockerDelta(SurroundingLane::EGO);

  ASSERT_EQ(result, 0_mps);
}

/******************************************
 * CHECK CalculateVelocityToLaneMeanDelta *
 ******************************************/

TEST(LaneChangeBehavior, CalculateVelocityToLaneMeanDelta)
{
  LaneChangeBehaviorTester TEST_HELPER;

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(_)).WillByDefault(Return(5._mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(10._mps));
  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, CanVehicleCauseCollisionCourseForNonSideAreas(_)).WillByDefault(Return(false));

  auto result = TEST_HELPER.laneChangeBehavior.CalculateVelocityToLaneMeanDelta(SurroundingLane::EGO);

  ASSERT_EQ(result, 5_mps);
}

/**********************************
 * CHECK CalculateLaneIntensities *
 **********************************/

/// \brief Data table for definition of individual test cases
struct DataFor_CalculateLaneIntensities
{
  units::length::meter_t distanceToEndOfLaneEgo;
  units::length::meter_t distanceToEndOfLaneLeft;
  units::length::meter_t distanceToEndOfLaneRight;
  bool rightOvertakeProhibition;
};

class LaneChangeBehavior_CalculateLaneIntensities : public ::testing::Test,
                                                    public ::testing::WithParamInterface<DataFor_CalculateLaneIntensities>
{
};

TEST_P(LaneChangeBehavior_CalculateLaneIntensities, Check_CalculateLaneIntensities)
{
  LaneChangeBehaviorTester TEST_HELPER;

  DataFor_CalculateLaneIntensities data = GetParam();

  LaneChangeIntensities intensities{
      .EgoLaneIntensity = 0.4,
      .LeftLaneIntensity = 1.0,
      .RightLaneIntensity = 1.0};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::EGO, _)).WillByDefault(Return(data.distanceToEndOfLaneEgo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::LEFT, _)).WillByDefault(Return(data.distanceToEndOfLaneLeft));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::RIGHT, _)).WillByDefault(Return(data.distanceToEndOfLaneRight));
  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, EgoFitsInLane(_)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(300_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(_)).WillByDefault(Return(ScmDefinitions::INF_DISTANCE));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(false));

  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, GetTimeAtTargetSpeedInLane(_)).WillByDefault(Return(15_s));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(10_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(15_mps));

  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, CalculateLaneChangeIntensity(_, _)).WillByDefault(Return(0.4));

  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, IsOuterLaneOvertakingProhibited(_)).WillByDefault(Return(data.rightOvertakeProhibition));

  EXPECT_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, AdjustLaneConvenienceDueToHighwayExit(_, _)).WillOnce(Return(intensities));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetEgoLaneKeepingQuotaFulfilled()).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsMesoscopicSituationActive(MesoscopicSituation::CURRENT_LANE_BLOCKED)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetOuterKeepingQuotaFulfilled()).WillByDefault(Return(false));

  EXPECT_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, NormalizeLaneChangeIntensities(_)).WillOnce(Return(intensities));

  TrafficRulesScm trafficRules;
  trafficRules.common.rightHandTraffic.value = true;
  TEST_HELPER.laneChangeBehavior.CalculateLaneIntensities(trafficRules);
  const auto result = TEST_HELPER.laneChangeBehavior.GetLaneChangeWish();
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehavior_CalculateLaneIntensities,
    testing::Values(
        DataFor_CalculateLaneIntensities{
            .distanceToEndOfLaneEgo = 200.0_m,
            .distanceToEndOfLaneLeft = 1000.0_m,
            .distanceToEndOfLaneRight = 1000.0_m,
            .rightOvertakeProhibition = false},
        DataFor_CalculateLaneIntensities{
            .distanceToEndOfLaneEgo = 1000.0_m,
            .distanceToEndOfLaneLeft = 200.0_m,
            .distanceToEndOfLaneRight = 1000.0_m,
            .rightOvertakeProhibition = false},
        DataFor_CalculateLaneIntensities{
            .distanceToEndOfLaneEgo = 1000.0_m,
            .distanceToEndOfLaneLeft = 1000.0_m,
            .distanceToEndOfLaneRight = 200.0_m,
            .rightOvertakeProhibition = false},
        DataFor_CalculateLaneIntensities{
            .distanceToEndOfLaneEgo = 1000.0_m,
            .distanceToEndOfLaneLeft = 1000.0_m,
            .distanceToEndOfLaneRight = 200.0_m,
            .rightOvertakeProhibition = true}));

/****************************************************
 * CHECK DetermineBaseLaneConvenienceFromParameters *
 ****************************************************/

struct DataFor_DetermineBaseLaneConvenienceFromParameters
{
  LaneChangeIntensities intensities;
  units::velocity::meters_per_second_t meanVelocity;
  LaneChangeIntensities expectedIntensities;
};

class LaneChangeBehavior_DetermineBaseLaneConvenienceFromParameters : public ::testing::Test,
                                                                      public ::testing::WithParamInterface<DataFor_DetermineBaseLaneConvenienceFromParameters>
{
};

TEST_P(LaneChangeBehavior_DetermineBaseLaneConvenienceFromParameters, Check_DetermineBaseLaneConvenienceFromParameters)
{
  LaneChangeBehaviorTester TEST_HELPER;

  DataFor_DetermineBaseLaneConvenienceFromParameters data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::EGO, _)).WillByDefault(Return(400_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::LEFT, _)).WillByDefault(Return(400_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::RIGHT, _)).WillByDefault(Return(400_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(300_m));

  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, GetTimeAtTargetSpeedInLane(_)).WillByDefault(Return(99.0_s));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocity(_, _)).WillByDefault(Return(30_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(20_mps));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));
  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, CanVehicleCauseCollisionCourseForNonSideAreas(_)).WillByDefault(Return(false));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(_)).WillByDefault(Return(data.meanVelocity));

  auto result = TEST_HELPER.laneChangeBehavior.DetermineBaseLaneConvenienceFromParameters(data.intensities);

  ASSERT_EQ(result.EgoLaneIntensity, data.expectedIntensities.EgoLaneIntensity);
  ASSERT_EQ(result.LeftLaneIntensity, data.expectedIntensities.LeftLaneIntensity);
  ASSERT_EQ(result.RightLaneIntensity, data.expectedIntensities.RightLaneIntensity);
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehavior_DetermineBaseLaneConvenienceFromParameters,
    testing::Values(
        DataFor_DetermineBaseLaneConvenienceFromParameters{
            .intensities{.EgoLaneIntensity = -1,
                         .LeftLaneIntensity = -1,
                         .RightLaneIntensity = -1},
            .meanVelocity = 40.0_mps,
            .expectedIntensities{.EgoLaneIntensity = 1,
                                 .LeftLaneIntensity = 0,
                                 .RightLaneIntensity = 0}},
        DataFor_DetermineBaseLaneConvenienceFromParameters{
            .intensities{.EgoLaneIntensity = 1,
                         .LeftLaneIntensity = -1,
                         .RightLaneIntensity = 0},
            .meanVelocity = 40.0_mps,
            .expectedIntensities{.EgoLaneIntensity = 1,
                                 .LeftLaneIntensity = 0,
                                 .RightLaneIntensity = 0}},
        DataFor_DetermineBaseLaneConvenienceFromParameters{
            .intensities{.EgoLaneIntensity = -1,
                         .LeftLaneIntensity = 0,
                         .RightLaneIntensity = -1},
            .meanVelocity = 40.0_mps,
            .expectedIntensities{.EgoLaneIntensity = 0,
                                 .LeftLaneIntensity = 1,
                                 .RightLaneIntensity = 0}},
        DataFor_DetermineBaseLaneConvenienceFromParameters{
            .intensities{.EgoLaneIntensity = -1,
                         .LeftLaneIntensity = 0,
                         .RightLaneIntensity = -1},
            .meanVelocity = 20.0_mps,
            .expectedIntensities{.EgoLaneIntensity = 1,
                                 .LeftLaneIntensity = 0,
                                 .RightLaneIntensity = 0}}));

/***************************
 * CHECK CheckLaneIsDenied *
 ***************************/

struct DataFor_CheckLaneIsDenied
{
  double inIntensitiy;
  double expectedResult;
};

class LaneChangeBehavior_CheckLaneIsDenied : public ::testing::Test,
                                             public ::testing::WithParamInterface<DataFor_CheckLaneIsDenied>
{
};

TEST_P(LaneChangeBehavior_CheckLaneIsDenied, Check_LaneIsDenied)
{
  LaneChangeBehaviorTester TEST_HELPER;

  DataFor_CheckLaneIsDenied data = GetParam();

  auto result = TEST_HELPER.laneChangeBehavior.CheckLaneIsDenied(data.inIntensitiy);

  ASSERT_EQ(data.expectedResult, result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehavior_CheckLaneIsDenied,
    testing::Values(
        DataFor_CheckLaneIsDenied{
            .inIntensitiy = -1,
            .expectedResult = 0},
        DataFor_CheckLaneIsDenied{
            .inIntensitiy = 1,
            .expectedResult = 1}));

/**********************************
 * CHECK CheckIsReplacementsEmpty *
 **********************************/

struct DataFor_CheckIsReplacementsEmpty
{
  std::vector<units::velocity::meters_per_second_t> replacements;
  double outIntensity;
  double expectedResult;
};

class LaneChangeBehavior_CheckIsReplacementsEmpty : public ::testing::Test,
                                                    public ::testing::WithParamInterface<DataFor_CheckIsReplacementsEmpty>
{
};

TEST_P(LaneChangeBehavior_CheckIsReplacementsEmpty, Check_IsReplacementsEmpty)
{
  LaneChangeBehaviorTester TEST_HELPER;
  DataFor_CheckIsReplacementsEmpty data = GetParam();

  auto result = TEST_HELPER.laneChangeBehavior.CheckIsReplacementsEmpty(data.replacements, data.outIntensity);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehavior_CheckIsReplacementsEmpty,
    testing::Values(
        DataFor_CheckIsReplacementsEmpty{
            .replacements{},
            .outIntensity = 1,
            .expectedResult = 0},
        DataFor_CheckIsReplacementsEmpty{
            .replacements{1.0_mps, 2.0_mps},
            .outIntensity = 1,
            .expectedResult = 1}));

/*******************************************
 * CHECK FindMaximumReplacementIntensities *
 *******************************************/

struct DataFor_FindMaximumReplacementIntensities
{
  std::vector<units::velocity::meters_per_second_t> replacements;
  units::velocity::meters_per_second_t expectedResult;
};

class LaneChangeBehavior_FindMaximumReplacementIntensities : public ::testing::Test,
                                                             public ::testing::WithParamInterface<DataFor_FindMaximumReplacementIntensities>
{
};

TEST_P(LaneChangeBehavior_FindMaximumReplacementIntensities, Check_FindMaximumReplacementIntensities)
{
  LaneChangeBehaviorTester TEST_HELPER;
  DataFor_FindMaximumReplacementIntensities data = GetParam();

  auto result = TEST_HELPER.laneChangeBehavior.FindMaximumReplacementVelocity(data.replacements);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehavior_FindMaximumReplacementIntensities,
    testing::Values(
        DataFor_FindMaximumReplacementIntensities{
            .replacements{},
            .expectedResult = 0_mps},
        DataFor_FindMaximumReplacementIntensities{
            .replacements{1.0_mps, 2.0_mps},
            .expectedResult = 2.0_mps}));

/****************************
 * CHECK CalculateIntensity *
 ****************************/

struct DataFor_CalculateIntensity
{
  std::vector<units::velocity::meters_per_second_t> velocityAdjustments;
  units::velocity::meters_per_second_t maxAdjustment;
  double expectedResult;
};

class LaneChangeBehavior_CalculateIntensity : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_CalculateIntensity>
{
};

TEST_P(LaneChangeBehavior_CalculateIntensity, Check_CalculateIntensity)
{
  LaneChangeBehaviorTester TEST_HELPER;
  DataFor_CalculateIntensity data = GetParam();

  auto result = TEST_HELPER.laneChangeBehavior.CalculateIntensity(data.velocityAdjustments, data.maxAdjustment);
  ASSERT_EQ(result, data.expectedResult);
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehavior_CalculateIntensity,
    testing::Values(
        DataFor_CalculateIntensity{
            .velocityAdjustments{},
            .maxAdjustment = 1_mps,
            .expectedResult = 0},
        DataFor_CalculateIntensity{
            .velocityAdjustments{1.0_mps, 2.0_mps},
            .maxAdjustment = 0_mps,
            .expectedResult = 0},
        DataFor_CalculateIntensity{
            .velocityAdjustments{1.0_mps, 2.0_mps},
            .maxAdjustment = 0.5_mps,
            .expectedResult = 0},
        DataFor_CalculateIntensity{
            .velocityAdjustments{1.0_mps, -2.0_mps},
            .maxAdjustment = 0.5_mps,
            .expectedResult = 5.0}));

/**************************************
 * CHECK CalculateBaseLaneConvenience *
 **************************************/

struct DataFor_CalculateBaseLaneConvenience
{
  units::length::meter_t distanceToEndOfLane;
  units::length::meter_t influencingDistance;
  units::length::meter_t distanceToObstacle;
  bool egoFitsInLane;
  bool isVehicleVisible;
  bool isInfluencingDistanceViolated;
  double expectedResult;
};

class LaneChangeBehavior_CalculateBaseLaneConvenience : public ::testing::Test,
                                                        public ::testing::WithParamInterface<DataFor_CalculateBaseLaneConvenience>
{
};

TEST_P(LaneChangeBehavior_CalculateBaseLaneConvenience, Check_CalculateBaseLaneConvenience)
{
  LaneChangeBehaviorTester TEST_HELPER;
  DataFor_CalculateBaseLaneConvenience data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(RelativeLane::LEFT, _)).WillByDefault(Return(data.distanceToEndOfLane));
  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, EgoFitsInLane(_)).WillByDefault(Return(data.egoFitsInLane));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(data.influencingDistance));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(_)).WillByDefault(Return(data.distanceToObstacle));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(data.isVehicleVisible));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));
  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, GetIsInfluencingDistanceViolated(_)).WillByDefault(Return(data.isInfluencingDistanceViolated));

  auto result = TEST_HELPER.laneChangeBehavior.CalculateBaseLaneConvenience(SurroundingLane::LEFT);
  ASSERT_DOUBLE_EQ(result, data.expectedResult);
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehavior_CalculateBaseLaneConvenience,
    testing::Values(
        DataFor_CalculateBaseLaneConvenience{
            .distanceToEndOfLane = 1000.0_m,
            .influencingDistance = 300.0_m,
            .distanceToObstacle = 200.0_m,
            .egoFitsInLane = false,
            .isVehicleVisible = false,
            .isInfluencingDistanceViolated = false,
            .expectedResult = -1},
        DataFor_CalculateBaseLaneConvenience{
            .distanceToEndOfLane = 1000.0_m,
            .influencingDistance = 300.0_m,
            .distanceToObstacle = 200.0_m,
            .egoFitsInLane = true,
            .isVehicleVisible = false,
            .isInfluencingDistanceViolated = false,
            .expectedResult = 1},
        DataFor_CalculateBaseLaneConvenience{
            .distanceToEndOfLane = 1000.0_m,
            .influencingDistance = 300.0_m,
            .distanceToObstacle = 200.0_m,
            .egoFitsInLane = true,
            .isVehicleVisible = true,
            .isInfluencingDistanceViolated = false,
            .expectedResult = 1},
        DataFor_CalculateBaseLaneConvenience{
            .distanceToEndOfLane = 1000.0_m,
            .influencingDistance = 300.0_m,
            .distanceToObstacle = 200.0_m,
            .egoFitsInLane = true,
            .isVehicleVisible = true,
            .isInfluencingDistanceViolated = true,
            .expectedResult = 0}));

/*****************************
 * CHECK IsTimeAtTargetSpeed *
 *****************************/

struct DataFor_IsTimeAtTargetSpeed
{
  units::time::second_t timeAtTargetSpeedInLane;
  units::length::meter_t distanceToEndOfLane;
  units::length::meter_t influencingDistanceToEndOfLane;
  bool expectedResult;
};

class LaneChangeBehavior_IsTimeAtTargetSpeed : public ::testing::Test,
                                               public ::testing::WithParamInterface<DataFor_IsTimeAtTargetSpeed>
{
};

TEST_P(LaneChangeBehavior_IsTimeAtTargetSpeed, Check_IsTimeAtTargetSpeed)
{
  LaneChangeBehaviorTester TEST_HELPER;
  DataFor_IsTimeAtTargetSpeed data = GetParam();

  ON_CALL(*TEST_HELPER.fakeLaneChangeBehaviorQuery, GetTimeAtTargetSpeedInLane(_)).WillByDefault(Return(data.timeAtTargetSpeedInLane));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(1, _)).WillByDefault(Return(data.distanceToEndOfLane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(data.influencingDistanceToEndOfLane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(20.0_mps));

  auto result = TEST_HELPER.laneChangeBehavior.IsTimeAtTargetSpeed(SurroundingLane::LEFT);
  ASSERT_EQ(result, data.expectedResult);
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehavior_IsTimeAtTargetSpeed,
    testing::Values(
        DataFor_IsTimeAtTargetSpeed{
            .timeAtTargetSpeedInLane = 90.0_s,
            .distanceToEndOfLane = 100_m,
            .influencingDistanceToEndOfLane = 100_m,
            .expectedResult = false},
        DataFor_IsTimeAtTargetSpeed{
            .timeAtTargetSpeedInLane = 15.0_s,
            .distanceToEndOfLane = 100_m,
            .influencingDistanceToEndOfLane = 100_m,
            .expectedResult = true},
        DataFor_IsTimeAtTargetSpeed{
            .timeAtTargetSpeedInLane = 99.0_s,
            .distanceToEndOfLane = 100_m,
            .influencingDistanceToEndOfLane = 300_m,
            .expectedResult = true},
        DataFor_IsTimeAtTargetSpeed{
            .timeAtTargetSpeedInLane = 99.0_s,
            .distanceToEndOfLane = 300_m,
            .influencingDistanceToEndOfLane = 100_m,
            .expectedResult = false}));