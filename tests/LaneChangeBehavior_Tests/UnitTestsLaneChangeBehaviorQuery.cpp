/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeStochastics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "Fakes/FakeLaneChangeDimension.h"
#include "ScmDefinitions.h"
#include "include/common/SensorDriverScmDefinitions.h"
#include "module/driver/src/InfrastructureCharacteristicsInterface.h"
#include "module/driver/src/LaneChangeBehavior.h"
#include "module/driver/src/MicroscopicCharacteristics.h"
#include "module/driver/src/ScmCommons.h"
#include "module/driver/src/ScmDefinitions.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

struct LaneChangeBehaviorQueryTester
{
  class LaneChangeBehaviorQueryUnderTest : LaneChangeBehaviorQuery
  {
  public:
    template <typename... Args>
    LaneChangeBehaviorQueryUnderTest(Args&&... args)
        : LaneChangeBehaviorQuery{std::forward<Args>(args)...} {};

    using LaneChangeBehaviorQuery::AdjustLaneChangeIntensitiesForJam;
    using LaneChangeBehaviorQuery::AdjustLaneConvenienceDueToHighwayExit;
    using LaneChangeBehaviorQuery::CalculateIntensityHighwayExitLeft;
    using LaneChangeBehaviorQuery::CalculateLaneChangeIntensity;
    using LaneChangeBehaviorQuery::DetermineLaneChangeWishFromIntensities;
    using LaneChangeBehaviorQuery::EgoFitsInLane;
    using LaneChangeBehaviorQuery::GetRelativeNetDistance;
    using LaneChangeBehaviorQuery::IsLaneChangeSave;
    using LaneChangeBehaviorQuery::LaneWishAllIntensitiesAreEqual;
    using LaneChangeBehaviorQuery::LaneWishTwoIntensitiesAreEqual;
    using LaneChangeBehaviorQuery::NormalizeLaneChangeIntensities;
    using LaneChangeBehaviorQuery::ShouldChangeLaneInJam;
  };

  LaneChangeBehaviorQueryTester()
      : fakeStochastics{},
        fakeMentalModel{},
        fakeFeatureExtractor{},
        fakeMentalCalculations{},
        fakeInfrastructureCharacteristics{},
        laneChangeBehaviorQuery(
            fakeMentalCalculations,
            fakeMentalModel,
            fakeFeatureExtractor)
  {
  }

  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristics;

  LaneChangeBehaviorQueryUnderTest laneChangeBehaviorQuery;
};

/*******************************************
 * CHECK AdjustLaneChangeIntensitiesForJam *
 *******************************************/
struct DataFor_AdjustLaneChangeIntensitiesForJam
{
  units::velocity::meters_per_second_t in_speedEgoLane;
  units::velocity::meters_per_second_t in_speedLeftLane;
  units::velocity::meters_per_second_t in_speedRightLane;
  double out_adjustedIntensityEgoLane;
  double out_adjustedIntensityLeftLane;
  double out_adjustedIntensityRightLane;
};

class LaneChangeBehaviorQuery_AdjustLaneChangeIntensitiesForJam : public ::testing::Test,
                                                                  public ::testing::WithParamInterface<DataFor_AdjustLaneChangeIntensitiesForJam>
{
};

TEST_P(LaneChangeBehaviorQuery_AdjustLaneChangeIntensitiesForJam, LaneChangeBehaviorQuery_CheckAdjustLaneChangeIntensitiesForJam)
{
  DataFor_AdjustLaneChangeIntensitiesForJam data = GetParam();
  LaneChangeBehaviorQueryTester TEST_HELPER;

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(SurroundingLane::EGO)).WillByDefault(Return(data.in_speedEgoLane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(SurroundingLane::LEFT)).WillByDefault(Return(data.in_speedLeftLane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(SurroundingLane::RIGHT)).WillByDefault(Return(data.in_speedRightLane));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(50._mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(_, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCarQueuingDistance()).WillByDefault(Return(100_m));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetTrafficJamVelocityThreshold()).WillByDefault(Return(60_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleLength()).WillByDefault(Return(5_m));

  LaneChangeIntensities inIntensities;
  inIntensities.EgoLaneIntensity = 1;
  inIntensities.LeftLaneIntensity = 1;
  inIntensities.RightLaneIntensity = 1;

  const auto result = TEST_HELPER.laneChangeBehaviorQuery.AdjustLaneChangeIntensitiesForJam(inIntensities);
  ASSERT_NEAR(result.EgoLaneIntensity, data.out_adjustedIntensityEgoLane, 0.01);
  ASSERT_DOUBLE_EQ(result.LeftLaneIntensity, data.out_adjustedIntensityLeftLane);
  ASSERT_DOUBLE_EQ(result.RightLaneIntensity, data.out_adjustedIntensityRightLane);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehaviorQuery_AdjustLaneChangeIntensitiesForJam,
    testing::Values(
        DataFor_AdjustLaneChangeIntensitiesForJam{
            .in_speedEgoLane = 10._mps,
            .in_speedLeftLane = 3._mps,
            .in_speedRightLane = 2._mps,
            .out_adjustedIntensityEgoLane = 0.16,
            .out_adjustedIntensityLeftLane = 0,
            .out_adjustedIntensityRightLane = 0},
        DataFor_AdjustLaneChangeIntensitiesForJam{
            .in_speedEgoLane = 50._mps,
            .in_speedLeftLane = 3._mps,
            .in_speedRightLane = 2._mps,
            .out_adjustedIntensityEgoLane = 0.83,
            .out_adjustedIntensityLeftLane = 0,
            .out_adjustedIntensityRightLane = 0},
        DataFor_AdjustLaneChangeIntensitiesForJam{
            .in_speedEgoLane = 80._mps,
            .in_speedLeftLane = 20._mps,
            .in_speedRightLane = 10._mps,
            .out_adjustedIntensityEgoLane = 1,
            .out_adjustedIntensityLeftLane = 0,
            .out_adjustedIntensityRightLane = 0}));

/************************************************
 * CHECK DetermineLaneChangeWishFromIntensities *
 ***********************************************/
struct DataFor_DetermineLaneChangeWishFromIntensities
{
  double input_laneIntensityEgo;
  double input_laneIntensityLeft;
  double input_laneIntensityRight;
  bool input_hasJamVelocity;

  SurroundingLane expected_laneChangeWish;
};
class LaneChangeBehaviorQuery_DetermineLaneChangeWishFromIntensities : public ::testing::Test,
                                                                       public ::testing::WithParamInterface<DataFor_DetermineLaneChangeWishFromIntensities>
{
};

TEST_P(LaneChangeBehaviorQuery_DetermineLaneChangeWishFromIntensities, LaneChangeBehaviorQuery_CheckFunction_DetermineLaneChangeWishFromIntensities)
{
  // Create required classes for test
  DataFor_DetermineLaneChangeWishFromIntensities data = GetParam();
  LaneChangeBehaviorQueryTester TEST_HELPER;

  DriverParameters driverParams;
  driverParams.egoLaneKeepingIntensity = 0.2;
  driverParams.outerKeepingIntensity = 0.5;

  if (data.input_hasJamVelocity)
  {
    driverParams.outerKeepingIntensity = 1.;
  }

  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParams));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, HasJamVelocityEgo()).WillByDefault(Return(data.input_hasJamVelocity));

  const LaneChangeIntensities intensities{
      .EgoLaneIntensity = data.input_laneIntensityEgo,
      .LeftLaneIntensity = data.input_laneIntensityLeft,
      .RightLaneIntensity = data.input_laneIntensityRight};

  const SurroundingLane result = TEST_HELPER.laneChangeBehaviorQuery.DetermineLaneChangeWishFromIntensities(intensities);

  ASSERT_EQ(result, data.expected_laneChangeWish);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehaviorQuery_DetermineLaneChangeWishFromIntensities,
    testing::Values(
        // 0 ego intensities, doesn't matter if jam or not
        DataFor_DetermineLaneChangeWishFromIntensities{
            .input_laneIntensityEgo = 0.0,
            .input_laneIntensityLeft = 0.0,
            .input_laneIntensityRight = 0.0,
            .input_hasJamVelocity = true,
            .expected_laneChangeWish = SurroundingLane::EGO},  // no lane change intensities -> no lane change wish
        DataFor_DetermineLaneChangeWishFromIntensities{
            .input_laneIntensityEgo = 0.0,
            .input_laneIntensityLeft = 0.0,
            .input_laneIntensityRight = 0.0,
            .input_hasJamVelocity = false,
            .expected_laneChangeWish = SurroundingLane::EGO},  // no lane change intensities -> no lane change wish
        DataFor_DetermineLaneChangeWishFromIntensities{
            .input_laneIntensityEgo = 0.0,
            .input_laneIntensityLeft = 1.0,
            .input_laneIntensityRight = 0.0,
            .input_hasJamVelocity = true,
            .expected_laneChangeWish = SurroundingLane::LEFT},  // left intensity = 1 -> lane change wish left (1)
        DataFor_DetermineLaneChangeWishFromIntensities{
            .input_laneIntensityEgo = 0.0,
            .input_laneIntensityLeft = 1.0,
            .input_laneIntensityRight = 0.0,
            .input_hasJamVelocity = false,
            .expected_laneChangeWish = SurroundingLane::LEFT},  // left intensity = 1 -> lane change wish left (1)
        DataFor_DetermineLaneChangeWishFromIntensities{
            .input_laneIntensityEgo = 0.0,
            .input_laneIntensityLeft = 0.0,
            .input_laneIntensityRight = 1.0,
            .input_hasJamVelocity = true,
            .expected_laneChangeWish = SurroundingLane::RIGHT},  // right intensity = 1 -> lane change wish right (-1)
        DataFor_DetermineLaneChangeWishFromIntensities{
            .input_laneIntensityEgo = 0.0,
            .input_laneIntensityLeft = 0.0,
            .input_laneIntensityRight = 1.0,
            .input_hasJamVelocity = false,
            .expected_laneChangeWish = SurroundingLane::RIGHT},  // right intensity = 1 -> lane change wish right (-1)
        // check with some ego intensities > 0
        DataFor_DetermineLaneChangeWishFromIntensities{
            .input_laneIntensityEgo = 0.4,
            .input_laneIntensityLeft = 0.1,
            .input_laneIntensityRight = 1.0,
            .input_hasJamVelocity = false,
            .expected_laneChangeWish = SurroundingLane::RIGHT},
        DataFor_DetermineLaneChangeWishFromIntensities{
            .input_laneIntensityEgo = 0.4,
            .input_laneIntensityLeft = 0.1,
            .input_laneIntensityRight = 1.0,
            .input_hasJamVelocity = true,
            .expected_laneChangeWish = SurroundingLane::RIGHT},
        DataFor_DetermineLaneChangeWishFromIntensities{
            .input_laneIntensityEgo = 0.4,
            .input_laneIntensityLeft = 0.1,
            .input_laneIntensityRight = 0.375,
            .input_hasJamVelocity = true,
            .expected_laneChangeWish = SurroundingLane::EGO}));

/**************************************
 * CHECK CalculateLaneChangeIntensity *
 **************************************/

TEST(LaneChangeBehaviorQuery_CalculateLaneChangeIntensity, TargetVelocity_Equal_EstimatedMeanLaneVelocity)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;
  ComponentsForLaneChangeIntensity componentsForLaneChangeIntensity{false, false, false};

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(SurroundingLane::EGO)).WillByDefault(Return(10_kph));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(10._kph));

  const double result = TEST_HELPER.laneChangeBehaviorQuery.CalculateLaneChangeIntensity(SurroundingLane::EGO, componentsForLaneChangeIntensity);

  ASSERT_DOUBLE_EQ(result, 0);
}

TEST(LaneChangeBehaviorQuery_CalculateLaneChangeIntensity, TargetVelocity_Lesser_EstimatedMeanLaneVelocity)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;
  ComponentsForLaneChangeIntensity componentsForLaneChangeIntensity{false, false, false};

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(SurroundingLane::EGO)).WillByDefault(Return(3.6_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(10._kph));

  const double result = TEST_HELPER.laneChangeBehaviorQuery.CalculateLaneChangeIntensity(SurroundingLane::EGO, componentsForLaneChangeIntensity);
  ASSERT_NEAR(result, 0.88, 0.01);
}

TEST(LaneChangeBehaviorQuery_CalculateLaneChangeIntensity, TargetVelocity_Greater_EstimatedMeanLaneVelocity)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;
  ComponentsForLaneChangeIntensity componentsForLaneChangeIntensity{false, false, false};

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(SurroundingLane::EGO)).WillByDefault(Return(10_kph));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(3.6_mps));

  const double result = TEST_HELPER.laneChangeBehaviorQuery.CalculateLaneChangeIntensity(SurroundingLane::EGO, componentsForLaneChangeIntensity);
  ASSERT_NEAR(result, 0.70, 0.01);
}

TEST(LaneChangeBehaviorQuery_CalculateLaneChangeIntensity, SlowBlocker_TimeAtTargetSpeed_isDistanceToEndOfLaneSmallerOrEqToInfluencingDistance_Is_True)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;
  ComponentsForLaneChangeIntensity componentsForLaneChangeIntensity{true, true, true};

  /* Set up mocks */
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(SurroundingLane::EGO)).WillByDefault(Return(10._kph));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(10._kph));

  const double result = TEST_HELPER.laneChangeBehaviorQuery.CalculateLaneChangeIntensity(SurroundingLane::EGO, componentsForLaneChangeIntensity);
  ASSERT_DOUBLE_EQ(result, 0);
}

/***********************************
 * CHECK ShouldChangeLaneInJam *
 **********************************/
struct DataFor_ShouldChangeLaneInJam
{
  SurroundingLane input_lane;
  units::velocity::meters_per_second_t input_egoSpeed;
  units::velocity::meters_per_second_t input_meanLaneVelocity;
  units::length::meter_t input_relativeNetDistance;
  units::length::meter_t input_carQueuingDistance;
  bool expected_shouldChangeLane;
};
class LaneChangeBehaviorQuery_ShouldChangeLaneInJam : public ::testing::Test,
                                                      public ::testing::WithParamInterface<DataFor_ShouldChangeLaneInJam>
{
};

TEST_P(LaneChangeBehaviorQuery_ShouldChangeLaneInJam, LaneChangeBehaviorQuery_CheckFunction_ShouldChangeLaneInJam)
{
  // Create required classes for test
  DataFor_ShouldChangeLaneInJam data = GetParam();
  LaneChangeBehaviorQueryTester TEST_HELPER;

  const AreaOfInterest targetAoi{SurroundingLane2AreaOfInterest.at(data.input_lane)};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(data.input_lane)).WillByDefault(Return(data.input_meanLaneVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).WillByDefault(Return(data.input_egoSpeed));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCarQueuingDistance()).WillByDefault(Return(data.input_carQueuingDistance));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(targetAoi, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(targetAoi)).WillByDefault(Return(data.input_relativeNetDistance));

  const auto result = TEST_HELPER.laneChangeBehaviorQuery.ShouldChangeLaneInJam(data.input_lane);

  ASSERT_EQ(result, data.expected_shouldChangeLane);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehaviorQuery_ShouldChangeLaneInJam,
    testing::Values(
        DataFor_ShouldChangeLaneInJam{
            /* Test 0: Mean lane velocity more than 10 percent higher than ego speed
                       and ego speed equal to 5 km/h
                       returns true */
            .input_lane = SurroundingLane::LEFT,
            .input_egoSpeed = units::velocity::meters_per_second_t(5_kph),
            .input_meanLaneVelocity = 10._mps,
            .input_relativeNetDistance = 1._m,
            .input_carQueuingDistance = 10._m,
            .expected_shouldChangeLane = true},
        DataFor_ShouldChangeLaneInJam{
            /* Test 1: Mean lane velocity more than 10 percent higher than ego speed
                       and ego speed lower than 5 km/h
                       returns false */
            .input_lane = SurroundingLane::LEFT,
            .input_egoSpeed = units::velocity::meters_per_second_t(2_kph),
            .input_meanLaneVelocity = 10._mps,
            .input_relativeNetDistance = 20_m,
            .input_carQueuingDistance = 10._m,
            .expected_shouldChangeLane = false},
        /* Car Queuing distance not used ATM */
        DataFor_ShouldChangeLaneInJam{
            /* Test 2: Mean lane velocity lower than ego speed
                       and relative net distance larger than two times carQueuingDistance
                       returns true */
            .input_lane = SurroundingLane::RIGHT,
            .input_egoSpeed = 10._mps,
            .input_meanLaneVelocity = 1._mps,
            .input_relativeNetDistance = 20.1_m,
            .input_carQueuingDistance = 10._m,
            .expected_shouldChangeLane = false},
        /* Car Queuing distance not used ATM */
        DataFor_ShouldChangeLaneInJam{
            /* Test 3: Mean lane velocity zero
                       and relative net distance equals two times carQueuingDistance
                       returns false */
            .input_lane = SurroundingLane::LEFT,
            .input_egoSpeed = 1._mps,
            .input_meanLaneVelocity = 0._mps,
            .input_relativeNetDistance = 20._m,
            .input_carQueuingDistance = 10._m,
            .expected_shouldChangeLane = false}));

/********************************
 * CHECK GetRelativeNetDistance *
 ********************************/
class LaneChangeBehaviorQuery_GetRelativeNetDistanceSideLaneTests : public ::testing::Test,
                                                                    public ::testing::WithParamInterface<std::tuple<SurroundingLane, AreaOfInterest, AreaOfInterest>>
{
};

TEST_P(LaneChangeBehaviorQuery_GetRelativeNetDistanceSideLaneTests, VehicleVisibleOnSide_ReturnZero)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;

  const auto param = GetParam();
  const SurroundingLane targetLane{std::get<0>(param)};
  const AreaOfInterest sideAoi{std::get<1>(param)};
  const AreaOfInterest sideFrontAoi{std::get<2>(param)};

  const bool input_vehicleVisibleOnSide{true};
  const auto input_relativeNetDistance{10._m};

  const auto expected_relativeNetDistance{0._m};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(sideAoi, _)).WillByDefault(Return(input_vehicleVisibleOnSide));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(sideFrontAoi, _)).WillByDefault(Return(input_vehicleVisibleOnSide));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(sideFrontAoi)).WillByDefault(Return(input_relativeNetDistance));

  const auto result = TEST_HELPER.laneChangeBehaviorQuery.GetRelativeNetDistance(targetLane);
  ASSERT_EQ(result, expected_relativeNetDistance);
}

TEST_P(LaneChangeBehaviorQuery_GetRelativeNetDistanceSideLaneTests, VehicleVisibleSideFront_ReturnRelativeNetDistanceToAoi)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;

  const auto param = GetParam();
  const SurroundingLane targetLane{std::get<0>(param)};
  const AreaOfInterest sideAoi{std::get<1>(param)};
  const AreaOfInterest sideFrontAoi{std::get<2>(param)};

  const bool input_vehicleVisibleOnSide{false};
  const bool input_vehicleVisibleSideFront{true};
  const auto input_relativeNetDistance{10._m};

  const auto expected_relativeNetDistance{10._m};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(sideAoi, _)).WillByDefault(Return(input_vehicleVisibleOnSide));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(sideFrontAoi, _)).WillByDefault(Return(input_vehicleVisibleSideFront));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(sideFrontAoi)).WillByDefault(Return(input_relativeNetDistance));

  const auto result = TEST_HELPER.laneChangeBehaviorQuery.GetRelativeNetDistance(targetLane);
  ASSERT_EQ(result, expected_relativeNetDistance);
}

TEST_P(LaneChangeBehaviorQuery_GetRelativeNetDistanceSideLaneTests, NoVehicleVisible_ReturnInfinity)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;

  const auto param = GetParam();
  const SurroundingLane targetLane{std::get<0>(param)};
  const AreaOfInterest sideAoi{std::get<1>(param)};
  const AreaOfInterest sideFrontAoi{std::get<2>(param)};

  const bool input_vehicleVisibleOnSide{false};
  const bool input_vehicleVisibleSideFront{false};
  const auto input_relativeNetDistance{10._m};

  const auto expected_relativeNetDistance{ScmDefinitions::INF_DISTANCE};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(sideAoi, _)).WillByDefault(Return(input_vehicleVisibleOnSide));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(sideFrontAoi, _)).WillByDefault(Return(input_vehicleVisibleSideFront));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(sideFrontAoi)).WillByDefault(Return(input_relativeNetDistance));

  const auto result = TEST_HELPER.laneChangeBehaviorQuery.GetRelativeNetDistance(targetLane);
  ASSERT_TRUE(units::math::isinf(result));
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehaviorQuery_GetRelativeNetDistanceSideLaneTests,
    testing::Values(
        std::make_tuple(SurroundingLane::LEFT, AreaOfInterest::LEFT_SIDE, AreaOfInterest::LEFT_FRONT),
        std::make_tuple(SurroundingLane::RIGHT, AreaOfInterest::RIGHT_SIDE, AreaOfInterest::RIGHT_FRONT)));

class LaneChangeBehaviorQuery_GetRelativeNetDistanceEgoLaneTests : public ::testing::Test
{
};

TEST_F(LaneChangeBehaviorQuery_GetRelativeNetDistanceEgoLaneTests, VehicleVisible_ReturnRelativeNetDistance)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;
  const SurroundingLane targetLane{SurroundingLane::EGO};

  const bool input_vehicleVisible{true};
  const auto input_relativeNetDistance{10._m};

  const auto expected_relativeNetDistance{10._m};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(input_vehicleVisible));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::EGO_FRONT)).WillByDefault(Return(input_relativeNetDistance));

  const auto result = TEST_HELPER.laneChangeBehaviorQuery.GetRelativeNetDistance(targetLane);
  ASSERT_EQ(result, expected_relativeNetDistance);
}

TEST_F(LaneChangeBehaviorQuery_GetRelativeNetDistanceEgoLaneTests, NoVehicleVisible_ReturnInfinity)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;

  const SurroundingLane targetLane{SurroundingLane::EGO};

  const bool input_vehicleVisible{false};
  const auto input_relativeNetDistance{10._m};

  const auto expected_relativeNetDistance{ScmDefinitions::INF_DISTANCE};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(input_vehicleVisible));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::EGO_FRONT)).WillByDefault(Return(input_relativeNetDistance));

  const auto result = TEST_HELPER.laneChangeBehaviorQuery.GetRelativeNetDistance(targetLane);

  ASSERT_TRUE(units::math::isinf(result));
}

/**************************
 * CHECK IsLaneChangeSave *
 **************************/
struct DataFor_IsLaneChangeSave
{
  SurroundingLane input_lane;
  scm::LaneType input_laneType;
  bool input_obstacleOnLane;
  units::velocity::meters_per_second_t input_legalVelocity;
  units::velocity::meters_per_second_t input_speedViolation;
  units::velocity::meters_per_second_t input_targetVelocity;
  units::length::meter_t input_distanceToEndOfLane;
  units::length::meter_t input_influencingDistance;
  units::time::second_t input_timeAtTargetSpeed;
  bool expected_laneChangeSave;
  static constexpr auto MinimumTimeAtTargetSpeedThreshold{20._s};
};
class LaneChangeBehaviorQuery_IsLaneChangeSave : public ::testing::Test,
                                                 public ::testing::WithParamInterface<DataFor_IsLaneChangeSave>
{
};

TEST_P(LaneChangeBehaviorQuery_IsLaneChangeSave, LaneChangeBehaviorQuery_CheckFunction_IsLaneChangeSave)
{
  // Create required classes for test
  DataFor_IsLaneChangeSave data = GetParam();
  LaneChangeBehaviorQueryTester TEST_HELPER;

  const auto relativeLane{SurroundingLane2RelativeLane.at(data.input_lane)};
  const AreaOfInterest targetAoi{SurroundingLane2AreaOfInterest.at(data.input_lane)};

  const bool laneDrivable{data.input_laneType == scm::LaneType::Driving};
  InfrastructureCharacteristics infrastructureCharacteristics;
  LaneInformationGeometrySCM geometryObject;
  geometryObject.laneType = data.input_laneType;

  if (data.input_lane == SurroundingLane::EGO)
  {
    infrastructureCharacteristics.UpdateGeometryInformation()->Close() = geometryObject;
    NiceMock<FakeSurroundingVehicle> fakeVehicle;
    ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsObstacle(_)).WillByDefault(Return(data.input_obstacleOnLane));
  }
  else if (data.input_lane == SurroundingLane::LEFT)
  {
    infrastructureCharacteristics.UpdateGeometryInformation()->Left() = geometryObject;
    NiceMock<FakeSurroundingVehicle> fakeVehicle;
    ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsObstacle(_)).WillByDefault(Return(data.input_obstacleOnLane));
  }
  else
  {
    infrastructureCharacteristics.UpdateGeometryInformation()->Right() = geometryObject;
    NiceMock<FakeSurroundingVehicle> fakeVehicle;
    ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(_, _)).WillByDefault(Return(&fakeVehicle));
    ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsObstacle(_)).WillByDefault(Return(data.input_obstacleOnLane));
  }

  DriverParameters driverParam;
  driverParam.amountOfSpeedLimitViolation = data.input_speedViolation;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLegalVelocity(data.input_lane)).WillByDefault(Return(data.input_legalVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfLane(relativeLane, _)).WillByDefault(Return(data.input_distanceToEndOfLane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityTargeted()).WillByDefault(Return(data.input_targetVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&infrastructureCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParam));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfluencingDistanceToEndOfLane()).WillByDefault(Return(data.input_influencingDistance));

  NiceMock<FakeSurroundingVehicle> vehicle;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(targetAoi, _)).WillByDefault(Return(&vehicle));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, GetTimeAtTargetSpeedInLane(_)).WillByDefault(Return(data.input_timeAtTargetSpeed));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneTypeDriveable(data.input_laneType)).WillByDefault(Return(laneDrivable));

  const auto result = TEST_HELPER.laneChangeBehaviorQuery.IsLaneChangeSave(data.input_lane);
  ASSERT_EQ(result, data.expected_laneChangeSave);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehaviorQuery_IsLaneChangeSave,
    testing::Values(
        DataFor_IsLaneChangeSave{
            /* Test 0: No Obstacle on lane,
                       distance to end of lane large enough,
                       target velocity below violation limit,
                       time at target speed above threshold
                       and lane drivable
                       return true */
            .input_lane = SurroundingLane::EGO,
            .input_laneType = scm::LaneType::Driving,
            .input_obstacleOnLane = false,
            .input_legalVelocity = 100._mps,
            .input_speedViolation = 5._mps,
            .input_targetVelocity = 100._mps,
            .input_distanceToEndOfLane = 11._m,
            .input_influencingDistance = 10._m,
            .input_timeAtTargetSpeed = 1._s + DataFor_IsLaneChangeSave::MinimumTimeAtTargetSpeedThreshold,
            .expected_laneChangeSave = true},
        DataFor_IsLaneChangeSave{
            /* Test 1: Obstacle on lane,
                       return false */
            .input_lane = SurroundingLane::EGO,
            .input_laneType = scm::LaneType::Driving,
            .input_obstacleOnLane = true,
            .input_legalVelocity = 100._mps,
            .input_speedViolation = 5._mps,
            .input_targetVelocity = 100._mps,
            .input_distanceToEndOfLane = 11._m,
            .input_influencingDistance = 10._m,
            .input_timeAtTargetSpeed = 1._s + DataFor_IsLaneChangeSave::MinimumTimeAtTargetSpeedThreshold,
            .expected_laneChangeSave = false},
        DataFor_IsLaneChangeSave{
            /* Test 2: Distance to end of lane equals influencing distance,
                       return false */
            .input_lane = SurroundingLane::LEFT,
            .input_laneType = scm::LaneType::Driving,
            .input_obstacleOnLane = false,
            .input_legalVelocity = 100._mps,
            .input_speedViolation = 5._mps,
            .input_targetVelocity = 100._mps,
            .input_distanceToEndOfLane = 10._m,
            .input_influencingDistance = 10._m,
            .input_timeAtTargetSpeed = 1._s + DataFor_IsLaneChangeSave::MinimumTimeAtTargetSpeedThreshold,
            .expected_laneChangeSave = false},
        DataFor_IsLaneChangeSave{
            /* Test 3: Target velocity is above legal velocity plus violation limit,
                       return false */
            .input_lane = SurroundingLane::RIGHT,
            .input_laneType = scm::LaneType::Driving,
            .input_obstacleOnLane = false,
            .input_legalVelocity = 100._mps,
            .input_speedViolation = 5._mps,
            .input_targetVelocity = 106._mps,
            .input_distanceToEndOfLane = 11._m,
            .input_influencingDistance = 10._m,
            .input_timeAtTargetSpeed = 1._s + DataFor_IsLaneChangeSave::MinimumTimeAtTargetSpeedThreshold,
            .expected_laneChangeSave = false},
        DataFor_IsLaneChangeSave{
            /* Test 4: Time at target speed is below threshold,
                       return false */
            .input_lane = SurroundingLane::RIGHT,
            .input_laneType = scm::LaneType::Driving,
            .input_obstacleOnLane = false,
            .input_legalVelocity = 100._mps,
            .input_speedViolation = 5._mps,
            .input_targetVelocity = 100._mps,
            .input_distanceToEndOfLane = 11._m,
            .input_influencingDistance = 10._m,
            .input_timeAtTargetSpeed = DataFor_IsLaneChangeSave::MinimumTimeAtTargetSpeedThreshold - 1._s,
            .expected_laneChangeSave = false},
        DataFor_IsLaneChangeSave{
            /* Test 5: Lane is not drivable,
                       return false */
            .input_lane = SurroundingLane::RIGHT,
            .input_laneType = scm::LaneType::Shoulder,
            .input_obstacleOnLane = false,
            .input_legalVelocity = 100._mps,
            .input_speedViolation = 5._mps,
            .input_targetVelocity = 100._mps,
            .input_distanceToEndOfLane = 11._m,
            .input_influencingDistance = 10._m,
            .input_timeAtTargetSpeed = 1._s + DataFor_IsLaneChangeSave::MinimumTimeAtTargetSpeedThreshold - 1._s,
            .expected_laneChangeSave = false}));

/***********************************************
 * CHECK AdjustLaneConvenienceDueToHighwayExit *
 ***********************************************/
struct DataFor_AdjustLaneConvenienceDueToHighwayExit
{
  LaneChangeIntensities input_LaneConvenience;
  units::length::meter_t input_DistanceToEndOfNextExit;
  int input_MainLaneId;
  int input_LaneIdForJunctionIngoing;
  scm::LaneMarking::Type input_LaneMarkingTypeLeft;
  scm::LaneMarking::Type input_LaneMarkingTypeRight;
  bool input_LaneMarkingsBold;
  double mock_UrgencyForNextExit;
  bool mock_LeftLaneExistence;
  bool mock_RightLaneExistence;
  LaneChangeIntensities result_LaneConvenience;  // Ego, left, right
};

class LaneChangeBehaviorQuery_AdjustLaneConvenienceDueToHighwayExit : public ::testing::Test,
                                                                      public ::testing::WithParamInterface<DataFor_AdjustLaneConvenienceDueToHighwayExit>
{
};

TEST_P(LaneChangeBehaviorQuery_AdjustLaneConvenienceDueToHighwayExit, LaneChangeBehaviorQuery_CheckFunction_AdjustLaneConvenienceDueToHighwayExit)
{
  // Create required classes for test
  DataFor_AdjustLaneConvenienceDueToHighwayExit data = GetParam();
  LaneChangeBehaviorQueryTester TEST_HELPER;

  GeometryInformationSCM geometry;
  TrafficRuleInformationScmExtended trafficRules;
  geometry.distanceToEndOfNextExit = data.input_DistanceToEndOfNextExit;
  geometry.distanceToStartOfNextExit = data.input_DistanceToEndOfNextExit - 500._m;
  geometry.relativeLaneIdForJunctionIngoing = {data.input_LaneIdForJunctionIngoing};

  MicroscopicCharacteristics microscopicCharacteristics;
  microscopicCharacteristics.UpdateOwnVehicleData()->mainLaneId = data.input_MainLaneId;

  scm::LaneMarking::Entity laneMarkingLeft;
  scm::LaneMarking::Entity laneMarkingRight;
  laneMarkingLeft.color = scm::LaneMarking::Color::White;
  laneMarkingLeft.relativeStartDistance = -100._m;
  laneMarkingLeft.type = data.input_LaneMarkingTypeLeft;
  laneMarkingRight.color = scm::LaneMarking::Color::White;
  laneMarkingRight.relativeStartDistance = -100._m;
  laneMarkingRight.type = data.input_LaneMarkingTypeRight;

  auto BoldLaneMarkingWidth = .3_m;
  auto NonBoldLaneMarkingWidth = .2_m;

  if (data.input_LaneMarkingsBold)
  {
    laneMarkingLeft.width = BoldLaneMarkingWidth;
    laneMarkingRight.width = BoldLaneMarkingWidth;
  }
  else
  {
    laneMarkingLeft.width = NonBoldLaneMarkingWidth;
    laneMarkingRight.width = NonBoldLaneMarkingWidth;
  }

  ON_CALL(TEST_HELPER.fakeMentalCalculations, DetermineUrgencyFactorForNextExit(_, _)).WillByDefault(Return(data.mock_UrgencyForNextExit));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneMarkingAtDistance(_, Side::Left)).WillByDefault(Return(laneMarkingLeft));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetLaneMarkingAtDistance(_, Side::Right)).WillByDefault(Return(laneMarkingRight));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::LEFT, _)).WillByDefault(Return(data.mock_LeftLaneExistence));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(RelativeLane::RIGHT, _)).WillByDefault(Return(data.mock_RightLaneExistence));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&microscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&TEST_HELPER.fakeInfrastructureCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetClosestRelativeLaneIdForJunctionIngoing()).WillByDefault(Return(data.input_LaneIdForJunctionIngoing));
  ON_CALL(TEST_HELPER.fakeInfrastructureCharacteristics, GetGeometryInformation()).WillByDefault(Return(&geometry));

  // Call test
  const LaneChangeIntensities result = TEST_HELPER.laneChangeBehaviorQuery.AdjustLaneConvenienceDueToHighwayExit(data.input_LaneConvenience, true);

  // Evaluate results
  ASSERT_DOUBLE_EQ(std::fabs(result.LeftLaneIntensity), std::fabs(data.result_LaneConvenience.LeftLaneIntensity));
  ASSERT_DOUBLE_EQ(std::fabs(result.EgoLaneIntensity), std::fabs(data.result_LaneConvenience.EgoLaneIntensity));
  ASSERT_DOUBLE_EQ(std::fabs(result.RightLaneIntensity), std::fabs(data.result_LaneConvenience.RightLaneIntensity));
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehaviorQuery_AdjustLaneConvenienceDueToHighwayExit,
    testing::Values(
        // No exit lane detected
        DataFor_AdjustLaneConvenienceDueToHighwayExit{
            .input_LaneConvenience = LaneChangeIntensities{.EgoLaneIntensity = 1., .LeftLaneIntensity = 0., .RightLaneIntensity = 0.},
            .input_DistanceToEndOfNextExit = -999._m,
            .input_MainLaneId = -3,
            .input_LaneIdForJunctionIngoing = -1,
            .input_LaneMarkingTypeLeft = scm::LaneMarking::Type::None,
            .input_LaneMarkingTypeRight = scm::LaneMarking::Type::None,
            .input_LaneMarkingsBold = false,
            .mock_UrgencyForNextExit = 0.,
            .mock_LeftLaneExistence = false,
            .mock_RightLaneExistence = false,
            .result_LaneConvenience{.EgoLaneIntensity = 1., .LeftLaneIntensity = 0., .RightLaneIntensity = 0.}},
        // Exit to agent's right, right lane exists
        DataFor_AdjustLaneConvenienceDueToHighwayExit{
            .input_LaneConvenience = LaneChangeIntensities{.EgoLaneIntensity = .8, .LeftLaneIntensity = .5, .RightLaneIntensity = .5},
            .input_DistanceToEndOfNextExit = 200._m,
            .input_MainLaneId = -3,
            .input_LaneIdForJunctionIngoing = -1,
            .input_LaneMarkingTypeLeft = scm::LaneMarking::Type::None,
            .input_LaneMarkingTypeRight = scm::LaneMarking::Type::None,
            .input_LaneMarkingsBold = false,
            .mock_UrgencyForNextExit = .2,
            .mock_LeftLaneExistence = true,
            .mock_RightLaneExistence = true,
            .result_LaneConvenience{.EgoLaneIntensity = .6, .LeftLaneIntensity = .5, .RightLaneIntensity = .7}},
        // Exit to agent's right, right lane does not exist
        DataFor_AdjustLaneConvenienceDueToHighwayExit{
            .input_LaneConvenience = LaneChangeIntensities{.EgoLaneIntensity = .8, .LeftLaneIntensity = .5, .RightLaneIntensity = .5},
            .input_DistanceToEndOfNextExit = 200._m,
            .input_MainLaneId = -3,
            .input_LaneIdForJunctionIngoing = -1,
            .input_LaneMarkingTypeLeft = scm::LaneMarking::Type::None,
            .input_LaneMarkingTypeRight = scm::LaneMarking::Type::None,
            .input_LaneMarkingsBold = false,
            .mock_UrgencyForNextExit = .1,
            .mock_LeftLaneExistence = true,
            .mock_RightLaneExistence = false,
            .result_LaneConvenience{.EgoLaneIntensity = .9, .LeftLaneIntensity = .5, .RightLaneIntensity = .0}},
        // Exit to agent's left, left lane exists
        DataFor_AdjustLaneConvenienceDueToHighwayExit{
            .input_LaneConvenience = LaneChangeIntensities{.EgoLaneIntensity = .8, .LeftLaneIntensity = .5, .RightLaneIntensity = .5},
            .input_DistanceToEndOfNextExit = 200._m,
            .input_MainLaneId = -3,
            .input_LaneIdForJunctionIngoing = 1,
            .input_LaneMarkingTypeLeft = scm::LaneMarking::Type::None,
            .input_LaneMarkingTypeRight = scm::LaneMarking::Type::None,
            .input_LaneMarkingsBold = false,
            .mock_UrgencyForNextExit = .2,
            .mock_LeftLaneExistence = true,
            .mock_RightLaneExistence = true,
            .result_LaneConvenience{.EgoLaneIntensity = .6, .LeftLaneIntensity = .5, .RightLaneIntensity = .3}},
        // Exit to agent's left, left lane does not exist
        DataFor_AdjustLaneConvenienceDueToHighwayExit{
            .input_LaneConvenience = LaneChangeIntensities{.EgoLaneIntensity = .8, .LeftLaneIntensity = .5, .RightLaneIntensity = .5},
            .input_DistanceToEndOfNextExit = 200._m,
            .input_MainLaneId = -3,
            .input_LaneIdForJunctionIngoing = 1,
            .input_LaneMarkingTypeLeft = scm::LaneMarking::Type::None,
            .input_LaneMarkingTypeRight = scm::LaneMarking::Type::None,
            .input_LaneMarkingsBold = false,
            .mock_UrgencyForNextExit = .1,
            .mock_LeftLaneExistence = false,
            .mock_RightLaneExistence = true,
            .result_LaneConvenience{.EgoLaneIntensity = .9, .LeftLaneIntensity = .5, .RightLaneIntensity = .4}},
        // Agent on exit lane, broken bold lane marking prevents lane change wish
        DataFor_AdjustLaneConvenienceDueToHighwayExit{
            .input_LaneConvenience = LaneChangeIntensities{.EgoLaneIntensity = .8, .LeftLaneIntensity = .5, .RightLaneIntensity = .5},
            .input_DistanceToEndOfNextExit = 200._m,
            .input_MainLaneId = -4,
            .input_LaneIdForJunctionIngoing = 0,
            .input_LaneMarkingTypeLeft = scm::LaneMarking::Type::Broken,
            .input_LaneMarkingTypeRight = scm::LaneMarking::Type::Broken,
            .input_LaneMarkingsBold = true,
            .mock_UrgencyForNextExit = .1,
            .mock_LeftLaneExistence = true,
            .mock_RightLaneExistence = true,
            .result_LaneConvenience{.EgoLaneIntensity = .8, .LeftLaneIntensity = .5, .RightLaneIntensity = 0.}},
        // Agent on exit lane, no broken bold lane marking on eather side
        DataFor_AdjustLaneConvenienceDueToHighwayExit{
            .input_LaneConvenience = LaneChangeIntensities{.EgoLaneIntensity = .8, .LeftLaneIntensity = .5, .RightLaneIntensity = .5},
            .input_DistanceToEndOfNextExit = 200._m,
            .input_MainLaneId = -4,
            .input_LaneIdForJunctionIngoing = 0,
            .input_LaneMarkingTypeLeft = scm::LaneMarking::Type::Broken,
            .input_LaneMarkingTypeRight = scm::LaneMarking::Type::Broken,
            .input_LaneMarkingsBold = false,
            .mock_UrgencyForNextExit = .1,
            .mock_LeftLaneExistence = true,
            .mock_RightLaneExistence = true,
            .result_LaneConvenience{.EgoLaneIntensity = .8, .LeftLaneIntensity = .5, .RightLaneIntensity = .5}}));

/**************************
 * CHECK IsLaneChangeSave *
 **************************/
struct DataFor_NormalizeLaneChangeIntensities
{
  double input_EgoLaneIntensity;
  double input_LeftLaneIntensity;
  double input_RightLaneIntensity;
  double result_EgoLaneIntensity;
  double result_LeftLaneIntensity;
  double result_RightLaneIntensity;
};
class LaneChangeBehaviorQuery_NormalizeLaneChangeIntensities : public ::testing::Test,
                                                               public ::testing::WithParamInterface<DataFor_NormalizeLaneChangeIntensities>
{
};

TEST_P(LaneChangeBehaviorQuery_NormalizeLaneChangeIntensities, NormalizeLaneChangeIntensities)
{
  DataFor_NormalizeLaneChangeIntensities data = GetParam();
  LaneChangeBehaviorQueryTester TEST_HELPER;

  LaneChangeIntensities inputIntensities{
      .EgoLaneIntensity = data.input_EgoLaneIntensity,
      .LeftLaneIntensity = data.input_LeftLaneIntensity,
      .RightLaneIntensity = data.input_RightLaneIntensity};

  const auto result = TEST_HELPER.laneChangeBehaviorQuery.NormalizeLaneChangeIntensities(inputIntensities);

  ASSERT_DOUBLE_EQ(result.EgoLaneIntensity, data.result_EgoLaneIntensity);
  ASSERT_DOUBLE_EQ(result.LeftLaneIntensity, data.result_LeftLaneIntensity);
  ASSERT_DOUBLE_EQ(result.RightLaneIntensity, data.result_RightLaneIntensity);
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehaviorQuery_NormalizeLaneChangeIntensities,
    testing::Values(
        DataFor_NormalizeLaneChangeIntensities{
            .input_EgoLaneIntensity = 0.5,
            .input_LeftLaneIntensity = 0.75,
            .input_RightLaneIntensity = 0.8,
            .result_EgoLaneIntensity = 0.5,
            .result_LeftLaneIntensity = 0.75,
            .result_RightLaneIntensity = 0.8},
        DataFor_NormalizeLaneChangeIntensities{
            .input_EgoLaneIntensity = 2.0,
            .input_LeftLaneIntensity = 0.8,
            .input_RightLaneIntensity = 0.8,
            .result_EgoLaneIntensity = 1.0,
            .result_LeftLaneIntensity = 0.4,
            .result_RightLaneIntensity = 0.4}));

/***********************
 * CHECK EgoFitsInLane *
 ***********************/

struct DataFor_EgoFitsInLane
{
  bool isShoulderLaneUsageLegit;
  bool laneExistence;
  units::length::meter_t vehicleWidth;
  bool expectedResult;
};

class LaneChangeBehaviorQuery_EgoFitsInLane : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_EgoFitsInLane>
{
};

TEST_P(LaneChangeBehaviorQuery_EgoFitsInLane, Check_EgoFitsInLane)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;
  DataFor_EgoFitsInLane data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, IsShoulderLaneUsageLegit(_)).WillByDefault(Return(data.isShoulderLaneUsageLegit));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneExistence(_, _)).WillByDefault(Return(data.laneExistence));

  scm::common::vehicle::properties::EntityProperties vehicleProperties;
  vehicleProperties.bounding_box.dimension.width = data.vehicleWidth;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicleModelParameters()).WillByDefault(Return(vehicleProperties));

  NiceMock<FakeInfrastructureCharacteristics> fakeInfrastructureCharacteristsics;
  LaneInformationGeometrySCM laneInfo{};
  laneInfo.width = 3.0_m;
  ON_CALL(fakeInfrastructureCharacteristsics, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&fakeInfrastructureCharacteristsics));

  auto result = TEST_HELPER.laneChangeBehaviorQuery.EgoFitsInLane(SurroundingLane::LEFT);
  ASSERT_EQ(result, data.expectedResult);
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehaviorQuery_EgoFitsInLane,
    testing::Values(
        DataFor_EgoFitsInLane{
            .isShoulderLaneUsageLegit = true,
            .laneExistence = true,
            .vehicleWidth = 5.0_m,
            .expectedResult = false},
        DataFor_EgoFitsInLane{
            .isShoulderLaneUsageLegit = true,
            .laneExistence = true,
            .vehicleWidth = 2.0_m,
            .expectedResult = true},
        DataFor_EgoFitsInLane{
            .isShoulderLaneUsageLegit = false,
            .laneExistence = false,
            .vehicleWidth = 2.0_m,
            .expectedResult = false}));

/****************************************
 * CHECK LaneWishAllIntensitiesAreEqual *
 ****************************************/
struct DataFor_LaneWishAllIntensitiesAreEqual
{
  units::velocity::meters_per_second_t meanVelocityLeft;
  units::velocity::meters_per_second_t meanVelocityEgo;
  units::velocity::meters_per_second_t meanVelocityRight;
  SurroundingLane expectedResult;
};

class LaneChangeBehaviorQuery_LaneWishAllIntensitiesAreEqual : public ::testing::Test,
                                                               public ::testing::WithParamInterface<DataFor_LaneWishAllIntensitiesAreEqual>
{
};

TEST_P(LaneChangeBehaviorQuery_LaneWishAllIntensitiesAreEqual, Check_LaneWishAllIntensitiesAreEqual)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;
  DataFor_LaneWishAllIntensitiesAreEqual data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(SurroundingLane::LEFT)).WillByDefault(Return(data.meanVelocityLeft));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(SurroundingLane::EGO)).WillByDefault(Return(data.meanVelocityEgo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(SurroundingLane::RIGHT)).WillByDefault(Return(data.meanVelocityRight));

  auto result = TEST_HELPER.laneChangeBehaviorQuery.LaneWishAllIntensitiesAreEqual();

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehaviorQuery_LaneWishAllIntensitiesAreEqual,
    testing::Values(
        DataFor_LaneWishAllIntensitiesAreEqual{
            .meanVelocityLeft = 1_mps,
            .meanVelocityEgo = 3_mps,
            .meanVelocityRight = 2_mps,
            .expectedResult = SurroundingLane::EGO},
        DataFor_LaneWishAllIntensitiesAreEqual{
            .meanVelocityLeft = 3_mps,
            .meanVelocityEgo = 1_mps,
            .meanVelocityRight = 2_mps,
            .expectedResult = SurroundingLane::LEFT},
        DataFor_LaneWishAllIntensitiesAreEqual{
            .meanVelocityLeft = 1_mps,
            .meanVelocityEgo = 2_mps,
            .meanVelocityRight = 3_mps,
            .expectedResult = SurroundingLane::RIGHT}));

/****************************************
 * CHECK LaneWishTwoIntensitiesAreEqual *
 ****************************************/

struct DataFor_LaneWishTwoIntensitiesAreEqual
{
  units::velocity::meters_per_second_t meanVelocityEgo;
  units::velocity::meters_per_second_t meanVelocity;
  std::optional<SurroundingLane> expectedResult;
};

class LaneChangeBehaviorQuery_LaneWishTwoIntensitiesAreEqual : public ::testing::Test,
                                                               public ::testing::WithParamInterface<DataFor_LaneWishTwoIntensitiesAreEqual>
{
};

TEST_P(LaneChangeBehaviorQuery_LaneWishTwoIntensitiesAreEqual, Check_LaneWishTwoIntensitiesAreEqual)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;
  DataFor_LaneWishTwoIntensitiesAreEqual data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMeanVelocity(SurroundingLane::EGO)).WillByDefault(Return(data.meanVelocityEgo));

  auto result = TEST_HELPER.laneChangeBehaviorQuery.LaneWishTwoIntensitiesAreEqual(data.meanVelocity);
  ASSERT_EQ(result, data.expectedResult);
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehaviorQuery_LaneWishTwoIntensitiesAreEqual,
    testing::Values(
        DataFor_LaneWishTwoIntensitiesAreEqual{
            .meanVelocityEgo = 50.0_mps,
            .meanVelocity = 30_mps,
            .expectedResult = SurroundingLane::EGO},
        DataFor_LaneWishTwoIntensitiesAreEqual{
            .meanVelocityEgo = 50.0_mps,
            .meanVelocity = 60_mps,
            .expectedResult = std::nullopt}));

/*******************************************
 * CHECK CalculateIntensityHighwayExitLeft *
 *******************************************/
struct DataFor_CalculateIntensityHighwayExitLeft
{
  int closestRelativeExitLane;
  units::length::meter_t distanceToEndOfExit;
  bool collision;
  bool isStatic;
  bool isLaneTypeDriveable;
  bool isShoulderLaneUsageLegit;
  double leftLaneIntensity;
  double expectedResult;
};

class LaneChangeBehaviorQuery_CalculateIntensityHighwayExitLeft : public ::testing::Test,
                                                                  public ::testing::WithParamInterface<DataFor_CalculateIntensityHighwayExitLeft>
{
};

TEST_P(LaneChangeBehaviorQuery_CalculateIntensityHighwayExitLeft, Check_CalculateIntensityHighwayExitLeft)
{
  LaneChangeBehaviorQueryTester TEST_HELPER;
  DataFor_CalculateIntensityHighwayExitLeft data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetClosestRelativeLaneIdForJunctionIngoing()).WillByDefault(Return(data.closestRelativeExitLane));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDistanceToEndOfNextExit()).WillByDefault(Return(data.distanceToEndOfExit));

  NiceMock<FakeMicroscopicCharacteristics> fakeMicscopicCharacteristics;
  SurroundingObjectsScmExtended surroundingObject;
  ObjectInformationScmExtended objectinformation;
  objectinformation.isStatic = data.isStatic;
  objectinformation.collision = data.collision;
  surroundingObject.Ahead().Close().push_back(objectinformation);

  ON_CALL(fakeMicscopicCharacteristics, GetSurroundingVehicleInformation()).WillByDefault(Return(&surroundingObject));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicscopicCharacteristics));

  DriverParameters driverParameters{};
  driverParameters.comfortLateralAcceleration = 1.0_mps_sq;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(true)).WillByDefault(Return(0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetRelativeNetDistance(AreaOfInterest::EGO_FRONT)).WillByDefault(Return(100_m));

  LaneInformationGeometrySCM laneInfo{};
  laneInfo.laneType = scm::LaneType::Driving;

  GeometryInformationSCM geometryInfo{};
  geometryInfo.Close().width = 5.0_m;
  geometryInfo.Close().distanceToEndOfLane = 200_m;

  ON_CALL(TEST_HELPER.fakeInfrastructureCharacteristics, GetLaneInformationGeometry(_)).WillByDefault(ReturnRef(laneInfo));
  ON_CALL(TEST_HELPER.fakeInfrastructureCharacteristics, GetGeometryInformation()).WillByDefault(Return(&geometryInfo));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&TEST_HELPER.fakeInfrastructureCharacteristics));

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneTypeDriveable(_)).WillByDefault(Return(data.isLaneTypeDriveable));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsShoulderLaneUsageLegit(_)).WillByDefault(Return(data.isShoulderLaneUsageLegit));

  auto result = TEST_HELPER.laneChangeBehaviorQuery.CalculateIntensityHighwayExitLeft(0.5, data.leftLaneIntensity);

  ASSERT_EQ(result, data.expectedResult);
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    LaneChangeBehaviorQuery_CalculateIntensityHighwayExitLeft,
    testing::Values(
        DataFor_CalculateIntensityHighwayExitLeft{
            .closestRelativeExitLane = 2,
            .distanceToEndOfExit = 150_m,
            .collision = false,
            .isStatic = false,
            .isLaneTypeDriveable = true,
            .isShoulderLaneUsageLegit = false,
            .leftLaneIntensity = 0.8,
            .expectedResult = 0.8},
        DataFor_CalculateIntensityHighwayExitLeft{
            .closestRelativeExitLane = 1,
            .distanceToEndOfExit = 400_m,
            .collision = false,
            .isStatic = false,
            .isLaneTypeDriveable = true,
            .isShoulderLaneUsageLegit = false,
            .leftLaneIntensity = 1.0,
            .expectedResult = 0.5},
        DataFor_CalculateIntensityHighwayExitLeft{
            .closestRelativeExitLane = 1,
            .distanceToEndOfExit = 400_m,
            .collision = true,
            .isStatic = true,
            .isLaneTypeDriveable = false,
            .isShoulderLaneUsageLegit = false,
            .leftLaneIntensity = 0.8,
            .expectedResult = 0.8}));