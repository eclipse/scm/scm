################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME ScmSignalCollector_Tests)
set(DRIVER_DIR ${ROOT_DIR}/module/driver)
set(PARAMETER_DIR ${ROOT_DIR}/module/parameterParser)

add_scm_target(
  NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
  DEFAULT_MAIN

  SOURCES
  TestScmSignalCollector.cpp
  ${DRIVER_DIR}/src/ScmDependencies.cpp
  ${DRIVER_DIR}/src/ScmSignalCollector.cpp

  HEADERS
  ${DRIVER_DIR}/src/ScmDependencies.h
  ${DRIVER_DIR}/src/ScmSignalCollector.h
  ${PARAMETER_DIR}/src/Signals/ParametersScmSignal.h

  INCDIRS
  ${DRIVER_DIR}
  ${DRIVER_DIR}/src

  LIBRARIES
  Stochastics::Stochastics
)

