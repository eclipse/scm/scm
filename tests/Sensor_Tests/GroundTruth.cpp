/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "GroundTruth.h"

namespace osiql::test {
Road::Road(std::string id, osi3::ReferenceLine *referenceLine, size_t referenceLineIndex) :
    id(id), referenceLine(referenceLine), referenceLineIndex(referenceLineIndex)
{
}

osi3::ReferenceLine *GroundTruth::AddReferenceLine(const BoundaryChain &boundaryChain)
{
    osi3::ReferenceLine *referenceLine{handle.add_reference_line()};
    std::vector<osiql::Vector2d> referenceLinePoints;
    {
        referenceLine->mutable_id()->set_value(++idCounter);
        {
            const Boundary &firstBoundary{boundaryChain[0]};
            const osiql::Vector2d &firstPoint{firstBoundary[0]};
            osi3::ReferenceLine_ReferenceLinePoint *referenceLinePoint{referenceLine->add_poly_line()};
            referenceLinePoint->mutable_world_position()->set_x(firstPoint.x);
            referenceLinePoint->mutable_world_position()->set_y(firstPoint.y);
            referenceLinePoints.push_back(firstPoint);
        }
        for (const Boundary &boundary : boundaryChain)
        {
            for (size_t i{1u}; i < boundary.size(); ++i)
            {
                const osiql::Vector2d &point{boundary[i]};
                osi3::ReferenceLine_ReferenceLinePoint *referenceLinePoint{referenceLine->add_poly_line()};
                referenceLinePoint->mutable_world_position()->set_x(point.x);
                referenceLinePoint->mutable_world_position()->set_y(point.y);
                referenceLinePoints.push_back(point);
            }
        }
        double s{0.0};
        referenceLine->mutable_poly_line(0)->set_s_position(s);
        for (size_t j{1u}; j < referenceLinePoints.size(); ++j)
        {
            s += (referenceLinePoints[j] - referenceLinePoints[j - 1]).Length();
            referenceLine->mutable_poly_line(static_cast<int>(j))->set_s_position(s);
        }
    }
    return referenceLine;
}

Road &GroundTruth::AddRoad(const std::vector<BoundaryChain> &boundaryChains, size_t referenceLineIndex, std::vector<std::vector<double>> &laneRows)
{
    if (boundaryChains.empty())
    {
        std::cerr << "GroundTruth::AddRoad: Unable to create road with empty set of boundary chains.";
        return roads.back();
    }
    if (referenceLineIndex >= boundaryChains.size())
    {
        std::cerr << "GroundTruth::AddRoad: Reference line index out of bounds.";
        return roads.back();
    }
    roads.emplace_back("Road" + std::to_string(++roadIdCounter), AddReferenceLine(boundaryChains[referenceLineIndex]), referenceLineIndex);
    AddBoundaryChain(roads.back(), boundaryChains[0]);
    for (size_t i{1u}; i < boundaryChains.size(); ++i)
    {
        AddBoundaryChain(roads.back(), boundaryChains[i]);
    }
    AddLanes(roads.back(), laneRows);
    return roads.back();
}

void GroundTruth::AddBoundaryChain(Road &road, const BoundaryChain &boundaryChain)
{
    road.boundaryChains.push_back({});
    std::vector<osi3::LogicalLaneBoundary *> &chain{road.boundaryChains.back()};
    {
        const osiql::Vector2d *prev{&boundaryChain[0][0]};
        double s{0.0};
        // Create all boundaries specified by the chain
        for (const Boundary &boundary : boundaryChain)
        {
            chain.push_back(handle.add_logical_lane_boundary());
            chain.back()->mutable_id()->set_value(++idCounter);
            chain.back()->mutable_reference_line_id()->set_value(road.referenceLine->id().value());
            for (const osiql::Vector2d &point : boundary)
            {
                osi3::LogicalLaneBoundary_LogicalBoundaryPoint *boundaryPoint{chain.back()->add_boundary_line()};
                boundaryPoint->mutable_position()->set_x(point.x);
                boundaryPoint->mutable_position()->set_y(point.y);
                boundaryPoint->mutable_position()->set_z(0.0);
                s += (point - *prev).Length();
                boundaryPoint->set_s_position(s);
                prev = &point;
            }
        }
        // Scale s-coordinates of all boundaries of this road to match the length of the reference line
        if (s == 0.0)
        {
            std::cerr << "Invalid boundary chain! All points in the boundary chain share the same coordinates.\n";
            return;
        }
        const double ratio{road.referenceLine->poly_line().rbegin()->s_position() / s};
        for (osi3::LogicalLaneBoundary *boundary : chain)
        {
            for (osi3::LogicalLaneBoundary_LogicalBoundaryPoint &point : *boundary->mutable_boundary_line())
            {
                point.set_s_position(point.s_position() * ratio);
            }
        }
    }

    const double factor{Vector2d(chain[0]->boundary_line(0).position()).GetSide(road.referenceLine->poly_line(0).world_position(), road.referenceLine->poly_line(1).world_position()) != Side::Right ? 1.0 : -1.0};
    // Assign t-coordinates
    for (osi3::LogicalLaneBoundary *boundary : chain)
    {
        for (osi3::LogicalLaneBoundary_LogicalBoundaryPoint &point : *boundary->mutable_boundary_line())
        {
            auto it{std::min_element(road.referenceLine->poly_line().begin(), road.referenceLine->poly_line().end(), [point = Vector2d(point)](const osi3::ReferenceLine_ReferenceLinePoint &lhs, const osi3::ReferenceLine_ReferenceLinePoint &rhs) {
                return (osiql::Vector2d(lhs.world_position()) - point).SquaredLength() < (osiql::Vector2d(rhs.world_position()) - point).SquaredLength();
            })};
            if (it == road.referenceLine->poly_line().begin())
            {
                point.set_t_position(factor * Vector2d(point).GetDistanceTo(*it, *std::next(it)));
            }
            else if (std::next(it) == road.referenceLine->poly_line().end())
            {
                point.set_t_position(factor * Vector2d(point).GetDistanceTo(*std::prev(it), *it));
            }
            else
            {
                const osiql::Vector2d distanceA{Vector2d(point).GetDistanceTo(*std::prev(it), *it)};
                const osiql::Vector2d distanceB{Vector2d(point).GetDistanceTo(*it, *std::next(it))};
                if (distanceA.SquaredLength() < distanceB.SquaredLength())
                {
                    point.set_t_position(factor * distanceA.Length());
                }
                else
                {
                    point.set_t_position(factor * distanceB.Length());
                }
            }
        }
    }
}

std::vector<osi3::LogicalLaneBoundary *>::iterator GroundTruth::InsertPointAtCoordinate(std::vector<osi3::LogicalLaneBoundary *> &boundaryChain, double s)
{
    // clang-format off
    std::vector<osi3::LogicalLaneBoundary*>::iterator result{std::upper_bound(boundaryChain.begin(), std::prev(boundaryChain.end()), s, 
        [](double s, const osi3::LogicalLaneBoundary *boundary) { return boundary->boundary_line().rbegin()->s_position() >= s; }
    )};
    auto &boundary{*(*result)->mutable_boundary_line()};
    const size_t k{static_cast<size_t>(std::distance(boundary.rbegin(), std::upper_bound(boundary.rbegin(), std::prev(boundary.rend()), 
        s, [](double s, const osi3::LogicalLaneBoundary_LogicalBoundaryPoint &point) { return point.s_position() <= s; })
    ))};
    // clang-format on
    const size_t k_{boundary.size() - (k + 1)};
    if (boundary[k_].s_position() < s)
    {
        // Adding a point to the container might invalidate iterators, which is why indices (k_) are used instead.
        osi3::LogicalLaneBoundary_LogicalBoundaryPoint *point{boundary.Add()};
        const double ratio{(s - boundary[k_].s_position()) / (boundary[k_ + 1].s_position() - boundary[k_].s_position())};
        osiql::Vector2d globalCoordinates{Vector2d(boundary[k_].position()) * (1.0 - ratio) + Vector2d(boundary[k_ + 1].position()) * ratio};
        point->mutable_position()->set_x(globalCoordinates.x);
        point->mutable_position()->set_y(globalCoordinates.y);
        point->mutable_position()->set_z(0.0);
        point->set_s_position(boundary[k_].s_position() * (1.0 - ratio) + boundary[k_ + 1].s_position() * ratio);
        point->set_t_position(boundary[k_].t_position() * (1.0 - ratio) + boundary[k_ + 1].t_position() * ratio);
        std::rotate(boundary.rbegin(), std::next(boundary.rbegin()), std::next(boundary.rbegin(), k + 1));
    }
    return result;
}

void GroundTruth::AddLanes(Road &road, std::vector<std::vector<double>> &laneRows)
{
    const double length{(*road.boundaryChains[0].rbegin())->boundary_line().rbegin()->s_position()};
    for (size_t i{1u}; i < road.boundaryChains.size(); ++i)
    {
        road.laneRows.push_back({});
        std::vector<osi3::LogicalLaneBoundary *>::iterator leftBoundary{road.boundaryChains[i].begin()};
        std::vector<osi3::LogicalLaneBoundary *>::iterator rightBoundary{road.boundaryChains[i - 1].begin()};
        std::vector<double> &lanes{laneRows[i - 1]};
        const double ratio{length / lanes.back()};
        for (size_t j{1u}; j < lanes.size(); ++j)
        {
            lanes[j] *= ratio;
            road.laneRows.back().push_back(handle.add_logical_lane());
            osi3::LogicalLane &lane{*road.laneRows.back().back()};
            lane.mutable_id()->set_value(++idCounter);
            lane.add_source_reference()->add_identifier(road.id);
            lane.mutable_reference_line_id()->set_value(road.referenceLine->id().value());
            lane.set_move_direction(i <= road.referenceLineIndex ? osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_INCREASING_S : osi3::LogicalLane_MoveDirection_MOVE_DIRECTION_DECREASING_S);
            auto lastLeft{InsertPointAtCoordinate(road.boundaryChains[i], lanes[j])};
            for (; leftBoundary != lastLeft; ++leftBoundary)
            {
                lane.add_left_boundary_id()->set_value((*leftBoundary)->id().value());
            }
            lane.add_left_boundary_id()->set_value((*leftBoundary)->id().value());
            if (lanes[j] == (*lastLeft)->boundary_line().rbegin()->s_position())
            {
                ++leftBoundary;
            }
            auto lastRight{InsertPointAtCoordinate(road.boundaryChains[i - 1], lanes[j])};
            for (; rightBoundary != lastRight; ++rightBoundary)
            {
                lane.add_right_boundary_id()->set_value((*rightBoundary)->id().value());
            }
            lane.add_right_boundary_id()->set_value((*rightBoundary)->id().value());
            if (lanes[j] == (*lastRight)->boundary_line().rbegin()->s_position())
            {
                ++rightBoundary;
            }
            lane.set_start_s(lanes[j - 1]);
            lane.set_end_s(lanes[j]);
            lane.set_type(osi3::LogicalLane_Type_TYPE_NORMAL);
        }
    }
    // Successor lanes
    for (std::vector<osi3::LogicalLane *> &lanes : road.laneRows)
    {
        for (auto it{std::next(lanes.begin())}; it != lanes.end(); ++it)
        {
            osi3::LogicalLane_LaneConnection *connection{(*std::prev(it))->add_successor_lane()};
            connection->mutable_other_lane_id()->set_value((*it)->id().value());
            connection->set_at_begin_of_other_lane(true);

            connection = (*it)->add_predecessor_lane();
            connection->mutable_other_lane_id()->set_value((*std::prev(it))->id().value());
            connection->set_at_begin_of_other_lane(false);
        }
    }
    // Adjacent lanes
    for (size_t i{1u}; i < road.laneRows.size(); ++i)
    {
        std::vector<osi3::LogicalLane *>::iterator leftLane{road.laneRows[i].begin()};
        std::vector<osi3::LogicalLane *>::iterator rightLane{road.laneRows[i - 1].begin()};
        while (leftLane != road.laneRows[i].end() /*&& rightLane != road.laneRows[i - 1].end()*/)
        {
            osi3::LogicalLane_LaneRelation *relation{(*leftLane)->add_right_adjacent_lane()};
            relation->set_start_s(std::max((*leftLane)->start_s(), (*rightLane)->start_s()));
            relation->set_start_s_other(std::max((*leftLane)->start_s(), (*rightLane)->start_s()));
            relation->set_end_s(std::min((*leftLane)->end_s(), (*rightLane)->end_s()));
            relation->set_end_s_other(std::min((*leftLane)->end_s(), (*rightLane)->end_s()));
            relation->mutable_other_lane_id()->set_value((*rightLane)->id().value());

            relation = (*rightLane)->add_left_adjacent_lane();
            relation->set_start_s(std::max((*rightLane)->start_s(), (*leftLane)->start_s()));
            relation->set_start_s_other(std::max((*rightLane)->start_s(), (*leftLane)->start_s()));
            relation->set_end_s(std::min((*rightLane)->end_s(), (*leftLane)->end_s()));
            relation->set_end_s_other(std::min((*rightLane)->end_s(), (*leftLane)->end_s()));
            relation->mutable_other_lane_id()->set_value((*leftLane)->id().value());

            if ((*leftLane)->end_s() == (*rightLane)->end_s())
            {
                ++leftLane;
                ++rightLane;
            }
            else if ((*leftLane)->end_s() < (*rightLane)->end_s())
            {
                ++leftLane;
            }
            else if ((*leftLane)->end_s() > (*rightLane)->end_s())
            {
                ++rightLane;
            }
        }
    }
}
} // namespace osiql::test
