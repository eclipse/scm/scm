/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <OsiQueryLibrary/osiql.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "TestGroundtruthGenerator.h"
#include "module/driver/src/ScmDefinitions.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

struct ExtrapolationTester
{
  class ExtrapolationUnderTest : Extrapolation
  {
  public:
    template <typename... Args>
    ExtrapolationUnderTest(Args&&... args)
        : Extrapolation{std::forward<Args>(args)...} {};

    using Extrapolation::ExtrapolateLongitudinalAcceleration;
    using Extrapolation::ExtrapolateSurroundingVehicleDataLateral;
    using Extrapolation::ExtrapolateSurroundingVehicleDataLongitudinal;
    using Extrapolation::ProjectSurroundingVehicle;
  };

  template <typename T>
  T& WITH_CYCLETIME_IN_S(T& fakeMentalModel, units::time::second_t cycleTime)
  {
    ON_CALL(fakeMentalModel, GetCycleTime()).WillByDefault(Return(cycleTime));
    return fakeMentalModel;
  }

  OwnVehicleRoutePose CREATE_EXTRAPLOATION_INFORMATION(const osiql::Query& query)
  {
    auto osiqlLane = query.GetLane(5);
    auto referenceLine = query.GetReferenceLine(0);
    auto referenceLine1 = query.GetReferenceLine(9);

    OwnVehicleRoutePose ownVehicleRoutePose{
        .route = std::make_shared<osiql::Route>(query.GetRoute(osiql::Vector2d{referenceLine->front()}, osiql::Vector2d{referenceLine1->back()})),
        .pose = std::make_shared<osiql::Pose<osiql::Point<const osiql::Lane>>>(osiql::Point<const osiql::Lane>{osiql::ReferenceLineCoordinates{75, -1.5}, *osiqlLane})};

    return ownVehicleRoutePose;
  }

  ExtrapolationTester()
      : fakeMentalModel{},
        extrapolation(WITH_CYCLETIME_IN_S(fakeMentalModel, 100_ms), ownVehicleRoutePose)
  {
    groundTruth = CREATE_GROUNDTRUTH();
    query = std::make_unique<osiql::Query>(groundTruth);
    ownVehicleRoutePose = CREATE_EXTRAPLOATION_INFORMATION(*query);
  }
  NiceMock<FakeMentalModel> fakeMentalModel;

  osi3::GroundTruth groundTruth;
  std::unique_ptr<osiql::Query> query;
  OwnVehicleRoutePose ownVehicleRoutePose;

  ExtrapolationUnderTest extrapolation;
};

/*************************************************
 * CHECK ExtrapolateSurroundingVehicleDataLateral *
 **************************************************/

TEST(Extrapolation_UnitTests, Extrapolation_CheckFunction_ExtrapolateSurroundingVehicleDataLateral)
{
  ExtrapolationTester TEST_HELPER;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCycleTime()).WillByDefault(Return(100_ms));

  // Set up test
  InfrastructureCharacteristics infrastructureCharacteristics;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;

  OwnVehicleInformationScmExtended agentEgo;
  agentEgo.id = 0;
  agentEgo.lateralVelocity = 0._mps;
  agentEgo.lateralPosition = 0._m;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocity()).WillByDefault(Return(agentEgo.lateralVelocity));

  ObjectInformationScmExtended agentObserved;
  agentObserved.ResetToDefault();
  agentObserved.id = 1;
  agentObserved.exist = true;
  agentObserved.lateralVelocity = 0._mps;
  agentObserved.lateralPositionInLane = 0._m;
  agentObserved.distanceToLaneBoundaryLeft = .875_m;
  agentObserved.distanceToLaneBoundaryRight = .875_m;
  agentObserved.relativeLateralDistance = 0._m;
  agentObserved.obstruction = ObstructionScm(2._m, 2._m, 0._m);

  ObjectInformationScmExtended agentObserved2;
  agentObserved2.ResetToDefault();
  agentObserved2.id = 2;
  agentObserved2.exist = true;
  agentObserved2.lateralVelocity = 0._mps;
  agentObserved2.lateralPositionInLane = 0._m;
  agentObserved2.distanceToLaneBoundaryLeft = .875_m;
  agentObserved2.distanceToLaneBoundaryRight = .875_m;
  agentObserved2.relativeLateralDistance = 0._m;
  agentObserved2.obstruction = ObstructionScm(1._m, -1._m, 0._m);

  std::vector<ObjectInformationScmExtended> sideObjectsScm = {agentObserved, agentObserved2};

  infrastructureCharacteristics.UpdateGeometryInformation()->Close().width = 3.75_m;
  infrastructureCharacteristics.UpdateGeometryInformation()->Left().width = 3.75_m;
  infrastructureCharacteristics.UpdateGeometryInformation()->Right().width = 3.75_m;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetInfrastructureCharacteristics()).WillByDefault(Return(&infrastructureCharacteristics));
  ON_CALL(fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).WillByDefault(Return(&agentEgo));

  // Test 1: Extrapolate without any lateral movement in front of ego
  AreaOfInterest aoi{AreaOfInterest::EGO_FRONT};
  ON_CALL(fakeMicroscopicCharacteristics, UpdateObjectInformation(_, _)).WillByDefault(Return(&agentObserved));
  ObjectInformationScmExtended testAgent = agentObserved;

  TEST_HELPER.extrapolation.ExtrapolateSurroundingVehicleDataLateral(&testAgent, aoi);
  const ObjectInformationScmExtended* result{&agentObserved};
  ASSERT_EQ(result->lateralVelocity, 0._mps);
  ASSERT_EQ(result->lateralPositionInLane, 0._m);
  ASSERT_EQ(result->distanceToLaneBoundaryLeft, .875_m);
  ASSERT_EQ(result->distanceToLaneBoundaryRight, .875_m);
  ASSERT_EQ(result->relativeLateralDistance, 0._m);
  ASSERT_TRUE(result->obstruction.left == 2._m && result->obstruction.right == 2._m &&
              result->obstruction.isOverlapping && result->obstruction.mainLaneLocator == 0._m);
  testAgent.ResetToDefault();

  // Test 2: Extrapolate without lateral displacement in right front of ego moving left
  // (reset to lane keeping, because driver won't expect endless lateral movement after lane change)
  aoi = AreaOfInterest::LEFT_FRONT;

  testAgent = agentObserved;
  testAgent.lateralVelocity = 1._mps;
  testAgent.obstruction = ObstructionScm(5.75_m, -1.75_m, 0._m);
  TEST_HELPER.extrapolation.ExtrapolateSurroundingVehicleDataLateral(&testAgent, aoi);
  ASSERT_EQ(testAgent.lateralVelocity, 0._mps);
  ASSERT_EQ(testAgent.lateralPositionInLane, 0._m);
  ASSERT_EQ(testAgent.distanceToLaneBoundaryLeft, .875_m);
  ASSERT_EQ(testAgent.distanceToLaneBoundaryRight, .875_m);
  ASSERT_TRUE(testAgent.obstruction.left == 5.75_m && testAgent.obstruction.right == -1.75_m &&
              !testAgent.obstruction.isOverlapping && testAgent.obstruction.mainLaneLocator == 0._m);
  testAgent.ResetToDefault();

  // Test 3: Extrapolate lateral displacement in right rear of ego moving right
  aoi = AreaOfInterest::RIGHT_REAR;
  testAgent = agentObserved;
  testAgent.lateralVelocity = -1._mps;
  testAgent.lateralPositionInLane = -.5_m;
  testAgent.distanceToLaneBoundaryLeft = 1.375_m;
  testAgent.distanceToLaneBoundaryRight = .375_m;
  testAgent.obstruction = ObstructionScm(-2.25_m, 6.25_m, 0._m);
  TEST_HELPER.extrapolation.ExtrapolateSurroundingVehicleDataLateral(&testAgent, aoi);
  ASSERT_EQ(testAgent.lateralVelocity, -1._mps);
  ASSERT_EQ(testAgent.lateralPositionInLane, -.6_m);
  ASSERT_EQ(testAgent.distanceToLaneBoundaryLeft, 1.475_m);
  ASSERT_EQ(testAgent.distanceToLaneBoundaryRight, .275_m);
  ASSERT_TRUE(testAgent.obstruction.left == -2.35_m && testAgent.obstruction.right == 6.35_m &&
              !testAgent.obstruction.isOverlapping && testAgent.obstruction.mainLaneLocator == 0._m);
  testAgent.ResetToDefault();

  // Test 4: Extrapolate lateral displacement in right front of ego moving left

  agentEgo.lateralPosition = 1._m;
  agentEgo.lateralVelocity = 1._mps;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocity()).WillByDefault(Return(agentEgo.lateralVelocity));
  aoi = AreaOfInterest::RIGHT_FRONT;
  testAgent = agentObserved;
  testAgent.lateralVelocity = 1._mps;
  testAgent.lateralPositionInLane = -.5_m;
  testAgent.distanceToLaneBoundaryLeft = 1.375_m;
  testAgent.distanceToLaneBoundaryRight = .375_m;
  testAgent.obstruction = ObstructionScm(-3.25_m, 7.25_m, 0._m);
  TEST_HELPER.extrapolation.ExtrapolateSurroundingVehicleDataLateral(&testAgent, aoi);

  ASSERT_EQ(testAgent.lateralVelocity, 1._mps);
  ASSERT_EQ(testAgent.lateralPositionInLane, -.4_m);
  ASSERT_EQ(testAgent.distanceToLaneBoundaryLeft, 1.275_m);
  ASSERT_EQ(testAgent.distanceToLaneBoundaryRight, .475_m);
  ASSERT_TRUE(testAgent.obstruction.left == -3.25_m && testAgent.obstruction.right == 7.25_m &&
              !testAgent.obstruction.isOverlapping && testAgent.obstruction.mainLaneLocator == 0._m);
  testAgent.ResetToDefault();
  agentEgo.lateralPosition = 0._m;
  agentEgo.lateralVelocity = 0._mps;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLateralVelocity()).WillByDefault(Return(agentEgo.lateralVelocity));

  // Test 5: Extrapolate with lateral movement in front of ego, leaving obstruction to the right (testing obstruction only)
  aoi = AreaOfInterest::EGO_FRONT;
  testAgent = agentObserved;
  testAgent.lateralVelocity = -1._mps;
  testAgent.lateralPositionInLane = -1.95_m;
  testAgent.obstruction = ObstructionScm(.05_m, 3.95_m, 0._m);
  TEST_HELPER.extrapolation.ExtrapolateSurroundingVehicleDataLateral(&testAgent, aoi);
  ASSERT_TRUE(testAgent.obstruction.left == -.05_m && testAgent.obstruction.right == 4.05_m &&
              !testAgent.obstruction.isOverlapping && testAgent.obstruction.mainLaneLocator == 0._m);
  testAgent.ResetToDefault();

  // Test 6: Extrapolate with lateral movement in front of ego, entering obstruction from the right (testing obstruction only)
  aoi = AreaOfInterest::EGO_FRONT;
  testAgent = agentObserved;
  testAgent.lateralVelocity = 1._mps;
  testAgent.lateralPositionInLane = -2.05_m;
  testAgent.obstruction = ObstructionScm(-.05_m, 4.05_m, 0._m);
  TEST_HELPER.extrapolation.ExtrapolateSurroundingVehicleDataLateral(&testAgent, aoi);
  ASSERT_EQ(testAgent.obstruction.left, .05_m);
  ASSERT_EQ(testAgent.obstruction.right, 3.95_m);
  ASSERT_EQ(testAgent.obstruction.mainLaneLocator, 0._m);
  ASSERT_TRUE(testAgent.obstruction.isOverlapping);
  testAgent.ResetToDefault();

  // Test 7: Extrapolate with lateral movement in front of ego, leaving obstruction to the left (testing obstruction only)
  aoi = AreaOfInterest::EGO_FRONT;
  testAgent = agentObserved;
  testAgent.lateralVelocity = 1._mps;
  testAgent.lateralPositionInLane = 1.95_m;
  testAgent.obstruction = ObstructionScm(3.95_m, .05_m, 0._m);
  TEST_HELPER.extrapolation.ExtrapolateSurroundingVehicleDataLateral(&testAgent, aoi);
  ASSERT_EQ(testAgent.obstruction.left, 4.05_m);
  ASSERT_EQ(testAgent.obstruction.right, -.05_m);
  ASSERT_EQ(testAgent.obstruction.mainLaneLocator, 0._m);
  ASSERT_TRUE(!testAgent.obstruction.isOverlapping);
  testAgent.ResetToDefault();

  // Test 8: Extrapolate with lateral movement in front of ego, entering obstruction from the left (testing obstruction only)
  aoi = AreaOfInterest::EGO_FRONT;
  testAgent = agentObserved;
  testAgent.lateralVelocity = -1._mps;
  testAgent.lateralPositionInLane = 2.05_m;
  testAgent.obstruction = ObstructionScm(4.05_m, -.05_m, 0._m);
  TEST_HELPER.extrapolation.ExtrapolateSurroundingVehicleDataLateral(&testAgent, aoi);
  ASSERT_EQ(testAgent.obstruction.left, 3.95_m);
  ASSERT_EQ(testAgent.obstruction.right, .05_m);
  ASSERT_EQ(testAgent.obstruction.mainLaneLocator, 0._m);
  ASSERT_TRUE(testAgent.obstruction.isOverlapping);
}

/*********************************************
 * CHECK ExtrapolateLongitudinalAcceleration *
 *********************************************/

/// \brief Data table for definition of individual test cases for CalculateLateralOffset
struct DataForExtrapolateLongitudinalAcceleration
{
  units::acceleration::meters_per_second_squared_t input_ComfortLongAcceleration;
  units::acceleration::meters_per_second_squared_t input_ComfortLongDeceleration;
  units::acceleration::meters_per_second_squared_t input_MaxLongAcceleration;
  units::acceleration::meters_per_second_squared_t input_MaxLongDeceleration;
  units::acceleration::meters_per_second_squared_t input_Acceleration;
  units::acceleration::meters_per_second_squared_t input_AccelerationChange;
  units::acceleration::meters_per_second_squared_t result_Acceleration;
  units::acceleration::meters_per_second_squared_t result_AccelerationChange;
};

class Extrapolation_ExtrapolateLongitudinalAcceleration : public ::testing::Test,
                                                          public ::testing::WithParamInterface<DataForExtrapolateLongitudinalAcceleration>
{
};

TEST_P(Extrapolation_ExtrapolateLongitudinalAcceleration, Extrapolation_CheckFunction_ExtrapolateLongitudinalAcceleration)
{
  // Create required classes for test
  DataForExtrapolateLongitudinalAcceleration data = GetParam();

  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;

  ExtrapolationTester TEST_HELPER;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));

  // Set up test suite
  DriverParameters driverParameters;
  driverParameters.comfortLongitudinalAcceleration = data.input_ComfortLongAcceleration;
  driverParameters.comfortLongitudinalDeceleration = data.input_ComfortLongDeceleration;
  driverParameters.maximumLongitudinalAcceleration = data.input_MaxLongAcceleration;
  driverParameters.maximumLongitudinalDeceleration = data.input_MaxLongDeceleration;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalAcceleration()).WillByDefault(Return(driverParameters.comfortLongitudinalAcceleration));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(driverParameters.comfortLongitudinalDeceleration));

  ObjectInformationScmExtended surroundingObject;
  surroundingObject.ResetToDefault();
  surroundingObject.acceleration = data.input_Acceleration;
  surroundingObject.accelerationLastStep = data.input_Acceleration;
  surroundingObject.accelerationChange = data.input_AccelerationChange;

  // Call test
  TEST_HELPER.extrapolation.ExtrapolateLongitudinalAcceleration(&surroundingObject);
  auto resultAcceleration = surroundingObject.acceleration;
  auto resultAccelerationChange = surroundingObject.accelerationChange;

  // Evaluate results
  EXPECT_NEAR(data.result_Acceleration.value(), resultAcceleration.value(), 1e-3);
  EXPECT_NEAR(data.result_AccelerationChange.value(), resultAccelerationChange.value(), 1e-3);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    Extrapolation_ExtrapolateLongitudinalAcceleration,
    testing::Values(
        // No extrapolation possible, Result: take old values
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = 0.5_mps_sq,
            .input_AccelerationChange = units::acceleration::meters_per_second_squared_t(ScmDefinitions::DEFAULT_VALUE),
            .result_Acceleration = 0.5_mps_sq,
            .result_AccelerationChange = units::acceleration::meters_per_second_squared_t(ScmDefinitions::DEFAULT_VALUE)},
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = -0.5_mps_sq,
            .input_AccelerationChange = 0.0_mps_sq,
            .result_Acceleration = -0.5_mps_sq,
            .result_AccelerationChange = 0.0_mps_sq},
        // Before: inside comfort, After: inside comfort, Result: sum up values
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = 0.1_mps_sq,
            .input_AccelerationChange = 0.2_mps_sq,
            .result_Acceleration = 0.3_mps_sq,
            .result_AccelerationChange = 0.17_mps_sq},
        // Pass acceleration zero, Result: acceleration and acceleration change become zero
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = 0.5_mps_sq,
            .input_AccelerationChange = -3.0_mps_sq,
            .result_Acceleration = 0._mps_sq,
            .result_AccelerationChange = 0._mps_sq},
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = -0.5_mps_sq,
            .input_AccelerationChange = 8.0_mps_sq,
            .result_Acceleration = -0._mps_sq,
            .result_AccelerationChange = 0._mps_sq},
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = -0.1_mps_sq,
            .input_AccelerationChange = -0.2_mps_sq,
            .result_Acceleration = -0.3_mps_sq,
            .result_AccelerationChange = -0.17_mps_sq},
        // Before: inside comfort, After: outside comfort and inside max, Result: take comfort
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = 0.1_mps_sq,
            .input_AccelerationChange = 0.5_mps_sq,
            .result_Acceleration = 0.5_mps_sq,
            .result_AccelerationChange = 0.425_mps_sq},
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = -0.1_mps_sq,
            .input_AccelerationChange = -1.5_mps_sq,
            .result_Acceleration = -1.0_mps_sq,
            .result_AccelerationChange = -1.275_mps_sq},
        // Before: inside comfort, After: outside max, Result: take comfort
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = 0.1_mps_sq,
            .input_AccelerationChange = 5.0_mps_sq,
            .result_Acceleration = 0.5_mps_sq,
            .result_AccelerationChange = 4.25_mps_sq},
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = -0.1_mps_sq,
            .input_AccelerationChange = -8.0_mps_sq,
            .result_Acceleration = -1.0_mps_sq,
            .result_AccelerationChange = -6.8_mps_sq},
        // Before: outside comfort, After: inside comfort, Result: sum up values
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = 0.8_mps_sq,
            .input_AccelerationChange = -0.5_mps_sq,
            .result_Acceleration = 0.3_mps_sq,
            .result_AccelerationChange = -0.425_mps_sq},
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = -1.2_mps_sq,
            .input_AccelerationChange = 0.3_mps_sq,
            .result_Acceleration = -0.9_mps_sq,
            .result_AccelerationChange = 0.255_mps_sq},
        // Before: outside comfort, After: outside comfort and inside max, Result: sum up values
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = 0.8_mps_sq,
            .input_AccelerationChange = 0.3_mps_sq,
            .result_Acceleration = 1.1_mps_sq,
            .result_AccelerationChange = 0.255_mps_sq},
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = -1.2_mps_sq,
            .input_AccelerationChange = -0.1_mps_sq,
            .result_Acceleration = -1.3_mps_sq,
            .result_AccelerationChange = -0.085_mps_sq},
        // Before: outside comfort, After: outside max, Result: take max
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = 3.0_mps_sq,
            .input_AccelerationChange = 2.0_mps_sq,
            .result_Acceleration = 4.0_mps_sq,
            .result_AccelerationChange = 1.7_mps_sq},
        DataForExtrapolateLongitudinalAcceleration{
            .input_ComfortLongAcceleration = 0.5_mps_sq,
            .input_ComfortLongDeceleration = 1.0_mps_sq,
            .input_MaxLongAcceleration = 4.0_mps_sq,
            .input_MaxLongDeceleration = 7.0_mps_sq,
            .input_Acceleration = -6.0_mps_sq,
            .input_AccelerationChange = -2.0_mps_sq,
            .result_Acceleration = -7.0_mps_sq,
            .result_AccelerationChange = -1.7_mps_sq}));

/*******************************************************
 * CHECK ExtrapolateSurroundingVehicleDataLongitudinal *
 *******************************************************/

struct DataFor_CheckFunction_ExtrapolateSurroundingVehicleDataLongitudinal
{
  AreaOfInterest aoi;
  units::time::second_t ttc;
  units::velocity::meters_per_second_t longitudinalVelocity;
  units::acceleration::meters_per_second_squared_t acceleration;

  units::acceleration::meters_per_second_squared_t expectedAcceleration;
  units::acceleration::meters_per_second_squared_t expectedAccelerationChange;
  units::velocity::meters_per_second_t expectedAbsoluteVelocity;
  units::velocity::meters_per_second_t expectedLongitudinalVelocity;
  units::time::second_t expectedGap;
  double expectedGapDot;
  units::time::second_t expectedTtc;
  double expectedTauDot;
};

class Extrapolation_ExtrapolateSurroundingVehicleDataLongitudinal : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CheckFunction_ExtrapolateSurroundingVehicleDataLongitudinal>
{
};

TEST_P(Extrapolation_ExtrapolateSurroundingVehicleDataLongitudinal, CheckFunction_ExtrapolateSurroundingVehicleDataLongitudinal)
{
  ExtrapolationTester TEST_HELPER;
  DataFor_CheckFunction_ExtrapolateSurroundingVehicleDataLongitudinal data = GetParam();

  ObjectInformationScmExtended surroundingObject;
  surroundingObject.id = 1;
  surroundingObject.accelerationChange = 5.0_mps_sq;
  surroundingObject.acceleration = data.acceleration;
  surroundingObject.accelerationLastStep = data.acceleration;
  surroundingObject.absoluteVelocity = 10.0_mps;
  surroundingObject.longitudinalVelocity = data.longitudinalVelocity;
  surroundingObject.relativeLongitudinalDistance = 20.0_m;
  surroundingObject.ttc = data.ttc;

  ObstructionScm obstruction{
      1.0_m,
      1.0_m,
      0.0_m};

  surroundingObject.obstruction = obstruction;

  DriverParameters driverParameters;
  driverParameters.maximumLongitudinalAcceleration = 5.0_mps_sq;
  driverParameters.maximumLongitudinalDeceleration = 5.0_mps_sq;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(3.0_mps_sq));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalAcceleration()).WillByDefault(Return(2.0_mps_sq));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(10.0_mps));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAcceleration()).WillByDefault(Return(10.0_mps_sq));

  TEST_HELPER.extrapolation.ExtrapolateSurroundingVehicleDataLongitudinal(&surroundingObject, data.aoi);

  ASSERT_EQ(surroundingObject.absoluteVelocity, data.expectedAbsoluteVelocity);
  ASSERT_EQ(surroundingObject.longitudinalVelocity, data.expectedLongitudinalVelocity);
  ASSERT_EQ(surroundingObject.accelerationChange, data.expectedAccelerationChange);
  ASSERT_EQ(surroundingObject.acceleration, data.expectedAcceleration);
  ASSERT_EQ(surroundingObject.tauDot, data.expectedTauDot);
  ASSERT_EQ(surroundingObject.gap, data.expectedGap);
  ASSERT_EQ(surroundingObject.gapDot, data.expectedGapDot);
  ASSERT_EQ(surroundingObject.ttc, data.expectedTtc);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    Extrapolation_ExtrapolateSurroundingVehicleDataLongitudinal,
    ::testing::Values(
        DataFor_CheckFunction_ExtrapolateSurroundingVehicleDataLongitudinal{
            .aoi = AreaOfInterest::LEFT_SIDE,
            .ttc = 120.0_s,
            .longitudinalVelocity = 10.0_mps,
            .acceleration = 6.0_mps_sq,
            .expectedAcceleration = 5_mps_sq,
            .expectedAccelerationChange = 4.25_mps_sq,
            .expectedAbsoluteVelocity = 10.5_mps,
            .expectedLongitudinalVelocity = 10.5_mps,
            .expectedGap = 0_s,
            .expectedGapDot = 0,
            .expectedTtc = ScmDefinitions::TTC_LIMIT,
            .expectedTauDot = 0},
        DataFor_CheckFunction_ExtrapolateSurroundingVehicleDataLongitudinal{
            .aoi = AreaOfInterest::EGO_FRONT,
            .ttc = 120.0_s,
            .longitudinalVelocity = 10.0_mps,
            .acceleration = 6.0_mps_sq,
            .expectedAcceleration = 5_mps_sq,
            .expectedAccelerationChange = 4.25_mps_sq,
            .expectedAbsoluteVelocity = 10.5_mps,
            .expectedLongitudinalVelocity = 10.5_mps,
            .expectedGap = 2_s,
            .expectedGapDot = -2,
            .expectedTtc = 120_s,
            .expectedTauDot = 0},
        DataFor_CheckFunction_ExtrapolateSurroundingVehicleDataLongitudinal{
            .aoi = AreaOfInterest::EGO_FRONT,
            .ttc = 80.0_s,
            .longitudinalVelocity = 10.0_mps,
            .acceleration = 6.0_mps_sq,
            .expectedAcceleration = 5_mps_sq,
            .expectedAccelerationChange = 4.25_mps_sq,
            .expectedAbsoluteVelocity = 10.5_mps,
            .expectedLongitudinalVelocity = 10.5_mps,
            .expectedGap = 2_s,
            .expectedGapDot = -2,
            .expectedTtc = -99_s,
            .expectedTauDot = -INFINITY},
        DataFor_CheckFunction_ExtrapolateSurroundingVehicleDataLongitudinal{
            .aoi = AreaOfInterest::EGO_FRONT,
            .ttc = 80.0_s,
            .longitudinalVelocity = 0.0_mps,
            .acceleration = -6.0_mps_sq,
            .expectedAcceleration = 0_mps_sq,
            .expectedAccelerationChange = 0_mps_sq,
            .expectedAbsoluteVelocity = 9.9_mps,
            .expectedLongitudinalVelocity = 0_mps,
            .expectedGap = 1.9_s,
            .expectedGapDot = -2.9,
            .expectedTtc = 1.9_s,
            .expectedTauDot = -2.9}));

/***********************************
 * CHECK ProjectSurroundingVehicle *
 ***********************************/

struct DataFor_CheckFunction_ProjectSurroundingVehicle
{
  units::length::meter_t longitudinalDistanceFrontBumperToRearAxle;
  units::length::meter_t relativeLateralDistance;

  units::length::meter_t result_relativeX;
  units::length::meter_t result_relativeY;
};

class Extrapolation_ProjectSurroundingVehicle : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CheckFunction_ProjectSurroundingVehicle>
{
};

TEST_P(Extrapolation_ProjectSurroundingVehicle, CheckFunction_ProjectSurroundingVehicle)
{
  ExtrapolationTester TEST_HELPER;
  DataFor_CheckFunction_ProjectSurroundingVehicle data = GetParam();

  ObjectInformationScmExtended surroundingVehicle;

  surroundingVehicle.longitudinalDistanceFrontBumperToRearAxle = data.longitudinalDistanceFrontBumperToRearAxle;
  surroundingVehicle.relativeLateralDistance = data.relativeLateralDistance;

  TEST_HELPER.extrapolation.ProjectSurroundingVehicle(&surroundingVehicle);

  ASSERT_EQ(surroundingVehicle.relativeX, data.result_relativeX);
  ASSERT_EQ(surroundingVehicle.relativeY, data.result_relativeY);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    Extrapolation_ProjectSurroundingVehicle,
    ::testing::Values(
        DataFor_CheckFunction_ProjectSurroundingVehicle{
            .longitudinalDistanceFrontBumperToRearAxle = 5_m,
            .relativeLateralDistance = 0_m,
            .result_relativeX = 5_m,
            .result_relativeY = 0_m},
        DataFor_CheckFunction_ProjectSurroundingVehicle{
            .longitudinalDistanceFrontBumperToRearAxle = -5_m,
            .relativeLateralDistance = 0_m,
            .result_relativeX = -5_m,
            .result_relativeY = 0_m},
        DataFor_CheckFunction_ProjectSurroundingVehicle{
            .longitudinalDistanceFrontBumperToRearAxle = 5_m,
            .relativeLateralDistance = 1_m,
            .result_relativeX = 5_m,
            .result_relativeY = 1_m},
        DataFor_CheckFunction_ProjectSurroundingVehicle{
            .longitudinalDistanceFrontBumperToRearAxle = -5_m,
            .relativeLateralDistance = -1_m,
            .result_relativeX = -5_m,
            .result_relativeY = -1_m},
        DataFor_CheckFunction_ProjectSurroundingVehicle{
            .longitudinalDistanceFrontBumperToRearAxle = 26_m,
            .relativeLateralDistance = -1_m,
            .result_relativeX = -999_m,
            .result_relativeY = -999_m}));