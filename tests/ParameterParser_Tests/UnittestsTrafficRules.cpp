/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../module/parameterParser/src/TrafficRules/TrafficRules.h"
#include "include/common/ScmDefinitions.h"

/******************************
 * CHECK GenerateTrafficRules *
 ******************************/
using namespace units::literals;
struct DataFor_GenerateTrafficRules
{
  std::string country;

  bool expectedResult_common_handsFreePhoneMandatoryApplicable;
  bool expectedResult_common_handsFreePhoneMandatoryValue;
  bool expectedResult_common_rightHandTrafficApplicable;
  bool expectedResult_common_rightHandTrafficValue;
  bool expectedResult_urban_OpenSpeedLimit_Applicable;
  units::velocity::meters_per_second_t expectedResult_urban_OpenSpeedLimit_Value;
  bool expectedResult_rural_OpenSpeedLimit_Applicable;
  units::velocity::meters_per_second_t expectedResult_rural_OpenSpeedLimit_Value;
  bool expectedResult_motorway_openSpeedLimit_Applicable;
  units::velocity::meters_per_second_t expectedResult_motorway_openSpeedLimit_Value;
  bool expectedResult_motorway_keepToOuterLanes_Applicable;
  bool expectedResult_motorway_keepToOuterLanes_Value;
  bool expectedResult_motorway_DontOvertakeOnOuterLanes_Applicable;
  bool expectedResult_motorway_DontOvertakeOnOuterLanes_Value;
  bool expectedResult_motorway_formRescueLane_Applicable;
  bool expectedResult_motorway_formRescueLane_Value;
  bool expectedResult_motorway_ZipperMerge_Applicable;
  bool expectedResult_motorway_ZipperMerge_Value;
};

class TrafficRules_GenerateTrafficRules : public ::testing::Test,
                                          public ::testing::WithParamInterface<DataFor_GenerateTrafficRules>
{
};

TEST_P(TrafficRules_GenerateTrafficRules, Check_GenerateTrafficRules)
{
  DataFor_GenerateTrafficRules data = GetParam();

  GenerateTrafficRules trafficRules;

  auto result = trafficRules.Generate(data.country);

  ASSERT_EQ(result.common.handsFreePhoneMandatory.applicable, data.expectedResult_common_handsFreePhoneMandatoryApplicable);
  ASSERT_EQ(result.common.handsFreePhoneMandatory.value, data.expectedResult_common_handsFreePhoneMandatoryValue);
  ASSERT_EQ(result.common.rightHandTraffic.applicable, data.expectedResult_common_rightHandTrafficApplicable);
  ASSERT_EQ(result.common.rightHandTraffic.value, data.expectedResult_common_rightHandTrafficValue);
  ASSERT_EQ(result.urban.openSpeedLimit.applicable, data.expectedResult_urban_OpenSpeedLimit_Applicable);
  ASSERT_EQ(result.urban.openSpeedLimit.value, data.expectedResult_urban_OpenSpeedLimit_Value);
  ASSERT_EQ(result.rural.openSpeedLimit.applicable, data.expectedResult_rural_OpenSpeedLimit_Applicable);
  ASSERT_EQ(result.rural.openSpeedLimit.value, data.expectedResult_rural_OpenSpeedLimit_Value);
  ASSERT_EQ(result.motorway.openSpeedLimit.applicable, data.expectedResult_motorway_openSpeedLimit_Applicable);

  if (units::math::isinf(result.motorway.openSpeedLimit.value))
  {
    ASSERT_TRUE(units::math::isinf(result.motorway.openSpeedLimit.value));
  }
  else
  {
    ASSERT_EQ(result.motorway.openSpeedLimit.value, data.expectedResult_motorway_openSpeedLimit_Value);
  }

  ASSERT_EQ(result.motorway.keepToOuterLanes.applicable, data.expectedResult_motorway_keepToOuterLanes_Applicable);
  ASSERT_EQ(result.motorway.keepToOuterLanes.value, data.expectedResult_motorway_keepToOuterLanes_Value);
  ASSERT_EQ(result.motorway.DontOvertakeOnOuterLanes.applicable, data.expectedResult_motorway_DontOvertakeOnOuterLanes_Applicable);
  ASSERT_EQ(result.motorway.DontOvertakeOnOuterLanes.value, data.expectedResult_motorway_DontOvertakeOnOuterLanes_Value);
  ASSERT_EQ(result.motorway.formRescueLane.applicable, data.expectedResult_motorway_formRescueLane_Applicable);
  ASSERT_EQ(result.motorway.formRescueLane.value, data.expectedResult_motorway_formRescueLane_Value);
  ASSERT_EQ(result.motorway.ZipperMerge.applicable, data.expectedResult_motorway_ZipperMerge_Applicable);
  ASSERT_EQ(result.motorway.ZipperMerge.value, data.expectedResult_motorway_ZipperMerge_Value);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    TrafficRules_GenerateTrafficRules,
    testing::Values(
        DataFor_GenerateTrafficRules{
            .country = "DE",
            .expectedResult_common_handsFreePhoneMandatoryApplicable = true,
            .expectedResult_common_handsFreePhoneMandatoryValue = true,
            .expectedResult_common_rightHandTrafficApplicable = true,
            .expectedResult_common_rightHandTrafficValue = true,
            .expectedResult_urban_OpenSpeedLimit_Applicable = true,
            .expectedResult_urban_OpenSpeedLimit_Value = 50_kph,
            .expectedResult_rural_OpenSpeedLimit_Applicable = true,
            .expectedResult_rural_OpenSpeedLimit_Value = 100_kph,
            .expectedResult_motorway_openSpeedLimit_Applicable = true,
            .expectedResult_motorway_openSpeedLimit_Value = ScmDefinitions::INF_VELOCITY,
            .expectedResult_motorway_keepToOuterLanes_Applicable = true,
            .expectedResult_motorway_keepToOuterLanes_Value = true,
            .expectedResult_motorway_DontOvertakeOnOuterLanes_Applicable = true,
            .expectedResult_motorway_DontOvertakeOnOuterLanes_Value = true,
            .expectedResult_motorway_formRescueLane_Applicable = true,
            .expectedResult_motorway_formRescueLane_Value = true,
            .expectedResult_motorway_ZipperMerge_Applicable = true,
            .expectedResult_motorway_ZipperMerge_Value = true},
        DataFor_GenerateTrafficRules{
            .country = "US",
            .expectedResult_common_handsFreePhoneMandatoryApplicable = true,
            .expectedResult_common_handsFreePhoneMandatoryValue = false,
            .expectedResult_common_rightHandTrafficApplicable = true,
            .expectedResult_common_rightHandTrafficValue = true,
            .expectedResult_urban_OpenSpeedLimit_Applicable = true,
            .expectedResult_urban_OpenSpeedLimit_Value = 40_kph,
            .expectedResult_rural_OpenSpeedLimit_Applicable = true,
            .expectedResult_rural_OpenSpeedLimit_Value = 100_kph,
            .expectedResult_motorway_openSpeedLimit_Applicable = true,
            .expectedResult_motorway_openSpeedLimit_Value = 120_kph,
            .expectedResult_motorway_keepToOuterLanes_Applicable = false,
            .expectedResult_motorway_keepToOuterLanes_Value = false,
            .expectedResult_motorway_DontOvertakeOnOuterLanes_Applicable = true,
            .expectedResult_motorway_DontOvertakeOnOuterLanes_Value = false,
            .expectedResult_motorway_formRescueLane_Applicable = true,
            .expectedResult_motorway_formRescueLane_Value = true,
            .expectedResult_motorway_ZipperMerge_Applicable = true,
            .expectedResult_motorway_ZipperMerge_Value = false},
        DataFor_GenerateTrafficRules{
            .country = "AT",
            .expectedResult_common_handsFreePhoneMandatoryApplicable = true,
            .expectedResult_common_handsFreePhoneMandatoryValue = true,
            .expectedResult_common_rightHandTrafficApplicable = true,
            .expectedResult_common_rightHandTrafficValue = true,
            .expectedResult_urban_OpenSpeedLimit_Applicable = true,
            .expectedResult_urban_OpenSpeedLimit_Value = 50_kph,
            .expectedResult_rural_OpenSpeedLimit_Applicable = true,
            .expectedResult_rural_OpenSpeedLimit_Value = 100_kph,
            .expectedResult_motorway_openSpeedLimit_Applicable = true,
            .expectedResult_motorway_openSpeedLimit_Value = 130_kph,
            .expectedResult_motorway_keepToOuterLanes_Applicable = true,
            .expectedResult_motorway_keepToOuterLanes_Value = true,
            .expectedResult_motorway_DontOvertakeOnOuterLanes_Applicable = true,
            .expectedResult_motorway_DontOvertakeOnOuterLanes_Value = true,
            .expectedResult_motorway_formRescueLane_Applicable = true,
            .expectedResult_motorway_formRescueLane_Value = true,
            .expectedResult_motorway_ZipperMerge_Applicable = true,
            .expectedResult_motorway_ZipperMerge_Value = true},
        DataFor_GenerateTrafficRules{
            .country = "CN",
            .expectedResult_common_handsFreePhoneMandatoryApplicable = true,
            .expectedResult_common_handsFreePhoneMandatoryValue = false,
            .expectedResult_common_rightHandTrafficApplicable = true,
            .expectedResult_common_rightHandTrafficValue = true,
            .expectedResult_urban_OpenSpeedLimit_Applicable = true,
            .expectedResult_urban_OpenSpeedLimit_Value = 50_kph,
            .expectedResult_rural_OpenSpeedLimit_Applicable = true,
            .expectedResult_rural_OpenSpeedLimit_Value = 100_kph,
            .expectedResult_motorway_openSpeedLimit_Applicable = true,
            .expectedResult_motorway_openSpeedLimit_Value = 130_kph,
            .expectedResult_motorway_keepToOuterLanes_Applicable = false,
            .expectedResult_motorway_keepToOuterLanes_Value = false,
            .expectedResult_motorway_DontOvertakeOnOuterLanes_Applicable = true,
            .expectedResult_motorway_DontOvertakeOnOuterLanes_Value = true,
            .expectedResult_motorway_formRescueLane_Applicable = true,
            .expectedResult_motorway_formRescueLane_Value = false,
            .expectedResult_motorway_ZipperMerge_Applicable = true,
            .expectedResult_motorway_ZipperMerge_Value = true}));