/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "Fakes/FakeMicroscopicCharacteristics.h"
#include "TestHighCognitive.h"

using ::testing::NiceMock;

/*************************************
 * CHECK AdvanceHighCognitive *
 *************************************/

/// \brief Data table
struct DataFor_AdvanceHighCognitive
{
  bool input_isHighCognitiveActive;
  bool input_externalControlActive;
  bool input_isRightFrontAndRightFrontFarAvailable;
  int input_time;
  int input_advanceTime;
  int input_currentSituationPattern;
  HighCognitiveSituation expected_currentHighCognitiveSituation;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_AdvanceHighCognitive& obj)
  {
    return os
           << "input_isHighCognitiveActive (bool): " << ScmCommons::BooleanToString(obj.input_isHighCognitiveActive)
           << " | input_externalControlActive (bool): " << ScmCommons::BooleanToString(obj.input_externalControlActive)
           << " | input_isRightFrontAndRightFrontFarAvailable (bool): " << ScmCommons::BooleanToString(obj.input_isRightFrontAndRightFrontFarAvailable)
           << " | input_time (int): " << obj.input_time
           << " | input_advanceTime (int): " << obj.input_advanceTime
           << " | input_currentSituationPattern (int): " << obj.input_currentSituationPattern
           << " | expected_currentHighCognitiveSituation (HighCognitiveSituation): " << ScmCommons::HighCognitiveSituationToString(obj.expected_currentHighCognitiveSituation);
  }
};

class HighCognitive_AdvanceHighCognitive : public ::testing::Test,
                                           public ::testing::WithParamInterface<DataFor_AdvanceHighCognitive>
{
};

TEST_P(HighCognitive_AdvanceHighCognitive, HighCognitive_CheckFunction_AdvanceHighCognitive)
{
  // Get test data
  DataFor_AdvanceHighCognitive data = GetParam();

  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;

  MicroscopicCharacteristics microscopicCharacteristics;
  ON_CALL(fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&microscopicCharacteristics));

  auto stubHighCognitive = std::make_unique<TestHighCognitive>(&microscopicCharacteristics, fakeMentalCalculations, fakeMentalModel, true);

  // Set up test
  ON_CALL(*stubHighCognitive, RightFrontAndRightFrontFarAvailable()).WillByDefault(Return(data.input_isRightFrontAndRightFrontFarAvailable));
  ON_CALL(*stubHighCognitive, DetermineCurrentSituationPattern()).WillByDefault(Return(data.input_currentSituationPattern));

  // Call test
  stubHighCognitive->TestAdvanceHighCognitive(data.input_time, data.input_externalControlActive);

  HighCognitiveSituation result_currentHighCognitiveSituation = stubHighCognitive->GetCurrentSituation(data.input_time);

  // Evaluate results
  ASSERT_EQ(data.expected_currentHighCognitiveSituation, result_currentHighCognitiveSituation);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_CASE_P(
    Default,
    HighCognitive_AdvanceHighCognitive,
    testing::Values(
        // External control just activated
        DataFor_AdvanceHighCognitive{
            .input_isHighCognitiveActive = true,
            .input_externalControlActive = true,
            .input_isRightFrontAndRightFrontFarAvailable = false,
            .input_time = 100,
            .input_advanceTime = 0,
            .input_currentSituationPattern = -1,
            .expected_currentHighCognitiveSituation = HighCognitiveSituation::UNDEFINED},
        // High cognitive already inactive
        DataFor_AdvanceHighCognitive{
            .input_isHighCognitiveActive = false,
            .input_externalControlActive = true,
            .input_isRightFrontAndRightFrontFarAvailable = false,
            .input_time = 100,
            .input_advanceTime = 0,
            .input_currentSituationPattern = -1,
            .expected_currentHighCognitiveSituation = HighCognitiveSituation::UNDEFINED},
        // External control just deactivated
        DataFor_AdvanceHighCognitive{
            .input_isHighCognitiveActive = false,
            .input_externalControlActive = false,
            .input_isRightFrontAndRightFrontFarAvailable = false,
            .input_time = 100,
            .input_advanceTime = 0,
            .input_currentSituationPattern = -1,
            .expected_currentHighCognitiveSituation = HighCognitiveSituation::UNDEFINED},
        DataFor_AdvanceHighCognitive{
            .input_isHighCognitiveActive = false,
            .input_externalControlActive = false,
            .input_isRightFrontAndRightFrontFarAvailable = true,
            .input_time = 7100,
            .input_advanceTime = 0,
            .input_currentSituationPattern = 1,
            .expected_currentHighCognitiveSituation = HighCognitiveSituation::SALIENT_APPROACH},
        // Remaining state "isHighCognitiveActive && !externalControlActive" is just the normal case
        DataFor_AdvanceHighCognitive{
            .input_isHighCognitiveActive = true,
            .input_externalControlActive = false,
            .input_isRightFrontAndRightFrontFarAvailable = false,
            .input_time = 7100,
            .input_advanceTime = 0,
            .input_currentSituationPattern = -1,
            .expected_currentHighCognitiveSituation = HighCognitiveSituation::UNDEFINED},
        DataFor_AdvanceHighCognitive{
            .input_isHighCognitiveActive = true,
            .input_externalControlActive = false,
            .input_isRightFrontAndRightFrontFarAvailable = true,
            .input_time = 200,
            .input_advanceTime = 0,
            .input_currentSituationPattern = 0,
            .expected_currentHighCognitiveSituation = HighCognitiveSituation::APPROACH},
        DataFor_AdvanceHighCognitive{
            .input_isHighCognitiveActive = true,
            .input_externalControlActive = false,
            .input_isRightFrontAndRightFrontFarAvailable = true,
            .input_time = 300,
            .input_advanceTime = 0,
            .input_currentSituationPattern = 1,
            .expected_currentHighCognitiveSituation = HighCognitiveSituation::SALIENT_APPROACH},
        DataFor_AdvanceHighCognitive{
            .input_isHighCognitiveActive = true,
            .input_externalControlActive = false,
            .input_isRightFrontAndRightFrontFarAvailable = true,
            .input_time = 400,
            .input_advanceTime = 0,
            .input_currentSituationPattern = 2,
            .expected_currentHighCognitiveSituation = HighCognitiveSituation::FOLLOW},
        DataFor_AdvanceHighCognitive{
            .input_isHighCognitiveActive = true,
            .input_externalControlActive = false,
            .input_isRightFrontAndRightFrontFarAvailable = true,
            .input_time = 500,
            .input_advanceTime = 0,
            .input_currentSituationPattern = 3,
            .expected_currentHighCognitiveSituation = HighCognitiveSituation::SALIENT_FOLLOW},
        DataFor_AdvanceHighCognitive{
            .input_isHighCognitiveActive = true,
            .input_externalControlActive = false,
            .input_isRightFrontAndRightFrontFarAvailable = true,
            .input_time = 7100,
            .input_advanceTime = 0,
            .input_currentSituationPattern = -1,
            .expected_currentHighCognitiveSituation = HighCognitiveSituation::UNDEFINED}));

/************************************************
 * CHECK GetTtcInverseBetweenTwoAreasOfInterest *
 ************************************************/
struct DataFor_GetTtcInverseBetweenTwoAreasOfInterest
{
  AreaOfInterest aoi_first;
  AreaOfInterest aoi_second;
  units::length::meter_t relativeNetDistanceAoi_first;
  units::length::meter_t relativeNetDistanceAoi_second;
  bool isVehicleVisible;
  double expectedResult;
};
class AnticipatedLaneChangerModel_GetTtcInverseBetweenTwoAreasOfInterest : public ::testing::Test,
                                                                           public ::testing::WithParamInterface<DataFor_GetTtcInverseBetweenTwoAreasOfInterest>
{
};
TEST_P(AnticipatedLaneChangerModel_GetTtcInverseBetweenTwoAreasOfInterest, Check_GetTtcInverseBetweenTwoAreasOfInterest)
{
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  DataFor_GetTtcInverseBetweenTwoAreasOfInterest data = GetParam();
  auto anticipatedLaneChangerModel = std::make_unique<AnticipatedLaneChangerModel>(&fakeMicroscopicCharacteristics, fakeMentalCalculations, fakeMentalModel);
  ON_CALL(fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(data.isVehicleVisible));
  ON_CALL(fakeMentalModel, GetRelativeNetDistance(data.aoi_first)).WillByDefault(Return(data.relativeNetDistanceAoi_first));
  ON_CALL(fakeMentalModel, GetRelativeNetDistance(data.aoi_second)).WillByDefault(Return(data.relativeNetDistanceAoi_second));
  ON_CALL(fakeMentalModel, GetVehicleLength(_, _)).WillByDefault(Return(5.0_m));
  ON_CALL(fakeMentalModel, GetLongitudinalVelocity(data.aoi_first, _)).WillByDefault(Return(20.0_mps));
  ON_CALL(fakeMentalModel, GetLongitudinalVelocity(data.aoi_second, _)).WillByDefault(Return(30.0_mps));
  auto result = anticipatedLaneChangerModel->GetTtcInverseBetweenTwoAreasOfInterest(data.aoi_first, data.aoi_second);
  if (data.expectedResult == INFINITY)
  {
    ASSERT_EQ(result, data.expectedResult);
  }
  else
  {
    EXPECT_NEAR(result, data.expectedResult, 0.01);
  }
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    AnticipatedLaneChangerModel_GetTtcInverseBetweenTwoAreasOfInterest,
    testing::Values(
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_FRONT,
            .aoi_second = AreaOfInterest::EGO_FRONT_FAR,
            .relativeNetDistanceAoi_first = 50.0_m,
            .relativeNetDistanceAoi_second = 100.0_m,
            .isVehicleVisible = false,
            .expectedResult = 0.01},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_FRONT,
            .aoi_second = AreaOfInterest::EGO_FRONT_FAR,
            .relativeNetDistanceAoi_first = 50.0_m,
            .relativeNetDistanceAoi_second = 100.0_m,
            .isVehicleVisible = true,
            .expectedResult = -0.2},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_FRONT,
            .aoi_second = AreaOfInterest::EGO_FRONT_FAR,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 50.0_m,
            .isVehicleVisible = true,
            .expectedResult = 0.2},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_FRONT,
            .aoi_second = AreaOfInterest::EGO_FRONT_FAR,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = INFINITY},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::LEFT_SIDE,
            .aoi_second = AreaOfInterest::EGO_FRONT,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = -0.1},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_FRONT,
            .aoi_second = AreaOfInterest::LEFT_SIDE,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 0.09},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_REAR,
            .aoi_second = AreaOfInterest::EGO_FRONT,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = -0.1},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_FRONT,
            .aoi_second = AreaOfInterest::EGO_REAR,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 0.09},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::LEFT_SIDE,
            .aoi_second = AreaOfInterest::EGO_REAR,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 0.09},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_REAR,
            .aoi_second = AreaOfInterest::LEFT_SIDE,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = -0.1},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_REAR,
            .aoi_second = AreaOfInterest::LEFT_REAR,
            .relativeNetDistanceAoi_first = 50.0_m,
            .relativeNetDistanceAoi_second = 100.0_m,
            .isVehicleVisible = true,
            .expectedResult = 0.18},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_REAR,
            .aoi_second = AreaOfInterest::LEFT_REAR,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 50.0_m,
            .isVehicleVisible = true,
            .expectedResult = -0.18},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::EGO_REAR,
            .aoi_second = AreaOfInterest::LEFT_REAR,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = INFINITY},
        DataFor_GetTtcInverseBetweenTwoAreasOfInterest{
            .aoi_first = AreaOfInterest::LEFT_SIDE,
            .aoi_second = AreaOfInterest::RIGHT_SIDE,
            .relativeNetDistanceAoi_first = 100.0_m,
            .relativeNetDistanceAoi_second = 105.0_m,
            .isVehicleVisible = true,
            .expectedResult = 0}));