/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/*
 * Unit Tests for Reliability calclation functions
 */

#include <gmock/gmock-actions.h>
#include <gmock/gmock-spec-builders.h>
#include <gtest/gtest-param-test.h>
#include <gtest/gtest.h>

#include <ostream>

#include "ScmDefinitions.h"
#include "module/driver/src/Reliability.h"

/**************************
 * CHECK DecayReliability *
 **************************/

TEST(ReliabilityCalculations_DecayReliability, ReliabilityReachesZeroAfterSevenSecondsWhenInitializedFromHundred)
{
  double reliability = 100.0;
  const auto CYCLE_TIME{100_ms};
  for (int i = 0; i < 7000; i += 100)
  {
    reliability = ReliabilityCalculations::DecayReliability(reliability, ParameterChangeRate::FAST, CYCLE_TIME);
  }

  ASSERT_DOUBLE_EQ(0.0, reliability);
}

TEST(ReliabilityCalculations_DecayReliability, ReliabilityDoesNotGoBelowZero)
{
  double reliability = 0.0;
  const auto CYCLE_TIME{100_ms};
  reliability = ReliabilityCalculations::DecayReliability(reliability, ParameterChangeRate::FAST, CYCLE_TIME);

  ASSERT_DOUBLE_EQ(0.0, reliability);
}

/******************************
 * CHECK CalculateReliability *
 ******************************/

struct DataFor_CalculateReliability
{
  units::angle::radian_t angleToOpticalAxis;
  double expectedReliability;
};

class ReliabilityCalculations_TestCalculateReliability : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CalculateReliability>
{
};

TEST_P(ReliabilityCalculations_TestCalculateReliability, Calculate_Reliability)
{
  const auto data{GetParam()};
  const double reliability = ReliabilityCalculations::CalculateReliability(data.angleToOpticalAxis);

  ASSERT_DOUBLE_EQ(data.expectedReliability, reliability);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ReliabilityCalculations_TestCalculateReliability,
    testing::Values(
        DataFor_CalculateReliability{
            .angleToOpticalAxis = 0_deg,
            .expectedReliability = 100.0},
        DataFor_CalculateReliability{
            .angleToOpticalAxis = 30_deg,
            .expectedReliability = DataQuality::MEDIUM},
        DataFor_CalculateReliability{
            .angleToOpticalAxis = 15_deg,
            .expectedReliability = DataQuality::MEDIUM + (100.0 - DataQuality::MEDIUM) / 2.0},
        DataFor_CalculateReliability{
            .angleToOpticalAxis = 40_deg,
            .expectedReliability = DataQuality::MEDIUM}));

/********************************
 * CHECK CalculateSectorUpdates *
 ********************************/

struct DataFor_CalculateSectorUpdates
{
  units::angle::radian_t gazeAngle;
  std::map<VisualPerceptionSector, double> expectedReliabilityMap;
};

class ReliabilityCalculations_TestCalculateSectorUpdates : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CalculateSectorUpdates>
{
};

TEST_P(ReliabilityCalculations_TestCalculateSectorUpdates, Calculate_Reliability)
{
  const auto data{GetParam()};
  const auto resultMap = VisualReliabilityCalculations::CalculateSectorUpdates(data.gazeAngle);

  ASSERT_EQ(data.expectedReliabilityMap, resultMap);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    ReliabilityCalculations_TestCalculateSectorUpdates,
    testing::Values(
        DataFor_CalculateSectorUpdates{
            .gazeAngle = 0_deg,
            .expectedReliabilityMap = {{VisualPerceptionSector::FRONT, 100.0}}},
        DataFor_CalculateSectorUpdates{
            .gazeAngle = 45_deg,
            .expectedReliabilityMap = {{VisualPerceptionSector::FRONT, 100.0}, {VisualPerceptionSector::LEFT, DataQuality::MEDIUM}}},
        DataFor_CalculateSectorUpdates{
            .gazeAngle = 35_deg,
            .expectedReliabilityMap = {{VisualPerceptionSector::FRONT, 100.0}, {VisualPerceptionSector::LEFT, DataQuality::MEDIUM}}},
        DataFor_CalculateSectorUpdates{
            .gazeAngle = -35_deg,
            .expectedReliabilityMap = {{VisualPerceptionSector::FRONT, 100.0}, {VisualPerceptionSector::RIGHT, DataQuality::MEDIUM}}},
        DataFor_CalculateSectorUpdates{
            .gazeAngle = 50_deg,
            .expectedReliabilityMap = {{VisualPerceptionSector::LEFT, 100.0}, {VisualPerceptionSector::FRONT, DataQuality::MEDIUM}}},
        DataFor_CalculateSectorUpdates{
            .gazeAngle = 90_deg,
            .expectedReliabilityMap = {{VisualPerceptionSector::LEFT, 100.0}}},
        DataFor_CalculateSectorUpdates{
            .gazeAngle = 130_deg,
            .expectedReliabilityMap = {{VisualPerceptionSector::LEFT, 100.0}, {VisualPerceptionSector::REAR, DataQuality::MEDIUM}}},
        DataFor_CalculateSectorUpdates{
            .gazeAngle = -50_deg,
            .expectedReliabilityMap = {{VisualPerceptionSector::RIGHT, 100.0}, {VisualPerceptionSector::FRONT, DataQuality::MEDIUM}}},
        DataFor_CalculateSectorUpdates{
            .gazeAngle = -90_deg,
            .expectedReliabilityMap = {{VisualPerceptionSector::RIGHT, 100.0}}},
        DataFor_CalculateSectorUpdates{
            .gazeAngle = -130_deg,
            .expectedReliabilityMap = {{VisualPerceptionSector::RIGHT, 100.0}, {VisualPerceptionSector::REAR, DataQuality::MEDIUM}}},
        DataFor_CalculateSectorUpdates{
            .gazeAngle = -180_deg,
            .expectedReliabilityMap = {{VisualPerceptionSector::REAR, 100.0}}}));
