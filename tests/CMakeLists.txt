################################################################################
# Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

set(TEST_PATH ${CMAKE_CURRENT_LIST_DIR})

include_directories(
        ${TEST_PATH}
        ${TEST_PATH}/common
        ${TEST_PATH}/fakes/gmock
)

add_subdirectory(ActionImplementation_Tests)
add_subdirectory(ActionManager_Tests)
add_subdirectory(AlgorithmLongitudinalScm_Tests)
add_subdirectory(AoiAssigner_Tests)
add_subdirectory(Extrapolation_Tests)
add_subdirectory(FeatureExtractor_Tests)
add_subdirectory(GazeControl_Tests)
add_subdirectory(HighCognitive_Tests)
add_subdirectory(InformationAcquisition_Tests)
add_subdirectory(InfrastructureCharacteristics_Tests)
add_subdirectory(LaneChangeBehavior_Tests)
add_subdirectory(LaneChangeTrajectoryCalculations_Tests)
add_subdirectory(LaneKeepingCalculations_Tests)
add_subdirectory(LongitudinalCalculations_Tests)
add_subdirectory(MentalCalculations_Tests)
add_subdirectory(MentalModel_Tests)
add_subdirectory(Merging_Tests)
add_subdirectory(ParametersScm_Tests)
add_subdirectory(Reliability_Tests)
add_subdirectory(ScmComponents_Tests)
add_subdirectory(ScmDriver_Tests)
add_subdirectory(ScmImplementation_Tests)
add_subdirectory(ScmMathUtils_Tests)
add_subdirectory(ScmSignalCollector_Tests)

#add_subdirectory(Sensor_Tests)

add_subdirectory(SideLaneSafety_Tests)
add_subdirectory(SituationManager_Tests)
add_subdirectory(SurroundingVehicles_Tests)
add_subdirectory(Swerving_Tests)
add_subdirectory(TrajectoryCalculations_Tests)
add_subdirectory(TrajectoryPlanner_Tests)
add_subdirectory(ScmCommons_Tests)
add_subdirectory(MicroscopicCharacteristics_Tests)
add_subdirectory(AuditoryPerception_Tests)
add_subdirectory(DesiredVelocityCalculations_Tests)
add_subdirectory(LaneQueryHelper_Tests)
add_subdirectory(ScmLifetimeState_Tests)
add_subdirectory(SpawningState_Tests)
add_subdirectory(PeriodicLogger_Tests)
add_subdirectory(ParameterParser_Tests)
add_subdirectory(LateralController_Tests)
add_subdirectory(LongitudinalController_Tests)
add_subdirectory(DynamicsModel_Tests)
add_subdirectory(Driver_Tests)
add_subdirectory(ModuleController_Tests)
