/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmath>
#include <limits>

#include "module/parameterParser/src/DriverCharacteristicsSampler.h"

using ::testing::_;
using ::testing::AllOf;
using ::testing::Each;
using ::testing::ElementsAre;
using ::testing::ElementsAreArray;
using ::testing::Eq;
using ::testing::Ge;
using ::testing::IsNull;
using ::testing::Le;
using ::testing::NanSensitiveDoubleEq;
using ::testing::Ne;
using ::testing::NiceMock;
using ::testing::NotNull;
using ::testing::Return;

// TODO: This is a TestCase for the Stochastics Unit Test
TEST(DriverCharacteristicsSampler_UnitTests, ConditionOnRandomVariables_DimensionReduction_DimensionOrderFromMatlab)
{
  static const CallbackInterface* Callbacks = nullptr;
  StochasticsImplementation* stochastics = new StochasticsImplementation(Callbacks);

  StochasticData stochasticData;
  // dist1 is testing table generated with matlab
  DiscreteDistributionInformation test = stochasticData.dist1;

  const double epsilon = 1e-5;

  // should contain only 1 (actually log(1) because log-probability is stored)
  std::vector<double> conditions{0, 19, NAN};
  struct DiscreteDistributionInformation* returned =
      stochastics->ConditionOnRandomVariables(&test, conditions, 2);
  EXPECT_THAT(returned->table, Each(Eq(std::log(1))));

  // should contain only 1
  std::vector<double> conditions2{0, NAN, 1};
  struct DiscreteDistributionInformation* returned2 =
      stochastics->ConditionOnRandomVariables(&test, conditions2, 1);
  EXPECT_THAT(returned2->table, Each(Eq(std::log(1))));

  // should contain 1 to 5
  std::vector<double> conditions3{NAN, 19, 1};
  struct DiscreteDistributionInformation* returned3 =
      stochastics->ConditionOnRandomVariables(&test, conditions3, 0);
  // Loop because ASSERT_That(returned3->table, ElementsAre(std::log(1),std::log(2),std::log(3),std::log(4),std::log(5))); doesnt check for error
  for (int i = 0; i < 5; ++i)
  {
    EXPECT_NEAR(returned3->table.at(i), std::log(i + 1), epsilon);
  }

  delete returned;
  delete returned2;
  delete returned3;
}

// TODO: This is a TestCase for the Stochastics Unit Test
TEST(DriverCharacteristicsSampler_UnitTests, ConditionOnRandomVariables_ErrorHandling1_ThrowError)
{
  static const CallbackInterface* Callbacks = nullptr;
  StochasticsImplementation* stochastics = new StochasticsImplementation(Callbacks);

  StochasticData stochasticData;
  DiscreteDistributionInformation test = stochasticData.dist1;

  // everything correct with this invoctaion, no error
  std::vector<double> conditions{0.5, 50, NAN};
  struct DiscreteDistributionInformation* returned =
      stochastics->ConditionOnRandomVariables(&test, conditions, 2);
  EXPECT_THAT(returned, NotNull());

  // method call not correct because dimUnconditionedVariable is not the position where NAN is
  // in the condition vector, should return a nullptr
  std::vector<double> conditions1{0.5, NAN, 3};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions1, 2), std::invalid_argument);

  // same as obove, NAN is at wrong position
  std::vector<double> conditions2{NAN, 50, 3};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions2, 2), std::invalid_argument);

  // wrong length of conditions
  std::vector<double> conditions3{NAN, 50};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions3, 2), std::invalid_argument);

  // no NAN in conditions
  std::vector<double> conditions4{0.5, 50, 3};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions4, 2), std::invalid_argument);

  // conditions is empty
  std::vector<double> conditions5{};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions5, 2), std::invalid_argument);

  // conditions is empty
  std::vector<double> conditions6;
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions6, 2), std::invalid_argument);

  // everything correct with this invoctaion, no error
  std::vector<double> conditions9{0.5, 40, NAN};
  returned = stochastics->ConditionOnRandomVariables(&test, conditions9, 2);
  EXPECT_THAT(returned, NotNull());

  delete returned;
}

// TODO: This is a TestCase for the Stochastics Unit Test
TEST(DriverCharacteristicsSampler_UnitTests, ConditionOnRandomVariables_WrongConditionsVector_ThrowError)
{
  static const CallbackInterface* Callbacks = nullptr;
  StochasticsImplementation* stochastics = new StochasticsImplementation(Callbacks);

  StochasticData stochasticData;
  DiscreteDistributionInformation test = stochasticData.dist1;

  // everything correct with this invoctaion, no error
  std::vector<double> conditions{0.5, 50, NAN};
  struct DiscreteDistributionInformation* returned =
      stochastics->ConditionOnRandomVariables(&test, conditions, 2);
  EXPECT_THAT(returned, NotNull());

  std::vector<double> conditions1{NAN, NAN, NAN};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions1, 2), std::invalid_argument);

  std::vector<double> conditions2{0.5, NAN, NAN};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions2, 2), std::invalid_argument);

  std::vector<double> conditions3{0.5, 1000, NAN};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions3, 2), std::invalid_argument);

  std::vector<double> conditions4{-0.5, 40, NAN};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions4, 2), std::invalid_argument);

  std::vector<double> conditions5{-0.5, 1000, NAN};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions5, 2), std::invalid_argument);

  std::vector<double> conditions6{0.5, 40, NAN};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions6, -1), std::invalid_argument);

  std::vector<double> conditions7{0.5, 40, NAN};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&test, conditions7, 42), std::invalid_argument);

  delete returned;
}

// TODO: This is a TestCase for the Stochastics Unit Test
TEST(DriverCharacteristicsSampler_UnitTests, StochasticsImplementation_EmptyVectors_ThrowError)
{
  static const CallbackInterface* Callbacks = nullptr;
  StochasticsImplementation* stochastics = new StochasticsImplementation(Callbacks);

  const std::vector<double> arr = {-18.131861, -7.664595, -11.445331, -10.410105, -13.935195, -7.681026, -8.282271, -7.504068, -7.760330, -10.311710, -8.100457, -7.952119, -7.533214, -7.641094, -9.802423, -7.795565, -9.606355, -11.285648, -10.114498, -10.054768, -13.759657, -8.586949, -9.155485, -12.037710, -18.326940};
  const std::vector<double> arr2 = {-18.131861, -7.664595, -11.445331, -10.410105, -13.935195, -7.681026, -8.282271};
  const std::vector<double> arrE = {};
  const std::vector<double> min = {19.000000, 40.000000};
  const std::vector<double> min2 = {40.000000};
  const std::vector<double> minE = {};
  const std::vector<double> max = {75.000000, 90.000000};
  const std::vector<double> max2 = {90.000000};
  const std::vector<double> maxE = {};
  const std::vector<std::string> dimNames = {"Alter", "STAI T PER"};
  const std::vector<std::string> dimNames2 = {"STAI T PER"};
  const std::vector<std::string> dimNamesE = {};
  const std::vector<int> dimIDs = {1, 2};
  const std::vector<int> dimIDs2 = {2};
  const std::vector<int> dimIDsE = {};
  const struct DiscreteDistributionInformation dist1 = {arr, min, max, 2, 5, dimNames, dimIDs};
  const struct DiscreteDistributionInformation dist2 = {arrE, min, max, 2, 5, dimNames, dimIDs};
  const struct DiscreteDistributionInformation dist3 = {arr, minE, max, 2, 5, dimNames, dimIDs};
  const struct DiscreteDistributionInformation dist4 = {arr, min, maxE, 2, 5, dimNames, dimIDs};
  const struct DiscreteDistributionInformation dist5 = {arr, min, max, 2, 5, dimNamesE, dimIDs};
  const struct DiscreteDistributionInformation dist6 = {arr, min, max, 2, 5, dimNames, dimIDsE};
  const struct DiscreteDistributionInformation dist7 = {arrE, minE, maxE, 2, 5, dimNamesE, dimIDsE};

  const struct DiscreteDistributionInformation dist1_2 = {arr2, min2, max2, 1, 7, dimNames2, dimIDs2};
  const struct DiscreteDistributionInformation dist2_2 = {arrE, min2, max2, 1, 7, dimNames2, dimIDs2};
  const struct DiscreteDistributionInformation dist3_2 = {arr2, minE, max2, 1, 7, dimNames2, dimIDs2};
  const struct DiscreteDistributionInformation dist4_2 = {arr2, min2, maxE, 1, 7, dimNames2, dimIDs2};
  const struct DiscreteDistributionInformation dist5_2 = {arr2, min2, max2, 1, 7, dimNamesE, dimIDs2};
  const struct DiscreteDistributionInformation dist6_2 = {arr2, min2, max2, 1, 7, dimNames2, dimIDsE};
  const struct DiscreteDistributionInformation dist7_2 = {arrE, minE, maxE, 1, 7, dimNamesE, dimIDsE};

  // ConditionOnRandomVariables
  std::vector<double> conditions{50, NAN};
  struct DiscreteDistributionInformation* returned;
  returned = stochastics->ConditionOnRandomVariables(&dist1, conditions, 1);
  EXPECT_THAT(returned, NotNull());
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist2, conditions, 1), std::invalid_argument);
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist3, conditions, 1), std::invalid_argument);
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist4, conditions, 1), std::invalid_argument);
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist5, conditions, 1), std::invalid_argument);
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist6, conditions, 1), std::invalid_argument);
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist7, conditions, 1), std::invalid_argument);

  delete returned;

  // ConditionOnRandomVariables
  EXPECT_THAT(stochastics->SampleFromDiscreteDistribution(&dist1_2), AllOf(Ge(40.0), Le(90.0)));
  EXPECT_THROW(stochastics->SampleFromDiscreteDistribution(&dist2_2), std::invalid_argument);
  EXPECT_THROW(stochastics->SampleFromDiscreteDistribution(&dist3_2), std::invalid_argument);
  EXPECT_THROW(stochastics->SampleFromDiscreteDistribution(&dist4_2), std::invalid_argument);
  EXPECT_THROW(stochastics->SampleFromDiscreteDistribution(&dist5_2), std::invalid_argument);
  EXPECT_THROW(stochastics->SampleFromDiscreteDistribution(&dist6_2), std::invalid_argument);
  EXPECT_THROW(stochastics->SampleFromDiscreteDistribution(&dist7_2), std::invalid_argument);
}

// TODO: This is a TestCase for the Stochastics Unit Test
TEST(DriverCharacteristicsSampler_UnitTests, ConditionOnRandomVariables_ErrorInHeaderFile_ThrowError)
{
  static const CallbackInterface* Callbacks = nullptr;
  StochasticsImplementation* stochastics = new StochasticsImplementation(Callbacks);

  // create wrong DiscreteDistributionInformation with wrong number of elements
  const std::vector<double> arr2 = {-4.959053, -4.321892, -4.062370, -4.064392, -6.441206};
  const std::vector<double> min2 = {19.000000};
  const std::vector<double> max2 = {75.000000};
  const std::vector<std::string> dimNames2 = {"Alter"};
  const std::vector<int> dimIDs2 = {1};
  const struct DiscreteDistributionInformation dist1 = {arr2, min2, max2, 2, 5, dimNames2, dimIDs2};
  const struct DiscreteDistributionInformation dist2 = {arr2, min2, max2, 1, 6, dimNames2, dimIDs2};
  const struct DiscreteDistributionInformation dist3 = {arr2, min2, max2, 1, 5, dimNames2, dimIDs2};

  const std::vector<double> arr3 = {-18.131861, -7.664595, -11.445331, -10.410105, -13.935195, -7.681026, -8.282271, -7.504068, -7.760330, -10.311710, -8.100457, -7.952119, -7.533214, -7.641094, -9.802423, -7.795565, -9.606355, -11.285648, -10.114498, -10.054768, -13.759657, -8.586949, -9.155485, -12.037710, -18.326940};
  const std::vector<double> arr4 = {-18.131861, -7.664595, -11.445331, -10.410105, -13.935195, -7.681026, -8.282271, -7.504068, -7.760330, -10.311710, -8.100457, -7.952119, -7.533214, -7.641094, -9.802423, -7.795565, -9.606355, -11.285648, -10.114498, -10.054768, -13.759657, -8.586949, -9.155485, -12.037710};
  const std::vector<double> min3 = {19.000000, 40.000000};
  const std::vector<double> max3 = {75.000000, 90.000000};
  const std::vector<std::string> dimNames3 = {"Alter", "STAI T PER"};
  const std::vector<int> dimIDs3 = {1, 2};
  const struct DiscreteDistributionInformation dist4 = {arr3, min3, max3, 3, 5, dimNames3, dimIDs3};
  const struct DiscreteDistributionInformation dist5 = {arr3, min3, max3, 2, 4, dimNames3, dimIDs3};
  const struct DiscreteDistributionInformation dist6 = {arr3, min3, max3, 2, 5, dimNames3, dimIDs3};
  const struct DiscreteDistributionInformation dist7 = {arr4, min3, max3, 2, 5, dimNames3, dimIDs3};

  // too many dimesnions in DiscreteDistributionInformation Object
  std::vector<double> conditions1{NAN};
  struct DiscreteDistributionInformation* returned;
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist1, conditions1, 0), std::invalid_argument);

  // too many sample points along the dimension
  std::vector<double> conditions2{NAN};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist2, conditions2, 0), std::invalid_argument);

  // Everything should be OK with that
  // This is a special corner case with dimension 1 where actually nothing should change.
  std::vector<double> conditions3{NAN};
  returned = stochastics->ConditionOnRandomVariables(&dist3, conditions3, 0);
  EXPECT_THAT(returned, NotNull());
  EXPECT_THAT(returned->table, ElementsAreArray(arr2));
  EXPECT_THAT(returned->min, ElementsAreArray(min2));
  EXPECT_THAT(returned->max, ElementsAreArray(max2));
  EXPECT_THAT(returned->dims, 1);
  EXPECT_THAT(returned->dims, Ne(2));
  EXPECT_THAT(returned->dimSize, 5);
  EXPECT_THAT(returned->dimNames, ElementsAreArray(dimNames2));
  EXPECT_THAT(returned->dimIDs, ElementsAreArray(dimIDs2));

  // too many dimesnions in DiscreteDistributionInformation Object
  std::vector<double> conditions4{50, NAN};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist4, conditions4, 1), std::invalid_argument);

  // too few sample points along dimension
  std::vector<double> conditions5{50, NAN};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist5, conditions5, 1), std::invalid_argument);

  // Everything should be OK with that
  std::vector<double> conditions6{50, NAN};
  returned = stochastics->ConditionOnRandomVariables(&dist6, conditions6, 1);
  EXPECT_THAT(returned, NotNull());

  // Everything should be OK with that
  std::vector<double> conditions7{50, NAN};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist7, conditions7, 1), std::invalid_argument);

  delete returned;
}

TEST(DriverCharacteristicsSampler_UnitTests, ConditionOnRandomVariables_DiscreteDistributionInformationVectorLength_ReturnNullptr)
{
  static const CallbackInterface* Callbacks = nullptr;
  StochasticsImplementation* stochastics = new StochasticsImplementation(Callbacks);

  // TODO: this has to be implemented as error handling (dimUnconditionedVariable range check)
  const std::vector<double> arr = {-18.131861, -7.664595, -11.445331, -10.410105, -13.935195, -7.681026, -8.282271, -7.504068, -7.760330, -10.311710, -8.100457, -7.952119, -7.533214, -7.641094, -9.802423, -7.795565, -9.606355, -11.285648, -10.114498, -10.054768, -13.759657, -8.586949, -9.155485, -12.037710, -18.326940};

  const std::vector<double> minWrong = {40.000000};
  const std::vector<double> maxWrong = {75.000000, 90.000000, 42.0};
  const std::vector<std::string> dimNamesWrong1 = {"STAI T PER"};
  const std::vector<std::string> dimNamesWrong2 = {"Alter", "STAI T PER", "FOO"};
  const std::vector<int> dimIDsWrong1 = {2};
  const std::vector<int> dimIDsWrong2 = {1, 2, -1};
  std::vector<double> conditions{50, NAN};

  const struct DiscreteDistributionInformation dist0 = {arr, minWrong, maxWrong, 2, 5, dimNamesWrong1, dimIDsWrong1};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist0, conditions, 1), std::invalid_argument);
  const struct DiscreteDistributionInformation dist1 = {arr, maxWrong, maxWrong, 2, 5, dimNamesWrong1, dimIDsWrong1};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist1, conditions, 1), std::invalid_argument);
  const struct DiscreteDistributionInformation dist2 = {arr, maxWrong, minWrong, 2, 5, dimNamesWrong1, dimIDsWrong1};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist2, conditions, 1), std::invalid_argument);
  const struct DiscreteDistributionInformation dist3 = {arr, minWrong, minWrong, 2, 5, dimNamesWrong1, dimIDsWrong1};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist3, conditions, 1), std::invalid_argument);

  const struct DiscreteDistributionInformation dist4 = {arr, minWrong, maxWrong, 2, 5, dimNamesWrong1, dimIDsWrong1};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist4, conditions, 1), std::invalid_argument);
  const struct DiscreteDistributionInformation dist5 = {arr, maxWrong, maxWrong, 2, 5, dimNamesWrong2, dimIDsWrong2};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist5, conditions, 1), std::invalid_argument);
  const struct DiscreteDistributionInformation dist6 = {arr, maxWrong, minWrong, 2, 5, dimNamesWrong1, dimIDsWrong2};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist6, conditions, 1), std::invalid_argument);
  const struct DiscreteDistributionInformation dist7 = {arr, minWrong, minWrong, 2, 5, dimNamesWrong2, dimIDsWrong1};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist7, conditions, 1), std::invalid_argument);

  const struct DiscreteDistributionInformation dist8 = {arr, minWrong, maxWrong, 2, 5, dimNamesWrong2, dimIDsWrong2};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist8, conditions, 1), std::invalid_argument);
  const struct DiscreteDistributionInformation dist9 = {arr, maxWrong, maxWrong, 2, 5, dimNamesWrong2, dimIDsWrong2};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist9, conditions, 1), std::invalid_argument);
  const struct DiscreteDistributionInformation dist10 = {arr, maxWrong, minWrong, 2, 5, dimNamesWrong2, dimIDsWrong2};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist10, conditions, 1), std::invalid_argument);
  const struct DiscreteDistributionInformation dist11 = {arr, minWrong, minWrong, 2, 5, dimNamesWrong2, dimIDsWrong2};
  EXPECT_THROW(stochastics->ConditionOnRandomVariables(&dist11, conditions, 1), std::invalid_argument);
}

TEST(DriverCharacteristicsSampler_UnitTests, SampleFromDiscreteDistribution_Sampling_CallExamples)
{
  std::uint32_t seed = 1;

  // first 5 results
  std::vector<double> sampels = {99.718480823026567,
                                 93.255736136816552,
                                 12.812444777230599,
                                 99.904051546527370,
                                 23.608897629816923};

  static const CallbackInterface* Callbacks = nullptr;
  StochasticsImplementation* stochastics = new StochasticsImplementation(Callbacks);
  stochastics->InitGenerator(seed);

  // gleiche wagrscheinlichkeit und range zwischen 0 und 100
  const std::vector<double> arr0 = {-0.523485, -0.523485, -0.523485, -0.523485, -0.523485};
  const std::vector<double> min0 = {0.000000};
  const std::vector<double> max0 = {100.0000};
  const std::vector<std::string> dimNames0 = {"BSSS ThrillSeek"};
  const std::vector<int> dimIDs0 = {4};
  const struct DiscreteDistributionInformation dist0 = {arr0, min0, max0, 1, 5, dimNames0, dimIDs0};

  const double epsilon = 1e-7;

  EXPECT_NEAR(stochastics->SampleFromDiscreteDistribution(&dist0), sampels.at(0), epsilon);
  EXPECT_NEAR(stochastics->SampleFromDiscreteDistribution(&dist0), sampels.at(1), epsilon);
  EXPECT_NEAR(stochastics->SampleFromDiscreteDistribution(&dist0), sampels.at(2), epsilon);
  EXPECT_NEAR(stochastics->SampleFromDiscreteDistribution(&dist0), sampels.at(3), epsilon);
  EXPECT_NEAR(stochastics->SampleFromDiscreteDistribution(&dist0), sampels.at(4), epsilon);
}

TEST(DriverCharacteristicsSampler_UnitTests, SampleFromDiscreteDistribution_Sampling_BoundaryValues)
{
  std::uint32_t seed = 1;
  std::mt19937 baseGeneratorTest;
  baseGeneratorTest.seed(seed);

  static const CallbackInterface* Callbacks = nullptr;
  StochasticsImplementation* stochastics = new StochasticsImplementation(Callbacks);
  stochastics->InitGenerator(seed);

  // ARRANGE

  double negInf = (-1) * std::numeric_limits<double>::infinity();
  double min = std::numeric_limits<double>::min();

  // gleiche wagrscheinlichkeit und range zwischen 0 und 100
  const std::vector<double> arr0 = {-2, -1, 0, 1, 2};
  const std::vector<double> min0 = {19.000000};
  const std::vector<double> max0 = {75.000000};
  const std::vector<std::string> dimNames0 = {"BSSS ThrillSeek"};
  const std::vector<int> dimIDs0 = {4};
  const struct DiscreteDistributionInformation dist0 = {arr0, min0, max0, 1, 5, dimNames0, dimIDs0};

  const std::vector<double> arr1 = {negInf, negInf, negInf, negInf, negInf};
  const std::vector<double> arr1_2 = {min, min, min, min, min};
  const std::vector<double> min1 = {19.000000};
  const std::vector<double> max1 = {75.000000};
  const std::vector<std::string> dimNames1 = {"Test"};
  const std::vector<int> dimIDs1 = {1};
  const struct DiscreteDistributionInformation dist1 = {arr1, min1, max1, 1, 5, dimNames1, dimIDs1};
  const struct DiscreteDistributionInformation dist1_2 = {arr1_2, min1, max1, 1, 5, dimNames1, dimIDs1};

  const std::vector<double> arr2 = {0.0, 0.0, 0.0, 0.0, 0.0};
  const std::vector<double> min2 = {0.000000};
  const std::vector<double> max2 = {1.000000};
  const std::vector<double> min2_2 = {-1.000000};
  const std::vector<double> max2_2 = {0.000000};
  const std::vector<double> min2_3 = {0.000000};
  const std::vector<double> max2_3 = {0.000000};
  const std::vector<double> min2_4 = {42.000000};
  const std::vector<double> max2_4 = {42.000000};
  const std::vector<std::string> dimNames2 = {"Alter"};
  const std::vector<int> dimIDs2 = {1};
  const struct DiscreteDistributionInformation dist2 = {arr2, min2, max2, 1, 5, dimNames2, dimIDs2};
  const struct DiscreteDistributionInformation dist3 = {arr2, min2_2, max2_2, 1, 5, dimNames2, dimIDs2};
  const struct DiscreteDistributionInformation dist4 = {arr2, min2_3, max2_3, 1, 5, dimNames2, dimIDs2};
  const struct DiscreteDistributionInformation dist5 = {arr2, min2_3, max2_3, 1, 5, dimNames2, dimIDs2};

  // ACT and ASSERT
  EXPECT_THAT(stochastics->SampleFromDiscreteDistribution(&dist0), AllOf(Ge(19.0), Le(75.0)));

  EXPECT_THAT(stochastics->SampleFromDiscreteDistribution(&dist1), NanSensitiveDoubleEq(NAN));
  EXPECT_THAT(stochastics->SampleFromDiscreteDistribution(&dist1_2), AllOf(Ge(19.0), Le(75.0)));

  EXPECT_THAT(stochastics->SampleFromDiscreteDistribution(&dist2), AllOf(Ge(0.0), Le(1.0)));
  EXPECT_THAT(stochastics->SampleFromDiscreteDistribution(&dist3), AllOf(Ge(-1.0), Le(0.0)));
  EXPECT_THROW(stochastics->SampleFromDiscreteDistribution(&dist4), std::invalid_argument);
  EXPECT_THROW(stochastics->SampleFromDiscreteDistribution(&dist5), std::invalid_argument);
}

TEST(DriverCharacteristicsSampler_UnitTests, SampleFromDiscreteDistribution_TooManyDimensions_ExpectedBehaviour)
{
  std::uint32_t seed = 1;

  static const CallbackInterface* Callbacks = nullptr;
  StochasticsImplementation* stochastics = new StochasticsImplementation(Callbacks);
  stochastics->InitGenerator(seed);

  NiceMock<FakeStochastics> fakeStochastics;
  DriverCharacteristicsSampler sampler(&fakeStochastics, nullptr);

  const std::vector<double> arr1 = {-18.131861, -7.664595, -11.445331, -10.410105, -13.935195, -7.681026, -8.282271, -7.504068, -7.760330, -10.311710, -8.100457, -7.952119, -7.533214, -7.641094, -9.802423, -7.795565, -9.606355, -11.285648, -10.114498, -10.054768, -13.759657, -8.586949, -9.155485, -12.037710};
  const std::vector<double> arr2 = {-18.131861, -7.664595, -11.445331, -10.410105, -13.935195, -7.681026, -8.282271, -7.504068, -7.760330, -10.311710, -8.100457, -7.952119, -7.533214, -7.641094, -9.802423, -7.795565, -9.606355, -11.285648, -10.114498, -10.054768, -13.759657, -8.586949, -9.155485, -12.037710, -8.282271};
  const std::vector<double> min = {19.000000, 40.000000};
  const std::vector<double> max = {75.000000, 90.000000};
  const std::vector<std::string> dimNames = {"Alter", "STAI T PER"};
  const std::vector<int> dimIDs = {1, 2};
  const struct DiscreteDistributionInformation dist1 = {arr1, min, max, 2, 5, dimNames, dimIDs};
  const struct DiscreteDistributionInformation dist2 = {arr2, min, max, 1, 5, dimNames, dimIDs};

  EXPECT_THROW(stochastics->SampleFromDiscreteDistribution(&dist1), std::invalid_argument);
  EXPECT_THROW(stochastics->SampleFromDiscreteDistribution(&dist2), std::invalid_argument);
}

TEST(DriverCharacteristicsSampler_UnitTests, SampleDriverCharacteristics_MockUsedMethods_DefaultValues)
{
  NiceMock<FakeStochastics> fakeStochastics;
  DriverCharacteristicsSampler sampler(&fakeStochastics, nullptr);

  EXPECT_CALL(fakeStochastics, SampleFromDiscreteDistribution(_))
      .Times(20)
      .WillOnce(Return(0))
      .WillOnce(Return(1))
      .WillOnce(Return(2))
      .WillOnce(Return(3))
      .WillOnce(Return(4))
      .WillOnce(Return(5))
      .WillOnce(Return(6))
      .WillOnce(Return(7))
      .WillOnce(Return(8))
      .WillOnce(Return(9))
      .WillOnce(Return(10))
      .WillOnce(Return(11))
      .WillOnce(Return(12))
      .WillOnce(Return(13))
      .WillOnce(Return(14))
      .WillOnce(Return(15))
      .WillOnce(Return(16))
      .WillOnce(Return(17))
      .WillOnce(Return(18))
      .WillOnce(Return(19))
      .WillOnce(Return(20));

  EXPECT_CALL(fakeStochastics, ConditionOnRandomVariables(_, _, _))
      .Times(16);

  auto ret = sampler.SampleDriverCharacteristics();

  EXPECT_THAT(ret->gender, 0);
  EXPECT_THAT(ret->age, 1);
  EXPECT_THAT(ret->drivingRoutine, 2);

  EXPECT_THAT(ret->patience, 3);
  EXPECT_THAT(ret->riskTolerance, 4);
  EXPECT_THAT(ret->anxiety, 5);
  EXPECT_THAT(ret->disinhibition, 6);
  EXPECT_THAT(ret->thrillSeeking, 7);
  EXPECT_THAT(ret->boredomSusceptibility, 8);

  EXPECT_THAT(ret->drivingDynamics, NanSensitiveDoubleEq(NAN));
  EXPECT_THAT(ret->safetyNeed, NanSensitiveDoubleEq(NAN));
  EXPECT_THAT(ret->driverPerformance, NanSensitiveDoubleEq(NAN));

  EXPECT_THAT(ret->vWish, 9 / 3.6);
  EXPECT_THAT(ret->deltaVViolation, 10 / 3.6);
  EXPECT_THAT(ret->tgapego, 11);

  EXPECT_THAT(ret->comfortLongitudinalAcceleration, 13);
  EXPECT_THAT(ret->comfortLongitudinalDeceleration, 14);
  EXPECT_THAT(ret->comfortLateralAccelerationMean, 15);
  EXPECT_THAT(ret->qualityLaneKeeping, 16);

  EXPECT_THAT(ret->stearingAngle, 17);
  EXPECT_THAT(ret->timeLaneChange, 18);
  EXPECT_THAT(ret->reactionTime, 19);
}

TEST(DriverCharacteristicsSampler_UnitTests, SampleDriverCharacteristics_GraphSampling_CorrectSampeledAgents)
{
  std::uint32_t seed = 1;

  static const CallbackInterface* Callbacks = nullptr;
  StochasticsInterface* stochastics = new StochasticsImplementation(Callbacks);
  stochastics->InitGenerator(seed);
  DriverCharacteristicsSampler sampler(stochastics, nullptr);

  // TODO: when the StocasticData is changed these numbers may change as well
  std::vector<double> sampels = {1.0,
                                 66.9337,
                                 1.4296,
                                 6.98596,
                                 1.62196,
                                 58.7574,
                                 1.97828,
                                 2.71813,
                                 3.79877,
                                 186.796,
                                 -9.90412,
                                 1.74453,
                                 0.505607,
                                 0.23998,
                                 -0.777033,
                                 1.11988,
                                 0.515683,
                                 0.0375816,
                                 50.0265,
                                 5.75823};

  auto ret = sampler.SampleDriverCharacteristics();

  const double epsilon = 1e-3;

  EXPECT_NEAR(ret->gender, sampels.at(0), epsilon);
  EXPECT_NEAR(ret->age, sampels.at(1), epsilon);
  EXPECT_NEAR(ret->drivingRoutine, sampels.at(2), epsilon);
  EXPECT_NEAR(ret->patience, sampels.at(3), epsilon);
  EXPECT_NEAR(ret->riskTolerance, sampels.at(4), epsilon);
  EXPECT_NEAR(ret->anxiety, sampels.at(5), epsilon);
  EXPECT_NEAR(ret->disinhibition, sampels.at(6), epsilon);
  EXPECT_NEAR(ret->thrillSeeking, sampels.at(7), epsilon);
  EXPECT_NEAR(ret->boredomSusceptibility, sampels.at(8), epsilon);
  EXPECT_NEAR(ret->vWish, sampels.at(9) / 3.6, epsilon);
  EXPECT_NEAR(ret->deltaVViolation, sampels.at(10) / 3.6, epsilon);
  EXPECT_NEAR(ret->tgapego, sampels.at(11), epsilon);

  EXPECT_NEAR(ret->comfortLongitudinalAcceleration, sampels.at(13), epsilon);
  EXPECT_NEAR(ret->comfortLongitudinalDeceleration, sampels.at(14), epsilon);
  EXPECT_NEAR(ret->comfortLateralAccelerationMean, sampels.at(15), epsilon);
  EXPECT_NEAR(ret->qualityLaneKeeping, sampels.at(16), epsilon);
  EXPECT_NEAR(ret->stearingAngle, sampels.at(17), epsilon);
  EXPECT_NEAR(ret->timeLaneChange, sampels.at(18), epsilon);
  EXPECT_NEAR(ret->reactionTime, sampels.at(19), epsilon);
  // used for showing some generated agents in the test
  for (int i = 0; i < 4; ++i)
  {
    sampler.SampleDriverCharacteristics();
  }
}
