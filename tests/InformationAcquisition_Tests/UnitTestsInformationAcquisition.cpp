/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <OsiQueryLibrary/osiql.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "../Extrapolation_Tests/TestGroundtruthGenerator.h"
#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeStochastics.h"
#include "ScmDependencies.h"
#include "module/driver/src/InformationAcquisition.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;
struct InformationAcquisitionTester
{
  class InformationAcquisitionUnderTest : InformationAcquisition
  {
  public:
    template <typename... Args>
    InformationAcquisitionUnderTest(Args&&... args)
        : InformationAcquisition{std::forward<Args>(args)...} {};

    using InformationAcquisition::CorrectAoiDataDueToLongitudinalTransitionFromSideArea;
    using InformationAcquisition::ExtrapolateOwnVehicleData;
    using InformationAcquisition::PerceiveSurroundingVehicles;
    using InformationAcquisition::SetAoiDataWithGroundTruth;
    using InformationAcquisition::UpdateInconsistencies;
    using InformationAcquisition::UpdateOwnVehicleData;
  };

  template <typename T>
  T& WITH_CYCLETIME(T& fakeMentalModel, units::time::millisecond_t cycleTime)
  {
    ON_CALL(fakeMentalModel, GetCycleTime()).WillByDefault(Return(cycleTime));
    return fakeMentalModel;
  }

  OwnVehicleRoutePose CREATE_EXTRAPLOATION_INFORMATION(const osiql::Query& query)
  {
    auto osiqlLane = query.GetLane(3);
    auto referenceLine = query.GetReferenceLine(0);

    OwnVehicleRoutePose ownVehicleRoutePose{
        .route = std::make_shared<osiql::Route>(query.GetRoute(osiql::Vector2d{referenceLine->front()}, osiql::Vector2d{referenceLine->back()})),
        .pose = std::make_shared<osiql::Pose<osiql::Point<const osiql::Lane>>>(osiql::Point<const osiql::Lane>{osiql::ReferenceLineCoordinates{75, 0}, *osiqlLane})};

    return ownVehicleRoutePose;
  }

  InformationAcquisitionTester()
      : fakeMentalModel{},
        fakeFeatureExtractor{},
        fakeMentalCalculations{},
        fakeMicroscopicCharacteristics{},
        ownVehicleInformation{},
        surroundingObjects{},
        informationAcquisition(WITH_CYCLETIME(fakeMentalModel, 100_ms),
                               fakeFeatureExtractor,
                               fakeMentalCalculations,
                               ownVehicleInformation,
                               surroundingObjects,
                               false,
                               ownVehicleRoutePose)
  {
    groundTruth = CREATE_GROUNDTRUTH();
    query = std::make_unique<osiql::Query>(groundTruth);
    ownVehicleRoutePose = CREATE_EXTRAPLOATION_INFORMATION(*query);
  }

  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;

  OwnVehicleInformationSCM ownVehicleInformation;
  SurroundingObjectsSCM surroundingObjects;

  osi3::GroundTruth groundTruth;
  std::unique_ptr<osiql::Query> query;
  OwnVehicleRoutePose ownVehicleRoutePose;

  InformationAcquisitionUnderTest informationAcquisition;
};

/*********************************************************
 * CHECK InformationAcquisition ExtrapolateOwnVehicleData*
 *********************************************************/

TEST(InformationAcquisition_UnitTests, InformationAcquisition_CheckFunction_ExtrapolateOwnVehicleData)
{
  InformationAcquisitionTester TEST_HELPER;

  //  Set up test
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));

  OwnVehicleInformationScmExtended agentEgo;
  agentEgo.absoluteVelocity = 15._mps;
  ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).WillByDefault(Return(&agentEgo));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetAbsoluteVelocityEgo(_)).WillByDefault(Return(agentEgo.absoluteVelocity));

  OwnVehicleInformationSCM agentEgo_GroundTruth;  // Fill all with non-default values
  agentEgo_GroundTruth.acceleration = 1._mps_sq;
  agentEgo_GroundTruth.collision = true;
  agentEgo_GroundTruth.distanceToLaneBoundaryLeft = .865_m;
  agentEgo_GroundTruth.distanceToLaneBoundaryRight = .885_m;
  agentEgo_GroundTruth.forcedLaneChangeStartTrigger = LaneChangeAction::ForceNegative;
  agentEgo_GroundTruth.heading = 0._rad;
  agentEgo_GroundTruth.id = 1;
  agentEgo_GroundTruth.lateralPosition = 0._m;
  agentEgo_GroundTruth.lateralVelocity = 0._mps;
  agentEgo_GroundTruth.longitudinalPosition = 100._m;
  agentEgo_GroundTruth.longitudinalVelocity = 20._mps;
  agentEgo_GroundTruth.mainLaneId = -2;
  agentEgo_GroundTruth.relativeLaneIdForJunctionIngoing = {-2};
  agentEgo_GroundTruth.steeringWheelAngle = 0._rad;
  agentEgo_GroundTruth.absoluteVelocity = 20._mps;

  TEST_HELPER.informationAcquisition.ExtrapolateOwnVehicleData(&agentEgo_GroundTruth);
  ASSERT_EQ(agentEgo.acceleration, 1._mps_sq);
  ASSERT_FALSE(agentEgo.collision);
  ASSERT_EQ(agentEgo.distanceToLaneBoundaryLeft, .865_m);
  ASSERT_EQ(agentEgo.distanceToLaneBoundaryRight, .885_m);
  ASSERT_EQ(agentEgo.forcedLaneChangeStartTrigger, LaneChangeAction::None);
  ASSERT_EQ(agentEgo.heading, 0._rad);
  ASSERT_EQ(agentEgo.id, -1);
  ASSERT_EQ(agentEgo.lateralPosition, 0._m);
  ASSERT_EQ(agentEgo.lateralVelocity, 0._mps);
  ASSERT_EQ(agentEgo.longitudinalPosition, -999._m);
  ASSERT_EQ(agentEgo.longitudinalVelocity, 20._mps);
  ASSERT_EQ(agentEgo.mainLaneId, -2);
  ASSERT_TRUE(agentEgo.relativeLaneIdForJunctionIngoing.empty());
  ASSERT_EQ(agentEgo.steeringWheelAngle, -999._rad);
  ASSERT_EQ(agentEgo.absoluteVelocity, 15.09_mps);
}

/*****************************************************
 * CHECK InformationAcquisition UpdateOwnVehicleData *
 *****************************************************/

TEST(InformationAcquisition_UnitTests, InformationAcquisition_CheckFunction_UpdateOwnVehicleData)
{
  InformationAcquisitionTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));

  OwnVehicleInformationScmExtended agentEgo;  // Fill all with non-default values
  agentEgo.acceleration = 0._mps_sq;
  agentEgo.collision = false;
  agentEgo.distanceToLaneBoundaryLeft = .875_m;
  agentEgo.distanceToLaneBoundaryRight = .875_m;
  agentEgo.forcedLaneChangeStartTrigger = LaneChangeAction::None;
  agentEgo.heading = -1._rad;
  agentEgo.id = 2;
  agentEgo.lateralPosition = -.1_m;
  agentEgo.lateralVelocity = .1_mps;
  agentEgo.longitudinalPosition = 200._m;
  agentEgo.longitudinalVelocity = 30._mps;
  agentEgo.mainLaneId = -1;
  agentEgo.relativeLaneIdForJunctionIngoing = {-1};
  agentEgo.steeringWheelAngle = 2._rad;
  agentEgo.absoluteVelocity = 15._mps;
  agentEgo.absoluteVelocityReal = 10._mps;
  ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).WillByDefault(Return(&agentEgo));

  OwnVehicleInformationSCM agentEgo_GroundTruth;  // Fill all with non-default values
  agentEgo_GroundTruth.acceleration = 1._mps_sq;
  agentEgo_GroundTruth.collision = true;
  agentEgo_GroundTruth.distanceToLaneBoundaryLeft = .865_m;
  agentEgo_GroundTruth.distanceToLaneBoundaryRight = .885_m;
  agentEgo_GroundTruth.forcedLaneChangeStartTrigger = LaneChangeAction::ForceNegative;
  agentEgo_GroundTruth.heading = 0._rad;
  agentEgo_GroundTruth.id = 1;
  agentEgo_GroundTruth.lateralPosition = 0._m;
  agentEgo_GroundTruth.lateralVelocity = 0._mps;
  agentEgo_GroundTruth.longitudinalPosition = 100._m;
  agentEgo_GroundTruth.longitudinalVelocity = 20._mps;
  agentEgo_GroundTruth.mainLaneId = -2;
  agentEgo_GroundTruth.relativeLaneIdForJunctionIngoing = {-2};
  agentEgo_GroundTruth.steeringWheelAngle = 0._rad;
  agentEgo_GroundTruth.absoluteVelocity = 20._mps;

  TEST_HELPER.informationAcquisition.UpdateOwnVehicleData(&agentEgo_GroundTruth);

  ASSERT_EQ(agentEgo.acceleration, 1._mps_sq);
  ASSERT_TRUE(agentEgo.collision);
  ASSERT_EQ(agentEgo.distanceToLaneBoundaryLeft, .865_m);
  ASSERT_EQ(agentEgo.distanceToLaneBoundaryRight, .885_m);
  ASSERT_EQ(agentEgo.forcedLaneChangeStartTrigger, LaneChangeAction::None);
  ASSERT_EQ(agentEgo.heading, 0._rad);
  ASSERT_EQ(agentEgo.id, 2);
  ASSERT_EQ(agentEgo.lateralPosition, 0._m);
  ASSERT_EQ(agentEgo.lateralVelocity, 0._mps);
  ASSERT_EQ(agentEgo.longitudinalPosition, 200._m);
  ASSERT_EQ(agentEgo.longitudinalVelocity, 20._mps);
  ASSERT_EQ(agentEgo.mainLaneId, -2);
  const std::vector<int> expectedLanes{-1};
  ASSERT_EQ(agentEgo.relativeLaneIdForJunctionIngoing, expectedLanes);
  ASSERT_EQ(agentEgo.steeringWheelAngle, 2._rad);
  ASSERT_EQ(agentEgo.absoluteVelocity, 20._mps);
  ASSERT_EQ(agentEgo.absoluteVelocityReal, 20._mps);
}

class InformationAcquisition_AssignSurroundingOpticalData : public ::testing::Test
{
protected:
  InformationAcquisitionTester TEST_HELPER;

  std::unique_ptr<ScmDependencies> scmDependencies;
  DriverParameters driverParameters;
  GeometryInformationSCM geometryInformation;
  OwnVehicleInformationScmExtended egoAgent;
  ObjectInformationSCM agentExist_GroundTruth;
  ObjectInformationScmExtended agentExist_GroundTruth3;
  ObjectInformationSCM agentExist_GroundTruth2;
  ObjectInformationScmExtended agentNotExist_GroundTruth;
  std::vector<ObjectInformationSCM> sideObjectsScm = {agentExist_GroundTruth, agentExist_GroundTruth2};
  std::vector<ObjectInformationScmExtended> leftSideAgents = {agentExist_GroundTruth3};
  SurroundingObjectsSCM surroundingObjects_GroundTruth;

  virtual void SetUp()
  {
    driverParameters.thresholdRetinalProjectionAngularSpeedFovea = 1._rad_per_s;
    driverParameters.previewDistance = 250._m;

    geometryInformation.Close().exists = true;
    geometryInformation.Close().width = 3.75_m;
    geometryInformation.Left().exists = true;
    geometryInformation.Left().width = 3.75_m;
    geometryInformation.Right().exists = true;
    geometryInformation.Right().width = 3.75_m;

    egoAgent.longitudinalVelocity = 15._mps;
    egoAgent.lateralVelocity = 1._mps;
    egoAgent.acceleration = 2._mps_sq;
    egoAgent.mainLaneId = -2;

    agentExist_GroundTruth.acceleration = 1._mps_sq;
    agentExist_GroundTruth.brakeLightsActive = true;
    agentExist_GroundTruth.collision = true;
    agentExist_GroundTruth.distanceToLaneBoundaryLeft = .875_m;
    agentExist_GroundTruth.distanceToLaneBoundaryRight = .875_m;
    agentExist_GroundTruth.exist = true;
    agentExist_GroundTruth.heading = 0.;
    agentExist_GroundTruth.height = 2._m;
    agentExist_GroundTruth.id = -1;
    agentExist_GroundTruth.indicatorState = scm::LightState::Indicator::Warn;
    agentExist_GroundTruth.isStatic = true;
    agentExist_GroundTruth.lateralPositionInLane = 0._m;
    agentExist_GroundTruth.lateralVelocity = .5_mps;
    agentExist_GroundTruth.length = 5._m;
    agentExist_GroundTruth.longitudinalObstruction = ObstructionLongitudinal(60._m, -50._m, 55._m);
    agentExist_GroundTruth.longitudinalVelocity = 20._mps;
    agentExist_GroundTruth.obstruction = ObstructionScm::NoOpponent();
    agentExist_GroundTruth.relativeLateralDistance = 0._m;
    agentExist_GroundTruth.relativeLongitudinalDistance = 50._m;
    agentExist_GroundTruth.absoluteVelocity = 20._mps;
    agentExist_GroundTruth.width = 2._m;

    agentExist_GroundTruth2.acceleration = 2._mps_sq;
    agentExist_GroundTruth2.brakeLightsActive = true;
    agentExist_GroundTruth2.collision = true;
    agentExist_GroundTruth2.distanceToLaneBoundaryLeft = .775_m;
    agentExist_GroundTruth2.distanceToLaneBoundaryRight = .775_m;
    agentExist_GroundTruth2.exist = true;
    agentExist_GroundTruth2.heading = 0.;
    agentExist_GroundTruth2.height = 2.2_m;
    agentExist_GroundTruth2.id = -2;
    agentExist_GroundTruth2.indicatorState = scm::LightState::Indicator::Warn;
    agentExist_GroundTruth2.isStatic = true;
    agentExist_GroundTruth2.lateralPositionInLane = 0._m;
    agentExist_GroundTruth2.lateralVelocity = .5_mps;
    agentExist_GroundTruth2.length = 5.1_m;
    agentExist_GroundTruth2.longitudinalObstruction = ObstructionLongitudinal(-25._m, 35._m, 30.1_m);
    agentExist_GroundTruth2.longitudinalVelocity = 20.2_mps;
    agentExist_GroundTruth2.obstruction = ObstructionScm::NoOpponent();
    agentExist_GroundTruth2.relativeLateralDistance = 0._m;
    agentExist_GroundTruth2.relativeLongitudinalDistance = 25._m;
    agentExist_GroundTruth2.absoluteVelocity = 20.2_mps;
    agentExist_GroundTruth2.width = 2.2_m;

    surroundingObjects_GroundTruth.ongoingTraffic.Ahead().Close().push_back(agentExist_GroundTruth);
    surroundingObjects_GroundTruth.ongoingTraffic.Close().Left() = sideObjectsScm;
    surroundingObjects_GroundTruth.ongoingTraffic.Close().Right() = sideObjectsScm;
  }

  void SetUpData()
  {
    ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));
    ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));
    ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, UpdateSideObjectsInformation(AreaOfInterest::LEFT_SIDE)).WillByDefault(Return(&leftSideAgents));
    ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, UpdateObjectInformation(_, _)).WillByDefault(Return(&agentExist_GroundTruth3));
    ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, GetOwnVehicleInformation()).WillByDefault(Return(&egoAgent));
    ON_CALL(TEST_HELPER.fakeMicroscopicCharacteristics, GetSideObjectVector(_)).WillByDefault(Return(&leftSideAgents));
  }

  void UpdateScmDependencies()
  {
    scm::signal::DriverInput driverInput;
    driverInput.sensorOutput.ownVehicleInformationSCM = {};
    driverInput.sensorOutput.trafficRuleInformationSCM = {};
    driverInput.sensorOutput.geometryInformationSCM = geometryInformation;
    driverInput.sensorOutput.surroundingObjectsSCM = surroundingObjects_GroundTruth;
    driverInput.driverParameters = driverParameters;

    NiceMock<FakeStochastics> fakeStochastics;
    DriverParameters testDriverParameters;
    TrafficRulesScm testTrafficRulesScm;
    scm::common::vehicle::properties::EntityProperties testVehicleParameters;
    scm::signal::SensorOutput sensorOutput;

    scmDependencies = std::make_unique<ScmDependencies>(testDriverParameters, testTrafficRulesScm, testVehicleParameters, sensorOutput, &fakeStochastics, 120_kph, 100_ms);
    scmDependencies->UpdateScmSensorData(driverInput);
  }
};

/**********************************************************
 * CHECK InformationAcquisition FrontVehicleFovealQuality *
 **********************************************************/

TEST_F(InformationAcquisition_AssignSurroundingOpticalData, FrontVehicleFovealQuality)
{
  SetUpData();
  UpdateScmDependencies();

  ObjectInformationScmExtended testObject;
  TEST_HELPER.informationAcquisition.SetAoiDataWithGroundTruth(&testObject, &surroundingObjects_GroundTruth.ongoingTraffic.Ahead().Close().front(), AreaOfInterest::EGO_FRONT);

  ASSERT_TRUE(testObject.exist);
  ASSERT_EQ(testObject.acceleration, agentExist_GroundTruth.acceleration);
  ASSERT_EQ(testObject.absoluteVelocity, agentExist_GroundTruth.absoluteVelocity);
  ASSERT_EQ(testObject.lateralVelocity, agentExist_GroundTruth.lateralVelocity);
  ASSERT_EQ(testObject.longitudinalVelocity, agentExist_GroundTruth.longitudinalVelocity);
  ASSERT_EQ(testObject.distanceToLaneBoundaryLeft, agentExist_GroundTruth.distanceToLaneBoundaryLeft);
  ASSERT_EQ(testObject.distanceToLaneBoundaryRight, agentExist_GroundTruth.distanceToLaneBoundaryRight);
  ASSERT_EQ(testObject.relativeLateralDistance, agentExist_GroundTruth.relativeLateralDistance);
  ASSERT_EQ(testObject.width, agentExist_GroundTruth.width);
  ASSERT_EQ(testObject.height, agentExist_GroundTruth.height);
  ASSERT_EQ(testObject.length, agentExist_GroundTruth.length);
  ASSERT_EQ(testObject.lateralPositionInLane, agentExist_GroundTruth.lateralPositionInLane);
  ASSERT_EQ(testObject.brakeLightsActive, agentExist_GroundTruth.brakeLightsActive);
  ASSERT_EQ(testObject.indicatorState, agentExist_GroundTruth.indicatorState);
  ASSERT_EQ(testObject.id, agentExist_GroundTruth.id);
  ASSERT_EQ(testObject.collision, agentExist_GroundTruth.collision);
  ASSERT_EQ(testObject.isStatic, agentExist_GroundTruth.isStatic);
  ASSERT_EQ(testObject.heading, agentExist_GroundTruth.heading);
  ASSERT_EQ(testObject.relativeLongitudinalDistance, agentExist_GroundTruth.relativeLongitudinalDistance);
  ASSERT_TRUE(testObject.longitudinalObstruction.front == agentExist_GroundTruth.longitudinalObstruction.front &&
              testObject.longitudinalObstruction.rear == agentExist_GroundTruth.longitudinalObstruction.rear &&
              testObject.longitudinalObstruction.mainLaneLocator == agentExist_GroundTruth.longitudinalObstruction.mainLaneLocator &&
              testObject.longitudinalObstruction.isOverlapping == agentExist_GroundTruth.longitudinalObstruction.isOverlapping &&
              testObject.longitudinalObstruction.valid == agentExist_GroundTruth.longitudinalObstruction.valid);
  ASSERT_EQ(testObject.gap, ScmCommons::CalculateNetGap(egoAgent.longitudinalVelocity, agentExist_GroundTruth.absoluteVelocity, agentExist_GroundTruth.relativeLongitudinalDistance, AreaOfInterest::EGO_FRONT));
  ASSERT_DOUBLE_EQ(testObject.gapDot, ScmCommons::CalculateNetGapDot(egoAgent.longitudinalVelocity, agentExist_GroundTruth.absoluteVelocity, egoAgent.acceleration, agentExist_GroundTruth.acceleration, agentExist_GroundTruth.relativeLongitudinalDistance, 5._mps, AreaOfInterest::EGO_FRONT));
  ASSERT_EQ(testObject.ttc, 99.0_s);
  ASSERT_DOUBLE_EQ(testObject.tauDot, 0.);
}

/*******************************************************
 * CHECK InformationAcquisition RearVehicleUfovQuality *
 *******************************************************/

TEST_F(InformationAcquisition_AssignSurroundingOpticalData, RearVehicleUfovQuality)
{
  SetUpData();
  surroundingObjects_GroundTruth.ongoingTraffic.Ahead().Close().push_back(agentNotExist_GroundTruth);
  surroundingObjects_GroundTruth.ongoingTraffic.Behind().Close().push_back(agentExist_GroundTruth);
  surroundingObjects_GroundTruth.ongoingTraffic.Behind().Close().front().longitudinalObstruction = ObstructionLongitudinal(-50._m, 60._m, -55._m);
  UpdateScmDependencies();

  ObjectInformationScmExtended testObject;
  TEST_HELPER.informationAcquisition.SetAoiDataWithGroundTruth(&testObject, &surroundingObjects_GroundTruth.ongoingTraffic.Behind().Close().front(), AreaOfInterest::EGO_REAR);

  const double ufovError{1.};
  ASSERT_TRUE(testObject.exist);
  ASSERT_EQ(testObject.acceleration, agentExist_GroundTruth.acceleration * ufovError);
  ASSERT_EQ(testObject.absoluteVelocity, agentExist_GroundTruth.absoluteVelocity * ufovError);
  ASSERT_EQ(testObject.lateralVelocity, agentExist_GroundTruth.lateralVelocity * ufovError);
  ASSERT_EQ(testObject.longitudinalVelocity, agentExist_GroundTruth.longitudinalVelocity);
  ASSERT_EQ(testObject.distanceToLaneBoundaryLeft, agentExist_GroundTruth.distanceToLaneBoundaryLeft);
  ASSERT_EQ(testObject.distanceToLaneBoundaryRight, agentExist_GroundTruth.distanceToLaneBoundaryRight);
  ASSERT_EQ(testObject.relativeLateralDistance, agentExist_GroundTruth.relativeLateralDistance);
  ASSERT_EQ(testObject.width, agentExist_GroundTruth.width);
  ASSERT_EQ(testObject.height, agentExist_GroundTruth.height);
  ASSERT_EQ(testObject.length, agentExist_GroundTruth.length);
  ASSERT_EQ(testObject.lateralPositionInLane, agentExist_GroundTruth.lateralPositionInLane);
  ASSERT_EQ(testObject.brakeLightsActive, agentExist_GroundTruth.brakeLightsActive);
  ASSERT_EQ(testObject.indicatorState, agentExist_GroundTruth.indicatorState);
  ASSERT_EQ(testObject.id, agentExist_GroundTruth.id);
  ASSERT_EQ(testObject.collision, agentExist_GroundTruth.collision);
  ASSERT_EQ(testObject.isStatic, agentExist_GroundTruth.isStatic);
  ASSERT_DOUBLE_EQ(testObject.heading, agentExist_GroundTruth.heading);
  ASSERT_EQ(testObject.relativeLongitudinalDistance, agentExist_GroundTruth.relativeLongitudinalDistance);
  ASSERT_TRUE(testObject.longitudinalObstruction.front == -50._m && testObject.longitudinalObstruction.rear == 60._m &&
              testObject.longitudinalObstruction.mainLaneLocator == -55._m && !testObject.longitudinalObstruction.isOverlapping &&
              testObject.longitudinalObstruction.valid);
  ASSERT_EQ(testObject.gap, ScmCommons::CalculateNetGap(15._mps, 20._mps * ufovError, 50._m, AreaOfInterest::EGO_REAR));
  ASSERT_DOUBLE_EQ(testObject.gapDot, ScmCommons::CalculateNetGapDot(15._mps, 20._mps, 2._mps_sq, 1._mps_sq, 50._m, -5._mps, AreaOfInterest::EGO_REAR));
  ASSERT_EQ(testObject.ttc, 99_s);
  ASSERT_DOUBLE_EQ(testObject.tauDot, 0.);
}

/*********************************************
 * CHECK InformationAcquisition SideVehicles *
 *********************************************/

TEST_F(InformationAcquisition_AssignSurroundingOpticalData, SideVehicles)
{
  SetUpData();

  sideObjectsScm = {agentExist_GroundTruth, agentExist_GroundTruth2};
  surroundingObjects_GroundTruth.ongoingTraffic.Behind().Close().push_back(agentNotExist_GroundTruth);
  surroundingObjects_GroundTruth.ongoingTraffic.Close().Left() = sideObjectsScm;
  surroundingObjects_GroundTruth.ongoingTraffic.Close().Left().front().longitudinalObstruction = ObstructionLongitudinal(.1_m, 8.5_m, 4.9_m);
  surroundingObjects_GroundTruth.ongoingTraffic.Close().Left().back().longitudinalObstruction = ObstructionLongitudinal(.1_m, 8.5_m, 4.9_m);
  UpdateScmDependencies();

  ObjectInformationScmExtended testObject;
  TEST_HELPER.informationAcquisition.SetAoiDataWithGroundTruth(&testObject, &surroundingObjects_GroundTruth.ongoingTraffic.Close().Left().front(), AreaOfInterest::LEFT_SIDE);

  ASSERT_TRUE(testObject.exist);
  ASSERT_EQ(testObject.acceleration, agentExist_GroundTruth.acceleration);
  ASSERT_EQ(testObject.absoluteVelocity, agentExist_GroundTruth.absoluteVelocity);
  ASSERT_EQ(testObject.lateralVelocity, agentExist_GroundTruth.lateralVelocity);
  ASSERT_EQ(testObject.longitudinalVelocity, agentExist_GroundTruth.longitudinalVelocity);
  ASSERT_EQ(testObject.distanceToLaneBoundaryLeft, agentExist_GroundTruth.distanceToLaneBoundaryLeft);
  ASSERT_EQ(testObject.distanceToLaneBoundaryRight, agentExist_GroundTruth.distanceToLaneBoundaryRight);
  ASSERT_EQ(testObject.relativeLateralDistance, agentExist_GroundTruth.relativeLateralDistance);
  ASSERT_EQ(testObject.width, agentExist_GroundTruth.width);
  ASSERT_EQ(testObject.height, agentExist_GroundTruth.height);
  ASSERT_EQ(testObject.length, agentExist_GroundTruth.length);
  ASSERT_EQ(testObject.lateralPositionInLane, agentExist_GroundTruth.lateralPositionInLane);
  ASSERT_EQ(testObject.brakeLightsActive, agentExist_GroundTruth.brakeLightsActive);
  ASSERT_EQ(testObject.indicatorState, agentExist_GroundTruth.indicatorState);
  ASSERT_EQ(testObject.id, agentExist_GroundTruth.id);
  ASSERT_EQ(testObject.collision, agentExist_GroundTruth.collision);
  ASSERT_EQ(testObject.isStatic, agentExist_GroundTruth.isStatic);
  ASSERT_DOUBLE_EQ(testObject.heading, agentExist_GroundTruth.heading);
  ASSERT_EQ(testObject.relativeLongitudinalDistance, agentExist_GroundTruth.relativeLongitudinalDistance);
  ASSERT_TRUE(testObject.longitudinalObstruction.front == .1_m && testObject.longitudinalObstruction.rear == 8.5_m &&
              testObject.longitudinalObstruction.mainLaneLocator == 4.9_m && testObject.longitudinalObstruction.isOverlapping &&
              testObject.longitudinalObstruction.valid);
  ASSERT_EQ(testObject.gap, 0._s);
  ASSERT_DOUBLE_EQ(testObject.gapDot, 0.);
  ASSERT_EQ(testObject.ttc, 99._s);
  ASSERT_DOUBLE_EQ(testObject.tauDot, 0.);
}

/*************************************
 * CHECK PerceiveSurroundingVehicles *
 *************************************/
struct DataFor_PerceiveSurroundingVehicles
{
  AreaOfInterest fovea;
  std::vector<AreaOfInterest> ufov;
  std::vector<std::tuple<AreaOfInterest, int>> expectedVehicles;
};

class InformationAcquisition_PerceiveSurroundingVehicles : public ::testing::Test,
                                                           public ::testing::WithParamInterface<DataFor_PerceiveSurroundingVehicles>
{
public:
  void SET_SURROUNDING_OBJECTS()
  {
    ObjectInformationSCM vehicle;
    vehicle.id = 0;
    vehicle.exist = true;
    _ongoingTrafficObjects.Ahead().Close().push_back(vehicle);
    vehicle.id = 1;
    vehicle.exist = true;
    _ongoingTrafficObjects.Ahead().Close().push_back(vehicle);
    vehicle.id = 2;
    vehicle.exist = true;
    _ongoingTrafficObjects.Behind().Close().push_back(vehicle);
    vehicle.id = 3;
    vehicle.exist = true;
    _ongoingTrafficObjects.Ahead().Left().push_back(vehicle);
    vehicle.id = 4;
    vehicle.exist = true;
    _ongoingTrafficObjects.Ahead().Left().push_back(vehicle);
    vehicle.id = 5;
    vehicle.exist = true;
    _ongoingTrafficObjects.Behind().Left().push_back(vehicle);
    vehicle.id = 6;
    vehicle.exist = true;
    _ongoingTrafficObjects.Ahead().FarLeft().push_back(vehicle);
    vehicle.id = 7;
    vehicle.exist = true;
    _ongoingTrafficObjects.Close().Left().push_back(vehicle);
    vehicle.id = 8;
    vehicle.exist = true;
    _ongoingTrafficObjects.Close().Left().push_back(vehicle);
    vehicle.id = 9;
    vehicle.exist = true;
    _ongoingTrafficObjects.Ahead().Right().push_back(vehicle);
    vehicle.id = 10;
    vehicle.exist = true;
    _ongoingTrafficObjects.Ahead().Right().push_back(vehicle);
    vehicle.id = 11;
    vehicle.exist = true;
    _ongoingTrafficObjects.Behind().Right().push_back(vehicle);
    vehicle.id = 12;
    vehicle.exist = true;
    _ongoingTrafficObjects.Ahead().Right().push_back(vehicle);
    vehicle.id = 13;
    vehicle.exist = true;
    _ongoingTrafficObjects.Close().Right().push_back(vehicle);
    vehicle.id = 14;
    vehicle.exist = true;
    _ongoingTrafficObjects.Close().Right().push_back(vehicle);
    _surroundingObjects.ongoingTraffic = _ongoingTrafficObjects;
  }
  SurroundingObjectsSCM _surroundingObjects;
  OngoingTrafficObjectsSCM _ongoingTrafficObjects;

  //! \brief Checks if at least one match is found for the result combination of AOI and vehicle id in the expectedIds
  //! Necessary because order of aois and ids might not be the same in test case result definition and actual result
  bool CHECK_RESULT(const std::tuple<AreaOfInterest, const ObjectInformationSCM*>& result,
                    const std::vector<std::tuple<AreaOfInterest, int>>& expectedIds)
  {
    const auto& resultAoi{std::get<0>(result)};
    const auto& resultVehicle{std::get<1>(result)};

    return std::any_of(begin(expectedIds), end(expectedIds), [resultAoi, resultVehicle](const auto& expectedPair)
                       {
      auto& [expectedAoi, expectedId] = expectedPair;
      return resultAoi == expectedAoi && resultVehicle->id == expectedId; });
  }
};

TEST_F(InformationAcquisition_PerceiveSurroundingVehicles, GivenVehicleExistsIdealPerception_AddToResult)
{
  InformationAcquisitionTester TEST_HELPER;

  SET_SURROUNDING_OBJECTS();
  TEST_HELPER.surroundingObjects = _surroundingObjects;
  ON_CALL(TEST_HELPER.fakeMentalModel, IsPointVisible).WillByDefault(Return(true));
  const auto result = TEST_HELPER.informationAcquisition.PerceiveSurroundingVehicles(true);

  ASSERT_EQ(15, result.size());
}

TEST_F(InformationAcquisition_PerceiveSurroundingVehicles, GivenVehicleNotExistsIdealPerception_AddToResult)
{
  InformationAcquisitionTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, IsPointVisible).WillByDefault(Return(false));

  SET_SURROUNDING_OBJECTS();
  TEST_HELPER.surroundingObjects = _surroundingObjects;
  const auto result = TEST_HELPER.informationAcquisition.PerceiveSurroundingVehicles(false);
  ASSERT_EQ(0, result.size());
}

/***************************************************************
 * CHECK CorrectAoiDataDueToLongitudinalTransitionFromSideArea *
 ***************************************************************/
class InformationAcquisition_CorrectAoiDataDueToLongitudinalTransitionFromSideArea : public ::testing::Test
{
};

TEST_F(InformationAcquisition_CorrectAoiDataDueToLongitudinalTransitionFromSideArea, TransitionToLeftFront)
{
  InformationAcquisitionTester TEST_HELPER;

  OwnVehicleInformationScmExtended egoInfo;
  egoInfo.acceleration = -2.5_mps_sq;
  egoInfo.longitudinalVelocity = 10._mps;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(egoInfo.longitudinalVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAcceleration()).WillByDefault(Return(egoInfo.acceleration));

  ObjectInformationScmExtended observedAgent;
  observedAgent.acceleration = 0._mps_sq;
  observedAgent.longitudinalVelocity = 5._mps;
  observedAgent.longitudinalObstruction.front = -10._m;
  observedAgent.longitudinalObstruction.rear = -5._m;
  observedAgent.longitudinalObstruction.mainLaneLocator = -15._m;
  observedAgent.width = 100._m;   // by-pass perception model for TTC
  observedAgent.height = 100._m;  // by-pass perception mode for TTC
  // start values for SIDE AreaOfInterest
  observedAgent.relativeLongitudinalDistance = 0._m;
  observedAgent.gap = 0._s;
  observedAgent.gapDot = 0.;
  observedAgent.ttc = 99._s;
  observedAgent.tauDot = 0.;
  AreaOfInterest aoi{AreaOfInterest::LEFT_FRONT};
  const auto accelerationDelta{observedAgent.acceleration - egoInfo.acceleration};
  const auto longitudinalVelocityDelta{observedAgent.longitudinalVelocity - egoInfo.longitudinalVelocity};
  TEST_HELPER.informationAcquisition.CorrectAoiDataDueToLongitudinalTransitionFromSideArea(&observedAgent, aoi);

  ASSERT_EQ(observedAgent.relativeLongitudinalDistance, -observedAgent.longitudinalObstruction.rear);
  ASSERT_EQ(observedAgent.gap, observedAgent.relativeLongitudinalDistance / egoInfo.longitudinalVelocity);
  ASSERT_EQ(observedAgent.gapDot, longitudinalVelocityDelta / egoInfo.longitudinalVelocity - (observedAgent.relativeLongitudinalDistance * egoInfo.acceleration) / (egoInfo.longitudinalVelocity * egoInfo.longitudinalVelocity));
  ASSERT_EQ(observedAgent.ttc, -observedAgent.relativeLongitudinalDistance / longitudinalVelocityDelta);
  ASSERT_DOUBLE_EQ(observedAgent.tauDot, -(1. - observedAgent.relativeLongitudinalDistance * accelerationDelta / (longitudinalVelocityDelta * longitudinalVelocityDelta)));
}

TEST_F(InformationAcquisition_CorrectAoiDataDueToLongitudinalTransitionFromSideArea, TransitionToLeftRear)
{
  InformationAcquisitionTester TEST_HELPER;
  OwnVehicleInformationScmExtended egoInfo;
  egoInfo.acceleration = -2.5_mps_sq;
  egoInfo.longitudinalVelocity = 10._mps;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLongitudinalVelocityEgo()).WillByDefault(Return(egoInfo.longitudinalVelocity));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetAcceleration()).WillByDefault(Return(egoInfo.acceleration));

  // ToDo: ObstructionLongitudinal is not possible
  ObjectInformationScmExtended observedAgent;
  observedAgent.ResetToDefault();
  observedAgent.acceleration = 0._mps_sq;
  observedAgent.longitudinalVelocity = 5._mps;
  observedAgent.longitudinalObstruction.front = -10._m;
  observedAgent.longitudinalObstruction.rear = -5._m;
  observedAgent.longitudinalObstruction.mainLaneLocator = -15._m;
  observedAgent.width = 100._m;   // by-pass perception model for TTC
  observedAgent.height = 100._m;  // by-pass perception mode for TTC
  // start values for SIDE AreaOfInterest
  observedAgent.relativeLongitudinalDistance = 0._m;
  observedAgent.gap = 0._s;
  observedAgent.gapDot = 0.;
  observedAgent.ttc = 99._s;
  observedAgent.tauDot = 0.;
  AreaOfInterest aoi{AreaOfInterest::LEFT_REAR};
  const auto accelerationDelta{egoInfo.acceleration - observedAgent.acceleration};
  const auto longitudinalVelocityDelta{egoInfo.longitudinalVelocity - observedAgent.longitudinalVelocity};

  TEST_HELPER.informationAcquisition.CorrectAoiDataDueToLongitudinalTransitionFromSideArea(&observedAgent, aoi);
  ASSERT_EQ(observedAgent.relativeLongitudinalDistance, -observedAgent.longitudinalObstruction.front);
  ASSERT_EQ(observedAgent.gap, observedAgent.relativeLongitudinalDistance / observedAgent.longitudinalVelocity);
  ASSERT_DOUBLE_EQ(observedAgent.gapDot, longitudinalVelocityDelta / observedAgent.longitudinalVelocity - (observedAgent.relativeLongitudinalDistance * observedAgent.acceleration) / (observedAgent.longitudinalVelocity * observedAgent.longitudinalVelocity));
  ASSERT_EQ(observedAgent.ttc, -observedAgent.relativeLongitudinalDistance / longitudinalVelocityDelta);
  ASSERT_DOUBLE_EQ(observedAgent.tauDot, -(1. - observedAgent.relativeLongitudinalDistance * accelerationDelta / (longitudinalVelocityDelta * longitudinalVelocityDelta)));
}

/*******************************
 * CHECK UpdateInconsistencies *
 *******************************/

TEST(InformationAcquisition_UpdateInconsistencies, IfTriggerInformationAcquisitionFalse_OnlyAddInconsistentAois)
{
  InformationAcquisitionTester TEST_HELPER;
  const std::vector<AreaOfInterest> UFOV{AreaOfInterest::EGO_FRONT, AreaOfInterest::LEFT_FRONT, AreaOfInterest::RIGHT_FRONT};
  const std::vector<AreaOfInterest> inconsistentAois{AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_REAR};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetUfov()).WillByDefault(Return(UFOV));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, MarkInconsistent(AreaOfInterest::EGO_FRONT)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, MarkInconsistent(AreaOfInterest::EGO_REAR)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ClearInconsistency(_)).Times(0);

  constexpr bool TRIGGER_INFORMATION_ACQUISITION_OFF = false;

  TEST_HELPER.informationAcquisition.UpdateInconsistencies(TRIGGER_INFORMATION_ACQUISITION_OFF, inconsistentAois);
}

TEST(InformationAcquisition_UpdateInconsistencies, IfTriggerInformationAcquisitionTrue_RemoveUfovAoisThatAreNoInconsistent)
{
  InformationAcquisitionTester TEST_HELPER;

  const std::vector<AreaOfInterest> UFOV{AreaOfInterest::EGO_FRONT, AreaOfInterest::LEFT_FRONT, AreaOfInterest::RIGHT_FRONT};
  const std::vector<AreaOfInterest> inconsistentAois{AreaOfInterest::EGO_FRONT, AreaOfInterest::EGO_REAR};

  ON_CALL(TEST_HELPER.fakeMentalModel, GetUfov()).WillByDefault(Return(UFOV));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, MarkInconsistent(AreaOfInterest::EGO_FRONT)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, MarkInconsistent(AreaOfInterest::EGO_REAR)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ClearInconsistency(AreaOfInterest::LEFT_FRONT)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ClearInconsistency(AreaOfInterest::RIGHT_FRONT)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ClearInconsistency(AreaOfInterest::EGO_FRONT)).Times(0);

  constexpr bool TRIGGER_INFORMATION_ACQUISITION_ON = true;

  TEST_HELPER.informationAcquisition.UpdateInconsistencies(TRIGGER_INFORMATION_ACQUISITION_ON, inconsistentAois);
}