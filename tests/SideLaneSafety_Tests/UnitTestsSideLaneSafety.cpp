/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock-actions.h>
#include <gmock/gmock-nice-strict.h>
#include <gmock/gmock.h>
#include <gtest/gtest-typed-test.h>
#include <gtest/gtest.h>

#include <memory>

#include "../Fakes/FakeInfrastructureCharacteristics.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeSideLaneSafetyCalculations.h"
#include "../Fakes/FakeSideLaneSafetyQuery.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSurroundingVehicleQuery.h"
#include "SideLaneSafety/SideLaneSafety.h"
#include "include/common/AreaOfInterest.h"
#include "include/common/ScmDefinitions.h"
#include "include/common/ScmEnums.h"

using ::testing::_;
using ::testing::Return;

/**********************************
 * CHECK CanFinishLaneChangeSafely *
 ***********************************/

struct DataFor_CanFinishLaneChangeSafely
{
  units::length::meter_t laneChangeLength;
  RelativeLane targetLane;
  bool frontSafe;
  bool rearSafe;
};

class Test_CanFinishLaneChangeSafely : public ::testing::Test, public ::testing::WithParamInterface<DataFor_CanFinishLaneChangeSafely>
{
};

TEST_P(Test_CanFinishLaneChangeSafely, ReturnsTrueIfSideLaneSafetyCalculationsReturnsTrueForFrontAndRear)
{
  const auto param = GetParam();

  auto fakeCalculations = std::make_shared<FakeSideLaneSafetyCalculations>();
  auto fakeQuery = std::make_shared<FakeSideLaneSafetyQuery>();
  FakeSurroundingVehicleQueryFactory fakeVehicleQueryFactory;

  const units::time::second_t FAKE_LANE_CHANGE_DURATION{123.4_s};
  const FakeSurroundingVehicle FRONT_VEHICLE;
  const FakeSurroundingVehicle REAR_VEHICLE;
  const SideLaneSafetyTypes::TargetLaneVehicleInformation FRONT_DATA{678_m, &FRONT_VEHICLE};
  const SideLaneSafetyTypes::TargetLaneVehicleInformation REAR_DATA{910_m, &REAR_VEHICLE};
  const SideLaneSafetyTypes::LaneSafetyParameters FAKE_PARAMETERS{
      FAKE_LANE_CHANGE_DURATION,
      FRONT_DATA,
      REAR_DATA};
  EXPECT_CALL(*fakeQuery, GetLaneChangeSafetyParameters(param.laneChangeLength, param.targetLane)).Times(1).WillOnce(Return(FAKE_PARAMETERS));
  ON_CALL(*fakeCalculations, CanFinishLaneChangeWithoutEnteringMinDistance(FAKE_LANE_CHANGE_DURATION, FAKE_PARAMETERS.front, _)).WillByDefault(Return(param.frontSafe));
  ON_CALL(*fakeCalculations, CanFinishLaneChangeWithoutEnteringMinDistance(FAKE_LANE_CHANGE_DURATION, FAKE_PARAMETERS.rear, _)).WillByDefault(Return(param.rearSafe));

  SideLaneSafety sideLaneSafety(fakeCalculations, fakeQuery, fakeVehicleQueryFactory);
  const auto result = sideLaneSafety.CanFinishLaneChangeSafely(param.laneChangeLength, param.targetLane);

  ASSERT_EQ(result, param.frontSafe && param.rearSafe);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    Test_CanFinishLaneChangeSafely,
    ::testing::Values(
        DataFor_CanFinishLaneChangeSafely{
            .laneChangeLength = 100.0_m,
            .targetLane = RelativeLane::LEFT,
            .frontSafe = false,
            .rearSafe = false},
        DataFor_CanFinishLaneChangeSafely{
            .laneChangeLength = 100.0_m,
            .targetLane = RelativeLane::RIGHT,
            .frontSafe = true,
            .rearSafe = false},
        DataFor_CanFinishLaneChangeSafely{
            .laneChangeLength = 100.0_m,
            .targetLane = RelativeLane::LEFT,
            .frontSafe = false,
            .rearSafe = true},
        DataFor_CanFinishLaneChangeSafely{
            .laneChangeLength = 100.0_m,
            .targetLane = RelativeLane::RIGHT,
            .frontSafe = true,
            .rearSafe = true}));