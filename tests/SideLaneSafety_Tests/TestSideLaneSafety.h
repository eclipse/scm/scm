/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <memory>

#include "SideLaneSafety/SideLaneSafety.h"
#include "unitTests/components/LaneChangeTrajectoryCalculations_Tests/FakeLaneChangeTrajectoryCalculations.h"
#include "unitTests/components/SideLaneSafety_Tests/FakeSideLaneSafetyQuery.h"
class TestSideLaneSafety : public SideLaneSafety
{
public:
  TestSideLaneSafety(MentalModelInterface& mentalModel, const MentalCalculationsInterface& mentalCalculations, const SurroundingVehicleQueryFactoryInterface& surroundingVehicleQueryFactory)
      : SideLaneSafety{mentalModel,
                       mentalCalculations,
                       surroundingVehicleQueryFactory}

  {
  }
  void SetLaneChangeTrajectoryCalculations(std::shared_ptr<FakeLaneChangeTrajectoryCalculations>& laneChangeTrajectoryCalculations)
  {
    _laneChangeTrajectoryCalculations = laneChangeTrajectoryCalculations;
  }

  void SetSideLaneSafetyQuery(std::shared_ptr<FakeSideLaneSafetyQuery>& sideLaneSafetyQuery)
  {
    _sideLaneSafetyQuery = sideLaneSafetyQuery;
  }
};