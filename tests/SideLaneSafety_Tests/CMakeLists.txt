################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_TEST_NAME SideLaneSaftey_Tests)
set(DRIVER_DIR ${ROOT_DIR}/module/driver)
set(PARAMETER_DIR ${ROOT_DIR}/module/parameterParser)
set(SENSOR_DIR ${ROOT_DIR}/module/sensor)

file(GLOB
    SRC_SUR_VEH
    CONFIGURE_DEPENDS
    FOLLOW_SYMLINKS false
    LIST_DIRECTORIES false
    "${DRIVER_DIR}/src/SurroundingVehicles/*.cpp")

file(GLOB
    HEADER_SUR_VEH
    CONFIGURE_DEPENDS
    FOLLOW_SYMLINKS false
    LIST_DIRECTORIES false
    "${DRIVER_DIR}/src/SurroundingVehicles/*.h")

add_scm_target(
    NAME ${COMPONENT_TEST_NAME} TYPE test COMPONENT core
    DEFAULT_MAIN
    LINKOSI

  SOURCES
    ${SRC_SUR_VEH}
    UnitTestsSideLaneSafety.cpp
    UnitTestsSideLaneSafetyCalculations.cpp
    UnitTestsSideLaneSafetyQuery.cpp
    ${DRIVER_DIR}/src/SurroundingVehicles/LeadingVehicleQuery.cpp
    ${DRIVER_DIR}/src/LaneQueryHelper.cpp
    ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafety.cpp
    ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafetyCalculations.cpp
    ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafetyQuery.cpp
    ${DRIVER_DIR}/src/LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculations.cpp
    ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehiclesModifier.cpp
    ${DRIVER_DIR}/src/Reliability.cpp
    ${DRIVER_DIR}/src/ScmCommons.cpp

  HEADERS
    TestSideLaneSafety.h # MERGE-TODO
    ../Fakes/FakeSideLaneSafetyCalculations.h
    ../Fakes/FakeSideLaneSafetyQuery.h
    ${DRIVER_DIR}/src/LaneQueryHelper.h
    ${DRIVER_DIR}/src/ScmCommons.h
    ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehicleInterface.h
    ${DRIVER_DIR}/src/SurroundingVehicles/LeadingVehicleQuery.h
    ${DRIVER_DIR}/src/SurroundingVehicles/SurroundingVehiclesModifier.h
    ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafety.h
    ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafetyCalculations.h
    ${DRIVER_DIR}/src/SideLaneSafety/SideLaneSafetyQuery.h
    ${DRIVER_DIR}/src/LaneChangeTrajectoryCalculations/LaneChangeTrajectoryCalculations.h
    ${DRIVER_DIR}/src/Reliability.h

    INCDIRS
    ${DRIVER_DIR}
    ${DRIVER_DIR}/src
    ${DRIVER_DIR}/src/HighCognitive
    ${PARAMETER_DIR}/src
    ${PARAMETER_DIR}/src/Signals
    ${SENSOR_DIR}/src/Signals
    ${ROOT_DIR}/include/signal

    LIBRARIES
    Stochastics::Stochastics
)