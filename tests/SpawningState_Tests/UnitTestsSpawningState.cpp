/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Fakes/FakeActionManager.h"
#include "Fakes/FakeGazeControl.h"
#include "Fakes/FakeIgnoringOuterLaneOvertakingProhibition.h"
#include "Fakes/FakeInformationAcquisition.h"
#include "Fakes/FakeMentalModel.h"
#include "Fakes/FakeMicroscopicCharacteristics.h"
#include "Fakes/FakePeriodicLogger.h"
#include "Fakes/FakeScmComponents.h"
#include "Fakes/FakeScmDependencies.h"
#include "Fakes/FakeSituationManager.h"
#include "Fakes/FakeStochastics.h"
#include "Fakes/FakeSwerving.h"
#include "Fakes/FakeZipMerging.h"
#include "SpawningState.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

struct SpawningStateTester
{
  class SpawningStateUnderTest : SpawningState
  {
  public:
    template <typename... Args>
    SpawningStateUnderTest(Args&&... args)
        : SpawningState{std::forward<Args>(args)...} {};

    using SpawningState::DetermineAgentStates;
    using SpawningState::SetInitialParameters;
    using SpawningState::UpdateMicroscopicData;

    void SET_NEW_EGO_LANE(bool isNewEgoLane)
    {
      _isNewEgoLane = isNewEgoLane;
    }

    void SET_EXTERNAL_CONTROL(ExternalControlState externalControlState)
    {
      _externalControlState = externalControlState;
    }
  };

  SpawningStateTester()
      : fakeScmComponents{},
        fakeMentalModel{},
        fakePeriodicLogger{},
        _externalControlState{},
        spawningState(componentName,
                      cycleTime,
                      fakePeriodicLogger,
                      fakeScmComponents,
                      _externalControlState)
  {
  }
  std::string componentName = "abc";
  units::time::millisecond_t cycleTime = 100_ms;
  ExternalControlState _externalControlState;
  SpawningStateUnderTest spawningState;
  NiceMock<FakePeriodicLogger>* fakePeriodicLogger;
  NiceMock<FakeScmComponents> fakeScmComponents;
  NiceMock<FakeMentalModel> fakeMentalModel;
};

/******************************
 * CHECK DetermineAgentStates *
 ******************************/

TEST(SpawningState_DetermineAgentStates, Check_DetermineAgentStates)
{
  SpawningStateTester TEST_HELPER;
  NiceMock<FakeSwerving> fakeSwerving;
  NiceMock<FakeActionManager> fakeActionManager;
  NiceMock<FakeSituationManager> fakeSituationManager;
  NiceMock<FakeZipMerging> fakeZipMerging;

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetRemainInRealignState(false)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetRemainInLaneChangeState(false)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetRemainInSwervingState(false)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetLaneChangeWidthTraveled()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetIsEndOfLateralMovement(true)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetTransitionState()).Times(1);

  EXPECT_CALL(fakeSwerving, SetSwervingState(SwervingState::NONE)).Times(1);
  EXPECT_CALL(fakeSwerving, ResetAgentIdOfSwervingTarget()).Times(1);

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetMergeRegulate()).Times(2);
  EXPECT_CALL(fakeActionManager, DetermineLongitudinalActionState()).Times(3);
  EXPECT_CALL(fakeActionManager, RunLeadingVehicleSelector()).Times(3);

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetLateralAction(_)).Times(1);

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, TriggerHighCognitive(_)).Times(1);
  EXPECT_CALL(fakeSituationManager, GenerateIntensitiesAndSampleSituation(_)).Times(1);

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, DrawLaneChangeProhibitionIgnored()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, DrawShoulderLaneUsage()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, DrawDistractionUsage()).Times(1);

  EXPECT_CALL(fakeActionManager, GenerateIntensityAndSampleLateralAction()).Times(1);

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetReactionBaseTime()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetAdjustmentBaseTime(0.0_s)).Times(1);

  ON_CALL(TEST_HELPER.fakeScmComponents, GetZipMerging()).WillByDefault(Return(&fakeZipMerging));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetSituationManager()).WillByDefault(Return(&fakeSituationManager));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetActionManager()).WillByDefault(Return(&fakeActionManager));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetSwerving()).WillByDefault(Return(&fakeSwerving));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  TEST_HELPER.spawningState.DetermineAgentStates();
}

/******************************
 * CHECK SetInitialParameters *
 ******************************/

TEST(SpawningState_SetInitialParameters, Check_SetInitialParameters)
{
  SpawningStateTester TEST_HELPER;
  NiceMock<FakeScmDependencies> fakeScmDependencies;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeStochastics> fakeStaticStochastics;
  NiceMock<FakeGazeControl> fakeGazeControl;

  EXPECT_CALL(fakeScmDependencies, SetSpawnTime(_)).Times(1);

  OwnVehicleInformationScmExtended agentEgo;
  agentEgo.id = 0;
  agentEgo.distributionForHighwayExit = 0;

  ON_CALL(fakeMicroscopicCharacteristics, UpdateOwnVehicleData()).WillByDefault(Return(&agentEgo));
  ON_CALL(fakeStaticStochastics, GetUniformDistributed(-0.2, 0.2)).WillByDefault(Return(0.1));

  ObjectInformationSCM surroundingVehicleLeft, surroundingVehicleSide;
  surroundingVehicleLeft.id = 1;
  surroundingVehicleSide.id = 2;
  std::vector<ObjectInformationSCM> surroundingVehiclesLeft{surroundingVehicleLeft};
  std::vector<ObjectInformationSCM> surroundingVehiclesSide{surroundingVehicleSide};

  SurroundingObjectsSCM surroundingObjects;
  surroundingObjects.ongoingTraffic.Ahead().Left() = surroundingVehiclesLeft;
  surroundingObjects.ongoingTraffic.Close().Right() = surroundingVehiclesSide;
  ON_CALL(fakeScmDependencies, GetSurroundingObjectsScm()).WillByDefault(Return(&surroundingObjects));

  std::vector<int> knownIds{1, 2};
  EXPECT_CALL(fakeGazeControl, SetKnownObjectIds(knownIds)).Times(1);

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetTime(100_ms)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, AdvanceReactionBaseTime()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, AdvanceAdjustmentBaseTime()).Times(1);

  ON_CALL(TEST_HELPER.fakeMentalModel, GetTime()).WillByDefault(Return(100_ms));
  ON_CALL(fakeScmDependencies, GetSpawnTime).WillByDefault(Return(0_ms));

  OwnVehicleInformationSCM ownVehicleInformation;
  ownVehicleInformation.longitudinalVelocity = 10.0_mps;
  ownVehicleInformation.longitudinalPosition = 100.0_m;
  ON_CALL(fakeScmDependencies, GetOwnVehicleInformationScm()).WillByDefault(Return(&ownVehicleInformation));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetLaneChangePreventionAtSpawn(true)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetUrgentUpdateThreshold(_)).Times(2);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneChangePreventionAtSpawn()).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetLaneChangePreventionExternalControl()).WillByDefault(Return(true));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetLaneChangePreventionExternalControl(false)).Times(1);

  ON_CALL(TEST_HELPER.fakeScmComponents, GetGazeControl()).WillByDefault(Return(&fakeGazeControl));
  ON_CALL(fakeScmDependencies, GetStochastics()).WillByDefault(Return(&fakeStaticStochastics));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetScmDependencies()).WillByDefault(Return(&fakeScmDependencies));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));

  TEST_HELPER.spawningState.SetInitialParameters(100_ms);
}

/*******************************
 * CHECK UpdateMicroscopicData *
 *******************************/

TEST(SpawningState_UpdateMicroscopicData, Check_UpdateMicroscopicData)
{
  SpawningStateTester TEST_HELPER;
  NiceMock<FakeInformationAcquisition> fakeInformationAcqisition;
  NiceMock<FakeGazeControl> fakeGazeControl;
  NiceMock<FakeScmDependencies> fakeScmDependencies;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;
  NiceMock<FakeIgnoringOuterLaneOvertakingProhibition> fakeIgnoringOuterLaneOvertakingProhibition;

  EXPECT_CALL(fakeGazeControl, IsNewInformationAcquisitionRequested()).Times(1).WillOnce(Return(true));
  EXPECT_CALL(fakeInformationAcqisition, UpdateEgoVehicleData(true)).Times(1);
  EXPECT_CALL(fakeInformationAcqisition, UpdateSurroundingVehicleData(true, _)).Times(1);

  DriverParameters driverParameters;
  driverParameters.idealPerception = false;
  ON_CALL(fakeScmDependencies, GetDriverParameters()).WillByDefault(Return(driverParameters));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, UpdateReliabilityMap()).Times(1);

  TrafficRulesScm trafficRules;
  trafficRules.common.rightHandTraffic.value = true;
  ON_CALL(fakeScmDependencies, GetTrafficRulesScm()).WillByDefault(Return(&trafficRules));

  SurroundingObjectsScmExtended surroundingObjects;
  ObjectInformationScmExtended vehicle;
  vehicle.id = 1;
  std::vector<ObjectInformationScmExtended> vehiclesLeft{vehicle};
  std::vector<ObjectInformationScmExtended> vehicles{vehicle};
  surroundingObjects.Ahead().Left() = vehiclesLeft;
  surroundingObjects.Ahead().Close() = vehicles;

  EXPECT_CALL(fakeIgnoringOuterLaneOvertakingProhibition, Update(_, _)).Times(1);
  ON_CALL(fakeMicroscopicCharacteristics, GetSurroundingVehicleInformation()).WillByDefault(Return(&surroundingObjects));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&fakeMicroscopicCharacteristics));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetMentalModel()).WillByDefault(Return(&TEST_HELPER.fakeMentalModel));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetIgnoringOuterLaneOvertakingProhibition()).WillByDefault(Return(&fakeIgnoringOuterLaneOvertakingProhibition));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetGazeControl()).WillByDefault(Return(&fakeGazeControl));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetInformationAcquisition()).WillByDefault(Return(&fakeInformationAcqisition));
  ON_CALL(TEST_HELPER.fakeScmComponents, GetScmDependencies()).WillByDefault(Return(&fakeScmDependencies));

  TEST_HELPER.spawningState.UpdateMicroscopicData();
}
