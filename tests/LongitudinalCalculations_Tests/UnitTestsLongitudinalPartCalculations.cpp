/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "LongitudinalCalculations/LongitudinalPartCalculations.h"
#include "ScmCommons.h"
#include "include/common/AreaOfInterest.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

struct LongitudinalPartCalculationsTester
{
  class LongitudinalPartCalculationsUnderTest : LongitudinalPartCalculations
  {
  public:
    template <typename... Args>
    LongitudinalPartCalculationsUnderTest(Args&&... args)
        : LongitudinalPartCalculations{std::forward<Args>(args)...} {};

    using LongitudinalPartCalculations::CalculateInsecurityDistance;
    using LongitudinalPartCalculations::CalculateNetDistanceBetweenObjects;
    using LongitudinalPartCalculations::CalculateVelocityDelta;
    using LongitudinalPartCalculations::DetermineScalingFactorForFollowingDistance;
    using LongitudinalPartCalculations::DetermineSecureDistanceData;
    using LongitudinalPartCalculations::DetermineTtcBasedScalingFactorForFollowingDistance;
    using LongitudinalPartCalculations::ExecuteStevensPowLaw;
  };

  LongitudinalPartCalculationsTester()
      : fakeMentalModel{},
        longitudinalPartCalculations(fakeMentalModel)
  {
  }

  NiceMock<FakeMentalModel> fakeMentalModel;

  LongitudinalPartCalculationsUnderTest longitudinalPartCalculations;
};

/********************************************
 * CHECK DetermineSecureDistanceDataWithAoi *
 ********************************************/

struct DataFor_DetermineSecureDistanceData
{
  AreaOfInterest input_Aoi;
  AreaOfInterest input_ReferenceAoi;
  bool input_IsObjectInRearArea;
  units::velocity::meters_per_second_t result_VelocityFront;
  units::velocity::meters_per_second_t result_VelocityRear;
  units::acceleration::meters_per_second_squared_t result_AccelerationRear;
};

class LongitudinalPartCalculations_DetermineSecureDistanceDataWithAoi : public ::testing::Test,
                                                                        public ::testing::WithParamInterface<DataFor_DetermineSecureDistanceData>
{
};

TEST_P(LongitudinalPartCalculations_DetermineSecureDistanceDataWithAoi, Calculate_SecureDistanceData)
{
  DataFor_DetermineSecureDistanceData data = GetParam();
  LongitudinalPartCalculationsTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, IsObjectInRearArea(_, _)).WillByDefault(Return(data.input_IsObjectInRearArea));

  NiceMock<FakeSurroundingVehicle> observedVehicle;
  ON_CALL(observedVehicle, GetLongitudinalVelocity()).WillByDefault(Return(1.0_mps));
  ON_CALL(observedVehicle, GetLongitudinalAcceleration()).WillByDefault(Return(4.0_mps_sq));
  ON_CALL(observedVehicle, GetAssignedAoi()).WillByDefault(Return(data.input_Aoi));

  NiceMock<FakeSurroundingVehicle> referenceVehicle;
  ON_CALL(referenceVehicle, GetLongitudinalVelocity()).WillByDefault(Return(3.0_mps));
  ON_CALL(referenceVehicle, GetLongitudinalAcceleration()).WillByDefault(Return(4.0_mps_sq));
  ON_CALL(referenceVehicle, GetAssignedAoi()).WillByDefault(Return(data.input_ReferenceAoi));

  auto result = TEST_HELPER.longitudinalPartCalculations.DetermineSecureDistanceData(observedVehicle, referenceVehicle, units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE));

  ASSERT_EQ(data.result_VelocityFront, result.velocityFront);
  ASSERT_EQ(data.result_VelocityRear, result.velocityRear);
  ASSERT_EQ(data.result_AccelerationRear, result.accelerationRear);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LongitudinalPartCalculations_DetermineSecureDistanceDataWithAoi,
    testing::Values(
        DataFor_DetermineSecureDistanceData{
            .input_Aoi = AreaOfInterest::EGO_FRONT,
            .input_ReferenceAoi = AreaOfInterest::NumberOfAreaOfInterests,
            .input_IsObjectInRearArea = false,
            .result_VelocityFront = 1.0_mps,
            .result_VelocityRear = 3.0_mps,
            .result_AccelerationRear = 4.0_mps_sq},
        DataFor_DetermineSecureDistanceData{
            .input_Aoi = AreaOfInterest::EGO_FRONT,
            .input_ReferenceAoi = AreaOfInterest::EGO_REAR,
            .input_IsObjectInRearArea = true,
            .result_VelocityFront = 3.0_mps,
            .result_VelocityRear = 1.0_mps,
            .result_AccelerationRear = 4.0_mps_sq}));

/************************************************
 * CHECK DetermineSecureDistanceDataWithVehicle *
 ************************************************/

struct DataFor_DetermineSecureDistanceDataWithVehicle
{
  MinThwPerspective input_Perspective;
  AreaOfInterest input_aoi;
  units::velocity::meters_per_second_t result_VelocityFront;
  units::length::meter_t result_VehicleLengthFront;
  units::velocity::meters_per_second_t result_VelocityRear;
  units::acceleration::meters_per_second_squared_t result_AccelerationRear;
};

class LongitudinalPartCalculations_DetermineSecureDistanceDataWithVehicle : public ::testing::Test,
                                                                            public ::testing::WithParamInterface<DataFor_DetermineSecureDistanceDataWithVehicle>
{
};

TEST_P(LongitudinalPartCalculations_DetermineSecureDistanceDataWithVehicle, Calculate_SecureDistanceDataWithVehicle)
{
  DataFor_DetermineSecureDistanceDataWithVehicle data = GetParam();
  LongitudinalPartCalculationsTester TEST_HELPER;

  NiceMock<FakeSurroundingVehicle> observedVehicle;
  ON_CALL(observedVehicle, GetId()).WillByDefault(Return(0));
  ON_CALL(observedVehicle, GetLongitudinalVelocity()).WillByDefault(Return(1.0_mps));
  ON_CALL(observedVehicle, GetLongitudinalAcceleration()).WillByDefault(Return(4.0_mps_sq));
  ON_CALL(observedVehicle, GetAssignedAoi()).WillByDefault(Return(data.input_aoi));

  OwnVehicleInformationScmExtended egoVehicle;
  egoVehicle.longitudinalVelocity = 3.0_mps;
  egoVehicle.acceleration = 4.0_mps_sq;

  auto result = TEST_HELPER.longitudinalPartCalculations.DetermineSecureDistanceData(observedVehicle, egoVehicle, data.input_Perspective, units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE));

  ASSERT_EQ(data.result_VelocityFront, result.velocityFront);
  ASSERT_EQ(data.result_VelocityRear, result.velocityRear);
  ASSERT_EQ(data.result_AccelerationRear, result.accelerationRear);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LongitudinalPartCalculations_DetermineSecureDistanceDataWithVehicle,
    testing::Values(
        DataFor_DetermineSecureDistanceDataWithVehicle{
            .input_Perspective = MinThwPerspective::NORM,
            .input_aoi = AreaOfInterest::EGO_FRONT,
            .result_VelocityFront = 1.0_mps,
            .result_VehicleLengthFront = 2.0_m,
            .result_VelocityRear = 3.0_mps,
            .result_AccelerationRear = 4.0_mps_sq},
        DataFor_DetermineSecureDistanceDataWithVehicle{
            .input_Perspective = MinThwPerspective::NORM,
            .input_aoi = AreaOfInterest::EGO_REAR,
            .result_VelocityFront = 3.0_mps,
            .result_VehicleLengthFront = 2.0_m,
            .result_VelocityRear = 1.0_mps,
            .result_AccelerationRear = 4.0_mps_sq},
        DataFor_DetermineSecureDistanceDataWithVehicle{
            .input_Perspective = MinThwPerspective::EGO_ANTICIPATED_REAR,
            .input_aoi = AreaOfInterest::EGO_REAR,
            .result_VelocityFront = 1.0_mps,
            .result_VehicleLengthFront = 2.0_m,
            .result_VelocityRear = 3.0_mps,
            .result_AccelerationRear = 4.0_mps_sq}));

/************************************************
 * CHECK DetermineSecureDistanceDataWithVehicle *
 ************************************************/

TEST(LongitudinalPartCalculations_ExecuteStevensPowLaw, ZeroScalingFactor_LeadsToBaseDeltaDistance)
{
  LongitudinalPartCalculationsTester TEST_HELPER;
  auto baseSecureDistance = 50.0_m;
  double scalingFactorFollowingDistance = 0.0;
  double proportionalityFactorForFollowingDistance = 2.0;
  const auto result = TEST_HELPER.longitudinalPartCalculations.ExecuteStevensPowLaw(baseSecureDistance, scalingFactorFollowingDistance, proportionalityFactorForFollowingDistance);
  ASSERT_EQ(result, baseSecureDistance);
}

TEST(LongitudinalPartCalculations_ExecuteStevensPowLaw, LeadsToMisjudgedSecureDistance)
{
  LongitudinalPartCalculationsTester TEST_HELPER;
  auto baseSecureDistance = 3.0_m;
  double scalingFactorFollowingDistance = 1.0;
  double proportionalityFactorForFollowingDistance = 2.0;
  const auto result = TEST_HELPER.longitudinalPartCalculations.ExecuteStevensPowLaw(baseSecureDistance, scalingFactorFollowingDistance, proportionalityFactorForFollowingDistance);
  ASSERT_LE(result, baseSecureDistance);
}

TEST(LongitudinalPartCalculations_ExecuteStevensPowLaw, CannotEnlargeBaseSecureDistance)
{
  LongitudinalPartCalculationsTester TEST_HELPER;
  auto baseSecureDistance = 1.0_m;
  double scalingFactorFollowingDistance = 2.0;
  double proportionalityFactorForFollowingDistance = 3.0;
  const auto result = TEST_HELPER.longitudinalPartCalculations.ExecuteStevensPowLaw(baseSecureDistance, scalingFactorFollowingDistance, proportionalityFactorForFollowingDistance);
  ASSERT_LE(result, baseSecureDistance);
}

/************************************************************
 * CHECK DetermineTtcBasedScalingFactorForFollowingDistance *
 ************************************************************/

struct DataForDetermineScalingFactorForFollowingDistance
{
  std::string description;
  units::time::second_t ttc;
  double expected_result;
};

class LongitudinalPartCalculations_DetermineTtcBasedScalingFactorForFollowingDistance : public ::testing::Test,
                                                                                        public ::testing::WithParamInterface<DataForDetermineScalingFactorForFollowingDistance>
{
};

TEST_P(LongitudinalPartCalculations_DetermineTtcBasedScalingFactorForFollowingDistance, CheckFunction_DetermineScalingFactorForFollowingDistance)
{
  DataForDetermineScalingFactorForFollowingDistance data = GetParam();

  LongitudinalPartCalculationsTester TEST_HELPER;
  const double result = TEST_HELPER.longitudinalPartCalculations.DetermineTtcBasedScalingFactorForFollowingDistance(data.ttc);

  ASSERT_NEAR(result, data.expected_result, 0.0001);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LongitudinalPartCalculations_DetermineTtcBasedScalingFactorForFollowingDistance,
    testing::Values(
        DataForDetermineScalingFactorForFollowingDistance{
            .description = "Testing Limits - negative ttc means moving away so no scaling is needed",
            .ttc = -1.0_s,
            .expected_result = 1.0},
        DataForDetermineScalingFactorForFollowingDistance{
            .description = "Testing Limits - ttc at its limit means effectively that there is no valid ttc so no scaling is needed",
            .ttc = 99.0_s,
            .expected_result = 1.0},
        DataForDetermineScalingFactorForFollowingDistance{
            .description = "Testing Limits - minimum possible positive ttc means that there is basically already a collision, hence scaling factor is at 'max'(=0.0)",
            .ttc = 0.0_s,
            .expected_result = 0.0},
        DataForDetermineScalingFactorForFollowingDistance{
            .description = "Testing lower ttc limit - should also result in 'max' factor (=0.0)",
            .ttc = 6.0_s,
            .expected_result = 0.0},
        DataForDetermineScalingFactorForFollowingDistance{
            .description = "Some Valid number in between lower and upper ttc limits - result in [0, 1]",
            .ttc = 10.0_s,
            .expected_result = 0.2857},
        DataForDetermineScalingFactorForFollowingDistance{
            .description = "Some Valid number in between lower and upper ttc limits - result in [0, 1]",
            .ttc = 18.0_s,
            .expected_result = 0.8571}));

/*************************************
 * CHECK CalculateInsecurityDistance *
 *************************************/

struct DataForCalculateInsecurityDistance
{
  std::string description;
  units::time::millisecond_t time_of_last_perception;
  units::length::meter_t expected_result;
};

class LongitudinalPartCalculations_CalculateInsecurityDistance : public ::testing::Test,
                                                                 public ::testing::WithParamInterface<DataForCalculateInsecurityDistance>
{
};

TEST_P(LongitudinalPartCalculations_CalculateInsecurityDistance, CheckFunction_DetermineScalingFactorForFollowingDistance)
{
  DataForCalculateInsecurityDistance data = GetParam();

  LongitudinalPartCalculationsTester TEST_HELPER;
  NiceMock<FakeSurroundingVehicle> fakeObserved;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetTime()).WillByDefault(Return(1000.0_ms));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetUrgentUpdateThreshold()).WillByDefault(Return(1000.0_ms));

  ON_CALL(fakeObserved, GetTimeOfLastPerception()).WillByDefault(Return(data.time_of_last_perception));

  const auto result = TEST_HELPER.longitudinalPartCalculations.CalculateInsecurityDistance(fakeObserved);

  ASSERT_EQ(result, data.expected_result);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LongitudinalPartCalculations_CalculateInsecurityDistance,
    testing::Values(
        DataForCalculateInsecurityDistance{
            .description = "RegularTimeSinceLastUpdate_LeadsToModerateInsecurityDistance",
            .time_of_last_perception = 500.0_ms,
            .expected_result = 5.0_m},
        DataForCalculateInsecurityDistance{
            .description = "MaxTimeSinceLastUpdate_LeadsToMaxInsecurityDistance",
            .time_of_last_perception = 0.0_ms,
            .expected_result = 10.0_m},
        DataForCalculateInsecurityDistance{
            .description = "MinTimeSinceLastUpdate_LeadsToMinInsecurityDistance",
            .time_of_last_perception = 1000.0_ms,
            .expected_result = 0.0_m},
        DataForCalculateInsecurityDistance{
            .description = "IsUnseenForLonger_LeadsToHigherInsecurityDistance",
            .time_of_last_perception = 100.0_ms,
            .expected_result = 9.0_m}));

/*************************************
 * CHECK CalculateInsecurityDistance *
 *************************************/

struct DataForDetermineLongitudinalVelocityDeltaWithReference
{
  AreaOfInterest input_aoiReference;
  AreaOfInterest input_aoiObserved;
  units::velocity::meters_per_second_t input_vReference;
  units::velocity::meters_per_second_t input_vObserved;
  bool input_isRearArea;
  bool input_isSideArea;
  units::velocity::meters_per_second_t result_expectedResult;
};

class LongitudinalPartCalculations_DetermineLongitudinalVelocityDelta_WithReference : public ::testing::Test,
                                                                                      public ::testing::WithParamInterface<DataForDetermineLongitudinalVelocityDeltaWithReference>
{
};

TEST_P(LongitudinalPartCalculations_DetermineLongitudinalVelocityDelta_WithReference, CheckFunction_DetermineLongitudinalVelocityDelta_WithReference)
{
  DataForDetermineLongitudinalVelocityDeltaWithReference data = GetParam();
  LongitudinalPartCalculationsTester TEST_HELPER;

  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicleObserved;
  ON_CALL(fakeSurroundingVehicleObserved, GetLongitudinalVelocity).WillByDefault(Return(data.input_vObserved));
  ON_CALL(fakeSurroundingVehicleObserved, GetAssignedAoi).WillByDefault(Return(data.input_aoiObserved));

  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicleReference;
  ON_CALL(fakeSurroundingVehicleReference, GetLongitudinalVelocity).WillByDefault(Return(data.input_vReference));
  ON_CALL(fakeSurroundingVehicleReference, GetAssignedAoi).WillByDefault(Return(data.input_aoiReference));

  ON_CALL(TEST_HELPER.fakeMentalModel, IsObjectInRearArea(_, _)).WillByDefault(Return(data.input_isRearArea));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsObjectInSideArea(_, _)).WillByDefault(Return(data.input_isSideArea));

  // Call test
  if (data.input_isSideArea /*&& data.input_aoiReference != AreaOfInterest::NumberOfAreaOfInterests*/)
  {
    // Expect exception
    EXPECT_THROW(TEST_HELPER.longitudinalPartCalculations.CalculateVelocityDelta(fakeSurroundingVehicleObserved, fakeSurroundingVehicleReference), std::runtime_error);
  }

  else
  {
    // No exception
    const auto result = TEST_HELPER.longitudinalPartCalculations.CalculateVelocityDelta(fakeSurroundingVehicleObserved, fakeSurroundingVehicleReference);

    // Evaluate results
    ASSERT_EQ(result, data.result_expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    WithReference,
    LongitudinalPartCalculations_DetermineLongitudinalVelocityDelta_WithReference,
    testing::Values(
        // Observed is in front of Reference
        DataForDetermineLongitudinalVelocityDeltaWithReference{
            .input_aoiReference = AreaOfInterest::LEFT_FRONT,
            .input_aoiObserved = AreaOfInterest::EGO_FRONT,
            .input_vReference = 25.0_mps,
            .input_vObserved = 30.0_mps,
            .input_isRearArea = false,
            .input_isSideArea = false,
            .result_expectedResult = 5.0_mps},
        // Observed is in side of Reference -> runtime_error
        DataForDetermineLongitudinalVelocityDeltaWithReference{
            .input_aoiReference = AreaOfInterest::RIGHT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFT_SIDE,
            .input_vReference = 15.0_mps,
            .input_vObserved = 5.0_mps,
            .input_isRearArea = false,
            .input_isSideArea = true,
            .result_expectedResult = 0.0_mps},
        // Observed is in rear of Reference
        DataForDetermineLongitudinalVelocityDeltaWithReference{
            .input_aoiReference = AreaOfInterest::EGO_REAR,
            .input_aoiObserved = AreaOfInterest::RIGHT_REAR,
            .input_vReference = 25.0_mps,
            .input_vObserved = 30.0_mps,
            .input_isRearArea = true,
            .input_isSideArea = false,
            .result_expectedResult = -5.0_mps}));

/********************************************************
 * CHECK DetermineLongitudinalNetDistanceBetweenObjects *
 ********************************************************/

struct DataForDetermineLongitudinalNetDistanceBetweenObjects
{
  AreaOfInterest input_aoiReference;
  AreaOfInterest input_aoiObserved;
  units::length::meter_t input_mLLReference;
  units::length::meter_t input_mLLObserved;
  bool input_IsObjectInRearArea;
  units::length::meter_t result_expectedResult;
};

class LongitudinalPartCalculations_DetermineLongitudinalNetDistanceBetweenObjects : public ::testing::Test,
                                                                                    public ::testing::WithParamInterface<DataForDetermineLongitudinalNetDistanceBetweenObjects>
{
};

TEST_P(LongitudinalPartCalculations_DetermineLongitudinalNetDistanceBetweenObjects, CheckFunction_DetermineLongitudinalNetDistanceBetweenObjects)
{
  // Get resources
  DataForDetermineLongitudinalNetDistanceBetweenObjects data = GetParam();
  LongitudinalPartCalculationsTester TEST_HELPER;

  NiceMock<FakeSurroundingVehicle> fakeObserved;
  ObstructionLongitudinal obstructionLongAoi;
  obstructionLongAoi.mainLaneLocator = data.input_mLLObserved;
  ON_CALL(fakeObserved, GetLongitudinalObstruction()).WillByDefault(Return(obstructionLongAoi));
  ON_CALL(fakeObserved, GetLength()).WillByDefault(Return(4.0_m));
  ON_CALL(fakeObserved, GetAssignedAoi()).WillByDefault(Return(data.input_aoiObserved));

  NiceMock<FakeSurroundingVehicle> fakeReference;
  ObstructionLongitudinal obstructionLongAoiReference;
  obstructionLongAoiReference.mainLaneLocator = data.input_mLLReference;
  ON_CALL(fakeReference, GetLongitudinalObstruction()).WillByDefault(Return(obstructionLongAoiReference));
  ON_CALL(fakeReference, GetLength()).WillByDefault(Return(4.0_m));
  ON_CALL(fakeReference, GetAssignedAoi()).WillByDefault(Return(data.input_aoiReference));
  ON_CALL(TEST_HELPER.fakeMentalModel, IsObjectInRearArea(data.input_aoiReference, data.input_aoiObserved)).WillByDefault(Return(data.input_IsObjectInRearArea));

  const auto result = TEST_HELPER.longitudinalPartCalculations.CalculateNetDistanceBetweenObjects(fakeObserved, fakeReference);

  // Evaluate results
  ASSERT_EQ(result, data.result_expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LongitudinalPartCalculations_DetermineLongitudinalNetDistanceBetweenObjects,
    testing::Values(
        // Reference is in front of ego
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::LEFT_FRONT,
            .input_aoiObserved = AreaOfInterest::RIGHT_FRONT_FAR,
            .input_mLLReference = 20.0_m,
            .input_mLLObserved = 30.0_m,
            .input_IsObjectInRearArea = false,
            .result_expectedResult = 6.0_m},  // 5
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::LEFT_FRONT,
            .input_aoiObserved = AreaOfInterest::EGO_FRONT,
            .input_mLLReference = 20.0_m,
            .input_mLLObserved = 22.0_m,
            .input_IsObjectInRearArea = false,
            .result_expectedResult = 2.0_m},  // 6
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::LEFT_FRONT,
            .input_aoiObserved = AreaOfInterest::LEFTLEFT_SIDE,
            .input_mLLReference = 20.0_m,
            .input_mLLObserved = 19.0_m,
            .input_IsObjectInRearArea = false,
            .result_expectedResult = 5.0_m},  // 7 (Not RearArea)
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::LEFT_FRONT,
            .input_aoiObserved = AreaOfInterest::LEFTLEFT_SIDE,
            .input_mLLReference = 20.0_m,
            .input_mLLObserved = 8.0_m,
            .input_IsObjectInRearArea = true,
            .result_expectedResult = 8.0_m},  // 8 (RearArea)
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::LEFT_FRONT,
            .input_aoiObserved = AreaOfInterest::EGO_REAR,
            .input_mLLReference = 20.0_m,
            .input_mLLObserved = -5.0_m,
            .input_IsObjectInRearArea = true,
            .result_expectedResult = 21.0_m},  // 9 (RearArea)
        // Reference is beside ego
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::LEFTLEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFT_FRONT_FAR,
            .input_mLLReference = -1.0_m,
            .input_mLLObserved = 5.0_m,
            .input_IsObjectInRearArea = false,
            .result_expectedResult = 2.0_m},  // 10
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::LEFTLEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFTLEFT_FRONT,
            .input_mLLReference = -1.0_m,
            .input_mLLObserved = 2.0_m,
            .input_IsObjectInRearArea = false,
            .result_expectedResult = 1.0_m},  // 11
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::LEFTLEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFT_SIDE,
            .input_mLLReference = -1.0_m,
            .input_mLLObserved = -3.0_m,
            .input_IsObjectInRearArea = false,
            .result_expectedResult = 6.0_m},  // 12 (Not RearArea)
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::LEFTLEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::RIGHT_REAR,
            .input_mLLReference = -1.0_m,
            .input_mLLObserved = -5.0_m,
            .input_IsObjectInRearArea = false,
            .result_expectedResult = 8.0_m},  // 13 (Not RearArea)
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::LEFTLEFT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFT_REAR,
            .input_mLLReference = -1.0_m,
            .input_mLLObserved = -7.0_m,
            .input_IsObjectInRearArea = true,
            .result_expectedResult = 2.0_m},  // 14 (RearArea)
        // Reference is behind ego
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::RIGHT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFTLEFT_FRONT,
            .input_mLLReference = -10.0_m,
            .input_mLLObserved = 3.0_m,
            .input_IsObjectInRearArea = false,
            .result_expectedResult = 9.0_m},  // 15
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::RIGHT_SIDE,
            .input_aoiObserved = AreaOfInterest::LEFT_FRONT_FAR,
            .input_mLLReference = -10.0_m,
            .input_mLLObserved = -1.0_m,
            .input_IsObjectInRearArea = false,
            .result_expectedResult = 5.0_m},  // 16
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::RIGHT_SIDE,
            .input_aoiObserved = AreaOfInterest::EGO_FRONT,
            .input_mLLReference = -10.0_m,
            .input_mLLObserved = -8.0_m,
            .input_IsObjectInRearArea = false,
            .result_expectedResult = 2.0_m},  // 17 (Not RearArea)
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::RIGHT_SIDE,
            .input_aoiObserved = AreaOfInterest::RIGHT_REAR,
            .input_mLLReference = -10.0_m,
            .input_mLLObserved = -13.0_m,
            .input_IsObjectInRearArea = false,
            .result_expectedResult = 7.0_m},  // 18 (Not RearArea)
        DataForDetermineLongitudinalNetDistanceBetweenObjects{
            .input_aoiReference = AreaOfInterest::RIGHT_SIDE,
            .input_aoiObserved = AreaOfInterest::RIGHTRIGHT_SIDE,
            .input_mLLReference = -10.0_m,
            .input_mLLObserved = -15.0_m,
            .input_IsObjectInRearArea = true,
            .result_expectedResult = 1.0_m}));  // 19 (RearArea)

/****************************************************
 * CHECK DetermineScalingFactorForFollowingDistance *
 ****************************************************/

TEST(LongitudinalPartCalculations_DetermineScalingFactorForFollowingDistance, CheckFunction_DetermineScalingFactorForFollowingDistance)
{
  LongitudinalPartCalculationsTester TEST_HELPER;
  NiceMock<FakeSurroundingVehicle> fakeVehicle;

  ON_CALL(fakeVehicle, GetTtc()).WillByDefault(Return(10.0_s));

  auto result = TEST_HELPER.longitudinalPartCalculations.DetermineScalingFactorForFollowingDistance(fakeVehicle);

  EXPECT_NEAR(result, 0.28, 0.01);
}