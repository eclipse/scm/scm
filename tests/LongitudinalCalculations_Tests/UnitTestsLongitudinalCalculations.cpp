/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "LongitudinalCalculations.h"
#include "module/driver/src/ScmCommons.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

/************************************
 * CHECK CalculateSecureNetDistance *
 ************************************/

TEST(LongitudinalCalculations_CalculateSecureNetDistance, LongitudinalDirection_CalculateSecureNetSurroundingVehicles)
{
  NiceMock<FakeMentalModel> fakeMentalModel;

  DriverParameters driverParameters;
  driverParameters.proportionalityFactorForFollowingDistance = 3.0;

  ON_CALL(fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ON_CALL(fakeMentalModel, GetTime()).WillByDefault(Return(11000_ms));
  ON_CALL(fakeMentalModel, GetUrgentUpdateThreshold()).WillByDefault(Return(1000.0_ms));
  ON_CALL(fakeMentalModel, GetTimeSinceLastUpdate(_)).WillByDefault(Return(10000.0_ms));

  const AreaOfInterest aoi = AreaOfInterest::RIGHTRIGHT_FRONT;
  const AreaOfInterest aoiReference = AreaOfInterest::EGO_REAR;
  const auto currentVehicleVel = 40._mps;
  const auto currentRefVel = 45._mps;
  const auto anticipAccFront = .01_mps_sq;
  const auto anticipVelEnd = 40._mps;
  const auto reactionTime = .2_s;
  const auto anticipAccRear = -5._mps_sq;
  const auto DeltaDistEnd = 20._m;
  const auto currentAccRear = 0._mps_sq;
  const bool isVehicleVisible = true;
  const auto ttc = 20._s;

  NiceMock<FakeSurroundingVehicle> observedVehicle;
  ON_CALL(observedVehicle, GetId).WillByDefault(Return(1));
  const ObstructionLongitudinal obstructionLongitudinalObserved(0.0_m, 0.0_m, 1.0_m);
  ON_CALL(observedVehicle, GetLongitudinalObstruction).WillByDefault(Return(obstructionLongitudinalObserved));
  ON_CALL(observedVehicle, GetLongitudinalVelocity).WillByDefault(Return(currentVehicleVel));
  ON_CALL(observedVehicle, GetAssignedAoi).WillByDefault(Return(aoi));
  ON_CALL(observedVehicle, GetLongitudinalAcceleration).WillByDefault(Return(currentAccRear));
  ON_CALL(observedVehicle, GetTtc).WillByDefault(Return(ttc));
  ON_CALL(observedVehicle, GetLength).WillByDefault(Return(5_m));

  NiceMock<FakeSurroundingVehicle> referenceVehicle;
  ON_CALL(referenceVehicle, GetId).WillByDefault(Return(2));
  const ObstructionLongitudinal obstructionLongitudinalReference(0.0_m, 0.0_m, 0.0_m);
  ON_CALL(referenceVehicle, GetLongitudinalObstruction).WillByDefault(Return(obstructionLongitudinalReference));
  ON_CALL(referenceVehicle, GetLongitudinalVelocity).WillByDefault(Return(currentRefVel));
  ON_CALL(referenceVehicle, GetAssignedAoi).WillByDefault(Return(aoiReference));
  ON_CALL(referenceVehicle, GetLongitudinalAcceleration).WillByDefault(Return(0.0_mps_sq));
  ON_CALL(referenceVehicle, GetLength).WillByDefault(Return(5_m));

  LongitudinalCalculations longitudinalCalculations(fakeMentalModel);
  auto result = longitudinalCalculations.CalculateSecureNetDistance(observedVehicle,
                                                                    referenceVehicle,
                                                                    anticipAccFront,
                                                                    anticipVelEnd,
                                                                    reactionTime,
                                                                    anticipAccRear,
                                                                    DeltaDistEnd,
                                                                    units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE));

  const double expected_secureLongitudinalDistance = 3. * std::pow(81.5, .25);
  ASSERT_DOUBLE_EQ(result.value(), expected_secureLongitudinalDistance);
}

/************************************
 * CHECK CalculateSecureNetDistance *
 ************************************/

struct DataFor_CalculateSecureNetDistance
{
  AreaOfInterest input_aoi;
  AreaOfInterest input_aoiReference;
  units::velocity::meters_per_second_t input_currentRefVel;
  units::acceleration::meters_per_second_squared_t input_anticipAccFront;
  units::velocity::meters_per_second_t input_anticipVelEnd;
  units::time::second_t input_reactionTime;
  units::acceleration::meters_per_second_squared_t input_anticipAccRear;
  units::length::meter_t input_DeltaDistEnd;
  MinThwPerspective input_perspective;
  units::velocity::meters_per_second_t currentVehicleVel;
  units::acceleration::meters_per_second_squared_t currentAccRear;
  bool isVehicleVisible;
  units::time::second_t ttc;
  units::length::meter_t expected_sercureLongitudinalDistance;
};

class LongitudinalCalculations_CalculateSecureNetDistance : public ::testing::Test,
                                                            public ::testing::WithParamInterface<DataFor_CalculateSecureNetDistance>
{
};

TEST_P(LongitudinalCalculations_CalculateSecureNetDistance, LongitudinalDirection_CalculateSecureNetDistanceEgoToOther)
{
  DataFor_CalculateSecureNetDistance data = GetParam();
  NiceMock<FakeMentalModel> fakeMentalModel;

  DriverParameters driverParameters;
  driverParameters.proportionalityFactorForFollowingDistance = 3.0;

  ON_CALL(fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ON_CALL(fakeMentalModel, GetTime()).WillByDefault(Return(11000_ms));
  ON_CALL(fakeMentalModel, GetUrgentUpdateThreshold()).WillByDefault(Return(1000.0_ms));
  ON_CALL(fakeMentalModel, GetTimeSinceLastUpdate(_)).WillByDefault(Return(10000.0_ms));

  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle;
  ON_CALL(fakeSurroundingVehicle, GetLongitudinalVelocity).WillByDefault(Return(data.currentVehicleVel));
  ON_CALL(fakeSurroundingVehicle, GetAssignedAoi).WillByDefault(Return(data.input_aoi));
  ON_CALL(fakeSurroundingVehicle, GetLongitudinalAcceleration).WillByDefault(Return(data.currentAccRear));
  ON_CALL(fakeSurroundingVehicle, GetTtc).WillByDefault(Return(data.ttc));

  OwnVehicleInformationScmExtended egoVehicle;
  egoVehicle.longitudinalVelocity = data.input_currentRefVel;
  egoVehicle.absoluteVelocity = data.input_currentRefVel;
  egoVehicle.acceleration = 0.0_mps_sq;

  LongitudinalCalculations longitudinalCalculations(fakeMentalModel);
  auto result = longitudinalCalculations.CalculateSecureNetDistance(fakeSurroundingVehicle,
                                                                    egoVehicle,
                                                                    data.input_anticipAccFront,
                                                                    data.input_anticipVelEnd,
                                                                    data.input_reactionTime,
                                                                    data.input_anticipAccRear,
                                                                    data.input_DeltaDistEnd,
                                                                    data.input_perspective,
                                                                    units::make_unit<units::velocity::meters_per_second_t>(ScmDefinitions::DEFAULT_VALUE));

  ASSERT_EQ(result, data.expected_sercureLongitudinalDistance);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    LongitudinalCalculations_CalculateSecureNetDistance,
    testing::Values(
        DataFor_CalculateSecureNetDistance{
            .input_aoi = AreaOfInterest::EGO_REAR,
            .input_currentRefVel = 40._mps,
            .input_anticipAccFront = .01_mps_sq,
            .input_anticipVelEnd = 40._mps,
            .input_reactionTime = .2_s,
            .input_anticipAccRear = -5._mps_sq,
            .input_DeltaDistEnd = 20._m,
            .input_perspective = MinThwPerspective::NORM,
            .currentVehicleVel = 45._mps,
            .currentAccRear = 0._mps_sq,
            .isVehicleVisible = true,
            .ttc = 20._s,
            .expected_sercureLongitudinalDistance = units::make_unit<units::length::meter_t>(3. * std::pow(81.5, .25))},
        DataFor_CalculateSecureNetDistance{
            .input_aoi = AreaOfInterest::EGO_FRONT,
            .input_currentRefVel = 45._mps,
            .input_anticipAccFront = .01_mps_sq,
            .input_anticipVelEnd = 40._mps,
            .input_reactionTime = .2_s,
            .input_anticipAccRear = -5._mps_sq,
            .input_DeltaDistEnd = 20._m,
            .input_perspective = MinThwPerspective::EGO_ANTICIPATED_REAR,
            .currentVehicleVel = 40._mps,
            .currentAccRear = 0._mps_sq,
            .isVehicleVisible = true,
            .ttc = 20._s,
            .expected_sercureLongitudinalDistance = units::make_unit<units::length::meter_t>(3. * std::pow(81.5, .25))},
        DataFor_CalculateSecureNetDistance{
            .input_aoi = AreaOfInterest::EGO_REAR,
            .input_currentRefVel = 0._mps,
            .input_anticipAccFront = .01_mps_sq,
            .input_anticipVelEnd = 0._mps,
            .input_reactionTime = .2_s,
            .input_anticipAccRear = -7._mps_sq,
            .input_DeltaDistEnd = 20._m,
            .input_perspective = MinThwPerspective::NORM,
            .currentVehicleVel = 35._mps,
            .currentAccRear = 0._mps_sq,
            .isVehicleVisible = true,
            .ttc = 5._s,
            .expected_sercureLongitudinalDistance = 124.5_m},
        DataFor_CalculateSecureNetDistance{
            .input_aoi = AreaOfInterest::EGO_FRONT,
            .input_currentRefVel = 35._mps,
            .input_anticipAccFront = .01_mps_sq,
            .input_anticipVelEnd = 0._mps,
            .input_reactionTime = .2_s,
            .input_anticipAccRear = -7._mps_sq,
            .input_DeltaDistEnd = 20._m,
            .input_perspective = MinThwPerspective::EGO_ANTICIPATED_REAR,
            .currentVehicleVel = 0._mps,
            .currentAccRear = 0._mps_sq,
            .isVehicleVisible = true,
            .ttc = 5._s,
            .expected_sercureLongitudinalDistance = 124.5_m},
        DataFor_CalculateSecureNetDistance{
            .input_aoi = AreaOfInterest::RIGHTRIGHT_FRONT,
            .input_currentRefVel = 45._mps,
            .input_anticipAccFront = .01_mps_sq,
            .input_anticipVelEnd = 40._mps,
            .input_reactionTime = .2_s,
            .input_anticipAccRear = -5._mps_sq,
            .input_DeltaDistEnd = 20._m,
            .input_perspective = MinThwPerspective::EGO_ANTICIPATED_REAR,
            .currentVehicleVel = 40._mps,
            .currentAccRear = 0._mps_sq,
            .isVehicleVisible = true,
            .ttc = 20._s,
            .expected_sercureLongitudinalDistance = units::make_unit<units::length::meter_t>(3. * std::pow(81.5, .25))},
        DataFor_CalculateSecureNetDistance{
            .input_aoi = AreaOfInterest::LEFTLEFT_FRONT,
            .input_currentRefVel = 45._mps,
            .input_anticipAccFront = .01_mps_sq,
            .input_anticipVelEnd = 40._mps,
            .input_reactionTime = .2_s,
            .input_anticipAccRear = -5._mps_sq,
            .input_DeltaDistEnd = 20._m,
            .input_perspective = MinThwPerspective::EGO_ANTICIPATED_REAR,
            .currentVehicleVel = 40._mps,
            .currentAccRear = 0._mps_sq,
            .isVehicleVisible = true,
            .ttc = 20._s,
            .expected_sercureLongitudinalDistance = units::make_unit<units::length::meter_t>(3. * std::pow(81.5, .25))},
        DataFor_CalculateSecureNetDistance{
            .input_aoi = AreaOfInterest::RIGHTRIGHT_SIDE,
            .input_currentRefVel = 0._mps,
            .input_anticipAccFront = 0._mps_sq,
            .input_anticipVelEnd = 0._mps,
            .input_reactionTime = 0._s,
            .input_anticipAccRear = 0._mps_sq,
            .input_DeltaDistEnd = 0._m,
            .input_perspective = MinThwPerspective::NORM,
            .currentVehicleVel = 0._mps,
            .currentAccRear = 0._mps_sq,
            .isVehicleVisible = true,
            .ttc = 0._s,
            .expected_sercureLongitudinalDistance = 0.0_m},
        DataFor_CalculateSecureNetDistance{
            .input_aoi = AreaOfInterest::LEFTLEFT_SIDE,
            .input_currentRefVel = 45._mps,
            .input_anticipAccFront = .01_mps_sq,
            .input_anticipVelEnd = 40._mps,
            .input_reactionTime = .2_s,
            .input_anticipAccRear = -5._mps_sq,
            .input_DeltaDistEnd = 20._m,
            .input_perspective = MinThwPerspective::NORM,
            .currentVehicleVel = 40._mps,
            .currentAccRear = 0._mps_sq,
            .isVehicleVisible = true,
            .ttc = 20._s,
            .expected_sercureLongitudinalDistance = units::make_unit<units::length::meter_t>(3. * std::pow(81.5, .25))},
        DataFor_CalculateSecureNetDistance{
            .input_aoi = AreaOfInterest::RIGHT_SIDE,
            .input_currentRefVel = 35._mps,
            .input_anticipAccFront = .01_mps_sq,
            .input_anticipVelEnd = 0._mps,
            .input_reactionTime = .2_s,
            .input_anticipAccRear = -7._mps_sq,
            .input_DeltaDistEnd = 20._m,
            .input_perspective = MinThwPerspective::EGO_ANTICIPATED_REAR,
            .currentVehicleVel = 0._mps,
            .currentAccRear = 0._mps_sq,
            .isVehicleVisible = true,
            .ttc = 5._s,
            .expected_sercureLongitudinalDistance = 124.5_m}));

/********************************************
 * CHECK DetermineLongitudinalVelocityDelta *
 ********************************************/

/// \brief Data table
struct DataForDetermineLongitudinalVelocityDeltaWithEgo
{
  units::velocity::meters_per_second_t input_vReference;
  units::velocity::meters_per_second_t input_vObserved;
  bool input_isRearArea;
  bool input_isSideArea;
  units::velocity::meters_per_second_t result_expectedResult;
};

class LongitudinalCalculations_DetermineLongitudinalVelocityDelta_WithEgo : public ::testing::Test,
                                                                            public ::testing::WithParamInterface<DataForDetermineLongitudinalVelocityDeltaWithEgo>
{
};

TEST_P(LongitudinalCalculations_DetermineLongitudinalVelocityDelta_WithEgo, CheckFunction_DetermineLongitudinalVelocityDelta_WithEgo)
{
  DataForDetermineLongitudinalVelocityDeltaWithEgo data = GetParam();
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeSurroundingVehicle> fakeSurroundingVehicle;
  ON_CALL(fakeSurroundingVehicle, GetLongitudinalVelocity).WillByDefault(Return(data.input_vObserved));
  ON_CALL(fakeSurroundingVehicle, ToTheSideOfEgo).WillByDefault(Return(data.input_isSideArea));
  ON_CALL(fakeSurroundingVehicle, BehindEgo).WillByDefault(Return(data.input_isRearArea));

  OwnVehicleInformationScmExtended egoVehicle;
  egoVehicle.longitudinalVelocity = data.input_vReference;

  // Call test
  LongitudinalCalculations longitudinalCalculations(fakeMentalModel);
  if (data.input_isSideArea && !data.input_isRearArea)
  {
    // Expect exception
    EXPECT_THROW(longitudinalCalculations.CalculateVelocityDelta(fakeSurroundingVehicle, egoVehicle), std::runtime_error);
  }

  else
  {
    // No exception
    const auto result = longitudinalCalculations.CalculateVelocityDelta(fakeSurroundingVehicle, egoVehicle);

    // Evaluate results
    ASSERT_EQ(result, data.result_expectedResult);
  }
}

INSTANTIATE_TEST_SUITE_P(
    WithEgo,
    LongitudinalCalculations_DetermineLongitudinalVelocityDelta_WithEgo,
    testing::Values(
        DataForDetermineLongitudinalVelocityDeltaWithEgo{
            .input_vReference = 20.0_mps,
            .input_vObserved = 30.0_mps,
            .input_isRearArea = true,
            .input_isSideArea = false,
            .result_expectedResult = -10.0_mps},  // 0
        DataForDetermineLongitudinalVelocityDeltaWithEgo{
            .input_vReference = 20.0_mps,
            .input_vObserved = 10.0_mps,
            .input_isRearArea = false,
            .input_isSideArea = true,
            .result_expectedResult = 10.0_mps}));
