/********************************************************************************
 * Copyright (c) 2018-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock-nice-strict.h>
#include <gmock/gmock-spec-builders.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "../Fakes/FakeFeatureExtractor.h"
#include "../Fakes/FakeMentalCalculations.h"
#include "../Fakes/FakeMentalModel.h"
#include "../Fakes/FakeMicroscopicCharacteristics.h"
#include "../Fakes/FakeScmDependencies.h"
#include "../Fakes/FakeStochastics.h"
#include "../Fakes/FakeSurroundingVehicle.h"
#include "../Fakes/FakeSwerving.h"
#include "Fakes/FakeIgnoringOuterLaneOvertakingProhibition.h"
#include "Fakes/FakeZipMerging.h"
#include "ScmCommons.h"
#include "SituationManager.h"
#include "gmock/gmock.h"

using ::testing::_;
using ::testing::NiceMock;
using ::testing::Return;

struct SituationManagerTester
{
  class SituationManagerUnderTest : SituationManager
  {
  public:
    template <typename... Args>
    SituationManagerUnderTest(Args&&... args)
        : SituationManager{std::forward<Args>(args)...} {};

    using SituationManager::AddRelevantSideClusterIntensities;
    using SituationManager::DetermineIntensityFromAccelerationLongitudinal;
    using SituationManager::FilterVehicle;
    using SituationManager::GenerateIntensitiesAndSampleSituation;
    using SituationManager::GetClusterVehicles;
    using SituationManager::GetClusterVehiclesSideLane;
    using SituationManager::GetIntensity_AnticipatedHighRiskLaneChanger;
    using SituationManager::GetIntensity_HighRiskLaneChanger;
    using SituationManager::GetIntensity_LeftSideCollisionRisk;
    using SituationManager::GetIntensity_RightSideCollisionRisk;
    using SituationManager::HaveIntensitiesChangedSignificantly;
    using SituationManager::IsSituationAmbigious;
    using SituationManager::SetIgnoringOuterLaneOvertakingProhibition;
    using SituationManager::SetLastIgnoreRiskState;
    using SituationManager::SetSideClusterIntensities;

    bool GET_LAST_IGNORE_RISK_STATE()
    {
      return _lastIgnoreRiskState;
    }
  };

  SituationManagerTester()
      : fakeStochastics{},
        fakeMentalModel{},
        fakeFeatureExtractor{},
        fakeMentalCalculations{},
        fakeSwerving{},
        fakeMicroscopicCharacteristics{},
        situationManager(100_ms,
                         &fakeStochastics,
                         fakeMentalModel,
                         fakeFeatureExtractor,
                         fakeMentalCalculations,
                         fakeSwerving)
  {
  }
  NiceMock<FakeStochastics> fakeStochastics;
  NiceMock<FakeMentalModel> fakeMentalModel;
  NiceMock<FakeFeatureExtractor> fakeFeatureExtractor;
  NiceMock<FakeMentalCalculations> fakeMentalCalculations;
  NiceMock<FakeSwerving> fakeSwerving;
  NiceMock<FakeMicroscopicCharacteristics> fakeMicroscopicCharacteristics;

  SituationManagerUnderTest situationManager;
};

/********************************
 * CHECK SideSituationAssesment *
 ********************************/

/// \brief Data table for definition of individual test cases
struct DataFor_SideSituations
{
  AreaOfInterest inputObservedVehicleArea;         // Area to find the intensities in
  bool return_IsMinimumFollowingDistanceViolated;  // Return value of the function IsMinimumFollowingDistanceViolated
  bool return_IsCollisionCourseDetected;           // Return value of the function IsCollisionCourseDetected
  bool return_HasActualLaneCrossingConflict;       // Return value of the function HasActualLaneCrossingConflict
  bool return_HasIntentionalLaneCrossingConflict;  // Return value of the function HasIntentionalLaneCrossingConflict
  double expected_Intensity;                       // Expected intensity calculated by the functions

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_SideSituations& obj)
  {
    return os
           << "inputObservedVehicleArea (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.inputObservedVehicleArea)
           << " | return_IsMinimumFollowingDistanceViolated (bool): " << obj.return_IsMinimumFollowingDistanceViolated
           << " | return_IsCollisionCourseDetected (bool): " << obj.return_IsCollisionCourseDetected
           << " | return_HasActualLaneCrossingConflict (bool): " << obj.return_HasActualLaneCrossingConflict
           << " | return_HasIntentionalLaneCrossingConflict (bool): " << obj.return_HasIntentionalLaneCrossingConflict
           << " | expected_Intensity (double): " << obj.expected_Intensity;
  }
};

class SituationManager_SideSituationsCheck : public ::testing::Test,
                                             public ::testing::WithParamInterface<DataFor_SideSituations>
{
};

TEST_P(SituationManager_SideSituationsCheck, SituationManager_Check_Side_Situation_Assesment)
{
  SituationManagerTester TEST_HELPER;
  // Get resources for testing
  DataFor_SideSituations data = GetParam();

  // Set data needed for calculation
  double calculatedIntensity{-99.};

  // Test
  switch (data.inputObservedVehicleArea)
  {
    case AreaOfInterest::LEFT_SIDE:
      // Input data for the function mocked, since those functions must be tested elsewhere
      calculatedIntensity =
          TEST_HELPER.situationManager.GetIntensity_LeftSideCollisionRisk(data.return_HasActualLaneCrossingConflict,
                                                                          data.return_HasIntentionalLaneCrossingConflict);
      break;
    case AreaOfInterest::RIGHT_SIDE:
      // Input data for the function mocked, since those functions must be tested elsewhere
      calculatedIntensity =
          TEST_HELPER.situationManager.GetIntensity_RightSideCollisionRisk(data.return_HasActualLaneCrossingConflict,
                                                                           data.return_HasIntentionalLaneCrossingConflict);
      break;
    default:
      // Tried to test a aoi that is not implemented --> Failed test
      calculatedIntensity = INFINITY;
  }

  // Compare actual values with expected values
  ASSERT_EQ(calculatedIntensity, data.expected_Intensity);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    SituationManager_SideSituationsCheck,
    testing::Values(
        // TESTCASE 1: Testing situation SIDE_COLLISION_RISK_FROM_LEFT
        DataFor_SideSituations{
            .inputObservedVehicleArea = AreaOfInterest::LEFT_SIDE,
            .return_IsMinimumFollowingDistanceViolated = false,
            .return_IsCollisionCourseDetected = false,
            .return_HasActualLaneCrossingConflict = false,
            .return_HasIntentionalLaneCrossingConflict = false,
            .expected_Intensity = 0.},
        // TESTCASE 2: Testing situation SIDE_COLLISION_RISK_FROM_LEFT
        DataFor_SideSituations{
            .inputObservedVehicleArea = AreaOfInterest::LEFT_SIDE,
            .return_IsMinimumFollowingDistanceViolated = false,
            .return_IsCollisionCourseDetected = false,
            .return_HasActualLaneCrossingConflict = false,
            .return_HasIntentionalLaneCrossingConflict = true,
            .expected_Intensity = 1. / 3.},
        // TESTCASE 3: Testing situation SIDE_COLLISION_RISK_FROM_LEFT
        DataFor_SideSituations{
            .inputObservedVehicleArea = AreaOfInterest::LEFT_SIDE,
            .return_IsMinimumFollowingDistanceViolated = false,
            .return_IsCollisionCourseDetected = false,
            .return_HasActualLaneCrossingConflict = true,
            .return_HasIntentionalLaneCrossingConflict = false,
            .expected_Intensity = 1.},
        // TESTCASE 4: Testing situation SIDE_COLLISION_RISK_FROM_LEFT
        DataFor_SideSituations{
            .inputObservedVehicleArea = AreaOfInterest::LEFT_SIDE,
            .return_IsMinimumFollowingDistanceViolated = false,
            .return_IsCollisionCourseDetected = false,
            .return_HasActualLaneCrossingConflict = true,
            .return_HasIntentionalLaneCrossingConflict = true,
            .expected_Intensity = 1.},
        // TESTCASE 5: Testing situation SIDE_COLLISION_RISK_FROM_RIGHT
        DataFor_SideSituations{
            .inputObservedVehicleArea = AreaOfInterest::RIGHT_SIDE,
            .return_IsMinimumFollowingDistanceViolated = false,
            .return_IsCollisionCourseDetected = false,
            .return_HasActualLaneCrossingConflict = false,
            .return_HasIntentionalLaneCrossingConflict = false,
            .expected_Intensity = 0.},
        // TESTCASE 6: Testing situation SIDE_COLLISION_RISK_FROM_RIGHT
        DataFor_SideSituations{
            .inputObservedVehicleArea = AreaOfInterest::RIGHT_SIDE,
            .return_IsMinimumFollowingDistanceViolated = false,
            .return_IsCollisionCourseDetected = false,
            .return_HasActualLaneCrossingConflict = false,
            .return_HasIntentionalLaneCrossingConflict = true,
            .expected_Intensity = 1. / 3.},
        // TESTCASE 7: Testing situation SIDE_COLLISION_RISK_FROM_RIGHT
        DataFor_SideSituations{
            .inputObservedVehicleArea = AreaOfInterest::RIGHT_SIDE,
            .return_IsMinimumFollowingDistanceViolated = false,
            .return_IsCollisionCourseDetected = false,
            .return_HasActualLaneCrossingConflict = true,
            .return_HasIntentionalLaneCrossingConflict = false,
            .expected_Intensity = 1.},
        // TESTCASE 8: Testing situation SIDE_COLLISION_RISK_FROM_RIGHT
        DataFor_SideSituations{
            .inputObservedVehicleArea = AreaOfInterest::RIGHT_SIDE,
            .return_IsMinimumFollowingDistanceViolated = false,
            .return_IsCollisionCourseDetected = false,
            .return_HasActualLaneCrossingConflict = true,
            .return_HasIntentionalLaneCrossingConflict = true,
            .expected_Intensity = 1.}));

/***********************
 * CHECK FilterVehicle *
 ***********************/

/// \brief Data table for definition of individual test cases
struct DataFor_FilterVehicle
{
  AreaOfInterest near;
  AreaOfInterest far;
  bool expectedReliability;
  bool obstructionOverlappingNear;
  bool obstructionOverlappingFar;
  bool collisionCourse;
  bool exceedingLateralMotion;
  std::vector<AreaOfInterest> result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_FilterVehicle& obj)
  {
    return os
           << "near (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.near)
           << " | far (AreaOfInterest): " << ScmCommons::AreaOfInterestToString(obj.far)
           << " | expectedReliability (bool): " << obj.expectedReliability
           << " | obstructionOverlappingNear (bool): " << obj.obstructionOverlappingNear
           << " | obstructionOverlappingFar (bool): " << obj.obstructionOverlappingFar
           << " | collisionCourse (bool): " << obj.collisionCourse
           << " | exceedingLateralMovement (bool): " << obj.exceedingLateralMotion;
  }
};

class SituationManager_FilterVehicle : public ::testing::Test,
                                       public ::testing::WithParamInterface<DataFor_FilterVehicle>
{
};

TEST_P(SituationManager_FilterVehicle, SituationManager_FilterVehicle)
{
  SituationManagerTester TEST_HELPER;
  static constexpr double SUFFICIENT_RELIABILITY{DataQuality::MEDIUM};
  static constexpr double INSUFFICIENT_RELIABILITY{DataQuality::MEDIUM - 1};

  // Get resources for testing
  DataFor_FilterVehicle data = GetParam();

  FakeSurroundingVehicle nearVehicle;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(data.near, _)).WillByDefault(Return(&nearVehicle));
  const double datapointReliability{data.expectedReliability ? SUFFICIENT_RELIABILITY : INSUFFICIENT_RELIABILITY};
  ObstructionScm obsNear(0_m, 0_m, 0_m);
  obsNear.isOverlapping = data.obstructionOverlappingNear;
  ON_CALL(nearVehicle, IsReliable(_, _)).WillByDefault(Return(data.expectedReliability));
  ON_CALL(nearVehicle, GetLateralObstruction).WillByDefault(Return(VehicleParameter<ObstructionScm>(obsNear, datapointReliability)));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetObstruction(data.near, _)).WillByDefault(Return(obsNear));

  ObstructionScm obsFar(0_m, 0_m, 0_m);
  obsFar.isOverlapping = data.obstructionOverlappingFar;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetObstruction(data.far, _)).WillByDefault(Return(obsFar));

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsCollisionCourseDetected(_)).WillByDefault(Return(data.collisionCourse));
  ON_CALL(nearVehicle, GetLaneOfPerception()).WillByDefault(Return(RelativeLane::EGO));
  ON_CALL(nearVehicle, IsExceedingLateralMotionThreshold()).WillByDefault(Return(data.exceedingLateralMotion));

  std::vector<AreaOfInterest> vehicles;
  vehicles.push_back(data.near);
  vehicles.push_back(data.far);

  TEST_HELPER.situationManager.FilterVehicle(vehicles, data.near, data.far);

  ASSERT_TRUE((vehicles == data.result));
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    SituationManager_FilterVehicle,
    testing::Values(
        DataFor_FilterVehicle{
            .near = AreaOfInterest::EGO_FRONT,
            .far = AreaOfInterest::EGO_FRONT_FAR,
            .expectedReliability = true,
            .obstructionOverlappingNear = true,
            .obstructionOverlappingFar = false,
            .collisionCourse = false,
            .exceedingLateralMotion = true,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::EGO_FRONT_FAR}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::EGO_FRONT,
            .far = AreaOfInterest::EGO_FRONT_FAR,
            .expectedReliability = false,
            .obstructionOverlappingNear = false,
            .obstructionOverlappingFar = true,
            .collisionCourse = true,
            .exceedingLateralMotion = false,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::EGO_FRONT_FAR}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::EGO_FRONT,
            .far = AreaOfInterest::EGO_FRONT_FAR,
            .expectedReliability = false,
            .obstructionOverlappingNear = true,
            .obstructionOverlappingFar = false,
            .collisionCourse = true,
            .exceedingLateralMotion = true,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::EGO_FRONT}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::EGO_FRONT,
            .far = AreaOfInterest::EGO_FRONT_FAR,
            .expectedReliability = false,
            .obstructionOverlappingNear = true,
            .obstructionOverlappingFar = false,
            .collisionCourse = false,
            .exceedingLateralMotion = true,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::EGO_FRONT}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::EGO_FRONT,
            .far = AreaOfInterest::EGO_FRONT_FAR,
            .expectedReliability = true,
            .obstructionOverlappingNear = true,
            .obstructionOverlappingFar = false,
            .collisionCourse = false,
            .exceedingLateralMotion = false,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::EGO_FRONT}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::LEFT_FRONT,
            .far = AreaOfInterest::LEFT_FRONT_FAR,
            .expectedReliability = true,
            .obstructionOverlappingNear = true,
            .obstructionOverlappingFar = false,
            .collisionCourse = false,
            .exceedingLateralMotion = true,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::LEFT_FRONT_FAR}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::LEFT_FRONT,
            .far = AreaOfInterest::LEFT_FRONT_FAR,
            .expectedReliability = false,
            .obstructionOverlappingNear = false,
            .obstructionOverlappingFar = true,
            .collisionCourse = true,
            .exceedingLateralMotion = false,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::LEFT_FRONT_FAR}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::LEFT_FRONT,
            .far = AreaOfInterest::LEFT_FRONT_FAR,
            .expectedReliability = false,
            .obstructionOverlappingNear = true,
            .obstructionOverlappingFar = false,
            .collisionCourse = true,
            .exceedingLateralMotion = true,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::LEFT_FRONT}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::LEFT_FRONT,
            .far = AreaOfInterest::LEFT_FRONT_FAR,
            .expectedReliability = false,
            .obstructionOverlappingNear = true,
            .obstructionOverlappingFar = false,
            .collisionCourse = false,
            .exceedingLateralMotion = true,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::LEFT_FRONT}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::LEFT_FRONT,
            .far = AreaOfInterest::LEFT_FRONT_FAR,
            .expectedReliability = false,
            .obstructionOverlappingNear = true,
            .obstructionOverlappingFar = false,
            .collisionCourse = false,
            .exceedingLateralMotion = false,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::LEFT_FRONT}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::RIGHT_FRONT,
            .far = AreaOfInterest::RIGHT_FRONT_FAR,
            .expectedReliability = true,
            .obstructionOverlappingNear = true,
            .obstructionOverlappingFar = false,
            .collisionCourse = false,
            .exceedingLateralMotion = true,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::RIGHT_FRONT_FAR}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::RIGHT_FRONT,
            .far = AreaOfInterest::RIGHT_FRONT_FAR,
            .expectedReliability = false,
            .obstructionOverlappingNear = false,
            .obstructionOverlappingFar = true,
            .collisionCourse = true,
            .exceedingLateralMotion = false,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::RIGHT_FRONT_FAR}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::RIGHT_FRONT,
            .far = AreaOfInterest::RIGHT_FRONT_FAR,
            .expectedReliability = true,
            .obstructionOverlappingNear = true,
            .obstructionOverlappingFar = false,
            .collisionCourse = true,
            .exceedingLateralMotion = true,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::RIGHT_FRONT}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::RIGHT_FRONT,
            .far = AreaOfInterest::RIGHT_FRONT_FAR,
            .expectedReliability = false,
            .obstructionOverlappingNear = true,
            .obstructionOverlappingFar = false,
            .collisionCourse = false,
            .exceedingLateralMotion = true,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::RIGHT_FRONT}},
        DataFor_FilterVehicle{
            .near = AreaOfInterest::RIGHT_FRONT,
            .far = AreaOfInterest::RIGHT_FRONT_FAR,
            .expectedReliability = false,
            .obstructionOverlappingNear = true,
            .obstructionOverlappingFar = false,
            .collisionCourse = false,
            .exceedingLateralMotion = false,
            .result = std::vector<AreaOfInterest>{AreaOfInterest::RIGHT_FRONT}}));

/********************************************************
 * CHECK DetermineIntensityFromAccelerationLongitudinal *
 ********************************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_DetermineIntensityFromAccelerationLongitudinal
{
  units::acceleration::meters_per_second_squared_t comfortAcceleration;
  units::acceleration::meters_per_second_squared_t maxAcceleration;
  units::acceleration::meters_per_second_squared_t acceleration;
  bool isFrontObject;
  double resultIntensity;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_DetermineIntensityFromAccelerationLongitudinal& obj)
  {
    return os
           << "comfortAcceleration (double): " << obj.comfortAcceleration << "; "
           << "maxAcceleration (double): " << obj.maxAcceleration << "; "
           << "acceleration (double): " << obj.acceleration << "; "
           << "isFrontObject (bool): " << obj.isFrontObject << "; "
           << "resultIntensity (double): " << obj.resultIntensity << "; ";
  }
};

class SituationManager_DetermineIntensityFromAccelerationLongitudinal
    : public ::testing::Test,
      public ::testing::WithParamInterface<DataFor_DetermineIntensityFromAccelerationLongitudinal>
{
};

TEST_P(SituationManager_DetermineIntensityFromAccelerationLongitudinal, SituationManager_DetermineIntensityFromAccelerationLongitudinal)
{
  SituationManagerTester TEST_HELPER;
  DataFor_DetermineIntensityFromAccelerationLongitudinal data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData())
      .WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));

  scm::signal::DriverInput driverInput;
  driverInput.driverParameters.comfortLongitudinalDeceleration = data.comfortAcceleration;
  driverInput.driverParameters.maximumLongitudinalDeceleration = data.maxAcceleration;

  driverInput.driverParameters.comfortLongitudinalAcceleration = data.comfortAcceleration;
  driverInput.driverParameters.maximumLongitudinalAcceleration = data.maxAcceleration;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalAcceleration())
      .WillByDefault(Return(driverInput.driverParameters.comfortLongitudinalAcceleration));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration())
      .WillByDefault(Return(driverInput.driverParameters.comfortLongitudinalDeceleration));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverInput.driverParameters));

  double resultIntensity = TEST_HELPER.situationManager
                               .DetermineIntensityFromAccelerationLongitudinal(data.acceleration, data.isFrontObject);

  EXPECT_NEAR(data.resultIntensity, resultIntensity, 1e-2);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SituationManager_DetermineIntensityFromAccelerationLongitudinal,
    testing::Values(
        // Expected results:
        //  - 0 for acceleration of 0 (or less)
        //  - 1 (GetRiskValue(Risk::LOW)) for acceleration up to comfortAcceleration
        //  - linear progression from 1 to 4 (GetRiskValue(Risk::MEDIUM)) for acceleration up to comfortAcceleration
        //      * 3.5 (ScmDefinitions::COMFORT_ACCELERATION_HIGH_RISK_MULTIPLIER)
        //  - linear progression from 4 to 8 (GetRiskValue(Risk::HIGH)) for acceleration up to maxAcceleration

        // tests for object approaching ego from the rear
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = -1.2_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 0},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 0_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 0},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 0.8_mps_sq,
            .maxAcceleration = 7_mps_sq,
            .acceleration = 0_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 0},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 0.5_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 1},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 0.6_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 0.5_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 1},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1.3_mps_sq,
            .maxAcceleration = 8_mps_sq,
            .acceleration = 1.1_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 1},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 1_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 1},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 1.5_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 1. + (1.5 - 1) * (4. - 1.) / (3.5 - 1)},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 1.5_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 1.6},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 2.3_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 2.56},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 3.1_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 3.52},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 3.5_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 4},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1.5_mps_sq,
            .maxAcceleration = 8_mps_sq,
            .acceleration = 3.8_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 1. + (3.8 - 1.5) * (4. - 1.) / ((1.5 * 3.5) - 1.5)},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1.5_mps_sq,
            .maxAcceleration = 8_mps_sq,
            .acceleration = 3.8_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 2.84},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1.5_mps_sq,
            .maxAcceleration = 8_mps_sq,
            .acceleration = 4.7_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 3.56},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1.5_mps_sq,
            .maxAcceleration = 8_mps_sq,
            .acceleration = 5.25_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 4},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 4_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 4. + (4 - 3.5) * (8. - 4.) / (10 - 3.5)},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 4_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 4.3077},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 6.8_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 6.0308},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 10_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 8},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 12.2_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 8},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1.5_mps_sq,
            .maxAcceleration = 8.3_mps_sq,
            .acceleration = 5.9_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 4. + (5.9 - 1.5 * 3.5) * (8. - 4.) / (8.3 - (1.5 * 3.5))},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1.5_mps_sq,
            .maxAcceleration = 8.3_mps_sq,
            .acceleration = 5.9_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 4.8525},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1.5_mps_sq,
            .maxAcceleration = 8.3_mps_sq,
            .acceleration = 7.3_mps_sq,
            .isFrontObject = false,
            .resultIntensity = 6.6885},
        // tests for ego approaching an object ahead (same calculation only with negative acceleration)
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 1.2_mps_sq,
            .isFrontObject = true,
            .resultIntensity = 0},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = 0_mps_sq,
            .isFrontObject = true,
            .resultIntensity = 0},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 0.8_mps_sq,
            .maxAcceleration = 7_mps_sq,
            .acceleration = 0_mps_sq,
            .isFrontObject = true,
            .resultIntensity = 0},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = -0.5_mps_sq,
            .isFrontObject = true,
            .resultIntensity = 1},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = -1_mps_sq,
            .isFrontObject = true,
            .resultIntensity = 1},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = -2.3_mps_sq,
            .isFrontObject = true,
            .resultIntensity = 2.56},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = -3.5_mps_sq,
            .isFrontObject = true,
            .resultIntensity = 4},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = -6.8_mps_sq,
            .isFrontObject = true,
            .resultIntensity = 6.0308},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = -10_mps_sq,
            .isFrontObject = true,
            .resultIntensity = 8},
        DataFor_DetermineIntensityFromAccelerationLongitudinal{
            .comfortAcceleration = 1_mps_sq,
            .maxAcceleration = 10_mps_sq,
            .acceleration = -12.2_mps_sq,
            .isFrontObject = true,
            .resultIntensity = 8}));

/************************************
 * CHECK GetClusterVehiclesSideLane *
 ************************************/

/// \brief Data table for definition of individual test cases
struct DataFor_GetClusterVehiclesSideLane
{
  std::vector<AreaOfInterest> input_aois;
  std::map<AreaOfInterest, bool> expected_clusterVehicles;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GetClusterVehiclesSideLane& obj)
  {
    os << "input_aois (vector<AreaOfInterest>): ";
    for (const auto& aoi : obj.input_aois)
    {
      os << ScmCommons::AreaOfInterestToString(aoi) << ", ";
    }
    return os
           << " | expected_criticalActionIntensities (std::map<CriticalActionState, double>): ";
    for (std::map<AreaOfInterest, bool>::const_iterator it = obj.expected_clusterVehicles.cbegin(); it != obj.expected_clusterVehicles.cend(); ++it)
    {
      os << "AreaOfInterest:" << ScmCommons::AreaOfInterestToString(it->first)
         << " " << it->second << std::endl;
    }
  }
};

class SituationManager_GetClusterVehiclesSideLane : public ::testing::Test,
                                                    public ::testing::WithParamInterface<DataFor_GetClusterVehiclesSideLane>
{
};

TEST_P(SituationManager_GetClusterVehiclesSideLane, SituationManager_GetClusterVehiclesSideLane)
{
  SituationManagerTester TEST_HELPER;
  DataFor_GetClusterVehiclesSideLane data = GetParam();

  VirtualAgentsSCM virtualAgentsScm;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVirtualAgents()).WillByDefault(Return(virtualAgentsScm));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsVehicleInSideLane(_)).WillByDefault(Return(true));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  NiceMock<FakeSurroundingVehicle> fakeVehicle2;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(&fakeVehicle));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(AreaOfInterest::LEFT_FRONT_FAR, _)).WillByDefault(Return(&fakeVehicle2));

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsVehicleInSideLane(&fakeVehicle)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsVehicleInSideLane(&fakeVehicle2)).WillByDefault(Return(false));

  std::map<AreaOfInterest, bool> actual_clusterVehicles = TEST_HELPER.situationManager.GetClusterVehiclesSideLane(data.input_aois);

  ASSERT_EQ(actual_clusterVehicles.size(), data.expected_clusterVehicles.size());
  ASSERT_TRUE(std::equal(actual_clusterVehicles.begin(), actual_clusterVehicles.end(), data.expected_clusterVehicles.begin()));
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    SituationManager_GetClusterVehiclesSideLane,
    testing::Values(
        DataFor_GetClusterVehiclesSideLane{
            .input_aois = {},
            .expected_clusterVehicles = {}},
        DataFor_GetClusterVehiclesSideLane{
            .input_aois = {AreaOfInterest::LEFT_FRONT},
            .expected_clusterVehicles = {{AreaOfInterest::LEFT_FRONT, false}}},
        DataFor_GetClusterVehiclesSideLane{
            .input_aois = {
                AreaOfInterest::LEFT_FRONT,
                AreaOfInterest::LEFT_FRONT_FAR,
                AreaOfInterest::RIGHT_FRONT,
                AreaOfInterest::RIGHT_FRONT_FAR,
                AreaOfInterest::LEFT_REAR,
                AreaOfInterest::RIGHT_REAR,
                AreaOfInterest::LEFT_SIDE,
                AreaOfInterest::RIGHT_SIDE,
                AreaOfInterest::LEFTLEFT_FRONT,
                AreaOfInterest::RIGHTRIGHT_FRONT,
                AreaOfInterest::LEFTLEFT_REAR,
                AreaOfInterest::RIGHTRIGHT_REAR,
                AreaOfInterest::LEFTLEFT_SIDE,
                AreaOfInterest::RIGHTRIGHT_SIDE},
            .expected_clusterVehicles = {{AreaOfInterest::LEFT_FRONT, false}, {AreaOfInterest::LEFT_FRONT_FAR, false}, {AreaOfInterest::RIGHT_FRONT, true}, {AreaOfInterest::RIGHT_FRONT_FAR, true}, {AreaOfInterest::LEFT_REAR, true}, {AreaOfInterest::RIGHT_REAR, true}, {AreaOfInterest::LEFT_SIDE, true}, {AreaOfInterest::RIGHT_SIDE, true}, {AreaOfInterest::LEFTLEFT_FRONT, true}, {AreaOfInterest::RIGHTRIGHT_FRONT, true}, {AreaOfInterest::LEFTLEFT_REAR, true}, {AreaOfInterest::RIGHTRIGHT_REAR, true}, {AreaOfInterest::LEFTLEFT_SIDE, true}, {AreaOfInterest::RIGHTRIGHT_SIDE, true}}}));

/****************************
 * CHECK GetClusterVehicles *
 ****************************/

/// \brief Data table for definition of individual test cases
struct DataFor_GetClusterVehicles
{
  std::vector<AreaOfInterest> input_aois;
  std::vector<AreaOfInterest> expected_clusterVehicles;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_GetClusterVehicles& obj)
  {
    os << "input_aois (vector<AreaOfInterest>): ";
    for (const auto& aoi : obj.input_aois)
    {
      os << ScmCommons::AreaOfInterestToString(aoi) << ", ";
    }
    os << "| expected_clusterVehicles (vector<AreaOfInterest>): ";
    for (const auto& aoi : obj.expected_clusterVehicles)
    {
      os << ScmCommons::AreaOfInterestToString(aoi) << ", ";
    }
    return os;
  }
};

class SituationManager_GetClusterVehicles : public ::testing::Test,
                                            public ::testing::WithParamInterface<DataFor_GetClusterVehicles>
{
};

TEST_P(SituationManager_GetClusterVehicles, SituationManager_GetClusterVehicles)
{
  SituationManagerTester TEST_HELPER;
  DataFor_GetClusterVehicles data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(_, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::LEFT_FRONT, _)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::LEFT_FRONT_FAR, _)).WillByDefault(Return(false));

  std::vector<AreaOfInterest> actual_clusterVehicles = TEST_HELPER.situationManager.GetClusterVehicles(data.input_aois);

  ASSERT_EQ(actual_clusterVehicles.size(), data.expected_clusterVehicles.size());
  ASSERT_TRUE(std::equal(actual_clusterVehicles.begin(), actual_clusterVehicles.end(), data.expected_clusterVehicles.begin()));
}
INSTANTIATE_TEST_SUITE_P(
    Default,
    SituationManager_GetClusterVehicles,
    testing::Values(
        DataFor_GetClusterVehicles{
            .input_aois = {},
            .expected_clusterVehicles = {}},
        DataFor_GetClusterVehicles{
            .input_aois = {AreaOfInterest::LEFT_FRONT},
            .expected_clusterVehicles = {}},
        DataFor_GetClusterVehicles{
            .input_aois = {
                AreaOfInterest::LEFT_FRONT,
                AreaOfInterest::LEFT_FRONT_FAR,
                AreaOfInterest::RIGHT_FRONT,
                AreaOfInterest::RIGHT_FRONT_FAR,
                AreaOfInterest::LEFT_REAR,
                AreaOfInterest::RIGHT_REAR,
                AreaOfInterest::EGO_FRONT,
                AreaOfInterest::EGO_FRONT_FAR,
                AreaOfInterest::EGO_REAR,
                AreaOfInterest::LEFT_SIDE,
                AreaOfInterest::RIGHT_SIDE,
                AreaOfInterest::INSTRUMENT_CLUSTER,
                AreaOfInterest::INFOTAINMENT,
                AreaOfInterest::HUD,
                AreaOfInterest::LEFTLEFT_FRONT,
                AreaOfInterest::RIGHTRIGHT_FRONT,
                AreaOfInterest::LEFTLEFT_REAR,
                AreaOfInterest::RIGHTRIGHT_REAR,
                AreaOfInterest::LEFTLEFT_SIDE,
                AreaOfInterest::RIGHTRIGHT_SIDE},
            .expected_clusterVehicles = {
                AreaOfInterest::RIGHT_FRONT,
                AreaOfInterest::RIGHT_FRONT_FAR,
                AreaOfInterest::LEFT_REAR,
                AreaOfInterest::RIGHT_REAR,
                AreaOfInterest::EGO_FRONT,
                AreaOfInterest::EGO_FRONT_FAR,
                AreaOfInterest::EGO_REAR,
                AreaOfInterest::LEFT_SIDE,
                AreaOfInterest::RIGHT_SIDE,
                AreaOfInterest::INSTRUMENT_CLUSTER,
                AreaOfInterest::INFOTAINMENT,
                AreaOfInterest::HUD,
                AreaOfInterest::LEFTLEFT_FRONT,
                AreaOfInterest::RIGHTRIGHT_FRONT,
                AreaOfInterest::LEFTLEFT_REAR,
                AreaOfInterest::RIGHTRIGHT_REAR,
                AreaOfInterest::LEFTLEFT_SIDE,
                AreaOfInterest::RIGHTRIGHT_SIDE,
            }}));

/******************************************************
 * CHECK HaveIntensitiesChangedSignificantlyEmptyMaps *
 ******************************************************/

TEST(SituationManager_HaveIntensitiesChangedSignificantly, SituationManager_CheckFunction_HaveIntensitiesChangedSignificantlyEmptyMaps)
{
  SituationManagerTester TEST_HELPER;

  std::map<Situation, double> intensities{{Situation::COLLISION, 0.},
                                          {Situation::FOLLOWING_DRIVING, 1.},
                                          {Situation::FREE_DRIVING, 0.},
                                          {Situation::LANE_CHANGER_FROM_LEFT, .5},
                                          {Situation::LANE_CHANGER_FROM_RIGHT, 0.},
                                          {Situation::SIDE_COLLISION_RISK_FROM_LEFT, 0.},
                                          {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, 0.},
                                          {Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, .05},
                                          {Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, 0.}};
  std::map<Situation, double> intensitiesLast = intensities;
  std::map<Situation, double> intensitiesEmpty{};

  bool result = TEST_HELPER.situationManager.HaveIntensitiesChangedSignificantly(intensities, intensitiesEmpty);
  ASSERT_TRUE(result);
  result = TEST_HELPER.situationManager.HaveIntensitiesChangedSignificantly(intensitiesEmpty, intensitiesLast);
  ASSERT_TRUE(result);
  result = TEST_HELPER.situationManager.HaveIntensitiesChangedSignificantly(intensitiesEmpty, intensitiesEmpty);
  ASSERT_TRUE(result);
}

/**************************************************************
 * CHECK HaveIntensitiesChangedSignificantlyIntensityNotInMap *
 **************************************************************/

TEST(SituationManager_HaveIntensitiesChangedSignificantly, SituationManager_CheckFunction_HaveIntensitiesChangedSignificantlyIntensityNotInMap)
{
  SituationManagerTester TEST_HELPER;

  std::map<Situation, double> intensities{{Situation::COLLISION, 0.},
                                          {Situation::FOLLOWING_DRIVING, 1.},
                                          {Situation::FREE_DRIVING, 0.},
                                          {Situation::LANE_CHANGER_FROM_LEFT, .5},
                                          {Situation::LANE_CHANGER_FROM_RIGHT, 0.},
                                          {Situation::SIDE_COLLISION_RISK_FROM_LEFT, 0.},
                                          {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, 0.},
                                          {Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, .05},
                                          {Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, 0.}};
  std::map<Situation, double> intensitiesLast = intensities;
  std::map<Situation, double> intensitiesEmpty{};

  intensities.at(Situation::FOLLOWING_DRIVING) = 4.9;
  intensitiesLast.erase(Situation::FOLLOWING_DRIVING);
  auto result = TEST_HELPER.situationManager.HaveIntensitiesChangedSignificantly(intensities, intensitiesLast);
  ASSERT_TRUE(result);
}

/************************************************************
 * CHECK HaveIntensitiesChangedSignificantlyMultipleEntries *
 ************************************************************/

TEST(SituationManager_HaveIntensitiesChangedSignificantly, SituationManager_CheckFunction_HaveIntensitiesChangedSignificantlyMultipleEntries)
{
  SituationManagerTester TEST_HELPER;

  std::map<Situation, double> intensities{{Situation::COLLISION, 0.},
                                          {Situation::FOLLOWING_DRIVING, 1.},
                                          {Situation::FREE_DRIVING, 0.},
                                          {Situation::LANE_CHANGER_FROM_LEFT, .5},
                                          {Situation::LANE_CHANGER_FROM_RIGHT, 0.},
                                          {Situation::SIDE_COLLISION_RISK_FROM_LEFT, 0.},
                                          {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, 0.},
                                          {Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, .05},
                                          {Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, 0.}};
  std::map<Situation, double> intensitiesLast = intensities;
  std::map<Situation, double> intensitiesEmpty{};

  intensities.emplace(Situation::FOLLOWING_DRIVING, 0.);
  intensities.at(Situation::LANE_CHANGER_FROM_RIGHT) = 1.33;
  intensitiesLast.at(Situation::LANE_CHANGER_FROM_RIGHT) = 1.33;
  intensities.at(Situation::SIDE_COLLISION_RISK_FROM_LEFT) = 2.8;
  intensitiesLast.at(Situation::SIDE_COLLISION_RISK_FROM_LEFT) = 1.;
  auto result = TEST_HELPER.situationManager.HaveIntensitiesChangedSignificantly(intensities, intensitiesLast);  // Only one significant change
  ASSERT_TRUE(result);

  intensities.at(Situation::LANE_CHANGER_FROM_RIGHT) = 1.32;
  result = TEST_HELPER.situationManager.HaveIntensitiesChangedSignificantly(intensities, intensitiesLast);  // Additionally one minor change
  ASSERT_TRUE(result);

  intensities.at(Situation::SIDE_COLLISION_RISK_FROM_LEFT) = 1.;
  result = TEST_HELPER.situationManager.HaveIntensitiesChangedSignificantly(intensities, intensitiesLast);  // Consistency-Check: Remove the significant change
  ASSERT_FALSE(result);
}

/**********************************************************
 * CHECK CheckFunctionHaveIntensitiesChangedSignificantly *
 **********************************************************/

struct DataFor_CheckFunctionHaveIntensitiesChangedSignificantly
{
  double intensityFollowingDriving;
  double intensityLaneChangerFromLeft;
  double intensitySuspiciousObjectInLeftLane;

  double lastIntensityFollowingDriving;
  double lastIntensityLaneChangerFromLeft;
  double lastIntensitySuspiciousObjectInLeftLane;
  bool result;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const DataFor_CheckFunctionHaveIntensitiesChangedSignificantly& obj)
  {
    return os
           << " intensityCollision (double): " << obj.intensityFollowingDriving
           << " | intensityLaneChangerFromLeft (double): " << obj.intensityLaneChangerFromLeft
           << " | intensitySudpiciousObjectInLeftLane (double): " << obj.intensitySuspiciousObjectInLeftLane
           << " | lastIntensityFollowingDriving (double): " << obj.lastIntensityFollowingDriving
           << " | lastIntensityLaneChangerFromLeft (double): " << obj.lastIntensityLaneChangerFromLeft
           << " | lastIntensitySudpiciousObjectInLeftLane (double): " << obj.lastIntensitySuspiciousObjectInLeftLane
           << " | result (bool): " << ScmCommons::BooleanToString(obj.intensityFollowingDriving);
  }
};

class SituationManager_HaveIntensitiesChangedSignificantly : public ::testing::Test,
                                                             public ::testing::WithParamInterface<DataFor_CheckFunctionHaveIntensitiesChangedSignificantly>
{
};

TEST_P(SituationManager_HaveIntensitiesChangedSignificantly, SituationManager_CheckFunction)
{
  SituationManagerTester TEST_HELPER;

  DataFor_CheckFunctionHaveIntensitiesChangedSignificantly data = GetParam();

  std::map<Situation, double> intensities{{Situation::COLLISION, 0.0},
                                          {Situation::FOLLOWING_DRIVING, data.intensityFollowingDriving},
                                          {Situation::FREE_DRIVING, 0.0},
                                          {Situation::LANE_CHANGER_FROM_LEFT, data.intensityLaneChangerFromLeft},
                                          {Situation::LANE_CHANGER_FROM_RIGHT, 0.0},
                                          {Situation::SIDE_COLLISION_RISK_FROM_LEFT, 0.0},
                                          {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, 0.0},
                                          {Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, data.intensitySuspiciousObjectInLeftLane},
                                          {Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, 0.0}};

  std::map<Situation, double> intensitiesLast{{Situation::COLLISION, 0.0},
                                              {Situation::FOLLOWING_DRIVING, data.lastIntensityFollowingDriving},
                                              {Situation::FREE_DRIVING, 0.0},
                                              {Situation::LANE_CHANGER_FROM_LEFT, data.lastIntensityLaneChangerFromLeft},
                                              {Situation::LANE_CHANGER_FROM_RIGHT, 0.0},
                                              {Situation::SIDE_COLLISION_RISK_FROM_LEFT, 0.0},
                                              {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, 0.0},
                                              {Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, data.lastIntensitySuspiciousObjectInLeftLane},
                                              {Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, 0.0}};

  std::map<Situation, double> intensitiesEmpty{};

  bool result = TEST_HELPER.situationManager.HaveIntensitiesChangedSignificantly(intensities, intensitiesLast);
  ASSERT_EQ(result, data.result);
}

/**********************************************************/
// The test data (must be defined below test)
/**********************************************************/

INSTANTIATE_TEST_SUITE_P(
    Default,
    SituationManager_HaveIntensitiesChangedSignificantly,
    testing::Values(
        // Test 1: Identical Maps
        DataFor_CheckFunctionHaveIntensitiesChangedSignificantly{
            .intensityFollowingDriving = 1.0,
            .intensityLaneChangerFromLeft = 0.5,
            .intensitySuspiciousObjectInLeftLane = 0.05,
            .lastIntensityFollowingDriving = 1.0,
            .lastIntensityLaneChangerFromLeft = 0.5,
            .lastIntensitySuspiciousObjectInLeftLane = 0.05,
            .result = false},
        // Test 2: Different maps, but no significant increase
        DataFor_CheckFunctionHaveIntensitiesChangedSignificantly{
            .intensityFollowingDriving = 1.01,
            .intensityLaneChangerFromLeft = 0.0,
            .intensitySuspiciousObjectInLeftLane = 0.0,
            .lastIntensityFollowingDriving = 1.0,
            .lastIntensityLaneChangerFromLeft = 0.0,
            .lastIntensitySuspiciousObjectInLeftLane = 0.0,
            .result = false},
        // Test 3: Different maps, but no significant increase
        DataFor_CheckFunctionHaveIntensitiesChangedSignificantly{
            .intensityFollowingDriving = 1.02,
            .intensityLaneChangerFromLeft = 0.5,
            .intensitySuspiciousObjectInLeftLane = 0.05,
            .lastIntensityFollowingDriving = 1.02,
            .lastIntensityLaneChangerFromLeft = 0.5,
            .lastIntensitySuspiciousObjectInLeftLane = 0.05,
            .result = false},
        // Test 4: Intensity change below value threshold but above percentage
        DataFor_CheckFunctionHaveIntensitiesChangedSignificantly{
            .intensityFollowingDriving = 1.0,
            .intensityLaneChangerFromLeft = 0.5,
            .intensitySuspiciousObjectInLeftLane = 0.05,
            .lastIntensityFollowingDriving = 1.3,
            .lastIntensityLaneChangerFromLeft = 0.5,
            .lastIntensitySuspiciousObjectInLeftLane = 0.05,
            .result = true},
        // Test 5: Intensity change above value threshold but below percentage
        DataFor_CheckFunctionHaveIntensitiesChangedSignificantly{
            .intensityFollowingDriving = 5.6,
            .intensityLaneChangerFromLeft = 0.5,
            .intensitySuspiciousObjectInLeftLane = 0.05,
            .lastIntensityFollowingDriving = 5.0,
            .lastIntensityLaneChangerFromLeft = 0.5,
            .lastIntensitySuspiciousObjectInLeftLane = 0.05,
            .result = true},
        // Test 6: Intensity change above value threshold and percentage
        DataFor_CheckFunctionHaveIntensitiesChangedSignificantly{
            .intensityFollowingDriving = 4.9,
            .intensityLaneChangerFromLeft = 0.5,
            .intensitySuspiciousObjectInLeftLane = 0.05,
            .lastIntensityFollowingDriving = 1.0,
            .lastIntensityLaneChangerFromLeft = 0.5,
            .lastIntensitySuspiciousObjectInLeftLane = 0.05,
            .result = true},
        // Test 7: Small intensities ignore percentage condition
        DataFor_CheckFunctionHaveIntensitiesChangedSignificantly{
            .intensityFollowingDriving = 0.1,
            .intensityLaneChangerFromLeft = 0.5,
            .intensitySuspiciousObjectInLeftLane = 0.05,
            .lastIntensityFollowingDriving = 0.05,
            .lastIntensityLaneChangerFromLeft = 0.5,
            .lastIntensitySuspiciousObjectInLeftLane = 0.05,
            .result = false},
        // Test 8: Intensity last was zero
        DataFor_CheckFunctionHaveIntensitiesChangedSignificantly{
            .intensityFollowingDriving = 5.0,
            .intensityLaneChangerFromLeft = 0.5,
            .intensitySuspiciousObjectInLeftLane = 0.05,
            .lastIntensityFollowingDriving = 0.0,
            .lastIntensityLaneChangerFromLeft = 0.5,
            .lastIntensitySuspiciousObjectInLeftLane = 0.05,
            .result = true},
        // Test 9: Intensity last was not in map and new intensity is zero
        DataFor_CheckFunctionHaveIntensitiesChangedSignificantly{
            .intensityFollowingDriving = 0.0,
            .intensityLaneChangerFromLeft = 0.5,
            .intensitySuspiciousObjectInLeftLane = 0.05,
            .lastIntensityFollowingDriving = 0.0,
            .lastIntensityLaneChangerFromLeft = 0.5,
            .lastIntensitySuspiciousObjectInLeftLane = 0.05,
            .result = false},
        // Test 10: Intensity is zero
        DataFor_CheckFunctionHaveIntensitiesChangedSignificantly{
            .intensityFollowingDriving = 0.0,
            .intensityLaneChangerFromLeft = 0.5,
            .intensitySuspiciousObjectInLeftLane = 0.05,
            .lastIntensityFollowingDriving = 4.0,
            .lastIntensityLaneChangerFromLeft = 0.5,
            .lastIntensitySuspiciousObjectInLeftLane = 0.05,
            .result = true}));

/********************************
 * CHECK SetLastIgnoreRiskState *
 ********************************/

struct DataFor_SetLastIgnoreRiskState
{
  std::map<Situation, double> intensities;
  bool expectedResult;
};

class SituationManager_SetLastIgnoreRiskState : public ::testing::Test,
                                                public ::testing::WithParamInterface<DataFor_SetLastIgnoreRiskState>
{
};

TEST_P(SituationManager_SetLastIgnoreRiskState, Check_SetLastIgnoreRiskState)
{
  SituationManagerTester TEST_HELPER;
  DataFor_SetLastIgnoreRiskState data = GetParam();

  TEST_HELPER.situationManager.SetLastIgnoreRiskState(Situation::FREE_DRIVING, data.intensities);

  auto result = TEST_HELPER.situationManager.GET_LAST_IGNORE_RISK_STATE();

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SituationManager_SetLastIgnoreRiskState,
    testing::Values(
        DataFor_SetLastIgnoreRiskState{
            .intensities{{Situation::FOLLOWING_DRIVING, 0.8}},
            .expectedResult = false},
        DataFor_SetLastIgnoreRiskState{
            .intensities{{Situation::FOLLOWING_DRIVING, 4.0}},
            .expectedResult = true},
        DataFor_SetLastIgnoreRiskState{
            .intensities{{Situation::FOLLOWING_DRIVING, 8.0}},
            .expectedResult = true}));

/***********************************************
 * CHECK GenerateIntensitiesAndSampleSituation *
 ***********************************************/

TEST(SituationManager_GenerateIntensitiesAndSampleSituation, Check_GenerateIntensitiesAndSampleSituation)
{
  SituationManagerTester TEST_HELPER;

  ON_CALL(TEST_HELPER.fakeMentalModel, GetCollisionState()).WillByDefault(Return(false));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetCausingVehicleOfFrontCluster()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, ResetCausingVehicleOfSideCluster()).Times(1);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetMicroscopicData()).WillByDefault(Return(&TEST_HELPER.fakeMicroscopicCharacteristics));
  OwnVehicleInformationScmExtended agentEgo;
  agentEgo.causingVehicleOfSituationFrontCluster.at(Situation::FOLLOWING_DRIVING) = AreaOfInterest::NumberOfAreaOfInterests;
  agentEgo.causingVehicleOfSituationFrontCluster.at(Situation::OBSTACLE_ON_CURRENT_LANE) = AreaOfInterest::NumberOfAreaOfInterests;

  agentEgo.causingVehicleOfSituationSideCluster.at(Situation::LANE_CHANGER_FROM_RIGHT) = AreaOfInterest::NumberOfAreaOfInterests;
  agentEgo.causingVehicleOfSituationSideCluster.at(Situation::LANE_CHANGER_FROM_LEFT) = AreaOfInterest::NumberOfAreaOfInterests;
  agentEgo.causingVehicleOfSituationSideCluster.at(Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE) = AreaOfInterest::NumberOfAreaOfInterests;
  agentEgo.causingVehicleOfSituationSideCluster.at(Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE) = AreaOfInterest::NumberOfAreaOfInterests;
  agentEgo.causingVehicleOfSituationSideCluster.at(Situation::SIDE_COLLISION_RISK_FROM_RIGHT) = AreaOfInterest::NumberOfAreaOfInterests;
  agentEgo.causingVehicleOfSituationSideCluster.at(Situation::SIDE_COLLISION_RISK_FROM_LEFT) = AreaOfInterest::NumberOfAreaOfInterests;

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetCausingVehicleOfSituationFrontCluster(Situation::FOLLOWING_DRIVING, AreaOfInterest::NumberOfAreaOfInterests)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetCausingVehicleOfSituationFrontCluster(Situation::OBSTACLE_ON_CURRENT_LANE, AreaOfInterest::NumberOfAreaOfInterests)).Times(1);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsVehicleVisible(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(true));

  NiceMock<FakeSurroundingVehicle> fakeVehicle;
  ON_CALL(fakeVehicle, GetRelativeNetDistance()).WillByDefault(Return(100.0_m));
  ON_CALL(fakeVehicle, IsReliable(DataQuality::MEDIUM, _)).WillByDefault(Return(true));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(&fakeVehicle));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsMinimumFollowingDistanceViolated(&fakeVehicle, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsObstacle(&fakeVehicle)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsNearEnoughForFollowing(_, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, HasJamVelocity(&fakeVehicle)).WillByDefault(Return(true));

  ON_CALL(TEST_HELPER.fakeMentalCalculations, GetInfDistance(AreaOfInterest::EGO_FRONT, _)).WillByDefault(Return(150_m));
  ObstructionScm obstruction{0_m, 0_m, 0_m};
  obstruction.isOverlapping = true;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetObstruction(_, _)).WillByDefault(Return(obstruction));

  DriverParameters driverParameters;
  driverParameters.maximumLongitudinalDeceleration = 3.0_mps_sq;
  driverParameters.maximumLateralAcceleration = 1.0_mps_sq;
  driverParameters.comfortLateralAcceleration = 3.0_mps_sq;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetDriverParameters()).WillByDefault(Return(driverParameters));

  ON_CALL(TEST_HELPER.fakeMentalCalculations, CalculateAccelerationToDefuse(_, _)).WillByDefault(Return(10.0_mps_sq));

  ON_CALL(TEST_HELPER.fakeMentalModel, GetComfortLongitudinalDeceleration()).WillByDefault(Return(2.0_mps_sq));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetCausingVehicleOfSituationFrontCluster(Situation::FOLLOWING_DRIVING, AreaOfInterest::EGO_FRONT)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetSituationAnticipated(Situation::FOLLOWING_DRIVING, _)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetSituationRisk(Situation::FOLLOWING_DRIVING, _)).Times(1);

  NiceMock<FakeSurroundingVehicle> fakeSideVehicle;
  ON_CALL(fakeSideVehicle, IsReliable(DataQuality::HIGH, _)).WillByDefault(Return(false));
  ON_CALL(fakeSideVehicle, IsReliable(DataQuality::MEDIUM, _)).WillByDefault(Return(false));
  ON_CALL(fakeSideVehicle, IsReliable(DataQuality::LOW, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(AreaOfInterest::LEFT_SIDE, _)).WillByDefault(Return(&fakeSideVehicle));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetIsAnticipating()).WillByDefault(Return(false));

  EXPECT_CALL(TEST_HELPER.fakeFeatureExtractor, IsVehicleInSideLane(_)).WillRepeatedly(Return(false));
  EXPECT_CALL(TEST_HELPER.fakeFeatureExtractor, IsVehicleInSideLane(&fakeSideVehicle)).WillOnce(Return(true));

  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsLaneDriveablePerceived(_)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, HasIntentionalLaneCrossingConflict(_)).WillByDefault(Return(false));
  ON_CALL(TEST_HELPER.fakeFeatureExtractor, IsInfluencingDistanceViolated(_)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, CalculateAccelerationToDefuseLateral(AreaOfInterest::LEFT_SIDE)).WillByDefault(Return(0.5_mps_sq));

  NiceMock<FakeSurroundingVehicle> fakeSideRightVehicle;
  ON_CALL(fakeSideRightVehicle, IsReliable(DataQuality::HIGH, _)).WillByDefault(Return(false));
  ON_CALL(fakeSideRightVehicle, IsReliable(DataQuality::MEDIUM, _)).WillByDefault(Return(false));
  ON_CALL(fakeSideRightVehicle, IsReliable(DataQuality::LOW, _)).WillByDefault(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVehicle(AreaOfInterest::RIGHT_SIDE, _)).WillByDefault(Return(&fakeSideRightVehicle));

  EXPECT_CALL(TEST_HELPER.fakeFeatureExtractor, IsVehicleInSideLane(&fakeSideRightVehicle)).WillOnce(Return(true));
  ON_CALL(TEST_HELPER.fakeMentalCalculations, CalculateAccelerationToDefuseLateral(AreaOfInterest::RIGHT_SIDE)).WillByDefault(Return(0.5_mps_sq));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetDurationCurrentSituation(0_ms)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, CheckSituationCooperativeBehavior()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetCurrentSituation(Situation::FOLLOWING_DRIVING)).Times(1);

  NiceMock<FakeIgnoringOuterLaneOvertakingProhibition> fakeIgnoringOuterLaneOvertakingProhibition;
  EXPECT_CALL(fakeIgnoringOuterLaneOvertakingProhibition, Update()).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeSwerving, CheckForPlannedEvading()).Times(1);

  TEST_HELPER.situationManager.SetIgnoringOuterLaneOvertakingProhibition(&fakeIgnoringOuterLaneOvertakingProhibition);
  NiceMock<FakeZipMerging> fakeZipMerging;
  TEST_HELPER.situationManager.GenerateIntensitiesAndSampleSituation(fakeZipMerging);
}

/***********************************
 * CHECK SetSideClusterIntensities *
 ***********************************/

struct DataFor_SetSideClusterIntensities
{
  Situation situation;
  double intensityFromAcceleration;
  HighCognitiveSituation situationHc;
  Risk risk;
};

class SituationManager_SetSideClusterIntensities : public ::testing::Test,
                                                   public ::testing::WithParamInterface<DataFor_SetSideClusterIntensities>
{
};

TEST_P(SituationManager_SetSideClusterIntensities, Check_SetSideClusterIntensities)
{
  SituationManagerTester TEST_HELPER;
  DataFor_SetSideClusterIntensities data = GetParam();

  std::map<Situation, double> situationIntensitiesSideCluster{{data.situation, 0.0}};

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetSituationAnticipated(data.situation, _)).Times(1);
  ON_CALL(TEST_HELPER.fakeMentalModel, GetHighCognitiveSituation).WillByDefault(Return(data.situationHc));

  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetSituationRisk(data.situation, data.risk)).Times(1);
  TEST_HELPER.situationManager.SetSideClusterIntensities(data.situation, true, 1.0, data.intensityFromAcceleration, situationIntensitiesSideCluster);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SituationManager_SetSideClusterIntensities,
    testing::Values(
        DataFor_SetSideClusterIntensities{
            .situation = Situation::FREE_DRIVING,
            .intensityFromAcceleration = 0.5,
            .situationHc = HighCognitiveSituation::APPROACH,
            .risk = Risk::LOW},
        DataFor_SetSideClusterIntensities{
            .situation = Situation::FREE_DRIVING,
            .intensityFromAcceleration = 2.0,
            .situationHc = HighCognitiveSituation::APPROACH,
            .risk = Risk::MEDIUM},
        DataFor_SetSideClusterIntensities{
            .situation = Situation::FREE_DRIVING,
            .intensityFromAcceleration = 5.0,
            .situationHc = HighCognitiveSituation::APPROACH,
            .risk = Risk::HIGH},
        DataFor_SetSideClusterIntensities{
            .situation = Situation::LANE_CHANGER_FROM_RIGHT,
            .intensityFromAcceleration = 0.0,
            .situationHc = HighCognitiveSituation::SALIENT_APPROACH,
            .risk = Risk::HIGH}));

/******************************
 * CHECK IsSituationAmbigious *
 ******************************/

struct DataFor_IsSituationAmbigious
{
  double situationIntensity;
  bool reliableFovea;
  bool reliableUfov;
  bool reliablePeriphery;
  bool expectedResult;
};

class SituationManager_IsSituationAmbigious : public ::testing::Test,
                                              public ::testing::WithParamInterface<DataFor_IsSituationAmbigious>
{
};

TEST_P(SituationManager_IsSituationAmbigious, Check_IsSituationAmbigious)
{
  SituationManagerTester TEST_HELPER;
  DataFor_IsSituationAmbigious data = GetParam();

  auto result = TEST_HELPER.situationManager.IsSituationAmbigious(data.situationIntensity, data.reliableFovea, data.reliableUfov, data.reliablePeriphery);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SituationManager_IsSituationAmbigious,
    testing::Values(
        DataFor_IsSituationAmbigious{
            .situationIntensity = 1.0,
            .reliableFovea = true,
            .reliableUfov = true,
            .reliablePeriphery = true,
            .expectedResult = false},
        DataFor_IsSituationAmbigious{
            .situationIntensity = 0.5,
            .reliableFovea = false,
            .reliableUfov = true,
            .reliablePeriphery = false,
            .expectedResult = true},
        DataFor_IsSituationAmbigious{
            .situationIntensity = 8.0,
            .reliableFovea = false,
            .reliableUfov = true,
            .reliablePeriphery = true,
            .expectedResult = true},
        DataFor_IsSituationAmbigious{
            .situationIntensity = 4.0,
            .reliableFovea = false,
            .reliableUfov = false,
            .reliablePeriphery = true,
            .expectedResult = true},
        DataFor_IsSituationAmbigious{
            .situationIntensity = 2.0,
            .reliableFovea = true,
            .reliableUfov = true,
            .reliablePeriphery = true,
            .expectedResult = false}));

/******************************************
 * CHECK GetIntensity_HighRiskLaneChanger *
 ******************************************/

struct DataFor_GetIntensity_HighRiskLaneChanger
{
  bool hasIntentionalLaneCrossingConflict;
  bool hasActualLaneCrossingConflict;
  bool isMinimumFollowingDistanceViolated;
  double expectedResult;
};

class SituationManager_GetIntensity_HighRiskLaneChanger : public ::testing::Test,
                                                          public ::testing::WithParamInterface<DataFor_GetIntensity_HighRiskLaneChanger>
{
};

TEST_P(SituationManager_GetIntensity_HighRiskLaneChanger, Check_GetIntensity_HighRiskLaneChanger)
{
  SituationManagerTester TEST_HELPER;
  DataFor_GetIntensity_HighRiskLaneChanger data = GetParam();

  auto result = TEST_HELPER.situationManager.GetIntensity_HighRiskLaneChanger(data.hasIntentionalLaneCrossingConflict, data.hasActualLaneCrossingConflict, data.isMinimumFollowingDistanceViolated);

  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SituationManager_GetIntensity_HighRiskLaneChanger,
    testing::Values(
        DataFor_GetIntensity_HighRiskLaneChanger{
            .hasIntentionalLaneCrossingConflict = true,
            .hasActualLaneCrossingConflict = true,
            .isMinimumFollowingDistanceViolated = true,
            .expectedResult = 1.0},
        DataFor_GetIntensity_HighRiskLaneChanger{
            .hasIntentionalLaneCrossingConflict = false,
            .hasActualLaneCrossingConflict = false,
            .isMinimumFollowingDistanceViolated = false,
            .expectedResult = 0.0}));

/****************************************
 * CHECK AnticipatedHighRiskLaneChanger *
 ****************************************/

struct DataFor_GetIntensity_AnticipatedHighRiskLaneChanger
{
  bool isMinimumFollowingDistanceViolated;
  bool isInfluencingDistanceViolated;
  bool hasAnticipatedLaneCrossingConflict;
  bool hasIntentionalLaneCrossingConflict;
  bool isLaneDriveablePerceived;
  bool isLaneChangePermittedDueToLaneMarkingsForAoi;
  double intensityForNeedToChangeLane;
  double intensityForSlowerLeadingVehicle;
  double expectedResult;
};

class SituationManager_GetIntensity_AnticipatedHighRiskLaneChanger : public ::testing::Test,
                                                                     public ::testing::WithParamInterface<DataFor_GetIntensity_AnticipatedHighRiskLaneChanger>
{
};

TEST_P(SituationManager_GetIntensity_AnticipatedHighRiskLaneChanger, Check_GetIntensity_AnticipatedHighRiskLaneChanger)
{
  SituationManagerTester TEST_HELPER;
  DataFor_GetIntensity_AnticipatedHighRiskLaneChanger data = GetParam();

  std::vector<ObjectInformationSCM> virtualAgents;
  ON_CALL(TEST_HELPER.fakeMentalModel, GetVirtualAgents()).WillByDefault(Return(virtualAgents));
  ON_CALL(TEST_HELPER.fakeMentalModel, GetCooperativeBehavior()).WillByDefault(Return(false));

  auto result = TEST_HELPER.situationManager.GetIntensity_AnticipatedHighRiskLaneChanger(false, data.isMinimumFollowingDistanceViolated, data.isInfluencingDistanceViolated, data.hasAnticipatedLaneCrossingConflict, data.hasIntentionalLaneCrossingConflict, data.isLaneDriveablePerceived, data.isLaneChangePermittedDueToLaneMarkingsForAoi, data.intensityForNeedToChangeLane, data.intensityForSlowerLeadingVehicle);
  ASSERT_EQ(result, data.expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SituationManager_GetIntensity_AnticipatedHighRiskLaneChanger,
    testing::Values(
        DataFor_GetIntensity_AnticipatedHighRiskLaneChanger{
            .isMinimumFollowingDistanceViolated = true,
            .isInfluencingDistanceViolated = true,
            .hasAnticipatedLaneCrossingConflict = true,
            .hasIntentionalLaneCrossingConflict = true,
            .isLaneDriveablePerceived = false,
            .isLaneChangePermittedDueToLaneMarkingsForAoi = false,
            .intensityForNeedToChangeLane = 1.0,
            .intensityForSlowerLeadingVehicle = 2.0,
            .expectedResult = 1.0},
        DataFor_GetIntensity_AnticipatedHighRiskLaneChanger{
            .isMinimumFollowingDistanceViolated = true,
            .isInfluencingDistanceViolated = false,
            .hasAnticipatedLaneCrossingConflict = true,
            .hasIntentionalLaneCrossingConflict = true,
            .isLaneDriveablePerceived = false,
            .isLaneChangePermittedDueToLaneMarkingsForAoi = false,
            .intensityForNeedToChangeLane = 1.0,
            .intensityForSlowerLeadingVehicle = 2.0,
            .expectedResult = 0.0},
        DataFor_GetIntensity_AnticipatedHighRiskLaneChanger{
            .isMinimumFollowingDistanceViolated = true,
            .isInfluencingDistanceViolated = false,
            .hasAnticipatedLaneCrossingConflict = true,
            .hasIntentionalLaneCrossingConflict = true,
            .isLaneDriveablePerceived = false,
            .isLaneChangePermittedDueToLaneMarkingsForAoi = true,
            .intensityForNeedToChangeLane = 1.0,
            .intensityForSlowerLeadingVehicle = 2.0,
            .expectedResult = 1.0},
        DataFor_GetIntensity_AnticipatedHighRiskLaneChanger{
            .isMinimumFollowingDistanceViolated = true,
            .isInfluencingDistanceViolated = true,
            .hasAnticipatedLaneCrossingConflict = true,
            .hasIntentionalLaneCrossingConflict = false,
            .isLaneDriveablePerceived = false,
            .isLaneChangePermittedDueToLaneMarkingsForAoi = true,
            .intensityForNeedToChangeLane = 1.0,
            .intensityForSlowerLeadingVehicle = 2.0,
            .expectedResult = 1.0},
        DataFor_GetIntensity_AnticipatedHighRiskLaneChanger{
            .isMinimumFollowingDistanceViolated = true,
            .isInfluencingDistanceViolated = true,
            .hasAnticipatedLaneCrossingConflict = true,
            .hasIntentionalLaneCrossingConflict = false,
            .isLaneDriveablePerceived = true,
            .isLaneChangePermittedDueToLaneMarkingsForAoi = true,
            .intensityForNeedToChangeLane = 1.0,
            .intensityForSlowerLeadingVehicle = 0.5,
            .expectedResult = 0.5},
        DataFor_GetIntensity_AnticipatedHighRiskLaneChanger{
            .isMinimumFollowingDistanceViolated = false,
            .isInfluencingDistanceViolated = true,
            .hasAnticipatedLaneCrossingConflict = true,
            .hasIntentionalLaneCrossingConflict = false,
            .isLaneDriveablePerceived = true,
            .isLaneChangePermittedDueToLaneMarkingsForAoi = true,
            .intensityForNeedToChangeLane = 1.0,
            .intensityForSlowerLeadingVehicle = 0.5,
            .expectedResult = 0.0}));

/*******************************************
 * CHECK AddRelevantSideClusterIntensities *
 *******************************************/

struct DataFor_AddRelevantSideClusterIntensities
{
  Situation situation;
  std::map<Situation, double> situationIntensitiesSideCluster;
  Side side;
  AreaOfInterest aoi;
};

class SituationManager_AddRelevantSideClusterIntensities : public ::testing::Test,
                                                           public ::testing::WithParamInterface<DataFor_AddRelevantSideClusterIntensities>
{
};

TEST_P(SituationManager_AddRelevantSideClusterIntensities, Check_AddRelevantSideClusterIntensities)
{
  SituationManagerTester TEST_HELPER;
  DataFor_AddRelevantSideClusterIntensities data = GetParam();

  ON_CALL(TEST_HELPER.fakeMentalModel, GetHighCognitiveSituation()).WillByDefault(Return(HighCognitiveSituation::APPROACH));
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetSituationAnticipated(data.situation, _)).Times(1);
  EXPECT_CALL(TEST_HELPER.fakeMentalModel, SetSituationRisk(data.situation, _)).Times(1);

  TEST_HELPER.situationManager.AddRelevantSideClusterIntensities(1.0, 1.0, false, 1.0, 1.0, 1.0, 1.0, data.situationIntensitiesSideCluster, data.side, data.aoi);
}

INSTANTIATE_TEST_SUITE_P(
    Default,
    SituationManager_AddRelevantSideClusterIntensities,
    testing::Values(
        DataFor_AddRelevantSideClusterIntensities{
            .situation = Situation::LANE_CHANGER_FROM_RIGHT,
            .situationIntensitiesSideCluster = {{Situation::LANE_CHANGER_FROM_RIGHT, 0.5},
                                                {Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, 2.0},
                                                {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, 2.0}},
            .side = Side::Right,
            .aoi = AreaOfInterest::RIGHT_FRONT},
        DataFor_AddRelevantSideClusterIntensities{
            .situation = Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE,
            .situationIntensitiesSideCluster = {{Situation::LANE_CHANGER_FROM_RIGHT, 2.0},
                                                {Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, 0.5},
                                                {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, 2.0}},
            .side = Side::Right,
            .aoi = AreaOfInterest::RIGHT_FRONT},
        DataFor_AddRelevantSideClusterIntensities{
            .situation = Situation::SIDE_COLLISION_RISK_FROM_RIGHT,
            .situationIntensitiesSideCluster = {{Situation::LANE_CHANGER_FROM_RIGHT, 2.0},
                                                {Situation::SUSPICIOUS_OBJECT_IN_RIGHT_LANE, 2.0},
                                                {Situation::SIDE_COLLISION_RISK_FROM_RIGHT, 0.5}},
            .side = Side::Right,
            .aoi = AreaOfInterest::RIGHT_FRONT},
        DataFor_AddRelevantSideClusterIntensities{
            .situation = Situation::LANE_CHANGER_FROM_LEFT,
            .situationIntensitiesSideCluster = {{Situation::LANE_CHANGER_FROM_LEFT, 0.5},
                                                {Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, 2.0},
                                                {Situation::SIDE_COLLISION_RISK_FROM_LEFT, 2.0}},
            .side = Side::Left,
            .aoi = AreaOfInterest::LEFT_FRONT},
        DataFor_AddRelevantSideClusterIntensities{
            .situation = Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE,
            .situationIntensitiesSideCluster = {{Situation::LANE_CHANGER_FROM_LEFT, 2.0},
                                                {Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, 0.5},
                                                {Situation::SIDE_COLLISION_RISK_FROM_LEFT, 2.0}},
            .side = Side::Left,
            .aoi = AreaOfInterest::LEFT_FRONT},
        DataFor_AddRelevantSideClusterIntensities{
            .situation = Situation::SIDE_COLLISION_RISK_FROM_LEFT,
            .situationIntensitiesSideCluster = {{Situation::LANE_CHANGER_FROM_LEFT, 2.0},
                                                {Situation::SUSPICIOUS_OBJECT_IN_LEFT_LANE, 2.0},
                                                {Situation::SIDE_COLLISION_RISK_FROM_LEFT, 0.5}},
            .side = Side::Left,
            .aoi = AreaOfInterest::LEFT_FRONT}));