/********************************************************************************
 * Copyright (c) 2020-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2020 HLRS, University of Stuttgart
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef SCMEXPORT_H
#define SCMEXPORT_H

#if defined(_WIN32) && !defined(NODLL)
#define SCMIMPORT __declspec(dllimport)
#define SCMEXPORT __declspec(dllexport)

#elif (defined(__GNUC__) && __GNUC__ >= 4 || defined(__clang__))
#define SCMEXPORT __attribute__((visibility("default")))
#define SCMIMPORT SCMEXPORT

#else
#define SCMIMPORT
#define SCMEXPORT
#endif

#if defined(Project_EXPORTS)
#define PROJECTEXPORT SCMEXPORT
#else
#define PROJECTEXPORT SCMIMPORT
#endif

#endif  // SCMEXPORT_H