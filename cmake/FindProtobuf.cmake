################################################################################
# Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
#
# find_package adapter for protobuf-static
#
# Original protpbuf CMake Config file doesn't provide static targets.
#
# Creates the follwoing imported targets (if available):
# - protobuf-static::libprotobuf_static

find_package(ZLIB)

set(PROTOBUF_STATIC_NAMES
  protobuf_static.lib
  libprotobuf.a
)

find_library(PROTOBUF_STATIC_LIBRARY NAMES ${PROTOBUF_STATIC_NAMES}
  PATHS
    ${PREFIX_PATH}
    /usr/local
    /usr
  PATH_SUFFIXES
    lib
    lib64
)

if(PROTOBUF_STATIC_LIBRARY)
  message(STATUS "Found protobuf (static): ${PROTOBUF_STATIC_LIBRARY}")
  get_filename_component(PROTOBUF_STATIC_LIBRARY_DIR "${PROTOBUF_STATIC_LIBRARY}" DIRECTORY)
  add_library(protobuf::libprotobuf_static IMPORTED STATIC)
  set_target_properties(protobuf::libprotobuf_static
                        PROPERTIES
                          IMPORTED_LOCATION ${PROTOBUF_STATIC_LIBRARY}
                          INTERFACE_INCLUDE_DIRECTORIES ${PROTOBUF_STATIC_LIBRARY_DIR}/../include
                          INTERFACE_LINK_LIBRARIES "pthread;ZLIB::ZLIB")
else()
  message(STATUS "Didn't find protobuf (static)")
endif()

unset(PROTOBUF_STATIC_LIBRARY)
